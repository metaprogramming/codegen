import pl.metaprogramming.codegen.Codegen;
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator;
import pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator;
import pl.metaprogramming.model.wsdl.WsdlApi;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class GeneratorRunner {

    public static void main(String[] args) throws IOException {
        generate(args[0]);
    }

    private static void generate(String baseDir) throws IOException {
        String wsdlPath = baseDir + "/src/main/resources/countries.wsdl";
        WsdlApi wsdlApi = WsdlApi.of(wsdlPath, cfg -> cfg.setServiceNameMapper(name -> name.replace("PortService", "")));
        new Codegen()
                .baseDir(new File(baseDir + "/.."))
                .indexFile(new File(baseDir + "/src/main/resource/GeneratedFiles.yaml"))
                .generate(new SpringCommonsGenerator(), cfg -> cfg
                        .rootPackage("example.commons")
                        .projectDir("app"))
                .generate(new SpringSoapClientGenerator(), cfg -> cfg
                        .model(wsdlApi)
                        .rootPackage("example.ws")
                        .setNamespacePackage("http://spring.io/guides/gs-producing-web-service", "example.ws.schema")
                        .projectDir("app"))
                .run();
    }

}
