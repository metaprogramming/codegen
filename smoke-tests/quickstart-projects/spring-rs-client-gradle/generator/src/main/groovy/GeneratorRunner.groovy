import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator
import pl.metaprogramming.model.oas.RestApi

class GeneratorRunner {

    static void main(String[] args) {
        generate(args[0], args[1])
    }

    static void generate(String baseDir, String indexFile) {
        new Codegen()
                .baseDir(new File(baseDir))
                .indexFile(new File(indexFile))
                .generate(new SpringCommonsGenerator()) {
                    it.rootPackage('example.commons')
                    it.typeOfCode(SpringCommonsGenerator.TOC.ENDPOINT_PROVIDER) { it.packageName.root = "example" }
                    it.projectDir('app')
                }
                .generate(new SpringRestClientGenerator()) {
                    it.model = RestApi.of(new File('src/main/resources/example-api.yaml').text)
                    it.rootPackage('example')
                    it.projectDir('app')
                }
                .run()
    }
}
