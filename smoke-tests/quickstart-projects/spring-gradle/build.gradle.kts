plugins {
    java
    id("pl.metaprogramming.codegen") version "2.0.1-SNAPSHOT"
    id("org.springframework.boot") version "2.7.18"
    id("io.spring.dependency-management") version "1.1.6"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.ws:spring-ws-core")
    implementation("com.google.code.findbugs:jsr305:3.0.2")

    implementation("javax.xml.bind:jaxb-api:2.3.1")
    implementation("com.sun.xml.bind:jaxb-impl:2.3.9")
    implementation("com.sun.xml.messaging.saaj:saaj-impl:1.5.3")

    compileOnly("org.projectlombok:lombok:1.18.34")
    annotationProcessor("org.projectlombok:lombok:1.18.34")

    implementation("org.zalando:logbook-spring-boot-starter:2.14.0")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.2")
    testImplementation("com.squareup.okhttp3:mockwebserver:4.12.0")
}

tasks.test {
    useJUnitPlatform()
}

codegen {
    dataTypeMapper[pl.metaprogramming.model.data.DataType.FLOAT] = "java.lang.Double"
    params.set(pl.metaprogramming.codegen.java.JavaParams::class) {
        generatedAnnotationValue = "my_generation_tag"
    }
    val api = restApi(file("example-api.yaml").readText(Charsets.UTF_8))
    generate(pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator()) {
        rootPackage = "example.commons"
        typeOfCode(toc.REST_EXCEPTION_HANDLER) {
            packageName.root = "example"
            onDecoration {
                methods["toResponseEntity(result, request)"].apply {
                    implDependencies.add(getClass(pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator.TOC.DTO, api.schemas.first { it.code == "ErrorDescription" }.dataType))
                    // or simpler
                    implDependencies.add("example.ports.in.rest.dtos.ErrorDescriptionDto")
                    implBody = """
                        |ValidationError firstError = result.getErrors().get(0);
                        |return ResponseEntity
                        |        .status(result.getStatus())
                        |        .header("x-correlation-id", request.getHeader("x-correlation-id"))
                        |        .body(new ErrorDescriptionDto()
                        |                .setCode(firstError.getCode())
                        |                .setField(firstError.getField())
                        |                .setMessage(String.format("%s: %s", firstError.getField(), firstError.getCode())));
                    """.trimMargin()
                }
            }
        }
    }
    generate(pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator()) {
        model = api
        rootPackage = "example"
        dataTypeMapper[pl.metaprogramming.model.data.DataType.FLOAT] = "java.math.BigDecimal"
    }
    generate(pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator()) {
        model = api
        rootPackage = "example"
        params.allowDuplicateClasses = true
        params.set(pl.metaprogramming.codegen.java.JavaParams::class) {
            generatedAnnotationValue = "my_client_generation_tag"
        }
        params[pl.metaprogramming.codegen.java.test.TestDataParams::class].isEnabled = false
    }
    generate(pl.metaprogramming.codegen.java.spring.SpringSwaggerUiGenerator()) {
        api(api, "example-api.yaml")
    }
    generate(pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator()) {
        model = wsdlApi("countries.wsdl")
        rootPackage = "example"
        setNamespacePackage("http://spring.io/guides/gs-producing-web-service", "example.ports.out.soap.dtos")
        setUrlProperty("COUNTRIES_SERVICE_URL")
        typeOfCode(toc.DTO).className.suffix = "SoapDto"
    }
}
