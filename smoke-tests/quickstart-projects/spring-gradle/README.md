Sample project illustrating the use of the codegen with gradle.

## Used generators:
- SpringCommonsGenerator
- SpringRestServiceGenerator
- SpringRestClientGenerator
- SpringSwaggerUiGenerator
- SpringSoapClientGenerator

## generate codes
`./gradlew generate`

## build project with generated codes
`./gradlew build`

## run spring boot application
`./gradlew bootRun`
