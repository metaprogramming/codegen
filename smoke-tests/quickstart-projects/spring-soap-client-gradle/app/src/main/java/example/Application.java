package example;

import example.ws.CountriesClient;
import example.ws.schema.GetCountryRequest;
import example.ws.schema.GetCountryResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner start(CountriesClient quoteClient) {
        return args -> {
            GetCountryResponse response = quoteClient.getCountry(new GetCountryRequest().setName("Poland"));
            log.info("response: " + response);
        };
    }
}
