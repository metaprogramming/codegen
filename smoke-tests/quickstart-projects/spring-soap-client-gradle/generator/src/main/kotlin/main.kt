import pl.metaprogramming.codegen.codegen
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator
import pl.metaprogramming.model.wsdl.WsdlApi
import java.io.File

fun main(args: Array<String>) {
    val wsdlApi = WsdlApi.of("src/main/resources/countries.wsdl") {
        serviceNameMapper = { name -> name.replace("PortService", "") }
    }

    codegen {
        baseDir = File(args[0])
        indexFile = File(args[1])

        generate(SpringCommonsGenerator()) {
            projectDir = "app"
            rootPackage = "example.commons"
        }

        generate(SpringSoapClientGenerator()) {
            model = wsdlApi
            projectDir = "app"
            rootPackage = "example.ws"
            setNamespacePackage("http://spring.io/guides/gs-producing-web-service", "example.ws.schema")
        }
    }
}