Sample project illustrating the use of the web-service client (SOAP) code generator.

## Sub projects

### app

Project (using Spring Boot) with web-service client.

It consumes services created in this [guide](https://spring.io/guides/gs/producing-web-service/).

[Here](https://spring.io/guides/gs/consuming-web-service/) is a guide for creating this client in more traditional way.


### generator

Project with an api model - [countries.wsdl](https://gitlab.com/metaprogramming/codegen-examples/spring-soap-client-gradle/-/blob/master/generator/src/main/resources/countries.wsdl) and a generator configuration/runner - [GeneratorRunner.groovy](https://gitlab.com/metaprogramming/codegen-examples/spring-soap-client-gradle/-/blob/master/generator/src/main/groovy/GeneratorRunner.groovy).

## HOWTO

### generates client codes
`./gradlew :generator:run`

### call web-service 
`./gradlew :app:bootRun`

NOTE: You must have a running server described in https://spring.io/guides/gs/producing-web-service/.
