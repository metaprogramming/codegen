package example.process;

import example.ports.in.rest.IEchoCommand;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EchoRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
public class EchoCommand implements IEchoCommand {
    

    @Override
    public ResponseEntity<EchoBodyDto> execute(@Nonnull EchoRequest request) {
        return ResponseEntity.ok(request.getBody());
    }

}
