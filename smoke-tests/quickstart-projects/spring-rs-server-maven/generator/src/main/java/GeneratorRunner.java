import pl.metaprogramming.codegen.Codegen;
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator;
import pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator;
import pl.metaprogramming.model.oas.RestApi;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class GeneratorRunner {

    public static void main(String[] args) {
        generate(args[0]);
    }

    static void generate(String baseDir) {
        new Codegen()
                .baseDir(new File(baseDir + "/.."))
                .indexFile(new File(baseDir + "/src/main/resources/GeneratedFiles.yaml"))
                .generate(new SpringCommonsGenerator(), cfg -> cfg
                        .rootPackage("example.commons")
                        .projectDir("app"))
                .generate(new SpringRestServiceGenerator(), cfg -> cfg
                        .model(RestApi.of(getResource("example-api.yaml")))
                        .rootPackage("example")
                        .projectDir("app"))
                .run();
    }

    static String getResource(String name) {
        return new BufferedReader(new InputStreamReader(GeneratorRunner.class.getResourceAsStream(name), StandardCharsets.UTF_8))
                .lines().collect(Collectors.joining(System.lineSeparator()));
    }
}
