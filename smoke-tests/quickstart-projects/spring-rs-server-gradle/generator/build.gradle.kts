plugins {
    kotlin("jvm") version "1.9.25"
    application
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("pl.metaprogramming:codegen:2.0.1-SNAPSHOT")
}

application {
    mainClass.set("MainKt")
}

tasks.withType(JavaExec::class) {
    args = listOf(
        rootProject.rootDir.path,
        File(project.projectDir.path, "src/main/resources/GeneratedFiles.yaml").path)
}
