import pl.metaprogramming.codegen.codegen
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator
import pl.metaprogramming.model.oas.RestApi
import java.io.File

fun main(args: Array<String>) {
    val apiText = object {}.javaClass.getResource("/example-api.yaml")?.readText(Charsets.UTF_8)
        ?: error("Can't read API file")

    codegen {
        baseDir = File(args[0])
        indexFile = File(args[1])

        generate(SpringCommonsGenerator()) {
            projectDir = "app"
            rootPackage = "example.commons"
            typeOfCode(toc.ENDPOINT_PROVIDER) { packageName.root = "example" }
        }

        generate(SpringRestServiceGenerator()) {
            model = RestApi.of(apiText)
            projectDir = "app"
            rootPackage = "example"
        }
    }
}