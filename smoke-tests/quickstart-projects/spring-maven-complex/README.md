Sample project illustrating the use of the codegen with maven as standard project.

## Used generators:
- SpringCommonsGenerator
- SpringRestServiceGenerator
- SpringRestClientGenerator
- SpringSwaggerUiGenerator
- SpringSoapClientGenerator

## generate and build codes
`mvn install`

## only generate codes
`mvn -f generator install`
`mvn -f app codegen:generate`

## run spring boot application
`mvn -f app spring-boot:run`
