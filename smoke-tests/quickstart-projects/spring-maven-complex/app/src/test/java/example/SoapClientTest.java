package example;

import example.ports.out.soap.CountriesPortServiceClient;
import example.ports.out.soap.dtos.GetCountryRequestSoapDto;
import example.ports.out.soap.dtos.GetCountryResponseSoapDto;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class SoapClientTest {

    @Autowired CountriesPortServiceClient client;

    @Test
    void testSoapCall() throws IOException {
        try (MockWebServer server = new MockWebServer()) {
            server.start(22222);
            server.setDispatcher(new MockDispatcher());
            GetCountryResponseSoapDto response = client.getCountry(new GetCountryRequestSoapDto().setName("Poland"));
            assertNotNull(response);
            assertEquals("PLN", response.getCountry().getCurrency().getValue());
        }
    }

    static class MockDispatcher extends Dispatcher {
        @Override
        public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
            MockResponse response = new MockResponse();
            response.setBody("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header /><SOAP-ENV:Body><ns2:getCountryResponse xmlns:ns2=\"http://spring.io/guides/gs-producing-web-service\"><ns2:country><ns2:name>POLAND</ns2:name><ns2:population>37636508</ns2:population><ns2:capital>Warsaw</ns2:capital><ns2:currency>PLN</ns2:currency></ns2:country></ns2:getCountryResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>");
            return response;
        }
    }
}
