package example;

import pl.metaprogramming.codegen.java.MethodCm;
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator;

import java.util.function.Consumer;

public class RestExceptionGeneratorInterceptor implements Consumer<SpringCommonsGenerator> {
    @Override
    public void accept(SpringCommonsGenerator generator) {
        generator.typeOfCode(SpringCommonsGenerator.TOC.REST_EXCEPTION_HANDLER, it -> {
            it.onDecoration(cls -> {
                        MethodCm methodCm = cls.getMethods().get("toResponseEntity(result, request)");
                        methodCm.getImplDependencies().add("example.ports.in.rest.dtos.ErrorDescriptionDto");
                        methodCm.setImplBody(cls.getCodeBuf()
                                .addLines("ValidationError firstError = result.getErrors().get(0);",
                                        "return ResponseEntity",
                                        "        .status(result.getStatus())",
                                        "        .header(\"x-correlation-id\", request.getHeader(\"x-correlation-id\"))",
                                        "        .body(new ErrorDescriptionDto()",
                                        "                .setCode(firstError.getCode())",
                                        "                .setField(firstError.getField())",
                                        "                .setMessage(String.format(\"%s: %s\", firstError.getField(), firstError.getCode())));"
                                )
                                .take()
                        );
                    }
            );
        });
    }
}
