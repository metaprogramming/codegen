Sample project illustrating the use of the codegen with maven.

## Used generators:
- SpringCommonsGenerator
- SpringRestServiceGenerator
- SpringRestClientGenerator
- SpringSwaggerUiGenerator
- SpringSoapClientGenerator

## generate and build codes
`mvn install`

## only generate codes
`mvn codegen:generate`

## run spring boot application
`mvn spring-boot:run`
