package example;

import example.commons.RestClientException;
import example.ports.out.rest.EchoClient;
import example.ports.out.rest.dtos.EchoBodyDto;
import example.ports.out.rest.dtos.EchoRequest;
import example.ports.out.rest.dtos.ErrorDescriptionDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class RestClientServerTest {

    @Autowired
    private EchoClient echoClient;

    @Test
    void testEchoCall() {
        String xCorrelationId = "xCorrelationId";
        String propAmount = "10.01";
        ZonedDateTime propDateTime = ZonedDateTime.now();
        Double propDouble = 20.02;
        Double propFloat = 40.04;
        Integer propInt = 3;
        ResponseEntity<EchoBodyDto> responseEntity = echoClient.echo(new EchoRequest()
                .setXCorrelationId(xCorrelationId)
                .setBody(new EchoBodyDto()
                        .setPropAmount(propAmount)
                        .setPropDateTime(propDateTime)
                        .setPropDouble(propDouble)
                        .setPropFloat(propFloat)
                        .setPropInt(propInt)
                )
        );

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(xCorrelationId, responseEntity.getHeaders().get("x-correlation-id").get(0));
        assertEquals(propAmount, responseEntity.getBody().getPropAmount());
        assertEquals(propDateTime.toInstant(), responseEntity.getBody().getPropDateTime().toInstant());
        assertEquals(propDouble, responseEntity.getBody().getPropDouble());
        assertEquals(propFloat, responseEntity.getBody().getPropFloat());
        assertEquals(propInt, responseEntity.getBody().getPropInt());
    }

    @Test
    void testEchoCallWithInvalidData() {
        String xCorrelationId = "xCorrelationId";
        RestClientException response = assertThrows(RestClientException.class, () -> echoClient.echo(new EchoRequest()
                .setXCorrelationId(xCorrelationId)
                .setBody(new EchoBodyDto().setPropDouble(0.0))
        ));

        assertEquals(xCorrelationId, response.getHeaders().get("x-correlation-id").get(0));

        ErrorDescriptionDto description = response.getResponse(ErrorDescriptionDto.class);
        assertEquals("prop_double", description.getField());
        assertEquals("is_too_small", description.getCode());
    }
}
