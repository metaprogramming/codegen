/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.out.rest

import example.ports.out.rest.dtos.EchoDateArrayGetRequest

class EchoDateArrayGetClientSpec extends BaseSpecification {

    def "should echo empty array"() {
        when:
        def response = client.echoDateArrayGet(new EchoDateArrayGetRequest())
        then:
        response.size() == 0
    }

    def "should echo single item"() {
        given:
        def list = [date()]
        when:
        def result = client.echoDateArrayGet(new EchoDateArrayGetRequest(dateArray: list))
        then:
        result.size() == 1
        result[0] == list[0]
    }

    def "should echo many items"() {
        given:
        def list = [date(), date(1), date(-1)]
        when:
        def response = client.echoDateArrayGet(new EchoDateArrayGetRequest(dateArray: list))
        then:
        response == list
    }
}
