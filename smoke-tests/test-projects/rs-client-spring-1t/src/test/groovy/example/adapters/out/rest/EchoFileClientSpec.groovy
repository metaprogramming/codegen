/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.out.rest

import example.ports.out.rest.dtos.DeleteFileRequest
import example.ports.out.rest.dtos.DownloadEchoFileRequest
import example.ports.out.rest.dtos.UploadEchoFileRequest
import example.ports.out.rest.dtos.UploadEchoFileWithFormRequest
import org.springframework.core.io.ByteArrayResource
import org.springframework.web.client.HttpClientErrorException

import java.nio.charset.StandardCharsets
import java.util.function.Consumer

class EchoFileClientSpec extends BaseSpecification {

    def id = System.currentTimeMillis()
    def fileBody = "Treść pliku"

    def "should upload, download and delete - #contentType"() {
        when:
        client.downloadEchoFile(new DownloadEchoFileRequest(id: id))
        then:
        thrown HttpClientErrorException.NotFound

        when:
        (uploadReqCall as Consumer).accept(this)
        then:
        noExceptionThrown()

        when:
        def downloadFile = client.downloadEchoFile(new DownloadEchoFileRequest(id: id))
        then:
        downloadFile.body.inputStream.getText('UTF8') == fileBody

        when:
        client.deleteFile(new DeleteFileRequest(id: id))
        then:
        noExceptionThrown()

        when:
        client.downloadEchoFile(new DownloadEchoFileRequest(id: id))
        then:
        thrown HttpClientErrorException.NotFound

        where:
        contentType                | uploadReqCall
        'application/octet-stream' | { EchoFileClientSpec spec -> spec.uploadCall() }
        'multipart/form-data'      | { EchoFileClientSpec spec -> spec.uploadWithFormCall() }
    }

    def uploadCall() {
        def resource = new ByteArrayResource(fileBody.getBytes(StandardCharsets.UTF_8))
        client.uploadEchoFile(new UploadEchoFileRequest(id: id, body: resource))
    }

    def uploadWithFormCall() {
        def resource = new ByteArrayResource(fileBody.getBytes(StandardCharsets.UTF_8)) {
            String getFilename() { "test.txt" }
        }
//        client.uploadEchoFile(new UploadEchoFileRequest(id: id, requestBody: resource))
        client.uploadEchoFileWithForm(new UploadEchoFileWithFormRequest(id: id, file: resource))
    }
}
