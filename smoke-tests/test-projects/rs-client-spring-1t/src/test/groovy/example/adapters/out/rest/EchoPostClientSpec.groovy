/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.out.rest

import example.commons.RestClientException
import example.ports.out.rest.dtos.*

import java.time.LocalDate
import java.time.ZonedDateTime

class EchoPostClientSpec extends BaseSpecification {

    def "should success for required fields"() {
        given:
        def request = new EchoPostRequest()
                .setAuthorization(AUTH_TOKEN)
                .setXCorrelationId(CORRELATION_ID)
                .setRequestBody(new EchoBodyDto().setPropIntRequired(123L))
        when:
        def response = client.echoPost(request)

        then:
        request.requestBody == response.body
        request.xCorrelationId == response.headers.getFirst(CORRELATION_ID_HEADER)
    }

    def "should success for all fields"() {
        given:
        def request = new EchoPostRequest()
                .setAuthorization(AUTH_TOKEN)
                .setXCorrelationId(CORRELATION_ID)
                .setTimestamp(ZonedDateTime.now())
                .setInlineHeaderParam("bla-bla-bla")
                .setRequestBody(new EchoBodyDto()
                        .setPropInt(1)
                        .setPropIntSecond(2)
                        .setPropIntRequired(123L)
                        .setPropFloat(12F)
                        .setPropDouble(23D)
                        .setPropAmount("12.10")
                        .setPropAmountNumber(BigDecimal.valueOf(12.12))
                        .setPropString("WHITE")
                        .setPropStringPattern("-/@?")
                        .setPropDefault("default-text")
                        .setPropDate(LocalDate.now())
                        .setPropDateSecond(LocalDate.now())
                        .setPropDateTime(dateTime())
                        .setPropBoolean(true)
                        .setPropBase64("test".bytes)
                        .setPropObject(new SimpleObjectDto()
                                .setSoPropDate(LocalDate.now().minusDays(1))
                                .setSoPropString("la"))
                        .setPropObjectAny([p: 'p1', o: [i: 12, b: true]])
                        .setPropObjectExtended((ExtendedObjectDto) new ExtendedObjectDto()
                                .setEoEnumReusable(ReusableEnum.B)
                                .setSoPropDate(LocalDate.now().minusDays(2))
                                .setSoPropString("al")
                        )
                        .setPropEnumReusable(ReusableEnum.B)
                        .setPropEnum(EnumType.V_1)
                )
        when:
        def response = client.echoPost(request)

        then:
        request.requestBody == response.body
        request.xCorrelationId == response.headers.getFirst(CORRELATION_ID_HEADER)
    }

    def "should fail on validation"() {
        given:
        def request = new EchoPostRequest()
                .setAuthorization(AUTH_TOKEN)
                .setXCorrelationId(CORRELATION_ID)
                .setRequestBody(new EchoBodyDto())
        when:
        client.echoPost(request)

        then:
        //thrown RestResponseException
        def e = thrown(RestClientException)
        e.response instanceof ErrorDescriptionDto

        when:
        def details = e.getResponse(ErrorDescriptionDto)
        then:
        details.errors.size() == 1
        details.errors[0].field == 'prop_int_required'
        details.errors[0].code == 'is_required'
        details.errors[0].message == 'prop_int_required is required'
    }
}
