/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.out.rest

import example.commons.RestClientException
import example.ports.out.rest.dtos.*
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

class EchoArraysPostClientSpec extends BaseSpecification {

    def "should echo empty array"() {
        when:
        def result = client.echoArraysPost(new EchoArraysPostRequest(
                authorization: AUTH_TOKEN,
                body: []
        ))
        then:
        result.statusCode == HttpStatus.NO_CONTENT
    }

    def "should echo single item"() {
        given:
        def item = createItem(1)
        when:
        ResponseEntity result = client.echoArraysPost(new EchoArraysPostRequest(
                authorization: AUTH_TOKEN,
                body: [item]
        ))
        then:
        result.statusCode == HttpStatus.OK
        result.body.size() == 1
        result.body[0] == item
    }

    def "should echo many items"() {
        given:
        def items = [createItem(1), createItem(2), createItem(3)]
        when:
        def result = client.echoArraysPost(new EchoArraysPostRequest(
                authorization: AUTH_TOKEN,
                body: items
        ))
        then:
        result.body == items
    }


    def "should fail on validation"() {
        when:
        client.echoArraysPost(new EchoArraysPostRequest(
                authorization: AUTH_TOKEN,
                body: [createItem(-1)]
        ))
        then:
        def e = thrown(RestClientException)
        e.response instanceof ErrorDescriptionDto
        e.httpStatusCode == 400

        when:
        def details = e.getResponse(ErrorDescriptionDto)
        then:
        details.errors.size() == 1
        details.errors[0].field == '[0].prop_double_list[0]'
        details.errors[0].code == 'is_too_small'
    }


    private EchoArraysBodyDto createItem(int factor) {
        return new EchoArraysBodyDto()
                .setPropAmountList(["123.10"])
                .setPropBooleanList([true])
                .setPropDateList([date()])
                .setPropDateTimeListOfList([[dateTime(), dateTime(-1)]])
                .setPropDoubleList([12.12 * factor as Double, 11.11 * factor as Double])
                .setPropEnumList([EnumType.V_1, EnumType.B])
                .setPropEnumReusableList([ReusableEnum.A])
                .setPropFloatList([1.1f * factor as Float])
                .setPropIntList([factor])
                .setPropObjectList([new SimpleObjectDto().setSoPropString("123456").setSoPropDate(date())])
                .setPropObjectListOfList([[new SimpleObjectDto().setSoPropString("654321").setSoPropDate(date())]])
                .setPropStringList(["MOUSE"])
                .setPropMapOfInt(["k1": 1])
                .setPropMapOfObject(["k1o": new SimpleObjectDto().setSoPropString("234567").setSoPropDate(date())])
                .setPropMapOfListOfObject(["k1o": [new SimpleObjectDto().setSoPropString("345678").setSoPropDate(date())]])
    }

}
