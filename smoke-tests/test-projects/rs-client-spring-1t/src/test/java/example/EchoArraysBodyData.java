package example;

import example.commons.SimpleObjectData;
import example.commons.TestData;
import javax.annotation.processing.Generated;
import static example.commons.TestData.*;

@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyData {

    private EchoArraysBodyData() {
    }

    public static TestData minDataSet() {
        return testData()
                .set("prop_double_list", list(0.01))
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("prop_int_list", list(-1))
                .set("prop_float_list", list(101))
                .set("prop_double_list", list(0.01))
                .set("prop_amount_list", list("prop_amount_list"))
                .set("prop_string_list", list("prop_strin"))
                .set("prop_date_list", list(date()))
                .set("prop_date_time_list_of_list", list(list(dateTime())))
                .set("prop_boolean_list", list(false))
                .set("prop_object_list", list(SimpleObjectData.fullDataSet().make()))
                .set("prop_object_list_of_list", list(list(SimpleObjectData.fullDataSet().make())))
                .set("prop_enum_reusable_list", list("a"))
                .set("prop_enum_list", list("A"))
                .set("prop_map_of_int", map("key", 100))
                .set("prop_map_of_object", map("key", SimpleObjectData.fullDataSet().make()))
                .set("prop_map_of_list_of_object", map("key", list(SimpleObjectData.fullDataSet().make())))
                ;
    }
}
