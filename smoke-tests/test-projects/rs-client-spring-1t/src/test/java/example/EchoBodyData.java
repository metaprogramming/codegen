package example;

import example.commons.SimpleObjectData;
import example.commons.TestData;
import javax.annotation.processing.Generated;
import static example.commons.TestData.*;

@Generated("pl.metaprogramming.codegen")
public class EchoBodyData {

    private EchoBodyData() {
    }

    public static TestData minDataSet() {
        return testData()
                .set("prop_int_required", -1)
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("prop_int", 1)
                .set("prop_int_second", 1)
                .set("prop_int_required", -1)
                .set("prop_float", 101)
                .set("prop_double", 5000)
                .set("prop_amount", "prop_amount")
                .set("prop_amount_number", 0.02)
                .set("prop_string", "prop_strin")
                .set("prop_string_pattern", "prop_string_pattern")
                .set("prop_default", "value")
                .set("prop_date", date())
                .set("prop_date_second", date())
                .set("prop_date_time", dateTime())
                .set("prop_base64", "XYZ-BASE64")
                .set("prop_boolean", false)
                .set("prop_object", SimpleObjectData.fullDataSet().make())
                .set("prop_object_any", testData().set("prop", "value").make())
                .set("prop_object_extended", ExtendedObjectData.fullDataSet().make())
                .set("prop_enum_reusable", "a")
                .set("prop_enum", "A")
                ;
    }
}
