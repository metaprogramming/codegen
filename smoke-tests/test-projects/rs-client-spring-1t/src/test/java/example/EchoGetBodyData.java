package example;

import example.commons.TestData;
import javax.annotation.processing.Generated;
import static example.commons.TestData.*;

@Generated("pl.metaprogramming.codegen")
public class EchoGetBodyData {

    private EchoGetBodyData() {
    }

    public static TestData minDataSet() {
        return testData()
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("prop_int_required", 1)
                .set("prop_float", 1.1)
                .set("prop_enum", "a1")
                ;
    }
}
