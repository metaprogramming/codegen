package example;

import example.commons.TestData;
import javax.annotation.processing.Generated;
import static example.commons.TestData.*;

@Generated("pl.metaprogramming.codegen")
public class MultiContentBodyData {

    private MultiContentBodyData() {
    }

    public static TestData minDataSet() {
        return testData()
                .set("response_content_type", "application/json")
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("response_content_type", "application/json")
                .set("prop_int", 1)
                .set("prop_int2", 1)
                .set("prop_date", date())
                .set("prop_list", list("prop_list"))
                ;
    }
}
