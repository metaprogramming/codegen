package example.commons;

import javax.annotation.processing.Generated;
import static example.commons.TestData.*;

@Generated("pl.metaprogramming.codegen")
public class ErrorItemData {

    private ErrorItemData() {
    }

    public static TestData minDataSet() {
        return testData()
                .set("code", "code")
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("code", "code")
                ;
    }
}
