package example.commons;

import javax.annotation.processing.Generated;
import static example.commons.TestData.*;

@Generated("pl.metaprogramming.codegen")
public class ErrorDetailData {

    private ErrorDetailData() {
    }

    public static TestData minDataSet() {
        return testData()
                .set("code", "code")
                .set("message", "message")
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("field", "field")
                .set("code", "code")
                .set("message", "message")
                ;
    }
}
