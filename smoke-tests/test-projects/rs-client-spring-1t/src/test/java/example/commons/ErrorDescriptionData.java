package example.commons;

import javax.annotation.processing.Generated;
import static example.commons.TestData.*;

@Generated("pl.metaprogramming.codegen")
public class ErrorDescriptionData {

    private ErrorDescriptionData() {
    }

    public static TestData minDataSet() {
        return testData()
                .set("code", 1)
                .set("message", "message")
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("code", 1)
                .set("message", "message")
                .set("errors", list(ErrorDetailData.fullDataSet().make()))
                ;
    }
}
