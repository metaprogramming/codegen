package example;

import example.commons.TestData;
import javax.annotation.processing.Generated;
import static example.commons.TestData.*;

@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectData {

    private ExtendedObjectData() {
    }

    public static TestData minDataSet() {
        return testData()
                .set("so_prop_string", "so_prop_string")
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("eo_enum_reusable", "a")
                .set("self_property", ExtendedObjectData.fullDataSet().make())
                .set("so_prop_string", "so_prop_string")
                .set("so_prop_date", date())
                ;
    }
}
