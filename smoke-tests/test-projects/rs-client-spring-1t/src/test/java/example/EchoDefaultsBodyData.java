package example;

import example.commons.TestData;
import javax.annotation.processing.Generated;
import static example.commons.TestData.*;

@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyData {

    private EchoDefaultsBodyData() {
    }

    public static TestData minDataSet() {
        return testData()
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("propInt", 101)
                .set("propFloat", 101.101)
                .set("propDouble", 202.202)
                .set("propNumber", 3.33)
                .set("propString", "WHITE")
                .set("propEnum", "3")
                .set("propDate", "2021-09-01")
                .set("propDateTime", "2021-09-01T09:09:09Z")
                ;
    }
}
