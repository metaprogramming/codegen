package example.commons;

import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;

@RequiredArgsConstructor
@Getter
@Generated("pl.metaprogramming.codegen")
public class RestClientException extends RuntimeException {

    @Nonnull private final Integer httpStatusCode;
    @Nonnull private final HttpHeaders headers;
    @Nonnull private final transient Object response;

    public <T> T getResponse(Class<T> clazz) {
        return clazz.cast(response);
    }
}
