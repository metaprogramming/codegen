package example.adapters.out.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import example.commons.RestClient;
import example.ports.out.rest.dtos.ErrorDescriptionDto;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class ExampleApiRestClientProvider {

    @Value("${EXAMPLE_API_BASE_URL:http://localhost:8080}") private String baseUrl;
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    public RestClient of(HttpMethod method, String path) {
        return new RestClient(method, baseUrl + path, restTemplate, objectMapper).onError(ErrorDescriptionDto.class);
    }
}
