package example.adapters.out.rest;

import example.ports.out.rest.Echo1tOnlyClient;
import example.ports.out.rest.dtos.EchoMultiContentRequest;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class Echo1tOnlyClientImpl implements Echo1tOnlyClient {

    private final ExampleApiRestClientProvider clientProvider;

    @Override
    public ResponseEntity<Object> echoMultiContent(EchoMultiContentRequest request) {
        return clientProvider.of(HttpMethod.POST, "/api/v1/echo-multi-content")
                .body(request.getBody())
                .exchange(Object.class);
    }
}
