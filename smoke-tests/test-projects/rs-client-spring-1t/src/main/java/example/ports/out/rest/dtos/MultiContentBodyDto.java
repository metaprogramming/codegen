package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class MultiContentBodyDto {

    @JsonProperty("response_content_type") @Nonnull private ResponseContentType responseContentType;
    @JsonProperty("prop_int") @Nullable private Integer propInt;
    @JsonProperty("prop_int2") @Nullable private Integer propInt2;
    @JsonProperty("prop_date") @Nullable private LocalDate propDate;
    @JsonProperty("prop_list") @Nullable private List<String> propList;
}
