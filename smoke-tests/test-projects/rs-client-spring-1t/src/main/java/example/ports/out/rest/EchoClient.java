package example.ports.out.rest;

import example.ports.out.rest.dtos.DeleteFileRequest;
import example.ports.out.rest.dtos.DownloadEchoFileRequest;
import example.ports.out.rest.dtos.EchoArraysBodyDto;
import example.ports.out.rest.dtos.EchoArraysPostRequest;
import example.ports.out.rest.dtos.EchoBodyDto;
import example.ports.out.rest.dtos.EchoDateArrayGetRequest;
import example.ports.out.rest.dtos.EchoDefaultsBodyDto;
import example.ports.out.rest.dtos.EchoDefaultsPostRequest;
import example.ports.out.rest.dtos.EchoEmptyRequest;
import example.ports.out.rest.dtos.EchoErrorRequest;
import example.ports.out.rest.dtos.EchoGetBodyDto;
import example.ports.out.rest.dtos.EchoGetRequest;
import example.ports.out.rest.dtos.EchoPostRequest;
import example.ports.out.rest.dtos.UploadEchoFileRequest;
import example.ports.out.rest.dtos.UploadEchoFileWithFormRequest;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

@Generated("pl.metaprogramming.codegen")
public interface EchoClient {

    /**
     * [POST] /api/v1/echo
     */
    ResponseEntity<EchoBodyDto> echoPost(EchoPostRequest request);

    /**
     * [GET] /api/v1/echo
     */
    EchoGetBodyDto echoGet(EchoGetRequest request);

    /**
     * [POST] /api/v1/echo-defaults
     */
    ResponseEntity<EchoDefaultsBodyDto> echoDefaultsPost(EchoDefaultsPostRequest request);

    /**
     * [POST] /api/v1/echo-arrays
     */
    ResponseEntity<List<EchoArraysBodyDto>> echoArraysPost(EchoArraysPostRequest request);

    /**
     * [GET] /api/v1/echo-date-array
     */
    List<LocalDate> echoDateArrayGet(EchoDateArrayGetRequest request);

    /**
     * [POST] /api/v1/echo-file/{id}/form<br/>
     * upload file using multipart/form-data
     */
    void uploadEchoFileWithForm(UploadEchoFileWithFormRequest request);

    /**
     * [POST] /api/v1/echo-file/{id}<br/>
     * upload file
     */
    void uploadEchoFile(UploadEchoFileRequest request);

    /**
     * [GET] /api/v1/echo-file/{id}<br/>
     * Returns a file
     */
    ResponseEntity<Resource> downloadEchoFile(DownloadEchoFileRequest request);

    /**
     * [DELETE] /api/v1/echo-file/{id}<br/>
     * deletes a file based on the id supplied
     */
    void deleteFile(DeleteFileRequest request);

    /**
     * [GET] /api/v1/echo-empty
     */
    void echoEmpty(EchoEmptyRequest request);

    /**
     * [GET] /api/v1/echo-error
     */
    void echoError(EchoErrorRequest request);
}
