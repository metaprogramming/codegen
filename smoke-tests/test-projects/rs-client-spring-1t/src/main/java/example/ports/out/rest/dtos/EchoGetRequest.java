package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetRequest {

    
    /**
     * Authorization HEADER parameter.
     */
    @JsonProperty("Authorization") @Nullable private String authorization;
    
    /**
     * api_key QUERY parameter.
     */
    @JsonProperty("api_key") @Nullable private String apiKey;
    
    /**
     * X-Correlation-ID HEADER parameter.<br/>
     * Correlates HTTP requests between a client and server.
     */
    @JsonProperty("correlationIdParam") @Nonnull private String xCorrelationId;
    
    /**
     * prop_int_required QUERY parameter.
     */
    @JsonProperty("prop_int_required") @Nonnull private Long propIntRequired;
    
    /**
     * prop_float QUERY parameter.
     */
    @JsonProperty("prop_float") @Nullable private Float propFloat;
    
    /**
     * prop_enum QUERY parameter.
     */
    @JsonProperty("prop_enum") @Nullable private DefaultEnum propEnum;
}
