package example.ports.out.rest.dtos;

import example.commons.SerializationUtils;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Echo object with default values
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyDto {

    @Nullable private Integer propInt = 101;
    @Nullable private Float propFloat = 101.101f;
    @Nullable private Double propDouble = 202.202;
    @Nullable private BigDecimal propNumber = new BigDecimal("3.33");
    @Nullable private String propString = "WHITE";
    @Nullable private ReusableEnum propEnum = ReusableEnum.V_3;
    @Nullable private LocalDate propDate = SerializationUtils.toLocalDate("2021-09-01");
    @Nullable private ZonedDateTime propDateTime = ZonedDateTime.parse("2021-09-01T09:09:09Z");
}
