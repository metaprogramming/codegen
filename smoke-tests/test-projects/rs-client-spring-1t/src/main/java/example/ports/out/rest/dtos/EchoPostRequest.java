package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.ZonedDateTime;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoPostRequest {

    
    /**
     * Authorization HEADER parameter.<br/>
     * The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
     */
    @JsonProperty("authorizationParam") @Nullable private String authorization;
    
    /**
     * X-Correlation-ID HEADER parameter.<br/>
     * Correlates HTTP requests between a client and server.
     */
    @JsonProperty("correlationIdParam") @Nonnull private String xCorrelationId;
    
    /**
     * timestamp HEADER parameter.
     */
    @JsonProperty("timestampParam") @Nullable private ZonedDateTime timestamp;
    
    /**
     * Inline-Header-Param HEADER parameter.<br/>
     * Example header param
     */
    @JsonProperty("Inline-Header-Param") @Nullable private String inlineHeaderParam;
    
    /**
     * body param
     */
    @Nonnull private EchoBodyDto requestBody;
}
