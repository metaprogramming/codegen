package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostRequest {

    
    /**
     * Authorization HEADER parameter.<br/>
     * The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
     */
    @JsonProperty("authorizationParam") @Nullable private String authorization;
    
    /**
     * Inline-Header-Param HEADER parameter.<br/>
     * Example header param
     */
    @JsonProperty("Inline-Header-Param") @Nullable private String inlineHeaderParam;
    
    /**
     * body param
     */
    @Nonnull private List<EchoArraysBodyDto> body;
}
