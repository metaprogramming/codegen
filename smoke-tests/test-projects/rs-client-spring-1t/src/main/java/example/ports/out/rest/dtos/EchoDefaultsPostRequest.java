package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostRequest {

    
    /**
     * Default-Header-Param HEADER parameter.<br/>
     * Example header param with default value
     */
    @JsonProperty("Default-Header-Param") @Nullable private DefaultEnum defaultHeaderParam = DefaultEnum.A2;
    
    /**
     * body param
     */
    @Nullable private EchoDefaultsBodyDto body;
}
