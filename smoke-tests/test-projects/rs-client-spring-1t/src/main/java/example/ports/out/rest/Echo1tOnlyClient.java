package example.ports.out.rest;

import example.ports.out.rest.dtos.EchoMultiContentRequest;
import javax.annotation.processing.Generated;
import org.springframework.http.ResponseEntity;

@Generated("pl.metaprogramming.codegen")
public interface Echo1tOnlyClient {

    /**
     * [POST] /api/v1/echo-multi-content
     */
    ResponseEntity<Object> echoMultiContent(EchoMultiContentRequest request);
}
