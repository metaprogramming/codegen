package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetRequest {

    
    /**
     * date_array QUERY parameter.
     */
    @JsonProperty("date_array") @Nullable private List<LocalDate> dateArray;
}
