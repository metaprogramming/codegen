package example.ports.out.rest.dtos;

import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentRequest {

    
    /**
     * body param
     */
    @Nonnull private MultiContentBodyDto body;
}
