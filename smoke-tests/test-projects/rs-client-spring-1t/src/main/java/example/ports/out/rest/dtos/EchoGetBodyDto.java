package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetBodyDto {

    @JsonProperty("prop_int_required") @Nullable private Long propIntRequired;
    @JsonProperty("prop_float") @Nullable private Float propFloat;
    @JsonProperty("prop_enum") @Nullable private DefaultEnum propEnum;
}
