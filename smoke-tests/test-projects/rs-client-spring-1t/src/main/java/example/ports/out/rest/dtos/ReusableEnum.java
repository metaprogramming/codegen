package example.ports.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonValue;
import example.commons.EnumValue;
import javax.annotation.processing.Generated;
import lombok.Getter;

/**
 * Reusable enum type title
 * 
 * Reusable enum type description
 */
@Generated("pl.metaprogramming.codegen")
public enum ReusableEnum implements EnumValue {

    A("a"), // a value
    B("B"), // B value
    V_3("3"); // 3 value

    @Getter @JsonValue private final String value;

    ReusableEnum(String value) {
        this.value = value;
    }
}
