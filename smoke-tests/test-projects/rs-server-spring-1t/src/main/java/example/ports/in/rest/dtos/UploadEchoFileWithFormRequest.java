package example.ports.in.rest.dtos;

import example.process.Context;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormRequest {

    private Context context;
    
    /**
     * id PATH parameter.<br/>
     * id of file
     */
    @Nonnull private Long id;
    
    /**
     * file FORMDATA parameter.<br/>
     * file to upload
     */
    @Nonnull private byte[] file;
}
