package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoMultiContentMultiContentBodyRequest;
import example.ports.in.rest.dtos.EchoMultiContentTextPlainRequest;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import org.springframework.http.ResponseEntity;

@Generated("pl.metaprogramming.codegen")
public interface IEchoMultiContentCommand {

    /**
     * [application/x-www-form-urlencoded, application/xml, application/json]
     */
    ResponseEntity<Object> execute(@Nonnull EchoMultiContentMultiContentBodyRequest request);

    /**
     * [text/plain]
     */
    ResponseEntity<Object> execute(@Nonnull EchoMultiContentTextPlainRequest request);
}
