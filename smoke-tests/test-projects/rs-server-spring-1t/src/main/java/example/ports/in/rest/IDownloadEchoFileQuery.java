package example.ports.in.rest;

import example.ports.in.rest.dtos.DownloadEchoFileRequest;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import org.springframework.http.ResponseEntity;

@Generated("pl.metaprogramming.codegen")
public interface IDownloadEchoFileQuery {

    /**
     * Returns a file
     */
    ResponseEntity<byte[]> execute(@Nonnull DownloadEchoFileRequest request);
}
