package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoEmptyRequest;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface IEchoEmptyQuery {

    void execute(@Nonnull EchoEmptyRequest request);
}
