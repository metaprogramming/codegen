package example.ports.in.rest.dtos;

import example.process.Context;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoErrorRequest {

    private Context context;
    
    /**
     * errorMessage QUERY parameter.<br/>
     * Example header param
     */
    @Nonnull private String errorMessage;
}
