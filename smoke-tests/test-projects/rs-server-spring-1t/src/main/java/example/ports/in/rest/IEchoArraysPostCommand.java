package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.EchoArraysPostRequest;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import org.springframework.http.ResponseEntity;

@Generated("pl.metaprogramming.codegen")
public interface IEchoArraysPostCommand {

    ResponseEntity<List<EchoArraysBodyDto>> execute(@Nonnull EchoArraysPostRequest request);
}
