package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoErrorRequest;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface IEchoErrorQuery {

    void execute(@Nonnull EchoErrorRequest request);
}
