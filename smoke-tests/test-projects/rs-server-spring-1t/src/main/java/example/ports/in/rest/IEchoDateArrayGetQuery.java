package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoDateArrayGetRequest;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface IEchoDateArrayGetQuery {

    List<LocalDate> execute(@Nonnull EchoDateArrayGetRequest request);
}
