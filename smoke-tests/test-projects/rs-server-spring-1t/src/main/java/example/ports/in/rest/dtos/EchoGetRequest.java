package example.ports.in.rest.dtos;

import example.process.Context;
import example.process.IRequest;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetRequest implements IRequest {

    private Context context;
    
    /**
     * Authorization HEADER parameter.
     */
    @Nullable private String authorization;
    
    /**
     * api_key QUERY parameter.
     */
    @Nullable private String apiKey;
    
    /**
     * X-Correlation-ID HEADER parameter.<br/>
     * Correlates HTTP requests between a client and server.
     */
    @Nonnull private String xCorrelationId;
    
    /**
     * prop_int_required QUERY parameter.
     */
    @Nonnull private Long propIntRequired;
    
    /**
     * prop_float QUERY parameter.
     */
    @Nullable private Float propFloat;
    
    /**
     * prop_enum QUERY parameter.
     */
    @Nullable private DefaultEnum propEnum;
}
