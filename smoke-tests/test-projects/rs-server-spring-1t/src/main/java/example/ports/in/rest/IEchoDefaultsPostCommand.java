package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoDefaultsBodyDto;
import example.ports.in.rest.dtos.EchoDefaultsPostRequest;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import org.springframework.http.ResponseEntity;

@Generated("pl.metaprogramming.codegen")
public interface IEchoDefaultsPostCommand {

    ResponseEntity<EchoDefaultsBodyDto> execute(@Nonnull EchoDefaultsPostRequest request);
}
