package example.ports.in.rest.dtos;

import example.process.Context;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentTextPlainRequest {

    private Context context;
    
    /**
     * body param
     */
    @Nonnull private String body;
}
