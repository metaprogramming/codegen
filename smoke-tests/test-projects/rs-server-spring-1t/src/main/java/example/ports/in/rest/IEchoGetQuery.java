package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoGetBodyDto;
import example.ports.in.rest.dtos.EchoGetRequest;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface IEchoGetQuery {

    EchoGetBodyDto execute(@Nonnull EchoGetRequest request);
}
