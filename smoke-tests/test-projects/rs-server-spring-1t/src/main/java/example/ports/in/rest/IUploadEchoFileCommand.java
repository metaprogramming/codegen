package example.ports.in.rest;

import example.ports.in.rest.dtos.UploadEchoFileRequest;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface IUploadEchoFileCommand {

    /**
     * upload file
     */
    void execute(@Nonnull UploadEchoFileRequest request);
}
