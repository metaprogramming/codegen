package example.ports.in.rest.dtos;

import example.process.Context;
import example.process.IRequest;
import java.time.ZonedDateTime;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoPostRequest implements IRequest {

    private Context context;
    
    /**
     * Authorization HEADER parameter.<br/>
     * The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
     */
    @Nullable private String authorization;
    
    /**
     * X-Correlation-ID HEADER parameter.<br/>
     * Correlates HTTP requests between a client and server.
     */
    @Nonnull private String xCorrelationId;
    
    /**
     * timestamp HEADER parameter.
     */
    @Nullable private ZonedDateTime timestamp;
    
    /**
     * Inline-Header-Param HEADER parameter.<br/>
     * Example header param
     */
    @Nullable private String inlineHeaderParam;
    
    /**
     * body param
     */
    @Nonnull private EchoBodyDto requestBody;
}
