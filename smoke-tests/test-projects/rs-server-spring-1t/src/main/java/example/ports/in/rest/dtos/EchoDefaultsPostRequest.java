package example.ports.in.rest.dtos;

import example.process.Context;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostRequest {

    private Context context;
    
    /**
     * Default-Header-Param HEADER parameter.<br/>
     * Example header param with default value
     */
    @Nullable private DefaultEnum defaultHeaderParam = DefaultEnum.A2;
    
    /**
     * body param
     */
    @Nullable private EchoDefaultsBodyDto body;
}
