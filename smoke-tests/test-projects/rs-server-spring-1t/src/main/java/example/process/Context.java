package example.process;

import lombok.Getter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Context {

    @Getter
    private UserData userData;

    public void initUser(UserData userData) {
        this.userData = userData;
    }
}
