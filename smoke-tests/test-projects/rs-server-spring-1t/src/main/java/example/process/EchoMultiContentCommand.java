package example.process;

import example.ports.in.rest.IEchoMultiContentCommand;
import example.ports.in.rest.dtos.EchoMultiContentMultiContentBodyRequest;
import example.ports.in.rest.dtos.EchoMultiContentTextPlainRequest;
import example.ports.in.rest.dtos.MultiContentBodyDto;
import example.ports.in.rest.dtos.ResponseContentType;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.time.format.DateTimeFormatter;

@Component
public class EchoMultiContentCommand implements IEchoMultiContentCommand {

    @Override
    public ResponseEntity<Object> execute(@Nonnull EchoMultiContentMultiContentBodyRequest request) {
        return ResponseEntity.ok()
                .contentType(MediaType.valueOf(request.getBody().getResponseContentType().getValue()))
                .body(request.getBody().getResponseContentType() == ResponseContentType.TEXT_PLAIN
                        ? toText(request.getBody()) : request.getBody());
    }

    @Override
    public ResponseEntity<Object> execute(@Nonnull EchoMultiContentTextPlainRequest request) {
        return ResponseEntity.ok(request.getBody());
    }

    String toText(MultiContentBodyDto dto) {
        return String.format("%s|%d|%s|%s",
                dto.getResponseContentType(),
                dto.getPropInt(),
                dto.getPropDate() != null ? dto.getPropDate().format(DateTimeFormatter.ISO_DATE) : "",
                dto.getPropList());
    }

}
