package example.process;

import example.ports.in.rest.IEchoPostCommand;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EchoPostRequest;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class EchoPostCommand implements IEchoPostCommand {

    private boolean state;

    @Override
    public ResponseEntity<EchoBodyDto> execute(@Nonnull EchoPostRequest request) {
        if (state) {
            throw new IllegalStateException();
        }
        state = true;
        return ResponseEntity.ok(request.getRequestBody());
    }

}
