package example.process;

import example.ports.in.rest.IEchoArraysPostCommand;
import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.EchoArraysPostRequest;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.List;

@Service
@Scope("prototype")
public class EchoArraysPostCommand implements IEchoArraysPostCommand {

    @Override
    public ResponseEntity<List<EchoArraysBodyDto>> execute(@Nonnull EchoArraysPostRequest request) {
        return request.getBody() == null || request.getBody().isEmpty()
                ? ResponseEntity.noContent().build()
                : ResponseEntity.ok(request.getBody());
    }

}
