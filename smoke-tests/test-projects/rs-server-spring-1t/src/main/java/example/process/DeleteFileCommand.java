package example.process;

import example.ports.in.rest.IDeleteFileCommand;
import example.ports.in.rest.dtos.DeleteFileRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;

@Service
@Scope("prototype")
@RequiredArgsConstructor
public class DeleteFileCommand implements IDeleteFileCommand {

    private final FileStore files;

    @Override
    public void execute(@Nonnull DeleteFileRequest request) {
        files.delete(request.getId());
    }

}
