package example.process;

import example.commons.validator.ValidationError;
import example.commons.validator.ValidationException;
import example.commons.validator.ValidationResult;
import example.ports.in.rest.IEchoErrorQuery;
import example.ports.in.rest.dtos.EchoErrorRequest;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
public class EchoErrorQuery implements IEchoErrorQuery {

    @Override
    public void execute(@Nonnull EchoErrorRequest request) {
        throw new ValidationException(new ValidationResult(request)
                .setStatus(500)
                .addError(ValidationError.builder()
                        .status(500)
                        .code("error")
                        .message(request.getErrorMessage()).build()));
    }

}
