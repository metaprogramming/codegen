/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.commons.validator;

public class Checkers {

    private Checkers() {}

    public static final Checker<String> AMOUNT_SCALE_CHECKER = context -> {
        int dotIdx = context.getValue().indexOf('.');
        if (dotIdx > 0) {
            String cents = context.getValue().substring(dotIdx + 1);
            if (cents.length() == 2 && cents.charAt(1) != '0') {
                CommonCheckers.writeError(context, String.format("%s must be divisible by 0.1", context.getPath()), context.getPath(), "must_be_divisible_by_0.1");
            }
        }
    };
}
