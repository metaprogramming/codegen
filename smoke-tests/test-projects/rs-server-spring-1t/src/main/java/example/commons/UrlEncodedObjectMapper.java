package example.commons;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UrlEncodedObjectMapper {

    private final ObjectMapper objectMapper;

    public <T> T map(MultiValueMap<String,String> body, Class<T> resultClass) {
        Map<String, Object> objMap = body.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().size() > 1 ? e.getValue() : e.getValue().get(0)));
        return objectMapper.convertValue(objMap, resultClass);
    }
}
