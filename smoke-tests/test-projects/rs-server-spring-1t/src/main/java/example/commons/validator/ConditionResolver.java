package example.commons.validator;

import example.ports.in.rest.dtos.ExtendedObjectDto;
import org.springframework.stereotype.Component;

@Component
public class ConditionResolver {


    public boolean resolve(Condition condition, ValidationContext<?> ctx) {
        if (condition == Condition.NOT_PART_OF_EXTENDED_OBJECT) {
            return !(ctx.getParent().getValue() instanceof ExtendedObjectDto);
        }
        return false;
    }

}
