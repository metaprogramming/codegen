package example.commons;

import java.time.LocalDate;
import java.util.function.Function;
import javax.annotation.processing.Generated;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

@Generated("pl.metaprogramming.codegen")
public class SerializationUtils {

    private SerializationUtils() {
    }

    public static <T> T fromString(String value, Function<String,T> transformer) {
        return value == null || value.isEmpty() ? null : transformer.apply(value);
    }

    public static <T> String toString(T value, Function<T,String> transformer) {
        return value == null ? null : transformer.apply(value);
    }

    public static LocalDate toLocalDate(String value) {
        return fromString(value, LocalDate::parse);
    }

    @SneakyThrows
    public static byte[] toBytes(Resource value) {
        return value != null ? IOUtils.toByteArray(value.getInputStream()) : null;
    }

    @SneakyThrows
    public static byte[] toBytes(MultipartFile file) {
        return file != null ? file.getBytes() : null;
    }
}
