package example.commons.validator;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class AmountChecker implements Checker<BigDecimal> {

    @Override
    public void check(ValidationContext<BigDecimal> ctx) {
        if (ctx.getValue().scale() != 2) {
            ctx.addError("is_not_amount", ctx.getPath() + " is not amount");
        }
    }
}
