package example.commons.validator;

import java.util.stream.Collectors;
import javax.annotation.processing.Generated;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Generated("pl.metaprogramming.codegen")
public class ValidationException extends RuntimeException {

    private final ValidationResult result;

    @Override
    public String getMessage() {
        String errors = result.getErrors().stream()
               .map(err -> err.getField() == null ? err.getCode() : String.format("%s - %s", err.getField(), err.getCode()))
               .collect(Collectors.joining(", "));
        return String.format("Failed validation (%d) with errors: %s", result.getStatus(), errors);
    }
}
