package example.commons.validator;

import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public abstract class Validator<V> implements Checker<V> {

    public ValidationResult validate(V input) {
        ValidationContext<V> context = new ValidationContext<>(input);
        check(context);
        if (!context.isValid()) {
            throw new ValidationException(context.getResult());
        }
        return context.getResult();
    }
}
