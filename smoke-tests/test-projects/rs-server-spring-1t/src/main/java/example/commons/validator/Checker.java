package example.commons.validator;

import example.adapters.in.rest.validators.custom.ErrorCode;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface Checker<V> {

    void check(ValidationContext<V> ctx);

    @SuppressWarnings("unchecked")
    default <T extends V> void checkWithParent(ValidationContext<T> ctx) {
        check((ValidationContext) ctx);
    }

    default boolean checkNull() {
        return false;
    }

    default Checker<V> withError(ErrorCode errorCode) {
        return ctx -> check(new ValidationContext<>(ctx, error -> ValidationError.builder()
                .code(errorCode.getValue())
                .message(error.getMessage())
                .field(error.getField())
                .status(error.getStatus())
                .stopValidation(error.isStopValidation())
                .build()));
    }
}
