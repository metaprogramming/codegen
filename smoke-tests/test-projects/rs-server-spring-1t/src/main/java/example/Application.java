package example;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import example.filters.RestLoggingFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public SimpleModule makeTrimToNull() {
        return new SimpleModule("trimToNull").addDeserializer(String.class, new StdScalarDeserializer<String>(String.class) {
            @Override
            public String deserialize(JsonParser jsonParser, DeserializationContext ctx) throws IOException {
                String value = jsonParser.getValueAsString().trim();
                return value.isEmpty() ? null : value;
            }
        });
    }

    // useful for debugging binary requests
//    @Bean
    public FilterRegistrationBean restLoggingFilter() {
        FilterRegistrationBean<RestLoggingFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new RestLoggingFilter());
        registration.addUrlPatterns("/api/*");
        registration.setOrder(HIGHEST_PRECEDENCE);
        return registration;
    }

}
