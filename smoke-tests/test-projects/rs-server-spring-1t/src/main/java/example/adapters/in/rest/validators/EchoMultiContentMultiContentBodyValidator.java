package example.adapters.in.rest.validators;

import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoMultiContentMultiContentBodyRequest;
import example.ports.in.rest.dtos.MultiContentBodyDto;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentMultiContentBodyValidator extends Validator<EchoMultiContentMultiContentBodyRequest> {

    public static final Field<EchoMultiContentMultiContentBodyRequest,MultiContentBodyDto> FIELD_BODY = new Field<>("body", EchoMultiContentMultiContentBodyRequest::getBody);

    private final MultiContentBodyValidator multiContentBodyValidator;

    public void check(ValidationContext<EchoMultiContentMultiContentBodyRequest> ctx) {
        ctx.setBean(OperationId.ECHO_MULTI_CONTENT);
        ctx.checkRoot(FIELD_BODY, required(), multiContentBodyValidator);
    }
}
