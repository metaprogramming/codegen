package example.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.ReusableEnum;
import org.springframework.stereotype.Component;

import static example.commons.validator.CommonCheckers.required;
import static example.commons.validator.CommonCheckers.writeError;

@Component
public class ExtendedObjectEnumChecker implements Checker<ReusableEnum> {

    public void check(ValidationContext<ReusableEnum> context) {
        context.check(required());
        if (context.isCurrentFieldValid()) {
            ValidationContext<ReusableEnum> mainFieldCtx = context.getParent(EchoBodyDto.class)
                    .orElseThrow(IllegalStateException::new)
                    .getChild(EchoBodyValidator.FIELD_PROP_ENUM_REUSABLE);
            if (context.getValue() != mainFieldCtx.getValue()) {
                writeError(context, context.getPath() + " should be equal to " + mainFieldCtx.getPath(), context.getPath(), "custom_code");
            }
        }
    }

    @Override
    public boolean checkNull() {
        return true;
    }
}
