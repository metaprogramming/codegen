package example.adapters.in.rest.validators;

import example.adapters.in.rest.validators.custom.DictionaryChecker;
import example.commons.validator.AmountChecker;
import example.commons.validator.Checker;
import example.commons.validator.Checkers;
import example.commons.validator.Field;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EnumType;
import example.ports.in.rest.dtos.ExtendedObjectDto;
import example.ports.in.rest.dtos.ReusableEnum;
import example.ports.in.rest.dtos.SimpleObjectDto;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.regex.Pattern;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.validators.custom.DictionaryCode.*;
import static example.adapters.in.rest.validators.custom.ErrorCode.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoBodyValidator implements Checker<EchoBodyDto> {

    public static final Field<EchoBodyDto,Integer> FIELD_PROP_INT = new Field<>("prop_int", EchoBodyDto::getPropInt);
    public static final Field<EchoBodyDto,Integer> FIELD_PROP_INT_SECOND = new Field<>("prop_int_second", EchoBodyDto::getPropIntSecond);
    public static final Field<EchoBodyDto,Long> FIELD_PROP_INT_REQUIRED = new Field<>("prop_int_required", EchoBodyDto::getPropIntRequired);
    public static final Field<EchoBodyDto,Float> FIELD_PROP_FLOAT = new Field<>("prop_float", EchoBodyDto::getPropFloat);
    public static final Field<EchoBodyDto,Double> FIELD_PROP_DOUBLE = new Field<>("prop_double", EchoBodyDto::getPropDouble);
    public static final Field<EchoBodyDto,String> FIELD_PROP_AMOUNT = new Field<>("prop_amount", EchoBodyDto::getPropAmount);
    public static final Field<EchoBodyDto,BigDecimal> FIELD_PROP_AMOUNT_NUMBER = new Field<>("prop_amount_number", EchoBodyDto::getPropAmountNumber);
    public static final Field<EchoBodyDto,String> FIELD_PROP_STRING = new Field<>("prop_string", EchoBodyDto::getPropString);
    public static final Field<EchoBodyDto,String> FIELD_PROP_STRING_PATTERN = new Field<>("prop_string_pattern", EchoBodyDto::getPropStringPattern);
    public static final Field<EchoBodyDto,String> FIELD_PROP_DEFAULT = new Field<>("prop_default", EchoBodyDto::getPropDefault);
    public static final Field<EchoBodyDto,LocalDate> FIELD_PROP_DATE = new Field<>("prop_date", EchoBodyDto::getPropDate);
    public static final Field<EchoBodyDto,LocalDate> FIELD_PROP_DATE_SECOND = new Field<>("prop_date_second", EchoBodyDto::getPropDateSecond);
    public static final Field<EchoBodyDto,ZonedDateTime> FIELD_PROP_DATE_TIME = new Field<>("prop_date_time", EchoBodyDto::getPropDateTime);
    public static final Field<EchoBodyDto,byte[]> FIELD_PROP_BASE64 = new Field<>("prop_base64", EchoBodyDto::getPropBase64);
    public static final Field<EchoBodyDto,Boolean> FIELD_PROP_BOOLEAN = new Field<>("prop_boolean", EchoBodyDto::getPropBoolean);
    public static final Field<EchoBodyDto,SimpleObjectDto> FIELD_PROP_OBJECT = new Field<>("prop_object", EchoBodyDto::getPropObject);
    public static final Field<EchoBodyDto,Map<String,Object>> FIELD_PROP_OBJECT_ANY = new Field<>("prop_object_any", EchoBodyDto::getPropObjectAny);
    public static final Field<EchoBodyDto,ExtendedObjectDto> FIELD_PROP_OBJECT_EXTENDED = new Field<>("prop_object_extended", EchoBodyDto::getPropObjectExtended);
    public static final Field<EchoBodyDto,ReusableEnum> FIELD_PROP_ENUM_REUSABLE = new Field<>("prop_enum_reusable", EchoBodyDto::getPropEnumReusable);
    public static final Field<EchoBodyDto,EnumType> FIELD_PROP_ENUM = new Field<>("prop_enum", EchoBodyDto::getPropEnum);

    private static final Checker<String> FIELD_PROP_AMOUNT_PATTERN = matches(Pattern.compile("^(0|([1-9][0-9]{0,}))\\.\\d{2}$"));
    private static final Checker<String> FIELD_PROP_STRING_PATTERN_PATTERN = matches(Pattern.compile("^[\\-\\\".',:;/\\\\!@#$%^&*()+_?|><=]{2,19}$"), "custom_error_code");

    private final UserDataValidationBean userDataValidationBean;
    private final AmountChecker amountChecker;
    private final DictionaryChecker dictionaryChecker;
    private final SimpleObjectValidator simpleObjectValidator;
    @Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT") private final Checker<SimpleObjectDto> simpleObjectCustomConstraint;
    private final ExtendedObjectValidator extendedObjectValidator;

    public void check(ValidationContext<EchoBodyDto> ctx) {
        ctx.check(FIELD_PROP_INT_REQUIRED, required(), ge(-1l));
        ctx.check(FIELD_PROP_FLOAT, le(101f));
        ctx.check(FIELD_PROP_DOUBLE, gt(0d), lt(10000d));
        ctx.check(FIELD_PROP_AMOUNT, FIELD_PROP_AMOUNT_PATTERN, userDataValidationBean::checkAmountByUser, Checkers.AMOUNT_SCALE_CHECKER);
        ctx.check(FIELD_PROP_AMOUNT_NUMBER, amountChecker, ge(new BigDecimal("0.02")), le(new BigDecimal("9999999999.99")));
        ctx.check(FIELD_PROP_STRING, minLength(5), maxLength(10), dictionaryChecker.check(COLORS));
        ctx.check(FIELD_PROP_STRING_PATTERN, FIELD_PROP_STRING_PATTERN_PATTERN);
        ctx.check(FIELD_PROP_DEFAULT, minLength(5));
        ctx.check(FIELD_PROP_OBJECT, simpleObjectValidator, simpleObjectCustomConstraint);
        ctx.check(FIELD_PROP_OBJECT_EXTENDED, extendedObjectValidator);
        ctx.check(lt(FIELD_PROP_INT, FIELD_PROP_INT_SECOND).withError(COMPARE_PROP_INT_FAILED));
        ctx.check(ge(FIELD_PROP_DATE, FIELD_PROP_DATE_SECOND));
    }
}
