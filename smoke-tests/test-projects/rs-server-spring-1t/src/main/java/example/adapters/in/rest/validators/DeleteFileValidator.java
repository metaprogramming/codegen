package example.adapters.in.rest.validators;

import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DeleteFileRequest;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class DeleteFileValidator extends Validator<DeleteFileRequest> {

    public static final Field<DeleteFileRequest,Long> FIELD_ID = new Field<>("id (PATH parameter)", DeleteFileRequest::getId);

    public void check(ValidationContext<DeleteFileRequest> ctx) {
        ctx.setBean(OperationId.DELETE_FILE);
        ctx.check(FIELD_ID, required());
    }
}
