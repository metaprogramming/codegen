package example.adapters.in.rest.validators;

import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DefaultEnum;
import example.ports.in.rest.dtos.EchoDefaultsBodyDto;
import example.ports.in.rest.dtos.EchoDefaultsPostRequest;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostValidator extends Validator<EchoDefaultsPostRequest> {

    public static final Field<EchoDefaultsPostRequest,DefaultEnum> FIELD_DEFAULT_HEADER_PARAM = new Field<>("Default-Header-Param (HEADER parameter)", EchoDefaultsPostRequest::getDefaultHeaderParam);
    public static final Field<EchoDefaultsPostRequest,EchoDefaultsBodyDto> FIELD_BODY = new Field<>("body", EchoDefaultsPostRequest::getBody);

    private final EchoDefaultsBodyValidator echoDefaultsBodyValidator;

    public void check(ValidationContext<EchoDefaultsPostRequest> ctx) {
        ctx.setBean(OperationId.ECHO_DEFAULTS_POST);
        ctx.checkRoot(FIELD_BODY, echoDefaultsBodyValidator);
    }
}
