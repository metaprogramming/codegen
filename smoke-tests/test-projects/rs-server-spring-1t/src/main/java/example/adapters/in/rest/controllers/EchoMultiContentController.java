package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.EchoMultiContentMultiContentBodyValidator;
import example.adapters.in.rest.validators.EchoMultiContentTextPlainValidator;
import example.commons.UrlEncodedObjectMapper;
import example.ports.in.rest.IEchoMultiContentCommand;
import example.ports.in.rest.dtos.EchoMultiContentMultiContentBodyRequest;
import example.ports.in.rest.dtos.EchoMultiContentTextPlainRequest;
import example.ports.in.rest.dtos.MultiContentBodyDto;
import example.process.Context;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentController {

    private final Context context;
    private final UrlEncodedObjectMapper urlEncodedObjectMapper;
    private final EchoMultiContentMultiContentBodyValidator echoMultiContentMultiContentBodyValidator;
    private final IEchoMultiContentCommand echoMultiContentCommand;
    private final EchoMultiContentTextPlainValidator echoMultiContentTextPlainValidator;

    @PostMapping(value = "/api/v1/echo-multi-content", produces = {"application/json", "application/xml", "text/plain"}, consumes = {"application/x-www-form-urlencoded"})
    public ResponseEntity<Object> echoMultiContent(@RequestParam MultiValueMap<String,String> body) {
        EchoMultiContentMultiContentBodyRequest request = new EchoMultiContentMultiContentBodyRequest()
                .setBody(urlEncodedObjectMapper.map(body, MultiContentBodyDto.class))
                .setContext(context);
        echoMultiContentMultiContentBodyValidator.validate(request);
        return echoMultiContentCommand.execute(request);
    }

    @PostMapping(value = "/api/v1/echo-multi-content", produces = {"application/json", "application/xml", "text/plain"}, consumes = {"application/xml", "application/json"})
    public ResponseEntity<Object> echoMultiContent(@RequestBody MultiContentBodyDto body) {
        EchoMultiContentMultiContentBodyRequest request = new EchoMultiContentMultiContentBodyRequest()
                .setBody(body)
                .setContext(context);
        echoMultiContentMultiContentBodyValidator.validate(request);
        return echoMultiContentCommand.execute(request);
    }

    @PostMapping(value = "/api/v1/echo-multi-content", produces = {"application/json", "application/xml", "text/plain"}, consumes = {"text/plain"})
    public ResponseEntity<Object> echoMultiContent(@RequestBody String body) {
        EchoMultiContentTextPlainRequest request = new EchoMultiContentTextPlainRequest()
                .setBody(body)
                .setContext(context);
        echoMultiContentTextPlainValidator.validate(request);
        return echoMultiContentCommand.execute(request);
    }
}
