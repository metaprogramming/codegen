package example.adapters.in.rest.validators.custom;

import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public enum DictionaryCode {

    ANIMALS,
    COLORS;
}
