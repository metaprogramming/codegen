package example.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.Condition;
import example.commons.validator.ConditionResolver;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.SimpleObjectDto;
import java.time.LocalDate;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectValidator implements Checker<SimpleObjectDto> {

    public static final Field<SimpleObjectDto,String> FIELD_SO_PROP_STRING = new Field<>("so_prop_string", SimpleObjectDto::getSoPropString);
    public static final Field<SimpleObjectDto,LocalDate> FIELD_SO_PROP_DATE = new Field<>("so_prop_date", SimpleObjectDto::getSoPropDate);

    private final ConditionResolver conditionResolver;

    public void check(ValidationContext<SimpleObjectDto> ctx) {
        ctx.setBean(conditionResolver);
        ctx.check(FIELD_SO_PROP_STRING, required());
        ctx.check(FIELD_SO_PROP_DATE, checkIf(required(), or(OperationId.ECHO_ARRAYS_POST, and(or(OperationId.FAKE_OPERATION, OperationId.ECHO_POST), Condition.NOT_PART_OF_EXTENDED_OBJECT))));
    }
}
