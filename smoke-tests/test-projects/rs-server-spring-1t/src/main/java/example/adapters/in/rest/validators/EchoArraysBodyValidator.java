package example.adapters.in.rest.validators;

import example.adapters.in.rest.validators.custom.DictionaryChecker;
import example.commons.validator.Checker;
import example.commons.validator.Field;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.EnumType;
import example.ports.in.rest.dtos.ReusableEnum;
import example.ports.in.rest.dtos.SimpleObjectDto;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.validators.custom.DictionaryCode.*;
import static example.adapters.in.rest.validators.custom.ErrorCode.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyValidator implements Checker<EchoArraysBodyDto> {

    public static final Field<EchoArraysBodyDto,List<Integer>> FIELD_PROP_INT_LIST = new Field<>("prop_int_list", EchoArraysBodyDto::getPropIntList);
    public static final Field<EchoArraysBodyDto,List<Float>> FIELD_PROP_FLOAT_LIST = new Field<>("prop_float_list", EchoArraysBodyDto::getPropFloatList);
    public static final Field<EchoArraysBodyDto,List<Double>> FIELD_PROP_DOUBLE_LIST = new Field<>("prop_double_list", EchoArraysBodyDto::getPropDoubleList);
    public static final Field<EchoArraysBodyDto,List<String>> FIELD_PROP_AMOUNT_LIST = new Field<>("prop_amount_list", EchoArraysBodyDto::getPropAmountList);
    public static final Field<EchoArraysBodyDto,List<String>> FIELD_PROP_STRING_LIST = new Field<>("prop_string_list", EchoArraysBodyDto::getPropStringList);
    public static final Field<EchoArraysBodyDto,List<LocalDate>> FIELD_PROP_DATE_LIST = new Field<>("prop_date_list", EchoArraysBodyDto::getPropDateList);
    public static final Field<EchoArraysBodyDto,List<List<ZonedDateTime>>> FIELD_PROP_DATE_TIME_LIST_OF_LIST = new Field<>("prop_date_time_list_of_list", EchoArraysBodyDto::getPropDateTimeListOfList);
    public static final Field<EchoArraysBodyDto,List<Boolean>> FIELD_PROP_BOOLEAN_LIST = new Field<>("prop_boolean_list", EchoArraysBodyDto::getPropBooleanList);
    public static final Field<EchoArraysBodyDto,List<SimpleObjectDto>> FIELD_PROP_OBJECT_LIST = new Field<>("prop_object_list", EchoArraysBodyDto::getPropObjectList);
    public static final Field<EchoArraysBodyDto,List<List<SimpleObjectDto>>> FIELD_PROP_OBJECT_LIST_OF_LIST = new Field<>("prop_object_list_of_list", EchoArraysBodyDto::getPropObjectListOfList);
    public static final Field<EchoArraysBodyDto,List<ReusableEnum>> FIELD_PROP_ENUM_REUSABLE_LIST = new Field<>("prop_enum_reusable_list", EchoArraysBodyDto::getPropEnumReusableList);
    public static final Field<EchoArraysBodyDto,List<EnumType>> FIELD_PROP_ENUM_LIST = new Field<>("prop_enum_list", EchoArraysBodyDto::getPropEnumList);
    public static final Field<EchoArraysBodyDto,Map<String,Integer>> FIELD_PROP_MAP_OF_INT = new Field<>("prop_map_of_int", EchoArraysBodyDto::getPropMapOfInt);
    public static final Field<EchoArraysBodyDto,Map<String,SimpleObjectDto>> FIELD_PROP_MAP_OF_OBJECT = new Field<>("prop_map_of_object", EchoArraysBodyDto::getPropMapOfObject);
    public static final Field<EchoArraysBodyDto,Map<String,List<SimpleObjectDto>>> FIELD_PROP_MAP_OF_LIST_OF_OBJECT = new Field<>("prop_map_of_list_of_object", EchoArraysBodyDto::getPropMapOfListOfObject);

    private static final Checker<String> FIELD_PROP_AMOUNT_LIST_PATTERN = matches(Pattern.compile("^(0|([1-9][0-9]{0,}))\\.\\d{2}$"), "invalid_amount");

    private final DictionaryChecker dictionaryChecker;
    private final SimpleObjectValidator simpleObjectValidator;
    @Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT") private final Checker<SimpleObjectDto> simpleObjectCustomConstraint;

    public void check(ValidationContext<EchoArraysBodyDto> ctx) {
        ctx.check(FIELD_PROP_INT_LIST, minItems(1), items(gt(-2)));
        ctx.check(FIELD_PROP_FLOAT_LIST, items(le(101f)));
        ctx.check(FIELD_PROP_DOUBLE_LIST, required(), unique(), minItems(2), maxItems(4), items(ge(0.01), le(1000000000000000d)));
        ctx.check(FIELD_PROP_AMOUNT_LIST, items(FIELD_PROP_AMOUNT_LIST_PATTERN));
        ctx.check(FIELD_PROP_STRING_LIST, items(dictionaryChecker.check(ANIMALS).withError(INVALID_ANIMAL), minLength(5), maxLength(10)));
        ctx.check(FIELD_PROP_DATE_TIME_LIST_OF_LIST, minItems(1), items(minItems(2)));
        ctx.check(FIELD_PROP_OBJECT_LIST, items(simpleObjectValidator, simpleObjectCustomConstraint.withError(CUSTOM_FAILED_CODE)));
        ctx.check(FIELD_PROP_OBJECT_LIST_OF_LIST, items(items(simpleObjectValidator, simpleObjectCustomConstraint)));
        ctx.check(FIELD_PROP_MAP_OF_INT, mapValues(le(100)));
        ctx.check(FIELD_PROP_MAP_OF_OBJECT, mapValues(simpleObjectValidator));
        ctx.check(FIELD_PROP_MAP_OF_LIST_OF_OBJECT, mapValues(items(simpleObjectValidator)));
    }
}
