package example.adapters.in.rest.validators;

import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DownloadEchoFileRequest;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileValidator extends Validator<DownloadEchoFileRequest> {

    public static final Field<DownloadEchoFileRequest,Long> FIELD_ID = new Field<>("id (PATH parameter)", DownloadEchoFileRequest::getId);

    public void check(ValidationContext<DownloadEchoFileRequest> ctx) {
        ctx.setBean(OperationId.DOWNLOAD_ECHO_FILE);
        ctx.check(FIELD_ID, required());
    }
}
