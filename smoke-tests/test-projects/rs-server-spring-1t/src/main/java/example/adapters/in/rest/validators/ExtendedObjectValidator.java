package example.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.Field;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.ExtendedObjectDto;
import example.ports.in.rest.dtos.ReusableEnum;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectValidator implements Checker<ExtendedObjectDto> {

    public static final Field<ExtendedObjectDto,ReusableEnum> FIELD_EO_ENUM_REUSABLE = new Field<>("eo_enum_reusable", ExtendedObjectDto::getEoEnumReusable);
    public static final Field<ExtendedObjectDto,ExtendedObjectDto> FIELD_SELF_PROPERTY = new Field<>("self_property", ExtendedObjectDto::getSelfProperty);

    private final ExtendedObjectChecker extendedObjectChecker;
    private final SimpleObjectValidator simpleObjectValidator;
    private final ExtendedObjectEnumChecker extendedObjectEnumChecker;

    public void check(ValidationContext<ExtendedObjectDto> ctx) {
        ctx.check(extendedObjectChecker);
        simpleObjectValidator.checkWithParent(ctx);
        ctx.check(FIELD_EO_ENUM_REUSABLE, extendedObjectEnumChecker);
        ctx.check(FIELD_SELF_PROPERTY, this);
    }
}
