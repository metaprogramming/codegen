package example.adapters.in.rest.validators;

import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoDateArrayGetRequest;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetValidator extends Validator<EchoDateArrayGetRequest> {

    public static final Field<EchoDateArrayGetRequest,List<LocalDate>> FIELD_DATE_ARRAY = new Field<>("date_array (QUERY parameter)", EchoDateArrayGetRequest::getDateArray);

    public void check(ValidationContext<EchoDateArrayGetRequest> ctx) {
        ctx.setBean(OperationId.ECHO_DATE_ARRAY_GET);
    }
}
