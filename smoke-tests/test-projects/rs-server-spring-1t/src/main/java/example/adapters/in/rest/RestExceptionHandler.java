package example.adapters.in.rest;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import example.commons.EnumValue;
import example.commons.validator.ValidationError;
import example.commons.validator.ValidationException;
import example.commons.validator.ValidationResult;
import example.ports.in.rest.dtos.EchoGetRequest;
import example.ports.in.rest.dtos.ErrorDescriptionDto;
import example.ports.in.rest.dtos.ErrorDetailDto;
import example.ports.in.rest.dtos.ErrorItemDto;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;
import static example.commons.validator.CommonCheckers.*;

@Slf4j
@ControllerAdvice
@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class RestExceptionHandler {

    private static final String REQ_BODY = "body";
    private static final Map<Class<?>,String> INVALID_CODE_BY_CLASS = makeInvalidCodeByClass();
    private static final Map<String,String> MESSAGE_BY_CODE = makeMessageByCode();

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(Exception e, WebRequest request) {
        if (e instanceof ValidationException) {
            return toResponseEntity(((ValidationException) e).getResult(), request);
        }
        if (e instanceof HttpMessageNotReadableException) {
           return toResponseEntity(toValidationResult((HttpMessageNotReadableException) e), request);
        }
        if (e instanceof MethodArgumentTypeMismatchException) {
           return toResponseEntity(new MatmExceptionConverter((MethodArgumentTypeMismatchException) e).convert(), request);
        }
        if (e instanceof ResponseStatusException) {
           return toResponseEntity((ResponseStatusException) e, request);
        }
        if (e instanceof MissingRequestHeaderException) {
           return toResponseEntity(toValidationResult((MissingRequestHeaderException) e), request);
        }
        log.error("Exception", e);
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<Object> toResponseEntity(ValidationResult result, WebRequest request) {
        if (result.getStatus() == 401) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorDescriptionDto().setCode(result.getStatus()).setMessage("Unauthenticated"));
        }
        if (result.getStatus() == 403) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ErrorDescriptionDto().setCode(result.getStatus()).setMessage("Unauthorized"));
        }
        if (result.getRequest() instanceof EchoGetRequest) {
            List<ErrorItemDto> items = result.getErrors().stream()
                    .map(d -> new ErrorItemDto().setCode(d.getCode()))
                    .collect(Collectors.toList());
            return new ResponseEntity<>(items, HttpStatus.valueOf(result.getStatus()));
        }
        return new ResponseEntity<>(new ErrorDescriptionDto()
                .setCode(result.getStatus())
                .setMessage(result.getMessage())
                .setErrors(result.getErrors().stream()
                        .map(d -> new ErrorDetailDto().setCode(d.getCode()).setField(d.getField()).setMessage(d.getMessage()))
                        .collect(Collectors.toList())),
                HttpStatus.valueOf(result.getStatus()));
    }

    private ValidationResult toValidationResult(String field, String code) {
        return new ValidationResult(null).addError(ValidationError.builder()
                .field(field)
                .code(code)
                .message(field + ' ' + MESSAGE_BY_CODE.get(code))
                .build());
    }

    private ValidationResult toValidationResult(String field, List<String> allowedValues) {
        return new ValidationResult(null).addError(ValidationError.builder()
                .field(field)
                .code(ERR_CODE_IS_NOT_ALLOWED_VALUE)
                .message(String.format("%s should have one of values: %s", field, allowedValues))
                .messageArgs(new Object[]{allowedValues.toString()})
                .build());
    }

    private List<String> getEnumValues(Class<EnumValue> enumClass) {
        return Arrays.stream(enumClass.getEnumConstants()).map(EnumValue::getValue).collect(Collectors.toList());
    }

    private ValidationResult makeUnknownValidationResult() {
        return new ValidationResult(null).addError(ValidationError.builder()
                .code("invalid_input")
                .message("invalid_input")
                .build());
    }

    private ResponseEntity<Object> toResponseEntity(ResponseStatusException e, WebRequest request) {
        return new ResponseEntity<>(new ErrorDescriptionDto().setCode(e.getStatus().value()).setMessage(e.getMessage()), e.getStatus());
    }

    private ValidationResult toValidationResult(MissingRequestHeaderException e) {
        return toValidationResult(e.getHeaderName() + " (HEADER parameter)", ERR_CODE_IS_REQUIRED);
    }

    @SuppressWarnings("unchecked")
    private ValidationResult toValidationResult(HttpMessageNotReadableException e) {
        if (e.getMessage() != null && e.getMessage().startsWith("Required request body is missing")) {
            return toValidationResult(REQ_BODY, ERR_CODE_IS_REQUIRED);
        }
        if (e.getCause() instanceof MismatchedInputException) {
            MismatchedInputException cex = (MismatchedInputException) e.getCause();
            String field = makeFieldPath(cex.getPath());
            String codeByClass = INVALID_CODE_BY_CLASS.get(cex.getTargetType());
            if (codeByClass != null) {
                return toValidationResult(field, codeByClass);
            }
            if (cex.getTargetType().isEnum()) {
                return toValidationResult(field, getEnumValues((Class<EnumValue>) cex.getTargetType()));
            }
        }
        if (e.getCause() instanceof JsonMappingException
                && e.getCause().getCause() instanceof NullPointerException) {
            JsonMappingException jme = (JsonMappingException) e.getCause();
            return toValidationResult(makeFieldPath(jme.getPath()), ERR_CODE_IS_REQUIRED);
        }
        log.info("Invalid request", e);
        return makeUnknownValidationResult();
    }

    private String makeFieldPath(List<JsonMappingException.Reference> path) {
        StringBuilder buf = new StringBuilder();
        path.forEach(r -> {
            if (r.getFrom() instanceof Map) {
                buf.append(String.format("[%s]", r.getFieldName()));
            } else if (r.getFieldName() != null) {
                if (buf.length() > 0) buf.append('.');
                buf.append(r.getFieldName());
            } else if (r.getFrom() instanceof List) {
                if (buf.length() == 0) buf.append(REQ_BODY);
                buf.append(String.format("[%d]", r.getIndex()));
            } else {
                buf.append(".NULL");
            }
        });
        String result = buf.toString();
        if (result.startsWith(REQ_BODY + '.')) return result.substring(REQ_BODY.length() + 1);
        if (result.startsWith(REQ_BODY + '[')) return result.substring(REQ_BODY.length());
        return result;
    }

    private static Map<Class<?>,String> makeInvalidCodeByClass() {
        Map<Class<?>, String> result = new HashMap<>();
        result.put(BigDecimal.class, ERR_CODE_IS_NOT_NUMBER);
        result.put(byte[].class, ERR_CODE_IS_NOT_BASE64);
        result.put(Boolean.class, ERR_CODE_IS_NOT_BOOLEAN);
        result.put(boolean.class, ERR_CODE_IS_NOT_BOOLEAN);
        result.put(Double.class, ERR_CODE_IS_NOT_DOUBLE);
        result.put(double.class, ERR_CODE_IS_NOT_DOUBLE);
        result.put(Float.class, ERR_CODE_IS_NOT_FLOAT);
        result.put(float.class, ERR_CODE_IS_NOT_FLOAT);
        result.put(Integer.class, ERR_CODE_IS_NOT_INT);
        result.put(int.class, ERR_CODE_IS_NOT_INT);
        result.put(Long.class, ERR_CODE_IS_NOT_LONG);
        result.put(long.class, ERR_CODE_IS_NOT_LONG);
        result.put(LocalDate.class, ERR_CODE_IS_NOT_DATE);
        result.put(LocalDateTime.class, ERR_CODE_IS_NOT_DATE_TIME);
        result.put(ZonedDateTime.class, ERR_CODE_IS_NOT_DATE_TIME);
        return result;
    }

    public static Map<String,String> makeMessageByCode() {
        Map<String, String> result = new HashMap<>();
        result.put(ERR_CODE_IS_REQUIRED, "is required");
        result.put(ERR_CODE_IS_NOT_BASE64, "is not base64");
        result.put(ERR_CODE_IS_NOT_BOOLEAN, "should have one of values: [true, false]");
        result.put(ERR_CODE_IS_NOT_DOUBLE, "is not double");
        result.put(ERR_CODE_IS_NOT_FLOAT, "is not float");
        result.put(ERR_CODE_IS_NOT_INT, "is not 32bit integer");
        result.put(ERR_CODE_IS_NOT_LONG, "is not 64bit integer");
        result.put(ERR_CODE_IS_NOT_NUMBER, "is not number");
        result.put(ERR_CODE_IS_NOT_DATE, "is not yyyy-MM-dd");
        result.put(ERR_CODE_IS_NOT_DATE_TIME, "should be valid date time in ISO8601 format");
        return result;
    }

    @RequiredArgsConstructor
    private class MatmExceptionConverter {
    
        private final MethodArgumentTypeMismatchException e;
    
        public ValidationResult convert() {
            String code = INVALID_CODE_BY_CLASS.get(getDataType());
            return code != null ? toValidationResult(getField(), code) : makeUnknownValidationResult();
        }
    
        private String getField() {
            return e.getName() + getParamLocation() + getListIndex();
        }
    
        private String getParamLocation() {
            if (isAnnotated(RequestHeader.class)) return " (HEADER parameter)";
            if (isAnnotated(RequestParam.class)) return " (QUERY parameter)";
            return "";
        }
    
        private String getListIndex() {
            if (isListParam()) {
                if (e.getCause() instanceof ConversionFailedException && e.getValue() instanceof String[]) {
                    ConversionFailedException cfe = (ConversionFailedException) e.getCause();
                    int index = Arrays.asList((Object[]) e.getValue()).indexOf(cfe.getValue());
                    return String.format("[%d]", index);
                }
                return "[?]";
            }
            return "";
        }
    
        private Class<?> getDataType() {
            if (isListParam()) {
                Type parameterizedType = e.getParameter().getParameter().getParameterizedType();
                if (parameterizedType instanceof ParameterizedType) {
                    Type[] params = ((ParameterizedType) parameterizedType).getActualTypeArguments();
                    if (params.length > 0 && params[0] instanceof Class) {
                        return (Class<?>) params[0];
                    }
                }
            }
            return e.getParameter().getParameterType();
        }
    
        private boolean isListParam() {
            return e.getParameter().getParameterType() == List.class;
        }
    
        private boolean isAnnotated(Class<? extends Annotation> annotation) {
            return e.getParameter().getParameterAnnotation(annotation) != null;
        }
    }
    
}
