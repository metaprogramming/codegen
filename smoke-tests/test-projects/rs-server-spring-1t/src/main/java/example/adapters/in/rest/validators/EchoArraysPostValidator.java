package example.adapters.in.rest.validators;

import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoArraysBodyDto;
import example.ports.in.rest.dtos.EchoArraysPostRequest;
import java.util.List;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostValidator extends Validator<EchoArraysPostRequest> {

    public static final Field<EchoArraysPostRequest,String> FIELD_AUTHORIZATION = new Field<>("Authorization (HEADER parameter)", EchoArraysPostRequest::getAuthorization);
    public static final Field<EchoArraysPostRequest,String> FIELD_INLINE_HEADER_PARAM = new Field<>("Inline-Header-Param (HEADER parameter)", EchoArraysPostRequest::getInlineHeaderParam);
    public static final Field<EchoArraysPostRequest,List<EchoArraysBodyDto>> FIELD_BODY = new Field<>("body", EchoArraysPostRequest::getBody);

    private final EchoArraysBodyValidator echoArraysBodyValidator;

    public void check(ValidationContext<EchoArraysPostRequest> ctx) {
        ctx.setBean(OperationId.ECHO_ARRAYS_POST);
        ctx.checkRoot(FIELD_BODY, required(), items(echoArraysBodyValidator));
    }
}
