package example.adapters.in.rest.validators;

import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.UploadEchoFileWithFormRequest;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormValidator extends Validator<UploadEchoFileWithFormRequest> {

    public static final Field<UploadEchoFileWithFormRequest,Long> FIELD_ID = new Field<>("id (PATH parameter)", UploadEchoFileWithFormRequest::getId);
    public static final Field<UploadEchoFileWithFormRequest,byte[]> FIELD_FILE = new Field<>("file (FORMDATA parameter)", UploadEchoFileWithFormRequest::getFile);

    public void check(ValidationContext<UploadEchoFileWithFormRequest> ctx) {
        ctx.setBean(OperationId.UPLOAD_ECHO_FILE_WITH_FORM);
        ctx.check(FIELD_ID, required());
        ctx.check(FIELD_FILE, required());
    }
}
