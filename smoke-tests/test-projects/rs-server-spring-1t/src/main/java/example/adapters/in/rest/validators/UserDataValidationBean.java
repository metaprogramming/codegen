package example.adapters.in.rest.validators;

import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EchoPostRequest;
import example.process.UserData;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static example.commons.validator.CommonCheckers.*;

@Component
public class UserDataValidationBean {

    public void checkAmountByUser(ValidationContext<String> context) {
        UserData userData = context.getRequest(EchoPostRequest.class).getContext().getUserData();
        BigDecimal amount = new BigDecimal(context.getValue());
        if (userData.getMaxAmount().compareTo(amount) < 0) {
            writeError(context, context.getPath() + " should be <= " + userData.getMaxAmount(), context.getPath(), ERR_CODE_IS_TOO_BIG);
        } else if (userData.getMinAmount().compareTo(amount) > 0) {
            writeError(context, context.getPath() + " should be >= " + userData.getMinAmount(), context.getPath(), ERR_CODE_IS_TOO_SMALL);
        }
    }

}
