package example.adapters.in.rest.validators;

import example.adapters.in.rest.validators.custom.DictionaryChecker;
import example.commons.validator.AmountChecker;
import example.commons.validator.Checker;
import example.commons.validator.Field;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EchoDefaultsBodyDto;
import example.ports.in.rest.dtos.ReusableEnum;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.adapters.in.rest.validators.custom.DictionaryCode.*;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyValidator implements Checker<EchoDefaultsBodyDto> {

    public static final Field<EchoDefaultsBodyDto,Integer> FIELD_PROP_INT = new Field<>("propInt", EchoDefaultsBodyDto::getPropInt);
    public static final Field<EchoDefaultsBodyDto,Float> FIELD_PROP_FLOAT = new Field<>("propFloat", EchoDefaultsBodyDto::getPropFloat);
    public static final Field<EchoDefaultsBodyDto,Double> FIELD_PROP_DOUBLE = new Field<>("propDouble", EchoDefaultsBodyDto::getPropDouble);
    public static final Field<EchoDefaultsBodyDto,BigDecimal> FIELD_PROP_NUMBER = new Field<>("propNumber", EchoDefaultsBodyDto::getPropNumber);
    public static final Field<EchoDefaultsBodyDto,String> FIELD_PROP_STRING = new Field<>("propString", EchoDefaultsBodyDto::getPropString);
    public static final Field<EchoDefaultsBodyDto,ReusableEnum> FIELD_PROP_ENUM = new Field<>("propEnum", EchoDefaultsBodyDto::getPropEnum);
    public static final Field<EchoDefaultsBodyDto,LocalDate> FIELD_PROP_DATE = new Field<>("propDate", EchoDefaultsBodyDto::getPropDate);
    public static final Field<EchoDefaultsBodyDto,ZonedDateTime> FIELD_PROP_DATE_TIME = new Field<>("propDateTime", EchoDefaultsBodyDto::getPropDateTime);

    private final AmountChecker amountChecker;
    private final DictionaryChecker dictionaryChecker;

    public void check(ValidationContext<EchoDefaultsBodyDto> ctx) {
        ctx.check(FIELD_PROP_INT, ge(1));
        ctx.check(FIELD_PROP_FLOAT, ge(1f));
        ctx.check(FIELD_PROP_DOUBLE, ge(1d));
        ctx.check(FIELD_PROP_NUMBER, amountChecker, ge(new BigDecimal("1")));
        ctx.check(FIELD_PROP_STRING, minLength(5), dictionaryChecker.check(COLORS));
    }
}
