package example.adapters.in.rest.validators;

import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.UploadEchoFileRequest;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileValidator extends Validator<UploadEchoFileRequest> {

    public static final Field<UploadEchoFileRequest,Long> FIELD_ID = new Field<>("id (PATH parameter)", UploadEchoFileRequest::getId);
    public static final Field<UploadEchoFileRequest,byte[]> FIELD_BODY = new Field<>("body", UploadEchoFileRequest::getBody);

    public void check(ValidationContext<UploadEchoFileRequest> ctx) {
        ctx.setBean(OperationId.UPLOAD_ECHO_FILE);
        ctx.check(FIELD_ID, required());
        ctx.checkRoot(FIELD_BODY, required());
    }
}
