package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.EchoErrorValidator;
import example.ports.in.rest.IEchoErrorQuery;
import example.ports.in.rest.dtos.EchoErrorRequest;
import example.process.Context;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoErrorController {

    private final Context context;
    private final EchoErrorValidator echoErrorValidator;
    private final IEchoErrorQuery echoErrorQuery;

    @GetMapping(value = "/api/v1/echo-error", produces = {"application/json"})
    public ResponseEntity<Void> echoError(@RequestParam String errorMessage) {
        EchoErrorRequest request = new EchoErrorRequest()
                .setErrorMessage(errorMessage)
                .setContext(context);
        echoErrorValidator.validate(request);
        echoErrorQuery.execute(request);
        return ResponseEntity.noContent().build();
    }
}
