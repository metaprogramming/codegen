package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.EchoDateArrayGetValidator;
import example.ports.in.rest.IEchoDateArrayGetQuery;
import example.ports.in.rest.dtos.EchoDateArrayGetRequest;
import example.process.Context;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetController {

    private final Context context;
    private final EchoDateArrayGetValidator echoDateArrayGetValidator;
    private final IEchoDateArrayGetQuery echoDateArrayGetQuery;

    @GetMapping(value = "/api/v1/echo-date-array", produces = {"application/json"})
    public ResponseEntity<List<LocalDate>> echoDateArrayGet(@RequestParam(value = "date_array", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) List<LocalDate> dateArray) {
        EchoDateArrayGetRequest request = new EchoDateArrayGetRequest()
                .setDateArray(dateArray)
                .setContext(context);
        echoDateArrayGetValidator.validate(request);
        return ResponseEntity.ok(echoDateArrayGetQuery.execute(request));
    }
}
