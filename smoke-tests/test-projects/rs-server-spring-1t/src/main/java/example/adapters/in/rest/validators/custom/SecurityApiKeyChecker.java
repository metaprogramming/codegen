package example.adapters.in.rest.validators.custom;

import example.commons.validator.Checker;
import example.commons.validator.Privilege;
import example.commons.validator.ValidationContext;
import example.process.IRequest;
import example.process.UserData;
import example.process.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static example.commons.validator.CommonCheckers.credentials;

@Component
@RequiredArgsConstructor
public class SecurityApiKeyChecker {

    private final UserService userService;


    public Checker<String> check(Privilege... privileges) {
        return credentials(this::getOwnedCredentials, Arrays.asList(privileges));
    }

    public List<Privilege> getOwnedCredentials(ValidationContext<String> ctx) {
        UserData userData = userService.loadUserData(ctx.getValue());
        ctx.getRequest(IRequest.class).getContext().initUser(userData);
        return userData.getCredentials();
    }

}
