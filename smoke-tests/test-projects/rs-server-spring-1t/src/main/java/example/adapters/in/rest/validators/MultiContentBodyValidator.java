package example.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.Field;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.MultiContentBodyDto;
import example.ports.in.rest.dtos.ResponseContentType;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class MultiContentBodyValidator implements Checker<MultiContentBodyDto> {

    public static final Field<MultiContentBodyDto,ResponseContentType> FIELD_RESPONSE_CONTENT_TYPE = new Field<>("response_content_type", MultiContentBodyDto::getResponseContentType);
    public static final Field<MultiContentBodyDto,Integer> FIELD_PROP_INT = new Field<>("prop_int", MultiContentBodyDto::getPropInt);
    public static final Field<MultiContentBodyDto,Integer> FIELD_PROP_INT2 = new Field<>("prop_int2", MultiContentBodyDto::getPropInt2);
    public static final Field<MultiContentBodyDto,LocalDate> FIELD_PROP_DATE = new Field<>("prop_date", MultiContentBodyDto::getPropDate);
    public static final Field<MultiContentBodyDto,List<String>> FIELD_PROP_LIST = new Field<>("prop_list", MultiContentBodyDto::getPropList);

    public void check(ValidationContext<MultiContentBodyDto> ctx) {
        ctx.check(FIELD_RESPONSE_CONTENT_TYPE, required());
        ctx.check(le(FIELD_PROP_INT, FIELD_PROP_INT2));
    }
}
