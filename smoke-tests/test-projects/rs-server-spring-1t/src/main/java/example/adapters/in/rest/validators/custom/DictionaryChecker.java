package example.adapters.in.rest.validators.custom;

import example.commons.validator.Checker;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;

@Component
public class DictionaryChecker {

    private final Collection<String> colors = Arrays.asList("BLACK", "RED", "WHITE");
    private final Collection<String> animals = Arrays.asList("MOUSE", "SNAKE", "ORCA", "CUTTLEFISH", "CATERPILLAR");

    public Checker<String> check(DictionaryCode code) {
        return check(code, "invalid_" + code.name());
    }

    private Checker<String> check(DictionaryCode code, String errorCode) {
        return ctx -> {
            if (!availableValues(code).contains(ctx.getValue())) {
                ctx.addError(errorCode, errorCode);
            }
        };
    }

    private Collection<String> availableValues(DictionaryCode dictionary) {
        return  DictionaryCode.COLORS == dictionary ? colors : animals;
    }

}
