package example.adapters.in.rest.validators.custom;

import example.commons.validator.Checker;
import example.commons.validator.Privilege;
import example.commons.validator.ValidationContext;
import example.process.IRequest;
import example.process.UserData;
import example.process.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

import static example.commons.validator.CommonCheckers.credentials;

@Component
@RequiredArgsConstructor
public class SecurityAuthorizationChecker {

    private final UserService userService;

    public Checker<String> check(Privilege... scopes) {
        return credentials(this::getOwnedCredentials, Arrays.asList(scopes));
    }

    public List<Privilege> getOwnedCredentials(ValidationContext<String> ctx) {
        if (!ctx.getValue().startsWith("Bearer ")) {
            throw new IllegalArgumentException("Authorization param should be bearer");
        }
        UserData userData = userService.loadUserData(ctx.getValue().substring(7));
        ctx.getRequest(IRequest.class).getContext().initUser(userData);
        return userData.getCredentials();
    }

}
