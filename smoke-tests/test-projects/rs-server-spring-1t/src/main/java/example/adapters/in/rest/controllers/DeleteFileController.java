package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.DeleteFileValidator;
import example.ports.in.rest.IDeleteFileCommand;
import example.ports.in.rest.dtos.DeleteFileRequest;
import example.process.Context;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DeleteFileController {

    private final Context context;
    private final DeleteFileValidator deleteFileValidator;
    private final IDeleteFileCommand deleteFileCommand;

    /**
     * deletes a file based on the id supplied
     */
    @DeleteMapping(value = "/api/v1/echo-file/{id}", produces = {"application/json"})
    public ResponseEntity<Void> deleteFile(@PathVariable Long id) {
        DeleteFileRequest request = new DeleteFileRequest()
                .setId(id)
                .setContext(context);
        deleteFileValidator.validate(request);
        deleteFileCommand.execute(request);
        return ResponseEntity.noContent().build();
    }
}
