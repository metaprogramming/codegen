package example.adapters.in.rest.validators;

import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoMultiContentTextPlainRequest;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentTextPlainValidator extends Validator<EchoMultiContentTextPlainRequest> {

    public static final Field<EchoMultiContentTextPlainRequest,String> FIELD_BODY = new Field<>("body", EchoMultiContentTextPlainRequest::getBody);

    public void check(ValidationContext<EchoMultiContentTextPlainRequest> ctx) {
        ctx.setBean(OperationId.ECHO_MULTI_CONTENT);
        ctx.checkRoot(FIELD_BODY, required());
    }
}
