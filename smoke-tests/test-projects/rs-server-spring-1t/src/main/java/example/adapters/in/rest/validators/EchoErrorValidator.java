package example.adapters.in.rest.validators;

import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoErrorRequest;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoErrorValidator extends Validator<EchoErrorRequest> {

    public static final Field<EchoErrorRequest,String> FIELD_ERROR_MESSAGE = new Field<>("errorMessage (QUERY parameter)", EchoErrorRequest::getErrorMessage);

    public void check(ValidationContext<EchoErrorRequest> ctx) {
        ctx.setBean(OperationId.ECHO_ERROR);
        ctx.check(FIELD_ERROR_MESSAGE, required());
    }
}
