package example.adapters.in.rest.controllers;

import example.adapters.in.rest.validators.EchoPostValidator;
import example.ports.in.rest.IEchoPostCommand;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EchoPostRequest;
import example.process.Context;
import java.time.ZonedDateTime;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoPostController {

    private final Context context;
    private final EchoPostValidator echoPostValidator;
    private final IEchoPostCommand echoPostCommand;

    @PostMapping(value = "/api/v1/echo", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity<EchoBodyDto> echoPost(@RequestHeader(value = "Authorization", required = false) String authorization, @RequestHeader("X-Correlation-ID") String xCorrelationId, @RequestHeader(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime timestamp, @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam, @RequestBody EchoBodyDto requestBody) {
        EchoPostRequest request = new EchoPostRequest()
                .setAuthorization(authorization)
                .setXCorrelationId(xCorrelationId)
                .setTimestamp(timestamp)
                .setInlineHeaderParam(inlineHeaderParam)
                .setRequestBody(requestBody)
                .setContext(context);
        echoPostValidator.validate(request);
        return echoPostCommand.execute(request);
    }
}
