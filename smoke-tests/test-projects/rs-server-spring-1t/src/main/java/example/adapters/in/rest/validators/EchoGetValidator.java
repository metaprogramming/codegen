package example.adapters.in.rest.validators;

import example.adapters.in.rest.validators.custom.SecurityApiKeyChecker;
import example.adapters.in.rest.validators.custom.SecurityAuthorizationChecker;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DefaultEnum;
import example.ports.in.rest.dtos.EchoGetRequest;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.commons.validator.Privilege.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoGetValidator extends Validator<EchoGetRequest> {

    public static final Field<EchoGetRequest,String> FIELD_AUTHORIZATION = new Field<>("Authorization (HEADER parameter)", EchoGetRequest::getAuthorization);
    public static final Field<EchoGetRequest,String> FIELD_API_KEY = new Field<>("api_key (QUERY parameter)", EchoGetRequest::getApiKey);
    public static final Field<EchoGetRequest,String> FIELD_X_CORRELATION_ID = new Field<>("X-Correlation-ID (HEADER parameter)", EchoGetRequest::getXCorrelationId);
    public static final Field<EchoGetRequest,Long> FIELD_PROP_INT_REQUIRED = new Field<>("prop_int_required (QUERY parameter)", EchoGetRequest::getPropIntRequired);
    public static final Field<EchoGetRequest,Float> FIELD_PROP_FLOAT = new Field<>("prop_float (QUERY parameter)", EchoGetRequest::getPropFloat);
    public static final Field<EchoGetRequest,DefaultEnum> FIELD_PROP_ENUM = new Field<>("prop_enum (QUERY parameter)", EchoGetRequest::getPropEnum);

    private final SecurityAuthorizationChecker securityAuthorizationChecker;
    private final SecurityApiKeyChecker securityApiKeyChecker;

    public void check(ValidationContext<EchoGetRequest> ctx) {
        ctx.setBean(OperationId.ECHO_GET);
        ctx.check(authParamNonNull(FIELD_AUTHORIZATION, FIELD_API_KEY));
        ctx.check(FIELD_AUTHORIZATION, securityAuthorizationChecker.check(WRITE, READ));
        ctx.check(FIELD_API_KEY, securityApiKeyChecker.check(WRITE, READ));
        ctx.check(FIELD_X_CORRELATION_ID, required());
        ctx.check(FIELD_PROP_INT_REQUIRED, required(), ge(5l));
        ctx.check(FIELD_PROP_FLOAT, le(5.02f));
    }
}
