package example.adapters.in.rest.validators;

import example.adapters.in.rest.validators.custom.SecurityAuthorizationChecker;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EchoPostRequest;
import java.time.ZonedDateTime;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.commons.validator.Privilege.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoPostValidator extends Validator<EchoPostRequest> {

    public static final Field<EchoPostRequest,String> FIELD_AUTHORIZATION = new Field<>("Authorization (HEADER parameter)", EchoPostRequest::getAuthorization);
    public static final Field<EchoPostRequest,String> FIELD_X_CORRELATION_ID = new Field<>("X-Correlation-ID (HEADER parameter)", EchoPostRequest::getXCorrelationId);
    public static final Field<EchoPostRequest,ZonedDateTime> FIELD_TIMESTAMP = new Field<>("timestamp (HEADER parameter)", EchoPostRequest::getTimestamp);
    public static final Field<EchoPostRequest,String> FIELD_INLINE_HEADER_PARAM = new Field<>("Inline-Header-Param (HEADER parameter)", EchoPostRequest::getInlineHeaderParam);
    public static final Field<EchoPostRequest,EchoBodyDto> FIELD_REQUEST_BODY = new Field<>("requestBody", EchoPostRequest::getRequestBody);

    private final SecurityAuthorizationChecker securityAuthorizationChecker;
    private final EchoBodyValidator echoBodyValidator;

    public void check(ValidationContext<EchoPostRequest> ctx) {
        ctx.setBean(OperationId.ECHO_POST);
        ctx.check(authParamNonNull(FIELD_AUTHORIZATION));
        ctx.check(FIELD_AUTHORIZATION, securityAuthorizationChecker.check(WRITE));
        ctx.check(FIELD_X_CORRELATION_ID, required());
        ctx.checkRoot(FIELD_REQUEST_BODY, required(), echoBodyValidator);
    }
}
