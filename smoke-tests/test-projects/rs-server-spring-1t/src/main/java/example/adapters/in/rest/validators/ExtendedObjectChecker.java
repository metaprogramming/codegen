/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest.validators;

import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.commons.validator.ValidationError;
import example.ports.in.rest.dtos.ExtendedObjectDto;
import example.ports.in.rest.dtos.ReusableEnum;
import org.springframework.stereotype.Component;

@Component
public class ExtendedObjectChecker implements Checker<ExtendedObjectDto> {

    @Override
    public void check(ValidationContext<ExtendedObjectDto> context) {
        if (ReusableEnum.V_3 == context.getValue().getEoEnumReusable()) {
            ValidationContext<ReusableEnum> child = context.getChild(ExtendedObjectValidator.FIELD_EO_ENUM_REUSABLE);
            context.addError(ValidationError.builder()
                    .message(String.format("we don't want 3 in %s", child.getPath()))
                    .field(child.getPath())
                    .code("custom_code2").build());
        }
    }
}
