/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest

import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.Resource
import org.springframework.http.RequestEntity
import org.springframework.util.LinkedMultiValueMap

import java.nio.charset.StandardCharsets
import java.util.function.Function

class EchoFileSpec extends BaseSpecification {

    String getOperationPath() {
        '/api/v1/echo-file/123'
    }

    def "should upload and download and delete file - #contentType"() {
        given:
        def uri = new URI(getEndpoint())

        when:
        def uploadReq = (uploadReqFactory as Function<EchoFileSpec, RequestEntity>).apply(this)
        def uploadRes = restTemplate.exchange(uploadReq, String)
        then:
        uploadRes.statusCodeValue == 204
        uploadRes.body == null

        when:
        def downloadReq = RequestEntity.get(uri).build()
        def downloadRes = restTemplate.exchange(downloadReq, Resource)
        then:
        downloadRes.statusCodeValue == 200
        downloadRes.body.inputStream.text == FILE_BODY

        when:
        def deleteReq = RequestEntity.delete(uri).build()
        def deleteRes = restTemplate.exchange(deleteReq, String)
        then:
        deleteRes.statusCodeValue == 204
        deleteRes.body == null

        when:
        def download2Res = restTemplate.exchange(downloadReq, Resource)
        then:
        download2Res.statusCodeValue == 404

        where:
        contentType                | uploadReqFactory
        'application/octet-stream' | { EchoFileSpec spec -> spec.makeUploadStreamRequest() }
        'multipart/form-data'      | { EchoFileSpec spec -> spec.makeUploadFormRequest() }
    }

    def makeUploadStreamRequest() {
        RequestEntity.post(new URI(getEndpoint())).body(FILE_BODY.bytes)
    }

    def makeUploadFormRequest() {
        def body = new LinkedMultiValueMap<>()
        body.add("file", new ByteArrayResource(FILE_BODY.getBytes(StandardCharsets.UTF_8)) {
            String getFilename() { 'test.txt' }
        })

        RequestEntity.post(new URI("${getEndpoint()}/form")).body(body)
    }

    static def FILE_BODY = '''It is a test body.
Line 1
Line 2'''


}
