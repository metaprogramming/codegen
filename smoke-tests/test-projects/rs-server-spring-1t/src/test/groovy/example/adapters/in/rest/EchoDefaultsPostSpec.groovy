/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest


import static org.springframework.http.HttpMethod.POST

class EchoDefaultsPostSpec extends BaseSpecification {

    String DEFAULT_HEADER_PARAM = 'Default-Header-Param'

    String getOperationPath() {
        'api/v1/echo-defaults'
    }

    def "should success when no values send"() {
        when:
        def response = call(POST).body(reqPayload).exchange(Map)

        then:
        response != null
        response.statusCode.value() == 200
        response.body != null
        response.headers.getFirst(DEFAULT_HEADER_PARAM) == 'a2'

        when:
        def body = response.body

        then:
        body.propInt == 101
        body.propFloat == 101.101
        body.propDouble == 202.202
        body.propNumber == 3.33
        body.propString == 'WHITE'
        body.propEnum == '3'
        body.propDate == '2021-09-01'
        body.propDateTime == '2021-09-01T09:09:09Z'

        where:
        reqPayload << [null, [:]]
    }

    def "should success when values send"() {
        given:
        def reqBody = [
                propInt     : 102,
                propFloat   : 102.201,
                propDouble  : 201.102,
                propNumber  : 3.21,
                propString  : 'BLACK',
                propEnum    : '3',
                propDate    : '2022-09-01',
                propDateTime: '2022-09-01T09:09:09Z'
        ]
        def defaultHeaderParam = 'a1'
        when:
        def response = call(POST).body(reqBody)
                .headerParam(DEFAULT_HEADER_PARAM, defaultHeaderParam)
                .exchange(Map)

        then:
        response != null
        response.statusCode.value() == 200
        response.body != null
        response.headers.getFirst(DEFAULT_HEADER_PARAM) == defaultHeaderParam
        response.body == reqBody
    }

    def "should fail when Default-Header-Param has invalid value"() {
        when:
        def response = call(POST).body([:])
                .headerParam(DEFAULT_HEADER_PARAM, 'bad-value')
                .exchange(Map)

        then:
        response != null
        response.statusCode.value() == 400
    }
}