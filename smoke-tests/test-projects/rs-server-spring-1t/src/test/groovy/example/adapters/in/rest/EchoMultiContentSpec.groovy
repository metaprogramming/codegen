/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import example.MultiContentBodyData
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap

import static org.springframework.http.HttpMethod.POST

class EchoMultiContentSpec extends BaseSpecification {

    @Override
    String getOperationPath() {
        '/api/v1/echo-multi-content'
    }

    def "should validation error on"() {
        given:
        def data = MultiContentBodyData.fullDataSet().set(field, value).make()

        when: 'json'
        def jsonRes = callInvalid('application/json', data)
        then:
        checkInvalidResult(jsonRes, field, code)

        when: 'xml'
        def xmlRes = callInvalid('application/xml', toXml(data))
        then:
        checkInvalidResult(xmlRes, field, code)

        when: 'x-www-form-urlencoded'
        def urlEncodedRes = callInvalid('application/x-www-form-urlencoded', toUrlEncoded(data))
        then:
        checkInvalidResult(urlEncodedRes, field, code)

        where:
        field                   | value | code
        'response_content_type' | null  | 'is_required'
        'prop_int'              | '2'   | 'is_too_big'
    }

    def "should send with x-www-form-urlencoded and return json"() {
        given:
        def data = MultiContentBodyData.fullDataSet()
                .set('response_content_type', 'application/json')
                .make()
        when:
        def result = callUrlEncoded(data, Map)
        then:
        result.getStatusCodeValue() == 200
        result.headers.getFirst('content-type') == 'application/json'
        result.body == data
    }

    def "should send with x-www-form-urlencoded and return xml"() {
        given:
        def data = MultiContentBodyData.fullDataSet()
                .set('response_content_type', 'application/xml')
                .set([prop_list: ['i1', 'i2']])
                .make()
        when:
        def result = callUrlEncoded(data, String)
        then:
        result.getStatusCodeValue() == 200
        result.headers.getFirst('content-type') == 'application/xml'
        result.body == toXml(data)
    }

    def "should send with x-www-form-urlencoded and return text"() {
        given:
        def data = MultiContentBodyData.fullDataSet()
                .set('response_content_type', 'text/plain')
                .make()
        when:
        def result = callUrlEncoded(data, String)
        then:
        result.getStatusCodeValue() == 200
        result.headers.getFirst('content-type') == 'text/plain'
        result.body == "TEXT_PLAIN|1|${data.get('prop_date')}|[prop_list]"
    }

    def "should send with json"() {
        given:
        def data = MultiContentBodyData.fullDataSet().make()
        when:
        def result = call(POST).body(data).exchange(Map)
        then:
        result.statusCodeValue == 200
        result.body == data
    }

    def "should send with xml"() {
        given:
        def data = MultiContentBodyData.fullDataSet().make()
        when:
        def result = callXml(data, Map)
        then:
        result.statusCodeValue == 200
        result.body == data
    }

    def "should send with text"() {
        given:
        def data = "TEXT_PLAIN|1|2022-11-11|[prop_list]"
        when:
        def result = call(POST)
                .headerParam('content-type', 'text/plain')
                .body(data)
                .exchange(String)
        then:
        result.statusCodeValue == 200
        result.body == data
    }

    private <T> ResponseEntity<T> callUrlEncoded(Map body, Class<T> resultClass) {
        call(POST).headerParam('content-type', 'application/x-www-form-urlencoded')
                .body(toUrlEncoded(body))
                .exchange(resultClass)
    }

    private <T> ResponseEntity<T> callXml(Map body, Class<T> resultClass) {
        call(POST).headerParam('content-type', 'application/xml')
                .body(toXml(body))
                .exchange(resultClass)
    }

    ResponseEntity<Map> callInvalid(String contentType, def data) {
        call(POST).headerParam('content-type', contentType).body(data).exchange(Map)
    }

    boolean checkInvalidResult(ResponseEntity<Map> response, String errorField, String errorCode) {
        assert response.statusCodeValue == 400
        assert response.body.errors
        verifyAll(response.body.errors[0] as Map) {
            field == errorField
            code == errorCode
        }
        true
    }

    def toUrlEncoded(Map<String, Object> value) {
        def result = new LinkedMultiValueMap<String, String>()
        value.each { k, v ->
            if (v instanceof List) {
                v.each { result.add(k, it as String) }
            } else {
                result.add(k, v as String)
            }
        }
        result
    }

    def toXml(Map<String, Object> data) {
        new XmlMapper().writeValueAsString(data)
                .replace('LinkedHashMap', 'MultiContentBodyDto')
                .replaceFirst("<prop_list>", "<prop_list><prop_list>")
                .replace("</MultiContentBodyDto>", "</prop_list></MultiContentBodyDto>")
    }

}
