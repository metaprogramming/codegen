package example.commons;

import javax.annotation.processing.Generated;
import static example.commons.TestData.*;

@Generated("pl.metaprogramming.codegen")
public class SimpleObjectData {

    private SimpleObjectData() {
    }

    public static TestData minDataSet() {
        return testData()
                .set("so_prop_string", "so_prop_string")
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("so_prop_string", "so_prop_string")
                .set("so_prop_date", date())
                ;
    }
}
