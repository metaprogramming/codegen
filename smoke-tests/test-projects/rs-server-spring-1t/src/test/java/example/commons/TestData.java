package example.commons;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public class TestData {

    private final Map<String, Object> result;

    public static TestData testData() {
        return new TestData();
    }

    public TestData() {
        this.result = new LinkedHashMap<>();
    }

    public TestData(Map<String, Object> map) {
        result = map;
    }

    public Map<String, Object> make() {
        return result;
    }

    public TestData set(TestData data) {
        return set(data.result);
    }

    @SuppressWarnings("unchecked")
    public TestData set(String key, Object value) {
        int dotIdx = key.indexOf('.');
        if (dotIdx > 0) {
            getObject(key.substring(0, dotIdx)).set(key.substring(dotIdx + 1), value);
        } else if (value == null) {
            result.remove(key);
        } else if (value instanceof Map map) {
            getObject(key).set(map);
        } else {
            result.put(key, value);
        }
        return this;
    }

    public TestData set(Map<String, Object> from) {
        from.forEach(this::set);
        return this;
    }

    @SuppressWarnings("unchecked")
    private TestData getObject(String field) {
        Object child = result.computeIfAbsent(field, (k) -> new LinkedHashMap<>());
        if (child instanceof Map map) {
            return new TestData(map);
        }
        throw new IllegalStateException(field + " is not a Map");
    }

    public static String format(String format) {
        return format;
    }

    public static List<?> list(Object o) {
        return Arrays.asList(o);
    }

    public static Map<String, Object> map(String key, Object value) {
        Map<String, Object> result = new LinkedHashMap<>();
        result.put(key, value);
        return result;
    }

    public static String dateTime() {
        return OffsetDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
    }

    public static String date() {
        return LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
}
