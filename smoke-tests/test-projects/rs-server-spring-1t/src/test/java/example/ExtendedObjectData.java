package example;

import example.commons.TestData;

import static example.commons.TestData.date;
import static example.commons.TestData.testData;

public class ExtendedObjectData {
    

    public static TestData minDataSet() {
        return testData()
                .set("so_prop_string", "so_prop_string")
                ;
    }

    public static TestData fullDataSet() {
        return testData()
                .set("eo_enum_reusable", "a")
                .set("self_property", ExtendedObjectData.minDataSet().make())
                .set("so_prop_string", "so_prop_string")
                .set("so_prop_date", date())
                ;
    }

}
