package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.DownloadEchoFileRrequest;
import example.commons.SerializationUtils;
import example.ports.in.rest.dtos.DownloadEchoFileRequest;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileRequestMapper {

    public DownloadEchoFileRrequest map2DownloadEchoFileRrequest(String id) {
        return new DownloadEchoFileRrequest().setId(id);
    }

    public DownloadEchoFileRequest map2DownloadEchoFileRequest(DownloadEchoFileRrequest value) {
        return value == null ? null : new DownloadEchoFileRequest().setId(SerializationUtils.toLong(value.getId()));
    }
}
