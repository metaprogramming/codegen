package example.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import java.util.Map;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Echo object
 * 
 * Test object
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoBodyRdto {

    @JsonProperty("prop_int")
    @JsonRawValue
    private String propInt;
    @JsonProperty("prop_int_second")
    @JsonRawValue
    private String propIntSecond;
    @JsonProperty("prop_int_required")
    @JsonRawValue
    private String propIntRequired;
    @JsonProperty("prop_float")
    @JsonRawValue
    private String propFloat;
    @JsonProperty("prop_double")
    @JsonRawValue
    private String propDouble;
    @JsonProperty("prop_amount")
    private String propAmount;
    @JsonProperty("prop_amount_number")
    @JsonRawValue
    private String propAmountNumber;
    @JsonProperty("prop_string")
    private String propString;
    @JsonProperty("prop_string_pattern")
    private String propStringPattern;
    @JsonProperty("prop_default")
    private String propDefault = "value";
    @JsonProperty("prop_date")
    private String propDate;
    @JsonProperty("prop_date_second")
    private String propDateSecond;
    @JsonProperty("prop_date_time")
    private String propDateTime;
    @JsonProperty("prop_base64")
    private String propBase64;
    @JsonProperty("prop_boolean")
    @JsonRawValue
    private String propBoolean;
    @JsonProperty("prop_object")
    private SimpleObjectRdto propObject;
    @JsonProperty("prop_object_any")
    private Map<String, Object> propObjectAny;
    @JsonProperty("prop_object_extended")
    private ExtendedObjectRdto propObjectExtended;
    @JsonProperty("prop_enum_reusable")
    private String propEnumReusable = "a";
    @JsonProperty("prop_enum")
    private String propEnum;
}
