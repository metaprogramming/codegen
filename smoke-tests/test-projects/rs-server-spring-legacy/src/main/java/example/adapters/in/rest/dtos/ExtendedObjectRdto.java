package example.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Extended object
 * 
 * Simple object for testing
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectRdto extends SimpleObjectRdto {

    @JsonProperty("eo_enum_reusable")
    private String eoEnumReusable;
    @JsonProperty("self_property")
    private ExtendedObjectRdto selfProperty;
}
