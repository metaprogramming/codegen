package example.adapters.in.rest;

import example.adapters.in.rest.dtos.DeleteFileRrequest;
import example.adapters.in.rest.dtos.DownloadEchoFileRrequest;
import example.adapters.in.rest.dtos.EchoArraysBodyRdto;
import example.adapters.in.rest.dtos.EchoArraysPostRrequest;
import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.adapters.in.rest.dtos.EchoDateArrayGetRrequest;
import example.adapters.in.rest.dtos.EchoDefaultsBodyRdto;
import example.adapters.in.rest.dtos.EchoDefaultsPostRrequest;
import example.adapters.in.rest.dtos.EchoEmptyRrequest;
import example.adapters.in.rest.dtos.EchoErrorRrequest;
import example.adapters.in.rest.dtos.EchoGetRrequest;
import example.adapters.in.rest.dtos.EchoPostRrequest;
import example.adapters.in.rest.dtos.UploadEchoFileRrequest;
import example.adapters.in.rest.dtos.UploadEchoFileWithFormRrequest;
import example.adapters.in.rest.mappers.DeleteFileRequestMapper;
import example.adapters.in.rest.mappers.DeleteFileResponseMapper;
import example.adapters.in.rest.mappers.DownloadEchoFileRequestMapper;
import example.adapters.in.rest.mappers.DownloadEchoFileResponseMapper;
import example.adapters.in.rest.mappers.EchoArraysPostRequestMapper;
import example.adapters.in.rest.mappers.EchoArraysPostResponseMapper;
import example.adapters.in.rest.mappers.EchoDateArrayGetRequestMapper;
import example.adapters.in.rest.mappers.EchoDateArrayGetResponseMapper;
import example.adapters.in.rest.mappers.EchoDefaultsPostRequestMapper;
import example.adapters.in.rest.mappers.EchoDefaultsPostResponseMapper;
import example.adapters.in.rest.mappers.EchoEmptyRequestMapper;
import example.adapters.in.rest.mappers.EchoEmptyResponseMapper;
import example.adapters.in.rest.mappers.EchoErrorRequestMapper;
import example.adapters.in.rest.mappers.EchoErrorResponseMapper;
import example.adapters.in.rest.mappers.EchoGetRequestMapper;
import example.adapters.in.rest.mappers.EchoGetResponseMapper;
import example.adapters.in.rest.mappers.EchoPostRequestMapper;
import example.adapters.in.rest.mappers.EchoPostResponseMapper;
import example.adapters.in.rest.mappers.UploadEchoFileRequestMapper;
import example.adapters.in.rest.mappers.UploadEchoFileResponseMapper;
import example.adapters.in.rest.mappers.UploadEchoFileWithFormRequestMapper;
import example.adapters.in.rest.mappers.UploadEchoFileWithFormResponseMapper;
import example.adapters.in.rest.validators.DeleteFileValidator;
import example.adapters.in.rest.validators.DownloadEchoFileValidator;
import example.adapters.in.rest.validators.EchoArraysPostValidator;
import example.adapters.in.rest.validators.EchoDateArrayGetValidator;
import example.adapters.in.rest.validators.EchoDefaultsPostValidator;
import example.adapters.in.rest.validators.EchoEmptyValidator;
import example.adapters.in.rest.validators.EchoErrorValidator;
import example.adapters.in.rest.validators.EchoGetValidator;
import example.adapters.in.rest.validators.EchoPostValidator;
import example.adapters.in.rest.validators.UploadEchoFileValidator;
import example.adapters.in.rest.validators.UploadEchoFileWithFormValidator;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Generated("pl.metaprogramming.codegen")
public class EchoController {

    @Autowired
    private EchoPostRequestMapper echoPostRequestMapper;
    @Autowired
    private EchoPostValidator echoPostValidator;
    @Autowired
    private EchoFacade echoFacade;
    @Autowired
    private EchoPostResponseMapper echoPostResponseMapper;
    @Autowired
    private ValidationResultMapper validationResultMapper;
    @Autowired
    private EchoGetRequestMapper echoGetRequestMapper;
    @Autowired
    private EchoGetValidator echoGetValidator;
    @Autowired
    private EchoGetResponseMapper echoGetResponseMapper;
    @Autowired
    private EchoDefaultsPostRequestMapper echoDefaultsPostRequestMapper;
    @Autowired
    private EchoDefaultsPostValidator echoDefaultsPostValidator;
    @Autowired
    private EchoDefaultsPostResponseMapper echoDefaultsPostResponseMapper;
    @Autowired
    private EchoArraysPostRequestMapper echoArraysPostRequestMapper;
    @Autowired
    private EchoArraysPostValidator echoArraysPostValidator;
    @Autowired
    private EchoArraysPostResponseMapper echoArraysPostResponseMapper;
    @Autowired
    private EchoDateArrayGetRequestMapper echoDateArrayGetRequestMapper;
    @Autowired
    private EchoDateArrayGetValidator echoDateArrayGetValidator;
    @Autowired
    private EchoDateArrayGetResponseMapper echoDateArrayGetResponseMapper;
    @Autowired
    private UploadEchoFileWithFormRequestMapper uploadEchoFileWithFormRequestMapper;
    @Autowired
    private UploadEchoFileWithFormValidator uploadEchoFileWithFormValidator;
    @Autowired
    private UploadEchoFileWithFormResponseMapper uploadEchoFileWithFormResponseMapper;
    @Autowired
    private UploadEchoFileRequestMapper uploadEchoFileRequestMapper;
    @Autowired
    private UploadEchoFileValidator uploadEchoFileValidator;
    @Autowired
    private UploadEchoFileResponseMapper uploadEchoFileResponseMapper;
    @Autowired
    private DownloadEchoFileRequestMapper downloadEchoFileRequestMapper;
    @Autowired
    private DownloadEchoFileValidator downloadEchoFileValidator;
    @Autowired
    private DownloadEchoFileResponseMapper downloadEchoFileResponseMapper;
    @Autowired
    private DeleteFileRequestMapper deleteFileRequestMapper;
    @Autowired
    private DeleteFileValidator deleteFileValidator;
    @Autowired
    private DeleteFileResponseMapper deleteFileResponseMapper;
    @Autowired
    private EchoEmptyValidator echoEmptyValidator;
    @Autowired
    private EchoEmptyRequestMapper echoEmptyRequestMapper;
    @Autowired
    private EchoEmptyResponseMapper echoEmptyResponseMapper;
    @Autowired
    private EchoErrorRequestMapper echoErrorRequestMapper;
    @Autowired
    private EchoErrorValidator echoErrorValidator;
    @Autowired
    private EchoErrorResponseMapper echoErrorResponseMapper;

    @PostMapping(value = "/api/v1/echo", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity echoPost(@RequestHeader(value = "Authorization", required = false) String authorizationParam,
            @RequestHeader(value = "X-Correlation-ID", required = false) String correlationIdParam,
            @RequestHeader(value = "timestamp", required = false) String timestampParam,
            @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam,
            @RequestBody EchoBodyRdto requestBody) {
        EchoPostRrequest request = echoPostRequestMapper.map2EchoPostRrequest(authorizationParam, correlationIdParam,
                timestampParam, inlineHeaderParam, requestBody);
        ValidationResult validationResult = echoPostValidator.validate(request);
        return validationResult.isValid()
                ? echoPostResponseMapper.map(echoFacade.echoPost(echoPostRequestMapper.map2EchoPostRequest(request)))
                : validationResultMapper.map(validationResult);
    }

    @GetMapping(value = "/api/v1/echo", produces = {"application/json"})
    public ResponseEntity echoGet(@RequestHeader(value = "Authorization", required = false) String authorization,
            @RequestHeader(value = "api_key", required = false) String apiKey,
            @RequestHeader(value = "X-Correlation-ID", required = false) String correlationIdParam,
            @RequestParam(value = "prop_int_required", required = false) String propIntRequired) {
        EchoGetRrequest request = echoGetRequestMapper.map2EchoGetRrequest(authorization, apiKey, correlationIdParam,
                propIntRequired);
        ValidationResult validationResult = echoGetValidator.validate(request);
        return validationResult.isValid()
                ? echoGetResponseMapper.map(echoFacade.echoGet(echoGetRequestMapper.map2EchoGetRequest(request)))
                : validationResultMapper.map(validationResult);
    }

    @PostMapping(value = "/api/v1/echo-defaults", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity echoDefaultsPost(
            @RequestHeader(value = "Default-Header-Param", required = false) String defaultHeaderParam,
            @RequestBody(required = false) EchoDefaultsBodyRdto requestBody) {
        EchoDefaultsPostRrequest request = echoDefaultsPostRequestMapper
                .map2EchoDefaultsPostRrequest(defaultHeaderParam, requestBody);
        ValidationResult validationResult = echoDefaultsPostValidator.validate(request);
        return validationResult.isValid()
                ? echoDefaultsPostResponseMapper.map(
                        echoFacade.echoDefaultsPost(echoDefaultsPostRequestMapper.map2EchoDefaultsPostRequest(request)))
                : validationResultMapper.map(validationResult);
    }

    @PostMapping(value = "/api/v1/echo-arrays", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity echoArraysPost(
            @RequestHeader(value = "Authorization", required = false) String authorizationParam,
            @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam,
            @RequestBody List<EchoArraysBodyRdto> requestBody) {
        EchoArraysPostRrequest request = echoArraysPostRequestMapper.map2EchoArraysPostRrequest(authorizationParam,
                inlineHeaderParam, requestBody);
        ValidationResult validationResult = echoArraysPostValidator.validate(request);
        return validationResult.isValid()
                ? echoArraysPostResponseMapper
                        .map(echoFacade.echoArraysPost(echoArraysPostRequestMapper.map2EchoArraysPostRequest(request)))
                : validationResultMapper.map(validationResult);
    }

    @GetMapping(value = "/api/v1/echo-date-array", produces = {"application/json"})
    public ResponseEntity echoDateArrayGet(
            @RequestParam(value = "date_array", required = false) List<String> dateArray) {
        EchoDateArrayGetRrequest request = echoDateArrayGetRequestMapper.map2EchoDateArrayGetRrequest(dateArray);
        ValidationResult validationResult = echoDateArrayGetValidator.validate(request);
        return validationResult.isValid()
                ? echoDateArrayGetResponseMapper.map(
                        echoFacade.echoDateArrayGet(echoDateArrayGetRequestMapper.map2EchoDateArrayGetRequest(request)))
                : validationResultMapper.map(validationResult);
    }

    /**
     * upload file using multipart/form-data
     */
    @PostMapping(value = "/api/v1/echo-file/{id}/form", produces = {"application/json"}, consumes = {
            "multipart/form-data"})
    public ResponseEntity uploadEchoFileWithForm(@PathVariable(required = false) String id,
            @RequestPart(required = false) MultipartFile file) {
        UploadEchoFileWithFormRrequest request = uploadEchoFileWithFormRequestMapper
                .map2UploadEchoFileWithFormRrequest(id, file);
        ValidationResult validationResult = uploadEchoFileWithFormValidator.validate(request);
        return validationResult.isValid()
                ? uploadEchoFileWithFormResponseMapper.map(echoFacade.uploadEchoFileWithForm(
                        uploadEchoFileWithFormRequestMapper.map2UploadEchoFileWithFormRequest(request)))
                : validationResultMapper.map(validationResult);
    }

    /**
     * upload file - not supported by OAS2
     */
    @PostMapping(value = "/api/v1/echo-file/{id}", produces = {"application/json"}, consumes = {
            "application/octet-stream"})
    public ResponseEntity uploadEchoFile(@PathVariable(required = false) String id, @RequestBody Resource requestBody) {
        UploadEchoFileRrequest request = uploadEchoFileRequestMapper.map2UploadEchoFileRrequest(id, requestBody);
        ValidationResult validationResult = uploadEchoFileValidator.validate(request);
        return validationResult.isValid()
                ? uploadEchoFileResponseMapper
                        .map(echoFacade.uploadEchoFile(uploadEchoFileRequestMapper.map2UploadEchoFileRequest(request)))
                : validationResultMapper.map(validationResult);
    }

    /**
     * Returns a file
     */
    @GetMapping(value = "/api/v1/echo-file/{id}", produces = {"image/jpeg", "application/json"})
    public ResponseEntity downloadEchoFile(@PathVariable(required = false) String id) {
        DownloadEchoFileRrequest request = downloadEchoFileRequestMapper.map2DownloadEchoFileRrequest(id);
        ValidationResult validationResult = downloadEchoFileValidator.validate(request);
        return validationResult.isValid()
                ? downloadEchoFileResponseMapper.map(
                        echoFacade.downloadEchoFile(downloadEchoFileRequestMapper.map2DownloadEchoFileRequest(request)))
                : validationResultMapper.map(validationResult);
    }

    /**
     * deletes a file based on the id supplied
     */
    @DeleteMapping(value = "/api/v1/echo-file/{id}", produces = {"application/json"})
    public ResponseEntity deleteFile(@PathVariable(required = false) String id) {
        DeleteFileRrequest request = deleteFileRequestMapper.map2DeleteFileRrequest(id);
        ValidationResult validationResult = deleteFileValidator.validate(request);
        return validationResult.isValid()
                ? deleteFileResponseMapper
                        .map(echoFacade.deleteFile(deleteFileRequestMapper.map2DeleteFileRequest(request)))
                : validationResultMapper.map(validationResult);
    }

    @GetMapping("/api/v1/echo-empty")
    public ResponseEntity echoEmpty() {
        EchoEmptyRrequest request = new EchoEmptyRrequest();
        ValidationResult validationResult = echoEmptyValidator.validate(request);
        return validationResult.isValid()
                ? echoEmptyResponseMapper
                        .map(echoFacade.echoEmpty(echoEmptyRequestMapper.map2EchoEmptyRequest(request)))
                : validationResultMapper.map(validationResult);
    }

    @GetMapping(value = "/api/v1/echo-error", produces = {"application/json"})
    public ResponseEntity echoError(@RequestParam(required = false) String errorMessage) {
        EchoErrorRrequest request = echoErrorRequestMapper.map2EchoErrorRrequest(errorMessage);
        ValidationResult validationResult = echoErrorValidator.validate(request);
        return validationResult.isValid()
                ? echoErrorResponseMapper
                        .map(echoFacade.echoError(echoErrorRequestMapper.map2EchoErrorRequest(request)))
                : validationResultMapper.map(validationResult);
    }
}
