package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.DeleteFileRrequest;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class DeleteFileValidator extends Validator<DeleteFileRrequest> {

    public static final Field<DeleteFileRrequest, String> FIELD_ID = new Field<>("id (PATH parameter)",
            DeleteFileRrequest::getId);

    public void check(ValidationContext<DeleteFileRrequest> ctx) {
        ctx.setBean(OperationId.DELETE_FILE);
        ctx.check(FIELD_ID, required(), INT64);
    }
}
