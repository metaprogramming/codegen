package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoDefaultsBodyRdto;
import example.adapters.in.rest.dtos.EchoDefaultsPostRrequest;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DefaultEnumEnum;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostValidator extends Validator<EchoDefaultsPostRrequest> {

    public static final Field<EchoDefaultsPostRrequest, String> FIELD_DEFAULT_HEADER_PARAM = new Field<>(
            "Default-Header-Param (HEADER parameter)", EchoDefaultsPostRrequest::getDefaultHeaderParam);
    public static final Field<EchoDefaultsPostRrequest, EchoDefaultsBodyRdto> FIELD_REQUEST_BODY = new Field<>(
            "requestBody", EchoDefaultsPostRrequest::getRequestBody);

    @Autowired
    private EchoDefaultsBodyValidator echoDefaultsBodyValidator;

    public void check(ValidationContext<EchoDefaultsPostRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_DEFAULTS_POST);
        ctx.check(FIELD_DEFAULT_HEADER_PARAM, allow(DefaultEnumEnum.values()));
        ctx.checkRoot(FIELD_REQUEST_BODY, echoDefaultsBodyValidator);
    }
}
