package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.adapters.in.rest.dtos.EchoPostRrequest;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoPostValidator extends Validator<EchoPostRrequest> {

    public static final Field<EchoPostRrequest, String> FIELD_AUTHORIZATION_PARAM = new Field<>(
            "Authorization (HEADER parameter)", EchoPostRrequest::getAuthorizationParam);
    public static final Field<EchoPostRrequest, String> FIELD_CORRELATION_ID_PARAM = new Field<>(
            "X-Correlation-ID (HEADER parameter)", EchoPostRrequest::getCorrelationIdParam);
    public static final Field<EchoPostRrequest, String> FIELD_TIMESTAMP_PARAM = new Field<>(
            "timestamp (HEADER parameter)", EchoPostRrequest::getTimestampParam);
    public static final Field<EchoPostRrequest, String> FIELD_INLINE_HEADER_PARAM = new Field<>(
            "Inline-Header-Param (HEADER parameter)", EchoPostRrequest::getInlineHeaderParam);
    public static final Field<EchoPostRrequest, EchoBodyRdto> FIELD_REQUEST_BODY = new Field<>("requestBody",
            EchoPostRrequest::getRequestBody);

    @Autowired
    private ValidationBeanFactory validationBeanFactory;
    @Autowired
    private AuthorizationChecker authorizationChecker;
    @Autowired
    private EchoBodyValidator echoBodyValidator;

    public void check(ValidationContext<EchoPostRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_POST);
        ctx.setBean(UserDataValidationBean.class, validationBeanFactory::createUserDataValidationBean);
        ctx.check(FIELD_AUTHORIZATION_PARAM, authorizationChecker, required());
        ctx.check(FIELD_CORRELATION_ID_PARAM, required());
        ctx.check(FIELD_TIMESTAMP_PARAM, ISO_DATE_TIME);
        ctx.checkRoot(FIELD_REQUEST_BODY, required(), echoBodyValidator);
    }
}
