package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoDefaultsBodyRdto;
import example.adapters.in.rest.dtos.EchoDefaultsPostRrequest;
import example.ports.in.rest.dtos.DefaultEnumEnum;
import example.ports.in.rest.dtos.EchoDefaultsPostRequest;
import java.util.Optional;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostRequestMapper {

    @Autowired
    private EchoDefaultsBodyMapper echoDefaultsBodyMapper;

    public EchoDefaultsPostRrequest map2EchoDefaultsPostRrequest(String defaultHeaderParam,
            EchoDefaultsBodyRdto requestBody) {
        return new EchoDefaultsPostRrequest()
                .setDefaultHeaderParam(Optional.ofNullable(defaultHeaderParam).orElse("a2"))
                .setRequestBody(requestBody);
    }

    public EchoDefaultsPostRequest map2EchoDefaultsPostRequest(EchoDefaultsPostRrequest value) {
        return value == null
                ? null
                : new EchoDefaultsPostRequest()
                        .setDefaultHeaderParam(DefaultEnumEnum.fromValue(value.getDefaultHeaderParam()))
                        .setRequestBody(echoDefaultsBodyMapper.map2EchoDefaultsBodyDto(value.getRequestBody()));
    }
}
