package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoArraysBodyRdto;
import example.adapters.in.rest.dtos.EchoArraysPostRrequest;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostValidator extends Validator<EchoArraysPostRrequest> {

    public static final Field<EchoArraysPostRrequest, String> FIELD_AUTHORIZATION_PARAM = new Field<>(
            "Authorization (HEADER parameter)", EchoArraysPostRrequest::getAuthorizationParam);
    public static final Field<EchoArraysPostRrequest, String> FIELD_INLINE_HEADER_PARAM = new Field<>(
            "Inline-Header-Param (HEADER parameter)", EchoArraysPostRrequest::getInlineHeaderParam);
    public static final Field<EchoArraysPostRrequest, List<EchoArraysBodyRdto>> FIELD_REQUEST_BODY = new Field<>(
            "requestBody", EchoArraysPostRrequest::getRequestBody);

    @Autowired
    private AuthorizationChecker authorizationChecker;
    @Autowired
    private EchoArraysBodyValidator echoArraysBodyValidator;

    public void check(ValidationContext<EchoArraysPostRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_ARRAYS_POST);
        ctx.check(FIELD_AUTHORIZATION_PARAM, authorizationChecker, required());
        ctx.checkRoot(FIELD_REQUEST_BODY, required(), items(echoArraysBodyValidator));
    }
}
