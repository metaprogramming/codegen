package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoGetRrequest;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoGetValidator extends Validator<EchoGetRrequest> {

    public static final Field<EchoGetRrequest, String> FIELD_AUTHORIZATION = new Field<>(
            "Authorization (HEADER parameter)", EchoGetRrequest::getAuthorization);
    public static final Field<EchoGetRrequest, String> FIELD_API_KEY = new Field<>("api_key (HEADER parameter)",
            EchoGetRrequest::getApiKey);
    public static final Field<EchoGetRrequest, String> FIELD_CORRELATION_ID_PARAM = new Field<>(
            "X-Correlation-ID (HEADER parameter)", EchoGetRrequest::getCorrelationIdParam);
    public static final Field<EchoGetRrequest, String> FIELD_PROP_INT_REQUIRED = new Field<>(
            "prop_int_required (QUERY parameter)", EchoGetRrequest::getPropIntRequired);

    public void check(ValidationContext<EchoGetRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_GET);
        ctx.check(FIELD_CORRELATION_ID_PARAM, required());
        ctx.check(FIELD_PROP_INT_REQUIRED, required(), INT64, ge("5", Long::valueOf));
    }
}
