/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

import static example.commons.validator.CommonCheckers.required;
import static example.commons.validator.CommonCheckers.writeError;

@RequiredArgsConstructor
public class ExtendedObjectEnumChecker implements Checker<String> {

    private final ValidationContext<EchoBodyRdto> echoBodyCtx;

    public void check(ValidationContext<String> context) {
        context.check(required());
        if (context.isValid()) {
            ValidationContext<String> mainFieldCtx = echoBodyCtx.getChild(EchoBodyValidator.FIELD_PROP_ENUM_REUSABLE);
            if (!Objects.equals(context.getValue(), mainFieldCtx.getValue())) {
                writeError(context, context.getPath() + " should be equal to " + mainFieldCtx.getPath(), context.getPath(), "custom_code");
            }
        }
    }

    @Override
    public boolean checkNull() {
        return true;
    }
}
