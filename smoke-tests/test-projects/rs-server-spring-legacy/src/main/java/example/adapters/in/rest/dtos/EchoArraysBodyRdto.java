package example.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import example.commons.MapRawValueSerializer;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import java.util.List;
import java.util.Map;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Echo object with arrays properties
 * 
 * Test object containing arrays properties
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyRdto {

    @JsonProperty("prop_int_list")
    @JsonRawValue
    private List<String> propIntList;
    @JsonProperty("prop_float_list")
    @JsonRawValue
    private List<String> propFloatList;
    @JsonProperty("prop_double_list")
    @JsonRawValue
    private List<String> propDoubleList;
    @JsonProperty("prop_amount_list")
    private List<String> propAmountList;
    @JsonProperty("prop_string_list")
    private List<String> propStringList;
    @JsonProperty("prop_date_list")
    private List<String> propDateList;
    @JsonProperty("prop_date_time_list_of_list")
    private List<List<String>> propDateTimeListOfList;
    @JsonProperty("prop_boolean_list")
    @JsonRawValue
    private List<String> propBooleanList;
    @JsonProperty("prop_object_list")
    private List<SimpleObjectRdto> propObjectList;
    @JsonProperty("prop_object_list_of_list")
    private List<List<SimpleObjectRdto>> propObjectListOfList;
    @JsonProperty("prop_enum_reusable_list")
    private List<String> propEnumReusableList;
    @JsonProperty("prop_enum_list")
    private List<String> propEnumList;
    @JsonProperty("prop_map_of_int")
    @JsonSerialize(using = MapRawValueSerializer.class)
    private Map<String, String> propMapOfInt;
    @JsonProperty("prop_map_of_object")
    private Map<String, SimpleObjectRdto> propMapOfObject;
    @JsonProperty("prop_map_of_list_of_object")
    private Map<String, List<SimpleObjectRdto>> propMapOfListOfObject;
}
