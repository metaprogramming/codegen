package example.adapters.in.rest.mappers;

import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper;
import example.ports.in.rest.dtos.EchoDateArrayGetResponse;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetResponseMapper {

    @Autowired
    private ErrorDescriptionMapper errorDescriptionMapper;

    public ResponseEntity map(@Nonnull EchoDateArrayGetResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        if (response.is200()) {
            return responseBuilder
                    .body(SerializationUtils.transformList(response.get200(), SerializationUtils::toString));
        }
        return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.getOther()));
    }
}
