package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoErrorRrequest;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoErrorValidator extends Validator<EchoErrorRrequest> {

    public static final Field<EchoErrorRrequest, String> FIELD_ERROR_MESSAGE = new Field<>(
            "errorMessage (QUERY parameter)", EchoErrorRrequest::getErrorMessage);

    public void check(ValidationContext<EchoErrorRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_ERROR);
        ctx.check(FIELD_ERROR_MESSAGE, required());
    }
}
