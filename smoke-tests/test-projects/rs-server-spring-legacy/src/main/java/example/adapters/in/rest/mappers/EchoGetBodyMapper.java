package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoGetBodyRdto;
import example.commons.SerializationUtils;
import example.ports.in.rest.dtos.EchoGetBodyDto;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoGetBodyMapper {

    public EchoGetBodyRdto map2EchoGetBodyRdto(EchoGetBodyDto value) {
        return value == null ? null : map(new EchoGetBodyRdto(), value);
    }

    public EchoGetBodyRdto map(EchoGetBodyRdto result, EchoGetBodyDto value) {
        return result.setPropIntRequired(SerializationUtils.toString(value.getPropIntRequired()))
                .setPropFloat(SerializationUtils.toString(value.getPropFloat()));
    }
}
