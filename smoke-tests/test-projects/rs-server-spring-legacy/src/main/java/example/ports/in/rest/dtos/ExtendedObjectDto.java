package example.ports.in.rest.dtos;

import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.ports.in.rest.dtos.SimpleObjectDto;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Extended object
 * 
 * Simple object for testing
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectDto extends SimpleObjectDto {

    /**
     * enum property with external definition
     */
    @Nullable
    private ReusableEnumEnum eoEnumReusable;
    @Nullable
    private ExtendedObjectDto selfProperty;
}
