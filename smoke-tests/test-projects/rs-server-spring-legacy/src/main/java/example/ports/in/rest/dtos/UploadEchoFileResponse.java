package example.ports.in.rest.dtos;

import example.commons.RestResponse204NoContent;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.in.rest.dtos.ErrorDescriptionDto;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileResponse extends RestResponseBase<UploadEchoFileResponse>
        implements
            RestResponse204NoContent<UploadEchoFileResponse>,
            RestResponseOther<UploadEchoFileResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(204);

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public UploadEchoFileResponse self() {
        return this;
    }
}
