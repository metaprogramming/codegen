package example.ports.in.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponse404NoContent;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.in.rest.dtos.ErrorDescriptionDto;
import java.util.Arrays;
import java.util.Collection;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileResponse extends RestResponseBase<DownloadEchoFileResponse>
        implements
            RestResponse200<DownloadEchoFileResponse, byte[]>,
            RestResponse404NoContent<DownloadEchoFileResponse>,
            RestResponseOther<DownloadEchoFileResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Arrays.asList(200, 404);

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public DownloadEchoFileResponse self() {
        return this;
    }
}
