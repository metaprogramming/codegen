package example.ports.in.rest.dtos;

import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.in.rest.dtos.ErrorDescriptionDto;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public class EchoErrorResponse extends RestResponseBase<EchoErrorResponse>
        implements
            RestResponseOther<EchoErrorResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.emptyList();

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoErrorResponse self() {
        return this;
    }
}
