package example.ports.in.rest.dtos;

import example.commons.EnumValue;
import javax.annotation.processing.Generated;
import lombok.Getter;

/**
 * Example header param with default value
 */
@Generated("pl.metaprogramming.codegen")
public enum DefaultEnumEnum implements EnumValue {

    A1("a1"), A2("a2");

    @Getter
    private final String value;

    DefaultEnumEnum(String value) {
        this.value = value;
    }

    public static DefaultEnumEnum fromValue(String value) {
        return EnumValue.fromValue(value, DefaultEnumEnum.class);
    }
}
