package example.ports.in.rest.dtos;

import java.time.LocalDateTime;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoPostRequest {

    /**
     * The value of the Authorization header should consist of 'type' +
     * 'credentials', where for the approach using the 'type' token should be
     * 'Bearer'.
     */
    @Nonnull
    private String authorizationParam;

    /**
     * Correlates HTTP requests between a client and server.
     */
    @Nonnull
    private String correlationIdParam;
    @Nullable
    private LocalDateTime timestampParam;

    /**
     * Example header param
     */
    @Nullable
    private String inlineHeaderParam;

    /**
     * body param
     */
    @Nonnull
    private EchoBodyDto requestBody;
}
