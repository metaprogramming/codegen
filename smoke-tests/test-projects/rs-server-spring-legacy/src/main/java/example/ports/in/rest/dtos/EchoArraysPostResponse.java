package example.ports.in.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponse204NoContent;
import example.commons.RestResponse400;
import example.commons.RestResponseBase;
import example.commons.ports.in.rest.dtos.ErrorDescriptionDto;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostResponse extends RestResponseBase<EchoArraysPostResponse>
        implements
            RestResponse200<EchoArraysPostResponse, List<EchoArraysBodyDto>>,
            RestResponse204NoContent<EchoArraysPostResponse>,
            RestResponse400<EchoArraysPostResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Arrays.asList(200, 204, 400);

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoArraysPostResponse self() {
        return this;
    }
}
