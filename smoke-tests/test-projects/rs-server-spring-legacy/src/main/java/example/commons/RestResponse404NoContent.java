package example.commons;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public interface RestResponse404NoContent<R> extends RestResponse<R> {

    default boolean is404() {
        return isStatus(404);
    }

    default R set404() {
        return set(404, null);
    }
}
