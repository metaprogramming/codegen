package example.commons.validator;

import javax.annotation.processing.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public enum ErrorCode {

    CUSTOM_FAILED_CODE("custom.failed.code"), INVALID_ANIMAL("invalid-animal");

    @Getter
    private final String value;

    ErrorCode(String value) {
        this.value = value;
    }
}
