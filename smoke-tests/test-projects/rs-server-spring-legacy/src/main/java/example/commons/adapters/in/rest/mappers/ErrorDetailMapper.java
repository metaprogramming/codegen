package example.commons.adapters.in.rest.mappers;

import example.commons.adapters.in.rest.dtos.ErrorDetailRdto;
import example.commons.ports.in.rest.dtos.ErrorDetailDto;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class ErrorDetailMapper {

    public ErrorDetailRdto map2ErrorDetailRdto(ErrorDetailDto value) {
        return value == null ? null : map(new ErrorDetailRdto(), value);
    }

    public ErrorDetailRdto map(ErrorDetailRdto result, ErrorDetailDto value) {
        return result.setField(value.getField()).setCode(value.getCode()).setMessage(value.getMessage());
    }
}
