package example.commons.validator;

import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class AmountChecker implements Checker<String> {

    private static final Pattern PATTERN = Pattern.compile("^(0|([1-9][0-9]{0,}))\\.\\d{2}$");

    @Override
    public void check(ValidationContext<String> ctx) {
        if (!PATTERN.matcher(ctx.getValue()).matches()) {
            ctx.addError("is_not_amount", ctx.getPath() + " is not amount");
        }
    }
}