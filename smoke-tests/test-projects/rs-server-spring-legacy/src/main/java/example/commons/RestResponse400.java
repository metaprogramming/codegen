package example.commons;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public interface RestResponse400<R, T> extends RestResponse<R> {

    default boolean is400() {
        return isStatus(400);
    }

    @SuppressWarnings("unchecked")
    default T get400() {
        if (is400()) {
            return (T) getBody();
        }
        throw new IllegalStateException("Response 400 is not set");
    }

    default R set400(T body) {
        return set(400, body);
    }
}
