package example.commons.adapters.in.rest.mappers;

import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.dtos.ErrorDescriptionRdto;
import example.commons.ports.in.rest.dtos.ErrorDescriptionDto;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class ErrorDescriptionMapper {

    @Autowired
    private ErrorDetailMapper errorDetailMapper;

    public ErrorDescriptionRdto map2ErrorDescriptionRdto(ErrorDescriptionDto value) {
        return value == null ? null : map(new ErrorDescriptionRdto(), value);
    }

    public ErrorDescriptionRdto map(ErrorDescriptionRdto result, ErrorDescriptionDto value) {
        return result.setCode(SerializationUtils.toString(value.getCode())).setMessage(value.getMessage())
                .setErrors(SerializationUtils.transformList(value.getErrors(), errorDetailMapper::map2ErrorDetailRdto));
    }
}
