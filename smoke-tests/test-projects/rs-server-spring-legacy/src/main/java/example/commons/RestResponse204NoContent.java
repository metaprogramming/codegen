package example.commons;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public interface RestResponse204NoContent<R> extends RestResponse<R> {

    default boolean is204() {
        return isStatus(204);
    }

    default R set204() {
        return set(204, null);
    }
}
