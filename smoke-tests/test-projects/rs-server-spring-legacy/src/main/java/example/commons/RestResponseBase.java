package example.commons;

import java.util.Map;
import java.util.TreeMap;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public abstract class RestResponseBase<R> implements RestResponse<R> {

    @Getter
    private final Map<String, String> headers = new TreeMap<>();

    @Getter
    private Integer status;
    @Getter
    private Object body;

    @Override
    public R set(@Nonnull Integer status, Object body) {
        if (this.status != null) {
            throw new IllegalStateException(
                    String.format("Response already initialized with %s - %s", this.status, this.body));
        }
        this.status = status;
        this.body = body;
        return self();
    }
}
