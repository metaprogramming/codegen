package example.commons.validator;

import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class ConditionalChecker<V> implements Checker<V> {

    private final Checker<V> checker;
    private final BoolExp condition;

    @Override
    public void check(ValidationContext<V> ctx) {
        if (condition.evaluate(ctx))
            checker.check(ctx);
    }

    @Override
    public boolean checkNull() {
        return checker.checkNull();
    }
}
