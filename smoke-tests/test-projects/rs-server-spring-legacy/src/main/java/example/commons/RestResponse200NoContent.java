package example.commons;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public interface RestResponse200NoContent<R> extends RestResponse<R> {

    default boolean is200() {
        return isStatus(200);
    }

    default R set200() {
        return set(200, null);
    }
}
