/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.commons.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import example.commons.validator.Checker;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EnumTypeEnum;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import static example.commons.validator.CommonCheckers.maxLength;
import static example.commons.validator.CommonCheckers.minLength;

@Component
@Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT")
public class SimpleObjectCustomValidator implements Checker<SimpleObjectRdto> {

    public void check(ValidationContext<SimpleObjectRdto> ctx) {
        if (ctx.getParent().getValue() instanceof EchoBodyRdto) {
            EchoBodyRdto parent = (EchoBodyRdto) ctx.getParent().getValue();
            if (EnumTypeEnum.V_1.equalsValue(parent.getPropEnum())) {
                ctx.check(SimpleObjectValidator.FIELD_SO_PROP_STRING, minLength(2), maxLength(2));
            }
        }
    }
}
