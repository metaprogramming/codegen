package example.commons;

import java.util.Map;
import java.util.TreeMap;
import javax.annotation.processing.Generated;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public abstract class RestResponseBase<R> implements RestResponse<R> {

    @Getter
    private final Map<String, String> headers = new TreeMap<>();

    @Getter
    private final Integer status;
    @Getter
    private final Object body;
}
