package example.commons.adapters.in.rest.mappers;

import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import example.commons.ports.in.rest.dtos.SimpleObjectDto;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectMapper {

    public SimpleObjectDto map2SimpleObjectDto(SimpleObjectRdto value) {
        return value == null ? null : map(new SimpleObjectDto(), value);
    }

    public SimpleObjectRdto map2SimpleObjectRdto(SimpleObjectDto value) {
        return value == null ? null : map(new SimpleObjectRdto(), value);
    }

    public SimpleObjectDto map(SimpleObjectDto result, SimpleObjectRdto value) {
        return result.setSoPropString(value.getSoPropString())
                .setSoPropDate(SerializationUtils.toLocalDate(value.getSoPropDate()));
    }

    public SimpleObjectRdto map(SimpleObjectRdto result, SimpleObjectDto value) {
        return result.setSoPropString(value.getSoPropString())
                .setSoPropDate(SerializationUtils.toString(value.getSoPropDate()));
    }
}
