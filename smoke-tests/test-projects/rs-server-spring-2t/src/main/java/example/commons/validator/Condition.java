package example.commons.validator;

import javax.annotation.processing.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public enum Condition implements BoolExp {

    NOT_PART_OF_EXTENDED_OBJECT("not-part-of-extended-object");

    @Getter
    private final String value;

    Condition(String value) {
        this.value = value;
    }

    @Override
    public Boolean evaluate(ValidationContext<?> ctx) {
        return ctx.getBean(ConditionResolver.class).resolve(this, ctx);
    }
}
