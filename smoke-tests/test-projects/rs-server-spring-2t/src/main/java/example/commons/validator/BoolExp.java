package example.commons.validator;

import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface BoolExp {

    Boolean evaluate(ValidationContext<?> ctx);
}
