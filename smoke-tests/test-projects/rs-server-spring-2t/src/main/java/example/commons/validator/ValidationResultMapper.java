package example.commons.validator;

import example.commons.adapters.in.rest.dtos.ErrorDescriptionRdto;
import example.commons.adapters.in.rest.dtos.ErrorDetailRdto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.stream.Collectors;

@Component
public class ValidationResultMapper {

    public ResponseEntity<ErrorDescriptionRdto> map(@Nonnull ValidationResult validationResult) {
        return ResponseEntity
                .status(validationResult.getStatus())
                .body(new ErrorDescriptionRdto()
                        .setCode(validationResult.getStatus().toString())
                        .setMessage(validationResult.getMessage())
                        .setErrors(validationResult.getErrors().stream().map(this::toErrorDetail).collect(Collectors.toList()))
                );
    }

    private ErrorDetailRdto toErrorDetail(ValidationError error) {
        return new ErrorDetailRdto()
                .setCode(error.getCode())
                .setField(error.getField())
                .setMessage(error.getMessage());
    }

}
