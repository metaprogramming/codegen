package example.commons.validator;

import javax.annotation.processing.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public enum OperationId implements BoolExp {

    DELETE_FILE("deleteFile"), DOWNLOAD_ECHO_FILE("downloadEchoFile"), ECHO_ARRAYS_POST(
            "echoArraysPost"), ECHO_DATE_ARRAY_GET(
                    "echoDateArrayGet"), ECHO_DEFAULTS_POST("Echo_defaults_POST"), ECHO_EMPTY("echoEmpty"), ECHO_ERROR(
                            "echoError"), ECHO_GET("echo-get"), ECHO_MULTI_CONTENT("echoMultiContent"), ECHO_POST(
                                    "echoPost"), FAKE_OPERATION("fakeOperation"), UPLOAD_ECHO_FILE(
                                            "uploadEchoFile"), UPLOAD_ECHO_FILE_WITH_FORM("uploadEchoFileWithForm");

    @Getter
    private final String value;

    OperationId(String value) {
        this.value = value;
    }

    @Override
    public Boolean evaluate(ValidationContext<?> ctx) {
        return ctx.getBean(getClass()) == this;
    }
}
