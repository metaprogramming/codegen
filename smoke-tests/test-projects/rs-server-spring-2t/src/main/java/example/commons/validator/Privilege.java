package example.commons.validator;

import javax.annotation.processing.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public enum Privilege {

    READ("read"), WRITE("write");

    @Getter
    private final String value;

    Privilege(String value) {
        this.value = value;
    }
}
