package example.commons.adapters.in.rest.mappers;

import example.commons.adapters.in.rest.dtos.ErrorItemRdto;
import example.commons.ports.in.rest.dtos.ErrorItemDto;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class ErrorItemMapper {

    public ErrorItemRdto map2ErrorItemRdto(ErrorItemDto value) {
        return value == null ? null : map(new ErrorItemRdto(), value);
    }

    public ErrorItemRdto map(ErrorItemRdto result, ErrorItemDto value) {
        return result.setCode(value.getCode());
    }
}
