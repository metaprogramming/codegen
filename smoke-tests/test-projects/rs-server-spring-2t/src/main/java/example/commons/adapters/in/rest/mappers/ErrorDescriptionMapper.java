package example.commons.adapters.in.rest.mappers;

import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.dtos.ErrorDescriptionRdto;
import example.commons.ports.in.rest.dtos.ErrorDescriptionDto;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class ErrorDescriptionMapper {

    private final ErrorDetailMapper errorDetailMapper;

    public ErrorDescriptionRdto map2ErrorDescriptionRdto(ErrorDescriptionDto value) {
        return value == null ? null : map(new ErrorDescriptionRdto(), value);
    }

    public ErrorDescriptionRdto map(ErrorDescriptionRdto result, ErrorDescriptionDto value) {
        return result.setCode(SerializationUtils.toString(value.getCode())).setMessage(value.getMessage())
                .setErrors(SerializationUtils.transformList(value.getErrors(), errorDetailMapper::map2ErrorDetailRdto));
    }
}
