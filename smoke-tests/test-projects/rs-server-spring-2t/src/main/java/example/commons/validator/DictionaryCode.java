package example.commons.validator;

import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public enum DictionaryCode {

    ANIMALS, COLORS;
}
