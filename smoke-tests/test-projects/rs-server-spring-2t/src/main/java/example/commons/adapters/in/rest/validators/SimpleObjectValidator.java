package example.commons.adapters.in.rest.validators;

import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import example.commons.validator.Checker;
import example.commons.validator.Condition;
import example.commons.validator.ConditionResolver;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectValidator implements Checker<SimpleObjectRdto> {

    public static final Field<SimpleObjectRdto, String> FIELD_SO_PROP_STRING = new Field<>("so_prop_string",
            SimpleObjectRdto::getSoPropString);
    public static final Field<SimpleObjectRdto, String> FIELD_SO_PROP_DATE = new Field<>("so_prop_date",
            SimpleObjectRdto::getSoPropDate);

    private final ConditionResolver conditionResolver;

    public void check(ValidationContext<SimpleObjectRdto> ctx) {
        ctx.setBean(conditionResolver);
        ctx.check(FIELD_SO_PROP_STRING, required());
        ctx.check(FIELD_SO_PROP_DATE, ISO_DATE, checkIf(required(), or(OperationId.ECHO_ARRAYS_POST,
                and(or(OperationId.FAKE_OPERATION, OperationId.ECHO_POST), Condition.NOT_PART_OF_EXTENDED_OBJECT))));
    }
}
