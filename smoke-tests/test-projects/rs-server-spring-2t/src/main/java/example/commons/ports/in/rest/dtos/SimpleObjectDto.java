package example.commons.ports.in.rest.dtos;

import java.time.LocalDate;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Simple object
 * 
 * Simple object for testing
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectDto {

    @Nonnull
    private String soPropString;
    @Nullable
    private LocalDate soPropDate;
}
