package example.commons.ports.in.rest.dtos;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ErrorDetailDto {

    @Nullable
    private String field;
    @Nonnull
    private String code;
    @Nonnull
    private String message;
}
