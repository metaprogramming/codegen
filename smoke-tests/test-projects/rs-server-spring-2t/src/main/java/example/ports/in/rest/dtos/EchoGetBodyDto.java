package example.ports.in.rest.dtos;

import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetBodyDto {

    @Nullable
    private Long propIntRequired;
    @Nullable
    private Float propFloat;
    @Nullable
    private DefaultEnumEnum propEnum;
}
