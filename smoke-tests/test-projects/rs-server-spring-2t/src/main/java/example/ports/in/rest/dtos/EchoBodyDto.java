package example.ports.in.rest.dtos;

import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.ports.in.rest.dtos.SimpleObjectDto;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Echo object title
 * 
 * Echo object description
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoBodyDto {

    @Nullable
    private Integer propInt;
    @Nullable
    private Integer propIntSecond;
    @Nonnull
    private Long propIntRequired;
    @Nullable
    private Float propFloat;
    @Nullable
    private Double propDouble;
    @Nullable
    private String propAmount;
    @Nullable
    private BigDecimal propAmountNumber;
    @Nullable
    private String propString;
    @Nullable
    private String propStringPattern;
    @Nullable
    private String propDefault;
    @Nullable
    private LocalDate propDate;
    @Nullable
    private LocalDate propDateSecond;
    @Nullable
    private LocalDateTime propDateTime;
    @Nullable
    private byte[] propBase64;
    @Nullable
    private Boolean propBoolean;

    /**
     * object property
     */
    @Nullable
    private SimpleObjectDto propObject;
    @Nullable
    private Map<String, Object> propObjectAny;
    @Nullable
    private ExtendedObjectDto propObjectExtended;

    /**
     * enum property with external definition
     */
    @Nullable
    private ReusableEnumEnum propEnumReusable;

    /**
     * enum property
     */
    @Nullable
    private EnumTypeEnum propEnum;
}
