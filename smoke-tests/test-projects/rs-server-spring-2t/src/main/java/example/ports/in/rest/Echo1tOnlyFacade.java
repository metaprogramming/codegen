package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoMultiContentRequest;
import example.ports.in.rest.dtos.EchoMultiContentResponse;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface Echo1tOnlyFacade {

    EchoMultiContentResponse echoMultiContent(@Nonnull EchoMultiContentRequest request);
}
