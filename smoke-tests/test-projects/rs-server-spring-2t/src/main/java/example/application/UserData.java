package example.application;

import example.commons.validator.Privilege;
import lombok.Value;

import java.math.BigDecimal;
import java.util.List;

@Value
public class UserData {
    BigDecimal minAmount;
    BigDecimal maxAmount;
    List<Privilege> credentials;
}
