/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.application;

import example.commons.validator.Privilege;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;

@Component
public class UserService {
    public UserData loadUserData(String token) {
        return "123".equals(token)
                ? new UserData(BigDecimal.valueOf(5), BigDecimal.valueOf(1000), Collections.singletonList(Privilege.WRITE))
                : new UserData(BigDecimal.valueOf(1), BigDecimal.valueOf(200), Collections.singletonList(Privilege.WRITE));
    }
}
