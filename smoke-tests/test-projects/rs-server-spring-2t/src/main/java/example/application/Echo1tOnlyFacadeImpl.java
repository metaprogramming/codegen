package example.application;

import example.ports.in.rest.Echo1tOnlyFacade;
import example.ports.in.rest.dtos.EchoMultiContentRequest;
import example.ports.in.rest.dtos.EchoMultiContentResponse;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class Echo1tOnlyFacadeImpl implements Echo1tOnlyFacade {

    @Override
    public EchoMultiContentResponse echoMultiContent(@Nonnull EchoMultiContentRequest request) {
        return null;
    }
}
