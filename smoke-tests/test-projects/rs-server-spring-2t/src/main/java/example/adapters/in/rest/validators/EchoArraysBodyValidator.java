package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoArraysBodyRdto;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import example.commons.adapters.in.rest.validators.SimpleObjectValidator;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.validator.Checker;
import example.commons.validator.DictionaryChecker;
import example.commons.validator.Field;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EnumTypeEnum;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.commons.validator.DictionaryCode.*;
import static example.commons.validator.ErrorCode.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysBodyValidator implements Checker<EchoArraysBodyRdto> {

    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_INT_LIST = new Field<>("prop_int_list",
            EchoArraysBodyRdto::getPropIntList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_FLOAT_LIST = new Field<>("prop_float_list",
            EchoArraysBodyRdto::getPropFloatList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_DOUBLE_LIST = new Field<>("prop_double_list",
            EchoArraysBodyRdto::getPropDoubleList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_AMOUNT_LIST = new Field<>("prop_amount_list",
            EchoArraysBodyRdto::getPropAmountList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_STRING_LIST = new Field<>("prop_string_list",
            EchoArraysBodyRdto::getPropStringList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_DATE_LIST = new Field<>("prop_date_list",
            EchoArraysBodyRdto::getPropDateList);
    public static final Field<EchoArraysBodyRdto, List<List<String>>> FIELD_PROP_DATE_TIME_LIST_OF_LIST = new Field<>(
            "prop_date_time_list_of_list", EchoArraysBodyRdto::getPropDateTimeListOfList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_BOOLEAN_LIST = new Field<>(
            "prop_boolean_list", EchoArraysBodyRdto::getPropBooleanList);
    public static final Field<EchoArraysBodyRdto, List<SimpleObjectRdto>> FIELD_PROP_OBJECT_LIST = new Field<>(
            "prop_object_list", EchoArraysBodyRdto::getPropObjectList);
    public static final Field<EchoArraysBodyRdto, List<List<SimpleObjectRdto>>> FIELD_PROP_OBJECT_LIST_OF_LIST = new Field<>(
            "prop_object_list_of_list", EchoArraysBodyRdto::getPropObjectListOfList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_ENUM_REUSABLE_LIST = new Field<>(
            "prop_enum_reusable_list", EchoArraysBodyRdto::getPropEnumReusableList);
    public static final Field<EchoArraysBodyRdto, List<String>> FIELD_PROP_ENUM_LIST = new Field<>("prop_enum_list",
            EchoArraysBodyRdto::getPropEnumList);
    public static final Field<EchoArraysBodyRdto, Map<String, String>> FIELD_PROP_MAP_OF_INT = new Field<>(
            "prop_map_of_int", EchoArraysBodyRdto::getPropMapOfInt);
    public static final Field<EchoArraysBodyRdto, Map<String, SimpleObjectRdto>> FIELD_PROP_MAP_OF_OBJECT = new Field<>(
            "prop_map_of_object", EchoArraysBodyRdto::getPropMapOfObject);
    public static final Field<EchoArraysBodyRdto, Map<String, List<SimpleObjectRdto>>> FIELD_PROP_MAP_OF_LIST_OF_OBJECT = new Field<>(
            "prop_map_of_list_of_object", EchoArraysBodyRdto::getPropMapOfListOfObject);

    private static final Checker<String> FIELD_PROP_AMOUNT_LIST_PATTERN = matches(
            Pattern.compile("^(0|([1-9][0-9]{0,}))\\.\\d{2}$"), "invalid_amount");

    private final DictionaryChecker dictionaryChecker;
    private final SimpleObjectValidator simpleObjectValidator;
    @Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT")
    private final Checker<SimpleObjectRdto> simpleObjectCustomConstraint;

    public void check(ValidationContext<EchoArraysBodyRdto> ctx) {
        ctx.check(FIELD_PROP_INT_LIST, minItems(1), items(INT32, gt("-2", Integer::valueOf)));
        ctx.check(FIELD_PROP_FLOAT_LIST, items(FLOAT, le("101", Float::valueOf)));
        ctx.check(FIELD_PROP_DOUBLE_LIST, required(), unique(), minItems(2), maxItems(4),
                items(DOUBLE, ge("0.01", Double::valueOf), le("1000000000000000", Double::valueOf)));
        ctx.check(FIELD_PROP_AMOUNT_LIST, items(FIELD_PROP_AMOUNT_LIST_PATTERN));
        ctx.check(FIELD_PROP_STRING_LIST,
                items(dictionaryChecker.check(ANIMALS).withError(INVALID_ANIMAL), minLength(5), maxLength(10)));
        ctx.check(FIELD_PROP_DATE_LIST, items(ISO_DATE));
        ctx.check(FIELD_PROP_DATE_TIME_LIST_OF_LIST, minItems(1), items(minItems(2), items(ISO_DATE_TIME)));
        ctx.check(FIELD_PROP_BOOLEAN_LIST, items(BOOLEAN));
        ctx.check(FIELD_PROP_OBJECT_LIST,
                items(simpleObjectValidator, simpleObjectCustomConstraint.withError(CUSTOM_FAILED_CODE)));
        ctx.check(FIELD_PROP_OBJECT_LIST_OF_LIST, items(items(simpleObjectValidator, simpleObjectCustomConstraint)));
        ctx.check(FIELD_PROP_ENUM_REUSABLE_LIST, items(allow(ReusableEnumEnum.values())));
        ctx.check(FIELD_PROP_ENUM_LIST, items(allow(EnumTypeEnum.values())));
        ctx.check(FIELD_PROP_MAP_OF_INT, mapValues(INT32, le("100", Integer::valueOf)));
        ctx.check(FIELD_PROP_MAP_OF_OBJECT, mapValues(simpleObjectValidator));
        ctx.check(FIELD_PROP_MAP_OF_LIST_OF_OBJECT, mapValues(items(simpleObjectValidator)));
    }
}
