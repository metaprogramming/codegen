package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoArraysBodyRdto;
import example.adapters.in.rest.dtos.EchoArraysPostRrequest;
import example.adapters.in.rest.mappers.EchoArraysPostRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import java.util.List;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostController {

    private final EchoArraysPostRequestMapper echoArraysPostRequestMapper;
    private final EchoArraysPostValidator echoArraysPostValidator;
    private final EchoFacade echoFacade;
    private final EchoArraysPostResponseMapper echoArraysPostResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    @PostMapping(value = "/api/v1/echo-arrays", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity echoArraysPost(
            @RequestHeader(value = "Authorization", required = false) String authorizationParam,
            @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam,
            @RequestBody List<EchoArraysBodyRdto> body) {
        EchoArraysPostRrequest request = echoArraysPostRequestMapper.map2EchoArraysPostRrequest(authorizationParam,
                inlineHeaderParam, body);
        ValidationResult validationResult = echoArraysPostValidator.validate(request);
        return validationResult.isValid()
                ? echoArraysPostResponseMapper
                        .map(echoFacade.echoArraysPost(echoArraysPostRequestMapper.map2EchoArraysPostRequest(request)))
                : validationResultMapper.map(validationResult);
    }
}
