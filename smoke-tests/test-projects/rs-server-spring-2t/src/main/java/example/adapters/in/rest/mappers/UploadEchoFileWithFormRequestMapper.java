package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.UploadEchoFileWithFormRrequest;
import example.commons.SerializationUtils;
import example.ports.in.rest.dtos.UploadEchoFileWithFormRequest;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormRequestMapper {

    public UploadEchoFileWithFormRrequest map2UploadEchoFileWithFormRrequest(String id, MultipartFile file) {
        return new UploadEchoFileWithFormRrequest().setId(id).setFile(file.getResource());
    }

    public UploadEchoFileWithFormRequest map2UploadEchoFileWithFormRequest(UploadEchoFileWithFormRrequest value) {
        return value == null
                ? null
                : new UploadEchoFileWithFormRequest().setId(SerializationUtils.toLong(value.getId()))
                        .setFile(SerializationUtils.toBytes(value.getFile()));
    }
}
