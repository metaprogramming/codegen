package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.adapters.in.rest.dtos.EchoPostRrequest;
import example.adapters.in.rest.mappers.EchoPostRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoPostController {

    private final EchoPostRequestMapper echoPostRequestMapper;
    private final EchoPostValidator echoPostValidator;
    private final EchoFacade echoFacade;
    private final EchoPostResponseMapper echoPostResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    @PostMapping(value = "/api/v1/echo", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity echoPost(@RequestHeader(value = "Authorization", required = false) String authorizationParam,
            @RequestHeader(value = "X-Correlation-ID", required = false) String correlationIdParam,
            @RequestHeader(value = "timestamp", required = false) String timestampParam,
            @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam,
            @RequestBody EchoBodyRdto requestBody) {
        EchoPostRrequest request = echoPostRequestMapper.map2EchoPostRrequest(authorizationParam, correlationIdParam,
                timestampParam, inlineHeaderParam, requestBody);
        ValidationResult validationResult = echoPostValidator.validate(request);
        return validationResult.isValid()
                ? echoPostResponseMapper.map(echoFacade.echoPost(echoPostRequestMapper.map2EchoPostRequest(request)))
                : validationResultMapper.map(validationResult);
    }
}
