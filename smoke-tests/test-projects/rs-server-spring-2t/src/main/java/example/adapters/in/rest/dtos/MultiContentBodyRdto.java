package example.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import java.util.List;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class MultiContentBodyRdto {

    @JsonProperty("response_content_type")
    private String responseContentType;
    @JsonProperty("prop_int")
    @JsonRawValue
    private String propInt;
    @JsonProperty("prop_int2")
    @JsonRawValue
    private String propInt2;
    @JsonProperty("prop_date")
    private String propDate;
    @JsonProperty("prop_list")
    private List<String> propList;
}
