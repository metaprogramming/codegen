package example.adapters.in.rest.dtos;

import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoPostRrequest {

    private String authorizationParam;
    private String correlationIdParam;
    private String timestampParam;
    private String inlineHeaderParam;
    private EchoBodyRdto requestBody;
}
