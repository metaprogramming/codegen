package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoDateArrayGetRrequest;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetValidator extends Validator<EchoDateArrayGetRrequest> {

    public static final Field<EchoDateArrayGetRrequest, List<String>> FIELD_DATE_ARRAY = new Field<>(
            "date_array (QUERY parameter)", EchoDateArrayGetRrequest::getDateArray);

    public void check(ValidationContext<EchoDateArrayGetRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_DATE_ARRAY_GET);
        ctx.check(FIELD_DATE_ARRAY, items(ISO_DATE));
    }
}
