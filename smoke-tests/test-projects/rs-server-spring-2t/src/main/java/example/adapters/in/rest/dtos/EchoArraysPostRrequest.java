package example.adapters.in.rest.dtos;

import java.util.List;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostRrequest {

    private String authorizationParam;
    private String inlineHeaderParam;
    private List<EchoArraysBodyRdto> body;
}
