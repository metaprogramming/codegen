package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.UploadEchoFileRrequest;
import example.adapters.in.rest.mappers.UploadEchoFileRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileController {

    private final UploadEchoFileRequestMapper uploadEchoFileRequestMapper;
    private final UploadEchoFileValidator uploadEchoFileValidator;
    private final EchoFacade echoFacade;
    private final UploadEchoFileResponseMapper uploadEchoFileResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    /**
     * upload file
     */
    @PostMapping(value = "/api/v1/echo-file/{id}", produces = {"application/json"}, consumes = {
            "application/octet-stream"})
    public ResponseEntity uploadEchoFile(@PathVariable(required = false) String id, @RequestBody Resource body) {
        UploadEchoFileRrequest request = uploadEchoFileRequestMapper.map2UploadEchoFileRrequest(id, body);
        ValidationResult validationResult = uploadEchoFileValidator.validate(request);
        return validationResult.isValid()
                ? uploadEchoFileResponseMapper
                        .map(echoFacade.uploadEchoFile(uploadEchoFileRequestMapper.map2UploadEchoFileRequest(request)))
                : validationResultMapper.map(validationResult);
    }
}
