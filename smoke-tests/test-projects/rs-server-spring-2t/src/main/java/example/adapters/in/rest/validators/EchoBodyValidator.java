package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.adapters.in.rest.dtos.ExtendedObjectRdto;
import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.dtos.SimpleObjectRdto;
import example.commons.adapters.in.rest.validators.SimpleObjectValidator;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.validator.AmountChecker;
import example.commons.validator.Checker;
import example.commons.validator.Checkers;
import example.commons.validator.DictionaryChecker;
import example.commons.validator.Field;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.EnumTypeEnum;
import java.math.BigDecimal;
import java.util.Map;
import java.util.regex.Pattern;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.commons.validator.DictionaryCode.*;
import static example.commons.validator.ErrorCode.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoBodyValidator implements Checker<EchoBodyRdto> {

    public static final Field<EchoBodyRdto, String> FIELD_PROP_INT = new Field<>("prop_int", EchoBodyRdto::getPropInt);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_INT_SECOND = new Field<>("prop_int_second",
            EchoBodyRdto::getPropIntSecond);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_INT_REQUIRED = new Field<>("prop_int_required",
            EchoBodyRdto::getPropIntRequired);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_FLOAT = new Field<>("prop_float",
            EchoBodyRdto::getPropFloat);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_DOUBLE = new Field<>("prop_double",
            EchoBodyRdto::getPropDouble);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_AMOUNT = new Field<>("prop_amount",
            EchoBodyRdto::getPropAmount);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_AMOUNT_NUMBER = new Field<>("prop_amount_number",
            EchoBodyRdto::getPropAmountNumber);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_STRING = new Field<>("prop_string",
            EchoBodyRdto::getPropString);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_STRING_PATTERN = new Field<>("prop_string_pattern",
            EchoBodyRdto::getPropStringPattern);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_DEFAULT = new Field<>("prop_default",
            EchoBodyRdto::getPropDefault);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_DATE = new Field<>("prop_date",
            EchoBodyRdto::getPropDate);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_DATE_SECOND = new Field<>("prop_date_second",
            EchoBodyRdto::getPropDateSecond);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_DATE_TIME = new Field<>("prop_date_time",
            EchoBodyRdto::getPropDateTime);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_BASE64 = new Field<>("prop_base64",
            EchoBodyRdto::getPropBase64);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_BOOLEAN = new Field<>("prop_boolean",
            EchoBodyRdto::getPropBoolean);
    public static final Field<EchoBodyRdto, SimpleObjectRdto> FIELD_PROP_OBJECT = new Field<>("prop_object",
            EchoBodyRdto::getPropObject);
    public static final Field<EchoBodyRdto, Map<String, Object>> FIELD_PROP_OBJECT_ANY = new Field<>("prop_object_any",
            EchoBodyRdto::getPropObjectAny);
    public static final Field<EchoBodyRdto, ExtendedObjectRdto> FIELD_PROP_OBJECT_EXTENDED = new Field<>(
            "prop_object_extended", EchoBodyRdto::getPropObjectExtended);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_ENUM_REUSABLE = new Field<>("prop_enum_reusable",
            EchoBodyRdto::getPropEnumReusable);
    public static final Field<EchoBodyRdto, String> FIELD_PROP_ENUM = new Field<>("prop_enum",
            EchoBodyRdto::getPropEnum);

    private static final Checker<String> FIELD_PROP_AMOUNT_PATTERN = matches(
            Pattern.compile("^(0|([1-9][0-9]{0,}))\\.\\d{2}$"));
    private static final Checker<String> FIELD_PROP_STRING_PATTERN_PATTERN = matches(
            Pattern.compile("^[\\-\\\".',:;/\\\\!@#$%^&*()+_?|><=]{2,19}$"), "custom_error_code");

    private final UserDataValidationBean userDataValidationBean;
    private final AmountChecker amountChecker;
    private final DictionaryChecker dictionaryChecker;
    private final SimpleObjectValidator simpleObjectValidator;
    @Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT")
    private final Checker<SimpleObjectRdto> simpleObjectCustomConstraint;
    private final ExtendedObjectValidator extendedObjectValidator;

    public void check(ValidationContext<EchoBodyRdto> ctx) {
        ctx.check(FIELD_PROP_INT, INT32);
        ctx.check(FIELD_PROP_INT_SECOND, INT32);
        ctx.check(FIELD_PROP_INT_REQUIRED, required(), INT64, ge("-1", Long::valueOf));
        ctx.check(FIELD_PROP_FLOAT, FLOAT, le("101", Float::valueOf));
        ctx.check(FIELD_PROP_DOUBLE, DOUBLE, gt("0", Double::valueOf), lt("10000", Double::valueOf));
        ctx.check(FIELD_PROP_AMOUNT, FIELD_PROP_AMOUNT_PATTERN, userDataValidationBean::checkAmountByUser,
                Checkers.AMOUNT_SCALE_CHECKER);
        ctx.check(FIELD_PROP_AMOUNT_NUMBER, amountChecker, ge("0.02", BigDecimal::new),
                le("9999999999.99", BigDecimal::new));
        ctx.check(FIELD_PROP_STRING, minLength(5), maxLength(10), dictionaryChecker.check(COLORS));
        ctx.check(FIELD_PROP_STRING_PATTERN, FIELD_PROP_STRING_PATTERN_PATTERN);
        ctx.check(FIELD_PROP_DEFAULT, minLength(5));
        ctx.check(FIELD_PROP_DATE, ISO_DATE);
        ctx.check(FIELD_PROP_DATE_SECOND, ISO_DATE);
        ctx.check(FIELD_PROP_DATE_TIME, ISO_DATE_TIME);
        ctx.check(FIELD_PROP_BASE64, BASE64);
        ctx.check(FIELD_PROP_BOOLEAN, BOOLEAN);
        ctx.check(FIELD_PROP_OBJECT, simpleObjectValidator, simpleObjectCustomConstraint);
        ctx.check(FIELD_PROP_OBJECT_EXTENDED, extendedObjectValidator);
        ctx.check(FIELD_PROP_ENUM_REUSABLE, allow(ReusableEnumEnum.values()));
        ctx.check(FIELD_PROP_ENUM, allow(EnumTypeEnum.values()));
        ctx.check(lt(FIELD_PROP_INT, FIELD_PROP_INT_SECOND, SerializationUtils::toInteger)
                .withError(COMPARE_PROP_INT_FAILED));
        ctx.check(ge(FIELD_PROP_DATE, FIELD_PROP_DATE_SECOND, SerializationUtils::toLocalDate));
    }
}
