package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoEmptyRrequest;
import example.ports.in.rest.dtos.EchoEmptyRequest;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyRequestMapper {

    public EchoEmptyRequest map2EchoEmptyRequest(EchoEmptyRrequest value) {
        return value == null ? null : new EchoEmptyRequest();
    }
}
