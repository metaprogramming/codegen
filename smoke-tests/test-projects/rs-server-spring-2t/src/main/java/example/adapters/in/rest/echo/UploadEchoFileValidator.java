package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.UploadEchoFileRrequest;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.processing.Generated;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileValidator extends Validator<UploadEchoFileRrequest> {

    public static final Field<UploadEchoFileRrequest, String> FIELD_ID = new Field<>("id (PATH parameter)",
            UploadEchoFileRrequest::getId);
    public static final Field<UploadEchoFileRrequest, Resource> FIELD_BODY = new Field<>("body",
            UploadEchoFileRrequest::getBody);

    public void check(ValidationContext<UploadEchoFileRrequest> ctx) {
        ctx.setBean(OperationId.UPLOAD_ECHO_FILE);
        ctx.check(FIELD_ID, required(), INT64);
        ctx.checkRoot(FIELD_BODY, required());
    }
}
