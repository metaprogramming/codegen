package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoBodyRdto;
import example.commons.SerializationUtils;
import example.commons.adapters.in.rest.mappers.SimpleObjectMapper;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EnumTypeEnum;
import java.util.Optional;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoBodyMapper {

    private final SimpleObjectMapper simpleObjectMapper;
    private final ExtendedObjectMapper extendedObjectMapper;

    public EchoBodyDto map2EchoBodyDto(EchoBodyRdto value) {
        return value == null ? null : map(new EchoBodyDto(), value);
    }

    public EchoBodyRdto map2EchoBodyRdto(EchoBodyDto value) {
        return value == null ? null : map(new EchoBodyRdto(), value);
    }

    public EchoBodyDto map(EchoBodyDto result, EchoBodyRdto value) {
        return result.setPropInt(SerializationUtils.toInteger(value.getPropInt()))
                .setPropIntSecond(SerializationUtils.toInteger(value.getPropIntSecond()))
                .setPropIntRequired(SerializationUtils.toLong(value.getPropIntRequired()))
                .setPropFloat(SerializationUtils.toFloat(value.getPropFloat()))
                .setPropDouble(SerializationUtils.toDouble(value.getPropDouble())).setPropAmount(value.getPropAmount())
                .setPropAmountNumber(SerializationUtils.toBigDecimal(value.getPropAmountNumber()))
                .setPropString(value.getPropString()).setPropStringPattern(value.getPropStringPattern())
                .setPropDefault(value.getPropDefault()).setPropDate(SerializationUtils.toLocalDate(value.getPropDate()))
                .setPropDateSecond(SerializationUtils.toLocalDate(value.getPropDateSecond()))
                .setPropDateTime(SerializationUtils.toLocalDateTime(value.getPropDateTime()))
                .setPropBase64(SerializationUtils.toBytes(value.getPropBase64()))
                .setPropBoolean(SerializationUtils.toBoolean(value.getPropBoolean()))
                .setPropObject(simpleObjectMapper.map2SimpleObjectDto(value.getPropObject()))
                .setPropObjectAny(SerializationUtils.transformMap(value.getPropObjectAny(), v -> v))
                .setPropObjectExtended(extendedObjectMapper.map2ExtendedObjectDto(value.getPropObjectExtended()))
                .setPropEnumReusable(ReusableEnumEnum.fromValue(value.getPropEnumReusable()))
                .setPropEnum(EnumTypeEnum.fromValue(value.getPropEnum()));
    }

    public EchoBodyRdto map(EchoBodyRdto result, EchoBodyDto value) {
        return result.setPropInt(SerializationUtils.toString(value.getPropInt()))
                .setPropIntSecond(SerializationUtils.toString(value.getPropIntSecond()))
                .setPropIntRequired(SerializationUtils.toString(value.getPropIntRequired()))
                .setPropFloat(SerializationUtils.toString(value.getPropFloat()))
                .setPropDouble(SerializationUtils.toString(value.getPropDouble())).setPropAmount(value.getPropAmount())
                .setPropAmountNumber(SerializationUtils.toString(value.getPropAmountNumber()))
                .setPropString(value.getPropString()).setPropStringPattern(value.getPropStringPattern())
                .setPropDefault(value.getPropDefault()).setPropDate(SerializationUtils.toString(value.getPropDate()))
                .setPropDateSecond(SerializationUtils.toString(value.getPropDateSecond()))
                .setPropDateTime(SerializationUtils.toString(value.getPropDateTime()))
                .setPropBase64(SerializationUtils.toString(value.getPropBase64()))
                .setPropBoolean(SerializationUtils.toString(value.getPropBoolean()))
                .setPropObject(simpleObjectMapper.map2SimpleObjectRdto(value.getPropObject()))
                .setPropObjectAny(SerializationUtils.transformMap(value.getPropObjectAny(), v -> v))
                .setPropObjectExtended(extendedObjectMapper.map2ExtendedObjectRdto(value.getPropObjectExtended()))
                .setPropEnumReusable(
                        Optional.ofNullable(value.getPropEnumReusable()).map(ReusableEnumEnum::getValue).orElse(null))
                .setPropEnum(Optional.ofNullable(value.getPropEnum()).map(EnumTypeEnum::getValue).orElse(null));
    }
}
