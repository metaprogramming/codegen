/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.ExtendedObjectRdto;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.validator.Checker;
import example.commons.validator.CommonCheckers;
import example.commons.validator.ValidationContext;
import org.springframework.stereotype.Component;

@Component
public class ExtendedObjectChecker implements Checker<ExtendedObjectRdto> {

    @Override
    public void check(ValidationContext<ExtendedObjectRdto> context) {
        if (ReusableEnumEnum.V_3.equalsValue(context.getValue().getEoEnumReusable())) {
            ValidationContext<String> child = context.getChild(ExtendedObjectValidator.FIELD_EO_ENUM_REUSABLE);
            CommonCheckers.writeError(context, String.format("we don't want 3 in %s", child.getPath()), child.getPath(), "custom_code2");
        }
    }
}
