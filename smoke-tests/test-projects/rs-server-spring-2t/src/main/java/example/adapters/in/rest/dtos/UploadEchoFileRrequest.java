package example.adapters.in.rest.dtos;

import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.core.io.Resource;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileRrequest {

    private String id;
    private Resource body;
}
