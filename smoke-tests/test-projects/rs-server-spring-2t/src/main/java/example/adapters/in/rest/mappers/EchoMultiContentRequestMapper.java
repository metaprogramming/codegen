package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.EchoMultiContentRrequest;
import example.adapters.in.rest.dtos.MultiContentBodyRdto;
import example.ports.in.rest.dtos.EchoMultiContentRequest;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentRequestMapper {

    private final MultiContentBodyMapper multiContentBodyMapper;

    public EchoMultiContentRrequest map2EchoMultiContentRrequest(MultiContentBodyRdto body) {
        return new EchoMultiContentRrequest().setBody(body);
    }

    public EchoMultiContentRequest map2EchoMultiContentRequest(EchoMultiContentRrequest value) {
        return value == null
                ? null
                : new EchoMultiContentRequest()
                        .setBody(multiContentBodyMapper.map2MultiContentBodyDto(value.getBody()));
    }
}
