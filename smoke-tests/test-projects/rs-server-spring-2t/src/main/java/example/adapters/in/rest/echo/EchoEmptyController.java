package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoEmptyRrequest;
import example.adapters.in.rest.mappers.EchoEmptyRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyController {

    private final EchoEmptyValidator echoEmptyValidator;
    private final EchoEmptyRequestMapper echoEmptyRequestMapper;
    private final EchoFacade echoFacade;
    private final EchoEmptyResponseMapper echoEmptyResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    @GetMapping("/api/v1/echo-empty")
    public ResponseEntity echoEmpty() {
        EchoEmptyRrequest request = new EchoEmptyRrequest();
        ValidationResult validationResult = echoEmptyValidator.validate(request);
        return validationResult.isValid()
                ? echoEmptyResponseMapper
                        .map(echoFacade.echoEmpty(echoEmptyRequestMapper.map2EchoEmptyRequest(request)))
                : validationResultMapper.map(validationResult);
    }
}
