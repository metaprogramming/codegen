package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.MultiContentBodyRdto;
import example.commons.SerializationUtils;
import example.commons.validator.Checker;
import example.commons.validator.Field;
import example.commons.validator.ValidationContext;
import example.ports.in.rest.dtos.ResponseContentTypeEnum;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class MultiContentBodyValidator implements Checker<MultiContentBodyRdto> {

    public static final Field<MultiContentBodyRdto, String> FIELD_RESPONSE_CONTENT_TYPE = new Field<>(
            "response_content_type", MultiContentBodyRdto::getResponseContentType);
    public static final Field<MultiContentBodyRdto, String> FIELD_PROP_INT = new Field<>("prop_int",
            MultiContentBodyRdto::getPropInt);
    public static final Field<MultiContentBodyRdto, String> FIELD_PROP_INT2 = new Field<>("prop_int2",
            MultiContentBodyRdto::getPropInt2);
    public static final Field<MultiContentBodyRdto, String> FIELD_PROP_DATE = new Field<>("prop_date",
            MultiContentBodyRdto::getPropDate);
    public static final Field<MultiContentBodyRdto, List<String>> FIELD_PROP_LIST = new Field<>("prop_list",
            MultiContentBodyRdto::getPropList);

    public void check(ValidationContext<MultiContentBodyRdto> ctx) {
        ctx.check(FIELD_RESPONSE_CONTENT_TYPE, required(), allow(ResponseContentTypeEnum.values()));
        ctx.check(FIELD_PROP_INT, INT32);
        ctx.check(FIELD_PROP_INT2, INT32);
        ctx.check(FIELD_PROP_DATE, ISO_DATE);
        ctx.check(le(FIELD_PROP_INT, FIELD_PROP_INT2, SerializationUtils::toInteger));
    }
}
