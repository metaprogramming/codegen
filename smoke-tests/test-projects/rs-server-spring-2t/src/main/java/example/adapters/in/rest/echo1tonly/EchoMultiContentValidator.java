package example.adapters.in.rest.echo1tonly;

import example.adapters.in.rest.dtos.EchoMultiContentRrequest;
import example.adapters.in.rest.dtos.MultiContentBodyRdto;
import example.adapters.in.rest.validators.MultiContentBodyValidator;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentValidator extends Validator<EchoMultiContentRrequest> {

    public static final Field<EchoMultiContentRrequest, MultiContentBodyRdto> FIELD_BODY = new Field<>("body",
            EchoMultiContentRrequest::getBody);

    private final MultiContentBodyValidator multiContentBodyValidator;

    public void check(ValidationContext<EchoMultiContentRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_MULTI_CONTENT);
        ctx.checkRoot(FIELD_BODY, required(), multiContentBodyValidator);
    }
}
