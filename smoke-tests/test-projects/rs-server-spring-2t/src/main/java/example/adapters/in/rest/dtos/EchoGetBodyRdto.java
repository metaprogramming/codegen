package example.adapters.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetBodyRdto {

    @JsonProperty("prop_int_required")
    @JsonRawValue
    private String propIntRequired;
    @JsonProperty("prop_float")
    @JsonRawValue
    private String propFloat;
    @JsonProperty("prop_enum")
    private String propEnum;
}
