package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.UploadEchoFileRrequest;
import example.commons.SerializationUtils;
import example.ports.in.rest.dtos.UploadEchoFileRequest;
import javax.annotation.processing.Generated;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileRequestMapper {

    public UploadEchoFileRrequest map2UploadEchoFileRrequest(String id, Resource body) {
        return new UploadEchoFileRrequest().setId(id).setBody(body);
    }

    public UploadEchoFileRequest map2UploadEchoFileRequest(UploadEchoFileRrequest value) {
        return value == null
                ? null
                : new UploadEchoFileRequest().setId(SerializationUtils.toLong(value.getId()))
                        .setBody(SerializationUtils.toBytes(value.getBody()));
    }
}
