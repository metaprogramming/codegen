package example.adapters.in.rest.validators.custom;

import example.commons.validator.Checker;
import example.commons.validator.Privilege;
import example.commons.validator.ValidationContext;
import example.application.UserData;
import example.application.UserService;

import java.util.Arrays;
import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.credentials;

@Component
@RequiredArgsConstructor
public class SecurityApiKeyChecker {

    private final UserService userService;


    public Checker<String> check(Privilege... scopes) {
        return credentials(this::getOwnedCredentials, Arrays.asList(scopes));
    }

    public List<Privilege> getOwnedCredentials(ValidationContext<String> ctx) {
        UserData userData = userService.loadUserData(ctx.getValue());
        ctx.setBean(userData);
        return userData.getCredentials();
    }

}
