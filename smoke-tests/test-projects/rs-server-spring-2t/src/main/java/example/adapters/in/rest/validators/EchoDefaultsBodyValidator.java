package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.EchoDefaultsBodyRdto;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.validator.AmountChecker;
import example.commons.validator.Checker;
import example.commons.validator.DictionaryChecker;
import example.commons.validator.Field;
import example.commons.validator.ValidationContext;
import java.math.BigDecimal;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.commons.validator.DictionaryCode.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyValidator implements Checker<EchoDefaultsBodyRdto> {

    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_INT = new Field<>("propInt",
            EchoDefaultsBodyRdto::getPropInt);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_FLOAT = new Field<>("propFloat",
            EchoDefaultsBodyRdto::getPropFloat);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_DOUBLE = new Field<>("propDouble",
            EchoDefaultsBodyRdto::getPropDouble);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_NUMBER = new Field<>("propNumber",
            EchoDefaultsBodyRdto::getPropNumber);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_STRING = new Field<>("propString",
            EchoDefaultsBodyRdto::getPropString);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_ENUM = new Field<>("propEnum",
            EchoDefaultsBodyRdto::getPropEnum);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_DATE = new Field<>("propDate",
            EchoDefaultsBodyRdto::getPropDate);
    public static final Field<EchoDefaultsBodyRdto, String> FIELD_PROP_DATE_TIME = new Field<>("propDateTime",
            EchoDefaultsBodyRdto::getPropDateTime);

    private final AmountChecker amountChecker;
    private final DictionaryChecker dictionaryChecker;

    public void check(ValidationContext<EchoDefaultsBodyRdto> ctx) {
        ctx.check(FIELD_PROP_INT, INT32, ge("1", Integer::valueOf));
        ctx.check(FIELD_PROP_FLOAT, FLOAT, ge("1", Float::valueOf));
        ctx.check(FIELD_PROP_DOUBLE, DOUBLE, ge("1", Double::valueOf));
        ctx.check(FIELD_PROP_NUMBER, amountChecker, ge("1", BigDecimal::new));
        ctx.check(FIELD_PROP_STRING, minLength(5), dictionaryChecker.check(COLORS));
        ctx.check(FIELD_PROP_ENUM, allow(ReusableEnumEnum.values()));
        ctx.check(FIELD_PROP_DATE, ISO_DATE);
        ctx.check(FIELD_PROP_DATE_TIME, ISO_DATE_TIME);
    }
}
