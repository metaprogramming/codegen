package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoArraysBodyRdto;
import example.adapters.in.rest.dtos.EchoArraysPostRrequest;
import example.adapters.in.rest.validators.EchoArraysBodyValidator;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import java.util.List;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostValidator extends Validator<EchoArraysPostRrequest> {

    public static final Field<EchoArraysPostRrequest, String> FIELD_AUTHORIZATION_PARAM = new Field<>(
            "Authorization (HEADER parameter)", EchoArraysPostRrequest::getAuthorizationParam);
    public static final Field<EchoArraysPostRrequest, String> FIELD_INLINE_HEADER_PARAM = new Field<>(
            "Inline-Header-Param (HEADER parameter)", EchoArraysPostRrequest::getInlineHeaderParam);
    public static final Field<EchoArraysPostRrequest, List<EchoArraysBodyRdto>> FIELD_BODY = new Field<>("body",
            EchoArraysPostRrequest::getBody);

    private final EchoArraysBodyValidator echoArraysBodyValidator;

    public void check(ValidationContext<EchoArraysPostRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_ARRAYS_POST);
        ctx.checkRoot(FIELD_BODY, required(), items(echoArraysBodyValidator));
    }
}
