package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.DeleteFileRrequest;
import example.adapters.in.rest.mappers.DeleteFileRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DeleteFileController {

    private final DeleteFileRequestMapper deleteFileRequestMapper;
    private final DeleteFileValidator deleteFileValidator;
    private final EchoFacade echoFacade;
    private final DeleteFileResponseMapper deleteFileResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    /**
     * deletes a file based on the id supplied
     */
    @DeleteMapping(value = "/api/v1/echo-file/{id}", produces = {"application/json"})
    public ResponseEntity deleteFile(@PathVariable(required = false) String id) {
        DeleteFileRrequest request = deleteFileRequestMapper.map2DeleteFileRrequest(id);
        ValidationResult validationResult = deleteFileValidator.validate(request);
        return validationResult.isValid()
                ? deleteFileResponseMapper
                        .map(echoFacade.deleteFile(deleteFileRequestMapper.map2DeleteFileRequest(request)))
                : validationResultMapper.map(validationResult);
    }
}
