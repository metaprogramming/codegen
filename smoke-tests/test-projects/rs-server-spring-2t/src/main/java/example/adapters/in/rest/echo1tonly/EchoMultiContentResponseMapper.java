package example.adapters.in.rest.echo1tonly;

import example.adapters.in.rest.mappers.MultiContentBodyMapper;
import example.ports.in.rest.dtos.EchoMultiContentResponse;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentResponseMapper {

    private final MultiContentBodyMapper multiContentBodyMapper;

    public ResponseEntity map(@Nonnull EchoMultiContentResponse response) {
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());
        response.getHeaders().forEach(responseBuilder::header);
        return responseBuilder.body(multiContentBodyMapper.map2MultiContentBodyRdto(response.get200()));
    }
}
