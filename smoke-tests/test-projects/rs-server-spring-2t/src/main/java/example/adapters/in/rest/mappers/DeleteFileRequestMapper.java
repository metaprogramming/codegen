package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.DeleteFileRrequest;
import example.commons.SerializationUtils;
import example.ports.in.rest.dtos.DeleteFileRequest;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class DeleteFileRequestMapper {

    public DeleteFileRrequest map2DeleteFileRrequest(String id) {
        return new DeleteFileRrequest().setId(id);
    }

    public DeleteFileRequest map2DeleteFileRequest(DeleteFileRrequest value) {
        return value == null ? null : new DeleteFileRequest().setId(SerializationUtils.toLong(value.getId()));
    }
}
