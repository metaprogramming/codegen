package example.adapters.in.rest.mappers;

import example.adapters.in.rest.dtos.ExtendedObjectRdto;
import example.commons.adapters.in.rest.mappers.SimpleObjectMapper;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.ports.in.rest.dtos.ExtendedObjectDto;
import java.util.Optional;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectMapper {

    private final SimpleObjectMapper simpleObjectMapper;

    public ExtendedObjectDto map2ExtendedObjectDto(ExtendedObjectRdto value) {
        return value == null ? null : map(new ExtendedObjectDto(), value);
    }

    public ExtendedObjectRdto map2ExtendedObjectRdto(ExtendedObjectDto value) {
        return value == null ? null : map(new ExtendedObjectRdto(), value);
    }

    public ExtendedObjectDto map(ExtendedObjectDto result, ExtendedObjectRdto value) {
        simpleObjectMapper.map(result, value);
        return result.setEoEnumReusable(ReusableEnumEnum.fromValue(value.getEoEnumReusable()))
                .setSelfProperty(map2ExtendedObjectDto(value.getSelfProperty()));
    }

    public ExtendedObjectRdto map(ExtendedObjectRdto result, ExtendedObjectDto value) {
        simpleObjectMapper.map(result, value);
        return result
                .setEoEnumReusable(
                        Optional.ofNullable(value.getEoEnumReusable()).map(ReusableEnumEnum::getValue).orElse(null))
                .setSelfProperty(map2ExtendedObjectRdto(value.getSelfProperty()));
    }
}
