package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.UploadEchoFileWithFormRrequest;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.processing.Generated;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormValidator extends Validator<UploadEchoFileWithFormRrequest> {

    public static final Field<UploadEchoFileWithFormRrequest, String> FIELD_ID = new Field<>("id (PATH parameter)",
            UploadEchoFileWithFormRrequest::getId);
    public static final Field<UploadEchoFileWithFormRrequest, Resource> FIELD_FILE = new Field<>(
            "file (FORMDATA parameter)", UploadEchoFileWithFormRrequest::getFile);

    public void check(ValidationContext<UploadEchoFileWithFormRrequest> ctx) {
        ctx.setBean(OperationId.UPLOAD_ECHO_FILE_WITH_FORM);
        ctx.check(FIELD_ID, required(), INT64);
        ctx.check(FIELD_FILE, required());
    }
}
