package example.adapters.in.rest.validators;

import example.adapters.in.rest.dtos.ExtendedObjectRdto;
import example.commons.adapters.in.rest.validators.SimpleObjectValidator;
import example.commons.ports.in.rest.dtos.ReusableEnumEnum;
import example.commons.validator.Checker;
import example.commons.validator.Field;
import example.commons.validator.ValidationContext;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class ExtendedObjectValidator implements Checker<ExtendedObjectRdto> {

    public static final Field<ExtendedObjectRdto, String> FIELD_EO_ENUM_REUSABLE = new Field<>("eo_enum_reusable",
            ExtendedObjectRdto::getEoEnumReusable);
    public static final Field<ExtendedObjectRdto, ExtendedObjectRdto> FIELD_SELF_PROPERTY = new Field<>("self_property",
            ExtendedObjectRdto::getSelfProperty);

    private final ExtendedObjectChecker extendedObjectChecker;
    private final SimpleObjectValidator simpleObjectValidator;
    private final ExtendedObjectEnumChecker extendedObjectEnumChecker;

    public void check(ValidationContext<ExtendedObjectRdto> ctx) {
        ctx.check(extendedObjectChecker);
        simpleObjectValidator.checkWithParent(ctx);
        ctx.check(FIELD_EO_ENUM_REUSABLE, allow(ReusableEnumEnum.values()), extendedObjectEnumChecker);
        ctx.check(FIELD_SELF_PROPERTY, this);
    }
}
