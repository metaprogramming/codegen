package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoGetRrequest;
import example.adapters.in.rest.validators.custom.SecurityApiKeyChecker;
import example.adapters.in.rest.validators.custom.SecurityAuthorizationChecker;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DefaultEnumEnum;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;
import static example.commons.validator.Privilege.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoGetValidator extends Validator<EchoGetRrequest> {

    public static final Field<EchoGetRrequest, String> FIELD_AUTHORIZATION = new Field<>(
            "Authorization (HEADER parameter)", EchoGetRrequest::getAuthorization);
    public static final Field<EchoGetRrequest, String> FIELD_API_KEY = new Field<>("api_key (QUERY parameter)",
            EchoGetRrequest::getApiKey);
    public static final Field<EchoGetRrequest, String> FIELD_CORRELATION_ID_PARAM = new Field<>(
            "X-Correlation-ID (HEADER parameter)", EchoGetRrequest::getCorrelationIdParam);
    public static final Field<EchoGetRrequest, String> FIELD_PROP_INT_REQUIRED = new Field<>(
            "prop_int_required (QUERY parameter)", EchoGetRrequest::getPropIntRequired);
    public static final Field<EchoGetRrequest, String> FIELD_PROP_FLOAT = new Field<>("prop_float (QUERY parameter)",
            EchoGetRrequest::getPropFloat);
    public static final Field<EchoGetRrequest, String> FIELD_PROP_ENUM = new Field<>("prop_enum (QUERY parameter)",
            EchoGetRrequest::getPropEnum);

    private final SecurityAuthorizationChecker securityAuthorizationChecker;
    private final SecurityApiKeyChecker securityApiKeyChecker;

    public void check(ValidationContext<EchoGetRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_GET);
        ctx.check(authParamNonNull(FIELD_AUTHORIZATION, FIELD_API_KEY));
        ctx.check(FIELD_AUTHORIZATION, securityAuthorizationChecker.check(WRITE, READ));
        ctx.check(FIELD_API_KEY, securityApiKeyChecker.check(WRITE, READ));
        ctx.check(FIELD_CORRELATION_ID_PARAM, required());
        ctx.check(FIELD_PROP_INT_REQUIRED, required(), INT64, ge("5", Long::valueOf));
        ctx.check(FIELD_PROP_FLOAT, FLOAT, le("5.02", Float::valueOf));
        ctx.check(FIELD_PROP_ENUM, allow(DefaultEnumEnum.values()));
    }
}
