package example.adapters.in.rest.echo1tonly;

import example.adapters.in.rest.dtos.EchoMultiContentRrequest;
import example.adapters.in.rest.dtos.MultiContentBodyRdto;
import example.adapters.in.rest.mappers.EchoMultiContentRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.Echo1tOnlyFacade;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentController {

    private final EchoMultiContentRequestMapper echoMultiContentRequestMapper;
    private final EchoMultiContentValidator echoMultiContentValidator;
    private final Echo1tOnlyFacade echo1tOnlyFacade;
    private final EchoMultiContentResponseMapper echoMultiContentResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    @PostMapping(value = "/api/v1/echo-multi-content", produces = {"application/json", "application/xml",
            "text/plain"}, consumes = {"application/xml", "application/json"})
    public ResponseEntity echoMultiContent(@RequestBody MultiContentBodyRdto body) {
        EchoMultiContentRrequest request = echoMultiContentRequestMapper.map2EchoMultiContentRrequest(body);
        ValidationResult validationResult = echoMultiContentValidator.validate(request);
        return validationResult.isValid()
                ? echoMultiContentResponseMapper.map(echo1tOnlyFacade
                        .echoMultiContent(echoMultiContentRequestMapper.map2EchoMultiContentRequest(request)))
                : validationResultMapper.map(validationResult);
    }
}
