package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.DownloadEchoFileRrequest;
import example.adapters.in.rest.mappers.DownloadEchoFileRequestMapper;
import example.commons.validator.ValidationResult;
import example.commons.validator.ValidationResultMapper;
import example.ports.in.rest.EchoFacade;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileController {

    private final DownloadEchoFileRequestMapper downloadEchoFileRequestMapper;
    private final DownloadEchoFileValidator downloadEchoFileValidator;
    private final EchoFacade echoFacade;
    private final DownloadEchoFileResponseMapper downloadEchoFileResponseMapper;
    private final ValidationResultMapper validationResultMapper;

    /**
     * Returns a file
     */
    @GetMapping(value = "/api/v1/echo-file/{id}", produces = {"image/jpeg", "application/json"})
    public ResponseEntity downloadEchoFile(@PathVariable(required = false) String id) {
        DownloadEchoFileRrequest request = downloadEchoFileRequestMapper.map2DownloadEchoFileRrequest(id);
        ValidationResult validationResult = downloadEchoFileValidator.validate(request);
        return validationResult.isValid()
                ? downloadEchoFileResponseMapper.map(
                        echoFacade.downloadEchoFile(downloadEchoFileRequestMapper.map2DownloadEchoFileRequest(request)))
                : validationResultMapper.map(validationResult);
    }
}
