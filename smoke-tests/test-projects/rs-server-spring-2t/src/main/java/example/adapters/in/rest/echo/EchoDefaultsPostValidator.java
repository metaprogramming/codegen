package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoDefaultsBodyRdto;
import example.adapters.in.rest.dtos.EchoDefaultsPostRrequest;
import example.adapters.in.rest.validators.EchoDefaultsBodyValidator;
import example.commons.validator.Field;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import example.ports.in.rest.dtos.DefaultEnumEnum;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import static example.commons.validator.CommonCheckers.*;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostValidator extends Validator<EchoDefaultsPostRrequest> {

    public static final Field<EchoDefaultsPostRrequest, String> FIELD_DEFAULT_HEADER_PARAM = new Field<>(
            "Default-Header-Param (HEADER parameter)", EchoDefaultsPostRrequest::getDefaultHeaderParam);
    public static final Field<EchoDefaultsPostRrequest, EchoDefaultsBodyRdto> FIELD_BODY = new Field<>("body",
            EchoDefaultsPostRrequest::getBody);

    private final EchoDefaultsBodyValidator echoDefaultsBodyValidator;

    public void check(ValidationContext<EchoDefaultsPostRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_DEFAULTS_POST);
        ctx.check(FIELD_DEFAULT_HEADER_PARAM, allow(DefaultEnumEnum.values()));
        ctx.checkRoot(FIELD_BODY, echoDefaultsBodyValidator);
    }
}
