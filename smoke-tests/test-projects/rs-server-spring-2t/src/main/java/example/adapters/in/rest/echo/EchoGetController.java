package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoGetRrequest;
import example.adapters.in.rest.mappers.EchoGetRequestMapper;
import example.commons.ports.in.rest.dtos.ErrorItemDto;
import example.commons.validator.ValidationResult;
import example.ports.in.rest.EchoFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class EchoGetController {

    private final EchoGetRequestMapper echoGetRequestMapper;
    private final EchoGetValidator echoGetValidator;
    private final EchoFacade echoFacade;
    private final EchoGetResponseMapper echoGetResponseMapper;

    @GetMapping(value = "/api/v1/echo", produces = {"application/json"})
    public ResponseEntity echoGet(
            @RequestHeader(value = "Authorization", required = false) String authorizationParam,
            @RequestParam(value = "api_key", required = false) String apiKey,
            @RequestHeader(value = "X-Correlation-ID", required = false) String correlationIdParam,
            @RequestParam(value = "prop_int_required", required = false) String propIntRequired,
            @RequestParam(value = "prop_float", required = false) String propFloat,
            @RequestParam(value = "prop_enum", required = false) String propEnum
    ) {
        EchoGetRrequest request = echoGetRequestMapper.map2EchoGetRrequest(authorizationParam, apiKey, correlationIdParam, propIntRequired, propFloat, propEnum);
        ValidationResult validationResult = echoGetValidator.validate(request);
        return validationResult.isValid()
                ? echoGetResponseMapper.map(echoFacade.echoGet(echoGetRequestMapper.map2EchoGetRequest(request)))
                : map(validationResult);
    }


    private ResponseEntity<List<ErrorItemDto>> map(@Nonnull ValidationResult validationResult) {
        return ResponseEntity
                .status(validationResult.getStatus())
                .body(validationResult.getErrors().stream()
                        .map(e -> new ErrorItemDto().setCode(e.getCode()))
                        .collect(Collectors.toList()));
    }

}
