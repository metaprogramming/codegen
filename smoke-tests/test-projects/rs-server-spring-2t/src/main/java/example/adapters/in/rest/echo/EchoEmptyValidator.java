package example.adapters.in.rest.echo;

import example.adapters.in.rest.dtos.EchoEmptyRrequest;
import example.commons.validator.OperationId;
import example.commons.validator.ValidationContext;
import example.commons.validator.Validator;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyValidator extends Validator<EchoEmptyRrequest> {

    public void check(ValidationContext<EchoEmptyRrequest> ctx) {
        ctx.setBean(OperationId.ECHO_EMPTY);
    }
}
