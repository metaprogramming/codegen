/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.commons

import example.ports.in.rest.dtos.EnumTypeEnum
import spock.lang.Specification

class EnumValueSpec extends Specification {

    def "should fail for enum from value"() {
        when:
        def value = EnumValue.fromValue('NONE', EnumTypeEnum)
        then:
        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "Unknown value 'NONE' of enum 'example.ports.in.rest.dtos.EnumTypeEnum'"
    }
}
