package example.commons

import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class SerializationUtilsSpec extends Specification {

    def setupSpec() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"))
    }

    @Unroll
    def "should deserialize and serialize LocalDateTime with ISO8601 date time format: #iso8601Value"() {
        when:
        def deserialized = SerializationUtils.toLocalDateTime(iso8601Value)
        then:
        deserialized instanceof LocalDateTime
        deserialized.format(DateTimeFormatter.ISO_DATE_TIME) == stringValue

        when:
        def serialized = SerializationUtils.toString(deserialized)
        then:
        serialized == serializedValue

        where:
        iso8601Value                    | stringValue               | serializedValue
        '2019-09-28T22:22:11.222Z'      | '2019-09-28T22:22:11.222' | '2019-09-28T22:22:11.222Z'
        '2019-09-28T22:22:11.222-02:00' | '2019-09-29T00:22:11.222' | '2019-09-29T00:22:11.222Z'
        '2019-09-28T22:22:11Z'          | '2019-09-28T22:22:11'     | '2019-09-28T22:22:11Z'
    }

    def "should deserialize and serialize LocalDate with ISO8601 date format"() {
        given:
        def iso8601Value = '2019-09-28'
        when:
        def deserialized = SerializationUtils.toLocalDate(iso8601Value)
        then:
        deserialized instanceof LocalDate
        deserialized.format(DateTimeFormatter.ISO_LOCAL_DATE) == iso8601Value

        when:
        def serialized = SerializationUtils.toString(deserialized)
        then:
        serialized == iso8601Value
    }
}
