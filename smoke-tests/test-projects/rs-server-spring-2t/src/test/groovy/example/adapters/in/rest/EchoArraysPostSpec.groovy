/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.in.rest

import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import spock.lang.Unroll

class EchoArraysPostSpec extends BaseSpecification {

    String getOperationPath() {
        'api/v1/echo-arrays'
    }

    @Unroll
    def "should success (#params)"() {
        given:
        def request = makeRequest(params)

        when:
        def response = post(request, List)

        then:
        response != null
        response.statusCode.value() == 200

        when:
        def reqBody = request.body[0]
        def resBody = response.body[0] as Map
        reqBody.prop_boolean_list = fixBooleanList(reqBody.prop_boolean_list as List<String>)
        reqBody.prop_date_time_list_of_list = fixDateTimeList(reqBody.prop_date_time_list_of_list as List<List<String>>)
        resBody.prop_date_time_list_of_list = fixDateTimeList(resBody.prop_date_time_list_of_list as List<List<String>>)

        then:
        cleanMap(resBody) == cleanMap(reqBody)
//        equals(cleanMap(resBody), cleanMap(reqBody))

        where:
        params << [
                [:],
                ['header.Inline-Header-Param': null],
                [prop_int_list: [-1]],
                [prop_int_list: [2147483647]],
                [prop_float_list: null],
                [prop_float_list: []],
                [prop_float_list: [0]],
                [prop_float_list: [101.0000000]],
                [prop_double_list: [0.01, 1000000000000000]],
                [prop_double_list: [0.01, 2, 9999.9999, 1000000000000000]],
                [prop_amount_list: null],
                [prop_amount_list: []],
                [prop_amount_list: ['0.00']],
                [prop_string_list: null],
                [prop_string_list: []],
                [prop_string_list: ['SNAKE', 'MOUSE']],
                [prop_date_list: null],
                [prop_date_list: []],
                [prop_date_time_list_of_list: [['2019-10-04T11:11:00.000Z', '2019-10-04T22:22:00.000Z'], ['2019-10-05T11:11:00.000Z', '2019-10-05T22:22:00.000Z']]],
                [prop_boolean_list: null],
                [prop_boolean_list: []],
                [prop_boolean_list: [true, false, 'TRUE', 'FALSE']],
                [prop_object_list: null],
                [prop_object_list: []],
                [prop_enum_list: null],
                [prop_enum_list: []],
                [prop_enum_list: ['1', 'b', 'A']],
                //[so_prop_date: null],
                [so_prop_date: '2020-02-29'],
                [so_prop_string: ''],
                [prop_map_of_int: null],
                // [prop_map_of_int: ['key': null]], its problematic for SerializationUtils and jackson
                [prop_map_of_int: ['k1': 1, 'k2': 2]],
                [prop_map_of_object: null],
                [prop_map_of_list_of_object: null],
        ]
    }

    @Unroll
    def "should validation fail for #field #code (#params)"() {
        given:
        def request = makeRequest(params)

        when:
        def response = post(request, Map)

        then:
        response != null
        response.statusCode.value() == 400
        checkCorrelationIdHeader(response, request)
        response.body.code == 400
        response.body.errors.any {
            it.code == code && it.field == field && it.message == message
        }

        where:
        field                                                      | code                   | params                                                                                   | message
        'requestBody'                                              | 'is_required'          | [body: null]                                                                             | 'requestBody is required'
        '[0].prop_int_list'                                        | 'has_too_few_items'    | [prop_int_list: []]                                                                      | '[0].prop_int_list has not enough elements, min allowed size is 1'
        '[0].prop_int_list[0]'                                     | 'is_not_32bit_integer' | [prop_int_list: ['2147483648']]                                                          | '[0].prop_int_list[0] is not 32bit integer'
        '[0].prop_int_list[0]'                                     | 'is_not_32bit_integer' | [prop_int_list: ['0.1']]                                                                 | '[0].prop_int_list[0] is not 32bit integer'
        '[0].prop_float_list[0]'                                   | 'is_too_big'           | [prop_float_list: ['101.001']]                                                           | '[0].prop_float_list[0] should be <= 101'
        '[0].prop_double_list'                                     | 'is_required'          | [prop_double_list: null]                                                                 | '[0].prop_double_list is required'
        '[0].prop_double_list'                                     | 'has_duplicated_items' | [prop_double_list: ['1', '1']]                                                           | '[0].prop_double_list has duplicated elements'
        '[0].prop_double_list'                                     | 'has_too_many_items'   | [prop_double_list: ['1', '2', '3', '4', '5']]                                            | '[0].prop_double_list has too many elements, max allowed size is 4'
        '[0].prop_double_list[1]'                                  | 'is_too_small'         | [prop_double_list: ['1', '0.00999999']]                                                  | '[0].prop_double_list[1] should be >= 0.01'
        '[0].prop_double_list[1]'                                  | 'is_too_big'           | [prop_double_list: ['1', '1000000000000000.1']]                                          | '[0].prop_double_list[1] should be <= 1000000000000000'
        '[0].prop_amount_list[0]'                                  | 'invalid_amount'       | [prop_amount_list: ['1']]                                                                | '[0].prop_amount_list[0] should match pattern: ^(0|([1-9][0-9]{0,}))\\.\\d{2}$'
        '[0].prop_amount_list[0]'                                  | 'invalid_amount'       | [prop_amount_list: ['1.0']]                                                              | '[0].prop_amount_list[0] should match pattern: ^(0|([1-9][0-9]{0,}))\\.\\d{2}$'
        '[0].prop_amount_list[0]'                                  | 'invalid_amount'       | [prop_amount_list: ['1.001']]                                                            | '[0].prop_amount_list[0] should match pattern: ^(0|([1-9][0-9]{0,}))\\.\\d{2}$'
        '[0].prop_string_list[1]'                                  | 'invalid-animal'       | [prop_string_list: ['MOUSE', 'INVALID']]                                                 | 'invalid_ANIMALS'
        '[0].prop_string_list[1]'                                  | 'is_too_short'         | [prop_string_list: ['MOUSE', 'ORCA']]                                                    | '[0].prop_string_list[1] has too short value, min allowed length is 5'
        '[0].prop_string_list[0]'                                  | 'is_too_short'         | [prop_string_list: ['ORCA']]                                                             | '[0].prop_string_list[0] has too short value, min allowed length is 5'
        '[0].prop_string_list[0]'                                  | 'is_too_long'          | [prop_string_list: ['CATERPILLAR']]                                                      | '[0].prop_string_list[0] has too long value, max allowed length is 10'
        '[0].prop_boolean_list[0]'                                 | 'is_not_boolean'       | [prop_boolean_list: ['x']]                                                               | '[0].prop_boolean_list[0] should have one of values: [true, false]'
        '[0].prop_boolean_list[1]'                                 | 'is_not_boolean'       | [prop_boolean_list: ['true', 'x']]                                                       | '[0].prop_boolean_list[1] should have one of values: [true, false]'
        '[0].prop_date_list[0]'                                    | 'is_not_date'          | [prop_date_list: ['2019-08-32']]                                                         | '[0].prop_date_list[0] is not yyyy-MM-dd'
        '[0].prop_date_time_list_of_list'                          | 'has_too_few_items'    | [prop_date_time_list_of_list: []]                                                        | '[0].prop_date_time_list_of_list has not enough elements, min allowed size is 1'
        '[0].prop_date_time_list_of_list[0]'                       | 'has_too_few_items'    | [prop_date_time_list_of_list: [[]]]                                                      | '[0].prop_date_time_list_of_list[0] has not enough elements, min allowed size is 2'
        '[0].prop_date_time_list_of_list[0]'                       | 'has_too_few_items'    | [prop_date_time_list_of_list: [['2019-09-28T22:22:00.000Z']]]                            | '[0].prop_date_time_list_of_list[0] has not enough elements, min allowed size is 2'
        '[0].prop_date_time_list_of_list[0][1]'                    | 'is_not_date_time'     | [prop_date_time_list_of_list: [['2019-09-28T22:22:00.000Z', '2019-10-05 22:22:00.000']]] | '[0].prop_date_time_list_of_list[0][1] should be valid date time in ISO8601 format'
        '[0].prop_enum_list[1]'                                    | 'is_not_allowed_value' | [prop_enum_list: ['1', 'a']]                                                             | '[0].prop_enum_list[1] should have one of values: [A, b, 1]'
        '[0].prop_object_list[0].so_prop_string'                   | 'is_required'          | [so_prop_string: null]                                                                   | '[0].prop_object_list[0].so_prop_string is required'
        '[0].prop_object_list[0].so_prop_string'                   | 'custom.failed.code'   | [so_prop_string: '123', prop_enum_list: ['b']]                                           | '[0].prop_object_list[0].so_prop_string has too short value, min allowed length is 6'
        '[0].prop_object_list[0].so_prop_date'                     | 'is_required'          | [so_prop_date: null]                                                                     | '[0].prop_object_list[0].so_prop_date is required'
        '[0].prop_object_list[0].so_prop_date'                     | 'is_not_date'          | [so_prop_date: '919-02-29']                                                              | '[0].prop_object_list[0].so_prop_date is not yyyy-MM-dd'
        '[0].prop_object_list[0].so_prop_date'                     | 'is_not_date'          | [so_prop_date: '12019-02-29']                                                            | '[0].prop_object_list[0].so_prop_date is not yyyy-MM-dd'
        '[0].prop_object_list[0].so_prop_date'                     | 'is_not_date'          | [so_prop_date: '2019-08-32']                                                             | '[0].prop_object_list[0].so_prop_date is not yyyy-MM-dd'
        '[0].prop_object_list[0].so_prop_date'                     | 'is_not_date'          | [so_prop_date: '2019-13-01']                                                             | '[0].prop_object_list[0].so_prop_date is not yyyy-MM-dd'
        '[0].prop_object_list[0].so_prop_date'                     | 'is_not_date'          | [so_prop_date: '2019-02-29']                                                             | '[0].prop_object_list[0].so_prop_date is not yyyy-MM-dd'
        '[0].prop_object_list_of_list[0][0].so_prop_string'        | 'is_required'          | ['lol.so_prop_string': null]                                                             | '[0].prop_object_list_of_list[0][0].so_prop_string is required'
        '[0].prop_object_list_of_list[0][0].so_prop_date'          | 'is_not_date'          | ['lol.so_prop_date': '2019-02-29']                                                       | '[0].prop_object_list_of_list[0][0].so_prop_date is not yyyy-MM-dd'
        '[0].prop_map_of_int[key]'                                 | 'is_not_32bit_integer' | [prop_map_of_int: ['key': 'str']]                                                        | '[0].prop_map_of_int[key] is not 32bit integer'
        '[0].prop_map_of_int[key]'                                 | 'is_too_big'           | [prop_map_of_int: ['key': '101']]                                                        | '[0].prop_map_of_int[key] should be <= 100'
        '[0].prop_map_of_object[key_o].so_prop_string'             | 'is_required'          | ['moo.so_prop_string': null]                                                             | '[0].prop_map_of_object[key_o].so_prop_string is required'
        '[0].prop_map_of_object[key_o].so_prop_date'               | 'is_not_date'          | ['moo.so_prop_date': '2019-02-29']                                                       | '[0].prop_map_of_object[key_o].so_prop_date is not yyyy-MM-dd'
        '[0].prop_map_of_list_of_object[key_ol][1].so_prop_string' | 'is_required'          | ['mol.so_prop_string': null]                                                             | '[0].prop_map_of_list_of_object[key_ol][1].so_prop_string is required'
        '[0].prop_map_of_list_of_object[key_ol][1].so_prop_date'   | 'is_not_date'          | ['mol.so_prop_date': '2019-02-29']                                                       | '[0].prop_map_of_list_of_object[key_ol][1].so_prop_date is not yyyy-MM-dd'
    }

    def makeRequest(Map params) {
        def headers = new HttpHeaders()
        setHeader('Authorization', 'Bearer 123123123', headers, params)
        setHeader('Inline-Header-Param', 'some value', headers, params)
        setHeader('content-type', 'application/json', headers, params)
        def body = params.getOrDefault('body', [cleanMap(makeBody(params), DO_NOT_SEND)]) as List<Map>
        new HttpEntity<List<Map>>(body, headers)
    }

    Map makeBody(Map params = [:]) {
        ["prop_int_list"              : params.getOrDefault('prop_int_list', [0]),
         "prop_float_list"            : params.getOrDefault('prop_float_list', [0.0]),
         "prop_double_list"           : params.getOrDefault('prop_double_list', [1.0, 2.0]),
         "prop_amount_list"           : params.getOrDefault('prop_amount_list', ['1.99']),
         "prop_string_list"           : params.getOrDefault('prop_string_list', ['MOUSE']),
         "prop_date_list"             : params.getOrDefault('prop_date_list', ['2019-10-24']),
         "prop_date_time_list_of_list": params.getOrDefault('prop_date_time_list_of_list', [['2019-10-24T19:39:11.335Z', '2020-05-01T14:00:11.335Z']]),
         "prop_boolean_list"          : params.getOrDefault('prop_boolean_list', [true]),
         "prop_object_list"           : params.getOrDefault('', [["so_prop_string": params.getOrDefault('so_prop_string', 'string'), "so_prop_date": params.getOrDefault('so_prop_date', '2019-10-24')]]),
         "prop_object_list_of_list"   : params.getOrDefault('prop_object_list_of_list', [[["so_prop_string": params.getOrDefault('lol.so_prop_string', 'string'), "so_prop_date": params.getOrDefault('lol.so_prop_date', '2019-10-24')]]]),
         "prop_enum_reusable_list"    : params.getOrDefault('prop_enum_reusable_list', [null]),
         "prop_enum_list"             : params.getOrDefault('prop_enum_list', ['A']),
         "prop_map_of_int"            : params.getOrDefault('prop_map_of_int', ['K': 1]),
         "prop_map_of_object"         : params.getOrDefault('prop_map_of_object', ['key_o': ['so_prop_string': params.getOrDefault('moo.so_prop_string', 'string'), 'so_prop_date': params.getOrDefault('moo.so_prop_date', '2020-11-28')]]),
         "prop_map_of_list_of_object" : params.getOrDefault('prop_map_of_list_of_object', ['key_ol': [['so_prop_string': 'string', 'so_prop_date': '2020-11-28'], ['so_prop_string': params.getOrDefault('mol.so_prop_string', 'string'), 'so_prop_date': params.getOrDefault('mol.so_prop_date', '2020-11-28')]]]),
        ]
    }

    def fixBooleanList(List<Object> value) {
        value ? value.collect {
            it instanceof String ? Boolean.parseBoolean(it) : it
        } : value
    }

    def fixDateTimeList(List<List<String>> value) {
        value.collect {
            it.collect {
                toDateTime(it)
            }
        }
    }

}
