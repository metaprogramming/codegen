package example;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import example.ports.in.rest.dtos.ErrorDescriptionDto;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorDescriptionDto> handleAll(Exception e, WebRequest request) {
        if (e instanceof MissingRequestHeaderException) {
            return makeResponse(toError((MissingRequestHeaderException) e));
        }
        if (e instanceof HttpMessageNotReadableException) {
            return makeResponse(toError((HttpMessageNotReadableException) e));
        }
        if (e instanceof MethodArgumentNotValidException) {
            return makeResponse(toError((MethodArgumentNotValidException) e));
        }
        if (e instanceof MethodArgumentTypeMismatchException) {
            return makeResponse(toError((MethodArgumentTypeMismatchException) e));
        }
        if (e instanceof ConstraintViolationException) {
            return makeResponse(toError((ConstraintViolationException) e));
        }
        log.error("Exception", e);
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<ErrorDescriptionDto> makeResponse(Error error) {
        return new ResponseEntity<>(new ErrorDescriptionDto()
                .setCode(400)
                .setMessage(error.toString()),
                HttpStatus.BAD_REQUEST);

    }

    private Error toError(MissingRequestHeaderException e) {
        return new Error()
                .setField(headerParam(e.getHeaderName()))
                .setRequired();
    }

    private String headerParam(String paramName) {
        return paramName + " (HEADER parameter)";
    }

    private Error toError(HttpMessageNotReadableException e) {
        Error result = new Error();
        if (e.getMessage() != null && e.getMessage().startsWith("Required request body is missing")) {
            return result.setDescription("requestBody is required");
        }
        if (e.getCause() instanceof InvalidFormatException) {
            InvalidFormatException cex = (InvalidFormatException) e.getCause();
            result.setField(cex.getPath().get(0).getFieldName());
            String description = INVALID_DESC_BY_CLASS.get(cex.getTargetType());
            if (description != null) {
                return result.setDescription(description);
            }
            if (cex.getTargetType().isEnum()) {
                return result.setAllowedValues(enumValues(cex.getTargetType()));
            }
        }
        log.info("Invalid request", e);
        return result;
    }

    private List<String> enumValues(Class<?> enumClass) {
        return Stream.of(enumClass.getEnumConstants())
                .map(this::readEnumValue)
                .collect(Collectors.toList());
    }

    @SneakyThrows
    private String readEnumValue(Object o) {
        return (String) FieldUtils.readField(o, "value", true);
    }

    private Error toError(MethodArgumentNotValidException e) {
        Error result = new Error();
        BindingResult bindingResult = e.getBindingResult();
        FieldError error = bindingResult.getFieldError();
        Object target = bindingResult.getTarget();
        if (error == null || target == null) {
            log.info("Can't handle error; bindingResult: {}, target: {}", bindingResult, target);
            return result;
        }
        result.setField(getJsonPath(target, error.getField()));
        if ("NotNull".equals(error.getCode())) {
            return result.setRequired();
        }
        if ("Min".equals(error.getCode()) || "DecimalMin".equals(error.getCode())) {
            return result.setDescription("should be >= " + getLimit(error));
        }
        if ("Max".equals(error.getCode()) || "DecimalMax".equals(error.getCode())) {
            return result.setDescription("should be <= " + getLimit(error));
        }
        log.info("Invalid request", e);
        return result;
    }

    String getLimit(FieldError error) {
        Object[] args = error.getArguments();
        return args != null && args.length > 0 ? args[args.length - 1].toString() : "unknown";
    }

    @SneakyThrows
    private String getJsonPath(Object root, String fieldPath) {
        int idx = fieldPath.indexOf('.');
        String propertyName = idx > 0 ? fieldPath.substring(0, idx) : fieldPath;
        Field field = FieldUtils.getField(root.getClass(), propertyName, true);
        JsonProperty jsonProperty = field.getAnnotation(JsonProperty.class);
        String jsonPropertyPath = jsonProperty != null ? jsonProperty.value() : propertyName;
        if (idx > 0) {
            return jsonPropertyPath + "." + getJsonPath(field.get(root), fieldPath.substring(idx + 1));
        } else {
            return jsonPropertyPath;
        }
    }

    private Error toError(MethodArgumentTypeMismatchException e) {
        Error result = new Error();
        RequestHeader headerAnnotation = e.getParameter().getParameterAnnotation(RequestHeader.class);
        result.setField(headerAnnotation == null ? e.getName()
                : headerParam(nvl(headerAnnotation.name(), e.getName())));
        String description = INVALID_DESC_BY_CLASS.get(e.getParameter().getParameterType());
        if (description != null) {
            result.setDescription(description);
        } else {
            log.info("invalid format", e);
        }
        return result;
    }

    private String nvl(String v1, String v2) {
        return v1 != null && !v1.isEmpty() ? v1 : v2;
    }

    private Error toError(ConstraintViolationException e) {
        return e.getConstraintViolations().stream().findFirst().map(violation -> {
            Error result = new Error().setField(violation.getPropertyPath().toString());
            if (violation.getConstraintDescriptor().getAnnotation() instanceof NotNull) {
                return result.setRequired();
            }
            return result;
        }).orElse(null);
    }

    static final Map<Class<?>, String> INVALID_DESC_BY_CLASS = Stream.of(new Object[][]{
            {Long.class, "is not 64bit integer"},
            {long.class, "is not 64bit integer"},
            {LocalDate.class, "is not yyyy-MM-dd"},
            {ZonedDateTime.class, "should be valid date time in ISO8601 format"},
    }).collect(Collectors.toMap(data -> (Class<?>) data[0], data -> (String) data[1]));

    @Data
    @Accessors(chain = true)
    static class Error {
        private String field;
        private String description = "invalid value";

        Error setAllowedValues(List<String> allowedValues) {
            description = "should have one of values: " + allowedValues;
            return this;
        }

        Error setRequired() {
            description = "is required";
            return this;
        }

        public String toString() {
            return (field != null ? field + ' ' : "") + description;
        }
    }
}
