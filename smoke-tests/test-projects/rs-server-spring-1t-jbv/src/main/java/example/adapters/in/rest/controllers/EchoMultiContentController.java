package example.adapters.in.rest.controllers;

import example.commons.UrlEncodedObjectMapper;
import example.ports.in.rest.Echo1tOnlyFacade;
import example.ports.in.rest.dtos.EchoMultiContentMultiContentBodyRequest;
import example.ports.in.rest.dtos.EchoMultiContentTextPlainRequest;
import example.ports.in.rest.dtos.MultiContentBodyDto;
import javax.annotation.processing.Generated;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentController {

    private final UrlEncodedObjectMapper urlEncodedObjectMapper;
    private final Echo1tOnlyFacade echo1tOnlyFacade;

    @PostMapping(value = "/api/v1/echo-multi-content", produces = {"application/json", "application/xml",
            "text/plain"}, consumes = {"application/x-www-form-urlencoded"})
    public ResponseEntity<Object> echoMultiContent(@RequestParam @Valid MultiValueMap<String, String> body) {
        EchoMultiContentMultiContentBodyRequest request = new EchoMultiContentMultiContentBodyRequest()
                .setBody(urlEncodedObjectMapper.map(body, MultiContentBodyDto.class));
        return echo1tOnlyFacade.echoMultiContent(request);
    }

    @PostMapping(value = "/api/v1/echo-multi-content", produces = {"application/json", "application/xml",
            "text/plain"}, consumes = {"application/xml", "application/json"})
    public ResponseEntity<Object> echoMultiContent(@RequestBody @Valid MultiContentBodyDto body) {
        EchoMultiContentMultiContentBodyRequest request = new EchoMultiContentMultiContentBodyRequest().setBody(body);
        return echo1tOnlyFacade.echoMultiContent(request);
    }

    @PostMapping(value = "/api/v1/echo-multi-content", produces = {"application/json", "application/xml",
            "text/plain"}, consumes = {"text/plain"})
    public ResponseEntity<Object> echoMultiContent(@RequestBody @Valid String body) {
        EchoMultiContentTextPlainRequest request = new EchoMultiContentTextPlainRequest().setBody(body);
        return echo1tOnlyFacade.echoMultiContent(request);
    }
}
