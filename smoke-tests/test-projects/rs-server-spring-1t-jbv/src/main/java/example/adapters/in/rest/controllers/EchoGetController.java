package example.adapters.in.rest.controllers;

import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.DefaultEnum;
import example.ports.in.rest.dtos.EchoGetBodyDto;
import example.ports.in.rest.dtos.EchoGetRequest;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoGetController {

    private final EchoFacade echoFacade;

    @GetMapping(value = "/api/v1/echo", produces = {"application/json"})
    public ResponseEntity<EchoGetBodyDto> echoGet(
            @RequestHeader(value = "Authorization", required = false) String authorization,
            @RequestParam(value = "api_key", required = false) String apiKey,
            @RequestHeader("X-Correlation-ID") String xCorrelationId,
            @RequestParam("prop_int_required") Long propIntRequired,
            @RequestParam(value = "prop_float", required = false) Float propFloat,
            @RequestParam(value = "prop_enum", required = false) String propEnum) {
        EchoGetRequest request = new EchoGetRequest().setAuthorization(authorization).setApiKey(apiKey)
                .setXCorrelationId(xCorrelationId).setPropIntRequired(propIntRequired).setPropFloat(propFloat)
                .setPropEnum(DefaultEnum.fromValue(propEnum));
        return ResponseEntity.ok(echoFacade.echoGet(request));
    }
}
