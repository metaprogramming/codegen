package example.adapters.in.rest.controllers;

import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.EchoErrorRequest;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoErrorController {

    private final EchoFacade echoFacade;

    @GetMapping(value = "/api/v1/echo-error", produces = {"application/json"})
    public ResponseEntity<Void> echoError(@RequestParam String errorMessage) {
        EchoErrorRequest request = new EchoErrorRequest().setErrorMessage(errorMessage);
        echoFacade.echoError(request);
        return ResponseEntity.noContent().build();
    }
}
