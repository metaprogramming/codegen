package example.adapters.in.rest.controllers;

import example.commons.SerializationUtils;
import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.UploadEchoFileRequest;
import javax.annotation.processing.Generated;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileController {

    private final EchoFacade echoFacade;

    /**
     * upload file
     */
    @PostMapping(value = "/api/v1/echo-file/{id}", produces = {"application/json"}, consumes = {
            "application/octet-stream"})
    public ResponseEntity<Void> uploadEchoFile(@PathVariable Long id, @RequestBody @Valid Resource body) {
        UploadEchoFileRequest request = new UploadEchoFileRequest().setId(id).setBody(SerializationUtils.toBytes(body));
        echoFacade.uploadEchoFile(request);
        return ResponseEntity.noContent().build();
    }
}
