package example.adapters.in.rest.controllers;

import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.DeleteFileRequest;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DeleteFileController {

    private final EchoFacade echoFacade;

    /**
     * deletes a file based on the id supplied
     */
    @DeleteMapping(value = "/api/v1/echo-file/{id}", produces = {"application/json"})
    public ResponseEntity<Void> deleteFile(@PathVariable Long id) {
        DeleteFileRequest request = new DeleteFileRequest().setId(id);
        echoFacade.deleteFile(request);
        return ResponseEntity.noContent().build();
    }
}
