package example.adapters.in.rest.controllers;

import example.ports.in.rest.EchoFacade;
import example.ports.in.rest.dtos.DownloadEchoFileRequest;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileController {

    private final EchoFacade echoFacade;

    /**
     * Returns a file
     */
    @GetMapping(value = "/api/v1/echo-file/{id}", produces = {"image/jpeg", "application/json"})
    public ResponseEntity<byte[]> downloadEchoFile(@PathVariable Long id) {
        DownloadEchoFileRequest request = new DownloadEchoFileRequest().setId(id);
        return echoFacade.downloadEchoFile(request);
    }
}
