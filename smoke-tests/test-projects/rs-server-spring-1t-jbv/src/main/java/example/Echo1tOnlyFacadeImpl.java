package example;

import example.ports.in.rest.Echo1tOnlyFacade;
import example.ports.in.rest.dtos.EchoMultiContentMultiContentBodyRequest;
import example.ports.in.rest.dtos.EchoMultiContentTextPlainRequest;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Generated("pl.metaprogramming.codegen")
public class Echo1tOnlyFacadeImpl implements Echo1tOnlyFacade {

    @Override
    public ResponseEntity<Object> echoMultiContent(@Nonnull EchoMultiContentMultiContentBodyRequest request) {
        return null;
    }

    @Override
    public ResponseEntity<Object> echoMultiContent(@Nonnull EchoMultiContentTextPlainRequest request) {
        return null;
    }
}
