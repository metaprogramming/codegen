package example.ports.in.rest.dtos;

import java.util.List;
import javax.annotation.processing.Generated;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostRequest {

    /**
     * Authorization HEADER parameter.<br/>
     * The value of the Authorization header should consist of 'type' +
     * 'credentials', where for the approach using the 'type' token should be
     * 'Bearer'.
     */
    private String authorization;

    /**
     * Inline-Header-Param HEADER parameter.<br/>
     * Example header param
     */
    private String inlineHeaderParam;

    /**
     * body param
     */
    @NotNull
    private List<EchoArraysBodyDto> body;
}
