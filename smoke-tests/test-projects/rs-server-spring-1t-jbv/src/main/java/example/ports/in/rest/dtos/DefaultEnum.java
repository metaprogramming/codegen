package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonValue;
import example.commons.EnumValue;
import javax.annotation.processing.Generated;
import lombok.Getter;

/**
 * DefaultEnum title
 * 
 * DefaultEnum description
 */
@Generated("pl.metaprogramming.codegen")
public enum DefaultEnum implements EnumValue {

    A1("a1"), A2("a2");

    @Getter
    @JsonValue
    private final String value;

    DefaultEnum(String value) {
        this.value = value;
    }

    public static DefaultEnum fromValue(String value) {
        return EnumValue.fromValue(value, DefaultEnum.class);
    }
}
