package example.ports.in.rest.dtos;

import java.time.LocalDate;
import java.util.List;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetRequest {

    /**
     * date_array QUERY parameter.
     */
    private List<LocalDate> dateArray;
}
