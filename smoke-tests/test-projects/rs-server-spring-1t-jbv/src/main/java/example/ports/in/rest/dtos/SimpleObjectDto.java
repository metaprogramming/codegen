package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import javax.annotation.processing.Generated;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Simple object
 * 
 * Simple object for testing
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectDto {

    @JsonProperty("so_prop_string")
    @NotNull
    private String soPropString;
    @JsonProperty("so_prop_date")
    private LocalDate soPropDate;
}
