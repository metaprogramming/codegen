package example.ports.in.rest.dtos;

import javax.annotation.processing.Generated;
import javax.validation.Valid;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostRequest {

    /**
     * Default-Header-Param HEADER parameter.<br/>
     * Example header param with default value
     */
    private DefaultEnum defaultHeaderParam = DefaultEnum.A2;

    /**
     * body param
     */
    @Valid
    private EchoDefaultsBodyDto body;
}
