package example.ports.in.rest.dtos;

import example.commons.SerializationUtils;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.annotation.processing.Generated;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Echo object with default values
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsBodyDto {

    @Min(1)
    private Integer propInt = 101;
    @DecimalMin("1")
    private Float propFloat = 101.101f;
    @DecimalMin("1")
    private Double propDouble = 202.202;
    @DecimalMin("1")
    private BigDecimal propNumber = new BigDecimal("3.33");
    private String propString = "WHITE";
    private ReusableEnum propEnum = ReusableEnum.V_3;
    private LocalDate propDate = SerializationUtils.toLocalDate("2021-09-01");
    private ZonedDateTime propDateTime = ZonedDateTime.parse("2021-09-01T09:09:09Z");
}
