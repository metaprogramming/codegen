package example.ports.in.rest.dtos;

import example.process.IRequest;
import java.time.ZonedDateTime;
import javax.annotation.processing.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoPostRequest implements IRequest {

    /**
     * Authorization HEADER parameter.<br/>
     * The value of the Authorization header should consist of 'type' +
     * 'credentials', where for the approach using the 'type' token should be
     * 'Bearer'.
     */
    private String authorization;

    /**
     * X-Correlation-ID HEADER parameter.<br/>
     * Correlates HTTP requests between a client and server.
     */
    @NotNull
    private String xCorrelationId;

    /**
     * timestamp HEADER parameter.
     */
    private ZonedDateTime timestamp;

    /**
     * Inline-Header-Param HEADER parameter.<br/>
     * Example header param
     */
    private String inlineHeaderParam;

    /**
     * body param
     */
    @NotNull
    @Valid
    private EchoBodyDto requestBody;
}
