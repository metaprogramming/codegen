package example.ports.in.rest.dtos;

import java.util.List;
import javax.annotation.processing.Generated;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ErrorDescriptionDto {

    @NotNull
    private Integer code;
    @NotNull
    private String message;
    private List<ErrorDetailDto> errors;
}
