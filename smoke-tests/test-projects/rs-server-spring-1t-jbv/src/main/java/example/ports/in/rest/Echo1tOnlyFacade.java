package example.ports.in.rest;

import example.ports.in.rest.dtos.EchoMultiContentMultiContentBodyRequest;
import example.ports.in.rest.dtos.EchoMultiContentTextPlainRequest;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import org.springframework.http.ResponseEntity;

@Generated("pl.metaprogramming.codegen")
public interface Echo1tOnlyFacade {

    ResponseEntity<Object> echoMultiContent(@Nonnull EchoMultiContentMultiContentBodyRequest request);

    ResponseEntity<Object> echoMultiContent(@Nonnull EchoMultiContentTextPlainRequest request);
}
