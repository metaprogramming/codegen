package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonValue;
import example.commons.EnumValue;
import javax.annotation.processing.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public enum ResponseContentType implements EnumValue {

    APPLICATION_JSON("application/json"), APPLICATION_XML("application/xml"), TEXT_PLAIN("text/plain");

    @Getter
    @JsonValue
    private final String value;

    ResponseContentType(String value) {
        this.value = value;
    }
}
