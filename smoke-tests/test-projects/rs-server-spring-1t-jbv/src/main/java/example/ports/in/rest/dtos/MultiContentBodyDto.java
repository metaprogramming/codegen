package example.ports.in.rest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.processing.Generated;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class MultiContentBodyDto {

    @JsonProperty("response_content_type")
    @NotNull
    private ResponseContentType responseContentType;
    @JsonProperty("prop_int")
    private Integer propInt;
    @JsonProperty("prop_int2")
    private Integer propInt2;
    @JsonProperty("prop_date")
    private LocalDate propDate;
    @JsonProperty("prop_list")
    private List<String> propList;
}
