package example.ports.in.rest.dtos;

import example.process.IRequest;
import javax.annotation.processing.Generated;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetRequest implements IRequest {

    /**
     * Authorization HEADER parameter.
     */
    private String authorization;

    /**
     * api_key QUERY parameter.
     */
    private String apiKey;

    /**
     * X-Correlation-ID HEADER parameter.<br/>
     * Correlates HTTP requests between a client and server.
     */
    @NotNull
    private String xCorrelationId;

    /**
     * prop_int_required QUERY parameter.
     */
    @NotNull
    @Min(5)
    private Long propIntRequired;

    /**
     * prop_float QUERY parameter.
     */
    @DecimalMax("5.02")
    private Float propFloat;

    /**
     * prop_enum QUERY parameter.
     */
    private DefaultEnum propEnum;
}
