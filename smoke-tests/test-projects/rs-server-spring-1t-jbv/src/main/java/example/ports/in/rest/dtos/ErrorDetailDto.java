package example.ports.in.rest.dtos;

import javax.annotation.processing.Generated;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ErrorDetailDto {

    private String field;
    @NotNull
    private String code;
    @NotNull
    private String message;
}
