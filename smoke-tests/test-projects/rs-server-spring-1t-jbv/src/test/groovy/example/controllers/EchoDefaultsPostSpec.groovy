/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.controllers

class EchoDefaultsPostSpec extends BaseSpecification {

    @Override
    String getOperationPath() {
        'api/v1/echo-defaults'
    }

    def "should success"() {
        when:
        def response = call([:])

        then:
        response != null
        response.statusCode.value() == 200
        response.body != null
        // to "rewrite" the default header value you must manually modify the controller method
        // response.headers.getFirst('Default-Header-Param') == 'a2'

        when:
        def body = response.body

        then:
        body.propInt == 101
        body.propFloat == 101.101
        body.propDouble == 202.202
        body.propNumber == 3.33
        body.propString == 'WHITE'
        body.propEnum == '3'
        body.propDate == '2021-09-01'
        body.propDateTime == '2021-09-01T09:09:09Z'
    }
}