/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BaseSpecification extends Specification {

    static final String HEADER_CORRELATION_ID = 'X-Correlation-ID'

    @LocalServerPort
    private int port

    @Autowired
    TestRestTemplate restTemplate

    String getOperationPath() {}

    def getEndpoint(String operationPath = getOperationPath()) {
        "http://localhost:${port}/${operationPath}"
    }

    ResponseEntity<Map> call(def request) {
        call(operationPath, request)
    }

    ResponseEntity<Map> call(String operationPath, def request) {
        restTemplate.postForEntity(getEndpoint(operationPath), request, Map)
    }

    boolean checkCorrelationIdHeader(def response, def request) {
        response.getHeaders().getFirst(HEADER_CORRELATION_ID) == request.getHeaders().getFirst(HEADER_CORRELATION_ID)
    }

    def setHeader(String header, String defaultValue, HttpHeaders headers, Map params) {
        String key = 'header.' + header
        if (params.containsKey(key)) {
            if (params[key]) {
                headers.set(header, params[key] as String)
            }
        } else {
            if (defaultValue) {
                headers.set(header, defaultValue)
            }
        }
    }

}
