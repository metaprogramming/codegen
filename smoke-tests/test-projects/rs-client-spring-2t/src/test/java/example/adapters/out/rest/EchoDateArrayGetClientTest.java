/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.out.rest;

import example.ports.out.rest.dtos.EchoDateArrayGetRequest;
import example.ports.out.rest.dtos.EchoDateArrayGetResponse;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EchoDateArrayGetClientTest extends BaseClientTest {

    @Test
    void shouldSuccessEchoDateArrayWithoutItems() {
        EchoDateArrayGetResponse response = client.echoDateArrayGet(new EchoDateArrayGetRequest());
        assertTrue(response.is200());
        assertTrue(response.get200().isEmpty());
    }

    @Test
    void shouldSuccessEchoDateArrayWithSingleItem() {
        LocalDate date = LocalDate.now();
        EchoDateArrayGetResponse response = client.echoDateArrayGet(new EchoDateArrayGetRequest().setDateArray(Collections.singletonList(date)));
        assertTrue(response.is200());
        assertEquals(1, response.get200().size());
        assertEquals(date, response.get200().get(0));
    }

    @Test
    void shouldSuccessEchoDateArrayWithManyItems() {
        LocalDate d1 = LocalDate.now();
        LocalDate d2 = d1.minusDays(1);
        LocalDate d3 = d1.minusDays(2);
        EchoDateArrayGetResponse response = client.echoDateArrayGet(new EchoDateArrayGetRequest().setDateArray(Arrays.asList(d1, d2, d3)));
        assertTrue(response.is200());
        assertEquals(3, response.get200().size());
        assertEquals(d1, response.get200().get(0));
        assertEquals(d2, response.get200().get(1));
        assertEquals(d3, response.get200().get(2));
    }
}
