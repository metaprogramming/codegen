/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.out.rest;

import example.commons.ports.out.rest.dtos.ReusableEnumEnum;
import example.commons.ports.out.rest.dtos.SimpleObjectDto;
import example.ports.out.rest.dtos.EchoArraysBodyDto;
import example.ports.out.rest.dtos.EchoArraysPostRequest;
import example.ports.out.rest.dtos.EchoArraysPostResponse;
import example.ports.out.rest.dtos.EnumTypeEnum;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.junit.jupiter.api.Assertions.*;

class EchoArraysPostClientTest extends BaseClientTest {

    //@Test - opcja niewspierana w client 2T - tworzony jest response 200 zamiast 204
    void shouldSuccessEchoArrayWithoutItems() {
        EchoArraysPostResponse response = client.echoArraysPost(new EchoArraysPostRequest()
                .setAuthorizationParam(AUTH_TOKEN)
                .setBody(Collections.emptyList()));
        assertTrue(response.is204());
    }

    @Test
    void shouldSuccessEchoArrayWithSingleItem() {
        List<EchoArraysBodyDto> body = singletonList(createItem(1));
        EchoArraysPostResponse response = client.echoArraysPost(new EchoArraysPostRequest()
                .setAuthorizationParam(AUTH_TOKEN)
                .setBody(body)
        );
        assertTrue(response.is200());
        assertEquals(1, response.get200().size());
        assertEquals(body.get(0), response.get200().get(0));
    }

    @Test
    void shouldSuccessEchoArrayWithManyItems() {
        List<EchoArraysBodyDto> body = asList(createItem(1), createItem(2));
        EchoArraysPostResponse response = client.echoArraysPost(new EchoArraysPostRequest()
                .setAuthorizationParam(AUTH_TOKEN)
                .setBody(body)
        );
        assertTrue(response.is200());
        assertEquals(2, response.get200().size());
        assertEquals(body.get(0), response.get200().get(0));
        assertEquals(body.get(1), response.get200().get(1));
        assertNotEquals(body.get(0), response.get200().get(1));
    }

    @Test
    void shouldFailEchoArrayWithInvalidData() {
        List<EchoArraysBodyDto> body = singletonList(createItem(0));
        EchoArraysPostResponse response = client.echoArraysPost(new EchoArraysPostRequest()
                .setAuthorizationParam(AUTH_TOKEN)
                .setBody(body)
        );
        assertTrue(response.is400());
        assertEquals(400, response.getStatus());
        assertNotNull(response.get400().getErrors());
        assertFalse(response.get400().getErrors().isEmpty());
        assertEquals("[0].prop_double_list[0]", response.get400().getErrors().get(0).getField());
        assertEquals("is_too_small", response.get400().getErrors().get(0).getCode());
    }

    private EchoArraysBodyDto createItem(int factor) {
        return new EchoArraysBodyDto()
                .setPropAmountList(singletonList("123.10"))
                .setPropBooleanList(singletonList(true))
                .setPropDateList(singletonList(LocalDate.now()))
                .setPropDateTimeListOfList(singletonList(asList(LocalDateTime.now(), LocalDateTime.now().minusDays(1))))
                .setPropDoubleList(asList(12.12 * factor, 11.11 * (factor + 1)))
                .setPropEnumList(singletonList(EnumTypeEnum.V_1))
                .setPropEnumReusableList(singletonList(ReusableEnumEnum.A))
                .setPropFloatList(singletonList(1.1f * factor))
                .setPropIntList(singletonList(factor))
                .setPropObjectList(singletonList(new SimpleObjectDto().setSoPropString("x").setSoPropDate(LocalDate.now())))
                .setPropObjectListOfList(singletonList(singletonList(new SimpleObjectDto().setSoPropString("y").setSoPropDate(LocalDate.now()))))
                .setPropStringList(singletonList("MOUSE"))
                .setPropMapOfInt(singletonMap("k1", 1))
                .setPropMapOfObject(singletonMap("k1o", new SimpleObjectDto().setSoPropString("xm1").setSoPropDate(LocalDate.now())))
                .setPropMapOfListOfObject(singletonMap("k1o", singletonList(new SimpleObjectDto().setSoPropString("xm2").setSoPropDate(LocalDate.now()))))
                ;
    }
}
