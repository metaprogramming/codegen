/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package example.adapters.out.rest;

import example.ports.out.rest.dtos.*;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EchoFileClientTest extends BaseClientTest {
    private static final String TEST_FILE_BODY = "Test file";
    private Long testFileId = System.currentTimeMillis();

    @Test
    void shouldUploadWithStreamAndDownloadAndDelete() {
        uploadWithStream();
        download();
        delete();
    }

    @Test
    void shouldUploadWithFormAndDownloadAndDelete() {
        uploadWithForm();
        download();
        delete();
    }

    private void uploadWithForm() {
        UploadEchoFileWithFormResponse response = client.uploadEchoFileWithForm(new UploadEchoFileWithFormRequest().setFile(fileResource()).setId(testFileId));
        assertTrue(response.is204());
    }

    private void uploadWithStream() {
        UploadEchoFileResponse response = client.uploadEchoFile(new UploadEchoFileRequest().setBody(fileResource()).setId(testFileId));
        assertTrue(response.is204());
    }

    @SneakyThrows
    private void download() {
        DownloadEchoFileResponse response = client.downloadEchoFile(new DownloadEchoFileRequest().setId(testFileId));
        assertTrue(response.is200());
        String result = IOUtils.toString(response.get200().getInputStream(), StandardCharsets.UTF_8);
        assertEquals(TEST_FILE_BODY, result);
    }

    private void delete() {
        DeleteFileResponse response = client.deleteFile(new DeleteFileRequest().setId(testFileId));
        assertTrue(response.is204());
        DownloadEchoFileResponse downloadResponse = client.downloadEchoFile(new DownloadEchoFileRequest().setId(testFileId));
        assertTrue(downloadResponse.is404());
    }

    private Resource fileResource() {
        return new ByteArrayResource(TEST_FILE_BODY.getBytes(StandardCharsets.UTF_8)) {
            @Override
            public String getFilename() {
                return "test.txt";
            }
        };
    }
}
