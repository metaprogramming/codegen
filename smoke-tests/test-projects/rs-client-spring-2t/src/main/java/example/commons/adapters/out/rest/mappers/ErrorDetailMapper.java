package example.commons.adapters.out.rest.mappers;

import example.commons.adapters.out.rest.dtos.ErrorDetailRdto;
import example.commons.ports.out.rest.dtos.ErrorDetailDto;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class ErrorDetailMapper {

    public ErrorDetailDto map2ErrorDetailDto(ErrorDetailRdto value) {
        return value == null ? null : map(new ErrorDetailDto(), value);
    }

    public ErrorDetailDto map(ErrorDetailDto result, ErrorDetailRdto value) {
        return result.setField(value.getField()).setCode(value.getCode()).setMessage(value.getMessage());
    }
}
