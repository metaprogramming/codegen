package example.commons;

import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface EnumValue {

    String getValue();

    default boolean equalsValue(String value) {
        return getValue().equals(value);
    }

    static <T extends EnumValue> T fromValue(String value, Class<T> enumClass) {
        if (value == null)
            return null;
        for (T e : enumClass.getEnumConstants()) {
            if (e.equalsValue(value))
                return e;
        }
        throw new IllegalArgumentException(
                String.format("Unknown value '%s' of enum '%s'", value, enumClass.getCanonicalName()));
    }
}
