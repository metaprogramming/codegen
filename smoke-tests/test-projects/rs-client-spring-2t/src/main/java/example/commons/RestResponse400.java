package example.commons;

import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface RestResponse400<R, T> extends RestResponse<R> {

    default boolean is400() {
        return isStatus(400);
    }

    @SuppressWarnings("unchecked")
    default T get400() {
        if (is400()) {
            return (T) getBody();
        }
        throw new IllegalStateException("Response 400 is not set");
    }
}
