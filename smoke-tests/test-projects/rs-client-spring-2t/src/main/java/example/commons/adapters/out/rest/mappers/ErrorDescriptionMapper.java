package example.commons.adapters.out.rest.mappers;

import example.commons.SerializationUtils;
import example.commons.adapters.out.rest.dtos.ErrorDescriptionRdto;
import example.commons.ports.out.rest.dtos.ErrorDescriptionDto;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class ErrorDescriptionMapper {

    private final ErrorDetailMapper errorDetailMapper;

    public ErrorDescriptionDto map2ErrorDescriptionDto(ErrorDescriptionRdto value) {
        return value == null ? null : map(new ErrorDescriptionDto(), value);
    }

    public ErrorDescriptionDto map(ErrorDescriptionDto result, ErrorDescriptionRdto value) {
        return result.setCode(SerializationUtils.toInteger(value.getCode())).setMessage(value.getMessage())
                .setErrors(SerializationUtils.transformList(value.getErrors(), errorDetailMapper::map2ErrorDetailDto));
    }
}
