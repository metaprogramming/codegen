package example.commons.ports.out.rest.dtos;

import example.commons.EnumValue;
import javax.annotation.processing.Generated;
import lombok.Getter;

/**
 * Reusable enum type title
 * 
 * Reusable enum type description
 */
@Generated("pl.metaprogramming.codegen")
public enum ReusableEnumEnum implements EnumValue {

    A("a"), // a value
    B("B"), // B value
    V_3("3"); // 3 value

    @Getter
    private final String value;

    ReusableEnumEnum(String value) {
        this.value = value;
    }

    public static ReusableEnumEnum fromValue(String value) {
        return EnumValue.fromValue(value, ReusableEnumEnum.class);
    }
}
