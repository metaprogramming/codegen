package example.commons.adapters.out.rest.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Simple object
 * 
 * Simple object for testing
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class SimpleObjectRdto {

    @JsonProperty("so_prop_string")
    private String soPropString;
    @JsonProperty("so_prop_date")
    private String soPropDate;
}
