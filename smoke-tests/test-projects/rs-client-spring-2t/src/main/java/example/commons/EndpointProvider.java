package example.commons;

import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class EndpointProvider {

    @Value("${BASE_URL:http://localhost:8080}")
    private String baseUrl;

    public String getEndpoint(String path) {
        return baseUrl + path;
    }
}
