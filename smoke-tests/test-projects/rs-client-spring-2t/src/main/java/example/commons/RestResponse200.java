package example.commons;

import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface RestResponse200<R, T> extends RestResponse<R> {

    default boolean is200() {
        return isStatus(200);
    }

    @SuppressWarnings("unchecked")
    default T get200() {
        if (is200()) {
            return (T) getBody();
        }
        throw new IllegalStateException("Response 200 is not set");
    }
}
