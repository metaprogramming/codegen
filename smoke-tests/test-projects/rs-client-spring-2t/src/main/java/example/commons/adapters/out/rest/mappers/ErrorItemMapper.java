package example.commons.adapters.out.rest.mappers;

import example.commons.adapters.out.rest.dtos.ErrorItemRdto;
import example.commons.ports.out.rest.dtos.ErrorItemDto;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class ErrorItemMapper {

    public ErrorItemDto map2ErrorItemDto(ErrorItemRdto value) {
        return value == null ? null : map(new ErrorItemDto(), value);
    }

    public ErrorItemDto map(ErrorItemDto result, ErrorItemRdto value) {
        return result.setCode(value.getCode());
    }
}
