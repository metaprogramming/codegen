package example.ports.out.rest.dtos;

import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.core.io.Resource;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileRequest {

    /**
     * id of file
     */
    @Nonnull
    private Long id;

    /**
     * file data to upload
     */
    @Nonnull
    private Resource body;
}
