package example.ports.out.rest.dtos;

import example.commons.EnumValue;
import javax.annotation.processing.Generated;
import lombok.Getter;

/**
 * enum property
 */
@Generated("pl.metaprogramming.codegen")
public enum EnumTypeEnum implements EnumValue {

    A("A"), // A value
    B("b"), // b value
    V_1("1"); // 1 value

    @Getter
    private final String value;

    EnumTypeEnum(String value) {
        this.value = value;
    }

    public static EnumTypeEnum fromValue(String value) {
        return EnumValue.fromValue(value, EnumTypeEnum.class);
    }
}
