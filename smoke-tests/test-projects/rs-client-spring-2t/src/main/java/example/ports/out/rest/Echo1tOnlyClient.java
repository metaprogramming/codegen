package example.ports.out.rest;

import example.ports.out.rest.dtos.EchoMultiContentRequest;
import example.ports.out.rest.dtos.EchoMultiContentResponse;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface Echo1tOnlyClient {

    EchoMultiContentResponse echoMultiContent(@Nonnull EchoMultiContentRequest request);
}
