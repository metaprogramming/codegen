package example.ports.out.rest.dtos;

import example.commons.RestResponse204NoContent;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.out.rest.dtos.ErrorDescriptionDto;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileResponse extends RestResponseBase<UploadEchoFileResponse>
        implements
            RestResponse204NoContent<UploadEchoFileResponse>,
            RestResponseOther<UploadEchoFileResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(204);

    private UploadEchoFileResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public UploadEchoFileResponse self() {
        return this;
    }

    public static UploadEchoFileResponse set204() {
        return new UploadEchoFileResponse(204, null);
    }

    public static UploadEchoFileResponse setOther(Integer status, ErrorDescriptionDto body) {
        if (DECLARED_STATUSES.contains(status)) {
            throw new IllegalArgumentException(
                    String.format("Status %s is declared. Use dedicated factory method for it.", status));
        }
        return new UploadEchoFileResponse(status, body);
    }
}
