package example.ports.out.rest.dtos;

import java.time.LocalDate;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class MultiContentBodyDto {

    @Nonnull
    private ResponseContentTypeEnum responseContentType;
    @Nullable
    private Integer propInt;
    @Nullable
    private Integer propInt2;
    @Nullable
    private LocalDate propDate;
    @Nullable
    private List<String> propList;
}
