package example.ports.out.rest;

import example.ports.out.rest.dtos.DeleteFileRequest;
import example.ports.out.rest.dtos.DeleteFileResponse;
import example.ports.out.rest.dtos.DownloadEchoFileRequest;
import example.ports.out.rest.dtos.DownloadEchoFileResponse;
import example.ports.out.rest.dtos.EchoArraysPostRequest;
import example.ports.out.rest.dtos.EchoArraysPostResponse;
import example.ports.out.rest.dtos.EchoDateArrayGetRequest;
import example.ports.out.rest.dtos.EchoDateArrayGetResponse;
import example.ports.out.rest.dtos.EchoDefaultsPostRequest;
import example.ports.out.rest.dtos.EchoDefaultsPostResponse;
import example.ports.out.rest.dtos.EchoEmptyRequest;
import example.ports.out.rest.dtos.EchoEmptyResponse;
import example.ports.out.rest.dtos.EchoErrorRequest;
import example.ports.out.rest.dtos.EchoErrorResponse;
import example.ports.out.rest.dtos.EchoGetRequest;
import example.ports.out.rest.dtos.EchoGetResponse;
import example.ports.out.rest.dtos.EchoPostRequest;
import example.ports.out.rest.dtos.EchoPostResponse;
import example.ports.out.rest.dtos.UploadEchoFileRequest;
import example.ports.out.rest.dtos.UploadEchoFileResponse;
import example.ports.out.rest.dtos.UploadEchoFileWithFormRequest;
import example.ports.out.rest.dtos.UploadEchoFileWithFormResponse;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface EchoClient {

    EchoPostResponse echoPost(@Nonnull EchoPostRequest request);

    EchoGetResponse echoGet(@Nonnull EchoGetRequest request);

    EchoDefaultsPostResponse echoDefaultsPost(@Nonnull EchoDefaultsPostRequest request);

    EchoArraysPostResponse echoArraysPost(@Nonnull EchoArraysPostRequest request);

    EchoDateArrayGetResponse echoDateArrayGet(@Nonnull EchoDateArrayGetRequest request);

    /**
     * upload file using multipart/form-data
     */
    UploadEchoFileWithFormResponse uploadEchoFileWithForm(@Nonnull UploadEchoFileWithFormRequest request);

    /**
     * upload file
     */
    UploadEchoFileResponse uploadEchoFile(@Nonnull UploadEchoFileRequest request);

    /**
     * Returns a file
     */
    DownloadEchoFileResponse downloadEchoFile(@Nonnull DownloadEchoFileRequest request);

    /**
     * deletes a file based on the id supplied
     */
    DeleteFileResponse deleteFile(@Nonnull DeleteFileRequest request);

    EchoEmptyResponse echoEmpty(@Nonnull EchoEmptyRequest request);

    EchoErrorResponse echoError(@Nonnull EchoErrorRequest request);
}
