package example.ports.out.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponse404NoContent;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.out.rest.dtos.ErrorDescriptionDto;
import java.util.Arrays;
import java.util.Collection;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;
import org.springframework.core.io.Resource;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileResponse extends RestResponseBase<DownloadEchoFileResponse>
        implements
            RestResponse200<DownloadEchoFileResponse, Resource>,
            RestResponse404NoContent<DownloadEchoFileResponse>,
            RestResponseOther<DownloadEchoFileResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Arrays.asList(200, 404);

    private DownloadEchoFileResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public DownloadEchoFileResponse self() {
        return this;
    }

    public static DownloadEchoFileResponse set200(Resource body) {
        return new DownloadEchoFileResponse(200, body);
    }

    public static DownloadEchoFileResponse set404() {
        return new DownloadEchoFileResponse(404, null);
    }

    public static DownloadEchoFileResponse setOther(Integer status, ErrorDescriptionDto body) {
        if (DECLARED_STATUSES.contains(status)) {
            throw new IllegalArgumentException(
                    String.format("Status %s is declared. Use dedicated factory method for it.", status));
        }
        return new DownloadEchoFileResponse(status, body);
    }
}
