package example.ports.out.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponseBase;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentResponse extends RestResponseBase<EchoMultiContentResponse>
        implements
            RestResponse200<EchoMultiContentResponse, MultiContentBodyDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(200);

    private EchoMultiContentResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoMultiContentResponse self() {
        return this;
    }

    public static EchoMultiContentResponse set200(MultiContentBodyDto body) {
        return new EchoMultiContentResponse(200, body);
    }
}
