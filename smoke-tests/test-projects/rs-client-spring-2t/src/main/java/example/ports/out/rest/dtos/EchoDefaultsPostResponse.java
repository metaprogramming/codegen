package example.ports.out.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.out.rest.dtos.ErrorDescriptionDto;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostResponse extends RestResponseBase<EchoDefaultsPostResponse>
        implements
            RestResponse200<EchoDefaultsPostResponse, EchoDefaultsBodyDto>,
            RestResponseOther<EchoDefaultsPostResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(200);

    private EchoDefaultsPostResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoDefaultsPostResponse self() {
        return this;
    }

    public static EchoDefaultsPostResponse set200(EchoDefaultsBodyDto body) {
        return new EchoDefaultsPostResponse(200, body);
    }

    public static EchoDefaultsPostResponse setOther(Integer status, ErrorDescriptionDto body) {
        if (DECLARED_STATUSES.contains(status)) {
            throw new IllegalArgumentException(
                    String.format("Status %s is declared. Use dedicated factory method for it.", status));
        }
        return new EchoDefaultsPostResponse(status, body);
    }
}
