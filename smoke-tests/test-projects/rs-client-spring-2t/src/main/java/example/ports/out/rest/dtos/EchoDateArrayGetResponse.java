package example.ports.out.rest.dtos;

import example.commons.RestResponse200;
import example.commons.RestResponseBase;
import example.commons.RestResponseOther;
import example.commons.ports.out.rest.dtos.ErrorDescriptionDto;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetResponse extends RestResponseBase<EchoDateArrayGetResponse>
        implements
            RestResponse200<EchoDateArrayGetResponse, List<LocalDate>>,
            RestResponseOther<EchoDateArrayGetResponse, ErrorDescriptionDto> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(200);

    private EchoDateArrayGetResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoDateArrayGetResponse self() {
        return this;
    }

    public static EchoDateArrayGetResponse set200(List<LocalDate> body) {
        return new EchoDateArrayGetResponse(200, body);
    }

    public static EchoDateArrayGetResponse setOther(Integer status, ErrorDescriptionDto body) {
        if (DECLARED_STATUSES.contains(status)) {
            throw new IllegalArgumentException(
                    String.format("Status %s is declared. Use dedicated factory method for it.", status));
        }
        return new EchoDateArrayGetResponse(status, body);
    }
}
