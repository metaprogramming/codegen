package example.ports.out.rest.dtos;

import example.commons.RestResponse200NoContent;
import example.commons.RestResponseBase;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.processing.Generated;

@ParametersAreNonnullByDefault
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyResponse extends RestResponseBase<EchoEmptyResponse>
        implements
            RestResponse200NoContent<EchoEmptyResponse> {

    private static final Collection<Integer> DECLARED_STATUSES = Collections.singletonList(200);

    private EchoEmptyResponse(Integer status, Object body) {
        super(status, body);
    }

    public Collection<Integer> getDeclaredStatuses() {
        return DECLARED_STATUSES;
    }

    public EchoEmptyResponse self() {
        return this;
    }

    public static EchoEmptyResponse set200() {
        return new EchoEmptyResponse(200, null);
    }
}
