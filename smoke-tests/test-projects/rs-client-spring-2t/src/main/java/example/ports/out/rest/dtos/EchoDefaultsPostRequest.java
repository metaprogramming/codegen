package example.ports.out.rest.dtos;

import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostRequest {

    /**
     * Example header param with default value
     */
    @Nullable
    private DefaultEnumEnum defaultHeaderParam;

    /**
     * body param
     */
    @Nullable
    private EchoDefaultsBodyDto body;
}
