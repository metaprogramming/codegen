package example.ports.out.rest.dtos;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.processing.Generated;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class EchoGetRequest {

    @Nullable
    private String authorization;
    @Nullable
    private String apiKey;

    /**
     * Correlates HTTP requests between a client and server.
     */
    @Nonnull
    private String correlationIdParam;
    @Nonnull
    private Long propIntRequired;
    @Nullable
    private Float propFloat;
    @Nullable
    private DefaultEnumEnum propEnum;
}
