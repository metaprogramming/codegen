package example.ports.out.rest.dtos;

import example.commons.EnumValue;
import javax.annotation.processing.Generated;
import lombok.Getter;

@Generated("pl.metaprogramming.codegen")
public enum ResponseContentTypeEnum implements EnumValue {

    APPLICATION_JSON("application/json"), APPLICATION_XML("application/xml"), TEXT_PLAIN("text/plain");

    @Getter
    private final String value;

    ResponseContentTypeEnum(String value) {
        this.value = value;
    }

    public static ResponseContentTypeEnum fromValue(String value) {
        return EnumValue.fromValue(value, ResponseContentTypeEnum.class);
    }
}
