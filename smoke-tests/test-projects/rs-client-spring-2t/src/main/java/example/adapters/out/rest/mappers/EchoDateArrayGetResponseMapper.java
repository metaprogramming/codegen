package example.adapters.out.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import example.commons.SerializationUtils;
import example.commons.adapters.out.rest.dtos.ErrorDescriptionRdto;
import example.commons.adapters.out.rest.mappers.ErrorDescriptionMapper;
import example.ports.out.rest.dtos.EchoDateArrayGetResponse;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

@Slf4j
@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDateArrayGetResponseMapper {

    private final ErrorDescriptionMapper errorDescriptionMapper;

    public EchoDateArrayGetResponse map(@Nonnull ResponseEntity<List<String>> responseEntity) {
        return EchoDateArrayGetResponse
                .set200(SerializationUtils.transformList(responseEntity.getBody(), SerializationUtils::toLocalDate));
    }

    public EchoDateArrayGetResponse map(HttpStatusCodeException e) {
        try {
            ErrorDescriptionRdto restValue = new ObjectMapper().readValue(e.getResponseBodyAsString(),
                    ErrorDescriptionRdto.class);
            return EchoDateArrayGetResponse.setOther(e.getRawStatusCode(),
                    errorDescriptionMapper.map2ErrorDescriptionDto(restValue));
        } catch (JsonProcessingException e2) {
            log.error("Can't deserialize data", e2);
            throw e;
        }
    }
}
