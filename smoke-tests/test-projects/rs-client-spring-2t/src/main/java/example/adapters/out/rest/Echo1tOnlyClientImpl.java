package example.adapters.out.rest;

import example.adapters.out.rest.dtos.MultiContentBodyRdto;
import example.adapters.out.rest.mappers.EchoMultiContentRequestMapper;
import example.adapters.out.rest.mappers.EchoMultiContentResponseMapper;
import example.ports.out.rest.Echo1tOnlyClient;
import example.ports.out.rest.dtos.EchoMultiContentRequest;
import example.ports.out.rest.dtos.EchoMultiContentResponse;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class Echo1tOnlyClientImpl implements Echo1tOnlyClient {

    private final RestTemplate restTemplate;
    private final EchoMultiContentRequestMapper echoMultiContentRequestMapper;
    private final EchoMultiContentResponseMapper echoMultiContentResponseMapper;

    @Override
    public EchoMultiContentResponse echoMultiContent(@Nonnull EchoMultiContentRequest request) {
        try {
            return echoMultiContentResponseMapper
                    .map(restTemplate.exchange(echoMultiContentRequestMapper.map(request), MultiContentBodyRdto.class));
        } catch (HttpStatusCodeException e) {
            return echoMultiContentResponseMapper.map(e);
        }
    }
}
