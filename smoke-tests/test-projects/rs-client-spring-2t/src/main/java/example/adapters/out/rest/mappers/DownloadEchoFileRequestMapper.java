package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.DownloadEchoFileRrequest;
import example.commons.EndpointProvider;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.DownloadEchoFileRequest;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DownloadEchoFileRequestMapper {

    private final EndpointProvider endpointProvider;

    public RequestEntity<Void> map(@Nonnull DownloadEchoFileRequest request) {
        DownloadEchoFileRrequest rawRequest = map2DownloadEchoFileRrequest(request);
        Map<String, String> pathParams = new HashMap<>();
        pathParams.put("id", rawRequest.getId());
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo-file/{id}"))
                .build(pathParams);
        return RequestEntity.get(uri).build();
    }

    private DownloadEchoFileRrequest map2DownloadEchoFileRrequest(DownloadEchoFileRequest value) {
        return new DownloadEchoFileRrequest().setId(SerializationUtils.toString(value.getId()));
    }
}
