package example.adapters.out.rest.mappers;

import example.ports.out.rest.dtos.EchoEmptyResponse;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

@Component
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyResponseMapper {

    public EchoEmptyResponse map(@Nonnull ResponseEntity<?> responseEntity) {
        return EchoEmptyResponse.set200();
    }

    public EchoEmptyResponse map(HttpStatusCodeException e) {
        throw e;
    }
}
