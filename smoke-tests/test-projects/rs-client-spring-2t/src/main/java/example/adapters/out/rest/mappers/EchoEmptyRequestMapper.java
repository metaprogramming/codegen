package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.EchoEmptyRrequest;
import example.commons.EndpointProvider;
import example.ports.out.rest.dtos.EchoEmptyRequest;
import java.net.URI;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoEmptyRequestMapper {

    private final EndpointProvider endpointProvider;

    public RequestEntity<Void> map(@Nonnull EchoEmptyRequest request) {
        EchoEmptyRrequest rawRequest = map2EchoEmptyRrequest(request);
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo-empty")).build()
                .toUri();
        return RequestEntity.get(uri).build();
    }

    private EchoEmptyRrequest map2EchoEmptyRrequest(EchoEmptyRequest value) {
        return new EchoEmptyRrequest();
    }
}
