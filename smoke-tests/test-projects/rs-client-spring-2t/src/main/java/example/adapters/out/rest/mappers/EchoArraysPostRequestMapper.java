package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.EchoArraysBodyRdto;
import example.adapters.out.rest.dtos.EchoArraysPostRrequest;
import example.commons.EndpointProvider;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.EchoArraysPostRequest;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoArraysPostRequestMapper {

    private final EndpointProvider endpointProvider;
    private final EchoArraysBodyMapper echoArraysBodyMapper;

    public RequestEntity<List<EchoArraysBodyRdto>> map(@Nonnull EchoArraysPostRequest request) {
        EchoArraysPostRrequest rawRequest = map2EchoArraysPostRrequest(request);
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo-arrays")).build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        Optional.ofNullable(rawRequest.getAuthorizationParam()).ifPresent(v -> headers.add("Authorization", v));
        Optional.ofNullable(rawRequest.getInlineHeaderParam()).ifPresent(v -> headers.add("Inline-Header-Param", v));
        return RequestEntity.post(uri).headers(headers).body(rawRequest.getBody());
    }

    private EchoArraysPostRrequest map2EchoArraysPostRrequest(EchoArraysPostRequest value) {
        return new EchoArraysPostRrequest().setAuthorizationParam(value.getAuthorizationParam())
                .setInlineHeaderParam(value.getInlineHeaderParam()).setBody(SerializationUtils
                        .transformList(value.getBody(), echoArraysBodyMapper::map2EchoArraysBodyRdto));
    }
}
