package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.EchoDefaultsBodyRdto;
import example.adapters.out.rest.dtos.EchoDefaultsPostRrequest;
import example.commons.EndpointProvider;
import example.ports.out.rest.dtos.DefaultEnumEnum;
import example.ports.out.rest.dtos.EchoDefaultsPostRequest;
import java.net.URI;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoDefaultsPostRequestMapper {

    private final EndpointProvider endpointProvider;
    private final EchoDefaultsBodyMapper echoDefaultsBodyMapper;

    public RequestEntity<EchoDefaultsBodyRdto> map(@Nonnull EchoDefaultsPostRequest request) {
        EchoDefaultsPostRrequest rawRequest = map2EchoDefaultsPostRrequest(request);
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo-defaults")).build()
                .toUri();
        HttpHeaders headers = new HttpHeaders();
        Optional.ofNullable(rawRequest.getDefaultHeaderParam()).ifPresent(v -> headers.add("Default-Header-Param", v));
        return RequestEntity.post(uri).headers(headers).body(rawRequest.getBody());
    }

    private EchoDefaultsPostRrequest map2EchoDefaultsPostRrequest(EchoDefaultsPostRequest value) {
        return new EchoDefaultsPostRrequest()
                .setDefaultHeaderParam(
                        Optional.ofNullable(value.getDefaultHeaderParam()).map(DefaultEnumEnum::getValue).orElse(null))
                .setBody(echoDefaultsBodyMapper.map2EchoDefaultsBodyRdto(value.getBody()));
    }
}
