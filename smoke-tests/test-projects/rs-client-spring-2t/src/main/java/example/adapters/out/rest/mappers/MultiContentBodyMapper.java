package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.MultiContentBodyRdto;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.MultiContentBodyDto;
import example.ports.out.rest.dtos.ResponseContentTypeEnum;
import java.util.Optional;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Component
@Generated("pl.metaprogramming.codegen")
public class MultiContentBodyMapper {

    public MultiContentBodyDto map2MultiContentBodyDto(MultiContentBodyRdto value) {
        return value == null ? null : map(new MultiContentBodyDto(), value);
    }

    public MultiContentBodyRdto map2MultiContentBodyRdto(MultiContentBodyDto value) {
        return value == null ? null : map(new MultiContentBodyRdto(), value);
    }

    public MultiContentBodyDto map(MultiContentBodyDto result, MultiContentBodyRdto value) {
        return result.setResponseContentType(ResponseContentTypeEnum.fromValue(value.getResponseContentType()))
                .setPropInt(SerializationUtils.toInteger(value.getPropInt()))
                .setPropInt2(SerializationUtils.toInteger(value.getPropInt2()))
                .setPropDate(SerializationUtils.toLocalDate(value.getPropDate()))
                .setPropList(SerializationUtils.transformList(value.getPropList(), v -> v));
    }

    public MultiContentBodyRdto map(MultiContentBodyRdto result, MultiContentBodyDto value) {
        return result
                .setResponseContentType(Optional.ofNullable(value.getResponseContentType())
                        .map(ResponseContentTypeEnum::getValue).orElse(null))
                .setPropInt(SerializationUtils.toString(value.getPropInt()))
                .setPropInt2(SerializationUtils.toString(value.getPropInt2()))
                .setPropDate(SerializationUtils.toString(value.getPropDate()))
                .setPropList(SerializationUtils.transformList(value.getPropList(), v -> v));
    }
}
