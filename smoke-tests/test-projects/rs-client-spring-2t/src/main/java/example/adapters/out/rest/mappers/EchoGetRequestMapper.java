package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.EchoGetRrequest;
import example.commons.EndpointProvider;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.DefaultEnumEnum;
import example.ports.out.rest.dtos.EchoGetRequest;
import java.net.URI;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoGetRequestMapper {

    private final EndpointProvider endpointProvider;

    public RequestEntity<Void> map(@Nonnull EchoGetRequest request) {
        EchoGetRrequest rawRequest = map2EchoGetRrequest(request);
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo"))
                .queryParam("api_key", rawRequest.getApiKey())
                .queryParam("prop_int_required", rawRequest.getPropIntRequired())
                .queryParam("prop_float", rawRequest.getPropFloat()).queryParam("prop_enum", rawRequest.getPropEnum())
                .build().toUri();
        HttpHeaders headers = new HttpHeaders();
        Optional.ofNullable(rawRequest.getAuthorization()).ifPresent(v -> headers.add("Authorization", v));
        Optional.ofNullable(rawRequest.getCorrelationIdParam()).ifPresent(v -> headers.add("X-Correlation-ID", v));
        return RequestEntity.get(uri).headers(headers).build();
    }

    private EchoGetRrequest map2EchoGetRrequest(EchoGetRequest value) {
        return new EchoGetRrequest().setAuthorization(value.getAuthorization()).setApiKey(value.getApiKey())
                .setCorrelationIdParam(value.getCorrelationIdParam())
                .setPropIntRequired(SerializationUtils.toString(value.getPropIntRequired()))
                .setPropFloat(SerializationUtils.toString(value.getPropFloat()))
                .setPropEnum(Optional.ofNullable(value.getPropEnum()).map(DefaultEnumEnum::getValue).orElse(null));
    }
}
