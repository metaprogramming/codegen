package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.MultiContentBodyRdto;
import example.ports.out.rest.dtos.EchoMultiContentResponse;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentResponseMapper {

    private final MultiContentBodyMapper multiContentBodyMapper;

    public EchoMultiContentResponse map(@Nonnull ResponseEntity<MultiContentBodyRdto> responseEntity) {
        return EchoMultiContentResponse
                .set200(multiContentBodyMapper.map2MultiContentBodyDto(responseEntity.getBody()));
    }

    public EchoMultiContentResponse map(HttpStatusCodeException e) {
        throw e;
    }
}
