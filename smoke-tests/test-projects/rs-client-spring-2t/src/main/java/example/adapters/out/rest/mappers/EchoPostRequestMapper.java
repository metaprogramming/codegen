package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.EchoBodyRdto;
import example.adapters.out.rest.dtos.EchoPostRrequest;
import example.commons.EndpointProvider;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.EchoPostRequest;
import java.net.URI;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoPostRequestMapper {

    private final EndpointProvider endpointProvider;
    private final EchoBodyMapper echoBodyMapper;

    public RequestEntity<EchoBodyRdto> map(@Nonnull EchoPostRequest request) {
        EchoPostRrequest rawRequest = map2EchoPostRrequest(request);
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo")).build().toUri();
        HttpHeaders headers = new HttpHeaders();
        Optional.ofNullable(rawRequest.getAuthorizationParam()).ifPresent(v -> headers.add("Authorization", v));
        Optional.ofNullable(rawRequest.getCorrelationIdParam()).ifPresent(v -> headers.add("X-Correlation-ID", v));
        Optional.ofNullable(rawRequest.getTimestampParam()).ifPresent(v -> headers.add("timestamp", v));
        Optional.ofNullable(rawRequest.getInlineHeaderParam()).ifPresent(v -> headers.add("Inline-Header-Param", v));
        return RequestEntity.post(uri).headers(headers).body(rawRequest.getRequestBody());
    }

    private EchoPostRrequest map2EchoPostRrequest(EchoPostRequest value) {
        return new EchoPostRrequest().setAuthorizationParam(value.getAuthorizationParam())
                .setCorrelationIdParam(value.getCorrelationIdParam())
                .setTimestampParam(SerializationUtils.toString(value.getTimestampParam()))
                .setInlineHeaderParam(value.getInlineHeaderParam())
                .setRequestBody(echoBodyMapper.map2EchoBodyRdto(value.getRequestBody()));
    }
}
