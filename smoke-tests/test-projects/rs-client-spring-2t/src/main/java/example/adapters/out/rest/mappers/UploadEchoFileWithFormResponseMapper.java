package example.adapters.out.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import example.commons.adapters.out.rest.dtos.ErrorDescriptionRdto;
import example.commons.adapters.out.rest.mappers.ErrorDescriptionMapper;
import example.ports.out.rest.dtos.UploadEchoFileWithFormResponse;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

@Slf4j
@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormResponseMapper {

    private final ErrorDescriptionMapper errorDescriptionMapper;

    public UploadEchoFileWithFormResponse map(@Nonnull ResponseEntity<?> responseEntity) {
        return UploadEchoFileWithFormResponse.set204();
    }

    public UploadEchoFileWithFormResponse map(HttpStatusCodeException e) {
        try {
            ErrorDescriptionRdto restValue = new ObjectMapper().readValue(e.getResponseBodyAsString(),
                    ErrorDescriptionRdto.class);
            return UploadEchoFileWithFormResponse.setOther(e.getRawStatusCode(),
                    errorDescriptionMapper.map2ErrorDescriptionDto(restValue));
        } catch (JsonProcessingException e2) {
            log.error("Can't deserialize data", e2);
            throw e;
        }
    }
}
