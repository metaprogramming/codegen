package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.EchoMultiContentRrequest;
import example.adapters.out.rest.dtos.MultiContentBodyRdto;
import example.commons.EndpointProvider;
import example.ports.out.rest.dtos.EchoMultiContentRequest;
import java.net.URI;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class EchoMultiContentRequestMapper {

    private final EndpointProvider endpointProvider;
    private final MultiContentBodyMapper multiContentBodyMapper;

    public RequestEntity<MultiContentBodyRdto> map(@Nonnull EchoMultiContentRequest request) {
        EchoMultiContentRrequest rawRequest = map2EchoMultiContentRrequest(request);
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo-multi-content")).build()
                .toUri();
        return RequestEntity.post(uri).body(rawRequest.getBody());
    }

    private EchoMultiContentRrequest map2EchoMultiContentRrequest(EchoMultiContentRequest value) {
        return new EchoMultiContentRrequest().setBody(multiContentBodyMapper.map2MultiContentBodyRdto(value.getBody()));
    }
}
