package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.UploadEchoFileWithFormRrequest;
import example.commons.EndpointProvider;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.UploadEchoFileWithFormRequest;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class UploadEchoFileWithFormRequestMapper {

    private final EndpointProvider endpointProvider;

    public RequestEntity<MultiValueMap<String, Object>> map(@Nonnull UploadEchoFileWithFormRequest request) {
        UploadEchoFileWithFormRrequest rawRequest = map2UploadEchoFileWithFormRrequest(request);
        Map<String, String> pathParams = new HashMap<>();
        pathParams.put("id", rawRequest.getId());
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo-file/{id}/form"))
                .build(pathParams);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", rawRequest.getFile());
        return RequestEntity.post(uri).body(body);
    }

    private UploadEchoFileWithFormRrequest map2UploadEchoFileWithFormRrequest(UploadEchoFileWithFormRequest value) {
        return new UploadEchoFileWithFormRrequest().setId(SerializationUtils.toString(value.getId()))
                .setFile(value.getFile());
    }
}
