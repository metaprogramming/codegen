package example.adapters.out.rest.mappers;

import example.adapters.out.rest.dtos.DeleteFileRrequest;
import example.commons.EndpointProvider;
import example.commons.SerializationUtils;
import example.ports.out.rest.dtos.DeleteFileRequest;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import lombok.RequiredArgsConstructor;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@RequiredArgsConstructor
@Generated("pl.metaprogramming.codegen")
public class DeleteFileRequestMapper {

    private final EndpointProvider endpointProvider;

    public RequestEntity<Void> map(@Nonnull DeleteFileRequest request) {
        DeleteFileRrequest rawRequest = map2DeleteFileRrequest(request);
        Map<String, String> pathParams = new HashMap<>();
        pathParams.put("id", rawRequest.getId());
        URI uri = UriComponentsBuilder.fromUriString(endpointProvider.getEndpoint("/api/v1/echo-file/{id}"))
                .build(pathParams);
        return RequestEntity.delete(uri).build();
    }

    private DeleteFileRrequest map2DeleteFileRrequest(DeleteFileRequest value) {
        return new DeleteFileRrequest().setId(SerializationUtils.toString(value.getId()));
    }
}
