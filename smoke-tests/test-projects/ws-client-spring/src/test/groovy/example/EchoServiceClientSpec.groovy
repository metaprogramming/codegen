package example

import example.ports.out.soap.EchoServiceClient
import example.commons.LocalDateTimeAdapter
import example.ports.out.soap.schema.EnumType
import example.ports.out.soap.schema.ExtendedType
import example.ports.out.soap.schema.Message
import example.ports.out.soap.schema.ObjectType
import io.undertow.Undertow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.ws.WebServiceMessage
import org.springframework.ws.client.core.WebServiceMessageCallback
import spock.lang.Specification

import javax.xml.transform.TransformerException
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@ActiveProfiles("test")
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE,
        properties="WS_ECHO_URL=http://localhost:18080/ws/echo"
)
class EchoServiceClientSpec extends Specification {

    @Autowired
    EchoServiceClient client

    def setupSpec() {
        startMockWsServer()
    }

    def "the received message should be the same as sent - echo"() {
        given:
        def dateTimeAdapter = new LocalDateTimeAdapter()
        def date = LocalDate.now()
        def dateText = date.format(DateTimeFormatter.ISO_DATE)
        def dateTime = LocalDateTime.now()
        def dateTimeText = dateTimeAdapter.marshal(dateTime)
        def request = createMessage(date, dateTime)

        when:
        def response = client.echo(request)

        then:
        response == request

        when:
        def requestCallback = new RequestCallback()
        client.getWebServiceTemplate().marshalSendAndReceive(request, requestCallback)
        then:
        requestCallback.requestText == """<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><ns3:message xmlns:ns3="http://example.com/echo"><nillableField xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"/><listStringField>s1</listStringField><listStringField>s2</listStringField><listObjectField><stringField>o1s</stringField></listObjectField><listObjectField><stringField>o2s</stringField></listObjectField><enumField>0</enumField><doubleField>1.1</doubleField><dateField>${dateText}</dateField><dateTimeField>${dateTimeText}</dateTimeField><UPerCaseField>UPPER_CASE</UPerCaseField></ns3:message></SOAP-ENV:Body></SOAP-ENV:Envelope>"""
    }

    def "the received message should be the same as sent - echoExtended"() {
        given:
        def request = new ExtendedType(baseRequestField: 'baseRequestField', someField: 'someField')

        when:
        def response = client.echoExtended(request)

        then:
        response == request

        when:
        def requestCallback = new RequestCallback()
        client.getWebServiceTemplate().marshalSendAndReceive(request, requestCallback)
        then:
        requestCallback.requestText == '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><ns3:extendedType xmlns:ns3="http://example.com/echo"><baseRequestField>baseRequestField</baseRequestField><someField>someField</someField></ns3:extendedType></SOAP-ENV:Body></SOAP-ENV:Envelope>'
    }

    void startMockWsServer() {
        Undertow server = Undertow.builder()
                .addHttpListener(18080, "localhost")
                .setHandler { it ->
                    it.requestReceiver.receiveFullString { exchange, message ->
                        exchange.getResponseSender().send(message)
                    }
                }.build()
        server.start()
    }


    static Message createMessage(LocalDate date, LocalDateTime dateTime) {
        return new Message()
                .setDateField(date)
                .setDateTimeField(dateTime)
                .setDoubleField(1.1)
                .setEnumField(EnumType.V_0)
                .setListObjectField(Arrays.asList(
                        new ObjectType().setStringField("o1s"),
                        new ObjectType().setStringField("o2s")
                ))
                .setListStringField(Arrays.asList("s1", "s2"))
                .setUPerCaseField("UPPER_CASE")
    }

    static class RequestCallback implements WebServiceMessageCallback {
        String requestText

        @Override
        void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
            def out = new ByteArrayOutputStream()
            message.writeTo(out)
            requestText = new String(out.toByteArray())
            println "responseText: $requestText"
        }
    }
}
