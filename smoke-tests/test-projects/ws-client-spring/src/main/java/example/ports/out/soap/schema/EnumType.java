package example.ports.out.soap.schema;

import example.commons.EnumValue;
import javax.annotation.processing.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;

@XmlEnum
@XmlType(name = "enumType", namespace = "http://example.com/echo")
@Generated("pl.metaprogramming.codegen")
public enum EnumType implements EnumValue {

    @XmlEnumValue("lowercase")
    LOWERCASE("lowercase"), @XmlEnumValue("0")
    V_0("0");

    @Getter
    private final String value;

    EnumType(String value) {
        this.value = value;
    }

    public static EnumType fromValue(String value) {
        return EnumValue.fromValue(value, EnumType.class);
    }
}
