package example.ports.out.soap.schema;

import javax.annotation.Nonnull;
import javax.annotation.processing.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "objectType", namespace = "http://example.com/echo", propOrder = {"stringField"})
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Generated("pl.metaprogramming.codegen")
public class ObjectType {

    @Nonnull
    @XmlElement(required = true)
    private String stringField;
}
