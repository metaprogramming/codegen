package example.ports.out.soap;

import example.ports.out.soap.schema.ExtendedType;
import example.ports.out.soap.schema.Message;
import javax.annotation.processing.Generated;

@Generated("pl.metaprogramming.codegen")
public interface EchoServiceClient {

    Message echo(Message request);

    ExtendedType echoExtended(ExtendedType request);
}
