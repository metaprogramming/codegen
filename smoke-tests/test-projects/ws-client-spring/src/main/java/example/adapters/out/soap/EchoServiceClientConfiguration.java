package example.adapters.out.soap;

import example.ports.out.soap.EchoServiceClient;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
@Generated("pl.metaprogramming.codegen")
public class EchoServiceClientConfiguration {

    @Value("${WS_ECHO_URL:http://localhost:8080/ws/echo}")
    private String url;

    @Bean
    public EchoServiceClient createEchoServiceClient() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("example.ports.out.soap.schema", "example.ws.ns2");
        EchoServiceClientImpl client = new EchoServiceClientImpl();
        client.setDefaultUri(url);
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
