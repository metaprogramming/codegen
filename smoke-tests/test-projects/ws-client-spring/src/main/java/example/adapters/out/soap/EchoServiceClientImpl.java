package example.adapters.out.soap;

import example.ports.out.soap.EchoServiceClient;
import example.ports.out.soap.schema.ExtendedType;
import example.ports.out.soap.schema.Message;
import javax.annotation.processing.Generated;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

@Generated("pl.metaprogramming.codegen")
public class EchoServiceClientImpl extends WebServiceGatewaySupport implements EchoServiceClient {

    @Override
    public Message echo(Message request) {
        return (Message) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public ExtendedType echoExtended(ExtendedType request) {
        return (ExtendedType) getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
