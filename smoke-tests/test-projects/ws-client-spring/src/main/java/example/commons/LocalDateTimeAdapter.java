package example.commons;

import java.time.LocalDateTime;
import javax.annotation.processing.Generated;
import javax.xml.bind.annotation.adapters.XmlAdapter;

@Generated("pl.metaprogramming.codegen")
public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

    @Override
    public LocalDateTime unmarshal(String v) throws Exception {
        return LocalDateTime.parse(v);
    }

    @Override
    public String marshal(LocalDateTime v) throws Exception {
        return v.toString();
    }
}
