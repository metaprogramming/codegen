package example.commons;

import java.time.LocalDate;
import javax.annotation.processing.Generated;
import javax.xml.bind.annotation.adapters.XmlAdapter;

@Generated("pl.metaprogramming.codegen")
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

    @Override
    public LocalDate unmarshal(String v) throws Exception {
        return LocalDate.parse(v);
    }

    @Override
    public String marshal(LocalDate v) throws Exception {
        return v.toString();
    }
}
