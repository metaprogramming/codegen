/*
 * Copyright (c) 2019 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import spock.lang.Specification
import spock.lang.Timeout
import spock.lang.Unroll

import static pl.metaprogramming.codegen.BuildRunner.gradle
import static pl.metaprogramming.codegen.BuildRunner.run
import static pl.metaprogramming.fixtures.Spring2tTestProjects.*
import static pl.metaprogramming.fixtures.TestProjects.*

@Timeout(600)
class TestProjectsSpec extends Specification {

    @Unroll
    def "generate and build: #testProject"() {
        expect:
        testProject.generate()
        run(gradle(testProject.projectDir, 'clean', task))

        where:
        testProject                   | task
        rsServerSpring1tGenerator     | 'build'
        rsServerSpring1tJbvGenerator  | 'build'
        rsClientSpring1tGenerator     | 'assemble'
        wsClientSpringGenerator       | 'build'
        rsServerSpring2tGenerator     | 'build'
        rsServerSpringLegacyGenerator | 'build'
        rsClientSpring2tGenerator     | 'assemble'
    }

    def "REST clients should interact with #serverProject"() {
        given:
        def testTimeout = 120
        when:
        def server = ServerRunner.of(gradle(serverProject.projectDir, ':bootRun'))
        server.start()

        then:
        server.ready

        when:
        def rsClientSpring1tSuccess = run(gradle(rsClientSpring1tGenerator.projectDir, 'cleanTest', 'test'), testTimeout)
        def rsClientSpring2tSuccess = run(gradle(rsClientSpring2tGenerator.projectDir, 'cleanTest', 'test'), testTimeout)

        then:
        rsClientSpring1tSuccess && rsClientSpring2tSuccess

        cleanup:
        server.stop()

        where:
        serverProject << [
                rsServerSpring1tGenerator,
                rsServerSpring2tGenerator,
        ]
    }

    private File projectDir(String testProject) {
        new File("../test-projects/$testProject/")
    }

}
