/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import groovy.util.logging.Slf4j
import org.apache.hc.client5.http.classic.methods.HttpGet
import org.apache.hc.client5.http.classic.methods.HttpPost
import org.apache.hc.client5.http.classic.methods.HttpUriRequest
import org.apache.hc.client5.http.impl.classic.HttpClients
import org.apache.hc.core5.http.ClassicHttpResponse

import java.time.Duration
import java.time.LocalDateTime
import java.util.concurrent.TimeUnit

@Slf4j
class ServerRunner {
    private static final int COUNTDOWN_LIMIT = 133
    private static final int COUNTDOWN_INTERVAL = 300
    private ProcessBuilder processBuilder
    private Process process
    private LocalDateTime startTime

    static ServerRunner of(ProcessBuilder processBuilder) {
        new ServerRunner(processBuilder: processBuilder)
    }

    void start() {
        log.info "Going to start server: $serverName (${processBuilder.command().join(' ')})"
        startTime = LocalDateTime.now()
        process = processBuilder.start()
        process.consumeProcessOutput(System.out, System.err)
        int countdown = COUNTDOWN_LIMIT
        sleep(2000)
        do {
            sleep(COUNTDOWN_INTERVAL)
            --countdown
            log.info "Check is server ready, probe ${COUNTDOWN_LIMIT - countdown}"
        } while (countdown > 0 && !ready)
    }

    void stop() {
        def duration = Duration.between(startTime, LocalDateTime.now())
        log.info "Going to stop server: $serverName (after $duration)"
        if (ready) {
            callServer(makePost('actuator/shutdown'))
            process.waitFor(5, TimeUnit.SECONDS)
        } else {
            process.waitForOrKill(5000)
        }
        if (process.exitValue() != 0) {
            log.info "Server error:"
            log.info process.text
        }
    }

    boolean isReady() {
        callServer(makeGet("actuator/health"))
                .map { it.code == 200 }
                .orElse(false)
    }

    private String getServerName() {
        processBuilder.directory().name
    }

    private Optional<ClassicHttpResponse> callServer(HttpUriRequest request) {
        try {
            def response = HttpClients.createDefault().execute(request) {
                it
            }
            log.info "$request.path: $response.code"
            Optional.of(response)
        } catch (Exception e) {
            log.info "$request.path: $e.message"
            Optional.empty()
        }
    }

    private def makeGet(String path) {
        new HttpGet(makeUrl(path))
    }

    private def makePost(String path) {
        new HttpPost(makeUrl(path))
    }

    private String makeUrl(String path) {
        "http://localhost:8080/$path"
    }

}
