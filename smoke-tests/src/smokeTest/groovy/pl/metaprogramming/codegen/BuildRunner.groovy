/*
 * Copyright (c) 2020 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import groovy.util.logging.Slf4j
import pl.metaprogramming.UtilsKt

import java.time.Duration
import java.time.LocalDateTime
import java.util.concurrent.TimeUnit

import static java.util.Arrays.asList

@Slf4j
class BuildRunner {

    private static final def DEBUG = false

    static boolean run(ProcessBuilder processBuilder, long timeout = 540, String successMark = null) {
        def workDir = processBuilder.directory().path
        def task = processBuilder.command().join(' ')
        def startTime = LocalDateTime.now()
        log.info "[$workDir][$task] Start"
        def process = processBuilder.start()

        if (DEBUG) {
            process.waitForProcessOutput(System.out, System.err)
            def duration = Duration.between(startTime, LocalDateTime.now())
            log.info "[$task][$duration] Completed: ${process.exitValue()}"
            return process.exitValue() == 0
        }

        boolean exited = process.waitFor(timeout, TimeUnit.SECONDS)
        def duration = Duration.between(startTime, LocalDateTime.now()).toSeconds()
        if (!exited || process.exitValue() != 0) {
            def status = exited ? "exitValue: ${process.exitValue()}" : "TIMEOUT (${timeout}s)"
            def stdOut = process.inputStream.text
            def errOut = process.errorStream.text
            process.closeStreams()
            if (successMark != null && stdOut.contains(successMark)) {
                log.info("[WARNING] Success (with invalid status): $status\nOUTPUT STREAM:\n$stdOut\n==================")
                true
            } else {
                log.info "[$workDir][$task][$duration] Failed [$status]\nERROR STREAM:\n$errOut\n==================\nOUTPUT STREAM:\n$stdOut\n=================="
                false
            }
        } else {
            log.info "[$workDir][$task][$duration] Completed"
            true
        }
    }

    static ProcessBuilder gradle(File workingDir, String... tasks) {
        def gradleCmd = isWindows() ? ['cmd', '/c', 'gradlew.bat'] : ['./gradlew']
        processBuilder(workingDir.absoluteFile, gradleCmd + asList(tasks))
    }

    static boolean mvn(File workingDir, String... tasks) {
        def params = ["mvn", "--batch-mode"]//[isWindows() ? 'mvn.cmd' : 'mvn']
        params.addAll(tasks)
        // avoiding the problem when the process hangs despite successful
        params.addAll("-Dexec.cleanupDaemonThreads=false", "-DreuseForks=false")
        // force java version
        def javaVersion = UtilsKt.javaVersion
        if (isWindows() && javaVersion > 17) {
            javaVersion = 17
        }
        ["target", "source", "release"].each {
            params.add("-Dmaven.compiler.${it}=${javaVersion <= 8 ? "1.$javaVersion" : javaVersion}".toString())
        }

        def runParams = (isWindows() ? ["cmd.exe", "/c"] : ["sh", "-c"]) + [params.join(" ")]

        def timeout = 60
        def retryCounter = 10
        do {
            if (run(processBuilder(workingDir, runParams), timeout, "[INFO] BUILD SUCCESS")) {
                return true
            }
            --retryCounter
        } while (retryCounter > 0)
        false
    }


    static ProcessBuilder npm(File workingDir, String task) {
        processBuilder(workingDir, [isWindows() ? 'npm.cmd' : 'npm', task])
    }

    static ProcessBuilder processBuilder(File workingDir, List<String> cmd) {
        new ProcessBuilder()
                .command(cmd)
                .directory(workingDir)
    }

    private static boolean isWindows() {
        System.getProperty('os.name').toLowerCase().contains('windows')
    }

}
