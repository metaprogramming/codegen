package pl.metaprogramming.codegen

import spock.lang.Ignore
import spock.lang.IgnoreIf
import spock.lang.Specification
import spock.lang.Timeout
import spock.lang.Unroll

import static pl.metaprogramming.codegen.BuildRunner.*

@Ignore
@Timeout(3600)
class OldQuickstartProjectsSpec extends Specification {

    static def ALL_PROJECTS = [
            'spring-rs-client',
            'spring-rs-server',
            'spring-soap-client'
    ]

    @Unroll
    def "should generate and build #project-gradle"() {
        given:
        def projectDir = projectDir(project + '-gradle')

        expect:
        run(gradle(projectDir, ':generator:run'))
        run(gradle(projectDir, ':app:build'))

        where:
        project << getProjects()
    }

    @IgnoreIf(
            value = { os.linux },
            reason = "disable for gitlab ci - downloading maven dependencies takes extremely long time"
    )
    @Unroll
    def "should generate and build #project-maven"() {
        given:
        def projectDir = projectDir(project + '-maven')

        expect:
        mvn(projectDir, '-f', 'generator', 'compile', 'exec:java')
        mvn(projectDir, '-f', 'app', 'package')

        where:
        project << getProjects()
    }

    static def getProjects() {
        ALL_PROJECTS
    }

    static File projectDir(String project) {
        new File('quickstart-projects', project)
    }
}
