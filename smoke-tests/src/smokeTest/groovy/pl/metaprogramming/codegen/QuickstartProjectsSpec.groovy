/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import spock.lang.Specification
import spock.lang.Timeout

import static pl.metaprogramming.codegen.BuildRunner.*

@Timeout(3600)
class QuickstartProjectsSpec extends Specification {

    def "should generate and build spring-gradle"() {
        given:
        def projectDir = projectDir('spring-gradle')

        expect:
        run(gradle(projectDir, 'generate'))
        run(gradle(projectDir, 'build'))
    }

    def "should generate and build spring-maven"() {
        given:
        def projectDir = projectDir('spring-maven')

        expect:
        mvn(projectDir, 'codegen:generate')
        mvn(projectDir, 'package')
    }

    def "should generate and build spring-maven-complex"() {
        given:
        def projectDir = projectDir('spring-maven-complex')

        expect:
        mvn(projectDir, 'install')
    }

    static File projectDir(String project) {
        new File('quickstart-projects', project)
    }
}
