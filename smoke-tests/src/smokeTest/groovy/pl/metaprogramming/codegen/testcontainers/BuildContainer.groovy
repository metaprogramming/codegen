/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.testcontainers

import com.github.dockerjava.api.model.Bind
import groovy.transform.TupleConstructor
import org.testcontainers.containers.GenericContainer
import org.testcontainers.utility.MountableFile

@TupleConstructor(includeFields = true, excludes = ['container'])
class BuildContainer {
    protected GenericContainer container
    protected List<String> projects
    protected String dockerImage
    protected String projectsSuffix
    protected String volumeBinds

    void start() {
        container = new GenericContainer<>(dockerImage)
                .withCommand("sleep", "infinity")
                .withCreateContainerCmdModifier({
                    it.hostConfig.withBinds(Bind.parse(volumeBinds))
                })
        def projectsDir = new File("quickstart-projects")
        projects.each {
            String project = "$it-${projectsSuffix}"
            container.withCopyFileToContainer(
                    MountableFile.forHostPath(new File(projectsDir, project).toPath()),
                    "/projects/$project")
        }
        copyCodegenMavenArtifacts()
        container.withReuse(true)
        container.start()
//        println '/projects: ' + container.execInContainer("ls", "-l", "/projects").stdout
//        println '/repo: ' + container.execInContainer("ls", "-l", "/root/.m2/repository/pl/metaprogramming/codegen/0.4.1-SNAPSHOT").stdout
//        println '/home: ' + container.execInContainer('echo', '$HOME').stdout
    }

    boolean exec(String... commands) {
        def execResult = container.execInContainer(commands)
        if (execResult.exitCode != 0) {
            println execResult.stdout
            System.err.println execResult.stderr
            false
        } else {
            true
        }
    }

    void stop() {
        container?.stop()
    }

    private void copyCodegenMavenArtifacts() {
        String localPath = System.getProperty('user.home') + '/.m2/repository/pl/metaprogramming/codegen'
        container.withCopyFileToContainer(
                MountableFile.forHostPath(localPath),
                "/root/.m2/repository/pl/metaprogramming/codegen/")
    }

}
