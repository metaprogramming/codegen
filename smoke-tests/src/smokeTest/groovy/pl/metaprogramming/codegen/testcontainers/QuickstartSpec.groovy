/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.testcontainers

import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import java.util.function.Consumer

//@Requires({ Boolean.valueOf(env['TESTCONTAINERS_ENABLED']) })
@Ignore
class QuickstartSpec extends Specification {

    static def SPRING_WS_CLIENT = 'spring-ws-client'
    static def SPRING_RS_CLIENT = 'spring-rs-client'
    static def SPRING_RS_SERVER = 'spring-rs-server'

    static def ALL_PROJECTS = [SPRING_WS_CLIENT, SPRING_RS_CLIENT, SPRING_RS_SERVER]

    @Shared
    Map<String, BuildContainer> containers = [:]

    def setupSpec() {
        def gradleJdk8 = new GradleContainer('gradle:jdk8', 'gradle-cache-jdk8', ALL_PROJECTS)
        def gradleJdk11 = new GradleContainer('gradle:jdk11', 'gradle-cache-jdk11', ALL_PROJECTS)
        def mavenJdk8 = new MavenContainer(ALL_PROJECTS)
        ALL_PROJECTS.each {
            containers.put("$it-gradle", it != SPRING_WS_CLIENT ? gradleJdk11 : gradleJdk8)
            containers.put("$it-maven", mavenJdk8)
        }
        forEachContainer({ it.start() })
    }

    def cleanupSpec() {
        forEachContainer({ it.stop() })
    }

    @Unroll
    def "should generate and build #project-maven"() {
        given:
        def mavenContainer = getMaven(project)
        expect:
        mavenContainer.exec(project, 'generator', 'compile', 'exec:java')
        mavenContainer.exec(project, 'app', 'package')

        where:
        project << ALL_PROJECTS
    }

    @Unroll
    def "should generate and build #project-gradle"() {
        given:
        def gradleContainer = getGradle(project)
        expect:
        gradleContainer.exec(project, ':generator:run')
        gradleContainer.exec(project, ':app:build')

        where:
        project << ALL_PROJECTS
    }

    private void forEachContainer(Consumer<BuildContainer> consumer) {
        new HashSet<>(containers.values()).parallelStream().forEach(consumer)
    }

    private MavenContainer getMaven(String project) {
        containers.get("$project-maven") as MavenContainer
    }

    private GradleContainer getGradle(String project) {
        containers.get("$project-gradle") as GradleContainer
    }
}
