val testcontainersVersion: String by project
val commonsIoVersion: String by project
val groovyVersion: String by project
val httpClientVersion = "5.3.1"

plugins {
    groovy
    jacoco
}

java {
    sourceSets.create("smokeTest") {
        compileClasspath += sourceSets.main.get().output
        runtimeClasspath += sourceSets.main.get().output
    }
}

dependencies {
    val smokeTestImplementation = configurations.getByName("smokeTestImplementation") {
        extendsFrom(configurations["implementation"])
    }
    smokeTestImplementation(testFixtures(project(":codegen")))
    smokeTestImplementation(testFixtures(project(":codegen-spring-rs2t")))
    smokeTestImplementation("org.testcontainers:spock:$testcontainersVersion")
    smokeTestImplementation("commons-io:commons-io:$commonsIoVersion")
    smokeTestImplementation("org.apache.httpcomponents.client5:httpclient5:$httpClientVersion")
}

tasks {
    register<Test>("smokeTest") {
        group = "verification"
        dependsOn(":updateDependencies")
        dependsOn(":codegen:publishToMavenLocal")
        dependsOn(":codegen-gradle-plugin:publishToMavenLocal")
        dependsOn(":codegen-maven-plugin:publishToMavenLocal")
        dependsOn(":codegen-spring-rs2t:publishToMavenLocal")
        useJUnitPlatform()
        outputs.upToDateWhen { false }
        val smokeSourceSets = sourceSets.getByName("smokeTest")
        testClassesDirs = smokeSourceSets.output.classesDirs
        classpath = smokeSourceSets.runtimeClasspath
        testLogging {
            showStandardStreams = true
        }
        extensions.configure(JacocoTaskExtension::class) {
            setDestinationFile(file(layout.buildDirectory.file("jacoco/smoke-test-java-${JavaVersion.current()}.exec")))
        }
    }
}