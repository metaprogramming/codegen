<img src="./codegen.png" width="400" alt="Codegen Logo" />

[![Apache License, Version 2.0, January 2004](https://img.shields.io/badge/license-Apache_2.0-green)](https://www.apache.org/licenses/LICENSE-2.0.html)
[![Maven Central](https://img.shields.io/maven-central/v/pl.metaprogramming/codegen)](https://mvnrepository.com/artifact/pl.metaprogramming/codegen)
[![Pipeline](https://img.shields.io/gitlab/pipeline/metaprogramming/codegen/develop)](https://gitlab.com/metaprogramming/codegen/-/pipelines/?page=1&scope=branches&ref=develop)
[![Codecov](https://codecov.io/gl/metaprogramming/codegen/branch/develop/graph/badge.svg)](https://codecov.io/gl/metaprogramming/codegen/branch/develop)
[![Website](https://img.shields.io/website-up-down-success-red/https/metaprogramming.gitlab.io/codegen/)](https://metaprogramming.gitlab.io/codegen/)

Codegen is a tool that aim to support software development with according to the API-first approach.

Codegen is a framework for creating your own code generators.
That the generated code will comply with the coding standards in your team.

Along with the tool, concrete code generators are delivered:
* REST server,
* REST client,
* SOAP client.

Each of them generate code for use with Spring Framework.

See the [Codegen website](https://metaprogramming.gitlab.io/codegen/) for more details.

# Project Status

Codegen is still under heavy development.
There can be breaking changes, but we're trying to keep them as minimum as possible.
To make it easier for you to adapt your codes to breaking changes, is maintained a [migration guide page](https://metaprogramming.gitlab.io/codegen/docs/migration-guide/).

# Getting Started

To learn how to get started with Codegen, see the [page](https://metaprogramming.gitlab.io/codegen/docs/getting-started/how-to-use).

# Installation

Codegen is available from [Maven Central Repository](https://mvnrepository.com/artifact/pl.metaprogramming/codegen).

# Contributing

If you ...
* have any problem with this tool
* have an idea to enhance this tool
* want to write code for this project
* want to help others use this tool by writing a tutorial

... submit new issue.

... or add a comment to an existing issue.

... or let me know directly via &#100;&#097;&#119;&#105;&#100;&#046;&#109;&#046;&#119;&#097;&#108;&#099;&#122;&#097;&#107;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;.

https://gitlab.com/metaprogramming/codegen/-/issues

# License

This project is licensed under the Apache 2.0 License - see the [LICENSE](LICENSE) file for details.
