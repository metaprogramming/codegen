import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile

plugins {
    kotlin("jvm")
//    `codegen-lib` conflict `maven-publish` with com.gradle.plugin-publish
    id("com.gradle.plugin-publish") version "1.3.0"
//    id("com.github.johnrengelman.shadow") version "8.1.1"

    // copy from `codegen-lib`
    signing
}

dependencies {
    implementation(project(":codegen"))
}

publishing {
    publications {
        withType<MavenPublication>().configureEach {
            pom {
                name.set("pl.metaprogramming:${project.name}")
                description.set("Java code generator framework - Gradle plugin")
                url.set("http://metaprogramming.pl")
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("waldenko")
                        name.set("Dawid Walczak")
                        email.set("dawid.m.walczak@gmail.com")
                    }
                }
                scm {
                    url.set("https://gitlab.com/metaprogramming/codegen")
                    connection.set("scm:git:https://gitlab.com/metaprogramming/codegen.git")
                    developerConnection.set("scm:git:ssh://git@gitlab.com:metaprogramming/codegen.git")
                }
            }
        }
    }
}

gradlePlugin {
    website = "https://gitlab.com/metaprogramming/codegen"
    vcsUrl = "https://gitlab.com/metaprogramming/codegen"
    plugins {
        create("codegenPlugin") {
            id = "pl.metaprogramming.codegen"
            displayName = "Codegen plugin"
            description = "A Gradle plugin for code generation tool (OpenAPI/swagger, WSDL)"
            tags = listOf("codegen", "swagger", "openapi", "soap", "wsdl", "plugins")
            implementationClass = "pl.metaprogramming.codegen.gradle.CodegenPlugin"
        }
    }
}

tasks.withType<KotlinJvmCompile>().configureEach {
    compilerOptions {
        jvmTarget.set(JvmTarget.JVM_1_8)
    }
}

// copy from `codegen-lib` plugin
java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
    withJavadocJar()
    withSourcesJar()
}

tasks.withType<Sign>().configureEach {
    enabled = project.hasProperty("sonatypeUsername")
}
