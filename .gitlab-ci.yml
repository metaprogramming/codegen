image: openjdk:17

stages:
  - build
  - test
  - code-coverage
  - code-coverage-post
  - deploy

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle
  - chmod +rwx .gradle/caches || true
  - export WORK_DIR=`pwd`
  - export MAVEN_OPTS=-Dmaven.repo.local=`pwd`/.m2/repository
  - export GRADLE_OPTS=-Dmaven.repo.local=`pwd`/.m2/repository

cache:
  paths:
    - .gradle/wrapper
    - .gradle/caches
    - .m2/repository

build:
  stage: build
  script:
    - ./gradlew assemble

java-8-tests:
  stage: test
  image: openjdk:8
  script:
    - ./gradlew check
  artifacts:
    reports:
      junit:
        - codegen/build/test-results/test/TEST-*.xml
        - codegen-maven-plugin/build/test-results/test/TEST-*.xml
        - codegen-spring-rs2t/build/test-results/test/TEST-*.xml
    paths:
      - build/jacoco/

java-11-tests:
  stage: test
  image: openjdk:11
  script:
    - ./gradlew check
  artifacts:
    reports:
      junit:
        - codegen/build/test-results/test/TEST-*.xml
        - codegen-maven-plugin/build/test-results/test/TEST-*.xml
        - codegen-spring-rs2t/build/test-results/test/TEST-*.xml
    paths:
      - build/jacoco/

java-17-tests:
  stage: test
  image: openjdk:17
  script:
    - ./gradlew check
  artifacts:
    reports:
      junit:
        - codegen/build/test-results/test/TEST-*.xml
        - codegen-maven-plugin/build/test-results/test/TEST-*.xml
        - codegen-spring-rs2t/build/test-results/test/TEST-*.xml
    paths:
      - build/jacoco/

java-21-tests:
  stage: test
  image: openjdk:21
  script:
    - ./gradlew check
  artifacts:
    reports:
      junit:
        - codegen/build/test-results/test/TEST-*.xml
        - codegen-maven-plugin/build/test-results/test/TEST-*.xml
        - codegen-spring-rs2t/build/test-results/test/TEST-*.xml
    paths:
      - build/jacoco/

java-8-smoke-tests:
  stage: test
  image: maven:3-openjdk-8
  timeout: 4h
  script:
    - ./gradlew smokeTest
  artifacts:
    reports:
      junit: smoke-tests/build/test-results/smokeTest/TEST-*.xml
    paths:
      - build/jacoco/

java-11-smoke-tests:
  stage: test
  image: maven:3-openjdk-11
  timeout: 4h
  script:
    - ./gradlew smokeTest
  artifacts:
    reports:
      junit: smoke-tests/build/test-results/smokeTest/TEST-*.xml
    paths:
      - build/jacoco/

java-17-smoke-tests:
  stage: test
  image: maven:3-openjdk-17
  timeout: 4h
  script:
    - ./gradlew smokeTest
  artifacts:
    reports:
      junit: smoke-tests/build/test-results/smokeTest/TEST-*.xml
    paths:
      - build/jacoco/

code-coverage:
  stage: code-coverage
  dependencies:
    - "java-8-tests"
    - "java-11-tests"
    - "java-17-tests"
    - "java-21-tests"
    - "java-8-smoke-tests"
    - "java-11-smoke-tests"
    - "java-17-smoke-tests"
  script:
    - ./gradlew jacocoTestReport
  coverage: '/    - Instruction Coverage: ([0-9.]+)%/'
  artifacts:
    paths:
      - codegen/build/reports/jacoco/
      - codegen-maven-plugin/build/reports/jacoco/
      - codegen-spring-rs2t/build/reports/jacoco/

doc-kotlin-api:
  stage: code-coverage
  script:
    - ./gradlew codegen:apiForKotlin
  artifacts:
    paths:
      - codegen/build/dokka-kotlin-api/

code-coverage-visualize:
  stage: code-coverage-post
  script:
    - ./gradlew jacotura
  needs: [ "code-coverage" ]
  dependencies: [ "code-coverage" ]
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: '*/build/cobertura.xml'

code-coverage-upload:
  stage: code-coverage-post
  dependencies: [ "code-coverage" ]
  script:
    - curl -Os https://uploader.codecov.io/latest/linux/codecov
    - chmod +x codecov
    - ./codecov -t ${CODECOV_TOKEN}

website-build:
  image: node:latest
  stage: code-coverage-post
  dependencies: [ "code-coverage", "doc-kotlin-api" ]
  script:
    - ls -l
    - node -v
    - cd maintenance/website
    - cat docusaurus.config.js
    - cp -r ../../codegen/build/reports/jacoco/test/html static/code-coverage
    - cp -r ../../codegen/build/dokka-kotlin-api static/kotlin-api
    - npm install
    - npm run build
  artifacts:
    paths:
      - maintenance/website/build

pages:
  only:
    - master
  stage: deploy
  dependencies: [ "website-build" ]
  script:
    - mv maintenance/website/build public
  artifacts:
    paths:
      - public

after_script:
  - echo "End CI"
