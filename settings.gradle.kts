rootProject.name = "codegen-parent"

pluginManagement {
    val kotlinVersion: String by settings
    val dokkaVersion: String by settings
    plugins {
        kotlin("jvm") version kotlinVersion
        id("org.jetbrains.dokka") version dokkaVersion
    }
}

include(
    "codegen",
    "codegen-gradle-plugin",
    "codegen-maven-plugin",
    "codegen-spring-rs2t",
    "smoke-tests",
    ":maintenance:api-doc-generator",
)