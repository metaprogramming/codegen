/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.gradle.api.tasks.TaskAction
import pl.metaprogramming.utils.MaintenanceTask
import pl.metaprogramming.utils.QUICKSTART_PROJECTS

open class SetupApiInQuickstartProjectsTask : MaintenanceTask() {
    @TaskAction
    fun action() {
        val projectTypes = listOf("maven", "gradle")
        val rsProjects = listOf("spring-rs-server", "spring-rs-client")
        val wsProjects = listOf("spring-soap-client")

        rsProjects.forEach { kind ->
            projectTypes.forEach { type ->
                copy(
                    "$QUICKSTART_PROJECTS/example-api.yaml",
                    "$QUICKSTART_PROJECTS/$kind-$type/generator/src/main/resources"
                )
            }
        }
        wsProjects.forEach { kind ->
            projectTypes.forEach { type ->
                copy(
                    "$QUICKSTART_PROJECTS/countries.wsdl",
                    "$QUICKSTART_PROJECTS/$kind-$type/generator/src/main/resources"
                )
            }
        }
    }
}