import com.diffplug.spotless.changelog.gradle.ChangelogExtension
import org.gradle.api.tasks.Internal
import pl.metaprogramming.utils.MaintenanceTask
import pl.metaprogramming.utils.ver.BetaVersionProvider
import kotlin.io.path.writeLines

/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

open class SetupVersionBaseTask : MaintenanceTask() {

    @Internal
    protected val betaVersion = BetaVersionProvider.instance

    fun changelog(): ChangelogExtension = project.properties["spotlessChangelog"] as ChangelogExtension

    fun setProjectVersion(newVersion: String) {
        updateVersionInProject(newVersion)
        updateWebsiteChangelog()
    }

    private fun updateVersionInProject(newVersion: String) {
        project.version = newVersion
        val propertiesFile = file("gradle.properties")
        propertiesFile.writeText(
            propertiesFile.readText().replace("version=.*".toRegex(), "version=${project.version}")
        )
    }

    private fun updateWebsiteChangelog() {
        val content = mutableListOf(
            "---",
            "id: changelog",
            "title: Changelog",
            "sidebar_label: Changelog",
            "---"
        )
        var releasedPartFlag = false
        file("CHANGELOG.md").readLines().forEach {
            if (!releasedPartFlag && it.startsWith("## ") && it != "## [Unreleased]")
                releasedPartFlag = true
            if (releasedPartFlag) content.add(it)
        }
        file("maintenance/website/docs/changelog.md").toPath().writeLines(content)
    }
}
