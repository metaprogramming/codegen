/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils.ver

class BetaVersionProvider(val isEnabled: Boolean, private val baseVersion: String) {

    companion object {
       val instance = BetaVersionProvider(false, "2.0.0-beta")
    }

    fun makeNextVersion(lastVersion: String?): String {
        if (lastVersion == null || !lastVersion.startsWith(baseVersion)) {
            return "${baseVersion}.1"
        }
        val subversion = lastVersion.substring(baseVersion.length)
        val nextSubversion = if (subversion.isEmpty()) 1 else subversion.substring(1).toInt() + 1
        return "${baseVersion}.${nextSubversion}"
    }
}