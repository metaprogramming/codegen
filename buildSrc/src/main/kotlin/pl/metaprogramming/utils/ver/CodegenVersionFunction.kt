/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils.ver

import com.diffplug.spotless.changelog.NextVersionFunction
import org.osgi.framework.Version

class CodegenVersionFunction : NextVersionFunction.Semver() {

    override fun nextVersion(unreleasedChanges: String?, lastVersion: String?): String {
        if (BetaVersionProvider.instance.isEnabled) {
            return BetaVersionProvider.instance.makeNextVersion(lastVersion)
        }
        if (unreleasedChanges == null) {
            return "0.0.1"
        }
        val addedOrChanged = listOf("### Added", "### Changed")
        val last = Version.parseVersion(lastVersion)
        val hasMajorChanges = ifFoundBumpBreaking.any { unreleasedChanges.contains(it) }
        val hasMinorChanges = addedOrChanged.any { unreleasedChanges.contains(it) }
        return if (hasMajorChanges) "${last.major + 1}.0.0"
        else if (hasMinorChanges) "${last.major}.${last.minor + 1}.0"
        else "${last.major}.${last.minor}.${last.micro + 1}"
    }
}