/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils

import org.jdom2.Element
import org.jdom2.input.SAXBuilder
import org.w3c.dom.Node
import java.io.File
import java.net.URI
import kotlin.io.path.writeLines

const val TEST_PROJECTS = "smoke-tests/test-projects"
const val QUICKSTART_PROJECTS = "smoke-tests/quickstart-projects"


fun getJavaVersion(): Int {
    var version = System.getProperty("java.version")
    if (version.startsWith("1.")) {
        version = version.substring(2, 3)
    } else {
        val dot = version.indexOf(".")
        if (dot != -1) {
            version = version.substring(0, dot)
        }
    }
    return Integer.parseInt(version)
}

fun File.isBuildGradle() = this.name == "build.gradle" || this.name == "build.gradle.kts"
fun File.isGradleProject() =
    isDirectory && (File(this, "build.gradle").exists() || File(this, "build.gradle.kts").exists())

fun String.readXml(): Element {
    return SAXBuilder().build(URI(this).toURL()).rootElement
}

fun org.w3c.dom.Element.getChildren(name: String): List<Node> {
    val elements = getElementsByTagName(name)
    val result = mutableListOf<Node>()
    for (i in 0 until elements.length) {
        result.add(elements.item(i))
    }
    return result
}

fun File.update(updater: (line: String) -> String) {
    val changes = mutableListOf<String>()
    val newContent = readLines().mapIndexed { idx, line ->
        try {
            val updatedLine = updater.invoke(line)
            if (updatedLine != line) {
                changes.add("%3d|$line -> $updatedLine".format(idx + 1))
            }
            updatedLine
        } catch (e: Exception) {
            e.printStackTrace()
            System.err.println("Fail processing $absolutePath at line $idx:")
            System.err.println(line)
            line
        }
    }
    if (changes.isNotEmpty()) {
        toPath().writeLines(newContent)
        println("Update $absolutePath")
        changes.forEach { println(it) }
    }
}
