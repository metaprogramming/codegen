/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils

import org.w3c.dom.Element
import javax.xml.parsers.DocumentBuilderFactory

class XmlSnippetHandler(private val tag: String, private val handler: (Element) -> Unit) {

    private val buf = StringBuilder()
    private var inProcess: Boolean = false
    fun handle(line: String): Boolean {
        if (line.startsWith("<$tag")) {
            inProcess = true
        }
        if (inProcess) {
            buf.append(line)
            if (line.endsWith("</$tag>")) {
                handle()
                inProcess = false
                buf.clear()
            }
            return true
        }
        return false
    }

    private fun handle() {
        val builder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        try {
            val document = builder.parse(buf.toString().byteInputStream())
            document.normalize()
            handler.invoke(document.documentElement)
        } catch (e: Exception) {
            throw IllegalStateException("Can't handle: $buf", e)
        }
    }
}