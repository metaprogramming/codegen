/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils.deps

import pl.metaprogramming.utils.update
import java.io.File

class GradlePropertiesUpdater(
    private val buildGradleFile: File,
    private val params: Map<String, Set<ArtifactVersion>>
) {
    fun update() {
        val param2ver = mutableMapOf<String, String>()
        params.forEach {
            if (it.value.distinctBy { v -> v.version }.size > 1) {
                println("Can't update parameter ${it.key} - versions mismatch: ${it.value}")
            } else {
                param2ver[it.key] = it.value.first().version
            }
        }
        findGradleProperties(buildGradleFile.parentFile)?.update { line ->
            val param = line.split("=")[0]
            if (param2ver.contains(param)) "$param=${param2ver[param]}" else line
        }
    }

    private fun findGradleProperties(dir: File): File? {
        val gradleProperties = File(dir, "gradle.properties")
        return if (gradleProperties.exists())
            gradleProperties
        else if (dir.parentFile != null) findGradleProperties(dir.parentFile)
        else null
    }
}