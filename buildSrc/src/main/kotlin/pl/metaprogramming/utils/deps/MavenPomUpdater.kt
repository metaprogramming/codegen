/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils.deps

import pl.metaprogramming.utils.update
import java.io.File

class MavenPomUpdater(private val file: File, private val versionFinder: ArtifactVersionFinder) {

    companion object {
        private val dependencyTags = listOf("dependency", "plugin")
        private val dependencyStartTags = dependencyTags.map { "<$it>" }
        private val dependencyEndTags = dependencyTags.map { "</$it>" }
    }

    fun update() {
        var dependencyHandler: DependencyHandler? = null
        var inDependencyTag = false
        file.update { line ->
            val trimmed = line.trim()
            if (inDependencyTag) {
                inDependencyTag = !dependencyEndTags.contains(trimmed)
                dependencyHandler!!.handle(line, trimmed)
            } else {
                inDependencyTag = dependencyStartTags.contains(trimmed)
                dependencyHandler = DependencyHandler()
                line
            }
        }
    }

    private inner class DependencyHandler {
        lateinit var artifactId: String
        lateinit var groupId: String

        fun handle(line: String, trimmed: String): String {
            when {
                trimmed.startsWith("<groupId>") -> groupId = trimmed.getTagValue()
                trimmed.startsWith("<artifactId>") -> artifactId = trimmed.getTagValue()
                trimmed.startsWith("<version>") -> {
                    val ver = trimmed.getTagValue()
                    if (ver != "\${project.version}") {
                        if (ver.startsWith("$")) {
                            error("Unsupported maven parameter version")
                        }
                        val latestVersion = versionFinder.find(groupId, artifactId).version
                        if (ver != latestVersion) {
                            println("   update $groupId:$artifactId $ver -> $latestVersion")
                            return line.setTagValue(latestVersion)
                        }
                    }
                }
            }
            return line
        }

        private fun String.getTagValue(): String {
            val i1 = indexOf(">") + 1
            return substring(i1, indexOf("<", i1 + 2))
        }

        private fun String.setTagValue(value: String): String {
            val i1 = indexOf(">") + 1
            return substring(0, i1) + value + substring(indexOf("<", i1 + 2))
        }
    }
}