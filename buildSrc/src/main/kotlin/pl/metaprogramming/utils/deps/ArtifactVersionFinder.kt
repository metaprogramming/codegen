/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils.deps

import pl.metaprogramming.utils.readXml
import java.net.URI
import java.util.regex.Pattern

private const val REPO_CENTRAL = "https://repo1.maven.org/maven2"
private const val REPO_GRADLE = "https://plugins.gradle.org/m2"

private val IGNORE_VERSION = listOf(
    "alpha",
    "beta",
    "rc", // "org.jetbrains.kotlin:kotlin-gradle-plugin:2.0.0-RC1"
    "m", // "org.spockframework:spock-core:2.4-M1-groovy-2.5"
)

class ArtifactVersionFinder {

    private val cache = mutableMapOf<String, ArtifactVersion>()
    private val versionConstraints = mutableListOf<VersionConstraint>()

    fun find(id: String) = cache.computeIfAbsent(id) {
        val isGradlePlugin = !id.contains(":")
        val mavenId = if (isGradlePlugin) findMavenArtifact(id) else id
        val repoUrls = if (isGradlePlugin) listOf(REPO_GRADLE, REPO_CENTRAL) else listOf(REPO_CENTRAL, REPO_GRADLE)
        val c = versionConstraints.find { it.pattern.matcher(mavenId).matches() }
        val groupId = mavenId.split(":")[0]
        val artifactId = mavenId.split(":")[1]
        val latestVersion = c?.fixedVersion
            ?: repoUrls.firstNotNullOfOrNull { repoUrl ->
                val metadataUrl = "$repoUrl/${groupId.replace(".", "/")}/${artifactId}/maven-metadata.xml"
                findLatestVersion(metadataUrl, c?.mainVersion)
            }
            ?: "error: Can't find version for $mavenId"
        ArtifactVersion(groupId, artifactId, latestVersion)
    }

    fun find(groupId: String, artifactId: String) = find("$groupId:$artifactId")

    fun force(pattern: String, mainVersion: String? = null, fixedVersion: String? = null) {
        versionConstraints.add(VersionConstraint(Pattern.compile(pattern), mainVersion, fixedVersion))
    }

    private fun findMavenArtifact(gradlePluginId: String): String {
        if (gradlePluginId == "pl.metaprogramming.codegen") {
            return "pl.metaprogramming:codegen"
        }
        val pluginPage = URI("https://plugins.gradle.org/plugin/$gradlePluginId").toURL().readText()
        val mark = "classpath(\""
        val idx = pluginPage.indexOf(mark) + mark.length
        val artifact = pluginPage.substring(idx, pluginPage.indexOf("\"", idx + 1))
        return artifact.substring(0, artifact.lastIndexOf(":")) // cut-off version
    }

    private fun findLatestVersion(metadataUrl: String, mainVersion: String?): String? {
        try {
            val metadata = metadataUrl.readXml().getChild("versioning")
            val latest = metadata.getChildText("latest")
            if (mainVersion == null && latest.isAcceptedVersion()) return latest
            val verPrefix = mainVersion ?: ""
            return metadata.getChild("versions").getChildren("version")
                .map { it.text }
                .sortedWith(::versionCompare)
                .last { it.startsWith(verPrefix) && it.isAcceptedVersion() }
        } catch (e: Exception) {
            return null
        }
    }

    private fun String.isAcceptedVersion(): Boolean {
        val ver = lowercase()
        return IGNORE_VERSION.none { ver.contains(it) }
    }

    private data class VersionConstraint(val pattern: Pattern, val mainVersion: String?, val fixedVersion: String?)

    private fun versionCompare(v1: String, v2: String): Int {
        val v1Parts = v1.split(".")
        val v2Parts = v2.split(".")
        if (v1Parts.size == v2Parts.size) {
            for (idx in v1Parts.indices) {
                if (v1Parts[idx] != v2Parts[idx]) {
                    val v1p = v1Parts[idx].toIntOrNull()
                    val v2p = v2Parts[idx].toIntOrNull()
                    if (v1p != null && v2p != null) {
                        return v1p.compareTo(v2p)
                    }
                    return v1Parts[idx].compareTo(v2Parts[idx])
                }
            }
        }
        return v1.compareTo(v2)
    }
}