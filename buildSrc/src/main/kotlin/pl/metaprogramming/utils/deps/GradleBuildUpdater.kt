/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils.deps

import java.io.File
import pl.metaprogramming.utils.update

class GradleBuildUpdater(private val file: File, private val versionFinder: ArtifactVersionFinder) {

    private val parametrizedVersions = mutableMapOf<String, MutableSet<ArtifactVersion>>()

    fun update() {
        val pluginsProcessor = PluginsProcessor()
        val dependenciesProcessor = DependenciesProcessor()
        file.update { line -> dependenciesProcessor.process(line) ?: pluginsProcessor.process(line) ?: line }
        if (parametrizedVersions.isNotEmpty()) {
            GradlePropertiesUpdater(file, parametrizedVersions).update()
        }
    }

    private inner class DependenciesProcessor : Processor("dependencies {") {
        override fun handle(line: String, quotedText: List<String>): String {
            // unsupported syntax:
            // compileOnly group: 'org.osgi', name: 'org.osgi.framework', version: '1.10.0'
            if (quotedText.size != 1) return line
            val artifactParts = quotedText[0].split(":")
            if (artifactParts.size != 3) return line
            return handle(line, versionFinder.find(artifactParts[0], artifactParts[1]), artifactParts[2])
        }
    }

    private inner class PluginsProcessor : Processor("plugins {") {
        override fun handle(line: String, quotedText: List<String>): String {
            if (quotedText.size == 2) {
                val isKotlin = line.trim().startsWith("kotlin(\"")
                val pluginId = if (isKotlin) "org.jetbrains.kotlin:kotlin-gradle-plugin" else quotedText[0]
                return handle(line, versionFinder.find(pluginId), quotedText[1])
            }
            return line
        }
    }

    private abstract inner class Processor(val token: String) {

        private var state: ProcessState = ProcessState.UNPROCESSED

        abstract fun handle(line: String, quotedText: List<String>): String

        fun process(line: String): String? {
            return when (state) {
                ProcessState.PROCESSED -> null
                ProcessState.UNPROCESSED -> {
                    if (line.startsWith(token)) {
                        state = ProcessState.IN_PROCESS
                        null
                    } else null
                }

                ProcessState.IN_PROCESS -> {
                    if (line.startsWith("}")) {
                        state = ProcessState.PROCESSED
                        line
                    } else {
                        handle(line, line.readQuotedText())
                    }
                }
            }
        }

        protected fun handle(line: String, latestVersion: ArtifactVersion, ver: String): String {
            if (ver == latestVersion.version) return line
            if (latestVersion.failed) {
                println(latestVersion.version)
                return line
            }
            if (ver.startsWith("$")) {
                val param = if (ver.startsWith("\${")) ver.substring(2, ver.length - 1) else ver.substring(1)
                parametrizedVersions.computeIfAbsent(param) { mutableSetOf() }.add(latestVersion)
                return line
            }
            return line.replace(ver, latestVersion.version)
        }
    }

    private enum class ProcessState {
        UNPROCESSED,
        PROCESSED,
        IN_PROCESS
    }

    private fun String.readQuotedText(): List<String> {
        val result = mutableListOf<QuotedText>()
        readQuotedText("'", result)
        readQuotedText("\"", result)
        return result.sortedBy { it.idx }.map { it.text }
    }

    private fun String.readQuotedText(quoteMark: String, result: MutableList<QuotedText>) {
        var idx = indexOf(quoteMark)
        while (idx >= 0) {
            val endIdx = indexOf(quoteMark, idx + 1)
            result.add(QuotedText(substring(idx + 1, endIdx), idx))
            idx = indexOf(quoteMark, endIdx + 1)
        }
    }

    private data class QuotedText(val text: String, val idx: Int)
}