plugins {
    `java-library`
    `maven-publish`
    signing

    groovy // tests in groovy
    jacoco

    id("org.barfuin.gradle.jacocolog")
    id("com.adarshr.test-logger")
    id("io.github.chiragji.jacotura")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
    withJavadocJar()
    withSourcesJar()
}

tasks {
    test {
        useJUnitPlatform()
        extensions.configure(JacocoTaskExtension::class) {
//            setDestinationFile(file(layout.buildDirectory.dir("jacoco/${project.name}-test-java-${JavaVersion.current()}.exec")))
            setDestinationFile(file(rootProject.layout.buildDirectory.dir("jacoco/${project.name}-test-java-${JavaVersion.current()}.exec")))
        }
    }

    jacocoTestReport {
        reports {
            xml.required.set(true)
            html.required.set(true)
        }
    }

    jacotura {
        val outDir = layout.buildDirectory.get().asFile
        properties {
            property("jacotura.jacoco.path", "$outDir/reports/jacoco/test/jacocoTestReport.xml")
            property("jacotura.cobertura.path", "$outDir/cobertura.xml")
        }
    }
}

if (project.hasProperty("sonatypeUsername")) {

    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(components["java"])
                pom {
                    name.set("pl.metaprogramming:${project.name}")
                    description.set("Java code generator framework")
                    url.set("http://metaprogramming.pl")
                    licenses {
                        license {
                            name.set("The Apache License, Version 2.0")
                            url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                        }
                    }
                    developers {
                        developer {
                            id.set("waldenko")
                            name.set("Dawid Walczak")
                            email.set("dawid.m.walczak@gmail.com")
                        }
                    }
                    scm {
                        url.set("https://gitlab.com/metaprogramming/codegen")
                        connection.set("scm:git:https://gitlab.com/metaprogramming/codegen.git")
                        developerConnection.set("scm:git:ssh://git@gitlab.com:metaprogramming/codegen.git")
                    }
                }
            }
        }
    }

    signing {
        sign(publishing.publications["mavenJava"])
    }
} else {
    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(components["java"])
            }
        }
    }
}
