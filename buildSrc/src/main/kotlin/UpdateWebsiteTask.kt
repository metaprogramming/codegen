import com.diffplug.spotless.changelog.gradle.ChangelogExtension
import com.google.gson.Gson
import groovy.ant.FileNameFinder
import org.apache.commons.lang3.StringUtils
import org.gradle.api.tasks.TaskAction
import org.w3c.dom.Element
import pl.metaprogramming.utils.*
import java.io.File
import java.io.PrintWriter

/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

open class UpdateWebsiteTask: MaintenanceTask() {

    @TaskAction
    fun action() {
        updateWebsiteDocTemplates()
    }

    private val ext2codeFormat = mapOf(
        "gradle" to "groovy",
        "wsdl" to "xml"
    )

    private val dokkaJavaApiIndex: List<Map<String, String>> by lazy { loadDokkaIndex("java") }
    private val dokkaKotlinApiIndex: List<Map<String, String>> by lazy { loadDokkaIndex("kotlin") }
    private val snippetsIndex: Map<String, String> by lazy { SnippetsCollector(file("codegen/src/test")).collect() }

    private lateinit var currentTemplate: File

    fun changelog(): ChangelogExtension = project.properties["spotlessChangelog"] as ChangelogExtension

    private fun updateWebsiteDocTemplates() {
        val rootDir = file("maintenance/website/docs-templates")
        rootDir.walk()
            .filter { it.isFile && !it.path.contains("drafts") }
            .forEach { template ->
                currentTemplate = template
                val destFile = file("maintenance/website/docs/${rootDir.toPath().relativize(template.toPath())}")
                destFile.parentFile.mkdirs()
                destFile.createNewFile()
                destFile.printWriter().use { writer ->
                    val tagHandlers = listOf(
                        XmlSnippetHandler("includeFile") {
                            writer.includeFile(it)
                        },
                        XmlSnippetHandler("codeSnippet") {
                            writer.println(it.asCodeSnippet())
                        })

                    template.readLines().forEach { line ->
                        if (tagHandlers.none { it.handle(line) }) {
                            writer.println(
                                line.handleCodeLink("javadoc").handleCodeLink("source")
                            )
                        }
                    }
                }
            }
    }

    private fun PrintWriter.includeFile(xml: Element) {
        val includeFile = if (xml.hasAttribute("path")) xml.getAttribute("path") else xml.textContent
        val fixedFilePath = includeFile
            .replace("\${codegen-quickstart-dir}", QUICKSTART_PROJECTS)
            .replace("\${codegen-tests-dir}", TEST_PROJECTS)
        val file = file(fixedFilePath)
        val title = if (xml.hasAttribute("noTitle")) "" else "title=\"${file.name}\""
        println("")
        println("```${file.codeFormat()} $title")
        write(file.contentToInclude(xml))
        println("")
        println("```")
        println("")
    }

    private fun File.contentToInclude(xml: Element): String {
        return xml.getChildrenText("line")?.let { readLines(it) }
            ?: xml.getChildrenText("block")?.let { readBlocks(it) }
            ?: readText().trim()
    }

    private fun Element.getChildrenText(name: String) = getChildren(name).map { it.textContent }.ifEmpty { null }

    private fun File.readLines(lines: List<String>): String {
        if (!readLines().containsAll(lines)) {
            error("Not all lines present in the file ${absolutePath}:\n${lines.joinToString("\n")}")
        }
        return lines.joinToString(System.lineSeparator())
    }

    private fun File.readBlocks(blocks: List<String>): String {
        val result = mutableListOf<String>()
        var brackets = 0
        val blocksToProcess = blocks.toMutableList()
        val annotations = ArrayDeque<String>()
        readLines().forEach { line ->
            if (brackets == 0) {
                val method = blocksToProcess.firstOrNull { line.contains(it) }
                if (method != null) {
                    blocksToProcess.remove(method)
                    if (result.isNotEmpty()) {
                        result.add("")
                    }
                    result.addAll(annotations)
                    annotations.clear()
                    brackets = 1
                    result.add(line)
                } else {
                    if (line.trimStart().startsWith("@")) {
                        annotations.add(line)
                    } else {
                        annotations.clear()
                    }
                }
            } else {
                result.add(line)
                brackets += line.count { it == '{' }
                brackets -= line.count { it == '}' }
                if (brackets == 0 && blocksToProcess.isEmpty()) return@forEach
            }
        }
        if (blocksToProcess.isNotEmpty()) {
            error("Can't find blocks in the file ${absolutePath}:\n${blocksToProcess.joinToString("\n")}")
        }
        return result.joinToString(System.lineSeparator())
    }

    private fun File.codeFormat(): String {
        val ext = this.name.substringAfterLast('.')
        return ext2codeFormat[ext] ?: ext
    }

    private fun String.handleCodeLink(linkType: String): String {
        var result = this
        this.between("<$linkType>", "</$linkType>").forEach {
            val parts0 = it.split("|")
            val label = parts0.first()
            val codePath = parts0.last()
            val parts = codePath.split('.')
            val clazz = parts[0]
            val newValue = when (linkType) {
                "javadoc" -> {
                    val canonicalName =
                        canonicalName(clazz) + if (codePath.length == clazz.length) "" else codePath.substring(clazz.length)
                    // dokka incorrectly generates properties documentation for java (see notes.txt)
                    //"<nobr>`$it`${canonicalName.javaApiLink()}/${canonicalName.kotlinApiLink()}</nobr>"
                    "[$label](pathname:///kotlin-api/${dokkaKotlinApiIndex.apiLink(canonicalName)})"
                }

                "source" -> {
                    val codeUrlPath = canonicalName(clazz).replace('.', '/')
                    "[$label](https://gitlab.com/metaprogramming/codegen/-/blob/develop/src/main/kotlin/$codeUrlPath)"
                }

                else -> error("Unsupported link type $linkType")
            }
            result = result.replace("<$linkType>$it</$linkType>", newValue)
        }
        return result
    }

    private fun String.javaApiLink() =
        "[<IIcon icon=\"devicon:java\" />](pathname:///java-api/${dokkaJavaApiIndex.apiLink(this)})"

    private fun String.kotlinApiLink() =
        "[<IIcon icon=\"devicon:kotlin\" />](pathname:///kotlin-api/${dokkaKotlinApiIndex.apiLink(this)})"

    private fun canonicalName(clazz: String): String {
        val srcPath = "codegen/src/main/kotlin/"
        val names = FileNameFinder().getFileNames(project.rootDir.path, "${srcPath}/**/${clazz}.kt")
        if (names.size == 1) {
            val fixedName = names[0].replace(File.separator, "/")
            return fixedName.substring(fixedName.indexOf(srcPath) + srcPath.length, fixedName.length - 3)
                .replace('/', '.')
        }
        error("[${currentTemplate.name}] Can't find class: $clazz ($srcPath, $names)")
//        logger.warn("[${currentTemplate.name}] Can't find class: $clazz ($srcPath, $names)")
//        return clazz
    }

    private fun String.between(open: String, close: String): Array<String> {
        return StringUtils.substringsBetween(this, open, close) ?: emptyArray()
    }

    private fun loadDokkaIndex(lang: String): List<Map<String, String>> {
        val jsonString = file("codegen/build/dokka-${lang}-api/scripts/pages.json").readText(Charsets.UTF_8)
        return Gson().fromJson(jsonString, ArrayList<Map<String, String>>().javaClass)
    }

    private fun List<Map<String, String>>.apiLink(clazz: String) =
        find { it["description"] == clazz }?.getValue("location")
            ?: error("Can't find API documentation for $clazz")
//            ?: "Can't find API documentation for $clazz"

    private fun Element.asCodeSnippet(): String {
        val snippetCode = this.textContent
        val javaSnippet = snippetsIndex["java:$snippetCode"]
        val kotlinSnippet = snippetsIndex["kotlin:$snippetCode"]
        check(javaSnippet != null || kotlinSnippet != null) { "Can't find snippet: $snippetCode" }
        if (javaSnippet != null && kotlinSnippet == null) {
            println("No kotlin snippet: $snippetCode")
            return "```java\n$javaSnippet```".lines().joinToString(System.lineSeparator())
        }
        if (javaSnippet == null) {
            println("No java snippet: $snippetCode")
            return "```kt\n$kotlinSnippet```".lines().joinToString(System.lineSeparator())
        }
        return ("""
            |<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
            |<TabItem value="kotlin">
            |```kt
            |""".trimMargin() + kotlinSnippet + """```
            |</TabItem>
            |<TabItem value="java">
            |```java
            |$javaSnippet```
            |</TabItem>
            |</Tabs>
            """.trimMargin()).lines().joinToString(System.lineSeparator())
    }

    private class SnippetsCollector(private val dir: File) {
        private val result = mutableMapOf<String, String>()
        private val tag = "// CODE_SNIPPET: "
        private val inProgress = mutableMapOf<String, SnippetBuffer>()

        fun collect(): Map<String, String> {
            listOf("java", "kotlin").forEach { lang ->
                File(dir, lang).walk().filter { it.isFile }.forEach { file ->
                    file.readLines().forEach { line ->
                        val idx = line.indexOf(tag)
                        if (idx >= 0) {
                            handleTag("$lang:" + line.substring(idx + tag.length), idx)
                        } else {
                            handle(line)
                        }
                    }
                    check(inProgress.isEmpty()) { "[${file.absolutePath}] Unfinished snippets: $inProgress" }
                }
            }
            return result
        }

        private fun handle(line: String) {
            inProgress.values.forEach {
                if (line.length > it.margin) {
                    it.buf.append(line.substring(it.margin))
                }
                it.buf.append("\n")
            }
        }

        private fun handleTag(code: String, margin: Int) {
            if (inProgress.containsKey(code)) {
                result[code] = inProgress.remove(code)!!.buf.toString()
            } else {
                check(!result.containsKey(code)) { "Snippet $code already registered" }
                inProgress[code] = SnippetBuffer(margin)
            }
        }

        class SnippetBuffer(val margin: Int, val buf: StringBuilder = StringBuilder())
    }

}