/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.gradle.api.tasks.TaskAction
import pl.metaprogramming.utils.MaintenanceTask
import pl.metaprogramming.utils.deps.GradleBuildUpdater
import pl.metaprogramming.utils.deps.GradleWrapperUpdater
import pl.metaprogramming.utils.deps.ArtifactVersionFinder
import pl.metaprogramming.utils.deps.MavenPomUpdater
import pl.metaprogramming.utils.getJavaVersion

open class UpdateDependenciesTask : MaintenanceTask() {

    @TaskAction
    fun action() {
        val versionFinder = ArtifactVersionFinder()
        val gradleUpdater = GradleWrapperUpdater()

        versionFinder.force("pl.metaprogramming:codegen", fixedVersion = project.version as String)
        versionFinder.force("pl.metaprogramming:codegen-maven-plugin", fixedVersion = project.version as String)

        versionFinder.force("javax.xml.bind:jaxb-api", mainVersion = "2.3.")
        versionFinder.force("org.jetbrains.kotlin:kotlin-gradle-plugin", mainVersion = "1.9.")
        versionFinder.force("org.jetbrains.kotlin:kotlin-gradle-plugin", mainVersion = "1.9.")
        versionFinder.force("org.apache.maven:maven-plugin-api", mainVersion = "3.6.")
        versionFinder.force("org.apache.maven.plugin-tools:maven-plugin-annotations", mainVersion = "3.6.")

        // keep codegen compatible with java 8
        versionFinder.force("org.apache.cxf:.*", mainVersion = "3.5.")
        versionFinder.force("ch.qos.logback:.*", mainVersion = "1.3.")

        // keep codegen build scripts compatible with java 8
        // smoke tests are run from codegen build
        versionFinder.force("com.lordcodes.turtle:turtle", fixedVersion = "0.8.0") // compatible with java 8 (codegen)
        versionFinder.force("com.diffplug.spotless-changelog:.*", mainVersion = "2.")
        versionFinder.force("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin", mainVersion = "4.0.")

        // keep codegen unit tests compatible with java 8
        versionFinder.force("org.jboss.forge.roaster:.*", mainVersion = "2.22.")
        versionFinder.force("org.eclipse.jdt:org.eclipse.jdt.core", fixedVersion = "3.10.0")

        // keep in test projects org.junit.jupiter fixed to 5.7.*
        versionFinder.force("org.junit.jupiter:.*", mainVersion = "5.7.")

        // fix xml implementations
        versionFinder.force("com.sun.xml.bind:jaxb-impl", mainVersion = "2.3.")
        versionFinder.force("com.sun.xml.messaging.saaj:saaj-impl", mainVersion = "1.")

        // TODO make smoke test works on java 21 (migrate to springboot 3)
        // wait for a new spock version
        versionFinder.force("org.zalando:logbook-spring-boot-starter", fixedVersion = "2.14.0")
        versionFinder.force("org.springframework.boot:.*", mainVersion = "2.")
        if (getJavaVersion() < 17) {
            versionFinder.force("org.eclipse.jdt:org.eclipse.jdt.core", fixedVersion = "3.10.0")
            versionFinder.force("org.jboss.forge.roaster:roaster-jdt", fixedVersion = "2.22.3.Final")
            versionFinder.force("io.undertow:undertow-core", mainVersion = "2.2.")
        }

        val skipDirs = setOf("build", ".gradle", "target")

        file(".").walk()
            .onEnter { !skipDirs.contains(it.name) }
            .forEach {
                when (it.name) {
                    "build.gradle", "build.gradle.kts" -> GradleBuildUpdater(it, versionFinder).update()
                    "gradle-wrapper.properties" -> gradleUpdater.update(it)
                    "pom.xml" -> MavenPomUpdater(it, versionFinder).update()
                }
            }
   }
}