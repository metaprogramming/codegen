/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.lordcodes.turtle.shellRun
import org.gradle.api.tasks.TaskAction
import pl.metaprogramming.utils.MaintenanceTask
import pl.metaprogramming.utils.QUICKSTART_PROJECTS
import pl.metaprogramming.utils.TEST_PROJECTS
import pl.metaprogramming.utils.isGradleProject

open class SetupGradleTestProjectsTask : MaintenanceTask() {
    @TaskAction
    fun action() {
        listOf(TEST_PROJECTS, QUICKSTART_PROJECTS).flatMap { group ->
            file(group).listFiles { file -> file.isGradleProject() }!!.map { "$group/${it.name}" }.toList()
        }.forEach {
            copy("gradle/wrapper", "$it/gradle/wrapper")
            copy("gradlew", it)
            copy("gradlew.bat", it)
            shellRun("git", listOf("add", "$it/gradlew"))
            shellRun("git", listOf("update-index", "--chmod=+x", "$it/gradlew"))
        }
    }
}