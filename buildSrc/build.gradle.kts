plugins {
    kotlin("jvm") version "1.9.25"
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal() // so that external plugins can be resolved in dependencies section
    mavenCentral()
}

dependencies {
    implementation("com.diffplug.spotless-changelog:spotless-changelog-lib:2.4.1")
    implementation("com.diffplug.spotless-changelog:spotless-changelog-plugin-gradle:2.4.1")
    implementation("org.osgi:org.osgi.framework:1.10.0")
    implementation("org.apache.commons:commons-lang3:3.17.0")
    implementation("com.lordcodes.turtle:turtle:0.8.0")
    implementation("com.google.code.gson:gson:2.11.0")
    implementation("org.jdom:jdom2:2.0.6.1")

    implementation("org.barfuin.gradle.jacocolog:gradle-jacoco-log:3.1.0")
    implementation("com.adarshr.test-logger:com.adarshr.test-logger.gradle.plugin:4.0.0")
    implementation("io.github.chiragji:jacotura-gradle-plugin:1.1.2")
}
