/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.fixtures

import pl.metaprogramming.model.data.constraint.CheckerRef
import pl.metaprogramming.model.data.constraint.Constraints
import pl.metaprogramming.model.oas.RestApi

class ExampleApiModels {

    static enum OpenapiVer {
        V2('v2/'),
        V3('')
        String location

        OpenapiVer(String location) {
            this.location = location
        }
    }

    static def loadExampleApiDepsModel(OpenapiVer ver = OpenapiVer.V3) {
        def content = Thread.currentThread().getContextClassLoader().getResource("openapi/${ver.location}example-api-deps.yaml").text
        RestApi.of(content)
    }

    static def loadExampleApiModel(RestApi exampleApiDepsModel, OpenapiVer ver = OpenapiVer.V3) {
        def filePath = "/openapi/${ver.location}example-api.yaml"
        def apiBody = ver.class.getResourceAsStream(filePath).text
        def api = RestApi.of(apiBody, exampleApiDepsModel)
                .forEachOperation {
                    if (it.path.contains('/echo-file')) {
                        it.group = 'echo'
                    }
                }
                .removeOperations { it.code == 'skipped' }
                .updateSchema('EchoBody.prop_string_pattern') {
                    it.setInvalidPatternCode('custom_error_code')
                }
        if (ver == OpenapiVer.V2) {
            api.getParameter('Authorization').addConstraint(CheckerRef
                    .of('example.adapters.in.rest.validators.AuthorizationChecker')
                    .setPriority(0))
//            api.updateSchema('EchoBody.prop_string') {
//                it.add(Constraints.dictionary('COLORS'))
//            }
//          longer transcript for above commented instruction
            api.forEachSchema {
                if (it.code == 'EchoBody') {
                    it.objectType.fields
                            .find { it.code == 'prop_string' }
                            .addConstraint(Constraints.dictionary('COLORS'))
                }
            }

            api.updateSchema('EchoArraysBody.prop_string_list') {
                it.arrayType.itemsSchema.addConstraint(Constraints
                        .dictionary('ANIMALS')
                        .setErrorCode('invalid-animal')
                        .setPriority(0))
            }
            api.getSchema('EchoArraysBody.prop_object_list').arrayType.itemsSchema.addConstraint(Constraints
                    .rule('SIMPLE_OBJECT_CUSTOM_CONSTRAINT')
                    .setErrorCode('custom.failed.code'))
        }
        api
    }
}
