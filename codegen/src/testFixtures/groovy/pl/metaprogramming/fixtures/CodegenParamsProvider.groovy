/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.fixtures


import pl.metaprogramming.codegen.java.DefaultJavaNameMapper
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.constraint.CheckerRef

class CodegenParamsProvider {

    static final def LEGACY_JAVA_NAME_MAPPER = new DefaultJavaNameMapper() {
        @Override
        String toFieldName(DataSchema schema) {
            toFieldName(schema.code)
        }
    }

    static ValidationParams makeValidationParams() {
        new ValidationParams()
                .formatValidator('amount', CheckerRef.of('example.commons.validator.AmountChecker'))
                .ruleValidator('AUTHORIZATION_CONSTRAINT', 'example.adapters.in.rest.validators.AuthorizationChecker')
                .ruleValidator('USER_AMOUNT', CheckerRef.ofMethodRef('example.adapters.in.rest.validators.UserDataValidationBean', 'checkAmountByUser'))
                .ruleValidator('AMOUNT_SCALE', CheckerRef.ofStaticField('example.commons.validator.Checkers', 'AMOUNT_SCALE_CHECKER'))
                .ruleValidator('EXTENDED_OBJECT_CONSTRAINT', 'example.adapters.in.rest.validators.ExtendedObjectChecker')
                .ruleValidator('EXTENDED_OBJECT_ENUM_CONSTRAINT', 'example.adapters.in.rest.validators.ExtendedObjectEnumChecker')
    }

}
