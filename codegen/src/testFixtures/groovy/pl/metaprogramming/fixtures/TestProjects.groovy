/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.fixtures

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.spring.*
import pl.metaprogramming.codegen.java.spring.rs.SpringRsTypeOfCode
import pl.metaprogramming.codegen.java.test.TestDataParams
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.oas.HttpRequestSchema
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.RestApi
import pl.metaprogramming.model.wsdl.WsdlApi

import static pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.*

class TestProjects {

    static def rsServerSpring1tGenerator = new RsServerGenerator('rs-server-spring-1t') {
        @Override
        void setParams(CodegenParams params) {
            setDefaultJavaParams(params, false)
                    .set(new SpringRestParams().injectBeanIntoRequest('context', 'example.process.Context'))
                    .set(new TestDataParams()
                            .valueByType(DataTypeCode.BASE64, '"dGVzdA=="')
                            .valueByCode('prop_amount', '10.10')
                            .valueByCode([
                                    'prop_string'    : 'BLACK',
                                    'prop_int_second': '2'
                            ])
                    )
        }

        @Override
        void customize(SpringCommonsGenerator it) {
            it.typeOfCode(SpringCommonsGenerator.TOC.REST_EXCEPTION_HANDLER) {
                it.onDecoration { cls ->
                    cls.fields.add("MESSAGE_BY_CODE", Java.map(Java.string(), Java.string())) {
                        it.static = true
                        it.final = true
                        it.value = ValueCm.value("makeMessageByCode()")
                    }
                    cls.methods.add("makeMessageByCode") { m ->
                        m.resultType = Java.string().asMapBy(Java.string())
                        m.static = true
                        m.implBody = cls.codeBuf.addLines(
                                "Map<String, String> result = new HashMap<>();",
                                'result.put(ERR_CODE_IS_REQUIRED, "is required");',
                                'result.put(ERR_CODE_IS_NOT_BASE64, "is not base64");',
                                'result.put(ERR_CODE_IS_NOT_BOOLEAN, "should have one of values: [true, false]");',
                                'result.put(ERR_CODE_IS_NOT_DOUBLE, "is not double");',
                                'result.put(ERR_CODE_IS_NOT_FLOAT, "is not float");',
                                'result.put(ERR_CODE_IS_NOT_INT, "is not 32bit integer");',
                                'result.put(ERR_CODE_IS_NOT_LONG, "is not 64bit integer");',
                                'result.put(ERR_CODE_IS_NOT_NUMBER, "is not number");',
                                'result.put(ERR_CODE_IS_NOT_DATE, "is not yyyy-MM-dd");',
                                'result.put(ERR_CODE_IS_NOT_DATE_TIME, "should be valid date time in ISO8601 format");',
                                'return result;'
                        ).take()
                    }
                    cls.methods.get("toValidationResult(field, code)").implBody = cls.codeBuf
                            .add("return new ValidationResult(null).addError(ValidationError.builder()")
                            .indentInc(2)
                            .addLines(".field(field)",
                                    ".code(code)",
                                    ".message(field + ' ' + MESSAGE_BY_CODE.get(code))",
                                    ".build());",
                            ).take()
                    cls.methods.get("toResponseEntity(e, request)").with {
                        implDependencies.add('example.ports.in.rest.dtos.ErrorDescriptionDto')
                        implBody = "return new ResponseEntity<>(new ErrorDescriptionDto().setCode(e.getStatus().value()).setMessage(e.getMessage()), e.getStatus());"
                    }
                    cls.methods.get("toResponseEntity(result, request)").with {
                        implDependencies.add(cls.getClass(SpringRestServiceGenerator.TOC.DTO, apiDepsModel.schemas.find { it.code == 'ErrorItem' }.dataType))
                        implDependencies.add(cls.getClass(SpringRestServiceGenerator.TOC.DTO, apiDepsModel.schemas.find { it.code == 'ErrorDetail' }.dataType))
                        implDependencies.add(cls.getClass(SpringRestServiceGenerator.TOC.REQUEST_DTO, apiModel.getOperation("echo-get").requestSchema))
                        implBody = cls.codeBuf
                                .ifBlock("result.getStatus() == 401",
                                        "return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorDescriptionDto().setCode(result.getStatus()).setMessage(\"Unauthenticated\"));")
                                .endBlock()
                                .ifBlock("result.getStatus() == 403",
                                        "return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ErrorDescriptionDto().setCode(result.getStatus()).setMessage(\"Unauthorized\"));")
                                .endBlock()
                                .ifBlock("result.getRequest() instanceof EchoGetRequest",
                                        "List<ErrorItemDto> items = result.getErrors().stream()",
                                        "        .map(d -> new ErrorItemDto().setCode(d.getCode()))",
                                        "        .collect(Collectors.toList());",
                                        "return new ResponseEntity<>(items, HttpStatus.valueOf(result.getStatus()));"
                                )
                                .endBlock()
                                .addLines("""return new ResponseEntity<>(new ErrorDescriptionDto()
        .setCode(result.getStatus())
        .setMessage(result.getMessage())
        .setErrors(result.getErrors().stream()
                .map(d -> new ErrorDetailDto().setCode(d.getCode()).setField(d.getField()).setMessage(d.getMessage()))
                .collect(Collectors.toList())),
        HttpStatus.valueOf(result.getStatus()));""")
                                .take()
                    }
                }
            }
        }
    }

    static def rsServerSpring1tJbvGenerator = new RsServerGenerator('rs-server-spring-1t-jbv') {
        @Override
        void setParams(CodegenParams params) {
            setDefaultJavaParams(params)
                    .set(new ValidationParams().useJakartaBeanValidation(true))
                    .set(new SpringRestParams().delegateToFacade(true))
                    .set(new TestDataParams().enabled(false))
        }
    }

    static def rsClientSpring1tGenerator = new TestProjectGenerator('rs-client-spring-1t') {
        @Override
        void setParams(CodegenParams params) {
            setDefaultJavaParams(params, false)
        }

        @Override
        Codegen makeCodegen() {
            def apiDepsModel = ExampleApiModels.loadExampleApiDepsModel()
            def apiModel = ExampleApiModels.loadExampleApiModel(apiDepsModel)
            super.makeCodegen()
                    .generate(new SpringCommonsGenerator()) {
                        it.rootPackage = 'example.commons'
                    }
                    .generate(new SpringRestClientGenerator()) {
                        it.model = apiDepsModel
                        it.rootPackage = 'example'
                        it.typeOfCode(SpringRestClientGenerator.TOC.TEST_DATA_PROVIDER) {
                            it.packageName.tail = 'commons'
                        }
                    }
                    .generate(new SpringRestClientGenerator()) {
                        it.model = apiModel
                        it.rootPackage = 'example'
                    }
        }
    }

    static def wsClientSpringGenerator = new TestProjectGenerator('ws-client-spring') {
        @Override
        void setParams(CodegenParams params) {
            setDefaultJavaParams(params)
                    .set(JavaParams) {
                        it.alwaysGenerateEnumFromValueMethod = true
                    }
        }

        @Override
        Codegen makeCodegen() {
            getProjectDir()

            def wsdlApi = WsdlApi.of(wsdlLocation) {
                it.serviceNameMapper = { name -> name.replace('PortService', '') }
            }.removeOperations { it.name == 'echoExtendedSkipped' }
            super.makeCodegen()
                    .generate(new SpringCommonsGenerator()) {
                        it.rootPackage = 'example.commons'
                    }
                    .generate(new SpringSoapClientGenerator()) {
                        it.model = wsdlApi
                        it.rootPackage = 'example'
                        it.setNamespacePackage('http://example.com/echo1', 'example.ws.ns2')
                        it.setNamespacePackage('http://example.com/echo2', 'example.ws.ns3')
                        it.setUrlProperty("WS_ECHO_URL:http://localhost:8080/ws/echo")
                    }
        }

        private String getWsdlLocation() {
            def path = 'src/test/resources/wsdl/echo.wsdl'
            firstExists(path, "../codegen/$path")
        }
    }

    static abstract class RsServerGenerator extends TestProjectGenerator {

        RestApi apiDepsModel
        RestApi apiModel

        RsServerGenerator(String project) {
            super(project)
        }

        @Override
        Codegen makeCodegen() {
            apiDepsModel = ExampleApiModels.loadExampleApiDepsModel()
            apiModel = ExampleApiModels.loadExampleApiModel(apiDepsModel)
            super.makeCodegen()
                    .generate(new SpringCommonsGenerator()) {
                        it.rootPackage = 'example.commons'
                        it.projectDir = ''
                        it.typeOfCode(REST_EXCEPTION_HANDLER) {
                            it.packageName.root = 'example.adapters.in.rest'
                        }
                        it.typeOfCode(VALIDATION_DICTIONARY_CHECKER, VALIDATION_DICTIONARY_CODE_ENUM, VALIDATION_ERROR_CODE_ENUM)
                                .forEach {
                                    it.packageName {
                                        it.root = 'example.adapters.in.rest.validators.custom'
                                        it.tail = ""
                                    }
                                }
                        customize(it)
                    }
                    .generate(new SpringRestServiceGenerator()) {
                        it.model = apiDepsModel
                        it.rootPackage = 'example'
                        it.typeOfCode(it.toc.TEST_DATA_PROVIDER) {
                            it.packageName.tail = 'commons'
                        }
                    }
                    .generate(new SpringRestServiceGenerator()) {
                        it.model = apiModel
                        it.rootPackage = 'example'
                        it.typeOfCode(SpringRsTypeOfCode.REQUEST_DTO) {
                            it.onDecoration { ClassCmBuilderTemplate<HttpRequestSchema> template ->
                                if ((template.model.operation as Operation).security) {
                                    template.implementationOf('example.process.IRequest')
                                }
                            }
                        }
                    }
        }

        void customize(SpringCommonsGenerator it) {}
    }

}
