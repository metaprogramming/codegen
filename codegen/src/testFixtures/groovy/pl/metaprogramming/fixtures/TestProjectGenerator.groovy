/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.fixtures

import org.jboss.forge.roaster.Roaster
import pl.metaprogramming.codegen.CodeDecorator
import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.internal.MainGenerator
import pl.metaprogramming.codegen.java.JavaParams

abstract class TestProjectGenerator {
    String testProject

    TestProjectGenerator(String project) {
        this.testProject = project
    }

    @Override
    String toString() {
        testProject
    }

    abstract void setParams(CodegenParams params)

    protected CodegenParams setDefaultJavaParams(CodegenParams params, boolean formatCode = true) {
        params
                .set(JavaParams) {
                    if (formatCode) it.addCodeDecorator(makeCodeFormatter())
                }
                .set(CodegenParamsProvider.makeValidationParams().stopAfterFirstError(false))
    }

    Codegen makeCodegen() {
        def codegen = new Codegen()
                .addLastUpdateTag(false)
                .baseDir(projectDir)
                .overrideWhenDiffsInWhitespacesOnly(true)
                .indexFile(new File(projectDir, 'generatedFiles.yaml'))
        setParams(codegen.params)
        codegen
    }

    static MainGenerator run(Codegen codegen) {
        def generator = new MainGenerator(codegen.cfg)
        generator.runAll()
        generator
    }

    File getProjectDir() {
        new File(testProjectsDir, testProject)
    }

    private File getTestProjectsDir() {
        new File(firstExists('test-projects', '../smoke-tests/test-projects/'))
    }

    protected String firstExists(String... paths) {
        for (path in paths) {
            if (new File(path).exists()) return path
        }
        throw new IllegalStateException("None path exists ${Arrays.asList(paths)}")
    }

    MainGenerator generate() {
        run(makeCodegen())
    }

    protected CodeDecorator makeCodeFormatter() {
        new CodeDecorator() {
            String apply(String code) {
                def props = new Properties()
                props.put('org.eclipse.jdt.core.formatter.tabulation.char', 'space')
                props.put('org.eclipse.jdt.core.formatter.lineSplit', '120')
                Roaster.format(props, code)
            }
        }
    }

    static final def LEGACY_JAVA_NAME_MAPPER = CodegenParamsProvider.LEGACY_JAVA_NAME_MAPPER


}
