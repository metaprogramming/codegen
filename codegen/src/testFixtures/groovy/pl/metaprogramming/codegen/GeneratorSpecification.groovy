/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.fixtures.CodegenParamsProvider
import pl.metaprogramming.utils.CheckUtils
import pl.metaprogramming.utils.ClassShadow
import pl.metaprogramming.utils.JavaCodeGenerationTestUtils
import pl.metaprogramming.utils.MethodShadow
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

abstract class GeneratorSpecification extends Specification {

    static final String MODULE_COMMONS = 'Commons'

    static final def JAVA_NAME_MAPPER = CodegenParamsProvider.LEGACY_JAVA_NAME_MAPPER

    @Shared
    Map<String, List<CodeGenerationTask>> tasks

    @Shared
    Map<String, List<ClassShadow>> classesToCheck

    @Shared
    Map<String, List<String>> expectedClasses

    def setupSpec() {
        tasks = generateCodeModels(makeCodegen())
        expectedClasses = makeExpectedClasses()
        classesToCheck = makeClassesToCheck()
        assert expectedClasses[MODULE_COMMONS] != null
        classesToCheck.put(MODULE_COMMONS, classesToCheckForCommons()
                .findAll { expectedClasses[MODULE_COMMONS].contains(it.name) })
    }

    abstract Codegen makeCodegen()

    abstract Map<String, List<ClassShadow>> makeClassesToCheck()

    abstract Map<String, List<String>> makeExpectedClasses()

    @Unroll
    def "check codes of #module"() {
        given:
        List<String> given = tasks[module].collect { CheckUtils.filepath2ClassName(it.destFilePath) }

        expect:
        assert expectedClasses[module]
        CheckUtils.checkList('files', given, expectedClasses[module])

        where:
        module << tasks.keySet()
    }

    def "#entry.shadow.name (#entry.module) should match the shadow"() {
        when:
        def codeGenerationTask = JavaCodeGenerationTestUtils.findGenerationTask(tasks[entry.module], entry.shadow)
        then:
        codeGenerationTask != null
        JavaCodeGenerationTestUtils.checkClass(codeGenerationTask, entry.shadow, generatedAnnotationClass())

        where:
        entry << flattenClassesToCheck()
    }

    def "#entry.shadow.fullName (#entry.module) should match the shadow"() {
        when:
        def codeGenerationTask = JavaCodeGenerationTestUtils.findGenerationTask(tasks[entry.module], entry.shadow.ownerClass)
        then:
        codeGenerationTask != null
        JavaCodeGenerationTestUtils.checkMethod(codeGenerationTask, entry.shadow)

        where:
        entry << flattenMethodsToCheck()
    }

    private List<CheckEntry<ClassShadow>> flattenClassesToCheck() {
        def result = []
        classesToCheck.each { module, classes ->
            classes.each {
                result.add(new CheckEntry(module: module, shadow: it))
            }
        }
        result
    }

    private List<CheckEntry<MethodShadow>> flattenMethodsToCheck() {
        def result = []
        classesToCheck.each { module, classes ->
            JavaCodeGenerationTestUtils.findMethodsToCheck(classes).each {
                result.add(new CheckEntry(module: module, shadow: it))
            }
        }
        result
    }

    static class CheckEntry<T> {
        String module
        T shadow
    }


    static Map<String, List<CodeGenerationTask>> generateCodeModels(Codegen codegen) {
        codegen.cfg.modules.each {
            try {
                it.generate()
            } catch (Exception e) {
                throw new IllegalStateException("Can't generate module $it", e)
            }
        }
        codegen.cfg.modules.collectEntries {
            def model = it.model
            assert model.name != null
            [(model.name): it.codesToGenerate]
        }
    }

    protected String generatedAnnotationClass() {
        new JavaParams().generatedAnnotationClass
    }

    protected List<ClassShadow> classesToCheckForCommons() {
        [new ClassShadow(
                name: 'commons.SerializationUtils',
                classHeader: 'public class SerializationUtils',
                noMoreMethods: true,
                annotations: ['@Generated("pl.metaprogramming.codegen")'],
                methods: [
                        JavaCodeGenerationTestUtils.methodShadow('SerializationUtils', 'private SerializationUtils()',
                                ['']),

                        JavaCodeGenerationTestUtils.methodShadow('toString', 'public static <T> String toString(T value, Function<T,String> transformer)',
                                ['return value == null ? null : transformer.apply(value);']),
                        JavaCodeGenerationTestUtils.methodShadow('fromString', 'public static <T> T fromString(String value, Function<String,T> transformer)',
                                ['return value == null || value.isEmpty() ? null : transformer.apply(value);']),
                        JavaCodeGenerationTestUtils.methodShadow('transformList', 'public static <R,T> List<R> transformList(List<T> value, Function<T,R> transformer)',
                                ['return value == null ? null : value.stream().map(transformer).collect(Collectors.toList());']),
                        JavaCodeGenerationTestUtils.methodShadow('transformMap', 'public static <K,R,T> Map<K,R> transformMap(Map<K,T> value, Function<T,R> transformer)',
                                ['return value == null ? null : value.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> transformer.apply(e.getValue())));']),

                        JavaCodeGenerationTestUtils.methodShadow('toBigDecimal', 'public static BigDecimal toBigDecimal(String value)',
                                ['return fromString(value, BigDecimal::new);']),
                        JavaCodeGenerationTestUtils.methodShadow('toString', 'public static String toString(BigDecimal value)',
                                ['return toString(value, Object::toString);']),

                        JavaCodeGenerationTestUtils.methodShadow('toLocalDateTime', 'public static LocalDateTime toLocalDateTime(String value)',
                                ['return fromString(value, v -> ZonedDateTime.parse(v, DateTimeFormatter.ISO_OFFSET_DATE_TIME).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());']),
                        JavaCodeGenerationTestUtils.methodShadow('toString', 'public static String toString(LocalDateTime value)',
                                ['return toString(value, v -> DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(value.atZone(ZoneOffset.UTC)));']),

                        JavaCodeGenerationTestUtils.methodShadow('toLocalDate', 'public static LocalDate toLocalDate(String value)',
                                ['return fromString(value, LocalDate::parse);']),
                        JavaCodeGenerationTestUtils.methodShadow('toString', 'public static String toString(LocalDate value)',
                                ['return toString(value, DateTimeFormatter.ISO_LOCAL_DATE::format);']),

                        JavaCodeGenerationTestUtils.methodShadow('toFloat', 'public static Float toFloat(String value)',
                                ['return fromString(value, Float::valueOf);']),
                        JavaCodeGenerationTestUtils.methodShadow('toString', 'public static String toString(Float value)',
                                ['return toString(value, Object::toString);']),

                        JavaCodeGenerationTestUtils.methodShadow('toInteger', 'public static Integer toInteger(String value)',
                                ['return fromString(value, Integer::valueOf);']),
                        JavaCodeGenerationTestUtils.methodShadow('toString', 'public static String toString(Integer value)',
                                ['return toString(value, Object::toString);']),

                        JavaCodeGenerationTestUtils.methodShadow('toLong', 'public static Long toLong(String value)',
                                ['return fromString(value, Long::valueOf);']),
                        JavaCodeGenerationTestUtils.methodShadow('toString', 'public static String toString(Long value)',
                                ['return toString(value, Object::toString);']),

                        JavaCodeGenerationTestUtils.methodShadow('toBoolean', 'public static Boolean toBoolean(String value)',
                                ['return fromString(value, Boolean::valueOf);']),
                        JavaCodeGenerationTestUtils.methodShadow('toString', 'public static String toString(Boolean value)',
                                ['return toString(value, Object::toString);']),

                        JavaCodeGenerationTestUtils.methodShadow('toDouble', 'public static Double toDouble(String value)',
                                ['return fromString(value, Double::valueOf);']),
                        JavaCodeGenerationTestUtils.methodShadow('toString', 'public static String toString(Double value)',
                                ['return toString(value, Object::toString);']),

                        JavaCodeGenerationTestUtils.methodShadow('toBytes', 'public static byte[] toBytes(String value)',
                                ['return value != null ? Base64.getDecoder().decode(value) : null;']),
                        JavaCodeGenerationTestUtils.methodShadow('toString', 'public static String toString(byte[] value)',
                                ['return value != null ? Base64.getEncoder().encodeToString(value) : null;']),

                        JavaCodeGenerationTestUtils.methodShadow('toBytes', 'public static byte[] toBytes(Resource value)',
                                ['return value != null ? IOUtils.toByteArray(value.getInputStream()) : null;'],
                                ['@SneakyThrows']
                        ),
                ],
                imports: [
                        'java.math.BigDecimal',
                        'java.time.LocalDate',
                        'java.time.LocalDateTime',
                        'java.time.ZoneOffset',
                        'java.time.ZonedDateTime',
                        'java.time.format.DateTimeFormatter',
                        'java.util.Base64',
                        'java.util.List',
                        'java.util.Map',
                        'java.util.function.Function',
                        'java.util.stream.Collectors',
                        'lombok.SneakyThrows',
                        'org.apache.commons.io.IOUtils',
                        'org.springframework.core.io.Resource',
                ],
        ),
         new ClassShadow(
                 name: 'commons.RestResponse',
                 classHeader: 'public interface RestResponse<R>',
                 annotations: [
                         '@Generated("pl.metaprogramming.codegen")',
                         '@ParametersAreNonnullByDefault'],
                 methods: [
                         JavaCodeGenerationTestUtils.methodShadow('getBody', 'Object getBody()'),
                         JavaCodeGenerationTestUtils.methodShadow('getStatus', 'Integer getStatus()'),
                         JavaCodeGenerationTestUtils.methodShadow('self', 'R self()'),
                         JavaCodeGenerationTestUtils.methodShadow('getDeclaredStatuses', 'Collection<Integer> getDeclaredStatuses()'),
                         JavaCodeGenerationTestUtils.methodShadow('getHeaders', 'Map<String,String> getHeaders()'),
                         JavaCodeGenerationTestUtils.methodShadow('setHeader', 'default R setHeader(String name, String value)', [
                                 'getHeaders().put(name, value);',
                                 'return self();',]),
                         JavaCodeGenerationTestUtils.methodShadow('isStatus', 'default boolean isStatus(Integer status)',
                                 ['return Objects.equals(status, getStatus());']),
                 ],
                 imports: [
                         'java.util.Collection',
                         'java.util.Map',
                         'java.util.Objects',
                         'javax.annotation.ParametersAreNonnullByDefault',
                 ],
         ),
         new ClassShadow(
                 name: 'commons.RestResponseBase',
                 classHeader: 'public abstract class RestResponseBase<R> implements RestResponse<R>',
                 annotations: [
                         '@Generated("pl.metaprogramming.codegen")',
                         '@RequiredArgsConstructor',
                 ],
                 fields: [
                         '@Getter private final Integer status',
                         '@Getter private final Map<String,String> headers = new TreeMap<>()',
                         '@Getter private final Object body',
                 ],
                 imports: [
                         'java.util.Map',
                         'java.util.TreeMap',
                         'lombok.Getter',
                         'lombok.RequiredArgsConstructor',
                 ],
         ),
         new ClassShadow(
                 name: 'commons.validator.ValidationResultMapper',
                 classHeader: 'public class ValidationResultMapper',
                 annotations: CheckUtils.COMPONENT_ANNOTATIONS_LEGACY,
                 fields: [],
                 methods: [
                         JavaCodeGenerationTestUtils.methodShadow('map', 'public ResponseEntity map(@Nonnull ValidationResult validationResult)',
                                 ['return ResponseEntity',
                                  '        .status(validationResult.getStatus())',
                                  '        .body(validationResult.getMessage());   // FIXME and remove \'@Generated\' annotation',
                                 ]),
                 ],
                 imports: [
                         'javax.annotation.Nonnull',
                         'org.springframework.http.ResponseEntity',
                         'org.springframework.stereotype.Component',
                 ],
         ),

        ]
    }

}
