/*
 * Copyright (c) 2019 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.oas.*

import java.util.function.Consumer

class RestMetaModels {

    static RestApi commonsRestServices = new RestApiBuilder() {
        {
            model.name = 'commons-api'
            dataDef('FieldValidation', 'Informacje o błędzie walidacji danych wejściowych', [
                    field    : DataSchemaFactory.stringType(true, 'Pole z niepoprawną wartością'),
                    errorCode: DataSchemaFactory.stringType(true, 'Kod błędu walidacji'),
            ])
            dataDef('BadRequest', 'Informacje o błędzie danych wejściowych', [
                    errors: DataSchemaFactory.listData(schemaRef('FieldValidation'), 1, 'Lista błędów walidacji'),
            ])
            dataDef('Error', 'Komunikat błędu', [
                    errorCode: DataSchemaFactory.stringType(true, 'Kod błędu'),
            ])

            paramDef('Authorization', ParamLocation.HEADER, DataSchemaFactory.STRING_TYPE, true, 'OAuth 2.0 bearer token')
        }
    }.model

    static RestApi echoRestServices = new RestApiBuilder([commonsRestServices]) {
        {
            model.name = 'echo-api'

            dataDef('DataForm', 'Formularz z danymi', [
                    d_r_string : DataSchemaFactory.stringType(true, 'Tekst - wymagany'),
                    d_o_string : DataSchemaFactory.stringType(false, 'Tekst - opcjonalny'),
                    d_int      : DataSchemaFactory.intType(true, 'Liczba całkowita - int'),
                    d_long     : DataSchemaFactory.longType(true, 'Liczba całkowita - long'),
                    d_list_long: DataSchemaFactory.listData(DataSchemaFactory.LONG_TYPE, 0, 'Lista liczb całkowitych'),
                    d_decimal  : DataSchemaFactory.dataElement(DataSchemaFactory.DECIMAL_TYPE, false, 'Liczba dziesiętna - decimal', ',|2'),
                    d_date     : DataSchemaFactory.dataElement(DataSchemaFactory.DATE_TYPE, false, 'Data yyyy-MM-dd'),
            ])

            dataDef('TreeLeaf', null, [
                    simpleField: DataSchemaFactory.stringType()
            ])

            dataDef('TreeRoot', null, [
                    complexField: schemaRef('TreeLeaf')
            ])

            addOperation('echoPost', '/api/v1/echo', HttpMethod.POST) {
                it.requestBody = requestBody('DataForm')
                it.responses.addAll([response(200, 'DataForm', 'Echo'),
                        response400(),
                        response404(),
                        response405()])
                it.additives.description = 'Usługa w odpowiedzi zwraca dane otrzymane na wejściu (POST)'
            }

            addOperation('echoGet', '/api/v1/echo', HttpMethod.GET) {
                it.additives.description = 'Usługa w odpowiedzi zwraca dane otrzymane na wejściu (URL)'
                it.parameters.addAll(params([['DataForm', ParamLocation.QUERY]]))
                it.responses.addAll([response(200, 'DataForm', 'Echo'), response400()])
            }

            addOperation('echoComplex', '/api/v1/echoComplex', HttpMethod.POST) {
                it.additives.description = 'Test obsługii strutkur zagnieżdżonych'
                it.requestBody = requestBody('TreeRoot')
                it.responses.addAll([response(200, 'TreeRoot', 'Echo complex'), response400()])
            }

        }

        void addOperation(String code, String path, HttpMethod method, Consumer<Operation> builder) {
            def operation = new Operation(model, 'Echo', code, path, method)
            builder.accept(operation)
            model.operations.add(operation)
        }
    }.model


    static class RestApiBuilder {

        RestApi model

        RestApiBuilder(List<RestApi> dependsOn = []) {
            model = new RestApi(dependsOn)
        }

        HttpRequestBody requestBody(String code) {
            new HttpRequestBody('requestBody', true, null, ['application/json': schemaRef(code)])
        }

        HttpResponse response400() {
            response(400, 'BadRequest', 'Odpowiedź na niepoprawne żądanie')
        }

        HttpResponse response404() {
            response(404, 'Error', 'Not Found')
        }

        HttpResponse response405() {
            response(405, 'Error', 'Method Not Allowed')
        }

        HttpResponse response(int code, String schemaCode, String description) {
            new HttpResponse(code, description, [] as Set<String>, ['application/json': schemaRef(schemaCode)] as Map<String, DataSchema>)
        }

        List<Parameter> params(List paramList) {
            List<Parameter> result = []
            paramList.each {
                if (it instanceof String) {
                    result.addAll(paramRef(it))
                } else if (it instanceof List) {
                    if (it.size() == 2) {
                        def schema = schemaRef((String) it.get(0))
                        def location = (ParamLocation) it.get(1)
                        schema.objectType.fields.each {
                            result.add(location.asParam(it))
                        }
                    } else if (it.size() == 5) {
//                        result.add(new Parameter((ParamLocation) it.get(1), new DataSchema((String) it.get(0), (DataType) it.get(2))
//                                .setIsRequired((Boolean) it.get(3))
//                                .setAdditive('description', it.get(4))))
                        result.add((it.get(1) as ParamLocation).asParam(it.get(0) as String, it.get(2) as DataType) { schema ->
                            schema.required = (Boolean) it.get(3)
                            schema.setAdditive('description', it.get(4))
                        })
                    } else {
                        throw new RuntimeException("Can't define param: $it")
                    }
                }
            }
            result
        }

        void dataDef(String code, String description, Map<String, DataSchema> properties) {
            dataDef(code, description, properties.collect {
                it.value.code = it.key
                it.value
            })
        }

        void dataDef(String code, String description, List<DataSchema> properties) {
            model.addSchema(new ObjectType(code: code, fields: properties, additives: [description: description]).asSchema())
        }

        void paramDef(String name, ParamLocation location, DataType schema, boolean isRequired, String description) {
            addParam(location.asParam(name, schema) {
                it.required = isRequired
                it.setAdditive('description', description)
            })
        }

        void addParam(Parameter param) {
            model.parameters.add(param)
        }

        DataSchema schemaRef(String code) {
            def result = model.getSchema(code)
            if (!result) {
                throw new IllegalStateException("Unknown schema $code")
            }
            result
        }


    }

}
