/*
 * Copyright (c) 2019 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.utils

import java.util.function.Function

class CheckUtils {

    static <T> boolean checkList(String label, T given, T expected, Function<T, List<String>> listProvider) {
        checkList(label, listProvider.apply(given), listProvider.apply(expected))
    }

    static boolean checkList(String label, Collection<String> given, List expected, boolean restrictive = true) {
        expected = expected ?: []
        given = given ?: [] as List<String>
        def notPresent = expected - given
        if (restrictive) {
            def notExpected = given - expected
            if (notPresent || notExpected) {
                println "Failes check list $label, given values: "
                printList(given.sort())
            }
            assert !notPresent && !notExpected, "\nMissmatch $label:\n" +
                    "not present : $notPresent\n" +
                    "not expected: $notExpected\n"
            assert expected.size() == given.size(), "\nMissmatch $label. Collections have different sizes but contain same items.\n" +
                    "given: ${given}\n" +
                    "expected: ${expected}"
        } else {
            if (notPresent) {
                println "Failes check list $label, given values: "
                printList(given.sort())
            }
            assert !notPresent, "\nMissmatch $label:\n" +
                    "not present : $notPresent\n"
        }
        true
    }

    static String filepath2ClassName(String filepath) {
        final String srcDir = 'src/main/java/'
        if (filepath.contains(srcDir)) {
            filepath.substring(filepath.indexOf(srcDir) + srcDir.length(), filepath.length() - 5).replace('/', '.')
        } else {
            filepath
        }
    }

    static def GEN_ANNOTATIONS = ['@Generated("pl.metaprogramming.codegen")']

    static def DTO_ANNOTATIONS = GEN_ANNOTATIONS + [
            '@Data',
            '@NoArgsConstructor',
            '@Accessors(chain=true)']

    static def DTO_IMPORTS = [
            'lombok.Data',
            'lombok.NoArgsConstructor',
            'lombok.experimental.Accessors']

    static def NONNULL_IMPORTS = ['javax.annotation.Nonnull']
    static def NULLABLE_IMPORTS = ['javax.annotation.Nullable']

    static def NONNULL_NULLABLE_IMPORTS = NONNULL_IMPORTS + NULLABLE_IMPORTS

    static def RAW_DTO_ANNOTATIONS = DTO_ANNOTATIONS + [
            '@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.NONE)']

    static def RAW_DTO_IMPORTS = DTO_IMPORTS + [
            'com.fasterxml.jackson.annotation.JsonAutoDetect',
            'com.fasterxml.jackson.annotation.JsonProperty'
    ]

    static def REST_CONTROLLER_ANNOTATIONS = GEN_ANNOTATIONS + [
            '@RestController'
    ]

    static def COMPONENT_ANNOTATIONS_LEGACY = GEN_ANNOTATIONS + [
            '@Component'
    ]

    static def COMPONENT_ANNOTATIONS = GEN_ANNOTATIONS + [
            '@Component',
            '@RequiredArgsConstructor'
    ]

    static def COMPONENT_WITH_LOG_ANNOTATIONS = GEN_ANNOTATIONS + [
            '@Slf4j',
            '@Component',
            '@RequiredArgsConstructor'
    ]

    static void printList(List<String> list) {
        list.each {
            println "'${it.replace('\\', '\\\\').replace('\'', '\\\'')}',"
        }
    }
}
