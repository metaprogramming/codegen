swagger: '2.0'
info:
  title: Example API
  version: '1.0'
host: "localhost:8080"
basePath: "/api/v1"
schemes:
  - "http"
consumes:
  - "application/json"
produces:
  - "application/json"
parameters:
  authorizationParam:
    name: Authorization
    in: header
    required: true
    type: string
    description: "The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'."
  correlationIdParam:
    name: X-Correlation-ID
    in: header
    required: true
    type: string
    description: "Correlates HTTP requests between a client and server. "

paths:
  /echo:
    post:
      tags:
        - echo
      operationId: echoPost
      parameters:
        - in: body
          name: requestBody
          description: body param
          required: true
          schema:
            $ref: '#/definitions/EchoBody'
        - $ref: "#/parameters/authorizationParam"
        - $ref: "#/parameters/correlationIdParam"
        - $ref: "example-api-deps.yaml#/parameters/timestampParam"
        - in: header
          name: Inline-Header-Param
          type: string
          description: Example header param
      x-validation-beans:
        - class: example.adapters.in.rest.validators.UserDataValidationBean
          factory: example.adapters.in.rest.validators.ValidationBeanFactory:createUserDataValidationBean
      responses:
        '200':
          description: 'request body'
          schema:
            $ref: '#/definitions/EchoBody'
          headers:
            X-Correlation-ID:
              type: string
        default:
          description: 'Error'
          schema:
            $ref: 'example-api-deps.yaml#/definitions/ErrorDescription'
      security:
        - oauth: ['write']
    get:
      tags:
        - echo
      operationId: echoGet
      parameters:
        - $ref: '#/parameters/correlationIdParam'
        - name: prop_int_required
          in: query
          required: true
          type: integer
          format: int64
          minimum: 5
      responses:
        '200':
          description: 'request body'
          schema:
            $ref: '#/definitions/EchoGetBody'
          headers:
            X-Correlation-ID:
              type: string
        '400':
          description: 'Error'
          schema:
            type: array
            items:
              $ref: 'example-api-deps.yaml#/definitions/ErrorItem'
      security:
        - oauth: ['write', 'read']
        - api_key: ['write', 'read']

  /echo-defaults:
    post:
      tags:
        - echo
      operationId: echoDefaultsPost
      parameters:
        - in: body
          name: requestBody
          description: body param with default values
          required: false
          schema:
            $ref: '#/definitions/EchoDefaultsBody'
        - name: Default-Header-Param
          in: header
          description: Example header param with default value
          type: string
          default: a2
          enum: [ a1, a2 ]
          x-ms-enum:
            name: DefaultEnum
      responses:
        '200':
          description: 'request body'
          schema:
            $ref: '#/definitions/EchoDefaultsBody'
          headers:
            Default-Header-Param:
              type: string
              default: a2
              enum: [ a1, a2 ]
              x-ms-enum:
                name: DefaultEnum
        default:
          description: 'Error'
          schema:
            $ref: 'example-api-deps.yaml#/definitions/ErrorDescription'

  /echo-arrays:
    post:
      tags:
        - echo
      operationId: echoArraysPost
      parameters:
        - in: body
          name: requestBody
          description: body param
          required: true
          schema:
            type: array
            items:
              $ref: '#/definitions/EchoArraysBody'
        - $ref: "#/parameters/authorizationParam"
        - in: header
          name: Inline-Header-Param
          type: string
          description: Example header param
      responses:
        200:
          description: 'request body'
          schema:
            type: array
            items:
              $ref: '#/definitions/EchoArraysBody'
        204:
          description: No input no output
        400:
          description: 'Error'
          schema:
            $ref: 'example-api-deps.yaml#/definitions/ErrorDescription'

  /echo-date-array:
    get:
      tags:
        - echo
      operationId: echoDateArrayGet
      parameters:
        - in: query
          name: date_array
          type: array
          items:
            type: string
            format: date
      responses:
        '200':
          description: 'request body'
          schema:
            type: array
            items:
              type: string
              format: date
        default:
          description: 'Error'
          schema:
            $ref: 'example-api-deps.yaml#/definitions/ErrorDescription'

  /echo-file/{id}/form:
    post:
      summary: upload file using multipart/form-data
      operationId: uploadEchoFileWithForm
      consumes:
        - 'multipart/form-data'
      produces:
        - 'application/json'
      parameters:
        - in: path
          name: id
          description: 'id of file'
          required: true
          type: integer
          format: int64
        - in: formData
          name: file
          description: 'file to upload'
          required: true
          type: file
      responses:
        '204':
          description: successful operation
        default:
          description: 'error'
          schema:
            $ref: 'example-api-deps.yaml#/definitions/ErrorDescription'

  /echo-file/{id}:
    post:
      description: 'upload file - not supported by OAS2'
      operationId: uploadEchoFile
      consumes:
        - 'application/octet-stream'
      produces:
        - 'application/json'
      parameters:
        - in: path
          name: id
          description: 'id of file'
          required: true
          type: integer
          format: int64
        - in: body
          name: requestBody
          description: file data to upload
          required: true
          schema:
            type: file
      responses:
        '204':
          description: successful operation
        default:
          description: 'error'
          schema:
            $ref: 'example-api-deps.yaml#/definitions/ErrorDescription'
    get:
      description: 'Returns a file'
      operationId: downloadEchoFile
      produces:
        - 'image/jpeg'
        - 'application/json'
      parameters:
        - in: path
          name: id
          description: 'id of file'
          required: true
          type: integer
          format: int64
      responses:
        '200':
          description: 'successful operation'
          schema:
            type: file
          headers:
            Content-Disposition:
              type: string
              description: 'attachment; filename=cover.jpg'
        '404':
          description: 'File not uploaded yet'
        default:
          description: 'error'
          schema:
            $ref: 'example-api-deps.yaml#/definitions/ErrorDescription'
    delete:
      description: 'deletes a file based on the id supplied'
      operationId: deleteFile
      parameters:
        - in: path
          name: id
          description: 'id of book to delete'
          required: true
          type: integer
          format: int64
      responses:
        '204':
          description: 'file deleted'
        default:
          description: 'error'
          schema:
            $ref: 'example-api-deps.yaml#/definitions/ErrorDescription'
  /echo-empty:
    get:
      tags:
        - echo
      operationId: echoEmpty
      responses:
        '200':
          description: No input, no output
  /echo-error:
    get:
      tags:
        - echo
      operationId: echoError
      parameters:
        - in: query
          name: errorMessage
          type: string
          required: true
          description: Example header param
      responses:
        default:
          description: 'Error'
          schema:
            $ref: 'example-api-deps.yaml#/definitions/ErrorDescription'
  /skipped:
    post:
      tags:
        - Skipped Operation
      operationId: skipped
      parameters:
        - in: body
          name: requestBody
          description: body param
          required: true
          schema:
            $ref: '#/definitions/ObjectReferredOnlyBySkippedOperation'
      responses:
        '200':
          description: Success
          schema:
            $ref: '#/definitions/ObjectReferredOnlyBySkippedOperation'

definitions:
  UnusedObject:
    properties:
      some_property:
        type: string
  ObjectReferredOnlyBySkippedOperation:
    properties:
      some_property:
        type: string
  ExtendedObject:
    title: Extended object
    description: Simple object for testing
    type: object
    allOf:
      - $ref: 'example-api-deps.yaml#/definitions/SimpleObject'
      - properties:
          eo_enum_reusable:
            $ref: 'example-api-deps.yaml#/definitions/ReusableEnum'
            description: enum property with external definition
            x-validations:
              - bean:example.adapters.in.rest.validators.ExtendedObjectEnumChecker
          self_property:
            $ref: '#/definitions/ExtendedObject'
    x-validations:
      - di-bean:example.adapters.in.rest.validators.ExtendedObjectChecker

  EchoBody:
    title: Echo object
    description: Test object
    type: object
    required:
      - prop_int_required
    x-validation-beans:
      - class: example.adapters.in.rest.validators.ExtendedObjectEnumChecker
        factory: example.adapters.in.rest.validators.ValidationBeanFactory:createExtendedObjectEnumChecker
    x-validations:
      - compare:prop_int < prop_int_second
      - compare:prop_date >= prop_date_second
    properties:
      prop_int:
        type: integer
        format: int32
      prop_int_second:
        type: integer
        format: int32
      prop_int_required:
        type: integer
        format: int64
        minimum: -1
      prop_float:
        type: number
        format: float
        maximum: 101
      prop_double:
        maximum: 10000
        exclusiveMaximum: true
        minimum: 0
        exclusiveMinimum: true
        type: number
        format: double
      prop_amount:
        type: string
        pattern: '^(0|([1-9][0-9]{0,}))\.\d{2}$'
        x-validations:
          - bean:example.adapters.in.rest.validators.UserDataValidationBean:checkAmountByUser
          - field:example.commons.validator.Checkers.AMOUNT_SCALE_CHECKER
      prop_amount_number:
        type: number
        format: 'amount'
        minimum: 0.02
        maximum: 9999999999.99
      prop_string:
        type: string
        minLength: 5
        maxLength: 10
      prop_string_pattern:
        type: string
        pattern: ^[\-\".',:;/\\!@#$%^&*()+_?|><=]{2,19}$
      prop_default:
        type: string
        default: 'value'
        minLength: 5
      prop_date:
        type: string
        format: date
      prop_date_second:
        type: string
        format: date
      prop_date_time:
        type: string
        format: date-time
      prop_base64:
        type: string
        format: byte
      prop_boolean:
        type: boolean
      prop_object:
        $ref: 'example-api-deps.yaml#/definitions/SimpleObject'
        description: object property
        x-validations:
          - di-bean:example.commons.adapters.in.rest.validators.SimpleObjectCustomValidator
      prop_object_any:
        type: object
      prop_object_extended:
        $ref: '#/definitions/ExtendedObject'
      prop_enum_reusable:
        $ref: 'example-api-deps.yaml#/definitions/ReusableEnum'
        description: enum property with external definition
        default: a
      prop_enum:
        type: string
        description: enum property
        enum:
          - A
          - b
          - 1
        x-ms-enum:
          name: enumType
          values:
            - value: A
              description: A value
            - value: b
              description: b value
            - value: 1
              description: 1 value

  EchoArraysBody:
    title: Echo object with arrays properties
    description: Test object containing arrays properties
    type: object
    required:
      - prop_double_list
    properties:
      prop_int_list:
        type: array
        minItems: 1
        items:
          type: integer
          format: int32
          minimum: -2
          exclusiveMinimum: true
      prop_float_list:
        type: array
        items:
          type: number
          format: float
          maximum: 101
      prop_double_list:
        type: array
        minItems: 2
        maxItems: 4
        uniqueItems: true
        items:
          type: number
          format: double
          minimum: 0.01
          maximum: 1000000000000000
      prop_amount_list:
        type: array
        items:
          type: string
          pattern: '^(0|([1-9][0-9]{0,}))\.\d{2}$'
          x-constraints:
            - invalid-pattern-code: invalid_amount
      prop_string_list:
        type: array
        items:
          type: string
          minLength: 5
          maxLength: 10
      prop_date_list:
        type: array
        items:
          type: string
          format: date
      prop_date_time_list_of_list:
        type: array
        minItems: 1
        items:
          type: array
          minItems: 2
          items:
            type: string
            format: date-time
      prop_boolean_list:
        type: array
        items:
          type: boolean
      prop_object_list:
        type: array
        items:
          $ref: 'example-api-deps.yaml#/definitions/SimpleObject'
      prop_object_list_of_list:
        type: array
        items:
          type: array
          items:
            $ref: 'example-api-deps.yaml#/definitions/SimpleObject'
            x-validations:
              - di-bean:example.commons.adapters.in.rest.validators.SimpleObjectCustomValidator:check
      prop_enum_reusable_list:
        description: enum property with external definition
        type: array
        items:
          $ref: 'example-api-deps.yaml#/definitions/ReusableEnum'
      prop_enum_list:
        type: array
        items:
          type: string
          description: enum property
          enum:
            - A
            - b
            - 1
          x-ms-enum:
            name: enumType
            values:
              - value: A
                description: A value
              - value: b
                description: b value
              - value: 1
                description: 1 value
      prop_map_of_int:
        description: associative array with integer value
        type: object
        additionalProperties:
          type: integer
          maximum: 100
          format: int32
      prop_map_of_object:
        description: associative array with object as value
        type: object
        additionalProperties:
          $ref: 'example-api-deps.yaml#/definitions/SimpleObject'
      prop_map_of_list_of_object:
        description: associative array with list of objects as value
        type: object
        additionalProperties:
          type: array
          items:
            $ref: 'example-api-deps.yaml#/definitions/SimpleObject'
  EchoDefaultsBody:
    title: Echo object with default values
    type: object
    properties:
      propInt:
        type: integer
        format: int32
        default: 101
        minimum: 1
      propFloat:
        type: number
        format: float
        default: 101.101
        minimum: 1
      propDouble:
        type: number
        format: double
        default: 202.202
        minimum: 1
      propNumber:
        type: number
        format: amount
        default: 3.33
        minimum: 1
      propString:
        minLength: 5
        type: string
        default: WHITE
        x-constraints:
          - dictionary: COLORS
      propEnum:
        $ref: 'example-api-deps.yaml#/definitions/ReusableEnum'
        default: 3
      propDate:
        type: string
        format: date
        default: 2021-09-01
      propDateTime:
        type: string
        format: date-time
        default: 2021-09-01T09:09:09Z
  EchoGetBody:
    properties:
      prop_int_required:
        type: integer
        format: int64
      prop_float:
        type: number
        format: float
securityDefinitions:
  oauth:
    type: oauth2
    flow: implicit
    authorizationUrl: https://localhost/authorize
    scopes:
      write: write rights
      read: read rights
  api_key:
    type: apiKey
    name: api_key
    in: header
