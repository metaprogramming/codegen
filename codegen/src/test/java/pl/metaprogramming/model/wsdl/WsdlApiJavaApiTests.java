/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.metaprogramming.model.data.DataType;

class WsdlApiJavaApiTests {

    @Test
    void parseApi() {
        WsdlApi api = WsdlApi.of("src/test/resources/wsdl/countries.wsdl");
        Assertions.assertEquals("CountriesPortService", api.getName());
        Assertions.assertTrue(api.getOperations().stream().anyMatch(it -> "getCountry".equals(it.getName())));
        Assertions.assertEquals(DataType.INT32, getCountryPopulationType(api));
    }

    @Test
    void parseApiWithCustomization() {
        WsdlApi api = WsdlApi.of("src/test/resources/wsdl/countries.wsdl", cfg -> {
            cfg.setServiceNameMapper(name -> name.replace("PortService", "Service"));
            cfg.getXsTypeParser().set("int", DataType.INT64);
        });
        Assertions.assertEquals("CountriesService", api.getName());
        Assertions.assertTrue(api.getOperations().stream().anyMatch(it -> "getCountry".equals(it.getName())));
        Assertions.assertEquals(DataType.INT64, getCountryPopulationType(api));
    }

    private DataType getCountryPopulationType(WsdlApi api) {
        return api.getSchemas().get("{http://spring.io/guides/gs-producing-web-service}country").getObjectType()
                .field("population").getDataType();
    }

}
