/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen;

import org.junit.jupiter.api.Test;
import pl.metaprogramming.codegen.java.*;
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate;
import pl.metaprogramming.codegen.java.base.IClassCmBuilder;
import pl.metaprogramming.codegen.java.builders.AnnotatedBuilder;
import pl.metaprogramming.model.data.DataType;

import static org.junit.jupiter.api.Assertions.*;

class CodegenParamsJavaApiTests {

    @Test
    void shouldSetJavaModuleParams() {
        Model.empty("");
        // BEGIN JAVA CODE EXAMPLE - SetJavaModuleParams
        CodeDecorator codeDecorator = (p) -> "// code header" + "\n" + p;
        String licenceHeader = "Licence header";
        JavaNameMapper nameMapper = new DefaultJavaNameMapper();
        IClassCmBuilder<?> componentStrategy = AnnotatedBuilder.by("org.springframework.stereotype.Component");
        ClassCmBuilderTemplate<?> diStrategy = new ClassCmBuilderTemplate<Object>() {
            private final AnnotationCm autowired = AnnotationCm.of("org.springframework.beans.factory.annotation.Autowired");
            @Override
            public void makeDecoration() {
                getFields().forEach(f -> {
                    if (!f.isStatic()) f.addAnnotation(autowired);
                });
            }
        };
        CodegenParams p = new CodegenParams()
                .set(new JavaParams()
                        .generatedAnnotationClass("javax.annotation.Generated")
                        .generatedAnnotationValue("my-generator")
                        .nameMapper(nameMapper)
                        .addCodeDecorator(codeDecorator)
                        .licenceHeader(licenceHeader)
                        .dependencyInjectionBuilder(diStrategy)
                        .managedComponentBuilder(componentStrategy)
                        .isAlwaysGenerateEnumFromValueMethod(true)
                );
        p.get(DataTypeMapper.class).set(DataType.DATE, "java.util.Date");
        // END JAVA CODE EXAMPLE - SetJavaModuleParams

        JavaParams jp = p.get(JavaParams.class);
        assertEquals("javax.annotation.Generated", jp.getGeneratedAnnotation().getAnnotationClass().getCanonicalName());
        assertEquals("\"my-generator\"", jp.getGeneratedAnnotation().getParams().get("value").toString());
        assertEquals(nameMapper, jp.getNameMapper());
        assertEquals(2, jp.getCodeDecorators().size());
        assertEquals(codeDecorator, jp.getCodeDecorators().get(0));
        assertInstanceOf(LicenceDecorator.class, jp.getCodeDecorators().get(1));
        assertEquals(diStrategy, jp.getDependencyInjectionBuilder());
        assertEquals(componentStrategy, jp.getManagedComponentBuilder());
        assertTrue(jp.isAlwaysGenerateEnumFromValueMethod());
        assertEquals("java.util.Date", "" + p.get(DataTypeMapper.class).get(DataType.DATE));
    }

    @Test
    void shouldUpdateJavaModuleParams() {
        CodegenParams codegenParams = new CodegenParams();
        assertFalse(codegenParams.get(JavaParams.class).isAlwaysGenerateEnumFromValueMethod());
        // BEGIN JAVA CODE EXAMPLE - UpdateJavaModuleParams v1
        codegenParams.set(JavaParams.class, jp -> jp
                .isAlwaysGenerateEnumFromValueMethod(true)
        );
        codegenParams.get(DataTypeMapper.class).set(DataType.DATE, "java.util.Date");
        // END JAVA CODE EXAMPLE - UpdateJavaModuleParams v1
        assertTrue(codegenParams.get(JavaParams.class).isAlwaysGenerateEnumFromValueMethod());
        assertEquals("java.util.Date", "" + codegenParams.get(DataTypeMapper.class).get(DataType.DATE));
    }

    @Test
    void shouldUpdateJavaModuleParamsV2() {
        CodegenParams codegenParams = new CodegenParams();
        assertFalse(codegenParams.get(JavaParams.class).isAlwaysGenerateEnumFromValueMethod());
        // BEGIN JAVA CODE EXAMPLE - UpdateJavaModuleParams v2
        codegenParams.get(JavaParams.class).setAlwaysGenerateEnumFromValueMethod(true);
        codegenParams.get(DataTypeMapper.class).set(DataType.DATE, "java.util.Date");
        // END JAVA CODE EXAMPLE - UpdateJavaModuleParams v1
        assertTrue(codegenParams.get(JavaParams.class).isAlwaysGenerateEnumFromValueMethod());
        assertEquals("java.util.Date", "" + codegenParams.get(DataTypeMapper.class).get(DataType.DATE));
    }
}
