/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen;

import org.jboss.forge.roaster.Roaster;
import org.junit.jupiter.api.Test;
import pl.metaprogramming.codegen.java.DefaultJavaNameMapper;
import pl.metaprogramming.codegen.java.JavaParams;
import pl.metaprogramming.codegen.java.base.MappingExpressionBuilder;
import pl.metaprogramming.codegen.java.libs.Java;
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator;
import pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator;
import pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator;
import pl.metaprogramming.codegen.java.spring.SpringSwaggerUiGenerator;
import pl.metaprogramming.codegen.java.validation.ValidationParams;
import pl.metaprogramming.model.data.DataType;
import pl.metaprogramming.model.data.constraint.CheckerRef;
import pl.metaprogramming.model.data.constraint.Constraints;
import pl.metaprogramming.model.oas.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class JavaSnippetsTests {

    @Test
    void testCodegen() throws IOException {
        File projectRoot = new File("out/gen-test");
        File indexFile = new File("out/gen-test", "generated-files.yaml");
        deleteDir(projectRoot);
        String openapiAppBody = getResource("/openapi/example-api.yaml");
        String openapiExternalAppBody = getResource("/openapi/petstore.yaml");
        String openapiCommonsBody = getResource("/openapi/example-api-deps.yaml");

        // CODE_SNIPPET: CODEGEN_SAMPLE
        RestApi apiCommons = RestApi.of(openapiCommonsBody);
        RestApi api = RestApi.of(openapiAppBody, apiCommons);
        RestApi externalApi = RestApi.of(openapiExternalAppBody, apiCommons);

        new Codegen()
                .baseDir(projectRoot)
                .lineSeparator("\r\n")
                .charset("UTF-8")
                .indexFile(indexFile)
                .forceMode(false)
                .addLastGenerationTag(false)
                .addLastUpdateTag(false)

                .dataTypeMapper(dtp -> dtp.set(DataType.NUMBER, "java.lang.Double"))

                .params(p -> p.set(JavaParams.class, jp -> {
                    jp.setAlwaysGenerateEnumFromValueMethod(true);
                    jp.setGeneratedAnnotationValue("my_generation_tag");
                }))

                .generate(new SpringCommonsGenerator(), m -> {
                    m.setRootPackage("com.example.commons");
                    m.typeOfCode(SpringCommonsGenerator.TOC.REST_EXCEPTION_HANDLER, cb -> {
                        cb.projectSubDir("src/main/java-gen");
                    });
                })
                .generate(new SpringRestServiceGenerator(), m -> {
                    m.model = apiCommons;
                    m.rootPackage("com.example.commons");
                })
                .generate(new SpringRestServiceGenerator(), m -> {
                    m.model = api;
                    m.rootPackage("com.example");
                    m.setAdapterPackageName("rest");
                })
                .generate(new SpringRestClientGenerator(), m -> {
                    m.model = externalApi;
                    m.rootPackage("com.example");
                    m.setAdapterPackageName("extsysname");
                    m.params.set(JavaParams.class, jp -> jp.setGeneratedAnnotationValue("my_client_generation_tag"));
                    m.dataTypeMapper.set(DataType.NUMBER, "java.lang.Float");
                    m.typeOfCode(SpringRestClientGenerator.TOC.CLIENT, cb -> {
                        cb.onDecoration(template -> {
                            template.getMethods().forEach(methodCm -> {
                                methodCm.setImplBody("// comment\n" + methodCm.getImplBody());
                            });
                        });
                    });
                })
                .generate(new SpringSwaggerUiGenerator(), m -> {
                    m.api(api, "api.yaml");
                    m.api(apiCommons, "commons.yaml");
                })
                .run();
        // CODE_SNIPPET: CODEGEN_SAMPLE

        assertTrue(projectRoot.exists());
        deleteDir(projectRoot);
    }

    @Test
    void paramsSnippets() {
        Codegen codegen = new Codegen();
        // CODE_SNIPPET: CODEGEN_FORCE_MODE
        codegen.setForceMode(false);
        // CODE_SNIPPET: CODEGEN_FORCE_MODE

        // CODE_SNIPPET: CODEGEN_PARAMS
        new CodegenParams()
                .set(new JavaParams()
                        .licenceHeader("My licence header")
                        .generatedAnnotationClass("javax.annotation.Generated")
                        .generatedAnnotationValue("my_generator")
                        .isAlwaysGenerateEnumFromValueMethod(true)
                )
                .set(ValidationParams.class, p -> {
                    p.setThrowExceptionIfValidationFailed(false);
                    p.setRequiredErrorCode("validation.error.required");
                    p.useNamedBeanAsRuleValidator(false);
                    p.ruleValidator("USER_ACCOUNT_BALANCE", "com.example.UserAccountBalanceValidator");
                    p.formatValidator("amount", "com.example.AmountValidator");
                })
        ;
        // CODE_SNIPPET: CODEGEN_PARAMS

        // CODE_SNIPPET: VALIDATION_ERROR_CODE
        new CodegenParams().set(ValidationParams.class, vp ->
                vp.setRequiredErrorCode("validation.error.required")
        );
        // CODE_SNIPPET: VALIDATION_ERROR_CODE

        // CODE_SNIPPET: VALIDATION_PARAMS_FORMAT
        new CodegenParams().set(ValidationParams.class, vp ->
                vp.formatValidator("amount", "com.example.AmountValidator"));
        // CODE_SNIPPET: VALIDATION_PARAMS_FORMAT

        // CODE_SNIPPET: VALIDATION_PARAMS_RULE
        new CodegenParams().set(ValidationParams.class, vp ->
                vp.ruleValidator("USER_ACCOUNT_BALANCE", "com.example.UserAccountBalanceValidator")
        );
        // CODE_SNIPPET: VALIDATION_PARAMS_RULE

        // CODE_SNIPPET: VALIDATION_STOP
        new CodegenParams().set(ValidationParams.class, vp ->
                vp.setStopAfterFirstError(true)
        );
        // CODE_SNIPPET: VALIDATION_STOP
    }

    @Test
    void generatorsSnippets() {
        new Codegen()
                // CODE_SNIPPET: CODE_INDEX_MAPPER
                .generate(new SpringRestServiceGenerator(), gb -> {
                    gb.model = getRestApi();
                    gb.typeOfCode(SpringRestServiceGenerator.TOC.OPERATION_EXECUTOR_IMPL, cbc -> cbc
                            .onDecoration(cb -> cb.getMethods().add("execute", mb -> {
                                mb.registerAsMapper();
                                mb.setResultType(cb.getClass(SpringRestServiceGenerator.TOC.DTO, cb.getModel().getSuccessResponseSchema()));
                                mb.getParams().add("request", cb.getClass(SpringRestServiceGenerator.TOC.REQUEST_DTO), p ->
                                        p.addAnnotation(Java.nonnul()));
                                mb.setImplBody("// TODO");
                            })));
                    gb.typeOfCode(SpringRestServiceGenerator.TOC.REST_CONTROLLER, cbc -> cbc
                            .onImplementation(cb -> cb.getMethods().add(cb.getNameMapper().toMethodName(cb.getModel().getCode()), mb -> {
                                mb.setResultType(cb.getClass(SpringRestServiceGenerator.TOC.DTO, cb.getModel().getSuccessResponseSchema()));
                                mb.getParams().add("request", cb.getClass(SpringRestServiceGenerator.TOC.REQUEST_DTO));
                                mb.setImplBody("return " + new MappingExpressionBuilder(cb.getContext(), cb.getModel(), mb.getImplDependencies())
                                        .to(cb.getClass(SpringRestServiceGenerator.TOC.DTO, cb.getModel().getSuccessResponseSchema()))
                                        .from(SpringRestServiceGenerator.TOC.REQUEST_DTO, "request")
                                        + ";"
                                );
                            })));
                })
                // CODE_SNIPPET: CODE_INDEX_MAPPER

                // CODE_SNIPPET: VALIDATION_TOC_CFG
                .generate(new SpringCommonsGenerator(), gb ->
                        gb.typeOfCode(
                                SpringCommonsGenerator.TOC.VALIDATION_DICTIONARY_CHECKER,
                                SpringCommonsGenerator.TOC.VALIDATION_DICTIONARY_CODE_ENUM
                        ).forEach(it -> it.getPackageName().setRoot("example.adapters.in.rest.validators.custom")))
                // CODE_SNIPPET: VALIDATION_TOC_CFG

                // CODE_SNIPPET: TARGET_DIRECTORY
                .baseDir(new File("some/project/directory"))
                .generate(new SpringCommonsGenerator(), gb -> {
                    gb.setProjectDir("commons");
                    gb.setProjectSubDir("src/main/java-gen");
                    gb.setRootPackage("example.commons");
                    gb.typeOfCode(
                            SpringCommonsGenerator.TOC.VALIDATION_DICTIONARY_CHECKER,
                            SpringCommonsGenerator.TOC.VALIDATION_DICTIONARY_CODE_ENUM
                    ).forEach(it -> it.getPackageName().setRoot("example.adapters.in.rest.validators.custom"));
                })
                // CODE_SNIPPET: TARGET_DIRECTORY

                // CODE_SNIPPET: GENERATOR_MODIFICATION
                .generate(new SpringCommonsGenerator(), gb -> gb
                        .typeOfCode(SpringCommonsGenerator.TOC.REST_EXCEPTION_HANDLER, cbc ->
                                cbc.onDecoration(cb -> {
                                    cb.getFields().add("messageSource", "org.springframework.context.MessageSource", fs -> fs.setFinal(true));
                                    cb.getMethods().add("resolveMessage", ms -> {
                                        ms.setPrivate(true);
                                        ms.setResultType(Java.string());
                                        ms.getParams().add("code", Java.string());
                                        ms.getParams().add("args", Java.objectType().asArray());
                                        ms.getParams().add("locale", "java.util.Locale");
                                        ms.setImplBody("try {\n" +
                                                "    return messageSource.getMessage(code, args, locale);\n" +
                                                "} catch (NoSuchMessageException e) {\n" +
                                                "    log.info(\"No message for code: {}\", code);\n" +
                                                "    return \"INVALID_INPUT\";\n" +
                                                "}");
                                    });
                                    cb.getMethods().set("toResponseEntity(result, request)", mb -> mb
                                            .setImplBody("return new ResponseEntity<>(resolveMessage(\n" +
                                                    "       result.getErrors().get(0).getCode(),\n" +
                                                    "       result.getErrors().get(0).getMessageArgs(),\n" +
                                                    "       request.getLocale());"));
                                })
                        ))
                // CODE_SNIPPET: GENERATOR_MODIFICATION

                // CODE_SNIPPET: CLASS_NAMING
                .params(p -> p.set(JavaParams.class, jp -> jp.setNameMapper(new DefaultJavaNameMapper())))
                .generate(new SpringRestServiceGenerator(), gb -> {
                    gb.setModel(getRestApi());
                    gb.typeOfCode(SpringRestServiceGenerator.TOC.OPERATION_EXECUTOR_IMPL, cbc ->
                            cbc.getClassName().resolver(op ->
                                    op.getMethod() == HttpMethod.POST || op.getMethod() == HttpMethod.PUT ? "Command" : "Query"));
                    gb.typeOfCode(SpringRestServiceGenerator.TOC.DTO, cbc ->
                            cbc.getClassName().setSuffix("DTO"));
                })
                // CODE_SNIPPET: CLASS_NAMING

                // CODE_SNIPPET: LICENCE_HEADER
                .params(p -> p.get(JavaParams.class).setLicenceHeader("My licence header"))
                // CODE_SNIPPET: LICENCE_HEADER

                // CODE_SNIPPET: CODE_FORMATTER
                .params(p -> p.get(JavaParams.class).getCodeDecorators().add(new CodeDecorator() {
                    @Override
                    public String apply(String text) {
                        return Roaster.format(text);
                    }
                }))
                // CODE_SNIPPET: CODE_FORMATTER

                // CODE_SNIPPET: DRY_RUN
                .generate(new SpringCommonsGenerator(), gb ->
                        gb.params.setDryRun(true)
                )
                // CODE_SNIPPET: DRY_RUN

                // CODE_SNIPPET: ALWAYS_GENERATE_ENUM_FROM_VALUE_METHOD
                .params(p -> p.get(JavaParams.class).setAlwaysGenerateEnumFromValueMethod(true))
                // CODE_SNIPPET: ALWAYS_GENERATE_ENUM_FROM_VALUE_METHOD
        ;
    }

    @Test
    void restApiSnippets() {
        String apiText = getResource("/openapi/petstore.yaml");
        // CODE_SNIPPET: REST_API_CREATE
        RestApi restApi = RestApi.of(apiText);
        // CODE_SNIPPET: REST_API_CREATE

        // CODE_SNIPPET: REST_API_OPERATIONS_REMOVE
        restApi.getOperations().removeIf(it -> it.getCode().equals("operationCode"));
        // CODE_SNIPPET: REST_API_OPERATIONS_REMOVE

        // CODE_SNIPPET: REST_API_OPERATIONS_GROUPING
        restApi.getOperations().forEach(it -> it.setGroup("group"));
        // CODE_SNIPPET: REST_API_OPERATIONS_GROUPING

        // CODE_SNIPPET: REST_API_ADD_PARAMETER
        Parameter newParam = new Parameter("x-my-header", ParamLocation.COOKIE, DataType.TEXT);
        restApi.getParameters().add(newParam);
        restApi.getOperations().forEach(o -> o.getParameters().add(newParam));
        // CODE_SNIPPET: REST_API_ADD_PARAMETER

        // CODE_SNIPPET: REST_API_SAVE
        OpenapiWriter.write(restApi, new File("out/api.yaml"));
        // CODE_SNIPPET: REST_API_SAVE

        // CODE_SNIPPET: REST_API_INJECT_CONSTRAINT
        restApi.getSchema("Address").addConstraint(CheckerRef.of("com.validators.AdvancedAddressValidator"));
        restApi.getSchema("Address.state").addConstraint(Constraints.dictionary("SATES"));
        // CODE_SNIPPET: REST_API_INJECT_CONSTRAINT
    }

    private void deleteDir(File projectRoot) throws IOException {
        if (!projectRoot.exists()) return;
        Files.walk(projectRoot.toPath())
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
        assertFalse(projectRoot.exists());
    }

    String getResource(String name) {
        return Utils.getResourceText(name);
    }

    private RestApi getRestApi() {
        return RestApi.of(getResource("/openapi/petstore.yaml"));
    }
}
