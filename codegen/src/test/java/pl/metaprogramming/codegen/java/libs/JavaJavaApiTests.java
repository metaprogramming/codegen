/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.libs;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JavaJavaApiTests {

    @Test
    @SuppressWarnings("java:S5961")
    void test() {
        assertEquals("T", Java.genericParamT().toString());
        assertEquals("R", Java.genericParamR().toString());
        assertEquals("V", Java.genericParamV().toString());
        assertEquals("?", Java.genericParamUnknown().toString());

        assertEquals("void", Java.voidType().toString());

        assertEquals("java.lang.Class", Java.claasType().toString());
        assertEquals("java.lang.Class<T>", Java.claasType(Java.genericParamT()).toString());
        assertEquals("java.lang.Object", Java.objectType().toString());

        assertEquals("java.lang.Boolean", Java.boxedBoolean().toString());
        assertEquals("boolean", Java.primitiveBoolean().toString());
        assertEquals("java.lang.Double", Java.boxedDouble().toString());
        assertEquals("java.lang.Float", Java.boxedFloat().toString());
        assertEquals("java.lang.Integer", Java.boxedInteger().toString());
        assertEquals("int", Java.primitiveInt().toString());
        assertEquals("java.lang.Long", Java.longBoxed().toString());
        assertEquals("java.lang.String", Java.string().toString());

        assertEquals("java.math.BigDecimal", Java.bigDecimal().toString());
        assertEquals("java.util.Collection<java.lang.String>", Java.collection(Java.string()).toString());
        assertEquals("java.util.List<java.util.Collection<java.lang.String>>", Java.list(Java.collection(Java.string())).toString());
        assertEquals("java.util.Map<java.lang.Integer, java.lang.Boolean>", Java.map(Java.boxedInteger(), Java.boxedBoolean()).toString());
        assertEquals("java.io.Serializable", Java.serializable().toString());

        assertEquals("@java.lang.Override", Java.override().toString());
        assertEquals("@javax.annotation.Nonnull", Java.nonnul().toString());
        assertEquals("@javax.annotation.Nullable", Java.nullable().toString());
        assertEquals("@javax.annotation.ParametersAreNonnullByDefault", Java.parametersAreNonnullByDefault().toString());
        assertEquals("@java.lang.SuppressWarnings(\"unchecked\")", Java.suppressWarnings().toString());
    }

}
