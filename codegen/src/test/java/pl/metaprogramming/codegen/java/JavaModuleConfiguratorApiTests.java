/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java;

import org.junit.jupiter.api.Test;
import pl.metaprogramming.codegen.CodegenParams;
import pl.metaprogramming.codegen.Generator;
import pl.metaprogramming.codegen.Model;
import pl.metaprogramming.codegen.TypeOfCode;

import static org.junit.jupiter.api.Assertions.assertTrue;

class JavaModuleConfiguratorApiTests {

    static class TOC {
        final static TypeOfCode<Object> SERVICE = new TypeOfCode<>("SERVICE");
        final static TypeOfCode<String> RESPONSE_DTO = new TypeOfCode<>("RESPONSE_DTO");
        final static TypeOfCode<String> REQUEST_DTO = new TypeOfCode<>("REQUEST_DTO");
    }

    static class TestConfigurator extends JavaGeneratorBuilder<Model, TOC, TestConfigurator> {

        TestConfigurator() {
            super(new TOC());
        }
        @Override
        public Generator<Model> make() {
            return null;
        }

        @Override
        public void init() {
            params = new CodegenParams();
        }
    }

    @Test
    void verifyTypeOfCode() {
        TestConfigurator configurator = new TestConfigurator();
        configurator.init();

        configurator.typeOfCode(TOC.SERVICE).onDeclaration(b ->
                b.getMethods().add("execute", m -> {
                    m.setResultType(b.getClass(TOC.RESPONSE_DTO));
                    m.getParams().add("request", b.getClass(TOC.REQUEST_DTO));
                    m.setImplBody("// TODO implement me");
                }));

        assertTrue(configurator.getCodeBuilders().containsKey(TOC.SERVICE));
    }
}
