package pl.metaprogramming.model.data

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import pl.metaprogramming.model.data.constraint.Constraints
import pl.metaprogramming.model.data.constraint.RequiredConstraint

class DataSchemaKtApiTests {

    @Test
    fun testAddRequiredConstraint() {
        val schema = DataSchema("test", DataType.TEXT)
        assert(schema.constraints.constraints.isEmpty())
        schema.addConstraint(Constraints.required {
            ifExpression = "CONDITION"
        })
        assert(schema.constraints.constraints.isNotEmpty())
        val constraint = schema.constraints.constraints[0]
        assert(constraint is RequiredConstraint)
        Assertions.assertEquals("CONDITION", constraint.ifExpression)
    }
}