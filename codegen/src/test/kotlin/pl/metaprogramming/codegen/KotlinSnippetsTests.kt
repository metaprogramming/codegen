/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import org.jboss.forge.roaster.Roaster
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import pl.metaprogramming.codegen.java.DefaultJavaNameMapper
import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.java.base.MappingExpressionBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator
import pl.metaprogramming.codegen.java.spring.SpringSwaggerUiGenerator
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.constraint.*
import pl.metaprogramming.model.oas.*
import java.io.File

class KotlinSnippetsTests {

    @Test
    fun `should configure and run code generation`() {
        val openapiAppBody = File("src/testFixtures/resources/openapi/example-api.yaml").readText()
        val openapiExternalAppBody = File("src/testFixtures/resources/openapi/petstore.yaml").readText()
        val openapiCommonsBody = File("src/testFixtures/resources/openapi/example-api-deps.yaml").readText()
        val projectRoot = File("out/gen-test")
        projectRoot.deleteRecursively()
        assertFalse(projectRoot.exists())

        // CODE_SNIPPET: CODEGEN_SAMPLE
        codegen {
            baseDir = projectRoot
            lineSeparator = "\r\n"
            charset = Charsets.UTF_8
            indexFile = File(baseDir, "generated_files.yaml")
            forceMode = false
            addLastGenerationTag = false
            addLastUpdateTag = false

            dataTypeMapper[DataType.NUMBER] = "java.lang.Double"

            params.set(JavaParams::class) {
                isAlwaysGenerateEnumFromValueMethod = true
                generatedAnnotationValue = "my_generation_tag"
            }

            generate(SpringCommonsGenerator()) {
                rootPackage = "com.example.commons"
                typeOfCode().forEach { classCfg ->
                    if (classCfg.typeOfCode != toc.REST_EXCEPTION_HANDLER) {
                        classCfg.projectSubDir = "src/main/java-gen"
                    }
                }
            }

            val apiCommons = restApi(openapiCommonsBody)
            val api = restApi(openapiAppBody, apiCommons)
            val externalApi = restApi(openapiExternalAppBody, apiCommons)

            generate(SpringRestServiceGenerator()) {
                model = apiCommons
                rootPackage = "com.example.commons"
            }

            generate(SpringRestServiceGenerator()) {
                model = api
                rootPackage = "com.example"
                adapterPackageName = "rest"
            }

            generate(SpringRestClientGenerator()) {
                model = externalApi
                rootPackage = "com.example"
                adapterPackageName = "extsysname"
                params.set(JavaParams::class) {
                    generatedAnnotationValue = "my_client_generation_tag"
                }
                dataTypeMapper[DataType.NUMBER] = "java.lang.Float"
                typeOfCode(toc.CLIENT) {
                    forceGeneration = false
                    onDecoration {
                        methods.forEach {
                            it.implBody = "// comment\n${it.implBody}"
                        }
                    }
                }
            }

            generate(SpringSwaggerUiGenerator()) {
                api(api, "api.yaml")
                api(apiCommons, "commons.yaml")
            }
        }
        // CODE_SNIPPET: CODEGEN_SAMPLE

        assertTrue(projectRoot.exists())
        projectRoot.deleteRecursively()
    }

    @Test
    fun `params snippets`() {
        val codegen = Codegen()
        // CODE_SNIPPET: CODEGEN_FORCE_MODE
        codegen.forceMode = false
        // CODE_SNIPPET: CODEGEN_FORCE_MODE

        // CODE_SNIPPET: CODEGEN_PARAMS
        CodegenParams()
            .set(
                JavaParams()
                    .licenceHeader("My licence header")
                    .generatedAnnotationClass("javax.annotation.Generated")
                    .generatedAnnotationValue("my_generator")
                    .isAlwaysGenerateEnumFromValueMethod(true)
            )
            .set(ValidationParams::class) {
                throwExceptionIfValidationFailed = false
                isRequiredErrorCode = "validation.error.required"
                useNamedBeanAsRuleValidator = false
                ruleValidator("USER_ACCOUNT_BALANCE", "com.example.UserAccountBalanceValidator")
                formatValidator("amount", "com.example.AmountValidator")
            }
        // CODE_SNIPPET: CODEGEN_PARAMS

        // CODE_SNIPPET: VALIDATION_ERROR_CODE
        CodegenParams().set(ValidationParams::class) {
            isRequiredErrorCode = "validation.error.required"
        }
        // CODE_SNIPPET: VALIDATION_ERROR_CODE

        // CODE_SNIPPET: VALIDATION_PARAMS_FORMAT
        CodegenParams().set(ValidationParams::class) {
            formatValidator("amount", "com.example.AmountValidator")
        }
        // CODE_SNIPPET: VALIDATION_PARAMS_FORMAT

        // CODE_SNIPPET: VALIDATION_PARAMS_RULE
        CodegenParams().set(ValidationParams::class) {
            ruleValidator("USER_ACCOUNT_BALANCE", "com.example.UserAccountBalanceValidator")
        }
        // CODE_SNIPPET: VALIDATION_PARAMS_RULE

        // CODE_SNIPPET: VALIDATION_STOP
        CodegenParams().set(ValidationParams::class) {
            stopAfterFirstError = true
        }
        // CODE_SNIPPET: VALIDATION_STOP
    }

    @Test
    fun `generators snippets`() {

        Codegen().apply {
            // CODE_SNIPPET: CODE_INDEX_MAPPER
            generate(SpringRestServiceGenerator()) {
                model = getRestApi()
                typeOfCode(toc.OPERATION_EXECUTOR_IMPL) {
                    onDeclaration {
                        methods.add("execute") {
                            registerAsMapper()
                            resultType = getClass(toc.DTO, model.successResponseSchema)
                            params.add("request", getClass(toc.REQUEST_DTO)) {
                                addAnnotation(Java.nonnul())
                            }
                            implBody = "// TODO"
                        }
                    }
                }
                typeOfCode(toc.REST_CONTROLLER) {
                    onImplementation {
                        methods.add(nameMapper.toMethodName(model.code)) {
                            resultType = getClass(toc.DTO, model.successResponseSchema)
                            params.add("request", getClass(toc.REQUEST_DTO))
                            implBody = "return ${
                                MappingExpressionBuilder(context, model, implDependencies)
                                    .to(getClass(toc.DTO, model.successResponseSchema))
                                    .from(toc.REQUEST_DTO, "request")
                            };"
                        }
                    }
                }
            }
            // CODE_SNIPPET: CODE_INDEX_MAPPER

            // CODE_SNIPPET: VALIDATION_TOC_CFG
            generate(SpringCommonsGenerator()) {
                typeOfCode(
                    toc.VALIDATION_DICTIONARY_CHECKER,
                    toc.VALIDATION_DICTIONARY_CODE_ENUM
                ).forEach { it.packageName.root = "example.adapters.in.rest.validators.custom" }
            }
            // CODE_SNIPPET: VALIDATION_TOC_CFG

            // CODE_SNIPPET: TARGET_DIRECTORY
            baseDir = File("some/project/directory")

            generate(SpringCommonsGenerator()) {
                projectDir = "commons"
                projectSubDir = "src/main/java-gen"
                rootPackage = "example.commons"
                typeOfCode(
                    toc.VALIDATION_DICTIONARY_CHECKER,
                    toc.VALIDATION_DICTIONARY_CODE_ENUM
                ).forEach { it.packageName.root = "example.adapters.in.rest.validators.custom" }
            }
            // CODE_SNIPPET: TARGET_DIRECTORY

            // CODE_SNIPPET: GENERATOR_MODIFICATION
            generate(SpringCommonsGenerator()) {
                typeOfCode(toc.REST_EXCEPTION_HANDLER) {
                    onDecoration {
                        fields.add("messageSource", "org.springframework.context.MessageSource") {
                            isFinal = true
                        }
                        methods.add("resolveMessage") {
                            isPrivate = true
                            resultType = Java.string()
                            params.add("code", Java.string())
                            params.add("args", Java.objectType().asArray())
                            params.add("locale", "java.util.Locale")
                            implBody = """
                                |try {
                                |    return messageSource.getMessage(code, args, locale);
                                |} catch (NoSuchMessageException e) {
                                |    log.info("No message for code: {}", code);
                                |    return "INVALID_INPUT";
                                |}
                            """.trimMargin()
                        }
                        methods.set("toResponseEntity(result, request)") {
                            implBody = """
                                |return new ResponseEntity<>(resolveMessage(
                                |       result.getErrors().get(0).getCode(),"
                                |       result.getErrors().get(0).getMessageArgs(),"
                                |       request.getLocale());
                            """.trimMargin()
                        }
                    }
                }
            }
            // CODE_SNIPPET: GENERATOR_MODIFICATION

            // CODE_SNIPPET: CLASS_NAMING
            params.set(JavaParams::class) {
                nameMapper = DefaultJavaNameMapper()
            }
            generate(SpringRestServiceGenerator()) {
                model = getRestApi()
                typeOfCode(toc.OPERATION_EXECUTOR_IMPL) {
                    className.resolver { op ->
                        if (listOf(HttpMethod.POST).contains(op.method)) "Command" else "Query"
                    }
                }
                typeOfCode(toc.DTO) {
                    className.suffix = "DTO"
                }
            }
            // CODE_SNIPPET: CLASS_NAMING

            // CODE_SNIPPET: LICENCE_HEADER
            params[JavaParams::class].licenceHeader = "My licence header"
            // CODE_SNIPPET: LICENCE_HEADER

            // CODE_SNIPPET: CODE_FORMATTER
            params[JavaParams::class].codeDecorators.add {
                Roaster.format(it)
            }
            // CODE_SNIPPET: CODE_FORMATTER

            // CODE_SNIPPET: DRY_RUN
            generate(SpringCommonsGenerator()) {
                params.dryRun = true
            }
            // CODE_SNIPPET: DRY_RUN

            // CODE_SNIPPET: ALWAYS_GENERATE_ENUM_FROM_VALUE_METHOD
            params[JavaParams::class].isAlwaysGenerateEnumFromValueMethod = true
            // CODE_SNIPPET: ALWAYS_GENERATE_ENUM_FROM_VALUE_METHOD
        }
    }

    @Test
    fun `RestApi snippets`() {
        val apiText = File("src/testFixtures/resources/openapi/petstore.yaml").readText()

        // CODE_SNIPPET: REST_API_CREATE
        val restApi = RestApi.of(apiText)
        // CODE_SNIPPET: REST_API_CREATE

        // CODE_SNIPPET: REST_API_OPERATIONS_REMOVE
        restApi.operations.removeIf { it.code == "operationCode" }
        // CODE_SNIPPET: REST_API_OPERATIONS_REMOVE

        // CODE_SNIPPET: REST_API_OPERATIONS_GROUPING
        restApi.operations.forEach { it.group = "group" }
        // CODE_SNIPPET: REST_API_OPERATIONS_GROUPING

        // CODE_SNIPPET: REST_API_ADD_PARAMETER
        val newParam = Parameter("x-my-header", ParamLocation.COOKIE, DataType.TEXT)
        restApi.addParameter(newParam)
        restApi.operations.forEach { it.parameters.add(newParam) }
        // CODE_SNIPPET: REST_API_ADD_PARAMETER

        // CODE_SNIPPET: REST_API_SAVE
        OpenapiWriter.write(restApi, File("out/api.yaml"))
        // CODE_SNIPPET: REST_API_SAVE

        // CODE_SNIPPET: REST_API_INJECT_CONSTRAINT
        restApi.getSchema("Address").addConstraint(CheckerRef.of("com.validators.AdvancedAddressValidator"))
        restApi.getSchema("Address.state").addConstraint(Constraints.dictionary("SATES"))
        // CODE_SNIPPET: REST_API_INJECT_CONSTRAINT
    }

    private fun getRestApi() =
        RestApi.of(File("src/testFixtures/resources/openapi/petstore.yaml").readText())
}