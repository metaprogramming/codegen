/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.AnnotationCm.Companion.of
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.builders.AnnotatedBuilder
import pl.metaprogramming.model.data.DataType

class CodegenParamsKtApiTests {

    @Test
    fun `should set JavaModuleParams`() {
        // BEGIN KOTLIN CODE EXAMPLE - SetJavaModuleParams
        val codeDecorator = CodeDecorator { p -> "// code header\n$p" }
        val myLicenceHeader = "Licence header"
        val myNameMapper: JavaNameMapper = DefaultJavaNameMapper()
        val myComponentStrategy = AnnotatedBuilder.by("org.springframework.stereotype.Component")
        val myDiStrategy: ClassCmBuilderTemplate<Nothing> = object : ClassCmBuilderTemplate<Nothing>() {
            private val autowired = of("org.springframework.beans.factory.annotation.Autowired")
            override fun makeDecoration() {
                fields.forEach { if (!it.isStatic) it.addAnnotation(autowired) }
            }
        }
        val p = CodegenParams()
            .set(JavaParams::class) {
                generatedAnnotationClass = "javax.annotation.Generated"
                generatedAnnotationValue = "my-generator"
                nameMapper = myNameMapper
                addCodeDecorator(codeDecorator)
                licenceHeader = myLicenceHeader
                dependencyInjectionBuilder = myDiStrategy
                managedComponentBuilder = myComponentStrategy
                isAlwaysGenerateEnumFromValueMethod = true
            }
        p[DataTypeMapper::class][DataType.DATE] = "java.time.OffsetDataTime"
        // END KOTLIN CODE EXAMPLE - SetJavaModuleParams

        val jp = p[JavaParams::class]
        assertEquals("javax.annotation.Generated", jp.generatedAnnotation.annotationClass.canonicalName)
        assertEquals("\"my-generator\"", jp.generatedAnnotation.params["value"].toString())
        assertEquals(myNameMapper, jp.nameMapper)
        assertEquals(2, jp.codeDecorators.size)
        assertEquals(codeDecorator, jp.codeDecorators[0])
        assertTrue(jp.codeDecorators[1] is LicenceDecorator)
        assertEquals(myDiStrategy, jp.dependencyInjectionBuilder)
        assertEquals(myComponentStrategy, jp.managedComponentBuilder)
        assertTrue(jp.isAlwaysGenerateEnumFromValueMethod)
        assertEquals("java.time.OffsetDataTime", "${p[DataTypeMapper::class][DataType.DATE]}")


        p[DataTypeMapper::class][DataType.DATE] = "java.util.Date"
        assertEquals("java.util.Date", "${p[DataTypeMapper::class][DataType.DATE]}")

    }

    @Test
    fun `should update JavaParams V1`() {
        val codegenParams = CodegenParams()
        assertFalse(codegenParams[JavaParams::class].isAlwaysGenerateEnumFromValueMethod)
        assertEquals("java.time.LocalDate", codegenParams[DataTypeMapper::class][DataType.DATE]?.canonicalName)
        // BEGIN KOTLIN CODE EXAMPLE - UpdateJavaModuleParams v1
        codegenParams.set(JavaParams::class) {
            isAlwaysGenerateEnumFromValueMethod = true
        }
        codegenParams[DataTypeMapper::class][DataType.DATE] = "java.util.Date"
        // END JAVA CODE EXAMPLE - UpdateJavaModuleParams v1
        assertTrue(codegenParams[JavaParams::class].isAlwaysGenerateEnumFromValueMethod)
        assertEquals("java.util.Date", "${codegenParams[DataTypeMapper::class][DataType.DATE]}")
    }

    @Test
    fun `should update JavaModuleParams V2`() {
        val codegenParams = CodegenParams()
        assertFalse(codegenParams[JavaParams::class].isAlwaysGenerateEnumFromValueMethod)
        assertEquals("java.time.LocalDate", "${codegenParams[DataTypeMapper::class][DataType.DATE]}")
        // BEGIN KOTLIN CODE EXAMPLE - UpdateJavaModuleParams v2
        codegenParams[JavaParams::class].isAlwaysGenerateEnumFromValueMethod = true
        codegenParams[DataTypeMapper::class][DataType.DATE] = "java.util.Date"
        // END JAVA CODE EXAMPLE - UpdateJavaModuleParams v2
        assertTrue(codegenParams[JavaParams::class].isAlwaysGenerateEnumFromValueMethod)
        assertEquals("java.util.Date", "${codegenParams[DataTypeMapper::class][DataType.DATE]}")
    }
}