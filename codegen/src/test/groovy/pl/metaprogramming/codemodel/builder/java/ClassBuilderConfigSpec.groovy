/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codemodel.builder.java

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.TypeOfCodeWithNoModel
import pl.metaprogramming.codegen.java.base.ClassBuilderConfigurator
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import spock.lang.Specification

class ClassBuilderConfigSpec extends Specification {

    def "should remove builder by class"() {
        given:
        def config = makeConfigurator()
        def strategy = new SomeBuilder()

        when:
        config.builders.add(strategy)
        config.builders.add(new ClassCmBuilderTemplate<Object>())
        config.builders.removeIf { it instanceof SomeBuilder }
        then:
        !config.builders.contains(strategy)
        config.builders.size() == 1
    }

    def "should remove builder by value"() {
        given:
        def config = makeConfigurator()
        def builder = new SomeBuilder()

        when:
        config.builders.add(builder)
        config.builders.add(new SomeBuilder())
        config.builders.remove(builder)
        then:
        !config.builders.contains(builder)
        config.builders.size() == 1
    }

    def "should replace builder by class"() {
        given:
        def config = makeConfigurator()
        def builder = new SomeBuilder()
        def b1 = new ClassCmBuilderTemplate()
        def b2 = new ClassCmBuilderTemplate()

        when:
        config.builders.add(builder)
        config.builders.add(b1)
        config.builders.replaceAll { it instanceof SomeBuilder ? b2 : it }
        then:
        !config.builders.contains(builder)
        config.builders.contains(b1)
        config.builders.contains(b2)
        config.builders.size() == 2
    }

    def "should replace builder by value"() {
        given:
        def config = makeConfigurator()
        def builder = new SomeBuilder()
        def b1 = new SomeBuilder()
        def b2 = new ClassCmBuilderTemplate()

        when:
        config.builders.add(builder)
        config.builders.add(b1)
        config.builders.replaceAll { it == builder ? b2 : it }

        then:
        !config.builders.contains(builder)
        config.builders.contains(b1)
        config.builders.contains(b2)
        config.builders.size() == 2
    }

    static class SomeBuilder extends ClassCmBuilderTemplate {
    }

    def makeConfigurator() {
        def params = new CodegenParams()
        new ClassBuilderConfigurator(new TypeOfCodeWithNoModel('typeOfCode'), params)
    }
}
