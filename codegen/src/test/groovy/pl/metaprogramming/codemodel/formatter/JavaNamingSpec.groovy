/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codemodel.formatter

import pl.metaprogramming.codegen.java.DefaultJavaNameMapper
import spock.lang.Specification

class JavaNamingSpec extends Specification {

    def "should map to field name (#text - #javaName)"() {
        given:
        def mapper = new DefaultJavaNameMapper()
        expect:
        mapper.toFieldName(text) == javaName

        where:
        text                         | javaName
        'camelCaseName'              | 'camelCaseName'
        'camel2caseName'             | 'camel2caseName'
        'camel2CaseName'             | 'camel2CaseName'
        'PascaleCaseName'            | 'pascaleCaseName'
        'snake_case_name'            | 'snakeCaseName'
        'SNAKE_CASE_NAME'            | 'snakeCaseName'
        'kebab-case-name'            | 'kebabCaseName'
        'KEBAB-CASE-NAME'            | 'kebabCaseName'
        'Mixed-camelCase_PascalCase' | 'mixedCamelCasePascalCase'
        'strange name'               | 'strangeName'
        'strange%name'               | 'strangeName'
        'name'                       | 'name'
        'NAME'                       | 'name'
        '_name'                      | 'name'
        'name_'                      | 'name'
        '_NAME'                      | 'name'
        '#name'                      | 'name'
        '10name'                     | 'v10name'
        'name10'                     | 'name10'
        'URLField'                   | 'urlField'
        'd_u_r_l_field'              | 'dURLField'
        'dURLField'                  | 'dUrlField'
        'dRField'                    | 'dRField'
    }

    def "should map to constant name (#text - #javaName"() {
        given:
        def mapper = new DefaultJavaNameMapper()
        expect:
        mapper.toConstantName(text) == javaName

        where:
        text                         | javaName
        'camelCaseName'              | 'CAMEL_CASE_NAME'
        'camel2CaseName'             | 'CAMEL2_CASE_NAME'
        'camel2caseName'             | 'CAMEL2CASE_NAME'
        'PascaleCaseName'            | 'PASCALE_CASE_NAME'
        'snake_case_name'            | 'SNAKE_CASE_NAME'
        'SNAKE_CASE_NAME'            | 'SNAKE_CASE_NAME'
        'kebab-case-name'            | 'KEBAB_CASE_NAME'
        'KEBAB-CASE-NAME'            | 'KEBAB_CASE_NAME'
        'Mixed-camelCase_PascalCase' | 'MIXED_CAMEL_CASE_PASCAL_CASE'
        'strange name'               | 'STRANGE_NAME'
        'strange%name'               | 'STRANGE_NAME'
        'name'                       | 'NAME'
        'NAME'                       | 'NAME'
        '_name'                      | 'NAME'
        'name_'                      | 'NAME'
        '_NAME'                      | 'NAME'
        '#name'                      | 'NAME'
        '10name'                     | 'V_10NAME'
        'name10'                     | 'NAME10'
        'URLField'                   | 'URL_FIELD'
    }

    def "should be mapped with underscore prefix: #text - #javaName"() {
        given:
        def mapper = new UnderscoreNameMapper()
        expect:
        mapper.toConstantName(text) == javaName

        where:
        text     | javaName
        '10name' | '_10NAME'
    }

    def "should be mapped with uppercase word: #text - #javaName"() {
        given:
        def mapper = new UppercaseWordsAllowed()
        expect:
        mapper.toClassName(text) == javaName

        where:
        text            | javaName
        'URLData'       | 'URLData'
        'URL_data'      | 'URLData'
        'DataWithURL'   | 'DataWithURL'
        'data-with-URL' | 'DataWithURL'
    }

    class UnderscoreNameMapper extends DefaultJavaNameMapper {
        List<String> toWords(String text) {
            def result = splitWords(text)
            if (result[0].charAt(0).isDigit()) {
                result[0] = '_' + result[0]
            }
            result
        }
    }

    class UppercaseWordsAllowed extends DefaultJavaNameMapper {
        @Override
        String toClassName(String text) {
            toWords(text).collect { it.capitalize() }.join('')
        }
    }
}
