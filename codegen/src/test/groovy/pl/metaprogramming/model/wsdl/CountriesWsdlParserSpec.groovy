/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl

import pl.metaprogramming.model.data.XmlObjectType
import spock.lang.Shared
import spock.lang.Specification

import static pl.metaprogramming.utils.CheckUtils.checkList

class CountriesWsdlParserSpec extends Specification {

    @Shared WsdlApi api
    @Shared String namespace = 'http://spring.io/guides/gs-producing-web-service'

    def setupSpec() {
        api = WsdlApi.of('src/test/resources/wsdl/countries.wsdl')
    }

    def "check service name"() {
        expect:
        api.name == 'CountriesPortService'
    }

    def "check service uri"() {
        expect:
        api.uri == 'http://localhost:8080/ws'
    }

    def "check namespaceElementFormDefault"() {
        expect:
        api.namespaceElementFormDefault['http://spring.io/guides/gs-producing-web-service'] == 'qualified'
    }

    def "check getCountry operation"() {
        when:
        def operation = api.operations.find { it.name == 'getCountry'}
        then:
        operation != null

        when:
        def inputQname = toQname(operation.input.xmlDataType.namespace, operation.input.code)
        def outputQname = toQname(operation.output.xmlDataType.namespace, operation.output.code)
        then:
        inputQname == toQname(namespace, 'getCountryRequest')
        outputQname == toQname(namespace, 'getCountryResponse')
    }

    def "check currency type"() {
        when:
        def schema = api.schemas[toQname(namespace, 'currency')]
        then:
        schema.enum
        checkList(
                'currency allowed values',
                schema.enumType.allowedValues,
                ['GBP', 'EUR', 'PLN'])
    }

    def "check country type"() {
        when:
        def schema = api.schemas[toQname(namespace, 'country')]
        then:
        schema.object
        checkList(
                'country type fields',
                schema.objectType.fields.collect { it.toString() },
                ['ENUM[currency, required, namespace: http://spring.io/guides/gs-producing-web-service, allowed: [GBP, EUR, PLN]]',
                 'capital STRING[required, namespace: http://www.w3.org/2001/XMLSchema]',
                 'name STRING[required, namespace: http://www.w3.org/2001/XMLSchema]',
                 'population INT32[required, namespace: http://www.w3.org/2001/XMLSchema]',
                ]
        )
    }

    def "check getCountryRequest type"() {
        when:
        def schema = api.schemas[toQname(namespace, 'getCountryRequest')]
        then:
        schema.object
        schema.objectType.rootElement
        checkList(
                'getCountryRequest type fields',
                schema.objectType.fields.collect { it.toString() },
                ['name STRING[required, namespace: http://www.w3.org/2001/XMLSchema]']
        )
    }

    def "check getCountryResponse type"() {
        when:
        def schema = api.schemas[toQname(namespace, 'getCountryResponse')]
        then:
        schema.object
        (schema.objectType as XmlObjectType).rootElement
        checkList(
                'getCountryResponse type fields',
                schema.objectType.fields.collect { it.toString() },
                ["OBJECT[country, required, namespace: http://spring.io/guides/gs-producing-web-service, fields: [name STRING[required, namespace: http://www.w3.org/2001/XMLSchema], population INT32[required, namespace: http://www.w3.org/2001/XMLSchema], capital STRING[required, namespace: http://www.w3.org/2001/XMLSchema], ENUM[currency, required, namespace: http://spring.io/guides/gs-producing-web-service, allowed: [GBP, EUR, PLN]]]]"]
        )
    }

    static String toQname(String namespace, String name) {
        "{$namespace}$name"
    }
}
