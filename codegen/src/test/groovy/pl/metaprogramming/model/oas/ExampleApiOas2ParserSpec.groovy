/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas


import pl.metaprogramming.fixtures.ExampleApiModels

import static pl.metaprogramming.fixtures.ExampleApiModels.loadExampleApiDepsModel
import static pl.metaprogramming.fixtures.ExampleApiModels.loadExampleApiModel

class ExampleApiOas2ParserSpec extends ExampleApiParserSpecification {

    def setupSpec() {
        depsApi = loadExampleApiDepsModel(ExampleApiModels.OpenapiVer.V2)
        api = loadExampleApiModel(depsApi, ExampleApiModels.OpenapiVer.V2)
        api.forEachOperation {
            it.requestBody?.code = 'body'
        }
    }

    def "should parse x-validation-beans for echoPost operation"() {
        when:
        def operations = api.getOperation('echoPost')
        then:
        operations.additives['x-validation-beans'] == [
                [class  : 'example.adapters.in.rest.validators.UserDataValidationBean',
                 factory: 'example.adapters.in.rest.validators.ValidationBeanFactory:createUserDataValidationBean']
        ]
    }

    def "should parse x-validation-beans for EchoBody definition"() {
        when:
        def schema = getSchema('EchoBody')
        then:
        schema.object
        schema.additives['x-validation-beans'] == [
                [class  : 'example.adapters.in.rest.validators.ExtendedObjectEnumChecker',
                 factory: 'example.adapters.in.rest.validators.ValidationBeanFactory:createExtendedObjectEnumChecker']
        ]
    }

    def "should parse x-validations for EchoBody.prop_amount definition"() {
        when:
        def schema = getSchema('EchoBody')
        def field = schema.objectType.fields.find { it.code == 'prop_amount' }
        then:
        field != null
        field.additives['x-validations'] == [
                'bean:example.adapters.in.rest.validators.UserDataValidationBean:checkAmountByUser',
                'field:example.commons.validator.Checkers.AMOUNT_SCALE_CHECKER'
        ]
    }

    def "should parse x-validations for ExtendedObject definition"() {
        when:
        def schema = getSchema('ExtendedObject')
        then:
        schema.additives['x-validations'] == ['di-bean:example.adapters.in.rest.validators.ExtendedObjectChecker']
    }

    def "should parse x-validations for ExtendedObject.eo_enum_reusable definition"() {
        when:
        def schema = getSchema('ExtendedObject')
        def field = schema.objectType.fields.find { it.code == 'eo_enum_reusable' }
        then:
        field != null
        field.additives['x-validations'] == ['bean:example.adapters.in.rest.validators.ExtendedObjectEnumChecker']
    }

}
