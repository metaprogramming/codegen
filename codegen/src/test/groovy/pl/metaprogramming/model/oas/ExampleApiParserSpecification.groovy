/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import pl.metaprogramming.model.data.DataSchema
import spock.lang.Shared
import spock.lang.Specification

import static pl.metaprogramming.utils.CheckUtils.checkList

abstract class ExampleApiParserSpecification extends Specification {

    @Shared RestApi api
    @Shared RestApi depsApi

    def "example-api-deps.yaml parser"() {
        when:
        def schemas = depsApi.schemas.collect { it.code }
        then:
        checkList(
                'schemas',
                schemas, [
                'ErrorDescription',
                'ErrorDetail',
                'ErrorItem',
                'ReusableEnum',
                'SimpleObject',
        ])
    }

    def "check EchoBody model"() {
        when:
        def schema = getSchema('EchoBody')
        then:
        schema.object

        when:
        def amountField = schema.objectType.fields.find { it.code == 'prop_amount_number'}
        then:
        amountField != null
        amountField.maximum == '9999999999.99'
    }

    def "check uploadEchoFile operation"() {
        when:
        def operation = api.getOperation('uploadEchoFile')
        then:
        operation != null
        operation.consumes == ['application/octet-stream']
        operation.parameters.collect {it.name} == ['id']
        operation.requestSchema.schema.fields.toString() == "[id INT64[required, 'id of file'], body BINARY[required, 'file data to upload']]"
        operation.responses.collect {it.toString() } == ['204', 'default OBJECT[ErrorDescription, fields: [code INT32[required], message STRING[required], errors LIST OF OBJECT[ErrorDetail, fields: [field STRING, code STRING[required], message STRING[required]]]]]']
        operation.produces == ['application/json']
    }

    def "check uploadEchoFileWithForm operation"() {
        when:
        def operation = api.getOperation('uploadEchoFileWithForm')
        then:
        operation != null
        operation.consumes == ['multipart/form-data']
        operation.parameters.collect {it.name} == ['id']
        operation.requestSchema.fields.toString() == "[id INT64[required, 'id of file'], file BINARY[required, 'file to upload']]"
        operation.responses.collect {it.toString() } == ['204', 'default OBJECT[ErrorDescription, fields: [code INT32[required], message STRING[required], errors LIST OF OBJECT[ErrorDetail, fields: [field STRING, code STRING[required], message STRING[required]]]]]']
        operation.produces == ['application/json']
    }

    def "check downloadEchoFile operation"() {
        when:
        def operation = api.getOperation('downloadEchoFile')
        then:
        operation != null

        when:
        def successResponse = operation.responses.find { it.status == 200 }
        then:
        successResponse != null
        successResponse.toString() == '200 BINARY'
    }

    DataSchema getSchema(String definition) {
        def schemaEntry = api.getSchema(definition)
        assert schemaEntry != null
        schemaEntry
    }

}
