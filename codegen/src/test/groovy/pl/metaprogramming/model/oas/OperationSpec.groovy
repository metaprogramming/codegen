/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas


import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.ObjectType
import spock.lang.Specification

/*
 Operation object caches data for: requestSchemas, securityParameters
 these data should be recalculated after changes in fields:
 - requestSchemas
 - security
*/
class OperationSpec extends Specification {

    def api = new RestApi([])
    def operation = new Operation(api, "resource", "operationId", "/resource", HttpMethod.POST)

    def "should refresh cached requestSchemas"() {
        when:
        operation.requestSchemas // fill cache
        then:
        operation.requestBodySchema == null

        when:
        operation.requestBody = new HttpRequestBody("body", true, "request payload", [
                "application/json": new ObjectType("requestBody")
                        .addField("field", DataType.TEXT)
                        .asSchema()
        ])
        then:
        operation.requestBodySchema != null
        operation.requestBodySchema.isObject()
        operation.requestBodySchema.objectType.field("field").dataType == DataType.TEXT
        operation.requestBodySchema.code == "body"

        when:
        operation.requestBody.code = "requestBody"
        then:
        operation.requestBodySchema.code == "requestBody"
    }

    def "should refresh cached securityParameters"() {
        when:
        operation.requestSchemas // fill cache
        then:
        operation.securityParameters.empty

        when:
        // adding security should invalid cache
        operation.security.add(new SecurityConstraint("authToken", ["write"]))
        api.securitySchemas.add(new SecuritySchema(
                "authToken",
                "apiKey",
                "description",
                [],
                ParamLocation.HEADER,
                "api_key"
        ))
        then:
        !operation.securityParameters.empty
    }
}
