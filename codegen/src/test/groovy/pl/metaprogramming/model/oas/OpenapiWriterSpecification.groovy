/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas


import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

import static pl.metaprogramming.utils.CheckUtils.checkList

@Stepwise
abstract class OpenapiWriterSpecification extends Specification {

    static File outDir = new File('out/oas3-writer')

    @Shared
    RestApi originalApi

    @Shared
    RestApi restoredApi

    abstract RestApi loadOriginalApi()

    abstract def getOutFilename()

    RestApi loadStoredApi() {
        fixApi(RestApi.of(getOutFile(getOutFilename() as String).text))
    }

    def saveApi() {
        OpenapiWriter.write(originalApi, getOutFile(getOutFilename() as String))
    }

    def setupSpec() {
        outDir.mkdirs()
        assert outDir.isDirectory()
        if (getOutFilename() instanceof String) {
            cleanOutFile(getOutFilename() as String)
        } else {
            (getOutFilename() as Map<RestApi, String>).values().each {
                cleanOutFile(it)
            }
        }
        originalApi = fixApi(loadOriginalApi())
    }

    def cleanOutFile(String filename) {
        def outFile = getOutFile(filename)
        outFile.delete()
        assert !outFile.exists()
    }

    File getOutFile(String filename) {
        new File(outDir, filename)
    }

    def "should store and load API again"() {
        when:
        saveApi()
        restoredApi = loadStoredApi()
        then:
        noExceptionThrown()
        restoredApi.name == originalApi.name
        restoredApi.info.version == originalApi.info.version
        restoredApi.servers == restoredApi.servers
        restoredApi.tags == restoredApi.tags
    }

    def "restored API should have the same operations"() {
        expect:
        checkList('operations', restoredApi, originalApi,
                { it.operations.collect { it.toString() } })
    }

    def "restored API should have the same security schema: #original.code"() {
        when:
        def restored = restoredApi.securitySchemas.find { it.code == original.code }
        then:
        restoredApi.securitySchemas.find { it.code == original.code }
        restored.type == original.type
        restored.description == original.description
        restored.paramLocation == original.paramLocation
        restored.paramName == original.paramName
        restored.oauth2Flows.size() == original.oauth2Flows.size()
        original.oauth2Flows.every { Oauth2Flow originalFlow ->
            def flow = restored.oauth2Flows.find { it.type == originalFlow.type }
            assert flow.type == originalFlow.type
            assert flow.authorizationUrl == originalFlow.authorizationUrl
            assert flow.tokenUrl == originalFlow.tokenUrl
            assert flow.scopes == originalFlow.scopes
            true
        }

        where:
        original << originalApi.securitySchemas
    }

    def "restored #op operation should be the same as the original"() {
        given:
        def operations = getOperations(op)

        when:
        def original = operations[0]
        def restored = operations[1]

        then:
        original != null
        restored != null
        restored.toString() == original.toString()
        restored.additives == original.additives
        checkList('parameters', restored, original,
                { it.parameters.collect { it.toString() } })
        checkList('responses', restored, original,
                { it.responses.collect { it.toString() } })
        checkRequestBody(restored.requestBody, original.requestBody)
        original.security == restored.security

        where:
        op << originalApi.operations.collect { "$it.group:$it.code" }
    }

    boolean checkRequestBody(HttpRequestBody given, HttpRequestBody expected) {
        if (given == null && expected == null) {
            return true
        } else if (given == null || expected == null) {
            return false
        }
        assert given.code == expected.code
        assert given.required == expected.required
        assert given.description == expected.description
        checkList("request contents", given, expected,
                { it.contents.collect { "$it.key - $it.value" as String } })
        true
    }

    List<Operation> getOperations(String groupWithCode) {
        def split = groupWithCode.split(':')
        def group = split[0]
        def code = split[1]
        [
                originalApi.operations.find { it.group == group && it.code == code },
                restoredApi.operations.find { it.group == group && it.code == code }
        ]
    }

    RestApi fixApi(RestApi api) {
        api
    }
}
