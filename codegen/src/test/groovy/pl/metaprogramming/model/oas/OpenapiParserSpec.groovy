/*
 * Copyright (c) 2021 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.data.EnumType
import pl.metaprogramming.utils.CheckUtils
import spock.lang.Specification

import java.util.function.Consumer

class OpenapiParserSpec extends Specification {

    /*
      Test Import swagger 2.0 specifications:
        https://petstore.swagger.io/v2/swagger.jyaml
        https://petstore.swagger.io/v2/swagger.json
        https://petstore3.swagger.io/api/v3/openapi.yaml
        https://github.com/OAI/OpenAPI-Specification/blob/master/examples/v2.0/yaml/petstore-simple.yaml
        https://docs.polishapi.org/files/ver2.1.1/PolishAPI-ver2_1_1.yaml
     */

    def "read #file"() {
        when:
        def apiContent = getClass().getResource("/openapi/$file").getText('UTF-8')
        def result = RestApi.of(apiContent)
        modelFixer == null || modelFixer.accept(result)

        then:
        apiContent != null
        result != null
        validator.call(result)

        where:
        file                         | modelFixer           | validator
        'petstore.yaml'              | null                 | { validatePetstore(it) }
        'petstore.yaml'              | setGroup('petstore') | { validatePetstore(it, ['petstore']) }
        'v2/petstore.yaml'           | null                 | { validatePetstore(it) }
        'v2/petstore.json'           | null                 | { validatePetstore(it) }
        'v2/petstore-simple.yaml'    | setGroup('pets')     | { validatePetstoreSimple(it, 'pets') }
        'v2/petstore-simple.yaml'    | setGroup('petstore') | { validatePetstoreSimple(it, 'petstore') }
        'v2/PolishAPI-ver2_1_1.yaml' | null                 | { validatePolishAPI(it) }
        'no-schemas.yaml'            | null                 | { true }
        'v2/no-schemas.yaml'         | null                 | { true }
    }

    boolean validatePetstore(RestApi rs, def resources = ['pet', 'store', 'user']) {
        assert new HashSet(rs.operations.collect { it.group }) == new HashSet(resources)
        def enums = collectEnums(rs)
        assert enums == ["ENUM[OrderStatus, 'Order Status', allowed: [placed, approved, delivered]]",
                         "ENUM[Status, 'pet status in the store', allowed: [available, pending, sold]]"]
        rs.securitySchemas.size() == 2

        // security schemas
        def security1 = rs.securitySchemas.find { it.code == 'petstore_auth' }
        assert security1.type == 'oauth2'
        assert !security1.oauth2Flows.empty
        assert security1.oauth2Flows[0].type == 'implicit'
        assert security1.oauth2Flows[0].authorizationUrl != null
        assert security1.oauth2Flows[0].tokenUrl == null
        assert security1.oauth2Flows[0].scopes.keySet() == ['write:pets', 'read:pets'] as Set
        def security2 = rs.securitySchemas.find { it.code == 'api_key' }
        assert security2.type == 'apiKey'
        assert security2.oauth2Flows.empty
        assert security2.paramLocation == ParamLocation.HEADER
        assert security2.paramName == 'api_key'

        // operation security
        def operation = rs.operations.find { it.code == 'findPetsByTags' }
        assert operation.security.size() == 1
        assert operation.security[0].schema == 'petstore_auth'
        assert operation.security[0].scopes == ['write:pets', 'read:pets']

        true
    }

    Consumer<RestApi> setGroup(String group) {
        { api -> api.forEachOperation { it.group = group } }
    }

    boolean validatePetstoreSimple(RestApi rs, String resource) {
        CheckUtils.checkList('schemas', rs.schemas.collect { it.code },
                ['Pet', 'NewPet', 'ErrorModel'])
        checkSchema(rs, 'Pet', "OBJECT[Pet, fields: [id INT64[required]], inherits: [OBJECT[NewPet, fields: [name STRING[required], tag STRING]]]]")
        checkSchema(rs, 'NewPet', "OBJECT[NewPet, fields: [name STRING[required], tag STRING]]")
        checkSchema(rs, 'ErrorModel', "OBJECT[ErrorModel, fields: [code INT32[required], message STRING[required]]]")

        def operations = rs.operations.collect {
            "${it.group}.${it.code} ${it.method} ${it.path}"
        }
        CheckUtils.checkList('operations', operations, [
                "${resource}.findPets GET /api/pets",
                "${resource}.addPet POST /api/pets",
                "${resource}.findPetById GET /api/pets/{id}",
                "${resource}.deletePet DELETE /api/pets/{id}"])

        def findPets = rs.getOperation('findPets')
        assert findPets != null
        CheckUtils.checkList('responses', findPets.responses.collect { it.toString() }, [
                '200 LIST OF OBJECT[Pet, fields: [id INT64[required]], inherits: [OBJECT[NewPet, fields: [name STRING[required], tag STRING]]]]',
                'default OBJECT[ErrorModel, fields: [code INT32[required], message STRING[required]]]',
        ])

        def findPetById = rs.getOperation('findPetById')
        assert findPetById != null
        CheckUtils.checkList('responses', findPetById.responses.collect { it.toString() }, [
                '200 OBJECT[Pet, fields: [id INT64[required]], inherits: [OBJECT[NewPet, fields: [name STRING[required], tag STRING]]]]',
                'default OBJECT[ErrorModel, fields: [code INT32[required], message STRING[required]]]',
        ])

        true
    }

    boolean validatePolishAPI(RestApi rs) {
        checkParam(rs, 'authorizationParam', 'Authorization', ParamLocation.HEADER, true, DataTypeCode.STRING)
        checkParam(rs, 'acceptEncodingParam', 'Accept-Encoding', ParamLocation.HEADER, true, new EnumType(['gzip', 'deflate']))
        checkParam(rs, 'acceptLanguageParam', 'Accept-Language', ParamLocation.HEADER, true, DataTypeCode.STRING)
        checkParam(rs, 'acceptCharsetParam', 'Accept-Charset', ParamLocation.HEADER, true, DataTypeCode.ENUM)
        checkParam(rs, 'xjwsSignatureParam', 'X-JWS-SIGNATURE', ParamLocation.HEADER, true, DataTypeCode.STRING)
        checkSchema(rs, 'RequestHeaderWithoutTokenAS',
                "OBJECT[RequestHeaderWithoutTokenAS, 'Klasa zawierająca informacje o PSU na potrzeby usług autoryzacji / PSU Information Class dedicated for authorization services', fields: [isCompanyContext BOOLEAN['(true / false) Znacznik oznaczający czy żądanie jest wysyłane w kontekście PSU korporacyjnego'], psuIdentifierType STRING['Typ identyfikatora PSU, służy do wskazania przez TPP na podstawie jakiej informacji zostanie zidentyfikowany PSU, który będzie podlegał uwierzytelnieniu. Wartość słownikowa. / PSU identifier type, used by TPP to indicate type of information based on which the PSU is to be authenticated. Dictionary value.'], psuIdentifierValue STRING['Wartość identyfikatora PSU. Wymagany warunkowo - w przypadku gdy została przekazana niepusta wartość typu identyfikatora PSU. / The value of the PSU's identifier. Required conditionally - in case non empty value of PSU identifier type was passed.'], psuContextIdentifierType STRING['Typ identyfikatora kontekstu w jakim występuje PSU. Wartość słownikowa. Wymagany warunkowo - w przypadku wysyłania żądania dla takiego PSU, który może występować w więcej niż jednym kontekście w wybranym ASPSP. / Identifier of context that is used by PSU. Dictionary value. Required conditionally - in case context of the request is used and PSU may use more then one context for the ASPSP.'], psuContextIdentifierValue STRING['Wartość identyfikatora kontekstu w jakim występuje PSU. Wymagany warunkowo - w przypadku wysyłania żądania dla takiego PSU, który może występować w więcej niż jednym kontekście w wybranym ASPSP. / The value of context that is used by PSU. Required conditionally - in case context of the request is used and PSU may use more then one context for the ASPSP.']], inherits: [OBJECT[RequestHeaderWithoutToken, 'Klasa zawierająca informacje o PSU / PSU Information Class', fields: [requestId STRING[required, 'Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1) zgodnym ze standardem RFC 4122 / Request ID using UUID format (Variant 1, Version 1) described in RFC 4122 standard'], userAgent STRING['Browser agent dla PSU / PSU browser agent'], ipAddress STRING['Adres IP końcowego urządzenia PSU. Wymagany dla isDirectPsu=true. / IP address of PSU's terminal device. Required when isDirectPsu=true.'], sendDate DATE_TIME['Oryginalna data wysłania, format: YYYY-MM-DDThh:mm:ss[.mmm] / Send date'], tppId STRING['Identyfikator TPP / TPP ID']]]]]")
        checkSchema(rs, 'ItemInfoBase',
                "OBJECT[ItemInfoBase, 'Klasa bazowa informacji o elemencie (transakcji lub blokadzie) / Element (transaction or hold) Information Base Class', fields: [itemId STRING[required, 'ID elementu (transakcji lub blokadzie) nadany przez ASPSP / Element (transaction or hold) ID (ASPSP)'], amount STRING[required, 'Kwota transakcji / Amount of the transaction'], currency STRING['Kod ISO waluty transakcji / Currency (ISO)'], description STRING['Tytuł transakcji / Description of the transaction'], transactionType STRING['Typ transakcji / Transaction type'], tradeDate DATE_TIME['Data operacji, YYYY-MM-DDThh:mm:ss[.mmm] / Date of the operation'], mcc STRING['Kod dla każdej transakcji/operacji wykonanej przy użyciu karty / A code of each transaction performed by card'], auxData MAP]]")
        def enums = collectEnums(rs)
        CheckUtils.checkList('enums', enums,
                ['ENUM[AcceptCharsetParam, \'UTF-8\', allowed: [utf-8]]',
                 'ENUM[AcceptEncodingParam, \'Gzip, deflate\', allowed: [gzip, deflate]]',
                 'ENUM[AccountHolderType, \'Rodzaj posiadacza rachunku: osoba fizyczna lub osoba prawna / Account holder type: individual person or corporation\', allowed: [individual, corporation]]',
                 'ENUM[AdditionalPayorIdType, \'Typ dodatkowego identyfikatora płatnika / Payor\'s additional identifier type\', allowed: [P, R, 1, 2], descriptions: [P:Pesel, R:Regon, 1:Dowód osobisty, 2:Paszport]]',
                 'ENUM[BundleStatus, \'Status paczki przelewów / Bundle of payments status\', allowed: [inProgress, cancelled, done, partiallyDone]]',
                 'ENUM[DayOffOffsetType, \'Rodzaj przesunięcia, który należy zastosować do wykonania przelewu w przypadku, gdy dzień wolny jest planowaną datą przelewu / Type of offset that should be used for transfer execution in case of day off being the planned date of transfer\', allowed: [before, after]]',
                 'ENUM[DeliveryMode, \'Tryb pilności / Urgency mode\', allowed: [ExpressD0, StandardD1]]',
                 'ENUM[DeliveryMode2, \'Tryb pilności / Urgency mode\', allowed: [ExpressD0, UrgentD1, StandardD2]]',
                 'ENUM[ExecutionMode, \'Tryb realizacji płatności. Nadrzędna informacja decydująca o tym w jakim trybie zostanie zrealizowana płatność. / Payment execution mode. The superior information deciding which mode is to be used to execute payment.\', allowed: [Immediate, FutureDated]]',
                 'ENUM[FundsAvailable, \'Status - Czy środki są dostępne / Status - are funds available\', allowed: [true, false]]',
                 'ENUM[HoldRequestType, \'Element filtru: transakcji / Filter element\', allowed: [DEBIT]]',
                 'ENUM[PaymentStatus, \'Słownik statusów płatności\', allowed: [submitted, cancelled, pending, done, rejected, scheduled]]',
                 'ENUM[PayorIdType, \'Typ identyfikatora płatnika / Payor ID type\', allowed: [N, P, R, 1, 2, 3], descriptions: [N:NIP, P:Pesel, R:Regon, 1:Dowód osobisty, 2:Paszport, 3:Inny]]',
                 'ENUM[PeriodType, \'Typ jednostki okresu czasu / The type of unit of time period\', allowed: [day, week, month]]',
                 'ENUM[RecurringPaymentStatus, \'Status płatności cyklicznej / Status of recurring payment\', allowed: [submitted, inProgress, cancelled, close]]',
                 'ENUM[ScopeGroupType, \'Type of consent\', allowed: [ais-accounts, ais, pis]]',
                 'ENUM[ScopeUsageLimit, \'Rodzaj limitu zgody / Type of limit of usages\', allowed: [single, multiple]]',
                 'ENUM[System, \'Droga jaką przelew ma być rozliczony / The way the transfer should be settled\', allowed: [Elixir, ExpressElixir, Sorbnet, BlueCash, Internal]]',
                 'ENUM[System2, \'Droga jaką przelew ma być rozliczony / The way the transfer should be settled\', allowed: [SEPA, InstantSEPA, Target]]',
                 'ENUM[System3, \'Droga jaką przelew ma być rozliczony / The way the transfer should be settled\', allowed: [Elixir, ExpressElixir]]',
                 'ENUM[System4, \'Droga jaką przelew ma być rozliczony / The way the transfer should be settled\', allowed: [Swift]]',
                 'ENUM[ThrottlingPolicy, \'Throttling policy\', allowed: [psd2Regulatory]]',
                 'ENUM[TransactionCategory, \'Kategoria transakcji uznanie/obciążenie / Transaction category (credit/debit)\', allowed: [CREDIT, DEBIT]]',
                 'ENUM[TypeOfPayment, \'Typ przelewów, które zostaną zlecone w ramach paczki / The type of transfers that will be initiated through the bundle\', allowed: [domestic, EEA, nonEEA, tax]]',
                ])

        !rs.securitySchemas.size() == 2
        def security = rs.securitySchemas[1]
        assert security.code == 'xs2a_auth_decoupled'
        assert security.type == 'oauth2'
        assert !security.oauth2Flows.empty
        assert security.oauth2Flows[0].type == 'accessCode'
        assert security.oauth2Flows[0].authorizationUrl == 'https://apiHost/auth/authorizeExt'
        assert security.oauth2Flows[0].tokenUrl == 'https://apiHost/v2_1_1.1/auth/v2_1_1.1/token'
        assert security.oauth2Flows[0].scopes.keySet() == ['ais-accounts', 'ais', 'pis'] as Set
        true
    }

    static boolean checkParam(RestApi rs, String code, String name, ParamLocation location, boolean required, def schema) {
        def paramDef = rs.getParameter(code)
        assert paramDef, "No parameter with code $code, existing parameters: ${rs.parameters.collect { it.code }}"
        assert paramDef.name == name, "Missmatch name for parameter $code"
        assert paramDef.location == location, "Missmatch location for parameter $code"
        assert paramDef.required == required
        if (schema instanceof DataTypeCode) {
            assert paramDef.dataType.typeCode == schema
        }
        if (schema instanceof EnumType) {
            assert paramDef.dataType instanceof EnumType
            assert schema.allowedValues == paramDef.enumType.allowedValues
        }
        true
    }

    static boolean checkSchema(RestApi rs, String name, String expected) {
        def schemaString = rs.getSchema(name).toString()
        assert schemaString == expected
        true
    }

    static List<String> collectEnums(RestApi rs) {
        rs.schemas.findAll { it.isEnum() }.collect { it.toString() }.sort()
    }
}
