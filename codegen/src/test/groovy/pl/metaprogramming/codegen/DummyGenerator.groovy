/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import pl.metaprogramming.codegen.java.JavaGenerator
import pl.metaprogramming.codegen.java.JavaGeneratorBuilder

class DummyGenerator extends JavaGeneratorBuilder<Model, TOC, DummyGenerator> {

    DummyGenerator() {
        super(new TOC())
        model = Model.empty("dummy")
    }

    static class TOC {
        static TypeOfCode<String> CLASS1 = new TypeOfCode<String>('Class1')
        static TypeOfCode<String> CLASS2 = new TypeOfCode<String>('Class2')
    }

    class Generator extends JavaGenerator<Model> {
        Generator() {
            super(DummyGenerator.this)
        }

        @Override
        void generate() {
            DummyGenerator.this.codeBuilders.values().each {
                register(it.typeOfCode, "dummy")
            }
            makeCodeModels()
        }
    }

    def addClass1() {
        typeOfCode(TOC.CLASS1)
                .className { it.fixed = 'Class1' }
                .packageName { it.root = 'pkg2' }
                .forceGeneration(true)
    }

    def addClass2() {
        typeOfCode(TOC.CLASS2)
                .className { it.fixed = 'Class2' }
                .packageName { it.root = 'pkg1' }
                .forceGeneration(true)
    }

    @Override
    pl.metaprogramming.codegen.Generator<Model> make() {
        return new Generator()
    }

    @Override
    void init() {
    }
}
