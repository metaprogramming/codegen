/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import spock.lang.Specification

class ConditionParserSpec extends Specification {

    def "should success"() {
        when:
        def parser = new ConditionParser(text)
        def condition = parser.parse()
        then:
        parser.tokens.toString() == expectedTokens.toString()
        condition.toString() == expectedCondition

        where:
        text                                   | expectedTokens                                                         | expectedCondition
        'C'                                    | ['C']                                                                  | 'C'
        'operationId[op-id-1|opId2]'           | ['operationId[op-id-1|opId2]']                                         | 'operationId[op-id-1|opId2]'
        'A1 OR B'                              | ['A1', 'OR', 'B']                                                      | 'OR[A1, B]'
        'A1 AND (B OR C)'                      | ['A1', 'AND', ['B', 'OR', 'C']]                                        | 'AND[A1, OR[B, C]]'
        '((A1 AND (((B) OR C))))'              | [[['A1', 'AND', [[['B'], 'OR', 'C']]]]]                                | 'AND[A1, OR[B, C]]'
        '(Y OR A AND (B OR x[a])) OR X'        | [['Y', 'OR', 'A', 'AND', ['B', 'OR', 'x[a]']], 'OR', 'X']              | 'OR[OR[Y, AND[A, OR[B, x[a]]]], X]'
        'x1 and x2 and x3 or x4 and x5 and x6' | ['x1', 'and', 'x2', 'and', 'x3', 'or', 'x4', 'and', 'x5', 'and', 'x6'] | 'OR[AND[x1, x2, x3], AND[x4, x5, x6]]'
    }

    def "should fail"() {
        when:
        new ConditionParser(text).parse()
        then:
        def e = thrown(IllegalArgumentException)
        e.message == "Invalid expression '$text' at $idx"
        where:
        text              | idx
        ''                | 1
        'OR'              | 1
        'OR AND OR'       | 1
        'A OR'            | 5
        'A OR AND'        | 6
        'A B'             | 3
        'A('              | 2
        'A)'              | 2
        'A AND ((B OR C)' | 16
        'A AND (B OR C))' | 15
    }
}
