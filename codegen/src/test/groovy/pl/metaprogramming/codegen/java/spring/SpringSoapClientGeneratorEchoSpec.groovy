/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.GeneratorSpecification
import pl.metaprogramming.codegen.java.PackageInfoCm
import pl.metaprogramming.codegen.java.formatter.PackageInfoFormatter
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.wsdl.WsdlApi
import pl.metaprogramming.model.wsdl.parser.XsTypeParser
import pl.metaprogramming.utils.CheckUtils
import pl.metaprogramming.utils.ClassShadow

import static pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode.LOCAL_DATE_ADAPTER
import static pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode.LOCAL_DATE_TIME_ADAPTER
import static pl.metaprogramming.codegen.java.spring.SpringCommonTypeOfCode.ENDPOINT_PROVIDER
import static pl.metaprogramming.utils.CheckUtils.checkList
import static pl.metaprogramming.utils.JavaCodeGenerationTestUtils.methodShadow

class SpringSoapClientGeneratorEchoSpec extends GeneratorSpecification {

    static final String MODULE_ECHO = 'echoService'

    def "check package-info"() {
        when:
        def packageInfoTask = tasks[MODULE_ECHO].find { it.destFilePath.endsWith('package-info.java') }
        then:
        packageInfoTask.codeModel instanceof PackageInfoCm
        packageInfoTask.formatter instanceof PackageInfoFormatter

        when:
        def body = packageInfoTask.formatter.format(packageInfoTask.codeModel)
        then:
        checkList('package-info.java body', body.readLines(), [
                "@${generatedAnnotationClass()}(\"pl.metaprogramming.codegen\")",
                '@javax.xml.bind.annotation.XmlSchema(namespace = "http://example.com/echo")',
                'package example.ports.out.soap.schema;',
        ])
    }

    @Override
    Codegen makeCodegen() {
        new Codegen()
                .generate(new SpringCommonsGenerator()) {
                    it.rootPackage('commons')
                    it.typeOfCode(LOCAL_DATE_ADAPTER, LOCAL_DATE_TIME_ADAPTER).forEach {
                        it.packageName.tail = "jaxb"
                    }
                    it.typeOfCode(ENDPOINT_PROVIDER) {
                        it.packageName.root = 'example.ws'
                    }
                }
                .generate(new SpringSoapClientGenerator()) {
                    it.model = loadApi()
                    it.rootPackage('example')
                    it.setNamespacePackage('http://example.com/echo', 'example.ports.out.soap.schema')
                    it.setNamespacePackage('http://example.com/echo1', 'example.ports.out.soap.schema.ns1')
                    it.setNamespacePackage('http://example.com/echo2', 'example.ports.out.soap.schema.ns2')
                }
    }

    @Override
    Map<String, List<ClassShadow>> makeClassesToCheck() {
        [(MODULE_COMMONS): classesToCheckForCommons(),
         (MODULE_ECHO)   : classesToCheckForEchoApi()]
    }

    @Override
    Map<String, List<String>> makeExpectedClasses() {
        [(MODULE_COMMONS): expectedClassesForCommons(),
         (MODULE_ECHO)   : expectedClassesForEchoApi()]
    }

    private WsdlApi loadApi() {
        WsdlApi.of('src/test/resources/wsdl/echo.wsdl') {
            it.serviceNameMapper = { name -> name.replace('PortService', '') }
            it.xsTypeParser = new XsTypeParser().set('double', DataType.DECIMAL)
        }.removeOperations { it.name == 'echoExtendedSkipped' }
    }

    List<ClassShadow> classesToCheckForCommons() {
        [new ClassShadow(
                name: 'example.EndpointProvider',
                annotations: CheckUtils.COMPONENT_ANNOTATIONS_LEGACY,
                classHeader: 'public class EndpointProvider',
                fields: ['@Value("${BASE_URL:http://localhost:8080}") private String baseUrl'],
                methods: [
                        methodShadow('getEndpoint', 'public String getEndpoint(String path)',
                                ['return baseUrl + path;'],
                        ),
                ],
                imports: [
                        'org.springframework.beans.factory.annotation.Value',
                        'org.springframework.stereotype.Component',
                ],
        ),
        ]
    }

    List<ClassShadow> classesToCheckForEchoApi() {
        [new ClassShadow(
                name: 'example.adapters.out.soap.EchoServiceClientImpl',
                annotations: CheckUtils.GEN_ANNOTATIONS,
                classHeader: 'public class EchoServiceClientImpl extends WebServiceGatewaySupport implements EchoServiceClient',
                methods: [
                        methodShadow('echo', 'public Message echo(Message request)',
                                ['return (Message) getWebServiceTemplate().marshalSendAndReceive(request);'],
                                ['@Override']
                        ),
                ],
                imports: [
                        'example.ports.out.soap.EchoServiceClient',
                        'example.ports.out.soap.schema.ExtendedType',
                        'example.ports.out.soap.schema.Message',
                        'org.springframework.ws.client.core.support.WebServiceGatewaySupport',
                ],
        ),
         new ClassShadow(
                 name: 'example.adapters.out.soap.EchoServiceClientConfiguration',
                 annotations: ['@RequiredArgsConstructor',
                               '@Configuration',
                               '@Generated("pl.metaprogramming.codegen")',
                 ],
                 classHeader: 'public class EchoServiceClientConfiguration',
                 fields: ['private final EndpointProvider endpointProvider'],
                 methods: [
                         methodShadow('createEchoServiceClient', 'public EchoServiceClient createEchoServiceClient()',
                                 ['Jaxb2Marshaller marshaller = new Jaxb2Marshaller();',
                                  'marshaller.setPackagesToScan("example.ports.out.soap.schema", "example.ports.out.soap.schema.ns1");',
                                  'EchoServiceClientImpl client = new EchoServiceClientImpl();',
                                  'client.setDefaultUri(endpointProvider.getEndpoint("/ws/echo"));',
                                  'client.setMarshaller(marshaller);',
                                  'client.setUnmarshaller(marshaller);',
                                  'return client;',
                                 ],
                                 ['@Bean']
                         ),
                 ],
                 imports: [
                         'example.ports.out.soap.EchoServiceClient',
                         'example.ws.EndpointProvider',
                         'lombok.RequiredArgsConstructor',
                         'org.springframework.context.annotation.Bean',
                         'org.springframework.context.annotation.Configuration',
                         'org.springframework.oxm.jaxb.Jaxb2Marshaller',
                 ],
         ),
         new ClassShadow(
                 name: 'example.ports.out.soap.schema.Message',
                 annotations: [
                         '@Accessors(chain=true)',
                         '@Data',
                         '@Generated("pl.metaprogramming.codegen")',
                         '@NoArgsConstructor',
                         '@XmlAccessorType(XmlAccessType.FIELD)',
                         '@XmlRootElement(name="message")',
                         '@XmlType(name="",propOrder={"stringField","restrictedField","nillableField","objectField","listStringField","nillableListField","listObjectField","enumField","doubleField","dateField","dateTimeField","uPerCaseField"})',
                 ],
                 fields: [
                         '@Nonnull @XmlElement(required = true) private ObjectType objectField',
                         '@Nonnull @XmlElement(required = true) private String restrictedField',
                         '@Nonnull @XmlElement(required = true) private String stringField',
                         '@Nullable @XmlElement(nillable = true) private List<String> nillableListField',
                         '@Nullable @XmlElement(required = true, nillable = true) private String nillableField',
                         '@Nullable private BigDecimal doubleField',
                         '@Nullable private EnumType enumField',
                         '@Nullable private List<ObjectType> listObjectField',
                         '@Nullable private List<String> listStringField',
                         '@Nullable @XmlJavaTypeAdapter(LocalDateAdapter.class) private LocalDate dateField',
                         '@Nullable @XmlJavaTypeAdapter(LocalDateTimeAdapter.class) private LocalDateTime dateTimeField',
                         '@Nullable @XmlElement(name = "UPerCaseField") private String uPerCaseField',
                 ],
                 imports: [
                         'java.math.BigDecimal',
                         'java.time.LocalDate',
                         'java.time.LocalDateTime',
                         'java.util.List',
                         'javax.annotation.Nonnull',
                         'javax.annotation.Nullable',
                         'javax.xml.bind.annotation.XmlAccessType',
                         'javax.xml.bind.annotation.XmlAccessorType',
                         'javax.xml.bind.annotation.XmlElement',
                         'javax.xml.bind.annotation.XmlRootElement',
                         'javax.xml.bind.annotation.XmlType',
                         'javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter',
                         'lombok.Data',
                         'lombok.NoArgsConstructor',
                         'lombok.experimental.Accessors',
                         'commons.jaxb.LocalDateAdapter',
                         'commons.jaxb.LocalDateTimeAdapter',
                 ],
         ),
         new ClassShadow(
                 name: 'example.ports.out.soap.schema.EnumType',
                 annotations: ['@Generated("pl.metaprogramming.codegen")',
                               '@XmlEnum',
                               '@XmlType(name="enumType")',
                 ],
                 classHeader: 'public enum EnumType implements EnumValue',
                 enums: ['LOWERCASE("lowercase")',
                         'V_0("0")',
                 ],
                 fields: ['@Getter private final String value'],
                 methods: [
                         methodShadow('EnumType', 'EnumType(String value)',
                                 ['this.value = value;'],
                         ),
//                         methodShadow('fromValue', 'public static EnumType fromValue(String value)',
//                                 ['return EnumValue.fromValue(value, EnumType.class);']
//                         ),
                 ],
                 imports: ['commons.EnumValue',
                           'javax.xml.bind.annotation.XmlEnum',
                           'javax.xml.bind.annotation.XmlEnumValue',
                           'javax.xml.bind.annotation.XmlType',
                           'lombok.Getter',
                 ],
         ),
        ]
    }

    List<String> expectedClassesForCommons() {
        ['commons.EnumValue',
         'commons.jaxb.LocalDateAdapter',
         'commons.jaxb.LocalDateTimeAdapter',
         'example.ws.EndpointProvider',
        ]
    }

    List<String> expectedClassesForEchoApi() {
        ['example.adapters.out.soap.EchoServiceClientImpl',
         'example.adapters.out.soap.EchoServiceClientConfiguration',
         'example.ports.out.soap.EchoServiceClient',
         'example.ports.out.soap.schema.EnumType',
         'example.ports.out.soap.schema.ExtendedType',
         'example.ports.out.soap.schema.Message',
         'example.ports.out.soap.schema.ObjectType',
         'example.ports.out.soap.schema.ns1.BaseRequest',
         'example.ports.out.soap.schema.ns1.package-info',
         'example.ports.out.soap.schema.package-info',
        ]
    }
}
