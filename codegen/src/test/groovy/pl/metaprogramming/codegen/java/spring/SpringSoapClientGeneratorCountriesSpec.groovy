/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.GeneratorSpecification
import pl.metaprogramming.codegen.java.PackageInfoCm
import pl.metaprogramming.codegen.java.formatter.PackageInfoFormatter
import pl.metaprogramming.model.wsdl.WsdlApi
import pl.metaprogramming.utils.CheckUtils
import pl.metaprogramming.utils.ClassShadow

import static pl.metaprogramming.utils.CheckUtils.checkList
import static pl.metaprogramming.utils.JavaCodeGenerationTestUtils.methodShadow

class SpringSoapClientGeneratorCountriesSpec extends GeneratorSpecification {

    static final String MODULE_COUNTRIES = 'Countries'

    def "check package-info"() {
        when:
        def packageInfoTask = tasks[MODULE_COUNTRIES].find { it.destFilePath.endsWith('package-info.java') }
        then:
        packageInfoTask.codeModel instanceof PackageInfoCm
        packageInfoTask.formatter instanceof PackageInfoFormatter

        when:
        def body = packageInfoTask.formatter.format(packageInfoTask.codeModel)
        then:
        checkList('package-info.java body', body.readLines(), [
                "@${generatedAnnotationClass()}(\"pl.metaprogramming.codegen\")",
                '@javax.xml.bind.annotation.XmlSchema(namespace = "http://spring.io/guides/gs-producing-web-service", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)',
                'package example.ports.out.soap.ns1;',
        ])
    }

    @Override
    Codegen makeCodegen() {
        new Codegen()
                .generate(new SpringCommonsGenerator()) {
                    it.rootPackage("commons")
                    it.codeBuilders.remove(SpringCommonsGenerator.TOC.ENDPOINT_PROVIDER)
                }
                .generate(new SpringSoapClientGenerator()) {
                    it.model = loadApi()
                    it.rootPackage('example')
                    it.setNamespacePackage('http://spring.io/guides/gs-producing-web-service', 'example.ports.out.soap.ns1')
                }
    }

    @Override
    Map<String, List<ClassShadow>> makeClassesToCheck() {
        [(MODULE_COUNTRIES): classesToCheckForCountiesApi()]
    }

    @Override
    Map<String, List<String>> makeExpectedClasses() {
        [(MODULE_COMMONS)  : expectedClassesForCommons(),
         (MODULE_COUNTRIES): expectedClassesForCountriesApi()]
    }

    private WsdlApi loadApi() {
        WsdlApi.of('src/test/resources/wsdl/countries.wsdl') {
            it.serviceNameMapper = { name -> name.replace('PortService', '') }
        }
    }

    List<ClassShadow> classesToCheckForCountiesApi() {
        [new ClassShadow(
                name: 'example.adapters.out.soap.CountriesClientImpl',
                annotations: CheckUtils.GEN_ANNOTATIONS,
                classHeader: 'public class CountriesClientImpl extends WebServiceGatewaySupport implements CountriesClient',
                methods: [
                        methodShadow('getCountry', 'public GetCountryResponse getCountry(GetCountryRequest request)',
                                ['return (GetCountryResponse) getWebServiceTemplate().marshalSendAndReceive(request);'],
                                ['@Override']
                        ),
                ],
                imports: [
                        'example.ports.out.soap.CountriesClient',
                        'example.ports.out.soap.ns1.GetCountryRequest',
                        'example.ports.out.soap.ns1.GetCountryResponse',
                        'org.springframework.ws.client.core.support.WebServiceGatewaySupport',
                ],
        ),
         new ClassShadow(
                 name: 'example.adapters.out.soap.CountriesClientConfiguration',
                 annotations: CheckUtils.GEN_ANNOTATIONS + ['@Configuration'],
                 classHeader: 'public class CountriesClientConfiguration',
                 methods: [
                         methodShadow('createCountriesClient', 'public CountriesClient createCountriesClient()',
                                 ['Jaxb2Marshaller marshaller = new Jaxb2Marshaller();',
                                  'marshaller.setPackagesToScan("example.ports.out.soap.ns1");',
                                  'CountriesClientImpl client = new CountriesClientImpl();',
                                  'client.setDefaultUri("http://localhost:8080/ws");',
                                  'client.setMarshaller(marshaller);',
                                  'client.setUnmarshaller(marshaller);',
                                  'return client;',
                                 ],
                                 ['@Bean']
                         ),
                 ],
                 imports: [
                         'example.ports.out.soap.CountriesClient',
                         'org.springframework.context.annotation.Bean',
                         'org.springframework.context.annotation.Configuration',
                         'org.springframework.oxm.jaxb.Jaxb2Marshaller',
                 ],
         ),
         new ClassShadow(
                 name: 'example.ports.out.soap.ns1.Currency',
                 annotations: CheckUtils.GEN_ANNOTATIONS + [
                         '@XmlType(name="currency")',
                         '@XmlEnum'],
                 classHeader: 'public enum Currency implements EnumValue',
                 enums: ['GBP("GBP")',
                         'EUR("EUR")',
                         'PLN("PLN")',
                 ],
                 fields: ['@Getter private final String value'],
                 methods: [
                         methodShadow('Currency', 'Currency(String value)',
                                 ['this.value = value;'],
                         ),
//                         methodShadow('fromValue', 'public static Currency fromValue(String value)',
//                                 ['return EnumValue.fromValue(value, Currency.class);'],
//                         ),
                 ],
                 imports: ['commons.EnumValue',
                           'javax.xml.bind.annotation.XmlEnum',
                           'javax.xml.bind.annotation.XmlType',
                           'lombok.Getter',
                 ],
         ),
         new ClassShadow(
                 name: 'example.ports.out.soap.ns1.Country',
                 classHeader: 'public class Country',
                 annotations: [
                         '@Accessors(chain=true)',
                         '@Data',
                         '@Generated("pl.metaprogramming.codegen")',
                         '@NoArgsConstructor',
                         '@XmlAccessorType(XmlAccessType.FIELD)',
                         '@XmlType(name="country",propOrder={"name","population","capital","currency"})',
                 ],
                 fields: [
                         '@Nonnull @XmlElement(required = true) private Currency currency',
                         '@Nonnull @XmlElement(required = true) private Integer population',
                         '@Nonnull @XmlElement(required = true) private String capital',
                         '@Nonnull @XmlElement(required = true) private String name',
                 ],
                 imports: [
                         'javax.annotation.Nonnull',
                         'javax.xml.bind.annotation.XmlAccessType',
                         'javax.xml.bind.annotation.XmlAccessorType',
                         'javax.xml.bind.annotation.XmlElement',
                         'javax.xml.bind.annotation.XmlType',
                         'lombok.Data',
                         'lombok.NoArgsConstructor',
                         'lombok.experimental.Accessors',
                 ],
         ),
         new ClassShadow(
                 name: 'example.ports.out.soap.ns1.GetCountryRequest',
                 annotations: [
                         '@Accessors(chain=true)',
                         '@Data',
                         '@Generated("pl.metaprogramming.codegen")',
                         '@NoArgsConstructor',
                         '@XmlAccessorType(XmlAccessType.FIELD)',
                         '@XmlType(name="",propOrder={"name"})',
                         '@XmlRootElement(name="getCountryRequest")'
                 ],
                 fields: [
                         '@Nonnull @XmlElement(required = true) private String name',
                 ],
                 imports: [
                         'javax.annotation.Nonnull',
                         'javax.xml.bind.annotation.XmlAccessType',
                         'javax.xml.bind.annotation.XmlAccessorType',
                         'javax.xml.bind.annotation.XmlElement',
                         'javax.xml.bind.annotation.XmlRootElement',
                         'javax.xml.bind.annotation.XmlType',
                         'lombok.Data',
                         'lombok.NoArgsConstructor',
                         'lombok.experimental.Accessors',
                 ],
         ),
         new ClassShadow(
                 name: 'example.ports.out.soap.ns1.GetCountryResponse',
                 annotations: [
                         '@Accessors(chain=true)',
                         '@Data',
                         '@Generated("pl.metaprogramming.codegen")',
                         '@NoArgsConstructor',
                         '@XmlAccessorType(XmlAccessType.FIELD)',
                         '@XmlType(name="",propOrder={"country"})',
                         '@XmlRootElement(name="getCountryResponse")'
                 ],
                 fields: [
                         '@Nonnull @XmlElement(required = true) private Country country',
                 ],
                 imports: [
                         'javax.annotation.Nonnull',
                         'javax.xml.bind.annotation.XmlAccessType',
                         'javax.xml.bind.annotation.XmlAccessorType',
                         'javax.xml.bind.annotation.XmlElement',
                         'javax.xml.bind.annotation.XmlRootElement',
                         'javax.xml.bind.annotation.XmlType',
                         'lombok.Data',
                         'lombok.NoArgsConstructor',
                         'lombok.experimental.Accessors',
                 ],
         ),
        ]
    }

    List<String> expectedClassesForCommons() {
        ['commons.EnumValue']
    }

    List<String> expectedClassesForCountriesApi() {
        ['example.adapters.out.soap.CountriesClientImpl',
         'example.adapters.out.soap.CountriesClientConfiguration',
         'example.ports.out.soap.CountriesClient',
         'example.ports.out.soap.ns1.Country',
         'example.ports.out.soap.ns1.Currency',
         'example.ports.out.soap.ns1.GetCountryRequest',
         'example.ports.out.soap.ns1.GetCountryResponse',
         'example.ports.out.soap.ns1.package-info',
        ]
    }
}