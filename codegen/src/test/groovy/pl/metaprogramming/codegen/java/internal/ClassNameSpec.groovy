/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.internal

import spock.lang.Specification

class ClassNameSpec extends Specification {

    def "should parse canonicalName: #canonicalName"() {
        when:
        def c = ClassName.of(canonicalName)
        then:
        c.name == className
        c.classPackage == pakageName

        where:
        canonicalName                | className    | pakageName
        "byte"                       | "byte"       | null
        "Test"                       | "Test"       | null
        "example.example"            | "example"    | "example"
        "example.Test"               | "Test"       | "example"
        "example.example.Test"       | "Test"       | "example.example"
        "Test.Inner"                 | "Test.Inner" | null
        "example.Test.Inner"         | "Test.Inner" | "example"
        "example.example.Test.Inner" | "Test.Inner" | "example.example"
    }
}
