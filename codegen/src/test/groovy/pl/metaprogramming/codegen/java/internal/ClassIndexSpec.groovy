/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.internal

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.TypeOfCodeWithNoModel
import pl.metaprogramming.codegen.internal.MainGeneratorSpec
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.CodeIndex
import pl.metaprogramming.codegen.java.DataTypeMapper
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BuildContext
import pl.metaprogramming.codegen.java.base.ClassCmBuildDirector
import spock.lang.Specification

import java.util.function.Predicate

class ClassIndexSpec extends Specification {

    def "should find first entry matching model criteria"() {
        given:
        def classType = new TypeOfCodeWithNoModel('X')
        def index = new CodeIndex()
        index.put(ClassCd.of('p.T1'), 'T1', classType)
        index.put(ClassCd.of('p.T2'), 'T2', classType)
        index.put(ClassCd.of('p.T3'), 'T3', classType)

        when:
        def t1 = first(index, classType) { it == 'T1' }
        then:
        t1.canonicalName == 'p.T1'

        when:
        def t3 = first(index, classType) { (it as String).endsWith('3') }
        then:
        t3.canonicalName == 'p.T3'
    }

    def "should print index"() {
        given:
        def classType = new TypeOfCode('X')
        def index = new CodeIndex()
        def clazz1 = ClassCd.of('p.T1')
        index.put(clazz1, 'T1', classType)
        def params = new CodegenParams()
        def dataTypeMapper = params.get(DataTypeMapper)
        def classContext = new BuildContext('T2-MODEL', classType, 'T2', 'p', params, false, index, dataTypeMapper, new MainGeneratorSpec.DummyGenerator())
        def clazz2 = classContext.classCm
        clazz2.used = true
        index.put(new ClassCmBuildDirector(classContext, []))
        index.putMappers(MethodCm.of("test", clazz1) {
            it.resultType = clazz2
        })

        when:
        index.printIndex()

        then:
        noExceptionThrown()
    }

    private ClassCd first(CodeIndex index, TypeOfCode<?> classType, Predicate<?> modelPredicate) {
        index.classIndex.values().find { it.typeOfCode == classType && modelPredicate.test(it.model) }?.clazz
    }

}
