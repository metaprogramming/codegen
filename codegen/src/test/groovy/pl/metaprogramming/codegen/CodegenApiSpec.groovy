/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.codegen.java.spring.SpringSoapParams
import spock.lang.Specification

class CodegenApiSpec extends Specification {

    def "params should be properly override"() {
        when:
        def codegen = new Codegen()
        codegen.params.get(JavaParams).alwaysGenerateEnumFromValueMethod = true
        codegen.params.get(JavaParams).generatedAnnotationClass = "javax.annotation.Generated"
        codegen.params.get(JavaParams).generatedAnnotationValue = "my_generation_tag"
        codegen.params.get(SpringSoapParams).setNamespacePackage("ns1", "pkg1")
        codegen.params.get(SpringSoapParams).setNamespacePackage("ns2", "pkg2")
        codegen.params.get(SpringRestParams).delegateToFacade = true

        codegen.generate(new SpringCommonsGenerator()) {
            it.params.get(JavaParams).generatedAnnotationValue = "my_common_generation_tag"
            it.params.get(SpringSoapParams).setNamespacePackage("ns1", "pkg2")
            it.params.get(SpringRestParams).controllerPerOperation = false
        }
        def commonsGenerator = codegen.cfg.modules[0] as SpringCommonsGenerator.Generator

        then:
        codegen.params.get(JavaParams).alwaysGenerateEnumFromValueMethod
        codegen.params.get(JavaParams).generatedAnnotationValue == "my_generation_tag"
        codegen.params.get(JavaParams).generatedAnnotationClass == "javax.annotation.Generated"
        codegen.params.get(SpringSoapParams).namespace2Package["ns1"] == "pkg1"
        codegen.params.get(SpringSoapParams).namespace2Package["ns2"] == "pkg2"
        codegen.params.get(SpringRestParams).delegateToFacade
        codegen.params.get(SpringRestParams).controllerPerOperation
        commonsGenerator.params.get(JavaParams).alwaysGenerateEnumFromValueMethod
        commonsGenerator.params.get(JavaParams).generatedAnnotationValue == "my_common_generation_tag"
        commonsGenerator.params.get(JavaParams).generatedAnnotationClass == "javax.annotation.Generated"
        commonsGenerator.params.get(SpringSoapParams).namespace2Package["ns1"] == "pkg2"
        commonsGenerator.params.get(SpringSoapParams).namespace2Package["ns2"] == "pkg2"
        commonsGenerator.params.get(SpringRestParams).delegateToFacade
        !commonsGenerator.params.get(SpringRestParams).controllerPerOperation
    }
}
