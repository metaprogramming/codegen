/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.internal

import org.yaml.snakeyaml.Yaml
import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.Generator
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.utils.CheckUtils
import spock.lang.Shared
import spock.lang.Specification

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.function.Consumer

import static pl.metaprogramming.codegen.internal.FileStatus.*

abstract class MainGeneratorSpecification extends Specification {

    String fixFileBody(String fileBody) {
        fileBody
    }

    def setup() {
        outDir.deleteDir()
        assert !outDir.exists()
        outDir.mkdirs()
    }

    CodegenConfig cfg

    String tmpDirPath = 'out/gentest'
    String indexFilename = 'generatedFiles.yaml'
    String class1FilePath = 'src/main/java/pkg2/Class1.java'
    String class1LowercaseFilePath = 'src/main/java/pkg2/class1.java'
    String class2FilePath = 'src/main/java/pkg1/Class2.java'
    String packageInfoPath = 'src/main/java/pkg1/package-info.java'
    @Shared String class1FileMd5 = HashBuilder.build(fixFileBody(CLASS1_FILE_BODY), true)
    @Shared String class1LowercaseFileMd5 = HashBuilder.build(fixFileBody(CLASS1_LOWERCASE_FILE_BODY), true)
    @Shared String class1V2FileMd5 = HashBuilder.build(fixFileBody(CLASS1_V2_FILE_BODY), true)
    @Shared String class2FileMd5 = HashBuilder.build(fixFileBody(CLASS2_FILE_BODY), true)
    @Shared String packageInfoMd5 = HashBuilder.build(fixFileBody(PACKAGE_INFO_BODY), true)

    File outDir = new File(tmpDirPath)
    File indexFile = new File(outDir, indexFilename)
    File class1File = new File(outDir, class1FilePath)
    File class1LowercaseFile = new File(outDir, class1LowercaseFilePath)
    File class2File = new File(outDir, class2FilePath)
    File packageInfoFile = new File(outDir, packageInfoPath)

    static def GENERATED_ANNOTATION = AnnotationCm.of('javax.annotation.Generated', ['value': ValueCm.escaped('pl.metaprogramming.codegen')])

    Codegen makeCodegen() {
        def codegen = new Codegen()
                .baseDir(outDir)
                .indexFile(indexFile)
                .lineSeparator('\n')
                .forceMode(false)
                .addLastGenerationTag(true)
        cfg = codegen.cfg
        cfg.dataTimeFormat = 'yyyy-MM-dd HH:mm:ss.SSS'
        cfg.storeAnyKindOfStatusesInIndexFile = true
        codegen
    }

    MainGenerator generate(Generator generator) {
        generate {
            it.generate(generator)
        }
    }

    MainGenerator generate(Consumer<Codegen> setup = {}) {
        def codegen = makeCodegen()
        setup.accept(codegen)
        def generator = new MainGenerator(codegen.cfg)
        generator.runAll()
        generator
    }

    boolean checkFile(File file, String fileBody) {
        assert file.exists()
        assert file.parentFile.list().contains(file.name)
        assert file.text == fixFileBody(fileBody)
        true
    }

    boolean checkGeneration(MainGenerator generator, List<Map> indexFiles) {
        CheckUtils.checkList('files paths', generator.fileIndex.keySet() as List, indexFiles.collect { it.path })
        indexFiles.each {
            def given = generator.fileIndex.get(it.path)
            assert given.status == it.status
        }
        checkFileIndex(generator.generationDate, generator.prevGenerationDate, indexFiles.findAll { it.status != ABANDONED })
        true
    }

    boolean checkFileIndex(String generationDate, String prevGenerationDate, List<Map> expectedFiles) {
        Map fileIndex = loadYaml(indexFile)
        assert fileIndex['lastGeneration'] == generationDate
        List<Map> givenFiles = (List<Map>) fileIndex['files']
        assert expectedFiles.size() == givenFiles.size()
        expectedFiles.eachWithIndex { Map expectedFile, int i ->
            if (!expectedFile.containsKey('lastUpdate')) {
                if ([CREATED, UPDATED].contains(expectedFile.status)) {
                    expectedFile.lastUpdate = generationDate
                } else {
                    expectedFile.lastUpdate = prevGenerationDate ?: 'unknown'
                }
            }
            assert checkFile(expectedFile, givenFiles[i])
        }
        true
    }

    boolean deleteIndexFile() {
        indexFile.delete()
        !indexFile.exists()
    }

    Map loadYaml(File file) {
        new Yaml().load(file.text)
    }

    void changeLastGenerationDate() {
        def formatter = DateTimeFormatter.ofPattern(cfg.dataTimeFormat)
        def newLastGeneration = LocalDateTime.now().minusDays(1).format(formatter)
        def text = indexFile.text
        def idx1 = text.indexOf("'") + 1
        String oldLastGeneration = text.substring(idx1, text.indexOf("'", idx1))
        overwriteFile(indexFile, text.replace(oldLastGeneration, newLastGeneration))
    }

    boolean checkFile(Map expectedFile, Map givenFile) {
        def expectedProperties = expectedFile.keySet()
        def givenProperties = givenFile.keySet()
        assert expectedProperties == givenProperties
        expectedProperties.each {
            assert givenFile[it] == '' + expectedFile[it]
        }
        true
    }

    void overwriteFile(File file, String content) {
        file.delete()
        file.createNewFile()
        file.write(content, 'UTF-8')
    }


    final static String CLASS1_FILE_BODY = '''package pkg2;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class Class1 {
}
'''

    final static String CLASS1_LOWERCASE_FILE_BODY = CLASS1_FILE_BODY.replace("class Class1", "class class1")

    final static String CLASS1_TO_SKIP_FILE_BODY = '''package pkg2;

public class Class1 {
    private String textProperty;
}
'''

    final static String CLASS1_V2_FILE_BODY = '''package pkg2;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class Class1 {

    private String textProperty;
}
'''

    final static String CLASS2_FILE_BODY = '''package pkg1;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class Class2 {
}
'''

    final static String PACKAGE_INFO_BODY = '''@javax.annotation.Generated("pl.metaprogramming.codegen")
package pkg1;'''
}
