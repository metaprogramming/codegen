/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.internal

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.DummyGenerator
import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.JavaParams

import static pl.metaprogramming.codegen.internal.FileStatus.CREATED

class SkipModuleGenerationSpec extends MainGeneratorSpecification {

    def "should generate codes only for module1"() {
        expect:
        outDir.isDirectory()
        !indexFile.exists()
        !class1File.exists()
        !class2File.exists()

        when:
        def generator = generate {
            it.generate(new DummyGenerator(),
                    makeModuleBuilder(makeParams(), 'pkg2', 'Class1'))
            it.generate(new DummyGenerator(),
                    makeModuleBuilder(makeParams(true), 'pkg1', 'Class2',))
        }

        then:
        outDir.isDirectory()
        indexFile.exists()
        !class2File.exists()
        checkFile class1File, CLASS1_FILE_BODY
        checkGeneration(generator, [
                [path: class1FilePath, md5: class1FileMd5, status: CREATED],
        ])
    }

    private def makeParams(boolean skipGeneration = false) {
        def params = new CodegenParams()
                .dryRun(skipGeneration)
                .set(new JavaParams()
                        .generatedAnnotationClass('javax.annotation.Generated'))
        params
    }

    def makeModuleBuilder(CodegenParams params, String packageName, String className) {
        (DummyGenerator it) -> {
            it.params = params
            it.typeOfCode(new TypeOfCode(className))
                    .className { it.fixed = className }
                    .packageName { it.root = packageName }
                    .forceGeneration(true)
        }
    }

}
