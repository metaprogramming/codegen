/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.internal

import org.eclipse.jdt.core.JavaCore
import org.eclipse.jdt.core.formatter.CodeFormatter
import org.eclipse.jdt.internal.formatter.DefaultCodeFormatter
import org.eclipse.jdt.internal.formatter.DefaultCodeFormatterOptions
import org.eclipse.jface.text.Document
import org.eclipse.jface.text.IDocument
import org.eclipse.text.edits.TextEdit
import org.jboss.forge.roaster.Roaster
import pl.metaprogramming.codegen.CodeDecorator
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.DummyGenerator
import pl.metaprogramming.codegen.java.JavaParams

class ExternalJavaCodeFormatterSpec extends MainGeneratorSpecification {

    @Override
    String fixFileBody(String fileBody) {
        fileBody.replace('\n', System.lineSeparator())
    }

    def "should format code with roaster"() {
        expect:
        outDir.isDirectory()
        !indexFile.exists()
        !class2File.exists()

        when:
        generate {
            it.generate(new DummyGenerator(), makeModuleBuilder(roasterCodeFormatter))
        }

        then:
        outDir.isDirectory()
        indexFile.exists()
        checkFile class2File, CLASS_FILE_BODY
    }

    def "should format code with eclipse jdt"() {
        expect:
        outDir.isDirectory()
        !indexFile.exists()
        !class2File.exists()

        when:
        generate {
            it.generate(new DummyGenerator(), makeModuleBuilder(eclipseJdtCodeFormatter))
        }

        then:
        outDir.isDirectory()
        indexFile.exists()
        checkFile class2File, CLASS_FILE_BODY
    }

    def getRoasterCodeFormatter() {
        new CodeDecorator() {
            String apply(String s) {
                def props = new Properties()
                props.put('org.eclipse.jdt.core.formatter.tabulation.char', 'space')
                Roaster.format(props, s)
            }
        }
    }

    def getEclipseJdtCodeFormatter() {
        new CodeDecorator() {
            String apply(String s) {
                def cfOptions = DefaultCodeFormatterOptions.getDefaultSettings()
                cfOptions.tab_char = DefaultCodeFormatterOptions.SPACE
                CodeFormatter cf = new DefaultCodeFormatter(
                        cfOptions,
                        [(JavaCore.COMPILER_SOURCE): "1.8"])
                TextEdit te = cf.format(CodeFormatter.K_UNKNOWN, s, 0,
                        s.length(), 0, null)
                IDocument dc = new Document(s)
                te.apply(dc)
                return dc.get()
            }
        }
    }

    def makeModuleBuilder(CodeDecorator formatter) {
        (DummyGenerator it) -> {
            it.params = new CodegenParams().set(
                    new JavaParams()
                            .generatedAnnotationClass('javax.annotation.Generated')
                            .addCodeDecorator(formatter))
            it.addClass2().onImplementation {
                it.methods.add('method') { it.implBody = 'int i1=1; i1+=1;' }
            }
        }
    }


    final static String CLASS_FILE_BODY = '''package pkg1;

import javax.annotation.Generated;

@Generated("pl.metaprogramming.codegen")
public class Class2 {

    public void method() {
        int i1 = 1;
        i1 += 1;
    }
}
'''

}
