/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.internal

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.DummyGenerator
import pl.metaprogramming.codegen.java.JavaParams

import static pl.metaprogramming.codegen.internal.FileStatus.CREATED

class LicenceHeaderSpec extends MainGeneratorSpecification {

    String fixFileBody(String fileBody) {
        LICENCE_HEADER + fileBody
    }

    def "should generate codes with licence header"() {
        expect:
        outDir.isDirectory()
        !indexFile.exists()
        !class2File.exists()

        when:
        def generator = generate {
            it.generate(new DummyGenerator(), makeModuleBuilder())
            it.lineSeparator = '\n'
        }

        then:
        outDir.isDirectory()
        indexFile.exists()
        checkFile class2File, CLASS2_FILE_BODY
        checkGeneration(generator, [
                [path: class2FilePath, md5: class2FileMd5, status: CREATED],
        ])
    }

    def makeModuleBuilder() {
        (DummyGenerator it) -> {
            it.params = new CodegenParams()
                    .set(JavaParams) {
                        it.licenceHeader = LICENCE_TEXT
                        it.generatedAnnotationClass = 'javax.annotation.Generated'
                    }
            it.addClass2()
        }
    }

    private static String LICENCE_TEXT = '''LICENCE line1
  
  LICENCE line3
'''.trim()

    private static String LICENCE_HEADER = '''/*
 * LICENCE line1
 *
 *   LICENCE line3
 */

'''
}
