package ${classCd.packageName};

import lombok.Getter;

import java.util.function.Function;

import ${JavaParams.getGeneratedAnnotationClass()};

@${JavaParams.getGeneratedAnnotationClass()?keep_after_last(".")}("${JavaParams.getGeneratedAnnotationValue()}")
public class ${classCd.className}<T, R> {
    @Getter private final String name;
    private final Function<T, R> valueGetter;

    public Field(String name, Function<T, R> valueGetter) {
        this.name = name;
        this.valueGetter = valueGetter;
    }

    public R getValue(T context) {
        return context != null ? valueGetter.apply(context) : null;
    }
}
