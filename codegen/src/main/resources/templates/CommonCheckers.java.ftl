<#assign cm = classCd
/>package ${cm.packageName};

${importDependencies()}

import java.util.*;
import java.util.function.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ${JavaParams.getGeneratedAnnotationClass()};

@${JavaParams.getGeneratedAnnotationClass()?keep_after_last(".")}("${JavaParams.getGeneratedAnnotationValue()}")
public class ${classCd.className} {

    // see https://www.debuggex.com/r/_G6Mvw1eoYJF2Bgf
    private static final String DATE_REGEX = "(?:[1-9]\\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)";
    private static final String TIME_REGEX = "(?:[01]\\d|2[0-3]):[0-5]\\d:[0-5]\\d(?:|.\\d+)(?:Z|[+-][01]\\d:[0-5]\\d)";
    private static final Pattern ISO8601_DATE_TIME_PATTERN = Pattern.compile(String.format("^%sT%s$", DATE_REGEX, TIME_REGEX));
    private static final Pattern ISO8601_DATE_PATTERN = Pattern.compile(String.format("^%s$", DATE_REGEX));

    public static final String ERR_CODE_IS_REQUIRED = "${ValidationParams.isRequiredErrorCode()}";
    public static final String ERR_CODE_IS_INVALID_DATA = "${ValidationParams.isInvalidDataErrorCode()}";
    public static final String ERR_CODE_IS_NOT_ALLOWED = "${ValidationParams.isNotAllowedErrorCode()}";
    public static final String ERR_CODE_IS_NOT_BASE64 = "${ValidationParams.isNotBase64ErrorCode()}";
    public static final String ERR_CODE_IS_NOT_BOOLEAN = "${ValidationParams.isNotBooleanErrorCode()}";
    public static final String ERR_CODE_IS_NOT_LONG = "${ValidationParams.isNotLongErrorCode()}";
    public static final String ERR_CODE_IS_NOT_INT = "${ValidationParams.isNotIntErrorCode()}";
    public static final String ERR_CODE_IS_NOT_FLOAT = "${ValidationParams.isNotFloatErrorCode()}";
    public static final String ERR_CODE_IS_NOT_DOUBLE = "${ValidationParams.isNotDoubleErrorCode()}";
    public static final String ERR_CODE_IS_NOT_NUMBER = "${ValidationParams.isNotNumberErrorCode()}";
    public static final String ERR_CODE_IS_NOT_DATE = "${ValidationParams.isNotDateErrorCode()}";
    public static final String ERR_CODE_IS_NOT_DATE_TIME = "${ValidationParams.isNotDateTimeErrorCode()}";
    public static final String ERR_CODE_IS_NOT_ALLOWED_VALUE = "${ValidationParams.isNotAllowedValueErrorCode()}";
    public static final String ERR_CODE_IS_TOO_SMALL = "${ValidationParams.isTooSmallErrorCode()}";
    public static final String ERR_CODE_IS_TOO_BIG = "${ValidationParams.isTooBigErrorCode()}";
    public static final String ERR_CODE_IS_TOO_SHORT = "${ValidationParams.isTooShortErrorCode()}";
    public static final String ERR_CODE_IS_TOO_LONG = "${ValidationParams.isTooLongErrorCode()}";
    public static final String ERR_CODE_IS_NOT_EQUAL = "${ValidationParams.isNotEqualErrorCode()}";
    public static final String ERR_CODE_IS_NOT_MATCH_PATTERN = "${ValidationParams.isNotMatchPatternErrorCode()}";
    public static final String ERR_CODE_HAS_TOO_FEW_ITEMS = "${ValidationParams.hasTooFewItemsErrorCode}";
    public static final String ERR_CODE_HAS_TOO_MANY_ITEMS = "${ValidationParams.hasTooManyItemsErrorCode}";
    public static final String ERR_CODE_HAS_DUPLICATED_ITEMS = "${ValidationParams.hasDuplicatedItemsErrorCode}";

    public interface DangerousConsumer<T> {
        @SuppressWarnings("java:S112")
        void accept(T value) throws Exception;
    }

    public static void writeError(ValidationContext<?> ctx, String message) {
        ctx.addError(null, message);
    }

    public static void writeError(ValidationContext<?> ctx, String message, String field, String code) {
        ctx.addError(ValidationError.builder().message(message).field(field).code(code).build());
    }

    public static boolean isNull(Object value) {
        return value == null;
    }

    public static boolean isNotNull(Object value) {
        return !isNull(value);
    }

    public static boolean isNoException(String value, DangerousConsumer<String> consumer) {
        try {
            consumer.accept(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static Checker<String> noException(DangerousConsumer<String> consumer, String dataType) {
        return noException(consumer, dataType, ERR_CODE_IS_INVALID_DATA);
    }

    public static Checker<String> noException(DangerousConsumer<String> consumer, String dataType, String code) {
        return ctx -> {
            if (!isNoException(ctx.getValue(), consumer)) {
                ctx.addError(code, String.format("%s is not %s", ctx.getPath(), dataType));
            }
        };
    }

    @SafeVarargs
    public static <T> Checker<List<T>> items(Checker<T>...checkers) {
        return ctx -> {
            for (int idx = 0; idx < ctx.getValue().size() && ctx.isValid(); idx++) {
                new ValidationContext<>(
                        ctx.getValue().get(idx),
                        String.format("%s[%d]", ctx.getPath() == null ? "" : ctx.getPath(), idx), ctx).check(checkers);
            }
        };
    }

    @SafeVarargs
    public static <T> Checker<Map<String,T>> mapValues(Checker<T>...checkers) {
        return ctx -> {
            Iterator<String> keys = ctx.getValue().keySet().iterator();
            while (keys.hasNext() && ctx.isValid()) {
                String key = keys.next();
                new ValidationContext<>(
                        ctx.getValue().get(key),
                        String.format("%s[%s]", ctx.getPath(), key), ctx).check(checkers);
            }
        };
    }

    public static Checker<String> allow(EnumValue...values) {
        return allow(Stream.of(values).map(EnumValue::getValue).collect(Collectors.toList()), ERR_CODE_IS_NOT_ALLOWED_VALUE);
    }

    public static Checker<String> allow(Collection<String> allowedValues) {
        return allow(allowedValues, ERR_CODE_IS_NOT_ALLOWED_VALUE);
    }

    public static Checker<String> allow(Collection<String> allowedValues, String code) {
        return ctx -> {
            if (!allowedValues.contains(ctx.getValue())) {
                ctx.addError(code, ctx.getPath() + " should have one of values: " + allowedValues);
            }
        };
    }

    public static Checker<String> allow(Collection<String> allowedValues, UnaryOperator<String> preprocessor, String code) {
        return ctx -> {
            if (!allowedValues.contains(preprocessor.apply(ctx.getValue()))) {
                ctx.addError(code, ctx.getPath() + " should have one of values: " + allowedValues);
            }
        };
    }

    public static Checker<String> matches(Pattern pattern) {
        return matches(pattern, ERR_CODE_IS_NOT_MATCH_PATTERN);
    }

    public static Checker<String> matches(Pattern pattern, String code) {
        return matches(pattern, code, "should match pattern: " + pattern.pattern());
    }

    public static Checker<String> matches(Pattern pattern, String code, String message) {
        return ctx -> {
            if (!pattern.matcher(ctx.getValue()).matches()) {
                ctx.addError(code, ctx.getPath() + " " + message);
            }
        };
    }

    public static <T> Checker<List<T>> minItems(Integer minSize) {
        return ctx -> {
            if (minSize > ctx.getValue().size()) {
                ctx.addError(ERR_CODE_HAS_TOO_FEW_ITEMS, ctx.getPath() + " has not enough elements, min allowed size is " + minSize, minSize);
            }
        };
    }

    public static <T> Checker<List<T>> maxItems(Integer maxSize) {
        return ctx -> {
            if (maxSize < ctx.getValue().size()) {
                ctx.addError(ERR_CODE_HAS_TOO_MANY_ITEMS, ctx.getPath() + " has too many elements, max allowed size is " + maxSize, maxSize);
            }
        };
    }

    public static <T> Checker<List<T>> unique() {
        return ctx -> {
            if (!ctx.getValue().stream().allMatch(new HashSet<>()::add)) {
                ctx.addError(ERR_CODE_HAS_DUPLICATED_ITEMS, ctx.getPath() + " has duplicated elements");
            }
        };
    }

    public static Checker<String> minLength(Integer minLength) {
        return ctx -> {
            if (minLength > ctx.getValue().length()) {
                ctx.addError(ERR_CODE_IS_TOO_SHORT, ctx.getPath() + " has too short value, min allowed length is " + minLength, minLength);
            }
        };
    }

    public static Checker<String> maxLength(Integer maxLength) {
        return ctx -> {
            if (maxLength < ctx.getValue().length()) {
                ctx.addError(ERR_CODE_IS_TOO_LONG, ctx.getPath() + " has too long value, max allowed length is " + maxLength, maxLength);
            }
        };
    }

    public static <T extends Comparable<T>> Checker<T> gt(T value) {
        return ctx -> compare(c -> c > 0, ">", value, ctx, ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable<T>> Checker<T> ge(T value) {
        return ctx -> compare(c -> c >= 0, ">=", value, ctx, ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable<T>> Checker<T> lt(T value) {
        return ctx -> compare(c -> c < 0, "<", value, ctx, ERR_CODE_IS_TOO_BIG);
    }

    public static <T extends Comparable<T>> Checker<T> le(T value) {
        return ctx -> compare(c -> c <= 0, "<=", value, ctx, ERR_CODE_IS_TOO_BIG);
    }

    private static <T extends Comparable<T>> void compare(IntPredicate expected, String operator, T value, ValidationContext<T> ctx, String errorCode) {
        if (!expected.test(ctx.getValue().compareTo(value))) {
            ctx.addError(errorCode, String.format("%s should be %s %s", ctx.getPath(), operator, value), value);
        }
    }

    public static <T extends Comparable<T>> Checker<String> gt(String value, Function<String, T> mapper) {
        return ctx -> compare(c -> c > 0, ">", value, mapper, ctx, ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable<T>> Checker<String> ge(String value, Function<String, T> mapper) {
        return ctx -> compare(c -> c >= 0, ">=", value, mapper, ctx, ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable<T>> Checker<String> lt(String value, Function<String, T> mapper) {
        return ctx -> compare(c -> c < 0, "<", value, mapper, ctx, ERR_CODE_IS_TOO_BIG);
    }

    public static <T extends Comparable<T>> Checker<String> le(String value, Function<String, T> mapper) {
        return ctx -> compare(c -> c <= 0, "<=", value, mapper, ctx, ERR_CODE_IS_TOO_BIG);
    }

    private static <T extends Comparable<T>> void compare(IntPredicate expected, String operator, String value, Function<String, T> mapper, ValidationContext<String> ctx, String errorCode) {
        if (!expected.test(mapper.apply(ctx.getValue()).compareTo(mapper.apply(value)))) {
            ctx.addError(errorCode, String.format("%s should be %s %s", ctx.getPath(), operator, value), value);
        }
    }

    public static <T extends Comparable<T>, P> Checker<P> gt(Field<P, String> field1, Field<P, String> field2, Function<String, T> mapper) {
        return compare(field1, field2, mapper, c -> c > 0, "%s should be greater than %s", ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable<T>, P> Checker<P> ge(Field<P, String> field1, Field<P, String> field2, Function<String, T> mapper) {
        return compare(field1, field2, mapper, c -> c >= 0, "%s should be greater equal than %s", ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable<T>, P> Checker<P> lt(Field<P, String> field1, Field<P, String> field2, Function<String, T> mapper) {
        return compare(field1, field2, mapper, c -> c < 0, "%s should be less than %s", ERR_CODE_IS_TOO_BIG);
    }

    public static <T extends Comparable<T>, P> Checker<P> le(Field<P, String> field1, Field<P, String> field2, Function<String, T> mapper) {
        return compare(field1, field2, mapper, c -> c <= 0, "%s should be less equal than %s", ERR_CODE_IS_TOO_BIG);
    }

    public static <T extends Comparable<T>, P> Checker<P> eq(Field<P, String> field1, Field<P, String> field2, Function<String, T> mapper) {
        return compare(field1, field2, mapper, c -> c == 0, "%s should be the same as %s", ERR_CODE_IS_NOT_EQUAL);
    }

    public static <T extends Comparable<T>, P> Checker<P> compare(
            Field<P, String> field1,
            Field<P, String> field2,
            Function<String, T> mapper,
            IntPredicate isComparisonValid,
            String messageTemplate,
            String code
    ) {
        return ctx -> {
            String value1 = field1.getValue(ctx.getValue());
            String value2 = field2.getValue(ctx.getValue());
            if (value1 != null && value2 != null && !isComparisonValid.test(mapper.apply(value1).compareTo(mapper.apply(value2)))) {
                ctx.addError(ValidationError.builder()
                        .field(ctx.getFieldPath(field1))
                        .code(code)
                        .message(String.format(messageTemplate, field1.getName(), field2.getName()))
                        .messageArgs(new Object[]{field1.getName(), field2.getName()}).build());
            }
        };
    }


    public static <T extends Comparable, P> Checker<P> gt(Field<P, T> field1, Field<P, T> field2) {
        return compare(field1, field2, c -> c > 0, "%s should be greater than %s", ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable, P> Checker<P> ge(Field<P, T> field1, Field<P, T> field2) {
        return compare(field1, field2, c -> c >= 0, "%s should be greater equal than %s", ERR_CODE_IS_TOO_SMALL);
    }

    public static <T extends Comparable, P> Checker<P> lt(Field<P, T> field1, Field<P, T> field2) {
        return compare(field1, field2, c -> c < 0, "%s should be less than %s", ERR_CODE_IS_TOO_BIG);
    }

    public static <T extends Comparable, P> Checker<P> le(Field<P, T> field1, Field<P, T> field2) {
        return compare(field1, field2, c -> c <= 0, "%s should be less equal than %s", ERR_CODE_IS_TOO_BIG);
    }

    public static <T extends Comparable, P> Checker<P> eq(Field<P, T> field1, Field<P, T> field2) {
        return compare(field1, field2, c -> c == 0, "%s should be the same as %s", ERR_CODE_IS_NOT_EQUAL);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Comparable, P> Checker<P> compare(
            Field<P, T> field1,
            Field<P, T> field2,
            IntPredicate isComparisonValid,
            String messageTemplate,
            String code
    ) {
        return ctx -> {
            T value1 = field1.getValue(ctx.getValue());
            T value2 = field2.getValue(ctx.getValue());
            if (value1 != null && value2 != null && !isComparisonValid.test(value1.compareTo(value2))) {
                ctx.addError(ValidationError.builder()
                        .field(ctx.getFieldPath(field1))
                        .code(code)
                        .message(String.format(messageTemplate, field1.getName(), field2.getName()))
                        .messageArgs(new Object[]{field1.getName(), field2.getName()}).build());
            }
        };
    }


    @SuppressWarnings("unchecked")
    public static <T> Checker<T> required() {
        return (Checker<T>) REQUIRED_CHECKER;
    }

    @SuppressWarnings("unchecked")
    public static <T> Checker<T> notAllowed() {
        return (Checker<T>) NOT_ALLOWED_CHECKER;
    }

    private static final Checker<Object> REQUIRED_CHECKER = new Checker<Object>() {
        @Override
        public boolean checkNull() {
            return true;
        }
        @Override
        public void check(ValidationContext<Object> ctx) {
            if (isNull(ctx.getValue())) {
                writeError(ctx, ctx.getPath() + " is required", ctx.getPath(), ERR_CODE_IS_REQUIRED);
            }
        }
    };

    private static final Checker<Object> NOT_ALLOWED_CHECKER = ctx -> {
        Object val = ctx.getValue();
        if (isNotNull(val) && !(val instanceof Collection && ((Collection) val).isEmpty())) {
            writeError(ctx, ctx.getPath() + " is not allowed", ctx.getPath(), ERR_CODE_IS_NOT_ALLOWED);
        }
    };

    public static final Checker<String> BOOLEAN = allow(Arrays.asList("true", "false"), String::toLowerCase, ERR_CODE_IS_NOT_BOOLEAN);

    public static final Checker<String> INT32 = noException(Integer::parseInt, "32bit integer", ERR_CODE_IS_NOT_INT);

    public static final Checker<String> INT64 = noException(Long::parseLong, "64bit integer", ERR_CODE_IS_NOT_LONG);

    public static final Checker<String> FLOAT = noException(Float::parseFloat, "float", ERR_CODE_IS_NOT_FLOAT);

    public static final Checker<String> DOUBLE = noException(Double::parseDouble, "double", ERR_CODE_IS_NOT_DOUBLE);

    public static final Checker<String> ISO_DATE = matches(ISO8601_DATE_PATTERN, ERR_CODE_IS_NOT_DATE, "is not yyyy-MM-dd");

    public static final Checker<String> ISO_DATE_TIME = matches(ISO8601_DATE_TIME_PATTERN, ERR_CODE_IS_NOT_DATE_TIME, "should be valid date time in ISO8601 format");

    public static final Checker<String> BASE64 = noException(Base64.getDecoder()::decode, "base64", ERR_CODE_IS_NOT_BASE64);

    @SafeVarargs
    public static <T> Checker<T> authParamNonNull(Field<T, ?>... fields) {
        return ctx -> {
            if (Stream.of(fields).noneMatch(f -> Objects.nonNull(f.getValue(ctx.getValue())))) {
                ctx.setError(401);
            }
        };
    }

    public static <T, P> Checker<T> credentials(Function<ValidationContext<T>, Collection<P>> credentialsSupplier, Collection<P> requiredCredentials) {
        return ctx -> {
            try {
                Collection<?> credentials = credentialsSupplier.apply(ctx);
                if (!requiredCredentials.isEmpty() && requiredCredentials.stream().noneMatch(credentials::contains)) {
                    ctx.setError(403);
                }
            } catch (Exception e) {
                ctx.setError(401);
            }
        };
    }

    public static <T> ConditionalChecker<T> checkIf(Checker<T> checker, BoolExp condition) {
        return new ConditionalChecker<>(checker, condition);
    }

    public static BoolExp or(BoolExp... items) {
        return ctx -> Stream.of(items).anyMatch(i -> i.evaluate(ctx));
    }

    public static BoolExp and(BoolExp... items) {
        return ctx -> Stream.of(items).allMatch(i -> i.evaluate(ctx));
    }
}
