/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming

import org.slf4j.LoggerFactory
import java.util.*
import java.util.function.Consumer

internal val log = LoggerFactory.getLogger("pl.metaprogramming.codegen")!!

internal fun String.upperFirstChar() = // String.capitalize is deprecated
    this.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }

internal fun String.lowerFirstChar() = // String.decapitalize is deprecated
    this.replaceFirstChar { if (it.isUpperCase()) it.lowercase(Locale.getDefault()) else it.toString() }

internal fun getJavaVersion(): Int {
    var version = System.getProperty("java.version")
    if (version.startsWith("1.")) {
        version = version.substring(2, 3)
    } else {
        val dot = version.indexOf(".")
        if (dot != -1) {
            version = version.substring(0, dot)
        }
    }
    return Integer.parseInt(version)
}

internal class ValueCounter<T>() {
    private val counter: MutableMap<T, Int> = mutableMapOf()
    private var all: Int = 0

    constructor(items: Iterable<T>) : this() {
        items.forEach { i -> count(i) }
    }

    fun count(obj: T?) {
        if (obj != null) {
            counter[obj] = (counter[obj] ?: 0) + 1
            ++all
        }
    }

    val dominant: T?
        get() {
            counter.size
            for (v in counter) {
                if (v.value > (all / 2)) {
                    return v.key
                }
            }
            return null
        }

    val most: Collection<T>
        get() {
            val maxOften = counter.values.max()
            return counter.filterValues { it == maxOften }.keys
        }
}

internal class ObservableList<T>(private val wrapped: MutableList<T>, private val addListener: Consumer<T>): MutableList<T> by wrapped {
    override fun add(element: T): Boolean {
        if (wrapped.add(element)) {
            addListener.accept(element)
            return true
        }
        return false
    }
}

internal fun <T : Any> Map<*, *>.value(path: String): T? {
    val dotIdx = path.indexOf('.')
    if (dotIdx > 0) {
        val node = this[path.substring(0, dotIdx)] as Map<*, *>?
        return node?.value(path.substring(dotIdx + 1))
    }
    @Suppress("UNCHECKED_CAST")
    return this[path] as T?
}