/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import java.util.function.Consumer
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberFunctions
import kotlin.reflect.full.memberProperties

/**
 * Code generation parameters container.
 *
 * Parameters should be passed in an object of any class.
 * The param object should have a parameterless constructor.
 * Only one param object of a given class can be set.
 */
class CodegenParams {

    internal val paramsMap: MutableMap<Class<*>, Any> = mutableMapOf()

    /** Determines that module files will not be generated. */
    var dryRun = false

    fun dryRun(dryRun: Boolean) = apply { this.dryRun = dryRun }

    /**
     * Specifies whether to allow duplicate classes.
     * If not, the generation process will end with an error when a class with the same name and package is declared.
     * Otherwise, the class reported first will be generated and a warning will be logged.
     */
    var allowDuplicateClasses = false

    fun allowDuplicateClasses(allowDuplicateClasses: Boolean) = apply { this.allowDuplicateClasses = allowDuplicateClasses }

    /**
     * Retrieves an object with parameters of the given class.
     *
     * If the parameter of the given class has not been set before, then the object will be created (the class should have parameterless constructor).
     */
    @Suppress("UNCHECKED_CAST")
    operator fun <T : Any> get(clazz: Class<T>): T {
        if (!paramsMap.containsKey(clazz)) {
            paramsMap[clazz] = clazz.getConstructor().newInstance()
        }
        return (paramsMap[clazz] as T)
    }

    /**
     * Retrieves an object with parameters of the given class.
     *
     * If the parameter of the given class has not been set before, then the object will be created (the class should have parameterless constructor).
     */
    operator fun <T : Any> get(clazz: KClass<T>): T = get(clazz.java)

    /**
     * Set params object.
     * If an object of the same class was previously added, it will be overwritten.
     */
    fun <T : Any> set(params: T): CodegenParams = apply { paramsMap[params.javaClass] = params }

    /**
     * Set or update params object.
     *
     * If the parameter of the given class has not been set before, then the object will be created (the class should have parameterless constructor).
     */
    fun <T : Any> set(clazz: KClass<T>, updater: T.() -> Unit) = apply { updater.invoke(get(clazz)) }

    /**
     * Set or update params object.
     *
     * If the parameter of the given class has not been set before, then the object will be created (the class should have parameterless constructor).
     */
    fun <T : Any> set(clazz: Class<T>, updater: Consumer<T>) = apply { updater.accept(get(clazz)) }

    fun copy(): CodegenParams {
        val result = CodegenParams()
        result.dryRun = dryRun
        paramsMap.forEach {
            result.paramsMap[it.key] = it.value.copy()
        }
        return result
    }

    private fun Any.copy(): Any {
        val clazz = this::class
        val copyFun = clazz.memberFunctions.find {
            it.name == "copy" && it.parameters.size == 1 && it.returnType.classifier == clazz
        }
        if (copyFun != null) {
            return copyFun.call(this)!!
        }
        val result = clazz.java.getDeclaredConstructor().newInstance()
        clazz.memberProperties.forEach { prop ->
            try {
                if (prop is KMutableProperty<*>) {
                    val propVal = prop.getter.call(this)
                    prop.setter.call(result, propVal)
                }
            } catch (e: Exception) {
                // nothing to do
            }
        }
        return result
    }
}