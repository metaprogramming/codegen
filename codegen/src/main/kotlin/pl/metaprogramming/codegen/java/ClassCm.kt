/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.java.base.BuildContext
import pl.metaprogramming.codegen.java.base.ClassCmFields
import pl.metaprogramming.codegen.java.base.ClassCmMethods
import pl.metaprogramming.codegen.java.internal.AccessModifier
import pl.metaprogramming.codegen.java.internal.ClassName
import pl.metaprogramming.lowerFirstChar
import java.util.*

class ClassCm private constructor(name: ClassName, val context: BuildContext<*>) :
    ClassCd(name, forceGeneration = context.forceGeneration) {

    constructor(packageName: String?, className: String, context: BuildContext<*>)
            : this(ClassName(className, packageName), context)

    override var kind: ClassKind = ClassKind.CLASS
    override var genericParams: MutableList<ClassCd> = mutableListOf()
    override val packageName: String get() = super.packageName!!
    override var superClass: ClassCd? = null
        set(value) {
            field = value
            if (used) field?.markAsUsed()
        }
    val fields: ClassCmFields = ClassCmFields(this)
    val methods: ClassCmMethods = ClassCmMethods(context)
    val innerClasses: MutableList<ClassCm> = mutableListOf()
    val interfaces: MutableList<ClassCd> = mutableListOf()
    val enumItems: MutableList<EnumItemCm> = mutableListOf()
    val annotations: MutableList<AnnotationCm> = mutableListOf()
    var description: String? = null
    private val modifiers: MutableSet<ClassModifier> = EnumSet.noneOf(ClassModifier::class.java)
    var isAbstract: Boolean
        get() = modifiers.contains(ClassModifier.ABSTRACT)
        set(value) = modifiers.switch(ClassModifier.ABSTRACT, value)
    private var accessModifier: AccessModifier = AccessModifier.PUBLIC
    var isPublic: Boolean
        get() = accessModifier == AccessModifier.PUBLIC
        set(value) {
            accessModifier = if (!value) AccessModifier.PRIVATE else AccessModifier.PUBLIC
        }
    var isPrivate: Boolean
        get() = accessModifier == AccessModifier.PRIVATE
        set(value) {
            accessModifier = if (value) AccessModifier.PRIVATE else AccessModifier.PUBLIC
        }

    override fun markAsUsed() {
        if (!used) {
            super.markAsUsed()
            interfaces.forEach { it.markAsUsed() }
            methods.forEach {
                if (!it.mapper) {
                    it.markAsUsed()
                }
            }
            fields.forEach { it.markAsUsed() }
        }
    }

    fun getMethodsToGenerate() = methods.filter { it.used }
    fun addGenericParams(vararg genericParams: ClassCd) = apply { this.genericParams.addAll(genericParams) }

    fun implementationOf(implementedInterface: ClassCd) {
        interfaces.add(implementedInterface)
        if (used) implementedInterface.markAsUsed()
    }

    fun addAnnotation(annotation: AnnotationCm) = apply {
        annotations.add(annotation)
    }

    @JvmOverloads
    fun addEnumItem(item: String, sort: Boolean = false): EnumItemCm {
        check(isEnum) { "Class $className is not an enum" }
        val exists = enumItems.find { it.name == item }
        if (exists != null) {
            return exists
        }
        val itemCm = EnumItemCm(this, item)
        enumItems.add(itemCm)
        if (sort) enumItems.sortWith(compareBy { it.name })
        return itemCm
    }

    fun injectDependency(toInject: ClassCd, fieldName: String? = null): FieldCm {
        if (this == toInject) return toInject.asField("this")
        val existField = fields.find { it.type == toInject }
        if (existField != null) return existField
        val name = fieldName ?: if (toInject.isInterface && toInject.className.startsWith('I'))
            toInject.className.substring(1).lowerFirstChar()
        else toInject.className.lowerFirstChar()
        return fields.add(name, toInject)
    }

    val dependencies: Dependencies
        get() {
            val dependencies = Dependencies()
            annotations.forEach { it.collectDependencies(dependencies) }
            superClass?.collectDependencies(dependencies)
            interfaces.forEach { it.collectDependencies(dependencies) }
            fields.forEach { it.collectDependencies(dependencies) }
            enumItems.forEach { it.collectDependencies(dependencies) }
            getMethodsToGenerate().forEach { it.collectDependencies(dependencies) }
            innerClasses.forEach {
                it.dependencies.collectDependencies(dependencies)
            }
            return dependencies
        }
}

private enum class ClassModifier {
    ABSTRACT
}
