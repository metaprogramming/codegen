/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.DataTypeMapper
import pl.metaprogramming.codegen.java.CodeIndex
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.model.data.ArrayType
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.MapType
import java.util.*

class ClassLocator(
    private val codeIndex: CodeIndex,
    private val dataTypeMapper: DataTypeMapper,
    private val classType: Any,
    private var model: Any?
) {
    private var forceDeclaration: Boolean = false

    fun model(model: Any?) = apply { this.model = model }

    fun find(): ClassCd? = when {
        classType is ClassCd -> classType
        classType is DataType -> dataTypeMapper[classType]
        model is DataType -> getClassForDataType(model as DataType)
        else -> getClass(model)
    }

    fun get(): ClassCd = checkNotNull(find()) { "Can't find Class ($classType) for $model" }

    fun getDeclared(): ClassCm {
        forceDeclaration = true
        return get() as ClassCm
    }

    fun getOptional() = Optional.ofNullable(find())

    fun getGeneric(genericParams: List<Any>): ClassCd = get().withGeneric(genericParams.map { clone(it).get() })

    fun getGeneric(vararg genericParams: Any): ClassCd = getGeneric(genericParams.toList())

    private fun clone(classType: Any) = ClassLocator(codeIndex, dataTypeMapper, classType, model)
    private fun getClassForDataType(dataType: DataType): ClassCd =
        dataType.getClassByTypeOfCode() ?: when (dataType) {
            is ArrayType -> getClassForDataType(dataType.itemsSchema.dataType).asList()
            is MapType -> dataType.valuesSchema
                .let { if (it.isAnyObject) Java.objectType() else getClassForDataType(it.dataType) }
                .asMapBy(Java.string())

            else -> checkNotNull(getClass(dataType)) { "Can't find class for $dataType (for type of code: $classType)" }
        }

    private fun DataType.getClassByTypeOfCode(): ClassCd? =
        if (classType is TypeOfCode<*>) dataTypeMapper[this, classType] else null

    private fun getClass(model: Any?): ClassCd? =
        codeIndex.getClass(classType, model, forceDeclaration)

}