/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

private val OPERATIONS = ConditionOperation.entries.map { it.name }.toSet()

class ConditionParser(val text: String) {
    private val tokens = mutableListOf<Any>()
    private val levels = ArrayDeque<MutableList<Any>>()
    private var tokenIdx: Int? = null

    init {
        levels.add(tokens)
    }

    fun parse(): Condition {
        text.forEachIndexed { idx, c ->
            when {
                c.isOpenBracket() -> {
                    check(tokenIdx == null, idx)
                    addNextLevel(idx)
                }

                c.isCloseBracket() -> {
                    check(levels.size > 1, idx)
                    putToken(idx)
                    levels.removeLast()
                }

                c.isWhitespace() -> putToken(idx)

                tokenIdx == null -> tokenIdx = idx
            }
        }
        putToken(text.length)
        check(levels.size == 1, text.length)
        check(tokens.isNotEmpty(), text.length)
        check(!tokens.last().isOperation(), text.length)
        return toCondition(flattenList(tokens))
    }

    private fun putToken(endIdx: Int) {
        tokenIdx?.let {
            val token = text.substring(it, endIdx)
            checkAddToken(token, it)
            levels.last().add(token)
        }
        tokenIdx = null
    }

    private fun addNextLevel(textIdx: Int) {
        val level = mutableListOf<Any>()
        checkAddToken(level, textIdx)
        levels.last().add(level)
        levels.add(level)
    }

    private fun checkAddToken(value: Any, textIdx: Int) {
        val prev = if (levels.last().isEmpty()) null else levels.last().last()
        if (value.isOperation()) {
            check(prev != null && !prev.isOperation(), textIdx)
        } else {
            check(prev == null || prev.isOperation(), textIdx)
        }
    }

    private fun check(condition: Boolean, textIdx: Int) {
        if (condition) return
        throw IllegalArgumentException("Invalid expression '$text' at ${textIdx + 1}")
    }

    private fun Char.isOpenBracket() = this == '('
    private fun Char.isCloseBracket() = this == ')'
    private fun Any.isOperation() = this is String && OPERATIONS.contains(this.uppercase())

    private fun toCondition(item: Any): Condition {
        if (item is String) return ConditionItem(item)
        @Suppress("UNCHECKED_CAST")
        val items = flattenList(item as List<Any>)
        return if (items.size == 1) toCondition(items[0]) else toExpression(items)
    }

    private fun toExpression(tokens: List<Any>): Condition {
        val orItems = mutableListOf<Condition>()
        val andItems = mutableListOf<Condition>()
        var operator = false
        tokens.forEach { token ->
            if (!operator) {
                andItems.add(toCondition(token))
            } else if ((token as String).uppercase() == "OR") {
                orItems.add(andItems.toAndOperation())
                andItems.clear()
            }
            operator = !operator
        }
        orItems.add(andItems.toAndOperation())
        return orItems.toOperation(ConditionOperation.OR)
    }

    @Suppress("UNCHECKED_CAST")
    private fun flattenList(tokens: List<Any>): List<Any> =
        if (tokens.size == 1 && tokens[0] is List<*>) flattenList(tokens[0] as List<Any>) else tokens

    private fun List<Condition>.toAndOperation() = this.toOperation(ConditionOperation.AND)

    private fun List<Condition>.toOperation(operation: ConditionOperation) =
        if (this.size == 1) this[0] else ConditionExpression(operation, this.toList())

}
