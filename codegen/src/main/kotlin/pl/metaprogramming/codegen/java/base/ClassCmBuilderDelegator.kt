/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

class ClassCmBuilderDelegator<M>(private val builderSupplier: () -> IClassCmBuilder<M>) : IClassCmBuilder<M> {
    private var _priority: Int? = null
    override var priority: Int
        set(value) {
            _priority = value
        }
        get() = _priority ?: (builderSupplier.invoke().priority)

    override fun makeDeclaration(context: BuildContext<M>) {
        builderSupplier.invoke().makeDeclaration(context)
    }

    override fun makeImplementation(context: BuildContext<M>) {
        builderSupplier.invoke().makeImplementation(context)
    }

    override fun makeDecoration(context: BuildContext<M>) {
        builderSupplier.invoke().makeDecoration(context)
    }
}