/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.NEW_LINE
import pl.metaprogramming.codegen.CodeDecorator

class LicenceDecorator(private val licence: String) : CodeDecorator {

    private val licenceHeader: String by lazy { makeLicenceHeader() }
    override fun apply(text: String): String = licenceHeader + text

    private fun makeLicenceHeader(): String {
        val lineBreak = NEW_LINE
        val buf = StringBuilder()
        buf.append("/*").append(lineBreak)
        licence.lines().forEach {
            buf.append(if (it.isNotBlank()) " * $it" else " *").append(lineBreak)
        }
        buf.append(" */").append(lineBreak).append(lineBreak)
        return buf.toString()
    }
}