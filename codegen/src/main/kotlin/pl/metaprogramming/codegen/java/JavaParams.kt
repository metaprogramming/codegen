/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.CodeDecorator
import pl.metaprogramming.codegen.java.base.IClassCmBuilder
import pl.metaprogramming.codegen.java.builders.AnnotatedBuilder
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.getJavaVersion
import kotlin.properties.Delegates.observable

/**
 * Representation of a set of parameters allowing the parameterization
 * (in selected aspects) of the java code generator.
 */
class JavaParams {

    /**
     * Name mapper from model to java.
     */
    var nameMapper: JavaNameMapper = DefaultJavaNameMapper()
    fun nameMapper(value: JavaNameMapper) = apply { nameMapper = value }

    /**
     * Builder for dependency injection code.
     *
     * Using this parameter you will be able to specify
     * whether dependencies will be injected using a constructor
     * or an appropriate annotation.
     *
     * Default builder marks class's fields as final if:
     *  - is not static
     *  - has not set value
     *  - is not annotated with 'org.springframework.beans.factory.annotation.Value'
     * If the class has at least one such field, the "lombok.RequiredArgsConstructor" annotation is added.
     */
    var dependencyInjectionBuilder: IClassCmBuilder<Nothing> = Spring.diWithConstructorBuilder().priority(90)
    fun dependencyInjectionBuilder(value: IClassCmBuilder<Nothing>) = apply { dependencyInjectionBuilder = value }

    /**
     * Builder for managed components used by dependency injection framework.
     *
     * Using this parameter you will be able to specify annotation used
     * to mark beans managed by dependency injection framework.
     *
     * Default builder annotates the class with "org.springframework.stereotype.Component".
     */
    var managedComponentBuilder: IClassCmBuilder<Nothing> =
        AnnotatedBuilder.by("org.springframework.stereotype.Component").priority(80)

    fun managedComponentBuilder(value: IClassCmBuilder<Nothing>) = apply { managedComponentBuilder = value }

    /**
     * It allows to force generation of the 'fromValue' method for enums.
     */
    var isAlwaysGenerateEnumFromValueMethod: Boolean = false

    fun isAlwaysGenerateEnumFromValueMethod(value: Boolean) = apply { isAlwaysGenerateEnumFromValueMethod = value }

    var javaVersion: Int by observable(getJavaVersion()) { _, _, newValue ->
        generatedAnnotationClass = defaultGeneratedAnnotationClass(newValue)
    }

    fun javaVersion(value: Int) = apply { javaVersion = value }

    /**
     * Specify annotation class used to marking generated classes.
     * By default, it is 'javax.annotation.Generated' or
     * 'javax.annotation.processing.Generated' if generation is run
     * with java newer than 1.8.
     *
     * It is not taken into account when the parameter 'generatedAnnotation' or 'generatedStrategy' is set.
     */
    var generatedAnnotationClass: String by observable(defaultGeneratedAnnotationClass(getJavaVersion())) { _, _, _ ->
        generatedAnnotation = makeGeneratedAnnotation()
    }

    fun generatedAnnotationClass(value: String) = apply { generatedAnnotationClass = value }

    /**
     * Specify annotation value used to marking generated classes.
     * By default, it is 'pl.metaprogramming.codegen'.
     *
     * It is not taken into account when the parameter 'generatedAnnotation' or 'generatedStrategy' is set.
     */
    var generatedAnnotationValue: String by observable("pl.metaprogramming.codegen") { _, _, _ ->
        generatedAnnotation = makeGeneratedAnnotation()
    }

    fun generatedAnnotationValue(value: String) = apply { generatedAnnotationValue = value }

    /**
     * Specify annotation used to marking generated classes.
     *
     * It is not taken into account when the parameter 'generatedStrategy' is set.
     */
    var generatedAnnotation: AnnotationCm by observable(makeGeneratedAnnotation()) { _, _, _ ->
        generatedMarkBuilder = makeGeneratedStrategy()
    }

    fun generatedAnnotation(value: AnnotationCm) = apply { generatedAnnotation = value }

    /**
     * Using this parameter you will be able to specify build strategy,
     * which will be applied to every class generation.
     *
     * By default, is used to marking them with 'Generated' annotation.
     */
    var generatedMarkBuilder: IClassCmBuilder<Nothing> = makeGeneratedStrategy()
    fun generatedMarkBuilder(value: IClassCmBuilder<Nothing>) = apply { generatedMarkBuilder = value }

    /**
     * Code contents decorators.
     * Used to modify / decorate the text form of the generated code.
     */
    val codeDecorators: MutableList<CodeDecorator> = mutableListOf()

    /**
     * Adds code content decorator.
     */
    fun addCodeDecorator(decorator: CodeDecorator): JavaParams = apply { codeDecorators.add(decorator) }

    fun licenceHeader(licenceHeader: String): JavaParams = apply {
        codeDecorators.removeIf { it is LicenceDecorator }
        if (licenceHeader.isNotEmpty()) codeDecorators.add(LicenceDecorator(licenceHeader))
    }

    var licenceHeader: String = ""
        set(value) {
            licenceHeader(value)
        }

    private fun makeGeneratedAnnotation() =
        AnnotationCm(generatedAnnotationClass, ValueCm.escaped(generatedAnnotationValue))

    private fun makeGeneratedStrategy() = AnnotatedBuilder(listOf(generatedAnnotation)).priority(100)

    private fun defaultGeneratedAnnotationClass(javaVersion: Int) =
        if (javaVersion > 8) "javax.annotation.processing.Generated" else "javax.annotation.Generated"

    fun copy(): JavaParams {
        val result = JavaParams()
            .nameMapper(nameMapper)
            .javaVersion(javaVersion)
            .dependencyInjectionBuilder(dependencyInjectionBuilder)
            .managedComponentBuilder(managedComponentBuilder)
            .isAlwaysGenerateEnumFromValueMethod(isAlwaysGenerateEnumFromValueMethod)
            .generatedAnnotationClass(generatedAnnotationClass)
            .generatedAnnotationValue(generatedAnnotationValue)
            .generatedAnnotation(generatedAnnotation)
            .generatedMarkBuilder(generatedMarkBuilder)
            .licenceHeader(licenceHeader)
        result.codeDecorators.clear()
        result.codeDecorators.addAll(codeDecorators)
        return result
    }

}