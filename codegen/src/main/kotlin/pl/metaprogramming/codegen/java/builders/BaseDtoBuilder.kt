/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.builders

import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.ObjectType
import java.util.function.Function

abstract class BaseDtoBuilder<T>(private val modelMapper: Function<T, ObjectType>) : ClassCmBuilderTemplate<T>() {
    constructor() : this({ it as ObjectType })

    val objectType: ObjectType get() = modelMapper.apply(model)

    abstract fun addField(schema: DataSchema)

    override fun makeDeclaration() {
        val model = objectType
        if (model.inherits.isNotEmpty()) {
            check(model.inherits.size == 1) { "Multiple inheritance is not allowed ('$model')" }
            superClass = getClass(typeOfCode, model.inherits[0])
        }
        classCm.description = model.description
        model.fields.forEach { field ->
            try {
                addField(field)
            } catch (e: Exception) {
                throw IllegalStateException("Can't handle field '${field.code}' for model '$model'", e)
            }
        }
    }
}