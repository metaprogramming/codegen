/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter

import pl.metaprogramming.codegen.NEW_LINE
import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.Dependencies
import pl.metaprogramming.codegen.java.ValueCm

open class BaseJavaCodeFormatter {
    protected var packageName: String? = null
    protected lateinit var imports: List<String>
    protected lateinit var dependencies: Dependencies
    private lateinit var classNameFormatter: ClassNameFormatter
    protected lateinit var buf: CodeBuffer

    fun init(packageName: String) {
        this.packageName = packageName
        buf = CodeBuffer()
        classNameFormatter = ClassNameFormatter(packageName, dependencies)
    }

    fun formatAnnotations(annotations: List<AnnotationCm>): List<String> = annotations.map(::formatAnnotation)

    fun formatAnnotations(annotations: List<AnnotationCm>, separator: String): String =
        if (annotations.isEmpty()) ""
        else annotations.joinToString(separator) { formatAnnotation(it) } + separator

    fun formatAnnotation(annotationCm: AnnotationCm): String {
        val buf = StringBuilder()
        buf.append('@').append(formatUsage(annotationCm.annotationClass))
        if (annotationCm.params.isNotEmpty()) {
            buf.append('(')
            if (annotationCm.params.all { it.key == "value" }) {
                buf.append(formatValueCm(annotationCm.params.getValue("value")))
            } else {
                buf.append(annotationCm.params.map {
                    it.key + " = ${formatValueCm(it.value)}"
                }.joinToString(", "))
            }
            buf.append(')')
        }
        return buf.toString()
    }

    protected fun formatGenericParams(genericParams: Collection<ClassCd>): String =
        classNameFormatter.genericParamsDeclaration(genericParams)

    private fun formatValueCm(valueCm: ValueCm): String = if (valueCm.array) {
        val itemSeparatorFirst = if (valueCm.itemPerLine) NEW_LINE + TAB else ""
        val itemSeparatorLast = if (valueCm.itemPerLine) NEW_LINE else ""
        val itemSeparator = ", $itemSeparatorFirst"
        '{' + itemSeparatorFirst +
                valueCm.value.joinToString(itemSeparator) { escapedString(it, valueCm.escaped) } +
                itemSeparatorLast + '}'
    } else {
        escapedString(valueCm.value[0], valueCm.escaped)
    }

    private fun escapedString(value: String?, escape: Boolean = true) = when {
        value != null && escape -> "\"${value}\""
        value != null -> value
        else -> "null"
    }

    protected fun formatUsage(classCd: ClassCd?) = classNameFormatter.format(classCd)
}
