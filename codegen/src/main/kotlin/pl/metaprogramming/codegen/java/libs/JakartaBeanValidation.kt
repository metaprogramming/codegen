/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.libs

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataTypeCode.*

object JakartaBeanValidation {
    @JvmStatic
    val VALID = AnnotationCm.of("javax.validation.Valid")

    @JvmStatic
    val NOT_NULL = AnnotationCm.of("javax.validation.constraints.NotNull")

    @JvmStatic
    fun addAnnotations(result: MutableList<AnnotationCm>, schema: DataSchema): List<AnnotationCm> {
        if (schema.required) result.add(NOT_NULL)
        if (schema.isObject) result.add(VALID)
        schema.minimum?.let { result.add(minMax("Min", it, schema)) }
        schema.maximum?.let { result.add(minMax("Max", it, schema)) }
        return result
    }

    private fun minMax(restriction: String, value: String, schema: DataSchema): AnnotationCm {
        val isInt = schema.isType(INT16, INT32, INT64)
        return AnnotationCm(
            "javax.validation.constraints.${if (isInt) "" else "Decimal"}$restriction",
            if (isInt) ValueCm.value(value) else ValueCm.escaped(value)
        )
    }
}