/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.TypeOfCodeWithNoModel

object SpringRs2tCommonTypeOfCode {
    @JvmStatic val VALIDATION_RESULT_MAPPER = TypeOfCodeWithNoModel("RS2T;VALIDATION_RESULT_MAPPER")
    @JvmStatic val MAP_RAW_VALUE_SERIALIZER = TypeOfCodeWithNoModel("RS2T;MAP_RAW_VALUE_SERIALIZER")
    @JvmStatic val REST_RESPONSE_INTERFACE = TypeOfCodeWithNoModel("RS2T;REST_RESPONSE_INTERFACE")
    @JvmStatic val REST_RESPONSE_ABSTRACT = TypeOfCodeWithNoModel("RS2T;REST_RESPONSE_ABSTRACT")
    @JvmStatic val REST_RESPONSE_STATUS_INTERFACE = TypeOfCode<Int>("RS2T;REST_RESPONSE_STATUS_INTERFACE")
    @JvmStatic val REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE = TypeOfCode<Int>("RS2T;REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE")
}