/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.libs

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassKind
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.internal.ClassName

private val VOID = ClassCd("java.lang.Void")
private val OBJECT = ClassCd("java.lang.Object")
private val CLASS = ClassCd("java.lang.Class")
private val GENERIC_PARAM_T = ClassCd.genericParam("T")
private val GENERIC_PARAM_R = ClassCd.genericParam("R")
private val GENERIC_PARAM_V = ClassCd.genericParam("V")
private val GENERIC_PARAM_UNKNOWN: ClassCd = ClassCd(ClassName("?"), kind = ClassKind.UNKNOWN_GENERIC_PARAM)
private val ANNOTATION = ClassCd("java.lang.annotation.Annotation")
private val STRING = ClassCd("java.lang.String")

private val INTEGER = ClassCd("java.lang.Integer")
private val INTEGER_PRIMITIVE = ClassCd("int")
private val LONG = ClassCd("java.lang.Long")
private val FLOAT = ClassCd("java.lang.Float")
private val DOUBLE = ClassCd("java.lang.Double")
private val BOOLEAN = ClassCd("java.lang.Boolean")
private val BOOLEAN_PRIMITIVE = ClassCd("boolean")
private val BIG_DECIMAL = ClassCd("java.math.BigDecimal")
private val SERIALIZABLE = ClassCd("java.io.Serializable")

private val BYTE_ARRAY = ClassCd("byte").asArray()

internal val COLLECTION = ClassCd("java.util.Collection")
internal val LIST = ClassCd("java.util.List")
internal val MAP = ClassCd("java.util.Map")
private val OBJECTS = ClassCd("java.util.Objects")

internal val COLLECTORS = ClassCd("java.util.stream.Collectors")

internal val SUPPLIER = ClassCd("java.util.function.Supplier")

internal val LOCAL_DATE = ClassCd("java.time.LocalDate")
internal val LOCAL_DATE_TIME = ClassCd("java.time.LocalDateTime")
internal val ZONED_DATE_TIME = ClassCd("java.time.ZonedDateTime")
internal val ZONED_OFFSET = ClassCd("java.time.ZoneOffset")
internal val DATA_TIME_FORMATTER = ClassCd("java.time.format.DateTimeFormatter")

private val OVERRIDE = AnnotationCm("java.lang.Override")
private val JAVAX_NONNULL = AnnotationCm("javax.annotation.Nonnull")
private val JAVAX_NULLABLE = AnnotationCm("javax.annotation.Nullable")
private val JAVAX_PARAMETERS_ARE_NONNULL_BY_DEFAULT = AnnotationCm("javax.annotation.ParametersAreNonnullByDefault")

private val EXCEPTION = ClassCd("java.lang.Exception")

object Java {

    // types

    @JvmStatic
    fun genericParamT() = GENERIC_PARAM_T

    @JvmStatic
    fun genericParamR() = GENERIC_PARAM_R

    @JvmStatic
    fun genericParamV() = GENERIC_PARAM_V

    @JvmStatic
    fun genericParamUnknown() = GENERIC_PARAM_UNKNOWN

    @JvmStatic
    fun voidType() = VOID

    @JvmStatic
    fun objectType() = OBJECT

    @JvmStatic
    @JvmOverloads
    fun claasType(type: ClassCd? = null) = if (type != null) CLASS.withGeneric(type) else CLASS

    @JvmStatic
    fun annotation() = ANNOTATION

    @JvmStatic
    fun string() = STRING

    @JvmStatic
    fun boxedInteger() = INTEGER

    @JvmStatic
    fun primitiveInt() = INTEGER_PRIMITIVE

    @JvmStatic
    fun longBoxed() = LONG

    @JvmStatic
    fun boxedFloat() = FLOAT

    @JvmStatic
    fun boxedDouble() = DOUBLE

    @JvmStatic
    fun boxedBoolean() = BOOLEAN

    @JvmStatic
    fun primitiveBoolean() = BOOLEAN_PRIMITIVE

    @JvmStatic
    fun byteArray() = BYTE_ARRAY

    @JvmStatic
    fun bigDecimal() = BIG_DECIMAL

    @JvmStatic
    fun collection(elementType: ClassCd) = COLLECTION.withGeneric(elementType)

    @JvmStatic
    fun list(elementType: ClassCd) = LIST.withGeneric(elementType)

    @JvmStatic
    fun map() = MAP

    @JvmStatic
    fun map(keyType: ClassCd, valueType: ClassCd) = MAP.withGeneric(keyType, valueType)

    @JvmStatic
    fun objects() = OBJECTS

    // stream
    @JvmStatic
    fun collectors() = COLLECTORS

    // util function
    @JvmStatic
    fun supplier(elementType: ClassCd) = SUPPLIER.withGeneric(elementType)

    // time
    @JvmStatic
    fun localDate() = LOCAL_DATE
    @JvmStatic
    fun localDateTime() = LOCAL_DATE_TIME
    @JvmStatic
    fun zonedDateTime() = ZONED_DATE_TIME
    @JvmStatic
    fun zonedOffset() = ZONED_OFFSET
    @JvmStatic
    fun dateTimeFormatter() = DATA_TIME_FORMATTER

    @JvmStatic
    fun serializable() = SERIALIZABLE

    // Annotations
    @JvmStatic
    fun override() = OVERRIDE

    @JvmStatic
    fun nonnul() = JAVAX_NONNULL

    @JvmStatic
    fun nullable() = JAVAX_NULLABLE

    @JvmStatic
    fun parametersAreNonnullByDefault() = JAVAX_PARAMETERS_ARE_NONNULL_BY_DEFAULT

    @JvmStatic
    @JvmOverloads
    fun suppressWarnings(valueParam: String = "unchecked") =
        AnnotationCm("java.lang.SuppressWarnings", ValueCm.escaped(valueParam))

    @JvmStatic
    fun exception() = EXCEPTION

    // class build strategies

    @JvmStatic
    fun <M> privateConstructorBuilder() = object : ClassCmBuilderTemplate<M>() {
        override fun makeDeclaration() {
            methods.add(classCm.className) { isPrivate = true }
        }
    }

}