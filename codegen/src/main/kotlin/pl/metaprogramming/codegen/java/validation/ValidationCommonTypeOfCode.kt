/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.TypeOfCodeWithNoModel

object ValidationCommonTypeOfCode {
    @JvmField val VALIDATION_CONTEXT = TypeOfCodeWithNoModel("VALIDATION_CONTEXT")
    @JvmField val VALIDATION_FIELD = TypeOfCodeWithNoModel("VALIDATION_FIELD")
    @JvmField val VALIDATION_CHECKER = TypeOfCodeWithNoModel("VALIDATION_CHECKER")
    @JvmField val VALIDATION_COMMON_CHECKERS = TypeOfCodeWithNoModel("VALIDATION_COMMON_CHECKERS")
    @JvmField val VALIDATION_ERROR = TypeOfCodeWithNoModel("VALIDATION_ERROR")
    @JvmField val VALIDATION_RESULT = TypeOfCodeWithNoModel("VALIDATION_RESULT")
    @JvmField val REST_EXCEPTION_HANDLER = TypeOfCodeWithNoModel("REST_EXCEPTION_HANDLER") // Exception (especially VALIDATION_EXCEPTION) -> ResponseEntity
    @JvmField val VALIDATION_EXCEPTION = TypeOfCodeWithNoModel("VALIDATION_EXCEPTION")
    @JvmField val VALIDATION_VALIDATOR = TypeOfCodeWithNoModel("VALIDATION_VALIDATOR")
    @JvmField val VALIDATION_DICTIONARY_CHECKER = TypeOfCodeWithNoModel("VALIDATION_DICTIONARY_CHECKER")
    @JvmField val VALIDATION_DICTIONARY_CODE_ENUM = TypeOfCodeWithNoModel("VALIDATION_DICTIONARY_CODE_ENUM")
    @JvmField val VALIDATION_ERROR_CODE_ENUM = TypeOfCodeWithNoModel("VALIDATION_ERROR_CODE_ENUM")

    @JvmField val BOOL_EXP = TypeOfCodeWithNoModel("BOOL_EXP")
    @JvmField val CONDITIONAL_CHECKER = TypeOfCodeWithNoModel("CONDITIONAL_CHECKER")
    @JvmField val CONDITION_RESOLVER = TypeOfCodeWithNoModel("CONDITION_RESOLVER")

    @JvmField val OPERATION_ID_ENUM = TypeOfCodeWithNoModel("OPERATION_ID_ENUM")
    @JvmField val CONDITION_ENUM = TypeOfCodeWithNoModel("CONDITION_ENUM")
    @JvmField val SECURITY_PRIVILEGE_ENUM = TypeOfCodeWithNoModel("SECURITY_PRIVILEGE_ENUM")
}