/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import freemarker.template.Configuration
import freemarker.template.TemplateExceptionHandler
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.template.FreeMarkerGeneratorFactory
import pl.metaprogramming.codegen.template.TemplateGenerator
import java.util.*

class ClassBuilderConfigurator<M>(
    val typeOfCode: TypeOfCode<M>,
    val params: CodegenParams,
) {

    var forceGeneration: Boolean = false
    val className = ClassNameBuilder<M>()
    val packageName = PackageNameBuilder<M>()
    var projectDir: String = ""
    var projectSubDir: String = "src/main/java"

    /**
     * Lets you register as a different type of code in the class index.
     *
     * For example, an ENUM type can be registered as DTO, which can simplify code model building implementations.
     * In particular, that the ENUM can be regarded as a special case of the DTO.
     */
    var registerAsTypeOfCode: Any? = null

    /**
     * List of all code model builders.
     *
     * It is not used when code is generated from a template.
     */
    val builders: MutableList<IClassCmBuilder<out M>> = mutableListOf()

    /**
     * Template to be used to generate code.
     *
     * You can use [freemarkerTemplate] instead of this property.
     */
    var template: TemplateGenerator? = null

    // used only by template generators
    val dependencies: MutableList<Any> = mutableListOf()

    val baseDir: String get() = if (projectDir.isNotEmpty()) "$projectDir/$projectSubDir" else projectSubDir

    /**
     * Adds code model builder for `declaration` phase.
     *
     * It is not used when code is generated from a template.
     *
     * Note that the strange construction `ClassCmBuilderTemplate.Builder<M>` is here to make it work like `ClassCmBuilderTemplate.() -> Unit` in Kotlin and like `Consumer<ClassCmBuilderTemplate>` in Java.
     */
    fun onDeclaration(declarer: ClassCmBuilderTemplate.Builder<M>) = apply {
        builders += object : ClassCmBuilderTemplate<M>() {
            override fun makeDeclaration() {
                declarer.make(this)
            }
        }
    }

    /**
     * Adds code model builder for `implementation` phase.
     *
     * It is not used when code is generated from a template.
     *
     * Note that the strange construction `ClassCmBuilderTemplate.Builder<M>` is here to make it work like `ClassCmBuilderTemplate.() -> Unit` in Kotlin and like `Consumer<ClassCmBuilderTemplate>` in Java.
     */
    fun onImplementation(implementer: ClassCmBuilderTemplate.Builder<M>) = apply {
        builders += object : ClassCmBuilderTemplate<M>() {
            override fun makeImplementation() {
                implementer.make(this)
            }
        }
    }

    /**
     * Adds code model builder for `decoration` phase.
     *
     * It is not used when code is generated from a template.
     *
     * Note that the strange construction `ClassCmBuilderTemplate.Builder<M>` is here to make it work like `ClassCmBuilderTemplate.() -> Unit` in Kotlin and like `Consumer<ClassCmBuilderTemplate>` in Java.
     */
    fun onDecoration(decorator: ClassCmBuilderTemplate.Builder<M>) = apply {
        builders += object : ClassCmBuilderTemplate<M>() {
            override fun makeDecoration() {
                decorator.make(this)
            }
        }
    }

    fun registerAsTypeOfCode(typeOfCode: Any) = apply { registerAsTypeOfCode = typeOfCode }

    fun projectSubDir(projectSubDir: String) = apply { this.projectSubDir = projectSubDir }

    fun className(builder: ClassNameBuilder.Setter<M>) = apply { this.className.setup(builder) }

    fun packageName(builder: PackageNameBuilder.Setter<M>) = apply { packageName.setup(builder) }

    /** Forces code generation even if it is not used by other generated code. */
    fun forceGeneration(forceGeneration: Boolean) = apply { this.forceGeneration = forceGeneration }

    /**
     * Use FreeMarker template as generator.
     *
     * The template should be located in classpath.
     *
     * If you require some custom FreeMarker configuration use [template] property.
     * Then you should also set the [className].
     */
    var freemarkerTemplate: String = ""
        set(value) {
            field = value
            className.fixed = value
                .indexOfLast { it == '/' }
                .let {
                    val startIdx = if (it > 0) it else 0
                    value.substring(startIdx, value.indexOf('.', startIdx))
                }
            template = freeMarkerGeneratorFactory.make(value)
        }

    /**
     * Adds [JavaParams.managedComponentBuilder] and [JavaParams.dependencyInjectionBuilder] builders.
     *
     * Note that if another builders adds an alternative annotation to the one provided by [JavaParams.managedComponentBuilder],
     * you should use the [addDependencyInjectionBuilder] method to provide dependency injection.
     */
    fun addManagedComponentBuilder() = apply {
        builders += managedComponentBuilder
        builders += dependencyInjectionBuilder
    }

    /** Adds [JavaParams.dependencyInjectionBuilder] builder. */
    fun addDependencyInjectionBuilder() = apply {
        builders += dependencyInjectionBuilder
    }

    private val managedComponentBuilder = ClassCmBuilderDelegator { params[JavaParams::class].managedComponentBuilder }
    private val dependencyInjectionBuilder =
        ClassCmBuilderDelegator { params[JavaParams::class].dependencyInjectionBuilder }

    internal fun <T> orderedStrategies(): List<IClassCmBuilder<T>> {
        val result = mutableListOf<IClassCmBuilder<T>>()
        @Suppress("UNCHECKED_CAST")
        (builders as List<IClassCmBuilder<T>>).forEach { s ->
            val idx = result.indexOfFirst { it.priority > s.priority }
                .let { if (it < 0) result.size else it }
            result.add(idx, s)
        }
        return result
    }

    private val freeMarkerGeneratorFactory: FreeMarkerGeneratorFactory by lazy {
        val fmCfg = Configuration(Configuration.VERSION_2_3_32).apply {
            setClassForTemplateLoading(javaClass, "/")
            defaultEncoding = "UTF-8"
            templateExceptionHandler = TemplateExceptionHandler.RETHROW_HANDLER
            logTemplateExceptions = false
            wrapUncheckedExceptions = true
            fallbackOnNullLoopVariable = false
            sqlDateAndTimeTimeZone = TimeZone.getDefault()
        }
        FreeMarkerGeneratorFactory(fmCfg, "UTF-8")
    }

    fun interface Setter<M> {
        fun ClassBuilderConfigurator<M>.setup()
    }

    private fun Setter<M>.setup(obj: ClassBuilderConfigurator<M>) {
        obj.setup()
    }

    internal fun setup(setter: Setter<M>) = apply { setter.setup(this) }
}