/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.builders.BaseEnumBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Lombok
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.BOOL_EXP
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.CONDITIONAL_CHECKER
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.CONDITION_ENUM
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.CONDITION_RESOLVER
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.OPERATION_ID_ENUM
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.REST_EXCEPTION_HANDLER
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.SECURITY_PRIVILEGE_ENUM
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_CHECKER
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_COMMON_CHECKERS
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_CONTEXT
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_DICTIONARY_CHECKER
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_DICTIONARY_CODE_ENUM
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_ERROR
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_ERROR_CODE_ENUM
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_EXCEPTION
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_FIELD
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_RESULT
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_VALIDATOR

class ValidationCommonCodesConfiguration : JavaGeneratorBuilder.Delegate() {

    override fun configure() {
        typeOfCode(VALIDATION_CONTEXT) {
            freemarkerTemplate = "templates/ValidationContext.java.ftl"
        }

        typeOfCode(VALIDATION_CHECKER) {
            className.fixed = "Checker"
            onDeclaration { makeChecker() }
        }

        typeOfCode(VALIDATION_FIELD) {
            className.fixed = "Field"
            freemarkerTemplate = "templates/Field.java.ftl"
        }

        typeOfCode(VALIDATION_ERROR) {
            className.fixed = "ValidationError"
            onImplementation {
                addAnnotation(Lombok.builder(), Lombok.value())
                implementationOf(Java.serializable())
                fields.add("field", Java.string())
                fields.add("code", Java.string())
                fields.add("message", Java.string())
                fields.add("messageArgs", Java.objectType().asArray())
                fields.add("stopValidation", Java.primitiveBoolean())
                fields.add("status", Java.boxedInteger())
            }
        }

        typeOfCode(VALIDATION_RESULT) {
            className.fixed = "ValidationResult"
            onImplementation { makeValidationResult() }
        }

        typeOfCode(VALIDATION_EXCEPTION) {
            className.fixed = "ValidationException"
            onImplementation {
                superClass = "java.lang.RuntimeException".asClassCd()
                addAnnotation(Lombok.allArgsConstructor(), Lombok.getter())
                fields.add("result", getClass(VALIDATION_RESULT)) { isFinal = true }
                methods.add("getMessage") {
                    resultType = Java.string()
                    annotations.add(Java.override())
                    implDependencies.add(Java.collectors())
                    implBody = """
                        |String errors = result.getErrors().stream()
                        |       .map(err -> err.getField() == null ? err.getCode() : String.format("%s - %s", err.getField(), err.getCode()))
                        |       .collect(Collectors.joining(", "));
                        |return String.format("Failed validation (%d) with errors: %s", result.getStatus(), errors);
                        """.trimMargin()
                }
            }
        }

        typeOfCode(REST_EXCEPTION_HANDLER) {
            className.fixed = "RestExceptionHandler"
            onImplementation { makeRestExceptionHandler() }
            dependencies.addAll(
                listOf(
                    VALIDATION_EXCEPTION,
                    VALIDATION_RESULT,
                    VALIDATION_ERROR,
                    VALIDATION_COMMON_CHECKERS,
                    JavaCommonTypeOfCode.ENUM_VALUE_INTERFACE
                )
            )
        }

        typeOfCode(VALIDATION_VALIDATOR) {
            className.fixed = "Validator"
            onImplementation {
                classCm.isAbstract = true
                addGenericParams(Java.genericParamV())
                implementationOf(classLocator(VALIDATION_CHECKER).getGeneric(Java.genericParamV()))
                methods.add("validate") {
                    resultType = getClass(VALIDATION_RESULT)
                    params.add("input", Java.genericParamV())
                    codeBuf + "ValidationContext<V> context = new ValidationContext<>(input);" +
                            "check(context);"
                    if (params<ValidationParams>().throwExceptionIfValidationFailed) {
                        val vex = getClass(VALIDATION_EXCEPTION)
                        implDependencies.add(vex)
                        codeBuf.ifBlock("!context.isValid()", "throw new ${vex.className}(context.getResult());")
                            .endBlock()
                    }
                    codeBuf + "return context.getResult();"
                    implBody = codeBuf.take()
                }

            }
        }

        typeOfCode(VALIDATION_COMMON_CHECKERS) {
            freemarkerTemplate = "templates/CommonCheckers.java.ftl"
            dependencies.addAll(
                listOf(
                    JavaCommonTypeOfCode.ENUM_VALUE_INTERFACE,
                    VALIDATION_CHECKER,
                    BOOL_EXP,
                    CONDITIONAL_CHECKER
                )
            )
        }

        typeOfCode(VALIDATION_DICTIONARY_CHECKER) {
            className.fixed = "DictionaryChecker"
            addManagedComponentBuilder()
            onDeclaration {
                methods.add("check") {
                    registerAsMapper()
                    resultType = getClass(VALIDATION_CHECKER).withGeneric(Java.string())
                    params.add("code", getClass(VALIDATION_DICTIONARY_CODE_ENUM))
                    implBody = "return ctx -> { /* TODO implement me */ };"
                }
            }
        }

        typeOfCode(VALIDATION_DICTIONARY_CODE_ENUM) {
            className.fixed = "DictionaryCode"
            onDeclaration { kind = ClassKind.ENUM }
        }

        typeOfCode(VALIDATION_ERROR_CODE_ENUM) {
            className.fixed = "ErrorCode"
            builders += BaseEnumBuilder()
        }

        typeOfCode(SECURITY_PRIVILEGE_ENUM) {
            className.fixed = "Privilege"
            builders += BaseEnumBuilder()
        }

        typeOfCode(BOOL_EXP) {
            className.fixed = "BoolExp"
            onDeclaration {
                kind = ClassKind.INTERFACE
                methods.add("evaluate") {
                    resultType = Java.boxedBoolean()
                    params.add("ctx", getClass(VALIDATION_CONTEXT).withUnknownGeneric())
                }
            }
        }

        typeOfCode(CONDITIONAL_CHECKER) {
            className.fixed = "ConditionalChecker"
            onDeclaration {
                val checkerType = getClass(VALIDATION_CHECKER).withGeneric(Java.genericParamV())
                addGenericParams(Java.genericParamV())
                implementationOf(checkerType)
                addAnnotation(Lombok.requiredArgsConstructor())

                fields.add("checker", checkerType) { isFinal = true }
                fields.add("condition", getClass(BOOL_EXP)) { isFinal = true }

                methods.add("check") {
                    annotations.add(Java.override())
                    params.add("ctx", getClass(VALIDATION_CONTEXT).withGeneric(Java.genericParamV()))
                    implBody = "if (condition.evaluate(ctx)) checker.check(ctx);"
                }
                methods.add("checkNull") {
                    annotations.add(Java.override())
                    resultType = Java.primitiveBoolean()
                    implBody = "return checker.checkNull();"
                }
            }
        }

        typeOfCode(CONDITION_ENUM) {
            className.fixed = "Condition"
            builders += BaseEnumBuilder()
            onDeclaration {
                implementationOf(getClass(BOOL_EXP))
                methods.add("evaluate") {
                    annotations.add(Java.override())
                    resultType = Java.boxedBoolean()
                    params.add("ctx", getClass(VALIDATION_CONTEXT).withUnknownGeneric())
                    implDependencies.add(getClass(CONDITION_RESOLVER))
                    implBody = "return ctx.getBean(ConditionResolver.class).resolve(this, ctx);"
                }
            }
        }

        typeOfCode(CONDITION_RESOLVER) {
            className.fixed = "ConditionResolver"
            onDeclaration {
                methods.add("resolve") {
                    resultType = Java.primitiveBoolean()
                    params.add("condition", getClass(CONDITION_ENUM))
                    params.add("ctx", getClass(VALIDATION_CONTEXT).withUnknownGeneric())
                    implBody = "return false;// TODO implement me"
                }
            }
            addManagedComponentBuilder()
        }

        typeOfCode(OPERATION_ID_ENUM) {
            className.fixed = "OperationId"
            builders += BaseEnumBuilder()
            onDeclaration {
                implementationOf(getClass(BOOL_EXP))
                methods.add("evaluate") {
                    annotations.add(Java.override())
                    resultType = Java.boxedBoolean()
                    params.add("ctx", getClass(VALIDATION_CONTEXT).withUnknownGeneric())
                    implBody = "return ctx.getBean(getClass()) == this;"
                }
            }
        }

        (typeOfCodes - REST_EXCEPTION_HANDLER).forEach {
            typeOfCode(it) { packageName.tail = "validator" }
        }
    }
}

private fun ClassCmBuilderTemplate<Nothing>.makeChecker() {
    kind = ClassKind.INTERFACE
    addGenericParams(Java.genericParamV())

    methods.add("check") {
        params.add("ctx", getClass(VALIDATION_CONTEXT).withGeneric(Java.genericParamV()))
    }
    methods.add("checkWithParent") {
        params.add(
            "ctx",
            getClass(VALIDATION_CONTEXT).withGeneric(
                Java.genericParamT().withSuper(Java.genericParamV())
            )
        )
        annotations.add(Java.suppressWarnings())
        implBody = "check((ValidationContext) ctx);"
    }
    methods.add("checkNull") {
        resultType = Java.primitiveBoolean()
        implBody = "return false;"
    }
    methods.add("withError") {
        registerAsMapper()
        resultType = classCm
        params.add("errorCode", getClass(VALIDATION_ERROR_CODE_ENUM))
        implBody = """
            |return ctx -> check(new ValidationContext<>(ctx, error -> ValidationError.builder()
            |        .code(errorCode.getValue())
            |        .message(error.getMessage())
            |        .field(error.getField())
            |        .status(error.getStatus())
            |        .stopValidation(error.isStopValidation())
            |        .build()));
            """.trimMargin()
    }
}

private fun ClassCmBuilderTemplate<Nothing>.makeValidationResult() {

    implementationOf(Java.serializable())

    fields.add("stopped", Java.primitiveBoolean()) { addAnnotation(Lombok.getter()) }
    val requestField = fields.add("request", Java.objectType()) {
        addAnnotation(Lombok.getter())
        isTransient = true
    }
    fields.add("status", Java.boxedInteger()) { addAnnotation(Lombok.getter()) }

    val errorClass = getClass(VALIDATION_ERROR)

    fields.add("errors", errorClass.asList()) {
        addAnnotation(Lombok.getter())
        value = ValueCm.newExp("java.util.ArrayList", true)
    }

    methods.add(classCm.className) {
        params.add(requestField.withName("request"))
        implBody = "this.request = request;"
    }
    methods.add("isValid") {
        resultType = Java.primitiveBoolean()
        implBody = "return status == null;"
    }
    methods.add("isFieldValid") {
        resultType = Java.primitiveBoolean()
        params.add("field", Java.string())
        implBody = """
            |for (ValidationError e : errors) {
            |    if (field != null && field.equals(e.getField())) return false;
            |}
            |return true;
            """.trimMargin()
    }
    methods.add("setStatus") {
        resultType = classCm
        params.add("status", Java.primitiveInt())
        implBody = """
            |this.status = status;
            |return this;
            """.trimMargin()
    }
    methods.add("addError") {
        resultType = classCm
        params.add("error", errorClass)
        implBody = """
            |status = error.getStatus() == null ? 400 : error.getStatus();
            |errors.add(error);
            |${if (params<ValidationParams>().stopAfterFirstError) "stopped = true" else "stopped = error.isStopValidation()"};
            |return this;
            """.trimMargin()
    }
    methods.add("getMessage") {
        resultType = Java.string()
        implBody = "return errors.get(0).getMessage();"
    }
    methods.add("setError") {
        resultType = classCm
        params.add("status", Java.primitiveInt())
        params.add("message", Java.string())
        implBody = """
            |addError(ValidationError.builder().status(status).message(message).stopValidation(true).build());
            |return this;
            """.trimMargin()
    }
}

private fun ClassCmBuilderTemplate<Nothing>.makeRestExceptionHandler() {
    addAnnotation(AnnotationCm("lombok.extern.slf4j.Slf4j"))
    addAnnotation(AnnotationCm("org.springframework.web.bind.annotation.ControllerAdvice"))
    addAnnotation(Java.parametersAreNonnullByDefault())

    val webRequestType = ClassCd("org.springframework.web.context.request.WebRequest")
    val responseType = Spring.responseEntity(Java.objectType())
    val validationResultType = getClass(VALIDATION_RESULT)
    val mapByClassToString = Java.map(Java.claasType(Java.genericParamUnknown()), Java.string())

    fields.add("REQ_BODY", Java.string()) {
        isStatic = true
        isFinal = true
        value = ValueCm.escaped("body")
    }
    fields.add("INVALID_CODE_BY_CLASS", mapByClassToString) {
        isStatic = true
        isFinal = true
        value = ValueCm.value("makeInvalidCodeByClass()")
    }

    methods.add("handleAll") {
        resultType = responseType
        params.add("e", Java.exception())
        params.add("request", webRequestType)
        annotations.add(
            AnnotationCm(
                "org.springframework.web.bind.annotation.ExceptionHandler",
                ValueCm.value("{Exception.class}")
            )
        )
        implBody = """
            |if (e instanceof ValidationException) {
            |    return toResponseEntity(((ValidationException) e).getResult(), request);
            |}
            |if (e instanceof HttpMessageNotReadableException) {
            |   return toResponseEntity(toValidationResult((HttpMessageNotReadableException) e), request);
            |}
            |if (e instanceof MethodArgumentTypeMismatchException) {
            |   return toResponseEntity(new MatmExceptionConverter((MethodArgumentTypeMismatchException) e).convert(), request);
            |}
            |if (e instanceof ResponseStatusException) {
            |   return toResponseEntity((ResponseStatusException) e, request);
            |}
            |if (e instanceof MissingRequestHeaderException) {
            |   return toResponseEntity(toValidationResult((MissingRequestHeaderException) e), request);
            |}
            |log.error("Exception", e);
            |return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            """.trimMargin()
        implDependencies.add(getClass(VALIDATION_EXCEPTION))
        implDependencies.add(
            "org.springframework.web.server.ResponseStatusException",
            "org.springframework.http.HttpStatus"
        )
    }

    methods.add("toResponseEntity") {
        isPrivate = true
        resultType = responseType
        params.add("result", validationResultType)
        params.add("request", webRequestType)
        implBody = """
            |List<String> responseBody = result.getErrors().stream().map(e ->
            |        e.getField() == null ? e.getCode()
            |                : String.format("%s: %s", e.getField(), e.getCode())).collect(Collectors.toList());
            |return ResponseEntity.status(result.getStatus()).body(responseBody);
            """.trimMargin()
        implDependencies.add(Java.collectors())
    }

    methods.add("toValidationResult") {
        isPrivate = true
        resultType = validationResultType
        params.add("field", Java.string())
        params.add("code", Java.string())
        implBody = """
            |return new ValidationResult(null).addError(ValidationError.builder()
            |        .field(field)
            |        .code(code)
            |        .build());
            """.trimMargin()
        implDependencies.add(getClass(VALIDATION_ERROR))
    }

    methods.add("toValidationResult") {
        isPrivate = true
        resultType = validationResultType
        params.add("field", Java.string())
        params.add("allowedValues", Java.list(Java.string()))
        implBody = """
            |return new ValidationResult(null).addError(ValidationError.builder()
            |        .field(field)
            |        .code(ERR_CODE_IS_NOT_ALLOWED_VALUE)
            |        .message(String.format("%s should have one of values: %s", field, allowedValues))
            |        .messageArgs(new Object[]{allowedValues.toString()})
            |        .build());
            """.trimMargin()
    }

    methods.add("getEnumValues") {
        isPrivate = true
        resultType = Java.list(Java.string())
        params.add("enumClass", Java.claasType(getClass(JavaCommonTypeOfCode.ENUM_VALUE_INTERFACE)))
        implBody =
            "return Arrays.stream(enumClass.getEnumConstants()).map(EnumValue::getValue).collect(Collectors.toList());"
        implDependencies.add("java.util.Arrays")
    }

    methods.add("makeUnknownValidationResult") {
        isPrivate = true
        resultType = validationResultType
        implBody = """
            |return new ValidationResult(null).addError(ValidationError.builder()
            |        .code("invalid_input")
            |        .message("invalid_input")
            |        .build());
            """.trimMargin()
    }

    methods.add("toResponseEntity") {
        isPrivate = true
        resultType = responseType
        params.add("e", "org.springframework.web.server.ResponseStatusException")
        params.add("request", webRequestType)
        implBody = "return new ResponseEntity<>(e.getMessage(), e.getStatus());"
    }

    methods.add("toValidationResult") {
        isPrivate = true
        resultType = validationResultType
        params.add("e", "org.springframework.web.bind.MissingRequestHeaderException")
        implBody = "return toValidationResult(e.getHeaderName() + \" (HEADER parameter)\", ERR_CODE_IS_REQUIRED);"
    }

    methods.add("toValidationResult") {
        isPrivate = true
        annotations.add(Java.suppressWarnings("unchecked"))
        resultType = validationResultType
        params.add("e", ClassCd("org.springframework.http.converter.HttpMessageNotReadableException"))
        implBody = """
            |if (e.getMessage() != null && e.getMessage().startsWith("Required request body is missing")) {
            |    return toValidationResult(REQ_BODY, ERR_CODE_IS_REQUIRED);
            |}
            |if (e.getCause() instanceof MismatchedInputException) {
            |    MismatchedInputException cex = (MismatchedInputException) e.getCause();
            |    String field = makeFieldPath(cex.getPath());
            |    String codeByClass = INVALID_CODE_BY_CLASS.get(cex.getTargetType());
            |    if (codeByClass != null) {
            |        return toValidationResult(field, codeByClass);
            |    }
            |    if (cex.getTargetType().isEnum()) {
            |        return toValidationResult(field, getEnumValues((Class<EnumValue>) cex.getTargetType()));
            |    }
            |}
            |if (e.getCause() instanceof JsonMappingException
            |        && e.getCause().getCause() instanceof NullPointerException) {
            |    JsonMappingException jme = (JsonMappingException) e.getCause();
            |    return toValidationResult(makeFieldPath(jme.getPath()), ERR_CODE_IS_REQUIRED);
            |}
            |log.info("Invalid request", e);
            |return makeUnknownValidationResult();
            """.trimMargin()
        implDependencies.add(
            "com.fasterxml.jackson.databind.exc.MismatchedInputException",
            "com.fasterxml.jackson.databind.JsonMappingException"
        )
    }

    methods.add("makeFieldPath") {
        isPrivate = true
        resultType = Java.string()
        params.add("path", Java.list(ClassCd("com.fasterxml.jackson.databind.JsonMappingException.Reference")))
        implBody = """
            |StringBuilder buf = new StringBuilder();
            |path.forEach(r -> {
            |    if (r.getFrom() instanceof Map) {
            |        buf.append(String.format("[%s]", r.getFieldName()));
            |    } else if (r.getFieldName() != null) {
            |        if (buf.length() > 0) buf.append('.');
            |        buf.append(r.getFieldName());
            |    } else if (r.getFrom() instanceof List) {
            |        if (buf.length() == 0) buf.append(REQ_BODY);
            |        buf.append(String.format("[%d]", r.getIndex()));
            |    } else {
            |        buf.append(".NULL");
            |    }
            |});
            |String result = buf.toString();
            |if (result.startsWith(REQ_BODY + '.')) return result.substring(REQ_BODY.length() + 1);
            |if (result.startsWith(REQ_BODY + '[')) return result.substring(REQ_BODY.length());
            |return result;
            """.trimMargin()
    }

    methods.add("makeInvalidCodeByClass") {
        isPrivate = true
        isStatic = true
        resultType = Java.map(Java.claasType(Java.genericParamUnknown()), Java.string())
        implBody = """
            |Map<Class<?>, String> result = new HashMap<>();
            |result.put(BigDecimal.class, ERR_CODE_IS_NOT_NUMBER);
            |result.put(byte[].class, ERR_CODE_IS_NOT_BASE64);
            |result.put(Boolean.class, ERR_CODE_IS_NOT_BOOLEAN);
            |result.put(boolean.class, ERR_CODE_IS_NOT_BOOLEAN);
            |result.put(Double.class, ERR_CODE_IS_NOT_DOUBLE);
            |result.put(double.class, ERR_CODE_IS_NOT_DOUBLE);
            |result.put(Float.class, ERR_CODE_IS_NOT_FLOAT);
            |result.put(float.class, ERR_CODE_IS_NOT_FLOAT);
            |result.put(Integer.class, ERR_CODE_IS_NOT_INT);
            |result.put(int.class, ERR_CODE_IS_NOT_INT);
            |result.put(Long.class, ERR_CODE_IS_NOT_LONG);
            |result.put(long.class, ERR_CODE_IS_NOT_LONG);
            |result.put(LocalDate.class, ERR_CODE_IS_NOT_DATE);
            |result.put(LocalDateTime.class, ERR_CODE_IS_NOT_DATE_TIME);
            |result.put(ZonedDateTime.class, ERR_CODE_IS_NOT_DATE_TIME);
            |return result;
            """.trimMargin()
        implDependencies.add(
            "java.math.BigDecimal",
            "java.time.LocalDate",
            "java.time.LocalDateTime",
            "java.time.ZonedDateTime",
            "java.util.HashMap"
        )
        implDependencies.add(getClass(VALIDATION_COMMON_CHECKERS), "*")
    }

    addInnerClass("MatmExceptionConverter") {
        addAnnotation(Lombok.requiredArgsConstructor())
        classCm.isPrivate = true
        fields.add("e", ClassCd("org.springframework.web.method.annotation.MethodArgumentTypeMismatchException")) {
            isFinal = true
        }

        methods.add("convert") {
            resultType = validationResultType
            implBody = """
                |String code = INVALID_CODE_BY_CLASS.get(getDataType());
                |return code != null ? toValidationResult(getField(), code) : makeUnknownValidationResult();
                """.trimMargin()
        }

        methods.add("getField") {
            isPrivate = true
            resultType = Java.string()
            implBody = "return e.getName() + getParamLocation() + getListIndex();"
        }

        methods.add("getParamLocation") {
            isPrivate = true
            resultType = Java.string()
            implBody = """
                |if (isAnnotated(RequestHeader.class)) return " (HEADER parameter)";
                |if (isAnnotated(RequestParam.class)) return " (QUERY parameter)";
                |return "";
                """.trimMargin()
            implDependencies.add(
                "org.springframework.web.bind.annotation.RequestHeader",
                "org.springframework.web.bind.annotation.RequestParam"
            )
        }

        methods.add("getListIndex") {
            isPrivate = true
            resultType = Java.string()
            implBody = """
                |if (isListParam()) {
                |    if (e.getCause() instanceof ConversionFailedException && e.getValue() instanceof String[]) {
                |        ConversionFailedException cfe = (ConversionFailedException) e.getCause();
                |        int index = Arrays.asList((Object[]) e.getValue()).indexOf(cfe.getValue());
                |        return String.format("[%d]", index);
                |    }
                |    return "[?]";
                |}
                |return "";
                """.trimMargin()
            implDependencies.add("org.springframework.core.convert.ConversionFailedException")
        }

        methods.add("getDataType") {
            isPrivate = true
            resultType = Java.claasType(Java.genericParamUnknown())
            implBody = """
                |if (isListParam()) {
                |    Type parameterizedType = e.getParameter().getParameter().getParameterizedType();
                |    if (parameterizedType instanceof ParameterizedType) {
                |        Type[] params = ((ParameterizedType) parameterizedType).getActualTypeArguments();
                |        if (params.length > 0 && params[0] instanceof Class) {
                |            return (Class<?>) params[0];
                |        }
                |    }
                |}
                |return e.getParameter().getParameterType();
                """.trimMargin()
            implDependencies.add("java.lang.reflect.ParameterizedType", "java.lang.reflect.Type")
        }

        methods.add("isListParam") {
            isPrivate = true
            resultType = Java.primitiveBoolean()
            implBody = "return e.getParameter().getParameterType() == List.class;"
        }

        methods.add("isAnnotated") {
            isPrivate = true
            resultType = Java.primitiveBoolean()
            params.add("annotation", Java.claasType(Java.genericParamUnknown().withSuper(Java.annotation())))
            implBody = "return e.getParameter().getParameterAnnotation(annotation) != null;"
        }
    }
}
