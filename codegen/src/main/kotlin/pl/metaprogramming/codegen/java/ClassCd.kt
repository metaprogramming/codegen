/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.java.internal.ClassName
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.LIST
import pl.metaprogramming.codegen.java.libs.MAP

/**
 * genericClass i genericParams są ze sobą powiązane.
 * genericClass to reprezentacja klasy szablonowej, a genericParams to jej parametry.
 * Wskazanie genericClass jest potrzebne tylko wtedy gdy jest to klasa generowana i
 * należy ją generować tylko wtedy gdy jest używana.
 */
open class ClassCd internal constructor(
    private val name: ClassName,
    open val kind: ClassKind = ClassKind.CLASS,
    private val genericClass: ClassCd? = null,
    open val genericParams: List<ClassCd> = emptyList(),
    open val superClass: ClassCd? = null,
    val forceGeneration: Boolean = false,
) {
    constructor(canonicalName: String, vararg genericParams: ClassCd) : this(
        ClassName.of(canonicalName),
        genericParams = genericParams.toList()
    )

    val canonicalName: String by name::canonicalName
    open val packageName: String? by name::classPackage
    val className: String by name::name

    var used: Boolean = false
    val isToGenerate: Boolean get() = used || forceGeneration

    open fun markAsUsed() {
        if (!used) {
            used = true
            superClass?.markAsUsed()
            genericClass?.markAsUsed()
            genericParams.forEach { it.markAsUsed() }
        }
    }

    fun collectDependencies(dependencies: Dependencies) {
        dependencies.add(this)
        if (isUnknownGenericParam) superClass?.let { dependencies.add(it) }
        genericParams.forEach { it.collectDependencies(dependencies) }
    }

    fun isClass(canonicalName: String) = this.canonicalName == canonicalName
    val isVoid: Boolean get() = this == Java.voidType()
    val isArray: Boolean get() = kind == ClassKind.ARRAY
    val isList: Boolean get() = this == LIST
    val isMap: Boolean get() = this == MAP
    val isEnum: Boolean get() = kind == ClassKind.ENUM
    val isInterface: Boolean get() = kind == ClassKind.INTERFACE
    val isGenericParam: Boolean get() = kind == ClassKind.GENERIC_PARAM
    val isUnknownGenericParam: Boolean get() = kind == ClassKind.UNKNOWN_GENERIC_PARAM

    fun asArray() = with(kind = ClassKind.ARRAY)
    fun asCollection() = Java.collection(this)
    fun asList() = Java.list(this)
    fun asMapBy(keyType: ClassCd) = Java.map(keyType, this)
    fun asMapTo(valueType: ClassCd) = Java.map(this, valueType)
    fun asField(name: String?) = if (name != null) FieldCm(this, name) else FieldCm(this)
    fun asField(name: String, builder: FieldCm.Setter) = FieldCm(name, this, builder)
    fun asField() = FieldCm(this)
    fun asExpression(expression: String) = FieldCm(this).setValue(expression)

    fun withGeneric(genericParams: List<ClassCd>) = with(genericParams = genericParams)
    fun withGeneric(vararg genericParams: ClassCd) = with(genericParams = genericParams.toList())
    fun withUnknownGeneric() = withGeneric(Java.genericParamUnknown())
    fun withSuper(superClass: ClassCd) = with(superClass = superClass)

    fun with(genericParams: List<ClassCd>? = null, superClass: ClassCd? = null, kind: ClassKind? = null): ClassCd =
        ClassCd(
            name,
            kind ?: this.kind,
            if (genericParams != null) this else genericClass,
            genericParams ?: this.genericParams,
            superClass ?: this.superClass
        )

    fun staticFieldRef(fieldName: String) = ValueCm.value("${className}.${fieldName}").dependsOn(this)
    fun classRef() = ValueCm.value("${className}.class").dependsOn(this)
    fun newExp() = ValueCm.value("new ${className}${if (genericParams.isEmpty()) "" else "<>"}()").dependsOn(this)

    override fun equals(other: Any?): Boolean {
        return other is ClassCd && other.isClass(canonicalName)
    }

    override fun hashCode(): Int {
        return canonicalName.hashCode()
    }

    override fun toString(): String {
        return when {
            isVoid -> "void"
            isArray -> "$canonicalName[]"
            genericParams.isNotEmpty() -> "$canonicalName<${genericParams.joinToString(", ")}>"
            else -> canonicalName
        }
    }

    companion object {
        @JvmStatic
        fun of(canonicalName: String): ClassCd {
            return when {
                canonicalName.endsWith("[]") ->
                    ClassCd(
                        ClassName.of(canonicalName.substring(0, canonicalName.length - 2)),
                        kind = ClassKind.ARRAY
                    )

                canonicalName.endsWith('>') -> {
                    val idx1 = canonicalName.indexOf('<')
                    val genericParams = canonicalName.substring(idx1 + 1, canonicalName.length - 1)
                        .split(",").map { of(it.trim()) }
                    ClassCd(ClassName.of(canonicalName.substring(0, idx1)), genericParams = genericParams)
                }

                canonicalName.length == 1 && canonicalName[0].isUpperCase() ->
                    genericParam(canonicalName)

                else ->
                    ClassCd(ClassName.of(canonicalName))
            }
        }

        @JvmStatic
        fun of(canonicalName: String, genericParams: List<ClassCd>): ClassCd =
            ClassCd(ClassName.of(canonicalName), genericParams = genericParams)


        @JvmStatic
        fun of(packageName: String, className: String): ClassCd =
            ClassCd(ClassName(className, packageName))

        @JvmStatic
        fun genericParam(symbol: String): ClassCd = ClassCd(ClassName(symbol), kind = ClassKind.GENERIC_PARAM)
    }
}
