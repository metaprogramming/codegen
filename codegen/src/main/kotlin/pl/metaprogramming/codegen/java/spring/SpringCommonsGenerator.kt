/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.Model
import pl.metaprogramming.codegen.java.JavaCommonCodesConfigurator
import pl.metaprogramming.codegen.java.JavaCommonTypeOfCode
import pl.metaprogramming.codegen.java.JavaGenerator
import pl.metaprogramming.codegen.java.JavaGeneratorBuilder
import pl.metaprogramming.codegen.java.jaxb.JaxbCommonCodesConfigurator
import pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode
import pl.metaprogramming.codegen.java.spring.rs.SpringRsCommonCodesConfiguration
import pl.metaprogramming.codegen.java.spring.rs.SpringRsCommonTypeOfCode
import pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tCommonCodesConfigurator
import pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tCommonTypeOfCode
import pl.metaprogramming.codegen.java.validation.ValidationCommonCodesConfiguration
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.model.oas.HttpResponse

private val HTTP_STATUSES: List<Int> = listOf(HttpResponse.DEFAULT) + (100..102) +
        (200..208) + 226 +
        (300..308) +
        (400..418) + (422..426) + (449..451) + 420 + 428 + 429 + 431 + 444 + 499 +
        (500..511) + 598 + 599

class SpringCommonsGenerator : JavaGeneratorBuilder<Model, SpringCommonsGenerator.TOC, SpringCommonsGenerator>(TOC) {

    object TOC {
        @JvmField val JAXB_LOCAL_DATE_ADAPTER = JaxbTypeOfCode.LOCAL_DATE_ADAPTER
        @JvmField val JAXB_LOCAL_DATE_TIME_ADAPTER = JaxbTypeOfCode.LOCAL_DATE_TIME_ADAPTER
        @JvmField val VALIDATION_CONTEXT = ValidationCommonTypeOfCode.VALIDATION_CONTEXT
        @JvmField val VALIDATION_FIELD = ValidationCommonTypeOfCode.VALIDATION_FIELD
        @JvmField val VALIDATION_CHECKER = ValidationCommonTypeOfCode.VALIDATION_CHECKER
        @JvmField val VALIDATION_COMMON_CHECKERS = ValidationCommonTypeOfCode.VALIDATION_COMMON_CHECKERS
        @JvmField val VALIDATION_ERROR = ValidationCommonTypeOfCode.VALIDATION_ERROR
        @JvmField val VALIDATION_RESULT = ValidationCommonTypeOfCode.VALIDATION_RESULT
        @JvmField val VALIDATION_RESULT_MAPPER = SpringRs2tCommonTypeOfCode.VALIDATION_RESULT_MAPPER
        @JvmField val VALIDATION_EXCEPTION = ValidationCommonTypeOfCode.VALIDATION_EXCEPTION
        @JvmField val VALIDATION_VALIDATOR = ValidationCommonTypeOfCode.VALIDATION_VALIDATOR
        @JvmField val VALIDATION_DICTIONARY_CHECKER = ValidationCommonTypeOfCode.VALIDATION_DICTIONARY_CHECKER
        @JvmField val VALIDATION_DICTIONARY_CODE_ENUM = ValidationCommonTypeOfCode.VALIDATION_DICTIONARY_CODE_ENUM
        @JvmField val VALIDATION_ERROR_CODE_ENUM = ValidationCommonTypeOfCode.VALIDATION_ERROR_CODE_ENUM
        @JvmField val VALIDATION_SECURITY_PRIVILEGE_ENUM = ValidationCommonTypeOfCode.SECURITY_PRIVILEGE_ENUM
        @JvmField val VALIDATION_BOOL_EXP = ValidationCommonTypeOfCode.BOOL_EXP
        @JvmField val VALIDATION_CONDITIONAL_CHECKER = ValidationCommonTypeOfCode.CONDITIONAL_CHECKER
        @JvmField val VALIDATION_CONDITION_ENUM = ValidationCommonTypeOfCode.CONDITION_ENUM
        @JvmField val VALIDATION_CONDITION_RESOLVER = ValidationCommonTypeOfCode.CONDITION_RESOLVER
        @JvmField val VALIDATION_OPERATION_ID_ENUM = ValidationCommonTypeOfCode.OPERATION_ID_ENUM
        @JvmField val REST_EXCEPTION_HANDLER = ValidationCommonTypeOfCode.REST_EXCEPTION_HANDLER
        @JvmField val ENDPOINT_PROVIDER = SpringCommonTypeOfCode.ENDPOINT_PROVIDER
        @JvmField val ENUM_VALUE_INTERFACE = JavaCommonTypeOfCode.ENUM_VALUE_INTERFACE
        @JvmField val REST_RESPONSE_ABSTRACT = SpringRs2tCommonTypeOfCode.REST_RESPONSE_ABSTRACT
        @JvmField val REST_RESPONSE_INTERFACE = SpringRs2tCommonTypeOfCode.REST_RESPONSE_INTERFACE
        @JvmField val REST_RESPONSE_STATUS_INTERFACE = SpringRs2tCommonTypeOfCode.REST_RESPONSE_STATUS_INTERFACE
        @JvmField val REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE =
            SpringRs2tCommonTypeOfCode.REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE
        @JvmField val MAP_RAW_VALUE_SERIALIZER = SpringRs2tCommonTypeOfCode.MAP_RAW_VALUE_SERIALIZER
        @JvmField val SERIALIZATION_UTILS = SpringRsCommonTypeOfCode.SERIALIZATION_UTILS

        @JvmField val REST_CLIENT = SpringRsCommonTypeOfCode.REST_CLIENT
        @JvmField val REST_CLIENT_EXCEPTION = SpringRsCommonTypeOfCode.REST_CLIENT_EXCEPTION

        @JvmField val URLENCODED_OBJECT_MAPPER = SpringCommonTypeOfCode.URLENCODED_OBJECT_MAPPER

        @JvmField val TEST_DATA = JavaCommonTypeOfCode.TEST_DATA
    }

    class Generator(generatorBuilder: SpringCommonsGenerator) :
        JavaGenerator<Model>(generatorBuilder) {

        override fun generate() {
            HTTP_STATUSES.forEach { httpStatus ->
                register(TOC.REST_RESPONSE_STATUS_INTERFACE, httpStatus)
                register(TOC.REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE, httpStatus)
            }

            register(TOC.JAXB_LOCAL_DATE_ADAPTER)
            register(TOC.JAXB_LOCAL_DATE_TIME_ADAPTER)

            register(TOC.VALIDATION_CONTEXT)
            register(TOC.VALIDATION_FIELD)
            register(TOC.VALIDATION_CHECKER)
            register(TOC.VALIDATION_COMMON_CHECKERS)
            register(TOC.VALIDATION_ERROR)
            register(TOC.VALIDATION_RESULT)
            register(TOC.VALIDATION_RESULT_MAPPER)
            register(TOC.VALIDATION_EXCEPTION)
            register(TOC.VALIDATION_VALIDATOR)
            register(TOC.VALIDATION_DICTIONARY_CHECKER)
            register(TOC.VALIDATION_DICTIONARY_CODE_ENUM)
            register(TOC.VALIDATION_ERROR_CODE_ENUM)
            register(TOC.VALIDATION_SECURITY_PRIVILEGE_ENUM)
            register(TOC.VALIDATION_BOOL_EXP)
            register(TOC.VALIDATION_CONDITIONAL_CHECKER)
            register(TOC.VALIDATION_CONDITION_ENUM)
            register(TOC.VALIDATION_CONDITION_RESOLVER)
            register(TOC.VALIDATION_OPERATION_ID_ENUM)

            register(TOC.REST_EXCEPTION_HANDLER)

            register(TOC.ENDPOINT_PROVIDER)
            register(TOC.ENUM_VALUE_INTERFACE)
            register(TOC.REST_RESPONSE_ABSTRACT)
            register(TOC.REST_RESPONSE_INTERFACE)
            register(TOC.MAP_RAW_VALUE_SERIALIZER)
            register(TOC.SERIALIZATION_UTILS)

            register(TOC.REST_CLIENT)
            register(TOC.REST_CLIENT_EXCEPTION)
            register(TOC.URLENCODED_OBJECT_MAPPER)
            register(TOC.TEST_DATA)

            makeCodeModels()
        }
    }

    override fun init() {
        model = Model.empty("Commons")
        ValidationCommonCodesConfiguration().configure(this)
        JaxbCommonCodesConfigurator().configure(this)
        JavaCommonCodesConfigurator().configure(this)
        SpringRs2tCommonCodesConfigurator().configure(this)
        SpringCommonCodesConfiguration().configure(this)
        SpringRsCommonCodesConfiguration().configure(this)
    }

    override fun make(): pl.metaprogramming.codegen.Generator<Model> {
        // force instantiate ValidationParams if not set before - required by FreeMarker template generators
        params[ValidationParams::class]
        return Generator(this)
    }
}

