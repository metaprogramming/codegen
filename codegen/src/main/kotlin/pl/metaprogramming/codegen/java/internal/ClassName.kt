/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.internal

internal class ClassName(
    val name: String,
    val classPackage: String? = null
) {
    companion object {
        @JvmStatic
        fun of(canonicalName: String): ClassName {
            val parts = canonicalName.split('.')
            val pkgParts = if (parts.size == 1) null
            else (parts.size - 1 downTo 1).find { parts[it - 1].first().isLowerCase() }
            return ClassName(
                parts.subList(pkgParts ?: 0, parts.size).joinToString("."),
                pkgParts?.let { parts.subList(0, pkgParts).joinToString(".") }
            )
        }
    }

    val canonicalName: String
        get() = if (classPackage == null) name else "${classPackage}.${name}"

}