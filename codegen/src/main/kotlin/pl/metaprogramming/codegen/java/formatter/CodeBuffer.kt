/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter

import pl.metaprogramming.codegen.NEW_LINE

class CodeBuffer(
    private val newLine: String = NEW_LINE,
    private val tab: String = TAB,
    private var buf: StringBuilder = StringBuilder(),
    private var indent: String = "",
) {
    fun take(): String {
        val result = buf.toString()
        reset()
        return result
    }

    fun reset() = apply {
        buf = StringBuilder()
        indent = ""
    }

    override fun toString(): String = buf.toString()

    @JvmOverloads
    fun indent(tabs: Int = 0) = apply { indent = tab.repeat(tabs) }
    fun indentInc(tabsChange: Int = 0) = apply {
        if (tabsChange >= 0) {
            indent += tab.repeat(tabsChange)
        } else {
            indent = indent.substring(0, indent.length + (tabsChange * tab.length))
        }
    }

    fun add(text: CharSequence) = apply { buf.append(text) }
    operator fun plus(line: CharSequence) = newLine(line)

    @JvmOverloads
    fun newLine(text: CharSequence = "") = apply {
        if (buf.isNotEmpty() || text.isEmpty()) buf.append(newLine)
        if (indent.isNotEmpty()) buf.append(indent)
        buf.append(text)
    }

    fun addLines(vararg lines: String) = apply {
        if (lines.size == 1) {
            addLines(lines[0] as CharSequence)
        } else {
            lines.forEach(::newLine)
        }
    }

    fun addLines(lines: Collection<String>) = apply { lines.forEach(::newLine) }

    fun addLines(lines: CharSequence) = addLines(lines.lines())

    fun addFirstLine(line: CharSequence)  = apply {
        if (buf.isNotEmpty()) {
            buf = StringBuilder().append(line).append(newLine).append(buf)
        } else {
            buf.append(line)
        }
    }

    fun block(header: String, vararg lines: String) = apply {
        newLine("$header {")
        indentInc(1)
        addLines(*lines)
    }

    fun endBlock() = apply {
        indentInc(-1)
        newLine("}")
    }

    fun ifBlock(condition: String, vararg lines: String) = block("if ($condition)", *lines)

    fun ifElseValue(condition: String, ifValue: String, elseValue: String) = apply {
        newLine(condition)
        indentInc(2)
        addLines("? $ifValue", ": $elseValue")
        indentInc(-2)
    }

    fun tryBlock(vararg lines: String) = block("try", *lines)

    fun catchBlock(exceptions: String, vararg lines: String) = apply {
        indentInc(-1)
        newLine("} catch ($exceptions) {")
        indentInc(1)
        addLines(*lines)
    }
}