/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.CodeGenerationTask
import pl.metaprogramming.codegen.java.formatter.JavaCodeFormatter

class ClassCmBuildDirector<M>(
    private val context: BuildContext<M>,
    private val builders: List<IClassCmBuilder<M>>
) : ClassBuilder<M>(context.model, context.typeOfCode, context.classCm, context.generator) {

    override fun makeGenerationTask(filePath: String): CodeGenerationTask<*> =
        CodeGenerationTask(filePath, context.classCm, JavaCodeFormatter())

    override fun makeDecoration() = perform("makeDecoration") { it.makeDecoration(context) }
    override fun makeDeclaration() = perform("makeDeclaration") { it.makeDeclaration(context) }

    override fun makeImplementation() = perform("makeImplementation") { it.makeImplementation(context) }

    private fun perform(actionName: String, action: (IClassCmBuilder<M>) -> Unit) {
        try {
            builders.forEach { action.invoke(it) }
        } catch (e: Exception) {
            throw IllegalStateException("Can't build (phase: $actionName) $classCd ($typeOfCode for $model)", e)
        }
    }

    override fun toString(): String = "Builder of $typeOfCode for $${context.model} - $classCd"
}