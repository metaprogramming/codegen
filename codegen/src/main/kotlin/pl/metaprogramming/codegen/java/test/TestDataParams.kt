/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.test

import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.data.DataTypeCode.*

class TestDataParams {

    var isEnabled = true
    val valueByCode = mutableMapOf<String, String>()
    val valueByType = mutableMapOf(
        DATE to "date()",
        DATE_TIME to "dateTime()",
        BOOLEAN to "false",
        INT16 to "1",
        INT32 to "1",
        INT64 to "1",
        NUMBER to "1.1",
        DECIMAL to "1.1",
        FLOAT to "1.1",
        DOUBLE to "1.1",
    )

    fun enabled(value: Boolean) = apply { isEnabled = value }
    fun valueByCode(code: String, value: String) = apply { valueByCode[code] = value }
    fun valueByCode(values: Map<String, String>) = apply { valueByCode.putAll(values) }
    fun valueByType(type: DataTypeCode, value: String) = apply { valueByType[type] = value }

    fun copy(): TestDataParams {
        val result = TestDataParams().enabled(isEnabled)
        result.valueByCode.clear()
        result.valueByCode.putAll(valueByCode)
        result.valueByType.clear()
        result.valueByType.putAll(valueByType)
        return result
    }
}