/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.jaxb

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.builders.BaseDtoBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.spring.SpringSoapParams
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.XmlDataType
import pl.metaprogramming.model.data.XmlObjectType

class XmlDtoBuilder : BaseDtoBuilder<XmlObjectType>() {

    override fun makeImplementation() {
        super.makeImplementation()
        addAnnotation(
            AnnotationCm(
                "javax.xml.bind.annotation.XmlAccessorType",
                ValueCm.staticRef("javax.xml.bind.annotation.XmlAccessType.FIELD")
            )
        )
        addAnnotation(model.prepareXmlTypeAnnotation())
        if (model.isRootElement) {
            addAnnotation(model.prepareXmlRootElementAnnotation())
        }
    }

    override fun addField(schema: DataSchema) {
        val typeOfCode = if (schema.isEnumOrItemEnum) JaxbTypeOfCode.ENUM else JaxbTypeOfCode.DTO
        val classCd = getClass(typeOfCode, schema.dataType)
        fields.add(getFieldName(schema), classCd) {
            description = schema.description
            annotations = getFieldAnnotations(schema, classCd)
        }
    }

    private fun getFieldAnnotations(schema: DataSchema, classCd: ClassCd): MutableList<AnnotationCm> {
        val result = mutableListOf<AnnotationCm>()
        result.add(if (schema.required && schema.nillable != true) Java.nonnul() else Java.nullable())
        result.addXmlElementAnnotation(schema)
        if (classCd.canonicalName == "java.time.LocalDate") {
            result.addXmlJavaTypeAdapterAnnotation(JaxbTypeOfCode.LOCAL_DATE_ADAPTER)
        }
        if (classCd.canonicalName == "java.time.LocalDateTime") {
            result.addXmlJavaTypeAdapterAnnotation(JaxbTypeOfCode.LOCAL_DATE_TIME_ADAPTER)
        }
        return result
    }

    private fun MutableList<AnnotationCm>.addXmlElementAnnotation(schema: DataSchema) {
        val xmlElementAttributes = mutableMapOf<String, ValueCm>()
        if (schema.required) {
            xmlElementAttributes["required"] = ValueCm.value("true")
        }
        if (schema.nillable == true) {
            xmlElementAttributes["nillable"] = ValueCm.value("true")
        }
        if (getFieldName(schema) != schema.code) {
            xmlElementAttributes["name"] = ValueCm.escaped(schema.code)
        }
        if (xmlElementAttributes.isNotEmpty()) {
            add(AnnotationCm("javax.xml.bind.annotation.XmlElement", xmlElementAttributes))
        }
    }

    private fun MutableList<AnnotationCm>.addXmlJavaTypeAdapterAnnotation(adapterClassType: Any) = add(
        AnnotationCm(
            "javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter",
            getClass(adapterClassType).classRef()
        )
    )

    private fun XmlObjectType.prepareXmlTypeAnnotation(): AnnotationCm {
        val params = linkedMapOf<String, ValueCm>()
        if (isRootElement) {
            params["name"] = ValueCm.escaped("")
        } else {
            params["name"] = ValueCm.escaped(code)
            addNamespaceParam(params)
        }
        params["propOrder"] = ValueCm.escapedArray(fields.map { getFieldName(it) }, true)
        return AnnotationCm("javax.xml.bind.annotation.XmlType", params)
    }

    private fun XmlObjectType.prepareXmlRootElementAnnotation(): AnnotationCm {
        val params = linkedMapOf("name" to ValueCm.escaped(code))
        addNamespaceParam(params)
        return AnnotationCm("javax.xml.bind.annotation.XmlRootElement", params)
    }

    private fun XmlDataType.addNamespaceParam(params: MutableMap<String, ValueCm>) {
        if (!context.params[SpringSoapParams::class].namespace2Package.containsKey(namespace)) {
            params["namespace"] = ValueCm.escaped(namespace)
        }
    }
}