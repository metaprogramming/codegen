/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.libs.Jackson
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.ParamLocation

private const val VAR_REQUEST = "request"

class RestTemplateCallMethodBuilder(private val operation: Operation) : BaseMethodCmBuilder<Operation>(operation) {

    private lateinit var requestClass: ClassCm
    override fun makeDeclaration(): MethodCm {
        requestClass = classLocator(SpringRsTypeOfCode.REQUEST_DTO).model(model.requestSchema).getDeclared()
        return newMethodCm(nameMapper.toMethodName(model.code)) {
            resultType = ResponseClassResolver.resolve(context, model)
            params.add(VAR_REQUEST, requestClass) { model = operation }
            description = makeDescription()
        }
    }

    override fun makeImplBody(): String {
        dependencies.add(Spring.httpMethod())

        val restClientProvider =
            componentRef(getClass(SpringRsTypeOfCode.REST_CLIENT_PROVIDER, model.api), "clientProvider")
        val callPrefix = if (model.successResponseSchema != null) "return " else ""
        codeBuf.addLines("${callPrefix}${restClientProvider}.of(${Spring.httpMethod(model.method)}, \"${model.path}\")")
        codeBuf.indentInc(2)
        appendParams(ParamLocation.PATH)
        appendParams(ParamLocation.QUERY)
        appendParams(ParamLocation.HEADER)
        appendOnError()
        appendBody()
        codeBuf.addLines(".exchange(${getResponseBodyClassValue()})")
        if (model.successResponseSchema != null && methodCm.resultType != Spring.responseEntity()) {
            codeBuf.addLines(".getBody()")
        }
        return codeBuf.add(";").take()
    }

    private fun appendParams(paramLocation: ParamLocation) {
        val addMethod = "${paramLocation.name.lowercase()}Param"
        model.getParameters(paramLocation).forEach {
            val field = requestClass.fields.getByModel(it)
            val optional = if (it.required || field.type.isList) "" else "Optional"
            val paramExp =
                if (paramLocation.isIn(ParamLocation.HEADER, ParamLocation.QUERY)) field.toStringExp()
                else getter(VAR_REQUEST, field.name)
            val paramName =
                if (paramLocation.isIn(ParamLocation.HEADER)) Spring.httpHeaders(it.name)
                else ValueCm.escaped(it.name)
            if (!paramName.escaped) dependencies.add(Spring.httpHeaders())
            codeBuf.addLines(".${addMethod}${optional}($paramName, $paramExp)")
        }
    }

    private fun FieldCm.toStringExp() = mapping()
        .to(if (type.isList) Java.string().asList() else Java.string())
        .from(withName("${VAR_REQUEST}.${name}"))
        .make()


    private fun appendOnError() {
        codeBuf.addLines(getErrorClasses().map {
            val typeRef = getTypeRef(it.value)
            if (it.key != 0) ".onError(${it.key}, $typeRef)"
            else ".onError($typeRef)"
        })
    }

    private fun getErrorClasses(): Map<Int, ClassCd> {
        val result = mutableMapOf<Int, ClassCd>()
        val defaultErrorResponse = model.api.defaultErrorResponse
        model.errorResponses.forEach {
            val errorDataType = it.schema?.dataType
            if (errorDataType != null && errorDataType != defaultErrorResponse) {
                result[it.status] = getClass(SpringRsTypeOfCode.DTO, errorDataType)
            }
        }
        return result
    }

    private fun getTypeRef(classCd: ClassCd): String {
        dependencies.add(classCd)
        return if (classCd.genericParams.isNotEmpty()) {
            val jacksonTypeRef = Jackson.typeReference(classCd)
            dependencies.add(jacksonTypeRef)
            "new ${classNameFormatter.format(jacksonTypeRef)}(){}"
        } else {
            classNameFormatter.classRef(classCd)
        }
    }

    private fun appendBody() {
        if (!model.isConsumeJson && model.consumes.isNotEmpty()) {
            codeBuf.addLines(".headerParam(\"content-type\", \"${model.consumes.first()}\")")
        }
        if (model.isMultipart) {
            codeBuf.addLines(model.getParameters(ParamLocation.FORMDATA).map {
                ".formDataParam(\"${it.name}\", ${getter(VAR_REQUEST, it.name)})"
            })
            model.multipartFormDataRequestBody?.fields?.forEach {
                codeBuf.addLines(".formDataParam(\"${it.code}\", ${getter(VAR_REQUEST, it.code!!)})")
            }
        } else if (model.requestBodySchema != null) {
            codeBuf.addLines(".body(${getter(VAR_REQUEST, model.requestBodySchema?.code!!)})")
        }
    }

    private fun getResponseBodyClassValue(): String {
        val classCd =
            if (methodCm.resultType == Spring.responseEntity()) methodCm.resultType.genericParams[0]
            else methodCm.resultType
        return if (classCd.genericParams.isNotEmpty()) {
            val responseClassType = classNameFormatter.format(classCd)
            dependencies.add("org.springframework.core.ParameterizedTypeReference")
            "new ParameterizedTypeReference<$responseClassType>() {}"
        } else {
            classNameFormatter.classRef(classCd)
        }
    }


    private fun makeDescription(): String {
        val head = "[${model.method}] ${model.path}"
        return if (model.description.isNotEmpty()) "$head<br/>\n${model.description}" else head
    }
}