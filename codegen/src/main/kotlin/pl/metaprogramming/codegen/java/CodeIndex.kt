/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.Generator
import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.base.ClassBuilder
import pl.metaprogramming.codegen.java.base.IMethodCmBuilder
import pl.metaprogramming.log

private var constructionPhase = false

class CodeIndex {
    private val classIndex = mutableMapOf<ClassEntry.Key, ClassEntry>()
    private val mappersIndex = mutableMapOf<MapperEntry.Key, MapperEntry>()
    private val usedNames = mutableSetOf<String>()

    fun makeCodeModels() {
        makeCodeDeclarations()
        constructionPhase = true
        markAsUsed()
        while (areCodeModelsToBuild()) {
            makeClasses()
            makeMappers()
        }
        constructionPhase = false
    }

    fun classesToGenerate(generator: Generator<*>) = classIndex.values
        .filter { it.builder?.generator == generator && it.isToGenerate() }
        .map { it.builder!! }

    fun put(builder: ClassBuilder<*>) = put(ClassEntry(builder.classCd, builder.typeOfCode, builder.model, builder))

    fun put(builder: ClassBuilder<*>, asTypeOfCode: Any) =
        put(ClassEntry(builder.classCd, asTypeOfCode, builder.model, builder))

    fun put(classCd: ClassCd, model: Any?, asTypeOfCode: TypeOfCode<*>) =
        put(ClassEntry(classCd, asTypeOfCode, model, null))


    fun getClass(typeOfCode: Any, model: Any?, forceDeclaration: Boolean): ClassCd? =
        getClassByKey(ClassEntry.Key(typeOfCode, model), constructionPhase, forceDeclaration)
            ?: if (model != null) getClassByKey(
                ClassEntry.Key(typeOfCode, null),
                constructionPhase,
                forceDeclaration
            ) else null

    fun markAsUsed(typeOfCode: Any, model: Any? = null): ClassCd =
        getClassByKey(toClassKey(typeOfCode, model), true)!!

    fun putMappers(vararg methods: MethodCm) = methods.forEach { addMapper(MapperEntry(it)) }
    fun putMapper(mapperBuilder: IMethodCmBuilder) = addMapper(MapperEntry(mapperBuilder))
    fun putMapper(canonicalMethodPath: String, resultType: ClassCd, vararg paramsTypes: ClassCd) {
        val splitIdx = canonicalMethodPath.lastIndexOf('.')
        val methodName = canonicalMethodPath.substring(splitIdx + 1)
        val className = canonicalMethodPath.substring(0, splitIdx)
        putMappers(MethodCm(methodName, className.asClassCd()) {
            this.resultType = resultType
            paramsTypes.forEach { params.add(it.asField()) }
        })
    }

    fun getMapper(resultType: ClassCd, params: List<ClassCd>, failIfNotFound: Boolean = true): MethodCm? {
        val key = MapperEntry.Key(resultType, params)
        val result = getMapper(key)
        if (result != null) return result.methodCm
        check(!failIfNotFound) { "Can't find mapper $key" }
        return null
    }

    private fun getMapper(key: MapperEntry.Key): MapperEntry? =
        mappersIndex[key]?.let {
            it.markAsUsed()
            it
        }


    private fun makeCodeDeclarations() = classIndex.values.forEach { it.makeDeclaration() }

    private fun markAsUsed() {
        // based on always generated classes mark other classes as used
        classIndex.values.forEach { if (it.isToMake()) it.markAsUsed() }
    }

    private fun areCodeModelsToBuild(): Boolean = classIndex.values.any { it.isToMake() }
            || mappersIndex.values.any { it.isToMake() }

    private fun makeClasses() {
        classIndex.values.forEach { if (it.isToMake()) it.makeImplementation() }
    }

    private fun makeMappers() {
        mappersIndex.values.filter { it.isToMake() }.forEach {
            log.debug("Going to makeImplementation for {}", it.methodCm)
            it.makeImplementation()
        }
    }

    private fun put(entry: ClassEntry) {
        val canonicalName = entry.clazz.canonicalName
        if (usedNames.contains(canonicalName)) {
            throw ClassAlreadyRegistered("$canonicalName already registered")
        }
        classIndex[entry.key]?.let {
            throw ClassAlreadyRegistered("$canonicalName is the same as ${it.clazz} [${it.key.typeOfCode} - ${it.key.model}]")
        }
        usedNames.add(canonicalName)
        classIndex[entry.key] = entry
    }

    private fun getClassByKey(key: ClassEntry.Key, markAsUsed: Boolean, forceDeclaration: Boolean = false): ClassCd? =
        classIndex[key]?.let {
            if (forceDeclaration) it.makeDeclaration()
            if (markAsUsed) it.clazz.markAsUsed()
            it.clazz
        }


    private fun toClassKey(typeOfCode: Any, model: Any? = null) = ClassEntry.Key(typeOfCode, model)

    private fun addMapper(mapper: MapperEntry) {
        if (mappersIndex.containsKey(mapper.key)) {
            val exists = mappersIndex.getValue(mapper.key).methodCm
            check(exists.ownerClass == mapper.methodCm.ownerClass && exists.name == mapper.methodCm.name) {
                printIndex()
                "Mapper already defined ${mapper.key}"
            }
            return
        }
        mappersIndex[mapper.key] = mapper
    }

    fun printIndex() {
        log.info("Code index")
        logSpace()
        logItems(collectClassesToGenerate())
        logItems(collectMappers())
    }

    private fun collectClassesToGenerate() = classIndex.filter { it.value.isToGenerate() }
        .map { "${it.value.clazz.canonicalName} (${it.key.typeOfCode} for ${it.key.model})" }
        .sorted()

    private fun collectMappers() = mappersIndex.map { "$it.key: $it.value" }.sorted()

    private fun logItems(items: List<*>) {
        items.forEach { log.info("\t$it") }
        logSpace()
    }

    private fun logSpace() = log.info("----------------------------------------------")

    class ClassAlreadyRegistered(message: String) : Exception(message)
}

private class ClassEntry(val clazz: ClassCd, val typeOfCode: Any, val model: Any?, val builder: ClassBuilder<*>?) {
    val key: Key by lazy { Key(typeOfCode, model) }
    private var declarationMade: Boolean = false
    private var built: Boolean = false

    fun isToGenerate() = builder != null && clazz.isToGenerate
    fun isToMake() = !built && isToGenerate()
    fun markAsUsed() = clazz.markAsUsed()
    fun makeImplementation() {
        if (!built) {
            built = true
            builder?.makeImplementation()
        }
    }

    fun makeDeclaration() {
        if (!declarationMade) {
            declarationMade = true
            builder?.makeDeclaration()
        }
    }

    data class Key(val typeOfCode: Any, val model: Any?) {
        override fun toString() = if (model != null) "$typeOfCode:$model" else typeOfCode.toString()
    }

    override fun toString() = clazz.toString()
}

private class MapperEntry(val methodCm: MethodCm, val builder: IMethodCmBuilder? = null) {
    private var built: Boolean = false
    val key: Key = Key(
        methodCm.resultType,
        if (methodCm.params.isNotEmpty()) methodCm.params.map { it.type } else listOf(methodCm.ownerClass))

    constructor(builder: IMethodCmBuilder) : this(builder.methodCm, builder)

    override fun toString() = methodCm.toString()
    fun markAsUsed() = methodCm.markAsUsed()
    fun isToMake() = !built && builder != null && (methodCm.used || methodCm.owner.forceGeneration)

    fun makeImplementation() {
        if (!built) {
            built = true
            builder!!.makeImplementation()
        }
    }

    data class Key(val inToOut: String) {
        constructor(to: ClassCd?, from: List<ClassCd>) : this("($from) -> $to")

        override fun toString() = inToOut
    }
}