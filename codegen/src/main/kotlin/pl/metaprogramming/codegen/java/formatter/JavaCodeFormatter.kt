/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter

import pl.metaprogramming.codegen.CodeFormatter
import pl.metaprogramming.codegen.NEW_LINE
import pl.metaprogramming.codegen.java.*
import java.util.*
import java.util.function.Predicate

internal class JavaCodeFormatter : BaseJavaCodeFormatter(), CodeFormatter<ClassCm> {

    override fun format(codeModel: ClassCm): String {
        initImports(codeModel)
        init(codeModel.packageName)

        buf.add("package ${codeModel.packageName};").newLine()
        buf.addLines(imports.map { "import $it;" }).newLine()

        ClassFormatter(codeModel, 0).format()

        return buf.take()
    }

    private fun initImports(classCm: ClassCm) {
        val excludePackages = mutableSetOf(classCm.packageName, JAVA_LANG_PKG)
        dependencies = classCm.dependencies
        imports = dependencies.asImports(excludePackages).distinct().sorted()
    }

    private fun addJavadoc(comment: String?) {
        if (comment.isNullOrEmpty()) return
        buf.newLine("/**")
        comment.trim().lines().forEach { buf.newLine(" * $it") }
        buf.newLine(" */")
    }

    private fun addField(field: FieldCm) {
        if (field.description != null) {
            buf.newLine()
            addJavadoc(field.description)
        }
        buf.newLine().add(formatField(field))
        buf.add(";")
    }

    private fun formatField(field: FieldCm): String {
        val tokens = mutableListOf<String>()
        field.annotations.forEach { tokens.add(formatAnnotation(it)) }
        tokens.add(field.accessModifier.name.lowercase(Locale.getDefault()))
        if (field.isStatic) tokens.add("static")
        if (field.isFinal) tokens.add("final")
        if (field.isTransient) tokens.add("transient")
        tokens.add(formatTypeName(field))
        if (field.value != null) {
            tokens.add("=")
            tokens.add(field.value.toString())
        }
        return tokens.joinToString(SPACE)
    }

    private fun formatTypeName(fieldCm: FieldCm) =
        if (fieldCm.vararg) {
            "${fieldCm.type.className}... ${fieldCm.name}"
        } else {
            "${formatUsage(fieldCm.type)} ${fieldCm.name}"
        }

    fun MethodCm.signature(): String {
        val builder = ConcatenationBuilder()
        addModifiers(builder)
        builder.append(genericParams())
        if (!isConstructor) {
            builder.append(formatUsage(resultType))
        }
        val params = params.joinToString(", ") {
            "${formatAnnotations(it.annotations, SPACE)}${formatTypeName(it)}"
        }
        builder.append("${name}($params)")
        if (throwExceptions.isNotEmpty()) {
            builder.append(NEW_LINE)
            builder.append("throws")
            builder.append(throwExceptions.joinToString(", ") { it.className })
        }
        return builder.toString()
    }


    private fun MethodCm.hasImplBody() = implBody != null && implBody!!.isNotEmpty()

    private fun MethodCm.addModifiers(builder: ConcatenationBuilder) {
        if (ownerClass.isInterface) {
            if (hasImplBody() && !isStatic) builder.append("default")
        } else {
            if (isPublic) builder.append("public")
            if (isPrivate && !(isConstructor && ownerClass.isEnum)) {
                builder.append("private")
            }
        }
        if (isStatic) builder.append("static")
    }

    private fun MethodCm.genericParams(): String {
        val genericParams = mutableSetOf<ClassCd>()
        collectGenericParams(resultType, genericParams)
        params.forEach { collectGenericParams(it.type, genericParams) }
        if (!isStatic && ownerClass.genericParams.isNotEmpty()) {
            genericParams.removeAll(ownerClass.genericParams.toSet())
        }
        return formatGenericParams(genericParams)
    }

    private fun collectGenericParams(cm: ClassCd, result: MutableSet<ClassCd>) {
        if (cm.isGenericParam) {
            result.add(cm)
        } else {
            cm.genericParams.forEach { collectGenericParams(it, result) }
        }
    }


    fun ClassCm.formatHeader(): String {
        val buf = StringBuilder()
        if (isPublic) buf.append("public ")
        if (isPrivate) buf.append("private ")
        if (isAbstract) buf.append("abstract ")
        buf.append(if (isInterface) "interface" else if (isEnum) "enum" else "class")
        buf.append(' ').append(className.split(".").last())
        buf.append(formatGenericParams(genericParams))
        if (superClass != null) {
            buf.append(" extends ${formatUsage(superClass)}")
        }
        if (interfaces.isNotEmpty()) {
            buf.append(if (isInterface) " extends " else " implements ")
            buf.append(interfaces.joinToString(", ") { formatUsage(it) })
        }
        return buf.toString()
    }


    private inner class ClassFormatter(val classCm: ClassCm, val level: Int) {

        fun format() {
            addJavadoc(classCm.description)
            buf.addLines(formatAnnotations(classCm.annotations)).newLine()
            buf.add(classCm.formatHeader()).add(" {")

            addEnumItems()
            addFields()
            addMethods()

            classCm.innerClasses.forEach {
                addIndent()
                ClassFormatter(it, level + 1).format()
            }

            buf.indent(level).newLine("}").newLine()
        }


        private fun addEnumItems() {
            var prevItem: EnumItemCm? = null
            classCm.enumItems.forEach {
                if (prevItem == null) {
                    addIndent()
                } else {
                    buf.add(",")
                    prevItem?.description?.let { desc -> buf.add(" // $desc") }
                }
                buf.addLines(formatAnnotations(it.annotations))
                buf.newLine(it.name)
                if (it.value != null) buf.add("(${it.value})")
                prevItem = it
            }
            if (prevItem != null) {
                buf.add(";")
                prevItem?.description?.let { desc -> buf.add(" // $desc") }
            }
        }

        private fun addFields() {
            addFields { it.isFinal && it.value != null && !it.isPrivate }
            addFields { it.isFinal && it.value != null && it.isPrivate }
            addFields { !(it.isFinal && it.value != null) }
        }

        private fun addFields(predicate: Predicate<FieldCm>) {
            val fields = classCm.fields.filter { predicate.test(it) }
            if (fields.isNotEmpty()) {
                addIndent()
                fields.forEach { addField(it) }
            }
        }

        private fun addMethods() {
            classCm.getMethodsToGenerate().forEach {
                addIndent()
                addJavadoc(it.description)
                buf.addLines(formatAnnotations(it.annotations))
                if (classCm.isInterface && !it.hasImplBody()) {
                    buf.newLine(it.signature()).add(";")
                } else {
                    buf.newLine("${it.signature()} {")
                    if (it.hasImplBody()) {
                        buf.indentInc(1).addLines(it.implBody!!).indentInc(-1)
                    }
                    buf.newLine("}")
                }
            }
        }

        private fun addIndent() {
            buf.indent(level).newLine().indent(level + 1)
        }
    }

}

private class ConcatenationBuilder {
    private val builder = StringBuilder()

    fun append(text: String?) = apply {
        val trimmed = text?.trim().orEmpty()
        if (trimmed.isNotEmpty()) {
            if (builder.isNotEmpty()) builder.append(' ')
            builder.append(text)
        }
    }

    override fun toString() = builder.toString()
}
