/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.builders

import pl.metaprogramming.codegen.java.JavaCommonTypeOfCode
import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.model.data.EnumType

open class EnumBuilder<M : EnumType> : BaseEnumBuilder<M>() {

    override fun makeDeclaration() {
        super.makeDeclaration()
        val enumInterface = getClass(JavaCommonTypeOfCode.ENUM_VALUE_INTERFACE)
        implementationOf(enumInterface)
        methods.add("fromValue") {
            registerAsMapper()
            if (params<JavaParams>().isAlwaysGenerateEnumFromValueMethod) forceGeneration()
            isStatic = true
            resultType = classCm
            params.add("value", Java.string())
            implBody = "return ${enumInterface.className}.fromValue(value, ${classCm.className}.class);"
        }
    }
}