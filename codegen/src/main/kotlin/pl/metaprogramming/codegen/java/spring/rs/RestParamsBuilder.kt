/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.base.BuildContext
import pl.metaprogramming.codegen.java.libs.JakartaBeanValidation
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.oas.*
import pl.metaprogramming.upperFirstChar

private val URL_ENCODED_FORM_BODY_TYPE = Spring.multiValueMap(Java.string(), Java.string())

private const val REQUEST_BODY_ANNOTATION_CLASS = "org.springframework.web.bind.annotation.RequestBody"
private val REQUEST_BODY_ANNOTATION = AnnotationCm(REQUEST_BODY_ANNOTATION_CLASS)
private val REQUEST_BODY_OPTIONAL_ANNOTATION =
    AnnotationCm(REQUEST_BODY_ANNOTATION_CLASS, "required" to ValueCm.value("false"))
private val REQUEST_PARAM_ANNOTATION = AnnotationCm.of("org.springframework.web.bind.annotation.RequestParam")

private val PARAM_ANNOTATIONS = mapOf(
    ParamLocation.PATH to "PathVariable",
    ParamLocation.QUERY to "RequestParam",
    ParamLocation.HEADER to "RequestHeader",
    ParamLocation.COOKIE to "CookieValue",
)

class RestParamsBuilder(
    private val builder: BuildContext<*>,
    private val bodyTypeOfCode: TypeOfCode<*>,
) {
    lateinit var operation: Operation
    var addControllerAnnotations = false
    var useDataAutoconversion = false
    var useJakartaBeanValidation = false
    var urlEncoded = false

    @JvmOverloads
    fun makeControllerMethod(
        operation: Operation,
        requestSchema: HttpRequestSchema = operation.requestSchema
    ): MethodCm {
        this.operation = operation
        addControllerAnnotations = true
        return MethodCm.of(builder.nameMapper.toMethodName(operation.code), builder.classCm).setup {
            resultType = Spring.responseEntity()
            params.addAll(make(requestSchema))
            annotations.add(makeMethodAnnotation(requestSchema))
            description = operation.description
        }
    }

    fun make(operation: Operation): List<FieldCm> {
        this.operation = operation
        return make(operation.requestSchema)
    }

    private fun make(requestSchema: HttpRequestSchema): List<FieldCm> {
        val params = mutableListOf<FieldCm>()
        val payloadField = builder.params[SpringRestParams::class].payloadField
        if (payloadField != null && requestSchema.bodySchema != null) {
            params.add(Java.string().asField(payloadField) {
                addAnnotation(getRequestBodyAnnotation())
            })
        }

        requestSchema.fields.forEach { schema ->
            val javaName = builder.getFieldName(schema)
            if (schema is Parameter) {
                params.add(schema.getParamType().asField(javaName) {
                    model = schema
                    annotations = makeParamAnnotations(schema, javaName)
                })
            } else {
                // request body
                val reqBodyType =
                    if (urlEncoded) URL_ENCODED_FORM_BODY_TYPE
                    else if (schema.isBinary) Spring.resource()
                    else builder.getClass(bodyTypeOfCode, schema.dataType)
                params.add(FieldCm.of(javaName, reqBodyType) {
                    model = schema
                    annotations = makeBodyParamAnnotations()
                })
            }
        }
        return params
    }

    private fun makeMethodAnnotation(requestSchema: HttpRequestSchema): AnnotationCm {
        val params = linkedMapOf("value" to ValueCm.escaped(operation.path))
        if (operation.produces.isNotEmpty()) {
            params["produces"] = ValueCm.escapedArray(operation.produces)
        }
        if (operation.method.hasBody()) {
            params["consumes"] =
                if (urlEncoded) ValueCm.escapedArray(listOf(HttpContentTypes.X_WWW_FORM_URLENCODED))
                else ValueCm.escapedArray(requestSchema.withoutFormUrlEncoded())
        }
        val httpMethod = operation.method.name.lowercase().upperFirstChar()
        return AnnotationCm("org.springframework.web.bind.annotation.${httpMethod}Mapping", params)
    }

    private fun DataSchema.getParamType(): ClassCd = if (isType(DataTypeCode.BINARY)) {
        Spring.multipartFile()
    } else if (isEnum) {
        Java.string()
    } else {
        builder.getClass(bodyTypeOfCode, dataType)
    }

    private fun makeBodyParamAnnotations(): MutableList<AnnotationCm> {
        if (!addControllerAnnotations) {
            return mutableListOf()
        }
        val result = mutableListOf(getRequestBodyAnnotation())
        if (useJakartaBeanValidation) {
            result.add(JakartaBeanValidation.VALID)
        }
        return result
    }

    private fun makeParamAnnotations(param: Parameter, javaName: String): MutableList<AnnotationCm> {
        if (!addControllerAnnotations) {
            return mutableListOf()
        }
        val params = mutableMapOf<String, ValueCm>()
        if (param.name != javaName) params["value"] = ValueCm.escaped(param.name)
        if (!(useDataAutoconversion && param.required)) {
            params["required"] = ValueCm.value("false")
        }
        val annotation = param.location.getAnnotation()
        val result = mutableListOf<AnnotationCm>()
        result.add(AnnotationCm("org.springframework.web.bind.annotation.$annotation", params))
        if (useDataAutoconversion) {
            if (param.isTypeOrItemType(DataTypeCode.DATE_TIME)) {
                result.add(makeDateTimeFormatAnnotation("DATE_TIME"))
            } else if (param.isTypeOrItemType(DataTypeCode.DATE)) {
                result.add(makeDateTimeFormatAnnotation("DATE"))
            }
        }
        return result
    }

    private fun ParamLocation.getAnnotation(): String =
        if (this == ParamLocation.FORMDATA) "RequestPart"
        else PARAM_ANNOTATIONS[this] ?: error("[$operation.code] Can't handle parameter location $this")

    private fun makeDateTimeFormatAnnotation(isoFormat: String) = AnnotationCm(
        "org.springframework.format.annotation.DateTimeFormat",
        "iso" to ValueCm.value("DateTimeFormat.ISO.$isoFormat")
    )

    private fun getRequestBodyAnnotation(): AnnotationCm =
        if (urlEncoded) REQUEST_PARAM_ANNOTATION
        else if (operation.requestBody?.required == true) REQUEST_BODY_ANNOTATION
        else REQUEST_BODY_OPTIONAL_ANNOTATION
}