/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.jaxb

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.EnumItemCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.builders.EnumBuilder
import pl.metaprogramming.codegen.java.spring.SpringSoapParams
import pl.metaprogramming.model.data.XmlEnumType

class XmlEnumBuilder : EnumBuilder<XmlEnumType>() {

    override fun makeImplementation() {
        super.makeImplementation()
        addAnnotation(AnnotationCm("javax.xml.bind.annotation.XmlEnum"))
        val xmlTypeParams = linkedMapOf("name" to ValueCm.escaped(model.code))
        if (!context.params[SpringSoapParams::class].namespace2Package.containsKey(model.namespace)) {
            xmlTypeParams["namespace"] = ValueCm.escaped(model.namespace)
        }
        addAnnotation(AnnotationCm("javax.xml.bind.annotation.XmlType", xmlTypeParams))
    }

    override fun addEnumItem(value: String): EnumItemCm {
        val item = super.addEnumItem(value)
        if (value != item.name) {
            item.annotations.add(AnnotationCm("javax.xml.bind.annotation.XmlEnumValue", ValueCm.escaped(value)))
        }
        return item
    }
}