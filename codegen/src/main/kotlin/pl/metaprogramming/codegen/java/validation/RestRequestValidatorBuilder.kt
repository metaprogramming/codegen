/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.model.oas.HttpRequestSchema

class RestRequestValidatorBuilder(
    private val dtoTypeOfCode: TypeOfCode<*>,
    private val enumTypeOfCode: TypeOfCode<*>,
    private val validatorTypeOfCode: TypeOfCode<*>,
    private val addDataTypeValidations: Boolean,
) : ClassCmBuilderTemplate<HttpRequestSchema>() {
    override fun makeDeclaration() {
        context.registerMapper(classCm, "validate") {
            resultType = getClass(ValidationCommonTypeOfCode.VALIDATION_RESULT)
            params.add(getClass(dtoTypeOfCode).asField())
        }
    }

    override fun makeImplementation() {
        val dtoClass = classLocator(dtoTypeOfCode).getDeclared()
        superClass = classLocator(ValidationCommonTypeOfCode.VALIDATION_VALIDATOR).getGeneric(dtoClass)

        val metaFieldBuilder = MetaFieldBuilder(fields, dtoClass, getClass(ValidationCommonTypeOfCode.VALIDATION_FIELD))
        model.fields.forEach { metaFieldBuilder.add(it) }

        methods.add(
            ValidationMethodBuilder(
                model.schema,
                dtoClass,
                validatorTypeOfCode,
                enumTypeOfCode,
                addDataTypeValidations,
                model.operation
            )
        )
    }
}