/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.model.data.EnumType

private val JSON_VALUE = AnnotationCm("com.fasterxml.jackson.annotation.JsonValue")
class EnumBuilder : pl.metaprogramming.codegen.java.builders.EnumBuilder<EnumType>() {
    override fun makeImplementation() {
        super.makeImplementation()
        classCm.fields[0].annotations.add(JSON_VALUE)
    }
}