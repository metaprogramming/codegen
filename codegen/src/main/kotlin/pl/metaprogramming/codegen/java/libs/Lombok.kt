/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.libs

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate

object Lombok {
    private val GETTER = AnnotationCm("lombok.Getter")
    private val REQUIRED_ARGS_CONSTRUCTOR = AnnotationCm("lombok.RequiredArgsConstructor")
    private val ALL_ARGS_CONSTRUCTOR = AnnotationCm("lombok.AllArgsConstructor")
    private val EQUALS_AND_HASH_CODE_CALL_SUPER =
        AnnotationCm("lombok.EqualsAndHashCode", "callSuper" to ValueCm.value("true"))
    private val DATA = AnnotationCm("lombok.Data")
    private val BUILDER = AnnotationCm("lombok.Builder")
    private val VALUE = AnnotationCm("lombok.Value")
    private val NO_ARGS_CONSTRUCTOR = AnnotationCm("lombok.NoArgsConstructor")
    private val ACCESSORS_CHAIN = AnnotationCm("lombok.experimental.Accessors", "chain" to ValueCm.value("true"))
    private val SNEAKY_THROWS = AnnotationCm("lombok.SneakyThrows")

    @JvmStatic
    fun getter() = GETTER

    @JvmStatic
    fun builder() = BUILDER

    @JvmStatic
    fun value() = VALUE

    @JvmStatic
    fun requiredArgsConstructor() = REQUIRED_ARGS_CONSTRUCTOR

    @JvmStatic
    fun allArgsConstructor() = ALL_ARGS_CONSTRUCTOR

    @JvmStatic
    fun <M> dataBuilder() = object : ClassCmBuilderTemplate<M>() {
        override fun makeDecoration() {
            addAnnotation(DATA)
            addAnnotation(NO_ARGS_CONSTRUCTOR)
            addAnnotation(ACCESSORS_CHAIN)
            if (superClass != null) addAnnotation(EQUALS_AND_HASH_CODE_CALL_SUPER)
        }
    }

    @JvmStatic
    fun sneakyThrows() = SNEAKY_THROWS
}