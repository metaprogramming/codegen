/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.jaxb

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.JavaGeneratorBuilder
import pl.metaprogramming.codegen.java.libs.Java

class JaxbCommonCodesConfigurator : JavaGeneratorBuilder.Delegate() {
    override fun configure() {
        typeOfCode(JaxbTypeOfCode.LOCAL_DATE_ADAPTER) {
            className.fixed = "LocalDateAdapter"
            onImplementation {
                superClass = ClassCd(
                    "javax.xml.bind.annotation.adapters.XmlAdapter",
                    Java.string(), Java.localDate()
                )
                methods.add("unmarshal") {
                    resultType = Java.localDate()
                    params.add("v", Java.string())
                    implBody = "return LocalDate.parse(v);"
                    annotations.add(Java.override())
                    throwExceptions.add(Java.exception())
                }
                methods.add("marshal") {
                    resultType = Java.string()
                    params.add("v", Java.localDate())
                    implBody = "return v.toString();"
                    annotations.add(Java.override())
                    throwExceptions.add(Java.exception())
                }
            }
        }

        typeOfCode(JaxbTypeOfCode.LOCAL_DATE_TIME_ADAPTER) {
            className.fixed = "LocalDateTimeAdapter"
            onImplementation {
                superClass = ClassCd(
                    "javax.xml.bind.annotation.adapters.XmlAdapter",
                    Java.string(), Java.localDateTime()
                )
                methods.add("unmarshal") {
                    resultType = Java.localDateTime()
                    params.add("v", Java.string())
                    implBody = "return LocalDateTime.parse(v);"
                    annotations.add(Java.override())
                    throwExceptions.add(Java.exception())
                }
                methods.add("marshal") {
                    resultType = Java.string()
                    params.add("v", Java.localDateTime())
                    implBody = "return v.toString();"
                    annotations.add(Java.override())
                    throwExceptions.add(Java.exception())
                }
            }
        }
    }
}