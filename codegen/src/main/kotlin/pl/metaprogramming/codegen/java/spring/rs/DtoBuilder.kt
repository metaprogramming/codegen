/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.Dependencies
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.base.MappingExpressionBuilder
import pl.metaprogramming.codegen.java.builders.BaseDtoBuilder
import pl.metaprogramming.codegen.java.libs.JakartaBeanValidation
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.oas.Parameter
import java.util.function.Function

class DtoBuilder<T>(modelMapper: Function<T, ObjectType>) : BaseDtoBuilder<T>(modelMapper) {

    private var withJsonAnnotations: Boolean = true

    constructor() : this({ it as ObjectType })

    fun withJsonAnnotations(withJsonAnnotations: Boolean) = apply {
        this.withJsonAnnotations = withJsonAnnotations
    }

    override fun makeImplementation() {
        objectType.fields.forEach {
            it.defaultValue?.let { defaultValue ->
                val field = fields.getByModel(it)
                field.value = MappingExpressionBuilder(context, objectType, Dependencies())
                    .from(defaultValue)
                    .to(field.type)
                    .makeValue()
            }
        }
    }

    override fun addField(schema: DataSchema) {
        fields.add(getFieldName(schema), getClass(SpringRsTypeOfCode.DTO, schema.dataType)) {
            model = schema
            description = schema.description
            annotations = schema.makeFieldAnnotations()
            if (schema is Parameter) {
                val paramDesc = "${schema.name} ${schema.location} parameter."
                description = if (description != null) "$paramDesc<br/>\n$description" else paramDesc
            }
        }
    }

    private fun DataSchema.makeFieldAnnotations(): MutableList<AnnotationCm> {
        val result = mutableListOf<AnnotationCm>()
        if (code != getFieldName(this) && withJsonAnnotations) {
            result.add(AnnotationCm("com.fasterxml.jackson.annotation.JsonProperty", ValueCm.escaped(code)))
        }
        if (params<ValidationParams>().useJakartaBeanValidation) {
            JakartaBeanValidation.addAnnotations(result, this)
        } else {
            result.add(if (required) Java.nonnul() else Java.nullable())
        }
        return result
    }
}