/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import java.util.function.Function

class PackageNameBuilder<out T> {

    var root: String = ""
    var tail: String = ""
    private var infix: ((model: @UnsafeVariance T) -> String)? = null
    private var fixed: ((model: @UnsafeVariance T) -> String?)? = null

    companion object {
        @JvmStatic
        fun of(vararg parts: String?): String = parts.filterNot { it.isNullOrEmpty() }.joinToString(".")
    }

    fun make(model: @UnsafeVariance T): String {
        val fixedPkg = fixed?.invoke(model)
        if (fixedPkg != null) return fixedPkg
        return of(root, infix?.invoke(model), tail)
    }

    fun infix(decorator: Function<@UnsafeVariance T, String>) {
        this.infix = decorator::apply
    }

    fun fixed(resolver: Function<@UnsafeVariance T, String?>) {
        this.fixed = resolver::apply
    }

    fun setup(builder: Setter<@UnsafeVariance T>) {
        builder.setup(this)
    }

    fun interface Setter<M> {
        fun PackageNameBuilder<M>.setup()
    }

    private fun <M> Setter<M>.setup(obj: PackageNameBuilder<M>) {
        obj.setup()
    }
}
