/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter

import pl.metaprogramming.codegen.CodeFormatter
import pl.metaprogramming.codegen.java.Dependencies
import pl.metaprogramming.codegen.java.PackageInfoCm

class PackageInfoFormatter : BaseJavaCodeFormatter(), CodeFormatter<PackageInfoCm> {
    override fun format(codeModel: PackageInfoCm): String {
        imports = emptyList()
        dependencies = Dependencies()
        init(codeModel.packageName)
        buf.addLines(formatAnnotations(codeModel.annotations))
        buf.newLine("package ${codeModel.packageName};")
        return buf.take()
    }
}