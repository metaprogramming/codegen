/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.GeneratorBuilder
import pl.metaprogramming.codegen.Model
import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.TypeOfCodeWithNoModel
import pl.metaprogramming.codegen.java.base.ClassBuilderConfigurator
import pl.metaprogramming.codegen.java.base.ClassCmBuilderDelegator

abstract class JavaGeneratorBuilder<M : Model, T : Any, S : JavaGeneratorBuilder<M, T, S>>(toc: T) :
    GeneratorBuilder<M, T, S>(toc) {

    val codeBuilders = mutableMapOf<TypeOfCode<*>, ClassBuilderConfigurator<*>>()
    var rootPackage: String = ""
        set(value) {
            typeOfCode().forEach { it.packageName.root = value }
            field = value
        }
    var projectDir: String = ""
        set(value) {
            typeOfCode().forEach { it.projectDir = value }
            field = value
        }
    var projectSubDir: String = "src/main/java"
        set(value) {
            typeOfCode().forEach { it.projectSubDir = value }
            field = value
        }

    @Suppress("UNCHECKED_CAST", "kotlin:S6531")
    fun <T> typeOfCode(typeOfCode: TypeOfCode<T>): ClassBuilderConfigurator<T> = codeBuilders.getOrPut(typeOfCode) {
        ClassBuilderConfigurator(typeOfCode, params).apply {
            builders.add(generatedMarkBuilder)
            packageName.root = rootPackage
            projectDir = this@JavaGeneratorBuilder.projectDir
            projectSubDir = this@JavaGeneratorBuilder.projectSubDir
        }
    } as ClassBuilderConfigurator<T>

    fun typeOfCode(vararg typeOfCodes: TypeOfCode<*>): Collection<ClassBuilderConfigurator<*>> =
        if (typeOfCodes.isEmpty()) codeBuilders.values else typeOfCodes.map { typeOfCode(it) }

    fun <M> typeOfCode(typeOfCode: TypeOfCode<M>, setter: ClassBuilderConfigurator.Setter<M>): S {
        typeOfCode(typeOfCode).setup(setter)
        return self()
    }

    // equivalent typeOfCode(TypeOfCode<Nothing>, ...) but accepted by java
    fun typeOfCode(typeOfCode: TypeOfCodeWithNoModel, setter: ClassBuilderConfigurator.Setter<Nothing>): S {
        typeOfCode(typeOfCode).setup(setter)
        return self()
    }

    fun rootPackage(rootPackage: String): S {
        this.rootPackage = rootPackage
        return self()
    }

    fun projectDir(projectDir: String, vararg typeOfCodes: TypeOfCode<*>): S {
        if (typeOfCodes.isEmpty()) {
            this.projectDir = projectDir
        } else {
            typeOfCode(*typeOfCodes).forEach { it.projectDir = projectDir }
        }
        return self()
    }

    fun projectSubDir(projectSubDir: String, vararg typeOfCodes: TypeOfCode<*>): S {
        typeOfCode(*typeOfCodes).forEach { it.projectSubDir = projectSubDir }
        return self()
    }

    private var generatedMarkBuilder = ClassCmBuilderDelegator { params[JavaParams::class].generatedMarkBuilder }

    abstract class Delegate {

        private lateinit var configurator: JavaGeneratorBuilder<*, *, *>
        val typeOfCodes = hashSetOf<TypeOfCode<*>>()
        val params get() = configurator.params

        fun configure(configurator: JavaGeneratorBuilder<*, *, *>) = apply {
            this.configurator = configurator
            configure()
        }

        protected abstract fun configure()
        protected fun <M> typeOfCode(typeOfCode: TypeOfCode<M>, setter: ClassBuilderConfigurator.Setter<M>) {
            typeOfCodes.add(typeOfCode)
            configurator.typeOfCode(typeOfCode).setup(setter)
        }
    }

}