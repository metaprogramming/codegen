/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Jackson
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Lombok
import pl.metaprogramming.codegen.java.libs.Spring

internal class RestClientBuilder : ClassCmBuilderTemplate<Nothing>() {

    override fun makeImplementation() {
        addAnnotation(Lombok.requiredArgsConstructor())

        addFields()
        addParamsMethods()
        addOnErrorMethods()
        addExchangeMethods()
    }

    private fun addFields() {
        fields.add("method", Spring.httpMethod()) { isFinal = true }
        fields.add("path", Java.string()) { isFinal = true }
        fields.add("restTemplate", Spring.restTemplate()) { isFinal = true }
        fields.add("objectMapper", Jackson.objectMapper()) { isFinal = true }

        fields.add("defaultErrorType", Java.objectType())
        addMapField("errorClasses", Java.boxedInteger(), Java.objectType())
        fields.add("body", Java.objectType())

        addMapField("pathParams", Java.string(), Java.objectType())
        addMultiValueMapField("headerParams", Java.string())
        addMultiValueMapField("formDataParams", Java.objectType())
        addMultiValueMapField("queryParams", Java.string())
    }

    private fun addOnErrorMethods() {
        val jacksonTypeReference = Jackson.typeReference(Java.genericParamT())

        addFluentApiMethod("onError") {
            params.add("defaultErrorType", jacksonTypeReference)
            implBody = "this.defaultErrorType = defaultErrorType;"
        }
        addFluentApiMethod("onError") {
            params.add("defaultErrorClass", Java.claasType(Java.genericParamT()))
            implBody = "this.defaultErrorType = defaultErrorClass;"
        }
        addFluentApiMethod("onError") {
            params.add("httpStatus", Java.boxedInteger())
            params.add("errorType", jacksonTypeReference)
            implBody = "errorClasses.put(httpStatus, errorType);"
        }
        addFluentApiMethod("onError") {
            params.add("httpStatus", Java.boxedInteger())
            params.add("errorClass", Java.claasType(Java.genericParamT()))
            implBody = "errorClasses.put(httpStatus, errorClass);"
        }
    }

    private fun addExchangeMethods() {
        val genericParam = Java.genericParamT()
        val restExceptionClass = getClass(SpringRsCommonTypeOfCode.REST_CLIENT_EXCEPTION)
        methods.add("exchange") {
            resultType = Spring.responseEntity(genericParam)
            params.add(
                "responseType",
                ClassCd.of("org.springframework.core.ParameterizedTypeReference").withGeneric(genericParam)
            )
            implBody = "return execute(() -> restTemplate.exchange(makeRequest(), responseType));"
        }
        methods.add("exchange") {
            resultType = Spring.responseEntity(genericParam)
            params.add("responseType", Java.claasType(genericParam))
            implBody = "return execute(() -> restTemplate.exchange(makeRequest(), responseType));"
        }

        //makeRequestEntity
        methods.add("makeRequest") {
            isPrivate = true
            resultType = Spring.requestEntity(Java.genericParamUnknown())
            implDependencies.add("java.net.URI", "org.springframework.web.util.UriComponentsBuilder")
            implBody = """
                |URI uri = UriComponentsBuilder.fromUriString(path)
                |        .queryParams(queryParams)
                |        .build(pathParams);
                |Object requestBody = formDataParams.isEmpty() ? this.body : formDataParams;
                |return new RequestEntity<>(requestBody, headerParams, method, uri);
            """.trimMargin()
        }

        methods.add("execute") {
            isPrivate = true
            resultType = genericParam
            params.add("executor", Java.supplier(genericParam))
            implBody = """
                |try {
                |    return executor.get();
                |} catch (RestClientResponseException e) {
                |    throw map(e);
                |}
            """.trimMargin()
        }
        methods.add("map") {
            isPrivate = true
            resultType = ClassCd.of("java.lang.RuntimeException")
            params.add("e", "org.springframework.web.client.RestClientResponseException")
            implDependencies.add("org.springframework.http.HttpHeaders", "org.springframework.http.MediaType")
            implBody = """
                |try {
                |    HttpHeaders headers = e.getResponseHeaders();
                |    if (headers == null || !MediaType.APPLICATION_JSON.equals(headers.getContentType())) return e;
                |    Object dto = readValue(e.getResponseBodyAsString(), getErrorBodyType(e.getRawStatusCode()));
                |    return new ${restExceptionClass.className}(e.getRawStatusCode(), headers, dto);
                |} catch (Exception ex) {
                |    return e;
                |}
            """.trimMargin()
        }
        methods.add("getErrorBodyType") {
            isPrivate = true
            resultType = Java.objectType()
            params.add("httpStatus", Java.primitiveInt())
            implBody = "return errorClasses.containsKey(httpStatus) ? errorClasses.get(httpStatus) : defaultErrorType;"
        }
        methods.add("readValue") {
            isPrivate = true
            resultType = Java.objectType()
            throwExceptions.add(ClassCd("com.fasterxml.jackson.core.JacksonException"))
            params.add("body", Java.string())
            params.add("type", Java.objectType())
            implBody = """
                |if (type instanceof Class) {
                |    return objectMapper.readValue(body, (Class<?>) type);
                |}
                |if (type instanceof TypeReference) {
                |    return objectMapper.readValue(body, (TypeReference<?>) type);
                |}
                |throw new IllegalArgumentException("Unsupported type reference: " + type);
            """.trimMargin()
        }
    }

    private fun addParamsMethods() {
        addFluentApiMethod("body") {
            addRequiredParam("body", Java.objectType())
            implBody = "this.body = body;"
        }
        addParamMethod("header", Java.string())
        addOptionalParamMethod("header", Java.string())
        addParamMethod("query", Java.string())
        addOptionalParamMethod("query", Java.string())
        addListParamMethod("query")
        addParamMethod("path", Java.objectType())
        addParamMethod("formData", Java.objectType())
        methods.add("required") {
            isPrivate = true
            resultType = Java.genericParamT()
            params.add("value", resultType)
            params.add("name", Java.string())
            implDependencies.add(Java.objects())
            implBody = "return Objects.requireNonNull(value, name + \" is required\");"
        }
    }

    private fun addOptionalParamMethod(paramType: String, valueType: ClassCd) {
        addFluentApiMethod("${paramType}ParamOptional") {
            addRequiredParam("name", Java.string())
            params.add("value", valueType)
            implBody = "if (value != null) ${paramType}Param(name, value);"
        }
    }

    private fun addParamMethod(paramType: String, valueType: ClassCd) {
        val addMethod = if (paramType == "path") "put" else "add"
        addFluentApiMethod("${paramType}Param") {
            addRequiredParam("name", Java.string())
            addRequiredParam("value", valueType)
            implBody = "${paramType}Params.${addMethod}(name, required(value, name));"
        }
    }

    private fun addListParamMethod(paramType: String) {
        addFluentApiMethod("${paramType}Param") {
            addRequiredParam("name", Java.string())
            params.add("value", Java.string().asList())
            implDependencies.add("java.util.Collections")
            implBody = "${paramType}Params.addAll(name, value != null ? value : Collections.emptyList());"
        }
    }

    private fun addMapField(name: String, keyType: ClassCd, valueType: ClassCd) {
        fields.add(name, Java.map(keyType, valueType)) {
            isFinal = true
            value = ValueCm.newExp("java.util.HashMap", true)
        }
    }

    private fun addMultiValueMapField(name: String, valueType: ClassCd) {
        fields.add(name, Spring.multiValueMap(Java.string(), valueType)) {
            isFinal = true
            value = Spring.linkedMultiValueMap().newExp()
        }
    }

    private fun addFluentApiMethod(methodName: String, methodBuilder: MethodCm.() -> Unit) {
        methods.add(methodName) {
            methodBuilder.invoke(this)
            resultType = classCm
            implBody = """
                |$implBody
                |return this;
            """.trimMargin()
        }
    }
}