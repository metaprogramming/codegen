/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.formatter.ClassNameFormatter
import pl.metaprogramming.codegen.java.formatter.CodeBuffer

abstract class BaseMethodCmBuilder<T>(model: T) : MethodCmBuilder<T>(model) {
    lateinit var classNameFormatter: ClassNameFormatter
    val codeBuf = CodeBuffer()
    val nameMapper: JavaNameMapper get() = context.nameMapper
    val dependencies: Dependencies get() = methodCm.implDependencies
    val params: CodegenParams get() = context.params

    abstract fun makeImplBody(): String

    override fun makeImplementation() {
        val collectedDependencies = Dependencies()
        methodCm.collectDependencies(collectedDependencies)
        collectedDependencies.add(methodCm.implDependencies)
        classNameFormatter = ClassNameFormatter(classCm.packageName, collectedDependencies)
        methodCm.implBody = makeImplBody()
    }

    @JvmOverloads
    fun getClass(typeOfCode: Any, model: Any? = this.model): ClassCd =
        if (typeOfCode is ClassCd) typeOfCode else context.getClass(typeOfCode, model)

    fun classLocator(typeOfCode: Any) = context.classLocator(typeOfCode).model(model)

    @JvmOverloads
    fun mapping(model: Any? = this.model) = MappingExpressionBuilder(context, model, dependencies)

    @JvmOverloads
    fun injectDependency(classCd: ClassCd, fieldName: String? = null): FieldCm = classCm.injectDependency(classCd, fieldName)

    @JvmOverloads
    fun componentRef(typeOfCode: Any, fieldName: String? = null): String {
        val dependency = injectDependency(getClass(typeOfCode), fieldName)
        return dependency.name
    }

    fun callComponent(typeOfCode: Any, callExp: String) = injectDependency(getClass(typeOfCode)).name + '.' + callExp

    fun getter(vararg path: String): String = path.map(nameMapper::toFieldName).toGetter()

    fun addVarDeclaration(field: FieldCm) = addVarDeclaration(field.name, field.type, field.value.toString())
    fun addVarDeclaration(varName: String, varType: ClassCd, varExp: String): String {
        dependencies.add(varType)
        val fixedVarExp = if (varExp == "new") varType.newExp().toString() else varExp
        codeBuf.addLines("${varType.className} $varName = $fixedVarExp;")
        return varName
    }

    fun ofNullable(expression: String): String {
        dependencies.add("java.util.Optional")
        return "Optional.ofNullable($expression)"
    }
}