/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Lombok
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tCommonTypeOfCode.MAP_RAW_VALUE_SERIALIZER
import pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tCommonTypeOfCode.REST_RESPONSE_ABSTRACT
import pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tCommonTypeOfCode.REST_RESPONSE_INTERFACE
import pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tCommonTypeOfCode.REST_RESPONSE_STATUS_INTERFACE
import pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tCommonTypeOfCode.REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE
import pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tCommonTypeOfCode.VALIDATION_RESULT_MAPPER
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_RESULT
import pl.metaprogramming.model.oas.HttpResponse

class SpringRs2tCommonCodesConfigurator : JavaGeneratorBuilder.Delegate() {
    override fun configure() {

        typeOfCode(VALIDATION_RESULT_MAPPER) {
            className.fixed = "ValidationResultMapper"
            packageName.tail = "validator"
            addManagedComponentBuilder()
            onDeclaration {
                methods.add("map") {
                    registerAsMapper()
                    resultType = Spring.responseEntity()
                    params.add("validationResult", getClass(VALIDATION_RESULT)) {
                        addAnnotation(Java.nonnul())
                    }
                    implBody = """
                        |return ResponseEntity
                        |        .status(validationResult.getStatus())
                        |        .body(validationResult.getMessage());   // FIXME and remove '@Generated' annotation
                        """.trimMargin()
                }
            }
        }

        typeOfCode(MAP_RAW_VALUE_SERIALIZER) {
            className.fixed = "MapRawValueSerializer"
            onImplementation {
                superClass = ClassCd(
                    "com.fasterxml.jackson.databind.JsonSerializer",
                    Java.string().asMapBy(Java.string())
                )
                methods.add("serialize") {
                    params.add("value", Java.string().asMapBy(Java.string()))
                    params.add("gen", "com.fasterxml.jackson.core.JsonGenerator")
                    params.add("serializers", "com.fasterxml.jackson.databind.SerializerProvider")
                    annotations.add(Java.override())
                    throwExceptions.add("java.io.IOException".asClassCd())
                    implBody = """
                                |gen.writeStartObject();
                                |for (Map.Entry<String, String> e: value.entrySet()) {
                                |    gen.writeFieldName(e.getKey());
                                |    gen.writeRawValue(e.getValue());
                                |}
                                |gen.writeEndObject();""".trimMargin()
                }
            }
        }

        typeOfCode(REST_RESPONSE_INTERFACE) {
            className.fixed = "RestResponse"
            onImplementation { makeRestResponseInterface() }
        }

        typeOfCode(REST_RESPONSE_ABSTRACT) {
            className.fixed = "RestResponseBase"
            onImplementation { makeRestResponseBase() }
        }

        typeOfCode(REST_RESPONSE_STATUS_INTERFACE) {
            className {
                resolver(::toModelName)
                prefix = "RestResponse"
            }
        }

        typeOfCode(REST_RESPONSE_STATUS_INTERFACE) {
            className {
                resolver(::toModelName)
                prefix = "RestResponse"
            }
            builders += RestResponseStatusInterfaceBuilder(true)
        }

        typeOfCode(REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE) {
            className {
                resolver(::toModelName)
                prefix = "RestResponse"
                suffix = "NoContent"
            }
            builders += RestResponseStatusInterfaceBuilder(false)
        }
    }
}

private fun toModelName(httpStatusCode: Int) = if (httpStatusCode == 0) "Other" else "$httpStatusCode"

private val ClassCmBuilderTemplate<*>.staticFactoryMethodForRestResponse: Boolean get() = params[SpringRestParams::class].staticFactoryMethodForRestResponse

private fun ClassCmBuilderTemplate<*>.makeRestResponseInterface() {
    kind = ClassKind.INTERFACE
    addGenericParams(Java.genericParamR())
    addAnnotation(Java.parametersAreNonnullByDefault())
    methods.add("getBody") { resultType = Java.objectType() }
    methods.add("getStatus") { resultType = Java.boxedInteger() }
    methods.add("self") { resultType = Java.genericParamR() }
    methods.add("getDeclaredStatuses") { resultType = Java.boxedInteger().asCollection() }
    methods.add("getHeaders") { resultType = Java.string().asMapBy(Java.string()) }
    methods.add("setHeader") {
        resultType = Java.genericParamR()
        params.add("name", Java.string())
        params.add("value", Java.string())
        implBody = """
            getHeaders().put(name, value);
            return self();
            """.trimIndent()
    }
    methods.add("isStatus") {
        resultType = Java.primitiveBoolean()
        params.add("status", Java.boxedInteger())
        implBody = "return Objects.equals(status, getStatus());"
        implDependencies.add("java.util.Objects")
    }

    if (!staticFactoryMethodForRestResponse) {
        methods.add("set") {
            resultType = Java.genericParamR()
            params.add("status", Java.boxedInteger())
            params.add("body", Java.objectType())
        }
    }
}

private fun ClassCmBuilderTemplate<*>.makeRestResponseBase() {
    addGenericParams(Java.genericParamR())
    classCm.isAbstract = true
    implementationOf(getClass(REST_RESPONSE_INTERFACE).withGeneric(Java.genericParamR()))
    fields.add("status", Java.boxedInteger()) {
        addAnnotation(Lombok.getter())
        isFinal = staticFactoryMethodForRestResponse
    }
    fields.add("body", Java.objectType()) {
        addAnnotation(Lombok.getter())
        isFinal = staticFactoryMethodForRestResponse
    }
    fields.add("headers", Java.string().asMapTo(Java.string())) {
        addAnnotation(Lombok.getter())
        isFinal = true
        value = ValueCm.newExp("java.util.TreeMap", true)
    }

    if (!staticFactoryMethodForRestResponse) {
        methods.add("set") {
            resultType = Java.genericParamR()
            params.add(Java.boxedInteger().asField("status").addAnnotation(Java.nonnul()))
            params.add("body", Java.objectType())
            annotations.add(Java.override())
            implBody = """
                |if (this.status != null) {
                |    throw new IllegalStateException(String.format("Response already initialized with %s - %s", this.status, this.body));
                |}
                |this.status = status;
                |this.body = body;
                |return self();
                """.trimMargin()
        }
    } else {
        addAnnotation(Lombok.requiredArgsConstructor())
    }
}

class RestResponseStatusInterfaceBuilder(private val withContent: Boolean) : ClassCmBuilderTemplate<Int>() {
    private val isDefaultResponse get() = model == HttpResponse.DEFAULT
    private val modelName get() = if (isDefaultResponse) "Other" else model.toString()

    override fun makeImplementation() {
        kind = ClassKind.INTERFACE
        addGenericParams(Java.genericParamR())
        superClass = getClass(REST_RESPONSE_INTERFACE).withGeneric(Java.genericParamR())

        methods.add("is$modelName") {
            resultType = Java.primitiveBoolean()
            implBody =
                if (isDefaultResponse) "return !getDeclaredStatuses().contains(getStatus());" else "return isStatus($model);"
        }

        if (withContent) addContentGetter()
        if (!staticFactoryMethodForRestResponse) addContentSetter()
    }

    private fun addContentGetter() {
        addGenericParams(Java.genericParamT())
        methods.add("get$modelName") {
            resultType = Java.genericParamT()
            annotations.add(Java.suppressWarnings())
            val illegalResponseMessage =
                if (isDefaultResponse) "String.format(\"Response other than %s is not set\", getDeclaredStatuses())"
                else "\"Response $model is not set\""
            implBody = """
                |if (is$modelName()) {
                |    return (T) getBody();
                |}
                |throw new IllegalStateException($illegalResponseMessage);
                """.trimMargin()
        }
    }

    private fun addContentSetter() {
        addAnnotation(Java.parametersAreNonnullByDefault())
        methods.add("set$modelName") {
            resultType = Java.genericParamR()
            if (isDefaultResponse) params.add("status", Java.boxedInteger())
            if (withContent) params.add("body", Java.genericParamT())
            implBody =
                "return set(${if (isDefaultResponse) "status" else modelName}, ${if (withContent) "body" else "null"});"
            if (isDefaultResponse) {
                implBody = """
                    |if (getDeclaredStatuses().contains(status)) {
                    |    throw new IllegalArgumentException(String.format("Status %s is declared. Use dedicated setter for it.", status));
                    |}
                    |$implBody
                    """.trimMargin()
            }
        }
    }
}
