/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.base.BuildContext
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.HttpResponse
import pl.metaprogramming.model.oas.Operation

object ResponseClassResolver {
    @JvmStatic
    fun resolve(builder: BuildContext<*>, operation: Operation): ClassCd {
        val successResponses = operation.successResponses
        val hasManySuccessSchemas = getManySuccessSchemas(successResponses)
        return if (operation.successResponseHeaders?.isNotEmpty() == true || hasManySuccessSchemas || successResponses.size > 1)
            Spring.responseEntity(successClass(builder, operation, hasManySuccessSchemas))
        else successClass(builder, operation, false)
    }

    @JvmStatic
    fun successClass(builder: BuildContext<*>, operation: Operation, hasManySuccessSchemas: Boolean? = null): ClassCd =
        if (hasManySuccessSchemas == true || hasManySuccessSchemas == null && getManySuccessSchemas(operation.successResponses))
            Java.objectType()
        else if (operation.successResponseSchema != null)
            builder.getClass(SpringRsTypeOfCode.DTO, operation.successResponseSchema!!.dataType)
        else Java.voidType()

    private fun getManySuccessSchemas(successResponses: List<HttpResponse>): Boolean {
        val schemas = mutableSetOf<DataSchema>()
        successResponses.forEach {
            if (it.contents.isNotEmpty()) schemas.addAll(it.contents.values)
        }
        return schemas.size > 1
    }

}