/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.model.oas.Operation

class FacadeBuilder : ClassCmBuilderTemplate<List<Operation>>() {
    override fun makeDeclaration() {
        addAnnotation(Spring.service())
        model.forEach { operation ->
            val responseClass = ResponseClassResolver.resolve(context, operation)
            operation.requestSchemas.forEach { requestSchema ->
                methods.add(nameMapper.toMethodName(operation.code)) {
                    registerAsMapper()
                    resultType = responseClass
                    params.add("request", getClass(SpringRsTypeOfCode.REQUEST_DTO, requestSchema)) {
                        addAnnotation(Java.nonnul())
                    }
                    implBody = if (responseClass.isVoid) "" else "return null;"
                    description = operation.description
                }
            }
        }
    }
}