/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.formatter.CodeBuffer
import pl.metaprogramming.model.data.DataSchema

open class ClassCmBuilderTemplate<M> : IClassCmBuilder<M> {

    fun interface Builder<T> {
        fun ClassCmBuilderTemplate<T>.make()
    }

    fun Builder<M>.make(obj: ClassCmBuilderTemplate<M>) {
        obj.make()
    }

    lateinit var context: BuildContext<M>
    override var priority: Int = 10

    override fun makeDeclaration(context: BuildContext<M>) {
        this.context = context
        makeDeclaration()
    }

    override fun makeImplementation(context: BuildContext<M>) {
        this.context = context
        makeImplementation()
    }

    override fun makeDecoration(context: BuildContext<M>) {
        this.context = context
        makeDecoration()
    }

    open fun makeDeclaration() = Unit

    open fun makeImplementation() = Unit

    open fun makeDecoration() = Unit

    var codeBuf: CodeBuffer = CodeBuffer()
    val classCm: ClassCm get() = context.classCm
    var kind: ClassKind
        get() = classCm.kind
        set(value) {
            classCm.kind = value
        }

    open val model: M get() = context.model
    val typeOfCode: TypeOfCode<M> get() = context.typeOfCode
    val params: CodegenParams get() = context.params
    inline fun <reified P : Any> params() = params[P::class]
    val nameMapper: JavaNameMapper get() = context.nameMapper
    var superClass
        get() = classCm.superClass
        set(value) {
            check(classCm.superClass == null) { "Can't set super class '$superClass', super class already set to '$classCm.superClass'" }
            classCm.superClass = value
        }
    val methods: ClassCmMethods get() = classCm.methods
    val fields: ClassCmFields get() = classCm.fields

    fun priority(priority: Int) = apply { this.priority = priority }
    fun classLocator(typeOfCode: Any) = context.classLocator(typeOfCode)

    @JvmOverloads
    fun getClass(typeOfCode: Any, model: Any? = this.context.model) = context.getClass(typeOfCode, model)
    fun getFieldName(schema: DataSchema) = context.getFieldName(schema)

    fun setComment(comment: String?) {
        classCm.description = comment
    }

    fun implementationOf(vararg interfaces: ClassCd) = interfaces.forEach { classCm.implementationOf(it) }
    fun implementationOf(vararg interfaces: String) = interfaces.forEach { classCm.implementationOf(ClassCd.of(it)) }

    fun addGenericParams(vararg genericParams: ClassCd) = classCm.addGenericParams(*genericParams)

    fun addAnnotation(vararg annotations: AnnotationCm) = annotations.forEach { classCm.annotations.add(it) }

    fun addInnerClass(name: String, builder: Builder<M>) {
        val template = ClassCmBuilderTemplate<M>()
        template.context = context.makeInnerClassContext(name)
        builder.make(template)
        template.context.classCm.methods.forEach { it.markAsUsed() }
    }

}