/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.FieldCm

open class FieldCmList(private val _fields: MutableList<FieldCm> = mutableListOf()) :
    List<FieldCm> by _fields {

    fun getByModel(model: Any) = find { it.model == model } ?: error("No field found for: $model, in class: $this")

    open fun add(fieldCm: FieldCm): Boolean {
        if (contains(fieldCm)) return false
        find { it.name == fieldCm.name }?.let {
            error("field ${fieldCm.name} already exists")
        }
        _fields.add(fieldCm)
        return true
    }

    fun addAll(fields: Collection<FieldCm>): Boolean {
        var result = false
        fields.forEach {
            if (add(it)) {
                result = true
            }
        }
        return result
    }

    @JvmOverloads
    fun add(name: String, type: ClassCd, builder: FieldCm.Setter = FieldCm.Setter {}): FieldCm {
        val field = FieldCm(name, type, builder)
        add(field)
        return field
    }

    @JvmOverloads
    fun add(name: String, type: String, builder: FieldCm.Setter = FieldCm.Setter {}) = add(name, ClassCd.of(type), builder)
}