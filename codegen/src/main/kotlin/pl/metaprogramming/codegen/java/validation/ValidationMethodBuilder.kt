/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.CONDITION_ENUM
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.CONDITION_RESOLVER
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.OPERATION_ID_ENUM
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.SECURITY_PRIVILEGE_ENUM
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_CHECKER
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_COMMON_CHECKERS
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_CONTEXT
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_DICTIONARY_CODE_ENUM
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_ERROR_CODE_ENUM
import pl.metaprogramming.codegen.java.validation.ValidationTypeOfCode.SECURITY_CHECKER
import pl.metaprogramming.log
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataTypeCode.*
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.data.constraint.*
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.value

class ValidationMethodBuilder(
    objectType: ObjectType,
    private val dtoClass: ClassCm,
    private val validatorTypeOfCode: TypeOfCode<*>,
    private val enumTypeOfCode: TypeOfCode<*>,
    private val addDataTypeValidations: Boolean = false,
    private val operationModel: Operation? = null,
) : BaseMethodCmBuilder<ObjectType>(objectType) {

    private var isCommonCheckersUsed: Boolean = false
    private var conditionResolverNeeded: Boolean = false
    private val validationParams get() = context.params[ValidationParams::class]
    override fun makeDeclaration(): MethodCm {
        val genericParams = classCm.superClass?.genericParams ?: classCm.interfaces[0].genericParams
        return newMethodCm("check") {
            params.add("ctx", classLocator(VALIDATION_CONTEXT).getGeneric(genericParams))
        }
    }

    override fun makeImplBody(): String {
        val constraints = ConstraintCheckers(dtoClass, ConstraintsSupport(model.additives).constraints)
        addSetOperationId()
        addSecurityValidations()
        constraints.highPriorityCheckers.forEach {
            codeBuf.addLines("ctx.check(${it});")
        }
        model.additives.value<List<Map<String, String>>>("x-validation-beans")?.forEach {
            setValidationBean(it)
        }
        if (model.inherits.isNotEmpty()) {
            codeBuf.addLines("${getObjectChecker(model.inherits[0])}.checkWithParent(ctx);")
        }
        model.fields.forEach {
            addFieldValidations(it)
        }
        constraints.lowPriorityCheckers.forEach {
            codeBuf.addLines("ctx.check(${it});")
        }

        if (conditionResolverNeeded) {
            codeBuf.addFirstLine("ctx.setBean(${injectDependency(getClass(CONDITION_RESOLVER)).name});")
        }

        if (isCommonCheckersUsed) {
            dependencies.add(getClass(VALIDATION_COMMON_CHECKERS), "*")
        }

        return codeBuf.take()
    }

    private fun addSetOperationId() {
        if (operationModel != null) {
            val operationEnum = OPERATION_ID_ENUM.enumItem(operationModel.code, false)
            codeBuf.addLines("ctx.setBean($operationEnum);")
        }
    }

    private fun setValidationBean(spec: Map<String, String>) {
        val beanClass = ClassCd.of(spec.getValue("class"))
        val factoryDesc = spec.getValue("factory").split(':')
        check(factoryDesc.size == 2)
        dependencies.add(beanClass)
        val factoryField = injectDependency(ClassCd.of(factoryDesc[0]))
        codeBuf.newLine("ctx.setBean(${beanClass.className}.class, ${factoryField.name}::${factoryDesc[1]});")
    }

    private fun getObjectChecker(objectType: ObjectType): String =
        injectDependency(getClass(validatorTypeOfCode, objectType)).name


    private fun addFieldValidations(schema: DataSchema) {
        val fieldCm = dtoClass.fields.getByModel(schema)
        val checkers = CheckersCollector(fieldCm, schema, fieldCm.type).collect()
        if (checkers.isNotEmpty()) {
            val method = if (operationModel?.isBodySchema(schema) == true) "checkRoot" else "check"
            codeBuf.newLine("ctx.${method}(${schema.descriptionField}, ${checkers.joinToString(", ")});")
        }
    }

    private fun addSecurityValidations() {
        if (!validationParams.securityValidation) return
        val securityParameters = operationModel?.securityParameters
        if (securityParameters.isNullOrEmpty()) return
        isCommonCheckersUsed = true
        codeBuf.newLine("ctx.check(authParamNonNull(${securityParameters.joinToString { it.descriptionField }}));")
        securityParameters.forEach { param ->
            val securityChecker = injectDependency(getClass(SECURITY_CHECKER, param.name))
            val scopes = operationModel!!.getSecurity(param).scopes.joinToString {
                SECURITY_PRIVILEGE_ENUM.enumItem(it).name
            }
            operationModel.getSecurity(param).scopes.joinToString { "\"$it\"" }
            codeBuf.newLine("ctx.check(${param.descriptionField}, ${securityChecker.name}.check(${scopes}));")
        }
    }

    private inner class CheckersCollector(
        private val fieldCm: FieldCm,
        private val schema: DataSchema,
        private val type: ClassCd,
        private val level: Int = 1,
        private val checkers: MutableList<String> = mutableListOf()
    ) {
        private fun nestedCheckers(schema: DataSchema, type: ClassCd): List<String> =
            CheckersCollector(fieldCm, schema, type, level + 1).collect()

        fun collect(): List<String> {
            val xValidations = ConstraintCheckers(type, schema.constraints.constraints)
            checkers.addAll(xValidations.highPriorityCheckers)
            if (schema.required) {
                isCommonCheckersUsed = true
                checkers.add("required()")
            }
            if (schema.isArray) {
                addArrayChecker()
                return checkers
            }
            if (schema.isMap) {
                addMapChecker()
                return checkers
            }
            if (addDataTypeValidations && schema.isType(BOOLEAN, INT32, INT64, FLOAT, DOUBLE, BASE64)) {
                isCommonCheckersUsed = true
                checkers.add(schema.dataType.typeCode.name)
            }
            addObjectChecker()
            addEnumChecker()
            addFormatChecker()
            addPatternChecker()
            addLengthChecker(schema.minLength, "min")
            addLengthChecker(schema.maxLength, "max")
            checkers.addAll(xValidations.lowPriorityCheckers)
            // validation that requires data conversion should be performed at the end
            addMinMaxChecker(schema.minimum, if (schema.exclusiveMinimum == true) "gt" else "ge")
            addMinMaxChecker(schema.maximum, if (schema.exclusiveMaximum == true) "lt" else "le")
            return checkers
        }

        private fun addArrayChecker() {
            val arrayType = schema.arrayType
            if (arrayType.uniqueItems) checkers.add("unique()")
            if (arrayType.minItems != null) checkers.add("minItems(${arrayType.minItems})")
            if (arrayType.maxItems != null) checkers.add("maxItems(${arrayType.maxItems})")
            val itemCheckers = nestedCheckers(arrayType.itemsSchema, type.genericParams[0])
            if (itemCheckers.isNotEmpty()) {
                isCommonCheckersUsed = true
                checkers.add("items(${itemCheckers.joinToString(", ")})")
            }
        }

        private fun addMapChecker() {
            val itemCheckers = nestedCheckers(schema.mapType.valuesSchema, type.genericParams[1])
            if (itemCheckers.isNotEmpty()) {
                isCommonCheckersUsed = true
                checkers.add("mapValues(${itemCheckers.joinToString(", ")})")
            }
        }

        private fun addObjectChecker() {
            if (!schema.isObject || schema is Parameter || schema.objectType.fields.isEmpty()) return
            checkers.add(getObjectChecker(schema.objectType))
        }

        private fun addEnumChecker() {
            if (!schema.isEnum || !addDataTypeValidations) return
            val enumClass = getClass(enumTypeOfCode, schema.dataType)
            dependencies.add(enumClass)
            checkers.add("allow(${enumClass.className}.values())")
            isCommonCheckersUsed = true
        }

        private fun addPatternChecker() {
            if (schema.pattern.isNullOrEmpty()) return
            val checkerName = "${getDescription()}_PATTERN"
            dependencies.add("java.util.regex.Pattern")
            val fixedPattern = schema.pattern!!.replace("\\", "\\\\").replace("\"", "\\\"")
            val errorCodeParam = schema.invalidPatternCode?.let { ", \"$it\"" } ?: ""
            addCheckerField(checkerName, "matches(Pattern.compile(\"$fixedPattern\")$errorCodeParam)")
            isCommonCheckersUsed = true
            checkers.add(checkerName)
        }

        private fun addMinMaxChecker(value: String?, operator: String) {
            if (value.isNullOrEmpty()) return
            if (addDataTypeValidations) {
                val type = getClass(schema.dataType)
                val constructor =
                    if (schema.dataType.isNumber() && type.packageName == "java.lang") "valueOf" else "new"
                dependencies.add(type)
                checkers.add("${operator}(${ValueCm.escaped(value)}, ${type.className}::$constructor)")
            } else {
                checkers.add("${operator}(${mapping().from(value).to(type)})")
            }
            isCommonCheckersUsed = true
        }

        private fun addLengthChecker(length: Int?, kind: String) {
            if (length == null) return
            isCommonCheckersUsed = true
            checkers.add("${kind}Length(${length})")
        }

        private fun addFormatChecker() {
            val validator = schema.format?.let { validationParams.formatValidators.getValidator(it) }
            if (validator != null) checkers.add(validator.toJavaExpr())
        }

        private fun getDescription() = (fieldCm.model as DataSchema).descriptionField

        private fun addCheckerField(checkerName: String, checkerValue: String, testedType: ClassCd = Java.string()) {
            classCm.fields.add(checkerName, classLocator(VALIDATION_CHECKER).getGeneric(testedType)) {
                isFinal = true
                isStatic = true
                setValue(checkerValue)
            }
        }
    }

    private inner class ConstraintCheckers(
        private val dataType: ClassCd,
        private val directives: List<DataConstraint>
    ) {
        val highPriorityCheckers get() = getCheckers { it.priority!! <= 0 }
        val lowPriorityCheckers get() = getCheckers { it.priority!! > 0 }

        private fun getCheckers(predicate: (DataConstraint) -> Boolean): List<String> =
            directives.filter(predicate).sortedBy { it.priority }
                .map { addCheckIf(addCustomInvalidCode(it.toJavaExpr(), it), it) }

        private fun addCheckIf(checkExpr: String, constraint: DataConstraint): String =
            constraint.ifExpression?.let { ifExpression ->
                try {
                    val ifExp = ConditionParser(ifExpression).parse().toJavaExpr()
                    "checkIf($checkExpr, $ifExp)"
                } catch (e: Exception) {
                    log.error("Ignore if expression '$ifExpression", e)
                    checkExpr
                }
            } ?: checkExpr

        private fun addCustomInvalidCode(checkExpr: String, constraint: DataConstraint): String =
            constraint.errorCode?.let { errorCode ->
                val enumItem = VALIDATION_ERROR_CODE_ENUM.enumItem(errorCode)
                (getClass(VALIDATION_CHECKER) as ClassCm).methods["withError(errorCode)"].markAsUsed()
                "${checkExpr}.withError(${enumItem.name})"
            } ?: checkExpr

        private fun Condition.toJavaExpr(): String = when (this) {
            is ConditionItem -> toJavaExpr()
            is ConditionExpression -> "${operation.name.lowercase()}(${items.joinToString { it.toJavaExpr() }})"
            else -> error("Can't handle condition ${this.javaClass}")
        }

        private fun ConditionItem.toJavaExpr(): String = if (code.startsWith("operationId[")) {
            val opsList = code.substring(code.indexOf('[') + 1, code.indexOf(']'))
            val ops = opsList.split("|").map {
                OPERATION_ID_ENUM.enumItem(it.trim(), false).toString()
            }
            if (ops.size == 1) ops[0] else "or(${ops.joinToString(", ")})"
        } else {
            conditionResolverNeeded = true
            CONDITION_ENUM.enumItem(code, false).toString()
        }

        private fun DataConstraint.toJavaExpr(): String = when (this) {
            is ValidationDirective -> pointer.toJavaExpr()
            is DictionaryConstraint -> makeDictionaryCheckExpr(this)
            is RuleConstraint -> makeRuleCheckExpr(this)
            is ConditionConstraint -> makeCompareCheckExpr(expression)
            is CheckerRef -> ValidatorPointer.of(this).toJavaExpr()
            is RequiredConstraint -> "required()"
            else -> error("Unsupported directive: ${this.javaClass}")
        }

        private fun makeRuleCheckExpr(rule: RuleConstraint): String {
            val validator = validationParams.ruleCheckers.getValidator(rule.ruleCode)
            if (validator != null) return validator.toJavaExpr()
            check(validationParams.useNamedBeanAsRuleValidator) {
                """
                    |Unknown constraint rule: ${rule.ruleCode}
                    |See https://metaprogramming.gitlab.io/codegen/docs/guides/validations to get know how to handle rule constraints.
                """.trimMargin()
            }
            val checkerName: String = nameMapper.toFieldName(rule.ruleCode)
            if (!classCm.fields.any { it.name == checkerName }) {
                classCm.fields.add(checkerName, getClass(VALIDATION_CHECKER).withGeneric(dataType)) {
                    addAnnotation(Spring.qualifier(rule.ruleCode))
                }
            }
            return checkerName
        }

        private fun makeCompareCheckExpr(compare: String): String {
            val exp = CompareExpression.parse(compare, model)
            return if (addDataTypeValidations) "${exp.operator}(${exp.field1.descriptionField}, ${exp.field2.descriptionField}, ${exp.field1.getDeserializeMapper()})"
            else "${exp.operator}(${exp.field1.descriptionField}, ${exp.field2.descriptionField})"
        }

        private fun makeDictionaryCheckExpr(constraint: DictionaryConstraint): String =
            mapping().to(getClass(VALIDATION_CHECKER).withGeneric(Java.string())).from(constraint.getEnumRef()).make()

        private fun DictionaryConstraint.getEnumRef(): FieldCm {
            val enumItem = VALIDATION_DICTIONARY_CODE_ENUM.enumItem(dictionaryCode)
            return enumItem.ownerClass.asExpression(enumItem.name)
        }

        private fun DataSchema.getDeserializeMapper() = mapping().to(dataType).from(Java.string()).make()
    }

    private fun ValidatorPointer.toJavaExpr(): String {
        if ("VALIDATION_COMMON_CHECKERS" == classCanonicalName) {
            return staticField!!
        }

        val validatorClass = ClassCd.of(classCanonicalName)
        dependencies.add(classCanonicalName)

        if (staticField != null) return "${validatorClass.className}.${staticField}"

        val validatorObject =
            if (isDiBean) injectDependency(validatorClass).name
            else "ctx.getBean(${validatorClass.className}.class)"
        return if (method != null) "${validatorObject}::${method}" else validatorObject
    }

    private fun TypeOfCode<*>.enumItem(itemValue: String, staticImport: Boolean = true): EnumItemCm {
        val enumClass = classLocator(this).getDeclared()
        if (staticImport) {
            dependencies.add(enumClass, "*")
        } else {
            dependencies.add(enumClass)
        }
        val enumItem = enumClass.addEnumItem(nameMapper.toConstantName(itemValue), true)
        if (this != VALIDATION_DICTIONARY_CODE_ENUM) {
            enumItem.value = ValueCm.escaped(itemValue)
        }
        return enumItem
    }

    private val DataSchema.descriptionField: String get() = getAdditive(DESCRIPTION_FIELD_NAME)
}