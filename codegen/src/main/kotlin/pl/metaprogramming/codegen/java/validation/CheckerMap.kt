/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.model.data.constraint.CheckerRef
import pl.metaprogramming.model.data.constraint.ValidatorPointer

class CheckerMap {

    private val map = mutableMapOf<String, ValidatorPointer>()

    fun setIfNotPresent(code: String, validator: String) = apply { if (!map.containsKey(code)) set(code, validator) }

    operator fun set(code: String, validator: String) = apply { map[code] = ValidatorPointer.fromExpression(validator) }
    fun set(code: String, checkerRef: CheckerRef) = apply { map[code] = ValidatorPointer.of(checkerRef) }

    fun getValidator(code: String): ValidatorPointer? = map[code]

    fun copy(): CheckerMap {
        val result = CheckerMap()
        map.forEach(result.map::put)
        return result
    }
}