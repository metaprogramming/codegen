/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.java.MethodCm.Setter
import pl.metaprogramming.codegen.java.base.FieldCmList
import pl.metaprogramming.codegen.java.internal.AccessModifier
import pl.metaprogramming.codegen.java.libs.Java
import java.util.*

class MethodCm(val name: String, val ownerClass: ClassCd) {
    var ownerInterface: ClassCd? = null
    var annotations: MutableList<AnnotationCm> = mutableListOf()
    var implBody: String? = null
    var description: String? = null
    var used: Boolean = false
    var mapper: Boolean = false
    val throwExceptions: MutableList<ClassCd> = mutableListOf()
    private var accessModifier: AccessModifier = AccessModifier.PUBLIC
    private val modifiers: MutableSet<MethodModifier> = EnumSet.noneOf(MethodModifier::class.java)
    val implDependencies = Dependencies()

    constructor(name: String, ownerClass: ClassCd, setter: Setter = Setter {}) : this(name, ownerClass) {
        setter.setup(this)
    }

    companion object {
        @JvmStatic
        @JvmOverloads
        fun of(name: String, ownerClass: ClassCd, builder: Setter = Setter {}) =
            MethodCm(name, ownerClass, builder)
    }

    var resultType: ClassCd = Java.voidType()
    val params = FieldCmList()

    val owner: ClassCd get() = ownerInterface ?: ownerClass
    val isConstructor: Boolean get() = name == ownerClass.className
    var isPublic: Boolean
        get() = accessModifier == AccessModifier.PUBLIC
        set(value) = setAccessModifier(if (!value) AccessModifier.PRIVATE else AccessModifier.PUBLIC)
    var isPrivate: Boolean
        get() = accessModifier == AccessModifier.PRIVATE
        set(value) = setAccessModifier(if (value) AccessModifier.PRIVATE else AccessModifier.PUBLIC)
    var isStatic: Boolean
        get() = modifiers.contains(MethodModifier.STATIC)
        set(value) = modifiers.switch(MethodModifier.STATIC, value)

    private fun setAccessModifier(modifier: AccessModifier) {
        accessModifier = modifier
    }

    @JvmOverloads
    fun staticModifier(set: Boolean = true) = apply { isStatic = set }

    fun privateModifier() = apply { accessModifier = AccessModifier.PRIVATE }

    fun matchNameAndParams(methodCm: MethodCm) = name == methodCm.name && paramTypes == methodCm.paramTypes

    private val paramTypes: List<ClassCd> get() = params.map { it.type }

    fun setResultType(canonicalName: String) {
        resultType = ClassCd.of(canonicalName)
    }

    fun addRequiredParam(name: String, type: ClassCd) =
        apply { params.add(FieldCm(name, type) { addAnnotation(Java.nonnul()) }) }

    fun markAsUsed() {
        if (!used) {
            used = true
            ownerClass.markAsUsed()
            ownerInterface?.markAsUsed()
            resultType.markAsUsed()
            params.forEach { it.markAsUsed() }
            implDependencies.markAsUsed()
        }
    }

    fun forceGeneration() = apply { this.used = true }
    fun registerAsMapper() = apply { mapper = true }

    internal fun collectDependencies(dependencies: Dependencies) {
        resultType.collectDependencies(dependencies)
        params.forEach { it.collectDependencies(dependencies) }
        annotations.forEach { it.collectDependencies(dependencies) }
        implDependencies.collectDependencies(dependencies)
        throwExceptions.forEach { it.collectDependencies(dependencies) }
    }

    fun simpleSignature() = "$name(${params.joinToString { it.name }})"

    override fun toString(): String = "${name}(${params})"

    fun interface Setter {
        fun MethodCm.setup()
    }

    private fun Setter.setup(obj: MethodCm) = obj.setup()

    fun setup(setter: Setter) = apply { setter.setup(this) }
}

private enum class MethodModifier {
    STATIC,
}
