/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.CodeGenerationTask
import pl.metaprogramming.codegen.GeneratorBuilder
import pl.metaprogramming.model.oas.OpenapiWriter
import pl.metaprogramming.model.oas.RestApi

class SpringSwaggerUiGenerator : GeneratorBuilder.Simple<SwaggerUiModel, SpringSwaggerUiGenerator>() {

    var name: String = "Swagger UI"
    var projectDir: String = ""
    var projectSubDir: String = "src/main/static"
    var swaggerUiVersion: String = "4.18.2"
    private val apis: MutableMap<RestApi, String> = mutableMapOf()

    inner class Generator(override val model: SwaggerUiModel) : pl.metaprogramming.codegen.Generator<SwaggerUiModel> {

        private val _codes = mutableListOf<CodeGenerationTask<*>>()
        override val codesToGenerate: List<CodeGenerationTask<*>> = _codes
        override fun generate() {
            _codes.add(CodeGenerationTask("${model.outDir}/index.html", model, ::makeIndexHtmlContent))
            val writer = OpenapiWriter(model.apis)
            model.apis.forEach { (api, filename) ->
                _codes.add(CodeGenerationTask("${model.outDir}/${filename}", api, writer::toText))
            }
        }

        private fun makeIndexHtmlContent(model: SwaggerUiModel): String {
            val urls = model.apis.map { "{url: '${it.value}', name: '${it.key.name}'}" }.joinToString(", ")
            return """<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta
      name="description"
      content="SwaggerUI"
    />
    <title>${model.name}</title>
    <link rel="stylesheet" href="https://unpkg.com/swagger-ui-dist@${model.swaggerUiVersion}/swagger-ui.css" />
  </head>
  <body>
  <div id="swagger-ui"></div>
  <script src="https://unpkg.com/swagger-ui-dist@${model.swaggerUiVersion}/swagger-ui-bundle.js" crossorigin></script>
  <script src="https://unpkg.com/swagger-ui-dist@${model.swaggerUiVersion}/swagger-ui-standalone-preset.js" crossorigin></script>
  <script>
    window.onload = () => {
      window.ui = SwaggerUIBundle({
        urls: [${urls}],
        validatorUrl : null,
        dom_id: '#swagger-ui',
        presets: [
          SwaggerUIBundle.presets.apis,
          SwaggerUIStandalonePreset
        ],
        layout: "StandaloneLayout",
        deepLinking: true,
        displayOperationId: true,
      });
    };
  </script>
  </body>
</html>
"""
        }
    }

    fun api(api: RestApi, filename: String) = apply { apis[api] = filename }

    override fun make(): pl.metaprogramming.codegen.Generator<SwaggerUiModel> {
        val dirsGlue = if (!projectDir.endsWith('/') && !projectSubDir.startsWith('/')) "/" else ""
        return Generator(
            SwaggerUiModel(
                outDir = projectDir + dirsGlue + projectSubDir,
                apis = apis,
                name = name,
                swaggerUiVersion = swaggerUiVersion
            )
        )
    }
}