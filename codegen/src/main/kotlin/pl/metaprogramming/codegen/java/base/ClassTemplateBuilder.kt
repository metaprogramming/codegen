/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import freemarker.ext.beans.GenericObjectModel
import freemarker.template.TemplateMethodModelEx
import pl.metaprogramming.codegen.CodeGenerationTask
import pl.metaprogramming.codegen.NEW_LINE
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.template.TemplateGenerator

class ClassTemplateBuilder<M>(
    private val context: BuildContext<M>,
    private val template: TemplateGenerator,
    private val dependencies: List<Any>
) : ClassBuilder<M>(context.model, context.typeOfCode, context.classCm, context.generator) {

    private val codeModel: MutableMap<String, Any> = mutableMapOf("classCd" to classCd, "params" to context.params)
    override fun makeDecoration() = Unit
    override fun makeDeclaration() = Unit

    override fun makeImplementation() {
        context.params.paramsMap.forEach { (k, v) ->
            codeModel[k.simpleName] = v
        }
        dependencies.forEach {
            codeModel[it.toString()] = context.getClass(it)
        }
        codeModel["importDependencies"] = ImportDependencies()
    }

    override fun makeGenerationTask(filePath: String): CodeGenerationTask<*> =
        CodeGenerationTask(filePath, codeModel, template)

    inner class ImportDependencies : TemplateMethodModelEx {
        override fun exec(args: List<Any?>): String {
            val classes = if (args.isEmpty()) dependencies.map { context.getClass(it) } else
                args.map { (it as GenericObjectModel).wrappedObject as ClassCd }
            return classes
                .filter { it.packageName != classCd.packageName }
                .map { it.toString() }
                .sorted()
                .joinToString(NEW_LINE) { "import $it;" }
        }
    }
}