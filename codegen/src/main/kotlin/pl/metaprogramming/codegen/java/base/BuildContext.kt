/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.Generator
import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.model.data.DataSchema

class BuildContext<M>(
    val model: M,
    val typeOfCode: TypeOfCode<M>,
    className: String,
    packageName: String,
    val params: CodegenParams,
    val forceGeneration: Boolean,
    private val codeIndex: CodeIndex,
    private val dataTypeMapper: DataTypeMapper,
    val generator: Generator<*>,
) {
    val classCm = ClassCm(packageName, className, this)
    val classesToGenerate get() = codeIndex.classesToGenerate(generator)

    @JvmOverloads
    fun classLocator(typeOfCode: Any, model: Any? = this.model): ClassLocator =
        ClassLocator(codeIndex, dataTypeMapper, typeOfCode, model)

    fun getClass(typeOfCode: Any, model: Any? = this.model): ClassCd = classLocator(typeOfCode, model).get()

    val nameMapper get() = params[JavaParams::class].nameMapper

    fun getFieldName(schema: DataSchema): String =
        schema.getAdditive("JAVA_NAME") { nameMapper.toFieldName(schema) }

    fun registerMapper(mapperBuilder: MethodCmBuilder<*>) {
        mapperBuilder.context = this
        codeIndex.putMapper(mapperBuilder)
    }

    fun registerMapper(ownerClass: ClassCd, methodName: String, builder: MethodCm.Setter) =
        codeIndex.putMappers(MethodCm(methodName, ownerClass, builder))


    fun findMapper(resultType: ClassCd, params: List<ClassCd>, failIfNotFound: Boolean) =
        codeIndex.getMapper(resultType, params, failIfNotFound)

    fun findMapper(resultType: ClassCd, params: List<ClassCd>) = findMapper(resultType, params, true)!!

    fun makeInnerClassContext(innerClassName: String): BuildContext<M> {
        val newCtx = BuildContext(
            model,
            typeOfCode,
            "${classCm.className}.$innerClassName",
            classCm.packageName,
            params,
            true,
            codeIndex,
            dataTypeMapper,
            generator
        )
        classCm.innerClasses.add(newCtx.classCm)
        return newCtx
    }

}