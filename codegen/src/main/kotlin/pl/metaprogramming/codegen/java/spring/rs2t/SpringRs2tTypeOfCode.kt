/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.model.data.EnumType
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.oas.HttpRequestSchema
import pl.metaprogramming.model.oas.Operation

object SpringRs2tTypeOfCode {
    @JvmStatic val REST_CLIENT = TypeOfCode<List<Operation>>("RS2T;REST_CLIENT")
    @JvmStatic val REST_CLIENT_IMPL = TypeOfCode<List<Operation>>("RS2T;REST_CLIENT_IMPL")

    @JvmStatic val REST_CONTROLLER_MO = TypeOfCode<List<Operation>>("RS2T;REST_CONTROLLER_MO") // all operation from group in one controller class
    @JvmStatic val REST_CONTROLLER = TypeOfCode<Operation>("RS2T;REST_CONTROLLER") // controller class for one operation

    @JvmStatic val FACADE = TypeOfCode<List<Operation>>("RS2T;FACADE")
    @JvmStatic val FACADE_IMPL = TypeOfCode<List<Operation>>("RS2T;FACADE_IMPL")
    @JvmStatic val ENUM = TypeOfCode<EnumType>("RS2T;ENUM")
    @JvmStatic val DTO = TypeOfCode<ObjectType>("RS2T;DTO")
    @JvmStatic val REQUEST_DTO = TypeOfCode<HttpRequestSchema>("RS2T;REQUEST_DTO")
    @JvmStatic val RESPONSE_DTO = TypeOfCode<Operation>("RS2T;RESPONSE_DTO")

    @JvmStatic val REST_MAPPER = TypeOfCode<ObjectType>("RS2T;REST_MAPPER") // REST_DTO <-> REST_REQUEST_DTO
    @JvmStatic val REQUEST_MAPPER = TypeOfCode<HttpRequestSchema>("RS2T;REQUEST_MAPPER") // REQUEST_REST_DTO -> REQUEST_DTO, REQUEST_DTO -> HttpEntity
    @JvmStatic val RESPONSE_MAPPER = TypeOfCode<Operation>("RS2T;RESPONSE_MAPPER") // RESPONSE_DTO -> ResponseEntity, ResponseEntity|HttpStatusCodeException -> RESPONSE_DTO

    @JvmStatic val REST_REQUEST_DTO = TypeOfCode<HttpRequestSchema>("RS2T;REST_REQUEST_DTO")
    @JvmStatic val REST_DTO = TypeOfCode<ObjectType>("RS2T;REST_DTO")
}