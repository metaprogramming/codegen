/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.*
import pl.metaprogramming.codegen.java.CodeIndex.ClassAlreadyRegistered
import pl.metaprogramming.codegen.java.base.*
import pl.metaprogramming.codegen.java.formatter.PackageInfoFormatter
import pl.metaprogramming.log
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.NamedDataType

abstract class JavaGenerator<T : Model>(
    configurator: JavaGeneratorBuilder<T, *, *>,
) : Generator<T> {
    override val model: T = configurator.model
    protected val codeIndex = configurator.codeIndex
    protected val codeBuilders = configurator.codeBuilders
    protected val params = configurator.params
    protected val dataTypeMapper = configurator.dataTypeMapper
    protected var packageInfoList: List<PackageInfoCm> = emptyList()
    private val usedPackages = mutableSetOf<String>()

    override val codesToGenerate: List<CodeGenerationTask<*>>
        get() = if (params.dryRun) emptyList()
        else makeClassGenerationTasksFromClassIndex() + makePackageInfoGenerationsTasks()

    override fun toString() = "${javaClass.simpleName} for ${model.name}"

    protected fun makeCodeModels() {
        codeIndex.makeCodeModels()
    }

    /** Force generation code of [typeOfCode] for [model]. */
    protected fun <M> forceGeneration(typeOfCode: TypeOfCode<M>, model: M) = codeIndex.markAsUsed(typeOfCode, model)

    /** Force generation code of [typeOfCode] (with no model). */
    protected fun forceGeneration(typeOfCode: TypeOfCodeWithNoModel) = codeIndex.markAsUsed(typeOfCode, null)

    /** Register in [CodeIndex] the external class (not generated class) as representation for [model] and [typeOfCode]. */
    protected fun <M> registerExternalClass(typeOfCode: TypeOfCode<M>, model: M, classCd: ClassCd) =
        codeIndex.put(classCd, model, typeOfCode)

    /** Register in [CodeIndex] the code of [typeOfCode] with no model. */
    protected fun register(typeOfCode: TypeOfCodeWithNoModel) = putToCodeIndex(typeOfCode, null)

    /** Register in [CodeIndex] the code of [typeOfCode] for [model]. */
    protected fun <M> register(typeOfCode: TypeOfCode<M>, model: M) =
        putToCodeIndex(typeOfCode, model)

    /**
     * Register in [CodeIndex] the code of [typeOfCode] for [schema].
     *
     * Note [schema].dataType should be an instance of [NamedDataType].
     * The [schema].dataType is taken as model and [schema].dataType.code as model name.
     */
    @Suppress("UNCHECKED_CAST")
    protected fun <M : DataType> register(typeOfCode: TypeOfCode<M>, schema: DataSchema) =
        register(typeOfCode, schema.dataType as M)

    private fun makeClassGenerationTasksFromClassIndex() = codeIndex.classesToGenerate(this).map {
        it.makeDecoration()
        makeCodeGenerationTask(it)
    }

    private fun makePackageInfoGenerationsTasks() = packageInfoList
        .filter { usedPackages.contains(it.packageName) }
        .map { CodeGenerationTask(it.getFilePath(), it, PackageInfoFormatter()) }

    private fun makeCodeGenerationTask(entry: ClassBuilder<*>): CodeGenerationTask<*> {
        val packageName = entry.classCd.packageName ?: ""
        check(packageName.isNotEmpty()) { "[${toString()}] Generated classes should have specified package: ${entry.classCd}" }
        usedPackages.add(packageName)
        try {
            return entry.makeGenerationTask(entry.getFilePath()).apply {
                codeDecorators = params[JavaParams::class].codeDecorators
            }
        } catch (e: Exception) {
            throw IllegalStateException("[${toString()}] Can't make CodeGenerationTask for: ${entry.typeOfCode}", e)
        }
    }

    private fun getJavaFilePath(baseDir: String, packageName: String, className: String) =
        "$baseDir/${packageName.toDir()}/${className}.java"

    private fun ClassBuilder<*>.getFilePath() =
        getJavaFilePath(codeBuilders.getValue(typeOfCode).baseDir, classCd.packageName!!, classCd.className)

    private fun PackageInfoCm.getFilePath() = getJavaFilePath(baseDir, packageName, "package-info")

    private fun String.toDir() = this.replace('.', '/')

    /**
     * It may return null value when given typeOfCode was removed - will be not generated.
     */
    private fun <M> putToCodeIndex(typeOfCode: TypeOfCode<M>, model: M?): ClassBuilder<M>? {
        try {
            return codeBuilders[typeOfCode]?.let {
                @Suppress("UNCHECKED_CAST", "kotlin:S6530")
                val builder = (it as ClassBuilderConfigurator<M>).makeBuilder(model as M)
                codeIndex.put(builder, it.registerAsTypeOfCode ?: builder.typeOfCode)
                builder
            }
        } catch (e: ClassAlreadyRegistered) {
            if (params.allowDuplicateClasses) {
                log.warn(e.message)
                return null
            } else {
                throw e
            }
        } catch (e: Exception) {
            throw IllegalStateException("Can't register code generation $typeOfCode for $model (${this.model})", e)
        }
    }

    private fun <M> ClassBuilderConfigurator<M>.makeBuilder(model: M): ClassBuilder<M> {
        val nameMapper = params[JavaParams::class].nameMapper
        val className: String = nameMapper.toClassName(className.make(model))
        val packageName: String = packageName.make(model)
        val buildContext = BuildContext(
            model,
            typeOfCode,
            className,
            packageName,
            params,
            forceGeneration,
            codeIndex,
            dataTypeMapper,
            this@JavaGenerator
        )
        if (template != null) {
            if (builders.isNotEmpty()) {
                log.warn("{} - code model builders will be ignored, only the template will be taken into account", typeOfCode)
            }
            return ClassTemplateBuilder(buildContext, template!!, dependencies)
        }
        return ClassCmBuildDirector(buildContext, orderedStrategies())
    }
}