/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.Parameter

interface JavaNameMapper {
    fun toClassName(text: String): String
    fun toConstantName(text: String): String
    fun toMethodName(text: String) = toClassName(text).replaceFirstChar { it.lowercaseChar() }
    fun toFieldName(text: String) = toMethodName(text)
    fun toPackagePart(text: String) = toMethodName(text).lowercase()

    fun toFieldName(schema: DataSchema) = toFieldName(if (schema is Parameter) schema.name else schema.code!!)
}

