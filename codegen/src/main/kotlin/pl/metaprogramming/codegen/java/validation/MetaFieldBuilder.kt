/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.base.ClassCmFields
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.upperFirstChar

class MetaFieldBuilder(
    private val fields: ClassCmFields,
    private val dtoClass: ClassCm,
    private val fieldClass: ClassCd
) {
    fun add(schema: DataSchema) {
        val fieldCm = dtoClass.fields.getByModel(schema)
        val originalName = if (schema is Parameter) "${schema.name} (${schema.location} parameter)" else schema.code
        val descriptionFieldName = schema.getAdditive(DESCRIPTION_FIELD_NAME) { "FIELD_" + toUpperCase(fieldCm.name) }
        fields.add(descriptionFieldName, fieldClass.withGeneric(dtoClass, fieldCm.type)) {
            isPublic = true
            isStatic = true
            isFinal = true
            setValue("new ${fieldClass.className}<>(\"$originalName\", ${dtoClass.className}::get${fieldCm.name.upperFirstChar()})")
        }
    }

    private fun toUpperCase(javaName: String): String {
        val buf = StringBuilder()
        javaName.forEachIndexed { i, c ->
            if (Character.isUpperCase(c) && i > 0) {
                buf.append('_')
            }
            buf.append(Character.toUpperCase(c))
        }
        return buf.toString()
    }
}