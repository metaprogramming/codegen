/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.model.data.DataType
import java.util.function.Function

class DataTypeMapper {

    private val map: MutableMap<DataType, ClassCd> = mutableMapOf()
    private var function: Function<DataType, ClassCd?> = Function { null }
    private val mapperByContext: MutableMap<TypeOfCode<*>, DataTypeMapper> = mutableMapOf()

    constructor() {
        mapOf(
            DataType.TEXT to "java.lang.String",
            DataType.DATE to "java.time.LocalDate",
            DataType.DATE_TIME to "java.time.LocalDateTime",
            DataType.NUMBER to "java.math.BigDecimal",
            DataType.DECIMAL to "java.math.BigDecimal",
            DataType.FLOAT to "java.lang.Float",
            DataType.DOUBLE to "java.lang.Double",
            DataType.BYTE to "java.lang.Byte",
            DataType.INT16 to "java.lang.Short",
            DataType.INT32 to "java.lang.Integer",
            DataType.INT64 to "java.lang.Long",
            DataType.BOOLEAN to "java.lang.Boolean",
            DataType.BINARY to "byte[]",
            DataType.BASE64 to "byte[]",
        ).forEach { this.map[it.key] = ClassCd.of(it.value) }
    }

    constructor(map: Map<DataType, String>) {
        map.forEach { this.map[it.key] = ClassCd.of(it.value) }
    }

    constructor(function: Function<DataType, ClassCd?>) {
        this.function = function
    }

    operator fun set(dataType: DataType, classCd: ClassCd) {
        map[dataType] = classCd
    }

    operator fun set(dataType: DataType, canonicalName: String) {
        set(dataType, ClassCd.of(canonicalName))
    }

    operator fun set(typeOfCode: TypeOfCode<*>, mapper: DataTypeMapper) {
        mapperByContext[typeOfCode] = mapper
    }

    operator fun get(dataType: DataType): ClassCd? = map[dataType] ?: function.apply(dataType)

    operator fun get(dataType: DataType, typeOfCode: TypeOfCode<*>): ClassCd? =
        mapperByContext.getOrDefault(typeOfCode, this)[dataType]

    fun copy(): DataTypeMapper {
        val result = DataTypeMapper(function)
        map.forEach { result.map[it.key] = it.value }
        mapperByContext.forEach { result.mapperByContext[it.key] = it.value }
        return result
    }
}