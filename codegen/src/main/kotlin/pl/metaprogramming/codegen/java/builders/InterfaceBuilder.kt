/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.builders

import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.ClassKind
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Java

class InterfaceBuilder<M>(private val implClassType: TypeOfCode<M>) : ClassCmBuilderTemplate<M>() {
    override fun makeDeclaration() {
        kind = ClassKind.INTERFACE
        val implClass = classLocator(implClassType).getDeclared()
        implClass.interfaces.add(classCm)
        implClass.methods.filter {
            it.isPublic && !it.isConstructor
        }.forEach { method ->
            methods.add(method.name) {
                resultType = method.resultType
                params.addAll(method.params)
                description = method.description
            }
            method.description = null
            method.annotations.add(Java.override())
            method.ownerInterface = classCm
        }
    }
}