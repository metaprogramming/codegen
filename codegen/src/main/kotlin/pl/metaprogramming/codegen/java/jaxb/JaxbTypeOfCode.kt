/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.jaxb

import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.TypeOfCodeWithNoModel
import pl.metaprogramming.model.data.XmlEnumType
import pl.metaprogramming.model.data.XmlObjectType

object JaxbTypeOfCode {
    @JvmStatic val ENUM = TypeOfCode<XmlEnumType>("JAXB;ENUM")
    @JvmStatic val DTO = TypeOfCode<XmlObjectType>("JAXB;DTO")
    @JvmStatic val LOCAL_DATE_ADAPTER = TypeOfCodeWithNoModel("JAXB;LOCAL_DATE_ADAPTER")
    @JvmStatic val LOCAL_DATE_TIME_ADAPTER = TypeOfCodeWithNoModel("JAXB;LOCAL_DATE_TIME_ADAPTER")
}