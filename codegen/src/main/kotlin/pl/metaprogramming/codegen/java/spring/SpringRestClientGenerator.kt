/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.java.JavaGenerator
import pl.metaprogramming.codegen.java.JavaGeneratorBuilder
import pl.metaprogramming.codegen.java.builders.InterfaceBuilder
import pl.metaprogramming.codegen.java.libs.Jackson
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Lombok
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.rs.*
import pl.metaprogramming.codegen.java.test.TestDataParams
import pl.metaprogramming.codegen.java.test.TestDataProviderBuilder
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.RestApi

class SpringRestClientGenerator :
    JavaGeneratorBuilder<RestApi, SpringRestClientGenerator.TOC, SpringRestClientGenerator>(TOC) {

    object TOC {
        @JvmField val CLIENT = SpringRsTypeOfCode.CLIENT
        @JvmField val CLIENT_IMPL = SpringRsTypeOfCode.CLIENT_IMPL
        @JvmField val DTO = SpringRsTypeOfCode.DTO
        @JvmField val REQUEST_DTO = SpringRsTypeOfCode.REQUEST_DTO
        @JvmField val ENUM = SpringRsTypeOfCode.ENUM
        @JvmField val REST_CLIENT_PROVIDER = SpringRsTypeOfCode.REST_CLIENT_PROVIDER
        @JvmField val TEST_DATA_PROVIDER = SpringRsTypeOfCode.TEST_DATA_PROVIDER
    }

    class Generator(generatorBuilder: SpringRestClientGenerator) :
        JavaGenerator<RestApi>(generatorBuilder) {
        override fun generate() {
            register(TOC.REST_CLIENT_PROVIDER, model)
            addSchemaCodes()
            addOperationCodes()
            makeCodeModels()
        }


        private fun addSchemaCodes() {
            model.schemas.forEach { schema ->
                if (schema.isEnum) {
                    register(TOC.ENUM, schema)
                }
                if (schema.isObject) {
                    register(TOC.DTO, schema)
                    if (params[TestDataParams::class].isEnabled) {
                        register(TOC.TEST_DATA_PROVIDER, schema)
                    }
                }
            }
        }

        private fun addOperationCodes() {
            model.groupedOperations.values.forEach { operations ->
                operations.forEach { operation ->
                    register(TOC.REQUEST_DTO, operation.requestSchema)
                    addTestDataProviders(operation)
                }
                register(TOC.CLIENT_IMPL, operations)
                register(TOC.CLIENT, operations)
            }
        }

        private fun addTestDataProviders(operation: Operation) {
            if (params[TestDataParams::class].isEnabled) {
                operation.responses.forEach {
                    val schema = it.schema
                    if (schema != null && schema.isTypeOrItemType(DataTypeCode.OBJECT) && schema.objectType.isNamed) {
                        forceGeneration(TOC.TEST_DATA_PROVIDER, schema.objectType)
                    }
                }
            }
        }
    }

    override fun make(): pl.metaprogramming.codegen.Generator<RestApi> {
        return Generator(this)
    }

    var adapterPackageName: String = "rest"

    private val dtoPackageTail get() = "ports.out.${adapterPackageName}.dtos"
    private val clientPackageTail get() = "adapters.out.${adapterPackageName}"

    override fun init() {
        dataTypeMapper[DataType.DATE_TIME] = "java.time.ZonedDateTime"
        dataTypeMapper[DataType.BINARY] = Spring.resource()
        typeOfCode(toc.ENUM) {
            registerAsTypeOfCode = toc.DTO
            className.resolver { it.code }
            packageName.infix { _ -> dtoPackageTail }
            builders += EnumBuilder()
        }

        typeOfCode(toc.DTO) {
            className {
                resolver { it.code }
                suffix = "Dto"
            }
            packageName.infix { _ -> dtoPackageTail }
            builders += DtoBuilder()
            builders += Lombok.dataBuilder()
        }

        typeOfCode(toc.REQUEST_DTO) {
            className {
                resolver { it.operation.code }
                suffix = "Request"
            }
            packageName.infix { _ -> dtoPackageTail }
            builders += Lombok.dataBuilder()
            builders += DtoBuilder { it.schema }
        }

        typeOfCode(toc.CLIENT_IMPL) {
            forceGeneration = true
            className {
                resolver { it.first().group }
                suffix = "ClientImpl"
            }
            packageName.infix { _ -> clientPackageTail }
            addManagedComponentBuilder()
            onDeclaration {
                model.forEach { methods.add(RestTemplateCallMethodBuilder(it)) }
            }
        }

        typeOfCode(toc.CLIENT) {
            forceGeneration = true
            className {
                resolver { it.first().group }
                suffix = "Client"
            }
            packageName.infix { _ -> "ports.out.${adapterPackageName}" }
            builders += InterfaceBuilder(toc.CLIENT_IMPL)
        }

        typeOfCode(toc.REST_CLIENT_PROVIDER) {
            className {
                resolver(RestApi::name)
                suffix = "RestClientProvider"
            }
            packageName.infix { _ -> clientPackageTail }
            addManagedComponentBuilder()
            onImplementation {
                val endpointProperty = "${nameMapper.toConstantName(model.name)}_BASE_URL"
                fields.add("baseUrl", Java.string()) {
                    addAnnotation(Spring.value("\${$endpointProperty:http://localhost:8080}"))
                }
                methods.add("of") {
                    resultType = getClass(SpringRsCommonTypeOfCode.REST_CLIENT)
                    params.add("method", Spring.httpMethod())
                    params.add("path", Java.string())

                    val defaultErrorResponse = model.defaultErrorResponse
                    val restTemplate = classCm.injectDependency(Spring.restTemplate()).name
                    val objectMapper = classCm.injectDependency(Jackson.objectMapper()).name
                    val impl = "new ${resultType.className}(method, baseUrl + path, $restTemplate, $objectMapper)"

                    implBody = if (defaultErrorResponse != null) {
                        val defaultErrorResponseClass = getClass(SpringRsTypeOfCode.DTO, defaultErrorResponse)
                        implDependencies.add(defaultErrorResponseClass)
                        "return $impl.onError(${defaultErrorResponseClass.className}.class);"
                    } else {
                        "return $impl;"
                    }
                }
            }
        }

        typeOfCode(toc.TEST_DATA_PROVIDER) {
            className {
                resolver { it.code }
                suffix = "Data"
            }
            projectSubDir("src/test/java")
            builders += TestDataProviderBuilder()
            builders += Java.privateConstructorBuilder()
        }
    }
}
