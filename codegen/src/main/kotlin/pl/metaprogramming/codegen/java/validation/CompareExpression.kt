/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.ObjectType

private val OPERATOR_MAP = mapOf(
    ">" to "gt",
    ">=" to "ge",
    "<" to "lt",
    "<=" to "le",
    "=" to "eq",
)

class CompareExpression(val field1: DataSchema, val field2: DataSchema, val operator: String) {
    companion object {
        fun parse(expression: String, schema: ObjectType) = Parser(expression, schema).parse()
    }
}

private class Parser(val expression: String, val schema: ObjectType) {
    fun parse(): CompareExpression {
        val tokens = expression.split(' ')
        check(tokens.size == 3) { errorMessage() }
        check(OPERATOR_MAP.containsKey(tokens[1]))
        val field1 = getField(tokens[0])
        val field2 = getField(tokens[2])
        check(field1.dataType == field2.dataType) { errorMessage("fields should be of the same type") }
        return CompareExpression(field1, field2, OPERATOR_MAP.getValue(tokens[1]))
    }

    private fun getField(fieldCode: String): DataSchema =
        checkNotNull(schema.field(fieldCode)) { errorMessage("$fieldCode doesn't exists") }

    private fun errorMessage(details: String = "") =
        "Invalid expression: $expression, should have syntax compare:field1 [>|>=|<|<=|=] filed2, $details"
}
