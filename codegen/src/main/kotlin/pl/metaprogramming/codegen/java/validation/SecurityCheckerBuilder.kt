/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Java

class SecurityCheckerBuilder : ClassCmBuilderTemplate<String>() {

    override fun makeDeclaration() {
        val privilegesEnum = getClass(ValidationCommonTypeOfCode.SECURITY_PRIVILEGE_ENUM)
        methods.add("check") {
            resultType = getClass(ValidationCommonTypeOfCode.VALIDATION_CHECKER).withGeneric(Java.string())
            params.add("privileges", privilegesEnum.asArray()) { vararg = true }
            implDependencies
                .add("java.util.Arrays")
                .add(getClass(ValidationCommonTypeOfCode.VALIDATION_COMMON_CHECKERS), "credentials")
            implBody = "return credentials(this::getCredentials, Arrays.asList(privileges));"
        }

        methods.add("getCredentials") {
            resultType = privilegesEnum.asCollection()
            isPrivate = true
            params.add("ctx", getClass(ValidationCommonTypeOfCode.VALIDATION_CONTEXT).withGeneric(Java.string()))
            implBody = "throw new UnsupportedOperationException();"
        }

    }
}