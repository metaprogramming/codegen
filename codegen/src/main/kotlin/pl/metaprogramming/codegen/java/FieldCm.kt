/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.java.FieldCm.Setter
import pl.metaprogramming.codegen.java.internal.AccessModifier
import pl.metaprogramming.codegen.java.libs.Java
import java.util.*

private const val UNNAMED = "<unnamed>"

class FieldCm private constructor(
    var type: ClassCd,
    var name: String = UNNAMED,
    var value: ValueCm? = null,
    var annotations: MutableList<AnnotationCm> = mutableListOf(),
    var model: Any? = null,
    var description: String? = null,
    internal var accessModifier: AccessModifier = AccessModifier.PRIVATE,
    private val modifiers: MutableSet<FieldModifier> = EnumSet.noneOf(FieldModifier::class.java),
    var isNonnull: Boolean = false,
    var vararg: Boolean = false,
) {
    constructor(type: ClassCd) : this(type, UNNAMED)
    constructor(type: ClassCd, name: String) : this(type, name, value = null)
    constructor(name: String, type: ClassCd, setter: Setter) : this(type, name) {
        setter.setup(this)
    }

    companion object {
        @JvmStatic
        @JvmOverloads
        fun of(name: String, type: ClassCd, builder: Setter = Setter {}) =
            FieldCm(name, type, builder)

        @JvmStatic
        fun stringValue(value: String) = FieldCm(Java.string(), value = ValueCm.escaped(value))
    }

    fun withModel(model: Any) = FieldCm(
        type,
        name,
        value,
        annotations.toMutableList(),
        model,
        description,
        accessModifier,
        EnumSet.copyOf(modifiers),
        isNonnull
    )

    fun withName(name: String) = FieldCm(type, name)

    fun collectDependencies(dependencies: Dependencies) {
        type.collectDependencies(dependencies)
        value?.dependencies?.collectDependencies(dependencies)
        annotations.forEach { it.collectDependencies(dependencies) }
    }

    fun markAsUsed() {
        type.markAsUsed()
        annotations.forEach { it.markAsUsed() }
    }

    fun addAnnotation(annotation: AnnotationCm) = apply { annotations.add(annotation) }

    fun hasAnnotation(annotationClass: String) =
        annotations.any { it.annotationClass.canonicalName == annotationClass }

    fun setValue(value: String) = apply { this.value = ValueCm.value(value) }

    val isUnnamed: Boolean get() = name == UNNAMED
    var isPublic: Boolean
        get() = accessModifier == AccessModifier.PUBLIC
        set(value) = setAccessModifier(if (!value) AccessModifier.PRIVATE else AccessModifier.PUBLIC)
    var isPrivate: Boolean
        get() = accessModifier == AccessModifier.PRIVATE
        set(value) = setAccessModifier(if (value) AccessModifier.PRIVATE else AccessModifier.PUBLIC)
    var isFinal: Boolean
        get() = modifiers.contains(FieldModifier.FINAL)
        set(value) = modifiers.switch(FieldModifier.FINAL, value)
    var isStatic: Boolean
        get() = modifiers.contains(FieldModifier.STATIC)
        set(value) = modifiers.switch(FieldModifier.STATIC, value)
    var isTransient: Boolean
        get() = modifiers.contains(FieldModifier.TRANSIENT)
        set(value) = modifiers.switch(FieldModifier.TRANSIENT, value)

    override fun toString(): String {
        return "${name}: $type"
    }

    private fun setAccessModifier(modifier: AccessModifier) {
        accessModifier = modifier
    }

    fun interface Setter {
        fun FieldCm.setup()
    }

    private fun Setter.setup(obj: FieldCm) = obj.setup()
}

private enum class FieldModifier {
    STATIC,
    FINAL,
    TRANSIENT
}
