/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.validation

import pl.metaprogramming.model.data.constraint.CheckerRef

/**
 * Representation of a set of parameters allowing the parameterization
 * of the generated validation codes.
 */
class ValidationParams {
    var formatValidators = CheckerMap()
    var ruleCheckers = CheckerMap()
    var useJakartaBeanValidation: Boolean = false
    var stopAfterFirstError: Boolean = false
    var throwExceptionIfValidationFailed: Boolean = true
    var useNamedBeanAsRuleValidator: Boolean = true
    var securityValidation = true
    var isRequiredErrorCode = "is_required"
    var isNotAllowedErrorCode = "cannot_be_set"
    var isInvalidDataErrorCode = "invalid_data"
    var isNotBase64ErrorCode = "is_not_base64"
    var isNotBooleanErrorCode = "is_not_boolean"
    var isNotLongErrorCode = "is_not_64bit_integer"
    var isNotIntErrorCode = "is_not_32bit_integer"
    var isNotFloatErrorCode = "is_not_float"
    var isNotDoubleErrorCode = "is_not_double"
    var isNotNumberErrorCode = "is_not_number"
    var isNotDateErrorCode = "is_not_date"
    var isNotDateTimeErrorCode = "is_not_date_time"
    var isNotAllowedValueErrorCode = "is_not_allowed_value"
    var isTooSmallErrorCode = "is_too_small"
    var isTooBigErrorCode = "is_too_big"
    var isTooShortErrorCode = "is_too_short"
    var isTooLongErrorCode = "is_too_long"
    var isNotEqualErrorCode = "is_not_equal"
    var isNotMatchPatternErrorCode = "is_not_match_pattern"
    var hasTooFewItemsErrorCode = "has_too_few_items"
    var hasTooManyItemsErrorCode = "has_too_many_items"
    var hasDuplicatedItemsErrorCode = "has_duplicated_items"

    fun formatValidator(code: String, validator: String) = apply { formatValidators[code] = validator }
    fun formatValidator(code: String, checkerRef: CheckerRef) = apply { formatValidators.set(code, checkerRef) }
    fun ruleValidator(rule: String, checkerClassName: String) = ruleValidator(rule, CheckerRef.of(checkerClassName))
    fun ruleValidator(rule: String, checkerRef: CheckerRef) = apply { ruleCheckers.set(rule, checkerRef) }

    fun useJakartaBeanValidation(value: Boolean) = apply { useJakartaBeanValidation = value }
    fun stopAfterFirstError(value: Boolean) = apply { stopAfterFirstError = value }
    fun throwExceptionIfValidationFailed(value: Boolean) = apply { throwExceptionIfValidationFailed = value }
    fun useNamedBeanAsRuleValidator(value: Boolean) = apply { useNamedBeanAsRuleValidator = value }
    fun securityValidation(value: Boolean) = apply { securityValidation = value }

    fun copy(): ValidationParams {
        val result = ValidationParams()
            .useJakartaBeanValidation(useJakartaBeanValidation)
            .stopAfterFirstError(stopAfterFirstError)
            .throwExceptionIfValidationFailed(throwExceptionIfValidationFailed)
            .useNamedBeanAsRuleValidator(useNamedBeanAsRuleValidator)
            .securityValidation(securityValidation)
        result.formatValidators = formatValidators.copy()
        result.ruleCheckers = ruleCheckers.copy()
        result.isRequiredErrorCode = isRequiredErrorCode
        result.isNotAllowedErrorCode = isNotAllowedErrorCode
        result.isInvalidDataErrorCode = isInvalidDataErrorCode
        result.isNotBase64ErrorCode = isNotBase64ErrorCode
        result.isNotBooleanErrorCode = isNotBooleanErrorCode
        result.isNotLongErrorCode = isNotLongErrorCode
        result.isNotIntErrorCode = isNotIntErrorCode
        result.isNotFloatErrorCode = isNotFloatErrorCode
        result.isNotDoubleErrorCode = isNotDoubleErrorCode
        result.isNotNumberErrorCode = isNotNumberErrorCode
        result.isNotDateErrorCode = isNotDateErrorCode
        result.isNotDateTimeErrorCode = isNotDateTimeErrorCode
        result.isNotAllowedValueErrorCode = isNotAllowedValueErrorCode
        result.isTooSmallErrorCode = isTooSmallErrorCode
        result.isTooBigErrorCode = isTooBigErrorCode
        result.isTooShortErrorCode = isTooShortErrorCode
        result.isTooLongErrorCode = isTooLongErrorCode
        result.isNotEqualErrorCode = isNotEqualErrorCode
        result.isNotMatchPatternErrorCode = isNotMatchPatternErrorCode
        result.hasTooFewItemsErrorCode = hasTooFewItemsErrorCode
        result.hasTooManyItemsErrorCode = hasTooManyItemsErrorCode
        result.hasDuplicatedItemsErrorCode = hasDuplicatedItemsErrorCode
        return result
    }
}