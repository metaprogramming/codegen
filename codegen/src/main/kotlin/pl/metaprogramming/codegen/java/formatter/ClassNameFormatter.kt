/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.formatter

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.Dependencies

class ClassNameFormatter(private val packageName: String?, private val dependencies: Dependencies) {
    /*
    Corner case to handle:
    1. method (and classes) with generic params
        public <T extends Some> T call(Class<T> clazz)
    2. expose nested generic params
        public Map<String, List<String>> call()
    3. avoid expose nested generic params
        object.cast(List.class)
    4. 'void' and 'Void' cases
        void callMe()
        ResponseEntity<Void> callMe()
    5. sometimes the name should be with the package name and sometimes not
     */

    fun classRef(classCd: ClassCd): String = "${getClassName(classCd)}${arraySuffix(classCd)}.class"

    @JvmOverloads
    fun format(classCd: ClassCd?, nested: Boolean = false, withGenericParamExtends: Boolean = false): String {
        if (classCd == null || classCd.isVoid) {
            return if (nested) "Void" else "void"
        }
        val buf = StringBuilder()
        buf.append(getClassName(classCd))
        buf.append(formatGenericParams(classCd.genericParams, withGenericParamExtends))
        if (classCd.superClass != null && (classCd.isUnknownGenericParam || (withGenericParamExtends && classCd.isGenericParam))) {
            buf.append(" extends ${format(classCd.superClass, true, withGenericParamExtends)}")
        }
        buf.append(arraySuffix(classCd))
        return buf.toString()
    }

    fun genericParamsDeclaration(genericParams: Collection<ClassCd>) = formatGenericParams(genericParams, true)

    private fun getClassName(classCd: ClassCd): String =
        if (isImported(classCd)) classCd.className else classCd.canonicalName

    private fun isImported(classCd: ClassCd) = classCd.packageName == JAVA_LANG_PKG ||
            classCd.packageName == packageName || dependencies.contains(classCd)

    private fun formatGenericParams(genericParams: Collection<ClassCd>, withGenericParamExtends: Boolean) =
        genericParams.joinToString(",") {
            format(it, true, withGenericParamExtends)
        }.let {
            if (it.isNotEmpty()) "<$it>" else ""
        }

    private fun arraySuffix(classCd: ClassCd): String = if (classCd.isArray) "[]" else ""
}