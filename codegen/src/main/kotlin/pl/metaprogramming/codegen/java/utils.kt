/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.upperFirstChar

fun String.asClassCd() = ClassCd.of(this)
fun String.asField(type: ClassCd) = FieldCm(type, this)
fun String.toGetter() = split('.').toGetter()
fun List<String>.toGetter() = first() + drop(1).joinToString("") { ".get${it.upperFirstChar()}()" }
fun <T> MutableSet<T>.switch(value: T, set: Boolean) {
    if (set) this.add(value) else this.remove(value)
}
