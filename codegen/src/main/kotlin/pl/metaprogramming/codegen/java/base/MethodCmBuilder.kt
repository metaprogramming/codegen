/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.MethodCm

abstract class MethodCmBuilder<T>(val model: T) : IMethodCmBuilder {
    lateinit var context: BuildContext<*>
    lateinit var classCm: ClassCm
    override val methodCm: MethodCm by lazy { makeDeclaration() }
    abstract fun makeDeclaration(): MethodCm
    fun makeDeclaration(context: BuildContext<*>): MethodCm {
        this.context = context
        this.classCm = context.classCm
        return methodCm
    }

    fun make(): MethodCm {
        makeDeclaration()
        makeImplementation()
        methodCm.markAsUsed()
        return methodCm
    }

    @JvmOverloads
    protected fun newMethodCm(name: String, setter: MethodCm.Setter = MethodCm.Setter {}): MethodCm =
        MethodCm(name, context.classCm, setter)
}