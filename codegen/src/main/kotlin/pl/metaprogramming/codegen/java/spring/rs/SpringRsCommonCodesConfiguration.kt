/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.JavaGeneratorBuilder
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Lombok
import pl.metaprogramming.codegen.java.libs.Spring

class SpringRsCommonCodesConfiguration : JavaGeneratorBuilder.Delegate() {
    override fun configure() {
        typeOfCode(SpringRsCommonTypeOfCode.SERIALIZATION_UTILS) {
            className.fixed = "SerializationUtils"
            builders += Java.privateConstructorBuilder()
            builders += SerializationUtilsBuilder()
        }

        typeOfCode(SpringRsCommonTypeOfCode.REST_CLIENT) {
            className.fixed = "RestClient"
            builders += RestClientBuilder()
        }

        typeOfCode(SpringRsCommonTypeOfCode.REST_CLIENT_EXCEPTION) {
            className.fixed = "RestClientException"
            onImplementation {
                superClass = ClassCd.of("java.lang.RuntimeException")
                addAnnotation(Lombok.requiredArgsConstructor())
                addAnnotation(Lombok.getter())

                fields.add("httpStatusCode", Java.boxedInteger()) {
                    isFinal = true
                    addAnnotation(Java.nonnul())
                }
                fields.add("headers", Spring.httpHeaders()) {
                    isFinal = true
                    addAnnotation(Java.nonnul())
                }
                fields.add("response", Java.objectType()) {
                    isFinal = true
                    isTransient = true
                    addAnnotation(Java.nonnul())
                }

                methods.add("getResponse") {
                    resultType = Java.genericParamT()
                    params.add("clazz", Java.claasType(Java.genericParamT()))
                    implBody = "return clazz.cast(response);"
                }
            }
        }
    }
}

private class SerializationUtilsBuilder : ClassCmBuilderTemplate<Nothing>() {
    override fun makeDeclaration() {
        // inner methods
        "fromString" method {
            resultType = Java.genericParamT()
            params.add("value", Java.string())
            params.add("transformer", "java.util.function.Function<java.lang.String, T>")
            implBody = "return value == null || value.isEmpty() ? null : transformer.apply(value);"
        }
        "toString" method {
            resultType = Java.string()
            params.add("value", Java.genericParamT())
            params.add("transformer", "java.util.function.Function<T, java.lang.String>")
            implBody = "return value == null ? null : transformer.apply(value);"
        }

        // exposed methods
        val rawParam = Java.string().asField("value")
        val formatParam = Java.string().asField("format")
        val toStringExp = "return toString(value, Object::toString);"

        // collections support
        "transformList" asMapper {
            setResultType("java.util.List<R>")
            params.add("value", "java.util.List<T>")
            params.add("transformer", "java.util.function.Function<T, R>")
            implBody = "return value == null ? null : value.stream().map(transformer).collect(Collectors.toList());"
            implDependencies.add(Java.collectors())
        }
        "transformMap" asMapper {
            setResultType("java.util.Map<K, R>")
            params.add("value", "java.util.Map<K, T>")
            params.add("transformer", "java.util.function.Function<T, R>")
            implBody =
                "return value == null ? null : value.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> transformer.apply(e.getValue())));"
            implDependencies.add(Java.collectors())
        }

        // long support
        "toLong" asMapper {
            resultType = Java.longBoxed()
            params.add(rawParam)
            implBody = "return fromString(value, Long::valueOf);"
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.longBoxed())
            implBody = toStringExp
        }

        // integer support
        "toInteger" asMapper {
            resultType = Java.boxedInteger()
            params.add(rawParam)
            implBody = "return fromString(value, Integer::valueOf);"
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.boxedInteger())
            implBody = toStringExp
        }

        // float support
        "toFloat" asMapper {
            resultType = Java.boxedFloat()
            params.add(rawParam)
            implBody = "return fromString(value, Float::valueOf);"
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.boxedFloat())
            implBody = toStringExp
        }
        
        // BigDecimal support
        "toBigDecimal" asMapper {
            resultType = Java.bigDecimal()
            params.add(rawParam)
            implBody = "return fromString(value, BigDecimal::new);"
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.bigDecimal())
            implBody = toStringExp
        }
        "toBigDecimal" asMapper {
            resultType = Java.bigDecimal()
            params.add(rawParam)
            params.add(formatParam)
            implBody = "return fromString(value, v -> new BigDecimal(v));"
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.bigDecimal())
            params.add(formatParam)
            implBody = "return toString(value, v -> v.toString());"
        }
        
        // LocalDate support
        "toLocalDate" asMapper {
            resultType = Java.localDate()
            params.add("value", Java.string())
            implBody = "return fromString(value, LocalDate::parse);"
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.localDate())
            implBody = "return toString(value, DateTimeFormatter.ISO_LOCAL_DATE::format);"
            implDependencies.add(Java.dateTimeFormatter())
        }
        
        // LocalDateTime support
        "toLocalDateTime" asMapper {
            resultType = Java.localDateTime()
            params.add("value", Java.string())
            implBody =
                "return fromString(value, v -> ZonedDateTime.parse(v, DateTimeFormatter.ISO_OFFSET_DATE_TIME).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());"
            implDependencies.add(Java.zonedDateTime(), Java.zonedOffset(), Java.dateTimeFormatter())
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.localDateTime())
            implBody =
                "return toString(value, v -> DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(value.atZone(ZoneOffset.UTC)));"
            implDependencies.add(Java.zonedOffset(), Java.dateTimeFormatter())
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.zonedDateTime())
            implBody = "return toString(value, v -> DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(value));"
            implDependencies.add(Java.dateTimeFormatter())
        }
        
        // Boolean support
        "toBoolean" asMapper {
            resultType = Java.boxedBoolean()
            params.add(rawParam)
            implBody = "return fromString(value, Boolean::valueOf);"
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.boxedBoolean())
            implBody = toStringExp
        }
        
        // Double support
        "toDouble" asMapper {
            resultType = Java.boxedDouble()
            implBody = "return fromString(value, Double::valueOf);"
            params.add(rawParam)
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.boxedDouble())
            implBody = toStringExp
        }
        
        // binary support
        "toBytes" asMapper {
            resultType = Java.byteArray()
            params.add(rawParam)
            implBody = "return value != null ? Base64.getDecoder().decode(value) : null;"
            implDependencies.add("java.util.Base64")
        }
        "toString" asMapper {
            resultType = Java.string()
            params.add("value", Java.byteArray())
            implBody = "return value != null ? Base64.getEncoder().encodeToString(value) : null;"
            implDependencies.add("java.util.Base64")
        }
        "toBytes" asMapper {
            resultType = Java.byteArray()
            params.add("value", "javax.servlet.http.HttpServletRequest")
            annotations.add(Lombok.sneakyThrows())
            implBody = "return value != null ? IOUtils.toByteArray(value.getInputStream()) : null;"
            implDependencies.add("org.apache.commons.io.IOUtils")
        }
        "toBytes" asMapper {
            resultType = Java.byteArray()
            params.add("value", Spring.resource())
            annotations.add(Lombok.sneakyThrows())
            implBody = "return value != null ? IOUtils.toByteArray(value.getInputStream()) : null;"
            implDependencies.add("org.apache.commons.io.IOUtils")
        }
        "toBytes" asMapper {
            resultType = Java.byteArray()
            params.add("file", Spring.multipartFile())
            annotations.add(Lombok.sneakyThrows())
            implBody = "return file != null ? file.getBytes() : null;"
        }
    }

    infix fun String.method(methodBuilder: MethodCm.() -> Unit) {
        methods.add(this) {
            isStatic = true
            methodBuilder.invoke(this)
        }
    }

    infix fun String.asMapper(builder: MethodCm.() -> Unit): MethodCm =
        methods.add(this) {
            builder.invoke(this)
            registerAsMapper()
            isStatic = true
        }
}
