/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

import pl.metaprogramming.codegen.java.builders.EnumValueInterfaceBuilder

class JavaCommonCodesConfigurator : JavaGeneratorBuilder.Delegate() {
    override fun configure() {
        typeOfCode(JavaCommonTypeOfCode.ENUM_VALUE_INTERFACE) {
            className.fixed = "EnumValue"
            builders += EnumValueInterfaceBuilder()
        }
        typeOfCode(JavaCommonTypeOfCode.TEST_DATA) {
            val variant = if (params[JavaParams::class].javaVersion >= 16) "java16" else "java"
            freemarkerTemplate = "/templates/TestData.${variant}.ftl"
            projectSubDir = "src/test/java"
        }
    }
}