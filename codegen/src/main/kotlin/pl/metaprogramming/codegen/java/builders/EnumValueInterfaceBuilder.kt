/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.builders

import pl.metaprogramming.codegen.java.ClassKind
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Java

class EnumValueInterfaceBuilder : ClassCmBuilderTemplate<Nothing>() {
    override fun makeImplementation() {
        val genericType = Java.genericParamT().withSuper(classCm)
        kind = ClassKind.INTERFACE
        methods.add("getValue") { resultType = Java.string() }
        methods.add("equalsValue") {
            resultType = Java.primitiveBoolean()
            params.add("value", Java.string())
            implBody = "return getValue().equals(value);"
        }
        methods.add("fromValue") {
            isStatic = true
            resultType = genericType
            params.add("value", Java.string())
            params.add("enumClass", "java.lang.Class<T>")
            implBody = """
                |if (value == null) return null;
                |for (T e : enumClass.getEnumConstants()) {
                |    if (e.equalsValue(value)) return e;
                |}
                |throw new IllegalArgumentException(String.format("Unknown value '%s' of enum '%s'", value, enumClass.getCanonicalName()));
            """.trimMargin()
        }
    }
}