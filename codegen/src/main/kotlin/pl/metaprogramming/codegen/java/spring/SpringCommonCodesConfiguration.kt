/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.java.JavaGeneratorBuilder
import pl.metaprogramming.codegen.java.libs.Jackson
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Spring

class SpringCommonCodesConfiguration : JavaGeneratorBuilder.Delegate() {
    override fun configure() {
        typeOfCode(SpringCommonTypeOfCode.ENDPOINT_PROVIDER) {
            className.fixed = "EndpointProvider"
            addManagedComponentBuilder()
            onImplementation {
                fields.add("baseUrl", Java.string()) {
                    addAnnotation(Spring.value("\${BASE_URL:http://localhost:8080}"))
                }
                methods.add("getEndpoint") {
                    resultType = Java.string()
                    params.add("path", Java.string())
                    implBody = "return baseUrl + path;"
                }
            }
        }

        typeOfCode(SpringCommonTypeOfCode.URLENCODED_OBJECT_MAPPER) {
            className.fixed = "UrlEncodedObjectMapper"
            addManagedComponentBuilder()
            onImplementation {
                fields.add("objectMapper", Jackson.objectMapper())
                methods.add("map") {
                    resultType = Java.genericParamT()
                    params.add("body", Spring.multiValueMap(Java.string(), Java.string()))
                    params.add("resultClass", Java.claasType(Java.genericParamT()))

                    implBody = """
                        |Map<String, Object> objMap = body.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().size() > 1 ? e.getValue() : e.getValue().get(0)));
                        |return objectMapper.convertValue(objMap, resultClass);
                        """.trimMargin()
                    implDependencies.add(Java.map(), Java.collectors())
                }
            }
        }

    }
}