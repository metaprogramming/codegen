/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.builders

import pl.metaprogramming.codegen.java.ClassKind
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Lombok
import pl.metaprogramming.model.data.EnumType

open class BaseEnumBuilder<M : EnumType?> : ClassCmBuilderTemplate<M>() {

    override fun makeDeclaration() {
        kind = ClassKind.ENUM
        if (model != null) {
            model!!.allowedValues.forEach(::addEnumItem)
            classCm.description = model!!.description
        }
        methods.add(classCm.className) {
            isPrivate = true
            params.add("value", Java.string())
            implBody = "this.value = value;"
        }
    }

    override fun makeImplementation() {
        fields.add("value", Java.string()) {
            addAnnotation(Lombok.getter())
            isFinal = true
        }
    }

    open fun addEnumItem(value: String) =
        classCm.addEnumItem(nameMapper.toConstantName(value))
            .value(value)
            .description(model!!.getDescription(value))

}