/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.java.JavaGenerator
import pl.metaprogramming.codegen.java.JavaGeneratorBuilder
import pl.metaprogramming.codegen.java.builders.InterfaceBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Lombok
import pl.metaprogramming.codegen.java.spring.rs.*
import pl.metaprogramming.codegen.java.test.TestDataParams
import pl.metaprogramming.codegen.java.test.TestDataProviderBuilder
import pl.metaprogramming.codegen.java.validation.*
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.oas.HttpMethod
import pl.metaprogramming.model.oas.HttpRequestSchema
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.RestApi
import pl.metaprogramming.upperFirstChar

class SpringRestServiceGenerator :
    JavaGeneratorBuilder<RestApi, SpringRestServiceGenerator.TOC, SpringRestServiceGenerator>(TOC) {

    object TOC {
        /**
         * Interface for operation executor (Query/Command).
         * Generated when SpringRestParams.delegateToFacade == false.
         */
        @JvmField val OPERATION_EXECUTOR = SpringRsTypeOfCode.OPERATION_EXECUTOR

        /**
         * Operation executor (Query/Command).
         * Generated when SpringRestParams.delegateToFacade == false.
         */
        @JvmField val OPERATION_EXECUTOR_IMPL = SpringRsTypeOfCode.OPERATION_EXECUTOR_IMPL

        /**
         * Facade for REST operations (interface).
         * Generated when SpringRestParams.delegateToFacade == true.
         */
        @JvmField val FACADE = SpringRsTypeOfCode.FACADE

        /**
         * Facade for REST operations (implementation).
         * Generated when SpringRestParams.delegateToFacade == true.
         */
        @JvmField val FACADE_IMPL = SpringRsTypeOfCode.FACADE_IMPL

        @JvmField val REST_CONTROLLER = SpringRsTypeOfCode.REST_CONTROLLER
        @JvmField val REQUEST_DTO = SpringRsTypeOfCode.REQUEST_DTO
        @JvmField val REQUEST_VALIDATOR = ValidationTypeOfCode.REQUEST_VALIDATOR
        @JvmField val SECURITY_CHECKER = ValidationTypeOfCode.SECURITY_CHECKER
        @JvmField val DTO = SpringRsTypeOfCode.DTO
        @JvmField val DTO_VALIDATOR = ValidationTypeOfCode.DTO_VALIDATOR
        @JvmField val ENUM = SpringRsTypeOfCode.ENUM

        @JvmField val TEST_DATA_PROVIDER = SpringRsTypeOfCode.TEST_DATA_PROVIDER
    }

    inner class Generator : JavaGenerator<RestApi>(this) {

        private val operationClassTypes = mutableListOf(TOC.REST_CONTROLLER)
        private val operationRequestClassTypes = mutableListOf(TOC.REQUEST_DTO)
        private val validationParams get() = params[ValidationParams::class]
        private val generateValidator get() = !validationParams.useJakartaBeanValidation
        private val delegateToFacade get() = params[SpringRestParams::class].delegateToFacade

        init {
            if (generateValidator) {
                operationRequestClassTypes.add(TOC.REQUEST_VALIDATOR)
            }
            if (!delegateToFacade) {
                operationClassTypes.apply {
                    add(TOC.OPERATION_EXECUTOR)
                    add(TOC.OPERATION_EXECUTOR_IMPL)
                }
            }
        }

        override fun generate() {
            addSecurityCheckers()
            addSchemaCodes()
            addOperationCodes()
            if (generateValidator) forceGeneration(ValidationCommonTypeOfCode.REST_EXCEPTION_HANDLER)
            makeCodeModels()
        }

        private fun addSecurityCheckers() {
            if (!validationParams.securityValidation || !generateValidator) return
            val securityParams = mutableListOf<String>()
            model.securitySchemas.forEach {
                if (!securityParams.contains(it.paramName)) {
                    securityParams.add(it.paramName)
                    register(TOC.SECURITY_CHECKER, it.paramName)
                }
            }
        }

        private fun addSchemaCodes() {
            model.schemas.forEach { schema ->
                if (schema.isEnum) {
                    register(TOC.ENUM, schema)
                }
                if (schema.isObject) {
                    register(TOC.DTO, schema)
                    register(TOC.TEST_DATA_PROVIDER, schema)
                    if (generateValidator) {
                        register(TOC.DTO_VALIDATOR, schema)
                    }
                }
            }
        }

        private fun addOperationCodes() {
            model.groupedOperations.values.forEach { operations ->
                operations.forEach { operation ->
                    operationClassTypes.forEach { classType ->
                        register(classType, operation)
                    }
                    operation.requestSchemas.forEach { requestSchema ->
                        operationRequestClassTypes.forEach { toc -> register(toc, requestSchema) }
                    }
                    addTestDataProviders(operation)
                }
                if (delegateToFacade) {
                    register(TOC.FACADE_IMPL, operations)
                    register(TOC.FACADE, operations)
                }
            }
        }

        private fun addTestDataProviders(operation: Operation) {
            if (params[TestDataParams::class].isEnabled) {
                operation.requestBody?.contents?.values?.forEach {
                    if (it.isTypeOrItemType(DataTypeCode.OBJECT) && it.objectType.isNamed) {
                        forceGeneration(TOC.TEST_DATA_PROVIDER, it.objectType)
                    }
                }
            }
        }
    }

    override fun make(): pl.metaprogramming.codegen.Generator<RestApi> = Generator()

    var adapterPackageName: String = "rest"

    private val dtoPackageTail get() = "ports.in.${adapterPackageName}.dtos"
    private val processPackageTail get() = "ports.in.${adapterPackageName}"
    private val validatorsPackageTail get() = "adapters.in.${adapterPackageName}.validators"

    override fun init() {
        dataTypeMapper[DataType.DATE_TIME] = "java.time.ZonedDateTime"
        params[ValidationParams::class].throwExceptionIfValidationFailed = true

        val requestAdditionalBeans = params[SpringRestParams::class].injectBeansIntoRequest

        typeOfCode(toc.REQUEST_VALIDATOR) {
            className {
                resolver { it.resolveModelName() }
                suffix = "Validator"
            }
            packageName.infix { _ -> validatorsPackageTail }
            addManagedComponentBuilder()
            builders += RestRequestValidatorBuilder(toc.REQUEST_DTO, toc.DTO, toc.DTO_VALIDATOR, false)
        }
        typeOfCode(toc.DTO_VALIDATOR) {
            className {
                resolver { it.code }
                suffix = "Validator"
            }
            packageName.infix { _ -> validatorsPackageTail }
            addManagedComponentBuilder()
            builders += RestDtoValidatorBuilder(toc.DTO, toc.DTO_VALIDATOR, toc.DTO, false)
        }
        typeOfCode(toc.ENUM) {
            className.resolver { it.code }
            packageName.infix { _ -> dtoPackageTail }
            builders += EnumBuilder()
            registerAsTypeOfCode(toc.DTO)
        }
        typeOfCode(toc.DTO) {
            className {
                resolver { it.code }
                suffix = "Dto"
            }
            packageName.infix { _ -> dtoPackageTail }
            builders += DtoBuilder()
            builders += Lombok.dataBuilder()
        }
        typeOfCode(toc.REQUEST_DTO) {
            className {
                resolver { it.resolveModelName() }
                suffix = "Request"
            }
            packageName.infix { _ -> dtoPackageTail }
            builders += AdditionalFieldsBuilder(requestAdditionalBeans) { it.operation }
            builders += DtoBuilder<HttpRequestSchema> { it.schema }.withJsonAnnotations(false)
            builders += Lombok.dataBuilder()
        }
        typeOfCode(toc.REST_CONTROLLER) {
            forceGeneration = true
            className {
                resolver(Operation::code)
                suffix = "Controller"
            }
            packageName.infix { _ -> "adapters.in.${adapterPackageName}.controllers" }
            addDependencyInjectionBuilder()
            builders += ControllerBuilder()
            builders += AdditionalFieldsBuilder(requestAdditionalBeans) { it }
        }
        typeOfCode(toc.OPERATION_EXECUTOR_IMPL) {
            forceGeneration = true
            className.resolver { operation -> operationExecutorClassName(operation) }
            packageName.tail = "process"
            builders += OperationExecutorBuilder()
        }
        typeOfCode(toc.OPERATION_EXECUTOR) {
            forceGeneration = true
            className {
                resolver { operation -> operationExecutorClassName(operation) }
                prefix = "I"
            }
            packageName.infix { _ -> processPackageTail }
            builders += InterfaceBuilder(toc.OPERATION_EXECUTOR_IMPL)
        }
        typeOfCode(toc.FACADE_IMPL) {
            forceGeneration = true
            className {
                resolver { it.first().group }
                suffix = "FacadeImpl"
            }
            builders += FacadeBuilder()
        }
        typeOfCode(toc.FACADE) {
            forceGeneration = true
            className {
                resolver { it.first().group }
                suffix = "Facade"
            }
            packageName.infix { _ -> processPackageTail }
            builders += InterfaceBuilder(toc.FACADE_IMPL)
        }

        typeOfCode(toc.SECURITY_CHECKER) {
            forceGeneration = true
            className {
                resolver { it }
                prefix = "Security"
                suffix = "Checker"
            }
            packageName.infix { _ -> "adapters.in.${adapterPackageName}.validators.custom" }
            builders += SecurityCheckerBuilder()
            addManagedComponentBuilder()
        }

        typeOfCode(toc.TEST_DATA_PROVIDER) {
            projectSubDir("src/test/java")
            className {
                resolver { it.code }
                suffix = "Data"
            }
            builders += TestDataProviderBuilder()
            builders += Java.privateConstructorBuilder()
        }
    }

    private fun HttpRequestSchema.resolveModelName() =
        operation.code + if (operation.hasManyRequestSchemas) "_${name}" else ""

    private fun operationExecutorClassName(operation: Operation) =
        operation.code.upperFirstChar() + if (HttpMethod.GET == operation.method) "Query" else "Command"
}
