/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.java.MethodCm

class ClassCmMethods(
    private val context: BuildContext<*>,
    private val _methods: MutableList<MethodCm> = mutableListOf()
) : List<MethodCm> by _methods {

    fun add(method: MethodCm): Boolean {
        add(method, null)
        return true
    }

    fun add(methodName: String, setter: MethodCm.Setter): MethodCm =
        MethodCm(methodName, this.context.classCm, setter).apply { add(this) }

    fun add(methodBuilder: MethodCmBuilder<*>) = add(methodBuilder.makeDeclaration(context), methodBuilder)

    operator fun get(signature: String): MethodCm = find { it.simpleSignature() == signature }
        ?: error("No '$signature' method found in the class: '${context.classCm}', which have methods: ${map { it.simpleSignature() }}")

    fun set(signature: String, setter: MethodCm.Setter) = apply {
        this[signature].setup(setter)
    }

    private fun add(method: MethodCm, methodBuilder: MethodCmBuilder<*>?) {
        find { it.matchNameAndParams(method) }?.let {
            error("Can't add method: $method; is in conflict with $it")
        }
        _methods.add(method)
        if (method.mapper) {
            context.registerMapper(methodBuilder ?: FixedMethodCmBuilder(method))
        } else {
            methodBuilder?.make()
            if (context.classCm.used) method.markAsUsed()
        }
    }
}