/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.libs

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.model.oas.HttpMethod

private const val VALUE_ANNOTATION = "org.springframework.beans.factory.annotation.Value"

private val COMPONENT_ANNOTATION = AnnotationCm("org.springframework.stereotype.Component")
private val SERVICE_ANNOTATION = AnnotationCm("org.springframework.stereotype.Service")
private val CONFIGURATION_ANNOTATION = AnnotationCm("org.springframework.context.annotation.Configuration")
private val REST_CONTROLLER_ANNOTATION = AnnotationCm("org.springframework.web.bind.annotation.RestController")
private val BEAN_ANNOTATION = AnnotationCm("org.springframework.context.annotation.Bean")
private val AUTOWIRED_ANNOTATION = AnnotationCm("org.springframework.beans.factory.annotation.Autowired")
private val SCOPE_PROTOTYPE_ANNOTATION = AnnotationCm(
    "org.springframework.context.annotation.Scope",
    "value" to ValueCm.escaped("prototype"),
    "proxyMode" to ValueCm.staticRef("org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS")
)


private val MULTI_VALUE_MAP =
    ClassCd.of("org.springframework.util.MultiValueMap")
private val LINKED_MULTI_VALUE_MAP =
    ClassCd.of("org.springframework.util.LinkedMultiValueMap")
        .withGeneric(Java.genericParamUnknown(), Java.genericParamUnknown())
private val HTTP_METHOD = ClassCd.of("org.springframework.http.HttpMethod")
private val HTTP_HEADERS = ClassCd.of("org.springframework.http.HttpHeaders")
private val REST_TEMPLATE = ClassCd.of("org.springframework.web.client.RestTemplate")
private val REQUEST_ENTITY = ClassCd.of("org.springframework.http.RequestEntity")
private val RESPONSE_ENTITY = ClassCd.of("org.springframework.http.ResponseEntity")
private val MULTIPART_FILE = ClassCd.of("org.springframework.web.multipart.MultipartFile")
private val RESOURCE = ClassCd.of("org.springframework.core.io.Resource")

private val  HTTP_STATUS_CODE_EXCEPTION = ClassCd.of("org.springframework.web.client.HttpStatusCodeException")

private val KNOWN_HEADERS = mapOf(
    "authorization" to "AUTHORIZATION",
    "set-cookie" to "SET_COOKIE",
    "date" to "DATE",
    "etag" to "ETAG",
    "expires" to "EXPIRES",
    "last-modified" to "LAST_MODIFIED",
)

object Spring {

    @JvmStatic
    fun linkedMultiValueMap() = LINKED_MULTI_VALUE_MAP

    @JvmStatic
    fun multiValueMap() = MULTI_VALUE_MAP

    @JvmStatic
    fun multiValueMap(keyType: ClassCd, valueType: ClassCd) = multiValueMap().withGeneric(keyType, valueType)

    @JvmStatic
    fun httpMethod() = HTTP_METHOD

    @JvmStatic
    fun httpMethod(method: HttpMethod) = HTTP_METHOD.staticFieldRef(method.name)

    @JvmStatic
    fun httpHeaders(): ClassCd = HTTP_HEADERS

    @JvmStatic
    fun httpHeaders(name: String) =
        KNOWN_HEADERS[name.lowercase()]?.let { HTTP_HEADERS.staticFieldRef(it) } ?: ValueCm.escaped(name)

    @JvmStatic
    fun restTemplate() = REST_TEMPLATE

    @JvmStatic
    fun requestEntity(bodyType: ClassCd) = REQUEST_ENTITY.withGeneric(bodyType)

    @JvmStatic
    @JvmOverloads
    fun responseEntity(bodyType: ClassCd? = null) =
        if (bodyType == null) RESPONSE_ENTITY else RESPONSE_ENTITY.withGeneric(bodyType)

    @JvmStatic
    fun multipartFile() = MULTIPART_FILE

    @JvmStatic
    fun resource() = RESOURCE

    @JvmStatic
    fun httpStatusCodeException() = HTTP_STATUS_CODE_EXCEPTION

    // ----------------------
    // annotations

    @JvmStatic
    fun component() = COMPONENT_ANNOTATION

    @JvmStatic
    fun service() = SERVICE_ANNOTATION

    @JvmStatic
    fun configuration() = CONFIGURATION_ANNOTATION

    @JvmStatic
    fun bean() = BEAN_ANNOTATION

    @JvmStatic
    fun value(valueParam: String) = AnnotationCm(VALUE_ANNOTATION, ValueCm.escaped(valueParam))

    @JvmStatic
    fun qualifier(valueParam: String) = AnnotationCm(
        "org.springframework.beans.factory.annotation.Qualifier",
        "value" to ValueCm.escaped(valueParam)
    )

    @JvmStatic
    fun restController() = REST_CONTROLLER_ANNOTATION

    @JvmStatic
    fun prototypeScope() = SCOPE_PROTOTYPE_ANNOTATION

    // ----------------------
    // class build strategies

    @JvmStatic
    fun diWithAutowiredBuilder() = object : ClassCmBuilderTemplate<Any?>() {
        override fun makeDecoration() {
            fields.forEach {
                if (!it.isStatic) it.addAnnotation(AUTOWIRED_ANNOTATION)
            }
        }
    }

    @JvmStatic
    fun diWithConstructorBuilder() = object : ClassCmBuilderTemplate<Nothing>() {
        override fun makeDecoration() {
            var hasDiBeans = false
            fields.forEach {
                val isDiBean = !it.isStatic && it.value == null && !it.hasAnnotation(VALUE_ANNOTATION)
                if (isDiBean) {
                    hasDiBeans = true
                    it.isFinal = true
                }
            }
            if (hasDiBeans) addAnnotation(Lombok.requiredArgsConstructor())
        }
    }

}
