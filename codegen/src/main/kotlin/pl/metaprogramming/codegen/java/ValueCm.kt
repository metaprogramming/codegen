/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

/**
 * Represents literals or field/params references
 */
class ValueCm private constructor(
    val value: List<String?>,
    val escaped: Boolean,
    val array: Boolean = false,
    val itemPerLine: Boolean = false,
    val dependencies: Dependencies = Dependencies(),
) {

    companion object {
        @JvmStatic
        @JvmOverloads
        fun value(value: String?, dependencies: Dependencies = Dependencies()) =
            ValueCm(listOf(value), escaped = false, dependencies = dependencies)

        @JvmStatic
        fun escaped(escaped: String?) = ValueCm(listOf(escaped), escaped = true)

        @JvmStatic
        @JvmOverloads
        fun escapedArray(value: List<String>, itemPerLine: Boolean = false) =
            ValueCm(value, array = true, escaped = true, itemPerLine = itemPerLine)

        @JvmStatic
        fun staticRef(value: String): ValueCm {
            val clazz = value.substring(0, value.lastIndexOf('.'))
            return value(value.substring(clazz.lastIndexOf('.') + 1))
                .apply { dependencies.add(clazz) }
        }

        @JvmStatic
        fun newExp(canonicalName: String, diamond: Boolean = false): ValueCm =
            value("new ${canonicalName.substring(canonicalName.lastIndexOf('.') + 1) + if (diamond) "<>" else ""}()")
                .apply { dependencies.add(canonicalName) }
    }

    fun dependsOn(classCd: ClassCd) = apply { dependencies.add(classCd) }

    override fun toString(): String =
        if (array) "{${value.joinToString { toString(it) }}}"
        else toString(value.first())

    fun toString(v: String?): String =
        if (v == null) "null"
        else if (escaped) "\"$v\""
        else v
}