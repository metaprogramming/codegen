/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.SpringCommonTypeOfCode
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.oas.HttpRequestSchema
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.upperFirstChar

class ControllerBuilder : ClassCmBuilderTemplate<Operation>() {
    override fun makeImplementation() {
        addAnnotation(Spring.restController())
        model.requestSchemas.forEach {
            if (it.isFormUrlEncoded) {
                methods.add(ControllerMethodBuilder(it, true))
                if (it.withoutFormUrlEncoded().isNotEmpty()) {
                    methods.add(ControllerMethodBuilder(it))
                }
            } else {
                methods.add(ControllerMethodBuilder(it))
            }
        }
        // force DTO generation for non successful responses
        model.responses.forEach {
            if (it.schema?.isTypeOrItemType(DataTypeCode.OBJECT) == true)
                getClass(SpringRsTypeOfCode.DTO, it.schema?.dataType)
        }
    }
}

private val STATUS_2_METHOD = mapOf(200 to "ok", 204 to "noContent")

private class ControllerMethodBuilder(private val requestSchema: HttpRequestSchema, private val urlEncoded: Boolean = false) :
    BaseMethodCmBuilder<Operation>(requestSchema.operation) {

    private val isJbvEnabled get() = context.params[ValidationParams::class].useJakartaBeanValidation

    override fun makeDeclaration(): MethodCm {
        val methodCm = RestParamsBuilder(context, SpringRsTypeOfCode.DTO).apply {
            useJakartaBeanValidation = isJbvEnabled
            useDataAutoconversion = true
            urlEncoded = this@ControllerMethodBuilder.urlEncoded
        }.makeControllerMethod(model, requestSchema)
        methodCm.resultType = methodCm.resultType.withGeneric(ResponseClassResolver.successClass(context, model, null))
        return methodCm
    }

    override fun makeImplBody(): String {
        val responseClass = ResponseClassResolver.resolve(context, model)
        val additionalFields =
            context.params[SpringRestParams::class].injectBeansIntoRequest.map { it.withModel(model) }
        val request = makeRequestField(methodCm.params + additionalFields)
        if (!isJbvEnabled) {
            codeBuf.addLines("${mapping().from(request).to(ValidationCommonTypeOfCode.VALIDATION_RESULT)};")
        }
        val serviceCall = mapping().to(responseClass).from(request).make()
        if (responseClass.isVoid) {
            codeBuf.addLines("$serviceCall;", "return ${getEmptyResponse()};")
        } else if (responseClass == Spring.responseEntity()) {
            codeBuf.addLines("return ${serviceCall};")
        } else {
            codeBuf.addLines("return ${getResponse(serviceCall)};")
        }
        return codeBuf.take()
    }

    private fun makeRequestField(fields: List<FieldCm>): FieldCm {
        val requestClass = getClass(SpringRsTypeOfCode.REQUEST_DTO, requestSchema) as ClassCm
        dependencies.add(requestClass)
        codeBuf.newLine("${requestClass.className} request = ${requestClass.newExp()}")
        codeBuf.indent(2)
        fields.forEach {
            val toField = requestClass.fields.getByModel(it.model!!)
            codeBuf.newLine(".set${it.name.upperFirstChar()}(${getParamValue(it, toField)})")
        }
        codeBuf.add(";").indent()
        return requestClass.asField("request")
    }


    private fun getParamValue(param: FieldCm, toField: FieldCm): String {
        val schema: DataSchema? = if (param.model is DataSchema) param.model as DataSchema else null
        if (urlEncoded && schema?.isObject == true && param.model !is Parameter) {
            val mapper = injectDependency(getClass(SpringCommonTypeOfCode.URLENCODED_OBJECT_MAPPER))
            dependencies.add(toField.type)
            return "${mapper.name}.map(${param.name}, ${toField.type.className}.class)"
        }
        val paramValue = if (param.type == toField.type) param.name else mapping().from(param).to(toField.type).make()
        return if (schema?.defaultValue == null) paramValue
        else "${ofNullable(paramValue)}.orElse(${mapping().from(schema.defaultValue!!).to(toField.type)})"
    }

    private fun getEmptyResponse(): String {
        val successStatus = model.successResponse?.status ?: 204
        val statusMethod = STATUS_2_METHOD[successStatus]
        val responseBuildExp = if (statusMethod != null) "${statusMethod}()" else "status(${successStatus})"
        return "ResponseEntity.${responseBuildExp}.build()"
    }

    private fun getResponse(serviceCallExp: String): String {
        val successStatus = model.successResponse?.status ?: 204
        val statusMethod = STATUS_2_METHOD[successStatus]
        val responseBuildExp = if (statusMethod != null) "${statusMethod}($serviceCallExp)"
        else "status(${successStatus}).body($serviceCallExp)"
        return "ResponseEntity.${responseBuildExp}"
    }
}