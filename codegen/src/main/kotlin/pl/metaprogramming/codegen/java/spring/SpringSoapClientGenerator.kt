/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.java.JavaGenerator
import pl.metaprogramming.codegen.java.JavaGeneratorBuilder
import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.java.asClassCd
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.builders.InterfaceBuilder
import pl.metaprogramming.codegen.java.jaxb.JaxbTypeOfCode
import pl.metaprogramming.codegen.java.jaxb.XmlDtoBuilder
import pl.metaprogramming.codegen.java.jaxb.XmlEnumBuilder
import pl.metaprogramming.codegen.java.jaxb.XmlPackageInfoBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Lombok
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.soap.SpringSoapTypeOfCode
import pl.metaprogramming.model.data.XmlDataType
import pl.metaprogramming.model.wsdl.WsdlApi
import java.net.URI
import java.util.function.Function

class SpringSoapClientGenerator :
    JavaGeneratorBuilder<WsdlApi, SpringSoapClientGenerator.TOC, SpringSoapClientGenerator>(TOC) {

    object TOC {
        @JvmField val CLIENT = SpringSoapTypeOfCode.CLIENT
        @JvmField val CLIENT_IMPL = SpringSoapTypeOfCode.CLIENT_IMPL
        @JvmField val CLIENT_CONFIGURATION = SpringSoapTypeOfCode.CLIENT_CONFIGURATION
        @JvmField val DTO = JaxbTypeOfCode.DTO
        @JvmField val ENUM = JaxbTypeOfCode.ENUM
    }

    inner class Generator : JavaGenerator<WsdlApi>(this) {
        override fun generate() {
            val schemaBuilders = model.schemas.values.map {
                if (it.isEnum) register(TOC.ENUM, it)
                else register(TOC.DTO, it)
            }
            addOperationCodes()
            makeCodeModels()
            val schemaPackages = mutableSetOf<String?>()
            schemaBuilders.forEach {
                if (it != null && it.classCd.isToGenerate) {
                    schemaPackages.add(it.classCd.packageName)
                }
            }
            val ns2pkg = params[SpringSoapParams::class].namespace2Package.filterValues { schemaPackages.contains(it) }
            addPackageInfo(ns2pkg)
        }

        private fun addPackageInfo(ns2pkg: Map<String, String>) {
            val dtoBaseDir = codeBuilders.getValue(TOC.DTO).baseDir
            val enumBaseDir = codeBuilders.getValue(TOC.ENUM).baseDir
            check(dtoBaseDir == enumBaseDir) {
                "baseDir for DTO and ENUM type of code should be the same"
            }
            packageInfoList = ns2pkg.map {
                val namespace = it.key
                val pkg = it.value
                XmlPackageInfoBuilder.make(
                    dtoBaseDir,
                    namespace,
                    pkg,
                    model.namespaceElementFormDefault.getValue(namespace),
                    params[JavaParams::class].generatedAnnotation
                )
            }
        }

        private fun addOperationCodes() {
            register(TOC.CLIENT_IMPL, model)
            register(TOC.CLIENT, model)
            register(TOC.CLIENT_CONFIGURATION, model)
        }

    }

    override fun make(): pl.metaprogramming.codegen.Generator<WsdlApi> = Generator()

    var adapterPackageName: String = "soap"
    private val clientPackageTail get() = "adapters.out.${adapterPackageName}"

    override fun init() {
        typeOfCode(toc.ENUM) {
            className.resolver { it.code }
            packageName {
                fixed { it.getPackage() }
                tail = "ports.out.${adapterPackageName}.schema"
            }
            builders += XmlEnumBuilder()
        }

        typeOfCode(toc.DTO) {
            className.resolver { it.code }
            packageName {
                fixed { it.getPackage() }
                tail = "ports.out.${adapterPackageName}.schema"
            }
            builders += XmlDtoBuilder()
            builders += Lombok.dataBuilder()
        }

        typeOfCode(toc.CLIENT_IMPL) {
            forceGeneration = true
            className {
                resolver(WsdlApi::name)
                suffix = "ClientImpl"
            }
            packageName.infix { _ -> clientPackageTail }
            onDeclaration {
                superClass = "org.springframework.ws.client.core.support.WebServiceGatewaySupport".asClassCd()
                model.operations.forEach { operation ->
                    val resultTypeClass = getClass(toc.DTO, operation.output.dataType)
                    methods.add(nameMapper.toMethodName(operation.name)) {
                        resultType = resultTypeClass
                        params.add("request", getClass(toc.DTO, operation.input.dataType))
                        implBody =
                            "return (${resultTypeClass.className}) getWebServiceTemplate().marshalSendAndReceive(request);"
                    }
                }
            }
        }

        typeOfCode(toc.CLIENT) {
            forceGeneration = true
            className {
                resolver(WsdlApi::name)
                suffix = "Client"
            }
            packageName.infix { _ -> "ports.out.${adapterPackageName}" }
            builders += InterfaceBuilder(toc.CLIENT_IMPL)
        }

        typeOfCode(toc.CLIENT_CONFIGURATION) {
            forceGeneration = true
            className {
                resolver(WsdlApi::name)
                suffix = "ClientConfiguration"
            }
            packageName.infix { _ -> clientPackageTail }
            addDependencyInjectionBuilder()
            onImplementation {
                addAnnotation(Spring.configuration())
                val clientClass = getClass(SpringSoapTypeOfCode.CLIENT)
                val resultClass = getClass(toc.CLIENT_IMPL).className
                val contextPaths = context.classesToGenerate
                    .filter { it.typeOfCode == toc.DTO }
                    .map { "\"${it.classCd.packageName}\"" }.distinct().joinToString(", ")
                methods.add("create${clientClass.className}") {
                    resultType = clientClass
                    annotations.add(Spring.bean())
                    implDependencies.add("org.springframework.oxm.jaxb.Jaxb2Marshaller")
                    implBody = """
                            |Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
                            |marshaller.setPackagesToScan($contextPaths);
                            |$resultClass client = new $resultClass();
                            |client.setDefaultUri(${makeEndpointUri()});
                            |client.setMarshaller(marshaller);
                            |client.setUnmarshaller(marshaller);
                            |return client;
                        """.trimMargin()
                }
            }
        }
    }

    private fun XmlDataType.getPackage() = params[SpringSoapParams::class].namespace2Package[namespace]

    fun setNamespacePackage(namespace: String, packageName: String) = apply {
        params[SpringSoapParams::class].setNamespacePackage(namespace, packageName)
    }

    fun setUrlProperty(property: String) = apply {
        params[SpringSoapParams::class].urlProperty = Function<WsdlApi, String?> { property }
    }
}

private fun ClassCmBuilderTemplate<WsdlApi>.makeEndpointUri(): String {
    val urlProperty = context.params[SpringSoapParams::class].urlProperty.apply(model)
    if (urlProperty != null) {
        classCm.fields.add("url", Java.string()) {
            addAnnotation(Spring.value("\${${urlProperty}}"))
        }
        return "url"
    }
    return classLocator(SpringCommonTypeOfCode.ENDPOINT_PROVIDER).getOptional()
        .map {
            val endpointProviderField = classCm.injectDependency(it)
            "${endpointProviderField.name}.getEndpoint(\"${URI(model.uri).path}\")"
        }.orElseGet { "\"${model.uri}\"" }
}