/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.test

import pl.metaprogramming.codegen.java.JavaCommonTypeOfCode
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.spring.rs.SpringRsTypeOfCode
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.data.ObjectType
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.math.BigDecimal.valueOf

class TestDataProviderMethodBuilder(
    model: ObjectType,
    private val methodName: String,
    private val requiredOnly: Boolean
) : BaseMethodCmBuilder<ObjectType>(model) {
    override fun makeDeclaration() = newMethodCm(methodName) {
        resultType = getClass(JavaCommonTypeOfCode.TEST_DATA)
        isStatic = true
    }

    override fun makeImplBody(): String {
        dependencies.add(getClass(JavaCommonTypeOfCode.TEST_DATA), "*")
        codeBuf.addLines("return testData()")
            .indentInc(2)
        collectFields(model).forEach {
            if (!requiredOnly || it.required) {
                codeBuf.addLines(".set(\"${it.code}\", ${fieldValue(it)})")
            }
        }
        return codeBuf.addLines(";").take()
    }

    private fun collectFields(type: ObjectType, result: MutableList<DataSchema> = mutableListOf()): List<DataSchema> {
        result.addAll(type.fields)
        type.inherits.forEach { collectFields(it, result) }
        return result
    }

    private fun fieldValue(field: DataSchema, code: String? = null): String {
        val fieldCode = code ?: field.code
        checkNotNull(fieldCode)
        val defaultValue = nvl(valueByCode(fieldCode), field.defaultValue, valueByMinMax(field))
        if (defaultValue != null) return fieldValue(defaultValue, field)
        if (field.isEnum) return field.enumType.allowedValues[0].quote()
        if (field.isObject) return objectValue(field.objectType)
        if (field.isType(DataTypeCode.STRING)) return field.format?.let { "format(\"$it\")" }
            ?: fieldCode.fixLength(field.minLength, field.maxLength).quote()
        val valueByType = valueByType(field.dataType.typeCode)
        if (valueByType != null) return valueByType
        if (field.isArray) return "list(" + fieldValue(field.arrayType.itemsSchema, fieldCode) + ")"
        if (field.isMap) {
            if (field.mapType.valuesSchema.isAnyObject) {
                return "testData().set(\"prop\", \"value\").make()"
            }
            return "map(\"key\", ${fieldValue(field.mapType.valuesSchema, fieldCode)})"
        }
        return "\"XYZ-${field.dataType.typeCode}\""
    }

    private fun nvl(vararg values: String?): String? = values.firstNotNullOfOrNull { it }

    private fun valueByCode(fieldCode: String) = params[TestDataParams::class].valueByCode[fieldCode]
    private fun valueByType(dataTypeCode: DataTypeCode) = params[TestDataParams::class].valueByType[dataTypeCode]

    private fun valueByMinMax(field: DataSchema): String? {
        if (field.minimum == null && field.maximum == null) return null
        if (field.minimum != null && field.exclusiveMinimum != true) return field.minimum
        if (field.maximum != null && field.exclusiveMaximum != true) return field.maximum
        val min = field.minimum.toNumber()
        val max = field.maximum.toNumber()
        val value = if (min != null && max != null) avg(min, max)
        else if (min != null) min.add(ONE) else max!!.minus(ONE)

        return if (listOf(DataType.INT16, DataType.INT32, DataType.INT64).contains(field.dataType))
            value.toInt().toString() else value.toString()
    }


    private fun avg(v1: BigDecimal, v2: BigDecimal) = v1.plus(v2).div(valueOf(2))

    private fun fieldValue(value: String, field: DataSchema): String =
        if (field.dataType.isNumberOrBoolean()) value else value.quote()

    private fun objectValue(objectType: ObjectType): String =
        if (objectType.isNamed) {
            val objectClass = getClass(SpringRsTypeOfCode.TEST_DATA_PROVIDER, objectType)
            dependencies.add(objectClass)
            objectClass.className + ".${methodName}().make()"
        } else {
            "testData().set(\"prop\", \"value\").make()"
        }

    private fun String?.toNumber(): BigDecimal? = this?.let { BigDecimal(it) }
    private fun String.quote() = "\"$this\""
    private fun String.fixLength(minLength: Int?, maxLength: Int?): String =
        if (maxLength != null && length > maxLength) substring(0, maxLength)
        else if (minLength != null && length < minLength) (this + this).fixLength(minLength, maxLength)
        else this

}