/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs

import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.model.data.EnumType
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.oas.HttpRequestSchema
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.RestApi

object SpringRsTypeOfCode {
    @JvmStatic val ENUM = TypeOfCode<EnumType>("RS_ENUM")
    @JvmStatic val DTO = TypeOfCode<ObjectType>("RS_DTO")
    @JvmStatic val REQUEST_DTO = TypeOfCode<HttpRequestSchema>("RS_REQUEST_DTO")

    // for REST services
    @JvmStatic val OPERATION_EXECUTOR = TypeOfCode<Operation>("RS_OPERATION_EXECUTOR")
    @JvmStatic val OPERATION_EXECUTOR_IMPL = TypeOfCode<Operation>("RS_OPERATION_EXECUTOR_IMPL")
    @JvmStatic val FACADE = TypeOfCode<List<Operation>>("RS_FACADE")
    @JvmStatic val FACADE_IMPL = TypeOfCode<List<Operation>>("RS_FACADE_IMPL")
    @JvmStatic val REST_CONTROLLER = TypeOfCode<Operation>("RS_REST_CONTROLLER")

    // for REST clients
    @JvmStatic val CLIENT = TypeOfCode<List<Operation>>("RS_CLIENT")
    @JvmStatic val CLIENT_IMPL = TypeOfCode<List<Operation>>("RS_CLIENT_IMPL")
    @JvmStatic val REST_CLIENT_PROVIDER = TypeOfCode<RestApi>("RS_REST_CLIENT_PROVIDER")

    // for tests
    @JvmStatic val TEST_DATA_PROVIDER = TypeOfCode<ObjectType>("RS_TEST_DATA_PROVIDER")
}