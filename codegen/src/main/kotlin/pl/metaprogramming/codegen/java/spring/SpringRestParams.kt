/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.FieldCm

/**
 * Representation of a set of parameters allowing the parameterization
 * (in selected aspects) of the REST server / client code generator
 * for the spring framework.
 */
class SpringRestParams {
    /**
     * If true, will be generated *Facade classes (one class for a group of operations),
     * otherwise, will be generated *Command / *Query classes (one class for one operation).
     */
    var delegateToFacade: Boolean = false

    fun delegateToFacade(value: Boolean) = apply { delegateToFacade = value }

    /**
     * If true, each REST operation will have its own RestController class,
     * otherwise, all operations from one group will share RestController class.
     */
    var controllerPerOperation: Boolean = true

    /**
     * If true, it will only be possible to create a REST response object using
     * the static factory method available from the REST response class.
     * NOTE Each REST operation has its own REST response class.
     * RS2T generator
     */
    var staticFactoryMethodForRestResponse: Boolean = true

    /**
     * Parameter name of the REST controller method used to inject the request body as a string.
     */
    var payloadField: String? = null

    var injectBeansIntoRequest: MutableList<FieldCm> = mutableListOf()

    var springVersion: Int = 5

    /**
     * Injects bean into request object.
     *
     * @param name field name of the injected bean in the request class
     * @param className class name of the injected bean
     * @return self
     */
    fun injectBeanIntoRequest(name: String, className: String) =
        apply { injectBeansIntoRequest.add(ClassCd.of(className).asField(name)) }

}