/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

class AnnotationCm(val annotationClass: ClassCd, val params: Map<String, ValueCm>) {

    constructor(canonicalName: String, valueParam: ValueCm) : this(
        ClassCd.of(canonicalName),
        mapOf("value" to valueParam)
    )

    constructor(canonicalName: String, vararg params: Pair<String, ValueCm>) : this(
        ClassCd.of(canonicalName),
        mapOf(*params)
    )

    @JvmOverloads
    constructor(canonicalName: String, params: Map<String, ValueCm> = mapOf()) : this(ClassCd.of(canonicalName), params)

    companion object {
        @JvmStatic
        @JvmOverloads
        fun of(canonicalName: String, params: Map<String, ValueCm> = mapOf()) =
            AnnotationCm(canonicalName, params)
    }

    fun collectDependencies(dependencies: Dependencies) {
        annotationClass.collectDependencies(dependencies)
        params.values.forEach { it.dependencies.collectDependencies(dependencies) }
    }

    fun markAsUsed() {
        annotationClass.markAsUsed()
        params.values.forEach { it.dependencies.markAsUsed() }
    }

    override fun toString() = "@$annotationClass" + if (params.isEmpty()) "" else '(' +
            (if (params.size == 1 && params.keys.contains("value")) params.values.first().toString()
            else params.map { "${it.key} = ${it.value}" }.joinToString(", ")) + ')'

}