/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.base

import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.libs.Java

private val FUN_T_R = ClassCd.of("java.util.function.Function<T, R>")
private val LIST_R = ClassCd.of("java.util.List<R>")
private val LIST_T = ClassCd.of("java.util.List<T>")
private val MAP_KR = ClassCd.of("java.util.Map<K, R>")
private val MAP_KT = ClassCd.of("java.util.Map<K, T>")

class MappingExpressionBuilder(
    private val context: BuildContext<*>,
    private val model: Any?,
    private val dependencies: Dependencies
) {
    private lateinit var _to: ClassCd
    private var _from: List<FieldCm>? = null
    private var _fromString: String? = null
    private val classCm: ClassCm = context.classCm

    fun to(to: ClassCd) = apply { this._to = to }
    fun to(typeOfCode: Any) = apply { this._to = getClass(typeOfCode) }
    fun from(from: List<FieldCm>) = apply { this._from = from }
    fun from(from: FieldCm) = from(listOf(from))
    fun from(typeOfCode: Any) = from(getClass(typeOfCode).asField(null))
    fun from(typeOfCode: Any, valueExp: String) = from(getClass(typeOfCode).asExpression(valueExp))
    fun from(from: String) = apply { this._fromString = from }
    fun from(transformationBuilder: MappingExpressionBuilder) =
        apply { _from = listOf(transformationBuilder.makeField()) }

    fun makeField(name: String? = null): FieldCm = _to.asField(name).setValue(make())

    fun makeValue() = ValueCm.value(make(), dependencies)

    fun make(): String = _from?.let {
        val allowMethodReference = !it.hasOne { !isUnnamed || value != null }
        makeTransformation(_to, it, 1, allowMethodReference)
    } ?: makeValueTransformation(_to, _fromString)

    override fun toString(): String =
        if (this::_to.isInitialized) make() else "[to: $_to; from: $_from; fromString: $_fromString]"

    private fun makeTransformation(
        toType: ClassCd,
        from: List<FieldCm>,
        level: Int = 1,
        allowMethodReference: Boolean = false
    ): String = when {
        isCollectionTransformation(toType, from) -> makeCollectionTransformation(toType, from, level)
        toType == Java.string() && from.hasOne { type.isEnum } -> makeEnumToStringTransformation(from[0])
        listOf(toType) == from.map { it.type } -> makeTransformationParams(from)
        else -> {
            val mapper = context.findMapper(toType, from.map { it.type }, from.isNotEmpty())
            when {
                mapper == null -> {
                    dependencies.add(toType)
                    "new ${toType.className}()"
                }

                mapper.params.isEmpty() && from.hasOne { type == mapper.ownerClass } ->
                    "${from[0].name}.${mapper.name}()"

                mapper.isStatic -> {
                    dependencies.add(mapper.owner)
                    makeCall(mapper.owner.className, mapper.name, from, allowMethodReference)
                }

                else -> {
                    val mapperField = classCm.injectDependency(mapper.owner)
                    makeCall(mapperField.name, mapper.name, from, allowMethodReference)
                }
            }
        }
    }

    private fun makeEnumToStringTransformation(from: FieldCm) = if (from.isNonnull) {
        "${from.name.toGetter()}.getValue()"
    } else {
        dependencies.add("java.util.Optional")
        dependencies.add(from.type)
        "Optional.ofNullable(${from.name.toGetter()}).map(${from.type.className}::getValue).orElse(null)"
    }

    private fun isCollectionTransformation(toType: ClassCd, from: List<FieldCm>) =
        (toType.isList && from[0].type.isList) || (toType.isMap && from[0].type.isMap)

    private fun makeCollectionTransformation(toType: ClassCd, from: List<FieldCm>, level: Int): String {
        val isList = toType.isList
        // e.g. baseDataMapper.transformList(raw.getAuthors(), v -> baseDataMapper.toLong(v))
        val toItemType = toType.genericParams[if (isList) 0 else 1]
        val fromItemType = from[0].type.genericParams[if (isList) 0 else 1]
        val varName = "v" + if (level > 1) level else ""
        val itemFrom = listOf(fromItemType.asField(varName)) + from.subList(1, from.size)
        val itemTransformation = makeTransformation(toItemType, itemFrom, level + 1, true)

        val mapper =
            if (isList) context.findMapper(LIST_R, listOf(LIST_T, FUN_T_R))
            else context.findMapper(MAP_KR, listOf(MAP_KT, FUN_T_R))
        val itemTransformationValue =
            if (varName != itemTransformation && !itemTransformation.contains('.')) itemTransformation else "$varName -> $itemTransformation"
        val methodCall = "${mapper.name}(${makeTransformationParams(listOf(from[0]))}, $itemTransformationValue)"

        return if (mapper.isStatic) {
            dependencies.add(mapper.owner)
            "${mapper.owner.className}.$methodCall"
        } else {
            val mapperField = classCm.injectDependency(mapper.owner)
            "${mapperField.name}.$methodCall"
        }
    }

    private fun makeValueTransformation(type: ClassCd, value: String?) = when {
        value == null -> "null"
        setOf("java.lang.Integer", "java.lang.Short").contains(type.canonicalName) -> value
        type == Java.string() -> "\"$value\""
        type == Java.longBoxed() -> value + 'l'
        type == Java.boxedFloat() -> value + 'f'
        type == Java.boxedDouble() -> value + (if (value.contains('.')) "" else "d")
        type == Java.bigDecimal() -> "new BigDecimal(\"${value}\")"
        type.canonicalName == "java.time.ZonedDateTime" -> "ZonedDateTime.parse(\"${value}\")"
        type is ClassCm && type.isEnum -> "${type.className}.${context.nameMapper.toConstantName(value)}"
        else -> makeTransformation(type, listOf(FieldCm.stringValue(value)))
    }

    private fun makeTransformationParams(from: List<FieldCm>) =
        from.joinToString(", ") { if (it.isUnnamed) it.value!!.toString() else it.name.toGetter() }

    private fun makeCall(instance: String, method: String, from: List<FieldCm>, allowMethodReference: Boolean) =
        if (allowMethodReference && from.size == 1) {
            "${instance}::${method}"
        } else {
            val call = "${method}(${makeTransformationParams(from)})"
            if (instance == "this") call else "${instance}.${call}"
        }

    private fun getClass(typeOfCode: Any) = context.classLocator(typeOfCode).model(model).get()

    private fun <T> List<T>.hasOne(predicate: T.() -> Boolean): Boolean = size == 1 && predicate(get(0))
}