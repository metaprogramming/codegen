/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java

class Dependencies(private val children: MutableSet<Dependencies> = mutableSetOf()) {
    private val _values = mutableSetOf<Dependency>()

    private var _used = false

    val values: Set<Dependency> = _values

    fun add(classCd: ClassCd, staticMember: String) = apply {
        if (_used) classCd.markAsUsed()
        _values.add(Dependency(classCd, staticMember))
    }

    fun add(vararg classes: ClassCd) = apply {
        classes.forEach { classCd ->
            if (_used) classCd.markAsUsed()
            _values.add(Dependency(classCd))
            classCd.genericParams.forEach { _values.add(Dependency(it)) }
        }
    }


    fun add(vararg classes: String) = apply {
        classes.forEach { canonicalName ->
            if (canonicalName.endsWith(".*"))
                _values.add(Dependency(packageName = canonicalName.substring(0, canonicalName.length - 2)))
            else add(ClassCd.of(canonicalName))
        }
    }

    fun add(child: Dependencies) = apply {
        if (_used) child.markAsUsed()
        children.add(child)
    }

    fun remove(child: Dependencies) = children.remove(child)

    fun markAsUsed() {
        if (!_used) {
            _used = true
            _values.forEach { it.classCd?.markAsUsed() }
            children.forEach { it.markAsUsed() }
        }
    }

    fun collectDependencies(dependencies: Dependencies) {
        dependencies._values.addAll(_values)
    }

    fun contains(classCd: ClassCd): Boolean {
        try {
            return _values.any {
                it.classCd == classCd || it.packageName == classCd.packageName
            } || children.any { it.contains(classCd) }
        } catch (e: Exception) {
            println("children: $children")
            throw e
        }
    }

    fun asImports(excludePackages: Collection<String>): Collection<String> {
        val allExcludedPackages = excludePackages.toMutableSet()
        _values.forEach { if (it.packageName != null) allExcludedPackages.add(it.packageName) }
        return _values
            .filter {
                it.staticMember != null || it.packageName != null || (it.classCd != null
                        && !it.classCd.isGenericParam
                        && !it.classCd.packageName.isNullOrEmpty()
                        && !allExcludedPackages.contains(it.classCd.packageName))
            }
            .map {
                when {
                    it.packageName != null -> it.packageName + ".*"
                    it.classCd != null ->
                        if (it.staticMember != null)
                            "static ${it.classCd.canonicalName}.${it.staticMember}"
                        else if (it.classCd.className.contains("."))
                            it.classCd.packageName + "." + it.classCd.className.substringBefore(".")
                        else it.classCd.canonicalName

                    else -> error("Invalid dependency definition $it")
                }
            }
    }
}