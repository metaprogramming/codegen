/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import pl.metaprogramming.codegen.internal.CodegenConfig
import pl.metaprogramming.codegen.internal.MainGenerator
import pl.metaprogramming.codegen.java.CodeIndex
import pl.metaprogramming.codegen.java.DataTypeMapper
import pl.metaprogramming.model.oas.RestApi
import pl.metaprogramming.model.wsdl.WsdlApi
import pl.metaprogramming.model.wsdl.parser.WsdlParserConfig
import java.io.File
import java.nio.charset.Charset
import java.util.function.Consumer

/**
 * Entrypoint for using Codegen.
 *
 * Here you add modules and run code generation for them.
 */
open class Codegen {
    private val cfg = CodegenConfig()

    init {
        NEW_LINE = System.lineSeparator()
    }

    /** The base/root directory for the generated codes. */
    var baseDir by cfg::baseDir

    /** The location for generated codes index file (if null then: ${baseDir}/generatedFiles.yaml). */
    var indexFile by cfg::indexFile

    /**
     * Determines the behavior of the generator when a manual change of generated codes is detected.
     *
     * Note that manually modified codes should have "@Generated" annotation removed.
     * - true - generating will overwrite manual changes (default)
     * - false - generation will fail
     */
    var forceMode by cfg::forceMode

    /** Specifies whether the index of the generated codes should contain the 'lastGeneration' tag with the last generation execution timestamp. */
    var addLastGenerationTag by cfg::addLastGenerationTag

    /** Specifies whether the entries in the index of generated codes should contain the tag "lastUpdate" with the timestamp of the last change of the generated code. */
    var addLastUpdateTag by cfg::addLastUpdateTag

    /** The charset for generated codes. */
    var charset by cfg::charset

    /** The file will be overwritten even if the newly generated code differs from the old one only in whitespaces. */
    var overrideWhenDiffsInWhitespacesOnly by cfg::overrideWhenDiffsInWhitespacesOnly

    /** The line separator for generated codes. */
    var lineSeparator by ::NEW_LINE

    val params = CodegenParams()

    var dataTypeMapper = DataTypeMapper()

    val codeIndex = CodeIndex()

    /**
     * Runs code generation.
     */
    fun run() {
        MainGenerator(cfg).runAll()
    }

    /**
     * Deletes generated files present in the index file.
     * Files with removed 'Generated' annotation will not be deleted.
     */
    fun cleanup() {
        MainGenerator(cfg).cleanup(false)
    }

    /**
     * Deletes generated files present in the index file.
     * Also, will be deleted files with removed 'Generated' annotation.
     */
    fun totalCleanup() {
        MainGenerator(cfg).cleanup(true)
    }

    /**
     * Adds module generator to the code generation schedule.
     * With builder version.
     */
    fun <M : Model, T : Any, B : GeneratorBuilder<M, T, B>> generate(
        generatorBuilder: B,
        setter: GeneratorBuilder.Setter<B>
    ) =
        apply {
            generatorBuilder.dataTypeMapper = dataTypeMapper.copy()
            generatorBuilder.params = params.copy()
            generatorBuilder.codeIndex = codeIndex
            generatorBuilder.init()
            generate(generatorBuilder.make(setter))
        }

    /**
     * Adds module generator to the code generation schedule.
     */
    fun generate(generator: Generator<*>) = apply { cfg.modules.add(generator) }

    fun restApi(content: String, vararg dependencies: RestApi) = RestApi.of(content, *dependencies)

    fun wsdlApi(wsdlLocation: String, configurator: Consumer<WsdlParserConfig> = Consumer {}) =
        WsdlApi.of(wsdlLocation, configurator)

    // for java api

    fun baseDir(baseDir: File) = apply { this.baseDir = baseDir }
    fun indexFile(indexFile: File?) = apply { this.indexFile = indexFile }
    fun forceMode(forceMode: Boolean) = apply { this.forceMode = forceMode }
    fun addLastGenerationTag(addLastGenerationTag: Boolean) = apply { this.addLastGenerationTag = addLastGenerationTag }
    fun addLastUpdateTag(addLastUpdateTag: Boolean) = apply { this.addLastUpdateTag = addLastUpdateTag }
    fun charset(charsetName: String) = apply { this.charset = Charset.forName(charsetName) }
    fun overrideWhenDiffsInWhitespacesOnly(overrideWhenDiffsInWhitespacesOnly: Boolean) =
        apply { this.overrideWhenDiffsInWhitespacesOnly = overrideWhenDiffsInWhitespacesOnly }

    fun lineSeparator(lineSeparator: String) = apply { this.lineSeparator = lineSeparator }
    fun dataTypeMapper(setter: Consumer<DataTypeMapper>) = apply { setter.accept(dataTypeMapper) }
    fun params(setter: Consumer<CodegenParams>) = apply { setter.accept(params) }
}

