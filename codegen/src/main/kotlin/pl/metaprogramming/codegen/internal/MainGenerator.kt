/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.internal

import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.Yaml
import pl.metaprogramming.codegen.CodeGenerationTask
import pl.metaprogramming.codegen.NEW_LINE
import pl.metaprogramming.log
import java.io.File
import java.nio.file.Files
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit

internal class MainGenerator(private val cfg: CodegenConfig) {

    private val indexFile: File = cfg.indexFile ?: File(cfg.baseDir, "generatedFiles.yaml")
    private var fileIndex: MutableMap<String, CodeFile> = mutableMapOf()
    private var prevGenerationDate: String? = null
    private val dateTimeFormatter = DateTimeFormatter.ofPattern(cfg.dataTimeFormat)
    private var generationDate: String = LocalDateTime.now().format(dateTimeFormatter)

    // support for changing support for changing the case of the filename
    private var dirFiles: MutableMap<File, Set<String>> = mutableMapOf()
    private var mngGitRepo: Boolean? = null

    fun runAll() {
        loadIndex(FileStatus.ABANDONED)
        cfg.modules.forEach { it.generate() }
        cfg.modules.forEach { it.codesToGenerate.forEach { task -> updateIndex(task) } }
        removeFiles("Delete abandoned files") { it.status == FileStatus.ABANDONED }
        writeFiles()
        saveIndex()
    }

    fun cleanup(total: Boolean) {
        mngGitRepo = false
        loadIndex(FileStatus.UNCHANGED)
        val removed =
            removeFiles("Delete generated files [${if (total) "include" else "skip"} manually modified codes]") {
                total || it.status != FileStatus.DETACHED
            }
        fileIndex.entries.retainAll { !removed.contains(it.value) }
        Files.walk(cfg.baseDir.toPath())
            .sorted(Comparator.reverseOrder())
            .map { it.toFile() }
            .filter { it.isDirectory }
            .forEach(File::delete)
        saveIndex()
    }

    private fun saveIndex() {
        val yaml = Yaml(DumperOptions().apply {
            defaultFlowStyle = DumperOptions.FlowStyle.BLOCK
            lineBreak = DumperOptions.LineBreak.entries.find { NEW_LINE == it.string }
                ?: DumperOptions.LineBreak.getPlatformLineBreak()
        }
        )
        val out = mutableMapOf<String, Any>()
        if (cfg.addLastGenerationTag) {
            out["lastGeneration"] = generationDate
        }
        out["files"] = fileIndex.values
            .filter { it.status != FileStatus.ABANDONED }
            .sortedBy { it.path }
            .map {
                val entry = mutableMapOf("path" to it.path)
                if (it.status == FileStatus.DETACHED || cfg.storeAnyKindOfStatusesInIndexFile) {
                    entry["status"] = it.status.name
                }
                if (!cfg.forceMode && it.status != FileStatus.DETACHED) {
                    entry["md5"] = it.task?.md5!!
                }
                if (cfg.addLastUpdateTag) {
                    entry["lastUpdate"] =
                        if (setOf(FileStatus.CREATED, FileStatus.UPDATED).contains(it.status)) generationDate
                        else it.lastUpdate ?: "unknown"
                }
                entry
            }
        indexFile.overwrite(yaml.dump(out))
    }

    private fun loadIndex(defaultStatus: FileStatus) {
        if (!indexFile.exists()) {
            log.warn("Index file (${indexFile.path}) does not exists")
            return
        }
        val index: IndexData = Yaml().loadAs(indexFile.readText(), IndexData::class.java)
        prevGenerationDate = index.lastGeneration
        index.files.forEach { info ->
            val state = info.path.getFileState()
            val oldMd5 = if (cfg.forceMode) state.md5 else info.md5
            fileIndex[info.path] = CodeFile(
                path = info.path,
                lastUpdate = info.lastUpdate,
                state = state,
                status = if (state.isDetached) FileStatus.DETACHED else if (state.md5 != oldMd5) FileStatus.MALFORMED else defaultStatus
            )
        }
    }

    private fun updateIndex(task: CodeGenerationTask<*>) {
        task.md5 = HashBuilder.build(task.content, !cfg.overrideWhenDiffsInWhitespacesOnly)
        val fileMetadata = fileIndex[task.destFilePath]
        if (fileMetadata == null) {
            addToIndex(task)
        } else {
            checkFileStatus(fileMetadata)
            fileMetadata.task = task
            fileMetadata.status = if (fileMetadata.state.isDetached) FileStatus.DETACHED
            else if (fileMetadata.state.md5 == task.md5) FileStatus.UNCHANGED
            else FileStatus.UPDATED
            log(task.destFilePath, fileMetadata.status)
        }
    }

    private fun addToIndex(task: CodeGenerationTask<*>) {
        val state = task.destFilePath.getFileState()
        if (state.exists) {
            if (state.isDetached) {
                if (indexFile.exists()) {
                    log.info("$task.destFilePath already exists, and does not have Generated annotaion")
                }
                addToIndex(task, FileStatus.DETACHED)
            } else if (state.md5 == task.md5) {
                if (indexFile.exists()) {
                    log.info("$task.destFilePath already exists, and has the same content like generated one")
                }
                addToIndex(task, FileStatus.UNCHANGED)
            } else {
                log.warn("$task.destFilePath already exists, and has other content than generated one.")
                addToIndex(task, FileStatus.UPDATED)
            }
        } else {
            addToIndex(task, FileStatus.CREATED)
        }
    }

    private fun addToIndex(task: CodeGenerationTask<*>, status: FileStatus) {
        log(task.destFilePath, status)
        fileIndex[task.destFilePath] = CodeFile(
            path = task.destFilePath,
            status = status,
            state = FileState(),
            task = task
        )
    }

    private fun checkFileStatus(fileMetadata: CodeFile) {
        if (!fileMetadata.state.exists) {
            if (cfg.forceMode) {
                log.warn("${fileMetadata.path} was manually removed and will be restored.")
            } else {
                throw RuntimeException("The file ${fileMetadata.path} has been manually deleted. Set forceMode to proceed.")
            }
        }
        if (fileMetadata.status == FileStatus.MALFORMED) {
            if (cfg.forceMode) {
                log.warn("${fileMetadata.path} has been manually modified and will be overwritten.")
            } else {
                throw RuntimeException("The file ${fileMetadata.path} has been manually modified. Set forceMode to proceed.")
            }
        }
    }

    private fun writeFiles() {
        val writeStatuses = EnumSet.of(FileStatus.CREATED, FileStatus.UPDATED)
        fileIndex.values.forEach {
            if (writeStatuses.contains(it.status)) {
                val task = requireNotNull(it.task)
                task.destFilePath.toFile().overwrite(task.content)
            }
        }

    }

    private fun removeFiles(label: String, filter: (CodeFile) -> Boolean): Collection<CodeFile> {
        val filtered = fileIndex.values.filter(filter)
        if (filtered.isNotEmpty()) {
            log.debug(label)
        }
        filtered.forEach {
            log(it.path, it.status)
            val file = it.path.toFile()
            if (file.exists()) {
                if (!file.delete()) {
                    throw RuntimeException("Can't remove file $it")
                }
                removeInGit(file)
            }
        }
        return filtered
    }

    private fun removeInGit(file: File) {
        if (mngGitRepo == null) {
            mngGitRepo = "git status".runCommand(file.parentFile) == 0
        }
        if (mngGitRepo == true) {
            "git rm ${file.name}".runCommand(file.parentFile)
        }
    }

    private fun String.runCommand(workingDir: File): Int {
        val processBuilder = ProcessBuilder("\\s".toRegex().split(this)).directory(workingDir)
        var process: Process? = null
        try {
            process = processBuilder.start()
            return process!!.also { it.waitFor(500, TimeUnit.MILLISECONDS) }.exitValue()
        } catch (e: Exception) {
            log.warn("Fail executing command: $this", e)
            process?.destroyForcibly()
            return -1
        }
    }

    private fun String.getFileState(): FileState {
        val file = this.toFile()
        val isJavaOrGroovy = file.name.endsWith(".java") || file.name.endsWith(".groovy")
        val hashBuilder = HashBuilder(!cfg.overrideWhenDiffsInWhitespacesOnly)
        val result = FileState()
        result.exists = file.isExists()
        result.shouldBeAnnotated = isJavaOrGroovy
        if (result.exists) {
            file.forEachLine {
                if (isJavaOrGroovy && !result.annotatedByGenerated) {
                    val trimmed = it.trim()
                    result.annotatedByGenerated = trimmed.startsWith("@Generated")
                            || trimmed.startsWith("@javax.annotation.processing.Generated")
                            || trimmed.startsWith("@javax.annotation.Generated")
                }
                hashBuilder.addLine(it)
            }
        }
        result.md5 = hashBuilder.hash
        return result
    }

    private fun String.toFile() = File(cfg.baseDir, this)

    // check file exists with case-sensitivity in case-insensitive file-system (windows)
    private fun File.isExists() =
        exists() && dirFiles.computeIfAbsent(parentFile) { it.list()?.toMutableSet().orEmpty() }
            .contains(name)

    private fun File.overwrite(content: String) {
        @Suppress("kotlin:S899") delete()
        checkNotNull(parentFile) { "'$path' has not parent directory" }
        try {
            parentFile.mkdirs()
            createNewFile()
            writeText(content, cfg.charset)
        } catch (e: Exception) {
            error("Can't write to file $absolutePath")
        }
    }

    private fun log(e1: Any?, e2: Any?) = log.debug("{} ... {}", e1, e2)

}