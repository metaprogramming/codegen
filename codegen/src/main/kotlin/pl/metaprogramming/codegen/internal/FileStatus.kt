/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.internal

internal enum class FileStatus {
    CREATED,    // plik został utworzony przez generator
    UPDATED,    // plik został nadpisany przez generator
    UNCHANGED,  // plik nie uległ zmianie w wyniku uruchomienia generatora
    DETACHED,   // plik nie będzie nadpisywany przez generator (w java usunięcie adnotacji @Generated)
    ABANDONED,  // plik był, ale już nie jest generowany - do usunięcia
    MALFORMED   // suma kontrolna istniejącego pliku nie zgadza się z sumą kontrolną zapisaną w indeksie, przy czym plik nie został oznaczony jako DETACHED
}