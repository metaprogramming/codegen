/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.internal

import java.math.BigInteger
import java.security.MessageDigest

internal class HashBuilder(private val ignoreWhitespaces: Boolean) {

    private val digest = MessageDigest.getInstance("MD5")

    val hash: String
        get() = String.format("%032x", BigInteger(1, digest.digest()))

    fun addLine(line: String) = apply {
        if (ignoreWhitespaces) {
            val trimmed = line.trim()
            if (trimmed.isNotEmpty()) {
                digest.update(trimmed.toByteArray())
            }
        } else {
            digest.update(line.toByteArray())
        }
    }

    fun addLines(lines: String) = apply { lines.lines().forEach(this::addLine) }

    companion object {
        @JvmStatic
        fun build(lines: String, ignoreWhitespaces: Boolean) = HashBuilder(ignoreWhitespaces).addLines(lines).hash
    }
}