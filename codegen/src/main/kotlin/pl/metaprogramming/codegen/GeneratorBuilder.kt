/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen

import pl.metaprogramming.codegen.java.CodeIndex
import pl.metaprogramming.codegen.java.DataTypeMapper

/**
 * @property toc an object with the code types supported by the generator listed as properties
 */
abstract class GeneratorBuilder<M : Model, T : Any, S : GeneratorBuilder<M, T, S>>(@JvmField val toc: T) {

    lateinit var model: M
    lateinit var params: CodegenParams
    lateinit var dataTypeMapper: DataTypeMapper
    lateinit var codeIndex: CodeIndex

    fun make(setter: Setter<S>): Generator<M> {
        @Suppress("UNCHECKED_CAST")
        setter.setup(this as S)
        return make()
    }

    protected fun self(): S {
        @Suppress("UNCHECKED_CAST") return this as S
    }

    fun model(model: M): S {
        this.model = model
        return self()
    }

    abstract fun init()
    abstract fun make(): Generator<M>

    fun interface Setter<G : GeneratorBuilder<*, *, G>> {
        fun G.setup()
    }

    private fun Setter<S>.setup(configurator: S) {
        configurator.setup()
    }

    abstract class Simple<M: Model, S : Simple<M, S>> : GeneratorBuilder<M, Any, S>(Unit) {
        override fun init() {
            // unnecessary for simple generators (without type of codes)
        }
    }
}