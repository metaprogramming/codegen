/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl.parser

import org.apache.cxf.BusFactory
import org.apache.cxf.service.model.ServiceInfo
import org.apache.cxf.tools.wsdlto.core.WSDLDefinitionBuilder
import org.apache.cxf.wsdl11.WSDLServiceBuilder
import org.apache.ws.commons.schema.*
import pl.metaprogramming.model.data.*
import pl.metaprogramming.model.wsdl.WsdlApi
import pl.metaprogramming.model.wsdl.WsdlOperation
import javax.wsdl.Definition
import javax.wsdl.Part
import javax.xml.namespace.QName

class WsdlParser(private val config: WsdlParserConfig) {

    private lateinit var api: WsdlApi
    private lateinit var definition: Definition
    private lateinit var serviceInfo: ServiceInfo
    private val allSchemas: MutableMap<String, DataSchema> = mutableMapOf()

    fun parse(wsdlLocation: String): WsdlApi {
        val bus = BusFactory.getDefaultBus()
        definition = WSDLDefinitionBuilder(bus).build(wsdlLocation)
        check(definition.services.size == 1) { "WSDL should contains definition for exactly one service" }
        serviceInfo = WSDLServiceBuilder(bus).buildServices(definition)[0]

        val name = config.serviceNameMapper.invoke(serviceInfo.name.localPart)
        val uri = serviceInfo.endpoints.first().address
        api = WsdlApi(name, uri)

        parseDataSchemas()
        parseOperations()
        return api
    }

    private fun parseOperations() {
        serviceInfo.`interface`.operations.forEach {
            val name = it.name.localPart
            val input = it.input.name
            val output = it.output.name
            api.operations.add(WsdlOperation(name, getRootDataSchema(input), getRootDataSchema(output)))
        }
    }

    private fun parseDataSchemas() {
        serviceInfo.xmlSchemaCollection.xmlSchemas
            .filterNot { it.targetNamespace == "http://www.w3.org/2001/XMLSchema" }
            .forEach { schema ->
                if (schema.targetNamespace != null) {
                    api.namespaceElementFormDefault[schema.targetNamespace] = schema.elementFormDefault.toString()
                }
                schema.items
                    .filterNot { it is XmlSchemaImport }
                    .forEach {
                        when (it) {
                            is XmlSchemaComplexType -> collect(it)
                            is XmlSchemaElement -> collect(it)
                            is XmlSchemaSimpleType -> collect(it)
                            else -> {
                                TODO("handle element: $it")
                            }
                        }
                    }
            }
        resolveUnknownDataTypes(allSchemas.values)
    }

    private fun resolveUnknownDataTypes(dataSchemas: Collection<DataSchema>) {
        dataSchemas.forEach(::resolveUnknownDataType)
    }


    private fun resolveUnknownDataType(dataSchema: DataSchema) {
        if (dataSchema.dataType is UnknownDataType) {
            dataSchema.dataType = findDataType((dataSchema.dataType as UnknownDataType).qname, true)
        }
        if (dataSchema.isObject) {
            resolveUnknownDataTypes(dataSchema.objectType.fields)
            resolveObjectTypeRefs(dataSchema.objectType.inherits)
        }
        if (dataSchema.isArray) {
            resolveUnknownDataType(dataSchema.arrayType.itemsSchema)
        }
    }

    private fun resolveObjectTypeRefs(list: MutableList<ObjectType>) {
        list.replaceAll {
            if (it is ObjectTypeRef) {
                findDataType(it.qname, true).objectType
            } else {
                it
            }
        }
    }

    fun collect(type: XmlSchemaComplexType) = collect(type, type.qName)

    fun collect(type: XmlSchemaComplexType, name: QName) {
        when {
            type.particle is XmlSchemaSequence -> register(name, type.particle as XmlSchemaSequence)
            type.contentModel is XmlSchemaComplexContent -> {
                val content = (type.contentModel as XmlSchemaComplexContent).content
                if (content is XmlSchemaComplexContentExtension && content.particle is XmlSchemaSequence) {
                    register(name, content.particle as XmlSchemaSequence)
                        .inherits.add(ObjectTypeRef(content.baseTypeName))
                } else {
                    TODO("handle XmlSchemaComplexContent: $content")
                }
            }

            type.particle != null || type.contentModel != null ->
                TODO("handle XmlSchemaComplexType: $name")

            else -> register(name, XmlObjectType(name.localPart, name.namespaceURI))
        }
    }

    fun collect(element: XmlSchemaElement) {
        if (element.schemaType is XmlSchemaComplexType) {
            if (element.schemaTypeName != null) {
                addSchema(element.qName, toDataType(element.schemaTypeName))
            } else {
                collect(element.schemaType as XmlSchemaComplexType, element.qName)
            }
        } else {
            TODO("handle XmlSchemaElement: ${element.qName}")
        }
    }

    fun collect(type: XmlSchemaSimpleType) {
        val dataType = config.simpleTypeParsers.firstNotNullOfOrNull { it.parse(type) }
        check(dataType != null) { "Can't handle $type" }
        if (dataType.isEnum) {
            register(type.qName, dataType)
        } else {
            addSchema(type.qName, dataType)
        }
    }

    private fun register(name: QName, sequence: XmlSchemaSequence): XmlObjectType {
        val dataType = XmlObjectType(name.localPart, name.namespaceURI, sequence.toFields())
        register(name, dataType)
        return dataType
    }

    private fun register(name: QName, dataType: DataType) {
        api.schemas[name.toString()] = addSchema(name, dataType)
    }

    private fun addSchema(name: QName, dataType: DataType): DataSchema {
        val dataSchema = dataType.asSchema(name.localPart) { namespace = name.namespaceURI }
        allSchemas[name.toString()] = dataSchema
        return dataSchema
    }

    private fun toDataType(qName: QName, required: Boolean = false): DataType =
        config.xsTypeParser.parse(qName) ?: findDataType(qName, required)


    private fun findDataType(qname: QName, required: Boolean = false): DataType =
        allSchemas[qname.toString()]?.dataType ?: if (required)
            error("Unknown data type: $qname")
        else UnknownDataType(qname)


    private fun XmlSchemaSequence.toFields(): MutableList<DataSchema> = items.map { item ->
        when (item) {
            is XmlSchemaElement -> {
                val dataType = toDataType(item.schemaTypeName)
                if (item.maxOccurs > 1)
                    dataType.asArray {
                        minItems = item.minOccurs.toInt()
                        maxItems = item.maxOccurs.toInt()
                    }.asSchema(item.name) {
                        nillable = item.isNillable
                    }
                else dataType.asSchema(item.name) {
                    namespace = item.schemaTypeName.namespaceURI
                    required = item.minOccurs > 0L
                    nillable = item.isNillable
                }
            }

            else -> TODO("Handle $item")
        }
    }.toMutableList()

    private fun getRootDataSchema(name: QName): DataSchema {
        val msg = definition.getMessage(name)
        check(msg.parts.size == 1) { "[$name] message should has only one part" }
        val dataTypeName = (msg.parts.values.first() as Part).elementName
        return toDataType(dataTypeName, true).asSchema(dataTypeName.localPart) {
            if (dataType is XmlObjectType) {
                (dataType as XmlObjectType).namespace = dataTypeName.namespaceURI
                (dataType as XmlObjectType).isRootElement = true
            }
        }
    }
}

private class ObjectTypeRef(val qname: QName) : ObjectType()

private class UnknownDataType(val qname: QName) : DataType(DataTypeCode.OBJECT)