/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl

import pl.metaprogramming.codegen.Model
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.wsdl.parser.WsdlParser
import pl.metaprogramming.model.wsdl.parser.WsdlParserConfig
import java.util.function.Consumer
import java.util.function.Predicate

class WsdlApi(override var name: String, var uri: String) : Model {
    val schemas: MutableMap<String, DataSchema> = mutableMapOf()
    val namespaceElementFormDefault: MutableMap<String, String> = mutableMapOf()
    val operations: MutableList<WsdlOperation> = mutableListOf()
    var location: String? = null

    companion object {
        fun of(wsdlLocation: String, configurator: WsdlParserConfig.() -> Unit): WsdlApi =
            of(wsdlLocation, Consumer { configurator.invoke(it) })

        @JvmStatic
        @JvmOverloads
        fun of(wsdlLocation: String, configurator: Consumer<WsdlParserConfig> = Consumer {}): WsdlApi {
            val config = WsdlParserConfig()
            configurator.accept(config)
            return WsdlParser(config).parse(wsdlLocation).apply {
                location = wsdlLocation
            }
        }
    }

    fun removeOperations(filter: Predicate<WsdlOperation>): WsdlApi = apply {
        operations.removeIf(filter)
    }

    override fun toString() = location ?: name
}

