/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.wsdl.parser

import pl.metaprogramming.model.data.DataType
import javax.xml.namespace.QName

class XsTypeParser : DataTypeParser<QName> {

    private val xsTypes: MutableMap<String, DataType> = mutableMapOf(
        "string" to DataType.TEXT,
        "byte" to DataType.BYTE,
        "boolean" to DataType.BOOLEAN,
        "unsignedByte" to DataType.BYTE,
        "short" to DataType.INT16,
        "unsignedShort" to DataType.INT16,
        "int" to DataType.INT32,
        "integer" to DataType.INT32,
        "unsignedInt" to DataType.INT32,
        "negativeInteger" to DataType.INT32,
        "nonNegativeInteger" to DataType.INT32,
        "nonPositiveInteger" to DataType.INT32,
        "positiveInteger" to DataType.INT32,
        "long" to DataType.INT64,
        "unsignedLong" to DataType.INT64,
        "float" to DataType.FLOAT,
        "double" to DataType.DOUBLE,
        "decimal" to DataType.DECIMAL,
        "date" to DataType.DATE,
        "dateTime" to DataType.DATE_TIME,
    )

    override fun parse(dataType: QName): DataType? = if (dataType.namespaceURI == "http://www.w3.org/2001/XMLSchema") {
        val xsType = dataType.localPart
        check(xsTypes.containsKey(xsType)) { "Can't handle $dataType type" }
        xsTypes[xsType]
    } else {
        null
    }

    fun set(xsType: String, dataType: DataType): XsTypeParser = apply {
        xsTypes[xsType] = dataType
    }
}