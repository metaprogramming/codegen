/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data.constraint

class CheckerRef internal constructor(
    val className: String,
    val methodRef: String? = null,
    val staticField: String? = null,
) : DataConstraint() {
    companion object {
        @JvmStatic
        fun of(className: String): CheckerRef = CheckerRef(className)

        @JvmStatic
        fun ofMethodRef(className: String, method: String): CheckerRef = CheckerRef(className, methodRef = method)

        @JvmStatic
        fun ofStaticField(className: String, field: String): CheckerRef = CheckerRef(className, staticField = field)
    }

    override fun toMap(): Map<String, Any?> = toMap(mutableMapOf<String, Any?>(SPEC_CHECKER_CLASS to className).apply {
        addNotNull(staticField, SPEC_CHECKER_STATIC_FIELD)
        addNotNull(methodRef, SPEC_CHECKER_METHOD_REF)
    })
}