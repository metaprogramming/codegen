/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data

open class DataType protected constructor(val typeCode: DataTypeCode) {
    companion object {
        @JvmField val BINARY = DataType(DataTypeCode.BINARY)
        @JvmField val BASE64 = DataType(DataTypeCode.BASE64)
        @JvmField val TEXT = DataType(DataTypeCode.STRING)
        @JvmField val BOOLEAN = DataType(DataTypeCode.BOOLEAN)
        @JvmField val DATE = DataType(DataTypeCode.DATE)
        @JvmField val DATE_TIME = DataType(DataTypeCode.DATE_TIME)
        @JvmField val NUMBER = DataType(DataTypeCode.NUMBER)
        @JvmField val BYTE = DataType(DataTypeCode.BYTE)
        @JvmField val INT16 = DataType(DataTypeCode.INT16)
        @JvmField val INT32 = DataType(DataTypeCode.INT32)
        @JvmField val INT64 = DataType(DataTypeCode.INT64)
        @JvmField val DECIMAL = DataType(DataTypeCode.DECIMAL)
        @JvmField val FLOAT = DataType(DataTypeCode.FLOAT)
        @JvmField val DOUBLE = DataType(DataTypeCode.DOUBLE)

        val NUMBER_TYPES = setOf(NUMBER, INT16, INT32, INT64, DECIMAL, FLOAT, DOUBLE)
    }

    override fun toString() = typeCode.toString()
    open fun asSchema(code: String?) = DataSchema(code, this)
    fun asSchema(code: String?, builder: DataSchema.() -> Unit) = asSchema(code).apply(builder)

    fun asArray() = asSchema(null).toArrayType()
    fun asArray(builder: ArrayType.() -> Unit) = asSchema(null).toArrayType(builder)

    @JvmName("is")
    fun isType(typeCode: DataTypeCode) = this.typeCode == typeCode

    fun isTypeOrItemType(code: DataTypeCode) =
        isType(code) || (isArray && arrayType.itemsSchema.isType(code)) || (isMap && mapType.valuesSchema.isType(
            code
        ))

    fun isComplex() = isObject || isArray || isMap

    val isEnum get() = typeCode == DataTypeCode.ENUM
    val isEnumOrItemEnum get() = isTypeOrItemType(DataTypeCode.ENUM)
    val isObject get() = typeCode == DataTypeCode.OBJECT
    val isBinary get() = typeCode == DataTypeCode.BINARY
    val isArray get() = typeCode == DataTypeCode.ARRAY
    val isMap get() = typeCode == DataTypeCode.MAP

    val arrayType: ArrayType
        get() {
            check(typeCode == DataTypeCode.ARRAY)
            return this as ArrayType
        }

    val mapType: MapType
        get() {
            check(typeCode == DataTypeCode.MAP)
            return this as MapType
        }

    val objectType: ObjectType
        get() {
            if (isArray) return arrayType.itemsSchema.objectType
            if (isMap) return mapType.valuesSchema.objectType
            check(typeCode == DataTypeCode.OBJECT)
            return this as ObjectType
        }

    val enumType: EnumType
        get() {
            check(typeCode == DataTypeCode.ENUM)
            return this as EnumType
        }

    fun isNumberOrBoolean() = BOOLEAN == this || isNumber()

    fun isNumber() = NUMBER_TYPES.contains(this)
}