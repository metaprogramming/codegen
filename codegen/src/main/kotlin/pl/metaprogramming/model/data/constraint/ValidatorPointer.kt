/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data.constraint

private const val FIELD_PTR = "field:"
private const val VALIDATION_BEAN_PTR = "bean:"
private const val DI_BEAN_PTR = "di-bean:"

class ValidatorPointer private constructor(
    val classCanonicalName: String,
    val staticField: String? = null,
    val method: String? = null,
    val isDiBean: Boolean = false,
) {
    companion object {
        @JvmStatic
        fun of(checkerRef: CheckerRef): ValidatorPointer = ValidatorPointer(
            classCanonicalName = checkerRef.className,
            method = checkerRef.methodRef,
            staticField = checkerRef.staticField,
            isDiBean = checkerRef.staticField == null,
        )

        @JvmStatic
        fun fromExpression(expression: String): ValidatorPointer = when {
            expression.startsWith(FIELD_PTR) -> {
                val value = expression.substring(FIELD_PTR.length)
                val idx = value.lastIndexOf('.')
                ValidatorPointer(
                    classCanonicalName = value.substring(0, idx),
                    staticField = value.substring(idx + 1)
                )
            }

            expression.startsWith(DI_BEAN_PTR) -> {
                val value = expression.substring((DI_BEAN_PTR.length)).split(':')
                ValidatorPointer(
                    classCanonicalName = value[0],
                    method = if (value.size > 1) value[1] else null,
                    isDiBean = true
                )
            }

            expression.startsWith(VALIDATION_BEAN_PTR) -> {
                val value = expression.substring((VALIDATION_BEAN_PTR.length)).split(':')
                ValidatorPointer(
                    classCanonicalName = value[0],
                    method = if (value.size > 1) value[1] else null,
                )
            }

            else -> ValidatorPointer(
                classCanonicalName = expression,
                isDiBean = true
            )
        }
    }
}