/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data.constraint

internal const val X_CONSTRAINTS = "x-constraints"
internal const val X_VALIDATIONS = "x-validations"
internal const val SPEC_RULE = "rule"
internal const val SPEC_DICT = "dictionary"
internal const val SPEC_CONDITION = "condition"
internal const val SPEC_REQUIRED = "required"
internal const val SPEC_ERROR_CODE = "error-code"
internal const val SPEC_PRIORITY = "priority"
internal const val SPEC_IF = "if"
internal const val SPEC_INVALID_PATTERN_CODE = "invalid-pattern-code"
internal const val SPEC_CHECKER_CLASS = "checker-class"
internal const val SPEC_CHECKER_STATIC_FIELD = "checker-static-field"
internal const val SPEC_CHECKER_METHOD_REF = "checker-method-ref"

internal fun MutableMap<String, Any?>.addNotNull(value: Any?, key: String) {
    if (value != null) this[key] = value
}