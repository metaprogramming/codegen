/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data

import pl.metaprogramming.model.data.constraint.ConstraintsSupport
import pl.metaprogramming.model.data.constraint.DataConstraint
import java.util.function.Supplier

/**
 * Represents a data schema.
 * @property code Nazwa kodowa danych.
 * @property required Flaga określająca czy dane są wymagane.
 * @property dataType Typ danych.
 * @property pattern Wyrażenie regularne (dotyczy pola tekstowego).
 * @property minLength Minimalna długość (dotyczy pola tekstowego).
 * @property maxLength Maksymalna długość (dotyczy pola tekstowego).
 * @property minimum Wartość minimalna (dotyczy pola liczbowego).
 * @property maximum Wartość maksymalna (dotyczy pola liczbowego).
 * @property namespace Namespace (applicable for xsd).
 * @property nillable (applicable for xsd).
 */
open class DataSchema(
    var code: String?, // it can be empty when it is the schema of an array element
    var dataType: DataType,
    var required: Boolean = false,
    val additives: MutableMap<String, Any?> = linkedMapOf<String, Any?>().withDefault { null },
    var constraints: ConstraintsSupport = ConstraintsSupport(additives),
) {

    constructor(code: String?, dataType: DataType) : this(code, dataType, false)

    override fun toString() = ToString(this).schemaToString()

    fun isType(vararg typeCodes: DataTypeCode) = typeCodes.any { dataType.isType(it) }
    fun isTypeOrItemType(vararg typeCodes: DataTypeCode) = typeCodes.any { dataType.isTypeOrItemType(it) }

    val isObject get() = dataType is ObjectType
    val isAnyObject get() = dataType.let { it is ObjectType && it.fields.isEmpty() && it.inherits.isEmpty() }
    val objectType get() = dataType.objectType
    val isEnum get() = dataType.isEnum
    val isEnumOrItemEnum get() = dataType.isEnumOrItemEnum
    val enumType get() = dataType.enumType
    val isArray get() = dataType.isArray
    val arrayType get() = dataType.arrayType
    val isMap get() = dataType.isMap
    val mapType get() = dataType.mapType
    val isBinary get() = dataType.isBinary
    val xmlDataType get() = dataType as XmlDataType

    var pattern: String? by additives
    var minLength: Int? by additives
    var maxLength: Int? by additives
    var minimum: String? by additives
    var exclusiveMinimum: Boolean? by additives
    var maximum: String? by additives
    var exclusiveMaximum: Boolean? by additives
    var namespace: String? by additives
    var nillable: Boolean? by additives
    var description: String? by additives
    var format: String?
        get() = if (isArray) arrayType.itemsSchema.format else additives["format"] as String?
        set(value) {
            additives["format"] = value
        }
    var defaultValue: String?
        get() = additives["default"]?.toString()
        set(value) {
            additives["default"] = value
        }

    var invalidPatternCode: String? by constraints::invalidPatternCode

    fun setAdditive(key: String, value: Any?) =
        apply { if (value == null) additives.remove(key) else additives[key] = value }

    @JvmOverloads
    fun <T> getAdditive(key: String, required: Boolean = true): T {
        check(!required || additives.containsKey(key)) { "No '$key' attribute found in data schema '${toString()}' - $additives" }
        @Suppress("UNCHECKED_CAST") return additives[key] as T
    }

    fun <T> getAdditive(key: String, supplier: Supplier<T>): T {
        if (!additives.containsKey(key)) setAdditive(key, supplier.get())
        @Suppress("UNCHECKED_CAST") return additives[key] as T
    }

    fun addConstraint(constraint: DataConstraint) = apply { constraints.add(constraint) }

    fun toArrayType() = ArrayType(this)
    fun toArrayType(builder: ArrayType.() -> Unit) = toArrayType().apply(builder)

    fun toMapType() = MapType(this)
}

private class ToString(val schema: DataSchema, private val doNotDescribeTypes: Set<ObjectType> = emptySet()) {

    fun schemaToString(): String = when {
        schema.isEnum -> enumToString(schema.enumType, schema)
        schema.isObject -> objectToString(schema.objectType, schema)
        schema.isArray -> arrayToString(schema.arrayType, schema)
        else -> "${fieldCode(schema)}${schema.dataType}${getAttributes(schema).ifEmpty { "" }}"
    }

    private fun objectToString(type: ObjectType, schema: DataSchema?): String {
        val attr = mutableListOf(type.code)
        if (!doNotDescribeTypes.contains(type)) {
            val ignoreTypes = this.doNotDescribeTypes.plusElement(type)
            if (schema != null) {
                attr.addAll(getAttributes(schema))
            } else {
                if (!type.description.isNullOrEmpty()) {
                    attr.add("'${type.description}'")
                }
            }
            attr.add("fields: " + type.fields.map { toString(it, ignoreTypes) })
            if (type.inherits.isNotEmpty()) {
                attr.add("inherits: " + type.inherits.map {
                    objectToString(it, null)
                })
            }
        }
        return "${fieldCode(schema, type.code)}OBJECT" + attr
    }

    private fun arrayToString(type: ArrayType, schema: DataSchema) =
        "${fieldCode(schema)}LIST OF ${toString(type.itemsSchema, doNotDescribeTypes)}"

    private fun enumToString(type: EnumType, schema: DataSchema): String {
        val attr = getAttributes(schema)
        attr.add(0, type.code)
        attr.add("allowed: " + type.allowedValues)
        if (!type.descriptions.isNullOrEmpty()) {
            attr.add("descriptions: " + type.descriptions!!.map { "${it.key}:${it.value}" })
        }
        return "${fieldCode(schema, type.code)}ENUM${attr}"
    }

    private fun getAttributes(schema: DataSchema): MutableList<String> {
        val result = mutableListOf<String>()
        if (schema.required) result.add("required")
        schema.nillable?.let { if (it) result.add("nillable") }
        schema.description?.let { result.add("'$it'") }
        schema.namespace?.let { result.add("namespace: $it") }
        return result
    }

    private fun fieldCode(schema: DataSchema?, typeCode: String? = null) =
        if (!schema?.code.isNullOrBlank() && schema?.code != typeCode) "${schema!!.code} " else ""

    private fun toString(schema: DataSchema, doNotDescribeTypes: Set<ObjectType>) =
        ToString(schema, doNotDescribeTypes).schemaToString()
}
