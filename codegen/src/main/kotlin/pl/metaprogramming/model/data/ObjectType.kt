/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data

import pl.metaprogramming.codegen.NEW_LINE


open class ObjectType @JvmOverloads constructor(
    code: String? = null,
    var fields: MutableList<DataSchema> = mutableListOf(),
    var additives: MutableMap<String, Any?> = mutableMapOf(),
    var inherits: MutableList<ObjectType> = mutableListOf(),
) : NamedDataType(DataTypeCode.OBJECT) {

    init {
        if (code != null) this.code = code
    }

    fun field(code: String) = fields.find { it.code == code }

    @JvmOverloads
    fun addField(code: String, type: DataType, builder: DataSchema.() -> Unit = {}) = apply {
        fields.add(type.asSchema(code).apply(builder))
    }

    val description: String?
        get() = listOf("title", "summary", "description").mapNotNull { additives[it] }
            .joinToString(NEW_LINE + NEW_LINE)
            .ifEmpty { null }
}
