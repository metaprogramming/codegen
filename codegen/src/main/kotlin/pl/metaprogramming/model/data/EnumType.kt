/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data

open class EnumType(
    code: String?,
    val allowedValues: List<String>,
    var description: String? = null,
    var descriptions: MutableMap<String, String>? = null,
) : NamedDataType(DataTypeCode.ENUM) {

    init {
        if (code != null) this.code = code
    }
    constructor(allowedValues: List<String>) : this(null, allowedValues)

    fun setDescription(value: String, description: String) = apply {
        check(allowedValues.contains(value))
        if (descriptions == null) descriptions = mutableMapOf()
        descriptions!![value] = description
    }

    fun getDescription(value: String) = descriptions?.let { it[value] }

    override fun asSchema() = super.asSchema().setAdditive("description", description)
}