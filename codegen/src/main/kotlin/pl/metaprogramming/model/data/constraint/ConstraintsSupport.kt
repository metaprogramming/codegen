/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.data.constraint

import org.slf4j.LoggerFactory

class ConstraintsSupport(val additives: MutableMap<String, Any?>) {

    private val log = LoggerFactory.getLogger(ConstraintsSupport::class.java)

    var invalidPatternCode: String?
        get() {
            for (it in getAdditives(X_CONSTRAINTS)) {
                if (it is Map<*, *> && it.containsKey(SPEC_INVALID_PATTERN_CODE))
                    return it[SPEC_INVALID_PATTERN_CODE] as String
            }
            return null
        }
        set(value) {
            getAdditives(X_CONSTRAINTS).add(mutableMapOf(SPEC_INVALID_PATTERN_CODE to value))
        }

    fun add(constraint: DataConstraint) {
        getAdditives(X_CONSTRAINTS).add(constraint.toMap())
    }

    val constraints: List<DataConstraint> get() = Collector().collect()

    private fun getAdditives(element: String): MutableList<Any> {
        if (additives[element] == null) {
            additives[element] = mutableListOf<Any>()
        }
        @Suppress("UNCHECKED_CAST")
        return additives[element] as MutableList<Any>
    }

    private inner class Collector {
        val result = mutableListOf<DataConstraint>()

        fun collect(): List<DataConstraint> {
            getAdditives(X_VALIDATIONS).forEach(this::addValidation)
            getAdditives(X_CONSTRAINTS).forEach(this::addConstraint)
            var priority = 100
            result.forEach {
                if (it.priority == null) it.priority = ++priority
            }
            return result
        }

        private fun addValidation(spec: Any) {
            if (spec is String) {
                if (spec.startsWith("compare:")) {
                    result.add(ConditionConstraint(spec.substring("compare:".length)))
                } else {
                    result.add(ValidationDirective(ValidatorPointer.fromExpression(spec)))
                }
            } else {
                log.error("Can't handle '$X_VALIDATIONS': $spec")
            }
        }

        private fun addConstraint(spec: Any) {
            val constraint = makeConstraint(spec)
            if (constraint != null && spec is Map<*, *>) {
                constraint.errorCode = spec[SPEC_ERROR_CODE] as String?
                constraint.priority = spec[SPEC_PRIORITY] as Int?
                constraint.ifExpression = spec[SPEC_IF] as String?
                result.add(constraint)
            } else {
                log.error("Can't handle '$X_CONSTRAINTS': $spec")
            }
        }

        private fun makeConstraint(spec: Any): DataConstraint? = if (spec is Map<*, *>) when {
            spec.containsKey(SPEC_RULE) -> Constraints.rule(spec[SPEC_RULE] as String)
            spec.containsKey(SPEC_DICT) -> Constraints.dictionary(spec[SPEC_DICT] as String)
            spec.containsKey(SPEC_CONDITION) -> Constraints.condition(spec[SPEC_CONDITION] as String)
            spec.containsKey(SPEC_REQUIRED) -> Constraints.required()
            spec.containsKey(SPEC_CHECKER_CLASS) -> CheckerRef(
                className = spec[SPEC_CHECKER_CLASS] as String,
                staticField = spec[SPEC_CHECKER_STATIC_FIELD] as String?,
                methodRef = spec[SPEC_CHECKER_METHOD_REF] as String?,
            )

            else -> null
        } else null
    }
}