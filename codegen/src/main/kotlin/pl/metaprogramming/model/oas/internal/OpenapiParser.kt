/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.model.oas.*
import pl.metaprogramming.value

internal class OpenapiParser(private val spec: Map<String, Any>, dependsOn: List<RestApi>) {

    private val oas: OpenapiSpec = OpenapiSpec(spec)
    private val model: RestApi
    private val resolver: DataTypeResolver
    private val schemaParser: SchemaParser
    private val parameterParser: ParameterParser
    private val operationsParser: OperationParser
    private val securitySchemaParser: SecuritySchemaParser

    init {
        model = RestApi(dependsOn)
        resolver = DataTypeResolver(oas, model)
        schemaParser = SchemaParser(oas, resolver)
        parameterParser = ParameterParser(oas, resolver)
        if (oas.isOas3) {
            operationsParser = Oas3OperationParser(oas, resolver, parameterParser)
            securitySchemaParser = Oas3SecuritySchemaParser()
        } else {
            operationsParser = Oas2OperationParser(oas, resolver, parameterParser)
            securitySchemaParser = Oas2SecuritySchemaParser()
        }
    }

    fun read(): RestApi {
        oas.spec.value<String>("openapi")?.let { model.openapiVersion = it }
        oas.spec.value<Map<String, Any>>("info")?.let(::readInfo)
        oas.spec.value<List<Map<String, Any>>>("servers").orEmpty().forEachIndexed(::readServer)
        oas.spec.value<List<Map<String, Any>>>("tags").orEmpty().forEachIndexed(::readTag)
        model.basePath = oas.basePath
        schemaParser.parse()
        parameterParser.parse()
        operationsParser.parse()
        resolver.collectAnonymousEnums()
        resolver.restApi.securitySchemas.addAll(securitySchemaParser.parse(spec))
        return resolver.restApi
    }

    private fun readInfo(infoSpec: Map<String, Any>) {
        infoSpec.value<String>("title")?.let { model.info.title = it }
        infoSpec.value<String>("version")?.let { model.info.version = it }
        model.info.apply {
            summary = infoSpec.value("summary")
            description = infoSpec.value("description")
            termsOfService = infoSpec.value("termsOfService")
            contact = infoSpec.value<Map<String, String>>("contact")?.let { contactSpec ->
                OasContact(contactSpec["name"], contactSpec["url"], contactSpec["email"])
            }
            licence = infoSpec.value<Map<String, String>>("license")?.let { licenseSpec ->
                OasLicence(licenseSpec["name"], licenseSpec["identifier"], licenseSpec["url"])
            }
        }
    }

    private fun readServer(index: Int, serverSpec: Map<String, Any>) {
        val path = "servers[$index]"
        model.servers.add(
            OasServer(
                url = serverSpec.valueNotNull("url", path),
                description = serverSpec.value("description"),
                variables = serverSpec.value<Map<String, Map<String, Any>>>("variables")?.mapValues {
                    OasServerVariable(
                        description = it.value.value("description"),
                        enum = it.value.value<List<String>>("enum").orEmpty().toMutableList(),
                        defaultValue = it.value.valueNotNull("default", "${path}.variables.${it.key}"),
                    )
                }.orEmpty().toMutableMap(),
            )
        )
    }

    private fun readTag(index: Int, tagSpec: Map<String, Any>) {
        val path = "tags[$index]"
        model.tags.add(
            OasTag(
                name = tagSpec.valueNotNull("name", path),
                description = tagSpec.value("description"),
                externalDocs = tagSpec.value<Map<String, String>>("externalDocs")?.let {
                    OasExternalDocs(
                        description = it.value("description"),
                        url = it.valueNotNull("url", "${path}.externalDocs")
                    )
                }
            ))
    }

    companion object {
        @JvmStatic
        fun parse(content: String, dependencies: Array<out RestApi>): RestApi {
            val isJson = content.startsWith('{')
            val model = if (isJson) JsonYamlParser.readJson(content) else JsonYamlParser.readYaml(content)
            return OpenapiParser(model, dependencies.asList()).read()
        }
    }
}