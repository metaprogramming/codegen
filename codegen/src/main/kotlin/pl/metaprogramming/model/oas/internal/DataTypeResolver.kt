/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.model.data.*
import pl.metaprogramming.model.oas.RestApi
import pl.metaprogramming.value

internal class DataTypeResolver(
    val path: String,
    val spec: Map<Any, Any>,
    val restApi: RestApi,
    private val enumParser: EnumParser
) {
    @Suppress("UNCHECKED_CAST")
    constructor(spec: OpenapiSpec, restApi: RestApi) : this(
        spec.schemasPath,
        spec.schemas as Map<Any, Any>,
        restApi,
        EnumParser(restApi)
    )

    /**
     * Resolves data type.
     * Inline object definitions are not supported (except free style objects - maps).
     * Each object type must be defined in the schemas section.
     */
    fun resolve(): DataType {
        if (isEnum) {
            // it should not be performed for named enums
            return enumParser.toEnumType(spec)
        }
        if (isRef) {
            return getRefDataType()
        }
        val format = spec["format"]
        return when (dataTypeCode) {
            "string" -> when (format) {
                "date" -> DataType.DATE
                "date-time" -> DataType.DATE_TIME
                "binary" -> DataType.BINARY
                "byte" -> DataType.BASE64
                else -> DataType.TEXT
            }

            "number" -> when (format) {
                "float" -> DataType.FLOAT
                "double" -> DataType.DOUBLE
                else -> DataType.DECIMAL
            }

            "object" -> toObjectTyp()
            "array" -> toArrayType()
            "map" -> toMapType()
            "integer" -> if (format == "int64") DataType.INT64 else DataType.INT32
            "boolean" -> DataType.BOOLEAN
            "file" -> DataType.BINARY // OAS2 only
            "oauth2" -> DataType.TEXT
            "apiKey" -> DataType.TEXT
            else -> error("Can't determine data type for $path: $spec")
        }
    }

    fun resolve(path: String, spec: Map<Any, Any>) = getResolver(path, spec).resolve()

    fun parseEnum(spec: Map<Any, Any>, code: String): EnumType = enumParser.parse(spec, code)
    fun collectAnonymousEnums() = enumParser.collectAnonymousEnums()

    fun getResolver(path: String, spec: Map<Any, Any>) = DataTypeResolver(path, spec, restApi, enumParser)

    fun getChildResolver(childName: String) = getChildResolver(childName, spec.valueNotNull(childName, path))

    private fun getChildResolver(childName: String, childSpec: Map<Any, Any>) =
        getResolver("${path}.${childName}", childSpec)

    private fun resolveChild(childName: String, childSpec: Map<Any, Any>) =
        getChildResolver(childName, childSpec).resolve()

    private fun toObjectTyp(): ObjectType {
        val result = ObjectType()
        val allOf = spec.value<List<Map<Any, Any>>>("allOf")
        if (allOf != null) {
            allOf.forEachIndexed { idx, c ->
                val type = resolveChild("allOf[$idx]", c)
                if (c.isRef()) {
                    if (type is DataTypePlaceholder) {
                        type.inheritedBy.add(result)
                    } else {
                        result.inherits.add(type.objectType)
                    }
                } else {
                    result.fields.addAll(type.objectType.fields)
                }
            }
        } else {
            val properties = spec.value<Map<String, Map<Any, Any>>>("properties").orEmpty()
            check(properties.isNotEmpty()) { "[$path] object should have properties" }
            val requiredProperties = spec.value<List<String>>("required").orEmpty().toSet()
            properties.forEach { (fieldCode, fieldSpec) ->
                result.addField(fieldCode, resolveChild(fieldCode, fieldSpec)) {
                    required = requiredProperties.contains(fieldCode)
                    setAdditives(fieldSpec)
                }
            }
        }
        return result
    }

    private fun toArrayType(): ArrayType {
        val itemsSpec = spec.value<Map<Any, Any>>("items")
        check(itemsSpec != null) { "[$path] items specification not found" }
        val result = resolveChild("items", itemsSpec).asArray()
        result.itemsSchema.setAdditives(itemsSpec)
        spec["minItems"]?.let { result.minItems = it as Int }
        spec["maxItems"]?.let { result.maxItems = it as Int }
        spec["uniqueItems"]?.let { result.uniqueItems = it as Boolean }
        return result
    }

    private fun toMapType(): MapType {
        val valueSpec = spec.value<Map<Any, Any>>("additionalProperties")
        if (valueSpec != null) {
            val valueSchema = resolveChild("additionalProperties", valueSpec).asSchema(null)
            valueSchema.setAdditives(valueSpec)
            return valueSchema.toMapType()
        }
        return ObjectType().asSchema().toMapType()
    }

    private fun getRefDataType(): DataType {
        val code = spec.refCode()
        val schema = restApi.findSchema(code)
        check(schema != null) { "[$path] Can't find schema $code" }
        return schema.dataType
    }

    val isRef: Boolean get() = spec.isRef()
    val isEnum: Boolean get() = spec["enum"] != null
    val isComplexType: Boolean get() = listOf("object", "map", "array").contains(dataTypeCode)
    private val dataTypeCode: String
        get() {
            // powinien być podany 'type' albo może być '$ref',
            // zdarza się, że nie ma żadnego z nich, i wtedy uznajemy, że jest 'Free-Form Object' (map)
            if (isRef) return REF
            val specType = spec.value<String>("type")
            if (specType != null && specType != "object") return specType
            return if (spec["properties"] != null || spec["allOf"] != null) "object" else "map"
        }
}