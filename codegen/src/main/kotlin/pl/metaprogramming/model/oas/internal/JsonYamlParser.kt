/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import org.json.JSONObject
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.LoaderOptions
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor
import org.yaml.snakeyaml.nodes.Tag
import org.yaml.snakeyaml.representer.Representer
import org.yaml.snakeyaml.resolver.Resolver

internal object JsonYamlParser {

    fun read(filename: String, content: String): Map<String, Any> {
        return if (isJson(filename)) {
            readJson(content)
        } else if (isYaml(filename)) {
            readYaml(content)
        } else {
            throw IllegalArgumentException("Can't determine file type (json/yaml) for $filename")
        }
    }

    fun readYaml(body: String): Map<String, Any> {
        val ops = DumperOptions()
        return Yaml(Constructor(LoaderOptions()), Representer(ops), ops, YamlTypeResolver())
            .load(body)
    }

    fun readJson(body: String): Map<String, Any> = JSONObject(body).toMap()

    private fun isJson(filename: String) = filename.endsWith(".json", true)
    private fun isYaml(filename: String) = filename.endsWith(".yaml", true) || filename.endsWith(".yml", true)
}

private class YamlTypeResolver : Resolver() {
    override fun addImplicitResolvers() {
        addImplicitResolver(Tag.BOOL, BOOL, "yYnNtTfFoO")
        addImplicitResolver(Tag.INT, INT, "-+0123456789")
        addImplicitResolver(Tag.NULL, NULL, "~nN0")
        addImplicitResolver(Tag.NULL, EMPTY, null)
    }
}