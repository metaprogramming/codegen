/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.model.oas.ParamLocation
import pl.metaprogramming.model.oas.SecuritySchema
import pl.metaprogramming.value

abstract class SecuritySchemaParser {
    abstract fun parse(spec: Map<String, Any>): MutableList<SecuritySchema>

    protected fun toSecuritySchema(code: String, spec: Map<String, Any>): SecuritySchema {
        val schema = SecuritySchema(code, spec.value("type")!!)
        schema.description = spec.value("description")
        spec.value<String>("in")?.let { schema.paramLocation = ParamLocation.valueOf(it.uppercase()) }
        spec.value<String>("name")?.let { schema.paramName = it }
        return schema
    }

}