/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.ParamLocation
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.value

internal class ParameterParser(private val oas: OpenapiSpec, private val resolver: DataTypeResolver) {
    fun parse() {
        oas.parameters.forEach { (k, v) ->
            val param = parse(v, "${oas.parametersPath}.$k", k)
            resolver.restApi.addParameter(param)
        }
    }

    fun parse(spec: Spec): Parameter = parse(spec.spec, spec.specPath)

    fun parse(spec: Map<Any, Any>, specPath: String, forceCode: String? = null): Parameter {
        val name = spec.valueNotNull<String>("name", specPath)
        val paramCode = forceCode ?: name
        val schemaSpec = oas.getParameterSchema(spec)
        val paramType = resolver.getResolver(name, schemaSpec).resolve()
        val location = ParamLocation.valueOf(spec.valueNotNull<String>("in", specPath).uppercase())
        return location.asParam(name, paramType) {
            required = spec.value<Boolean>("required") == true
            code = paramCode
            setAdditives(schemaSpec)
            spec.forEach { (k, v) ->
                if (k is String && (k == "description" || k.startsWith("x-"))) setAdditive(k, v)
            }
            fixEnumCode(paramCode)
        }
    }

    private fun DataSchema.fixEnumCode(code: String) {
        if (isEnum && !enumType.isNamed) {
            enumType.code = code
        }
        if (isArray) arrayType.itemsSchema.fixEnumCode(code)
    }
}