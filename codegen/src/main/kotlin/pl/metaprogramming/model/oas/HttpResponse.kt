/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import pl.metaprogramming.model.data.DataSchema

class HttpResponse(
    val status: Int,
    var description: String?,
    val headers: MutableSet<String>,
    val contents: MutableMap<String, DataSchema>
) {
    companion object {
        const val DEFAULT = 0
    }

    val isSuccessResponse: Boolean get() = status in 200..206
    val isDefault: Boolean get() = status == DEFAULT
    val schema: DataSchema? get() = contents.firstNotNullOfOrNull { it.value }

    override fun toString(): String {
        val buf = StringBuilder()
        buf.append(if (isDefault) "default" else status)
        schema?.let { buf.append(" $schema") }
        return buf.toString()
    }
}