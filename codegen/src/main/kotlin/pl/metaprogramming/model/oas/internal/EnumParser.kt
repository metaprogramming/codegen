/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.ValueCounter
import pl.metaprogramming.codegen.NEW_LINE
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.EnumType
import pl.metaprogramming.model.oas.RestApi
import pl.metaprogramming.upperFirstChar
import pl.metaprogramming.value

internal class EnumParser(private val restApi: RestApi) {

    private val enumByKey: MutableMap<String, EnumType> = mutableMapOf()
    private val enumCodeUsages: MutableMap<String, Int> = mutableMapOf()
    private val enumsToProcess: MutableSet<EnumType> = mutableSetOf()
    private val enumUsages: MutableSet<EnumUsage> = mutableSetOf()

    fun collectAnonymousEnums() {
        enumsToProcess.addAll(enumByKey.values)

        selectWithFixedCodes() // Wybiera enumy z ustaloną wartością kodową (parametry oraz z rozszerzeniem 'x-ms-enum.name')
        collectEnumUsages()
        selectByFieldCode() // Wybiera enumy, które są użyte w polach o nazwach niekolidujących z innymi polami-enumami. Takim enumom są nadawane kody zgodne z kodami pól.
        selectByObjectAndFieldCode() // Wybiera enumy, które są użyte tylko w jednym obiekcie. Takim enumom są nadawane kody zgodne z konkatenacją kodu obiektu i pola.
        selectRest() // Wybiera pozostałe enumy i nadaje kody zgodne z kodami pól (z postfixem numerycznym)

        check(enumsToProcess.isEmpty())

        enumByKey.values.forEach {
            check(it.isNamed)
            restApi.addSchema(it.asSchema())
        }
    }

    fun toEnumType(spec: Map<Any, Any>): EnumType =
        enumByKey.getOrPut(spec.enums().sorted().joinToString("_")) { parse(spec, null) }

    fun parse(spec: Map<Any, Any>, code: String?): EnumType {
        val result = EnumType(spec.enums())
        result.description = listOf("title", "description").mapNotNull { spec[it] }
            .joinToString(NEW_LINE + NEW_LINE)
            .ifEmpty { null }
        val xmsenum = spec.value<Map<String, *>>("x-ms-enum")
        (code ?: xmsenum?.value("name"))?.let { result.code = it }
        xmsenum?.value<List<Map<String, Any>>>("values")?.forEach {
            result.setDescription(it["value"].toString(), it["description"].toString())
        }
        return result
    }

    private fun selectWithFixedCodes() {
        val selected = enumsToProcess.filter { it.isNamed }.toSet()
        enumsToProcess.removeAll(selected)
        selected.forEach { reserveEnumCode(it) }
    }

    private fun reserveEnumCode(enumType: EnumType, code: String? = null) {
        if (code != null) {
            if (!enumType.isNamed) enumType.code = giveCode(code)
        } else if (enumType.isNamed) {
            enumType.code = giveCode(enumType.code)
        } else {
            error("Can't designate a name for enum: $enumType")
        }
    }

    private fun collectEnumUsages() {
        restApi.schemas.filter { it.isObject }.forEach { schema ->
            schema.objectType.fields.forEach { field ->
                val enumType = getEnumFromField(field)
                if (enumsToProcess.contains(enumType)) {
                    enumUsages.add(EnumUsage(enumType!!, field, schema.code!!))
                }
            }
        }
    }

    private fun getEnumFromField(field: DataSchema): EnumType? =
        if (field.isEnum) field.enumType
        else if (field.isArray) getEnumFromField(field.arrayType.itemsSchema)
        else null

    private fun selectByFieldCode() {
        enumUsages
            .groupBy(EnumUsage::fieldCode)
            .toSortedMap()
            .filter { !enumCodeUsages.containsKey(it.key.upperFirstChar()) && it.value.groupBy(EnumUsage::enumType).size == 1 }
            .forEach { select(it.value) }
    }

    private fun selectByObjectAndFieldCode() {
        enumUsages
            .groupBy(EnumUsage::enumType)
            .filter { it.value.size == 1 }
            .forEach {
                select(
                    it.value,
                    "${it.value[0].objectCode.upperFirstChar()}${it.value[0].fieldCode.upperFirstChar()}"
                )
            }
    }

    private fun selectRest() {
        enumUsages
            .groupBy(EnumUsage::enumType)
            .toList()
            .sortedWith { a, b ->
                val frequencyDiff = b.second.size.compareTo(a.second.size)
                if (frequencyDiff == 0) b.first.toSortValue().compareTo(a.first.toSortValue()) else frequencyDiff
            }.forEach {
                select(it.second)
            }
    }

    private fun select(enumType: EnumType, code: String) {
        enumUsages.removeIf { it.enumType == enumType }
        enumsToProcess.remove(enumType)
        reserveEnumCode(enumType, code.upperFirstChar())
    }

    private fun select(values: List<EnumUsage>, code: String) {
        select(values.first().enumType, code)
    }

    private fun select(usages: Collection<EnumUsage>) {
        val fieldCode = ValueCounter(usages.map(EnumUsage::fieldCode)).most.min()
        val usage = usages.first { it.fieldCode == fieldCode }
        val enumType = usage.enumType
        enumType.description = usage.field.description
        select(enumType, fieldCode)
    }

    private fun giveCode(code: String): String {
        val fixedCode = code.upperFirstChar()
        val usageCount = enumCodeUsages[fixedCode]?.let { it + 1 } ?: 1
        enumCodeUsages[fixedCode] = usageCount
        if (fixedCode == "TypeOfPayment") {
            println(code)
        }
        return "$fixedCode${if (usageCount == 1) "" else usageCount}"
    }

    private fun Map<*, *>.enums(): List<String> = this.value<List<*>>("enum")!!.map { it.toString() }

    private fun EnumType.toSortValue(): String {
        val size = String.format("%04d", enumType.allowedValues.size)
        return "$code $size ${enumType.allowedValues.joinToString(",")}"
    }

}

private class EnumUsage(val enumType: EnumType, val field: DataSchema, val objectCode: String) {
    val fieldCode = field.code!!
    override fun toString() = "${objectCode}.${field.code} ${enumType.allowedValues}"
}

