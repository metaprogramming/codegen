/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.value


internal const val REF = "\$ref"

internal fun DataSchema.setAdditives(values: Map<Any, Any>): DataSchema {
    @Suppress("UNCHECKED_CAST")
    additives.putAll(values as Map<String, Any>)
    additives["minimum"]?.let { minimum = it.toString() }
    additives["maximum"]?.let { maximum = it.toString() }
    return this
}

internal fun <T : Any> Map<*, *>.valueNotNull(path: String, parentPath: String): T =
    this.value(path) ?: error("Can't find value on ${parentPath}.${path}")

internal fun Map<*, *>.isRef(): Boolean = this[REF] != null
internal fun Map<*, *>.refCode(): String {
    val ref = this[REF] as String
    return ref.substring(ref.lastIndexOf('/') + 1)
}

internal open class Spec(val spec: Map<Any, Any>, val specPath: String) {

    val name: String
        get() {
            val dotIdx = specPath.lastIndexOf('.')
            return if (dotIdx > 0) specPath.substring(dotIdx + 1) else specPath
        }
    val isRef: Boolean get() = spec.containsKey(REF)
    val refCode: String
        get() {
            val ref = valueNotNull<String>(REF)
            return ref.substring(ref.lastIndexOf('/') + 1)
        }

    fun <T : Any> value(path: String): T? {
        @Suppress("UNCHECKED_CAST")
        return spec[path] as T?
    }

    fun <T : Any> valueNotNull(path: String): T =
        value(path) ?: this.value(path) ?: error("Can't find value on ${specPath}.${path}")

    fun child(path: String): Spec? = value<Map<Any, Any>>(path)?.let { Spec(it, "${specPath}.${path}") }
    fun childNotNull(path: String): Spec =
        child(path) ?: error("Can't find value on ${specPath}.${path}")

    fun children(): List<Spec> {
        val result = mutableListOf<Spec>()
        spec.forEach {
            if (it.value is Map<*, *>) {
                @Suppress("UNCHECKED_CAST")
                result.add(Spec(it.value as Map<Any, Any>, "${specPath}.${it.key}"))
            }
        }
        return result
    }

    fun children(path: String): List<Spec> = value<List<Map<Any, Any>>>(path)
        ?.mapIndexed { index, map -> Spec(map, "${specPath}.${path}[$index]") }
        ?.toList().orEmpty()
}