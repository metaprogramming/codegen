/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.model.oas.HttpRequestBody

internal class Oas3OperationParser(oas: OpenapiSpec, resolver: DataTypeResolver, parameterParser: ParameterParser) :
    OperationParser(oas, resolver, parameterParser) {
    override fun parseRequestBody(operation: OperationSpec) =
        operation.child("requestBody")?.let { requestBody ->
            // TODO alternatively to 'content' the '$ref' should be supported
            val contents = requestBody.childNotNull("content").children()
                .associate { it.name to parseSchema(it.childNotNull("schema")) }
                .toMutableMap()
            HttpRequestBody(
                operation.value("x-codegen-request-body-name") ?: "body",
                requestBody.value("required") ?: false,
                requestBody.value("description"),
                contents
            )
        }

    override fun parseResponseSchema(spec: Spec, operation: OperationSpec) =
        spec.child("content")?.children()
            ?.associate { it.name to parseSchema(it.childNotNull("schema")) }
            .orEmpty().toMutableMap()
}