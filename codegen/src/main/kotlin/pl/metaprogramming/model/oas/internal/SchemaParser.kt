/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.model.data.DataSchema

internal class SchemaParser(private val oas: OpenapiSpec, private val resolver: DataTypeResolver) {
    fun parse() {
        oas.schemas.keys.map { LazySchemaParser(it, resolver.getChildResolver(it)) }.forEach { it.make() }
    }
}

/**
 * Parser for schema definitions (/definitions - OAS2; /components/schemas - OAS3)
 */
private class LazySchemaParser(
    code: String,
    private val resolver: DataTypeResolver,
) {

    val schema: DataSchema = if (shouldUsePlaceholder) {
        DataSchema(code, DataTypePlaceholder())
    } else if (resolver.isEnum) {
        resolver.parseEnum(resolver.spec, code).asSchema()
    } else {
        resolver.resolve().asSchema(code)
    }

    init {
        resolver.restApi.addSchema(schema.setAdditives(resolver.spec))
    }

    fun make() {
        val placeholder = schema.dataType
        if (placeholder !is DataTypePlaceholder) return
        val dataType = resolver.resolve()
        placeholder.refs.forEach { it.dataType = dataType }
        if (dataType.isObject) {
            val obj = dataType.objectType
            placeholder.inheritedBy.forEach { it.inherits.add(obj) }
            obj.code = schema.code!!
            obj.additives = schema.additives
        }
        schema.dataType = dataType
    }

    private val shouldUsePlaceholder get() = resolver.isComplexType || resolver.isRef
}