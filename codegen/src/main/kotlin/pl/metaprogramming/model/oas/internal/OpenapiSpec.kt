/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.value
import java.net.URI

internal class OpenapiSpec(val spec: Map<String, Any>) {
    private val isOas2: Boolean =
        spec["swagger"] == "2.0" || spec.containsKey("definitions") || spec.containsKey("parameters")
    val isOas3: Boolean =
        spec.value<String>("openapi")?.startsWith("3.") == true || spec.containsKey("components")
    val schemasPath: String = if (isOas3) "components.schemas" else "definitions"
    val schemas: Map<String, Map<Any, Any>> = spec.value<Map<String, Map<Any, Any>>>(schemasPath).orEmpty()
    val parametersPath: String = if (isOas3) "components.parameters" else "parameters"
    val parameters: Map<String, Map<Any, Any>> = spec.value<Map<String, Map<Any, Any>>>(parametersPath).orEmpty()
    val basePath: String = if (isOas3) {
        val paths = spec.value<List<Map<String, String>>>("servers")
            ?.mapIndexed { idx, it ->
                val url = it.valueNotNull<String>("url", "servers[$idx]")
                if (!url.contains('{')) URI(url).path else null
            }?.filterNotNull()?.distinct().orEmpty()
        if (paths.size == 1) paths[0] else ""
    } else spec.value<String>("basePath").orEmpty()

    init {
        check(isOas2.xor(isOas3)) { "Invalid OpenAPI specification" }
    }

    fun getParameterSchema(paramSpec: Map<Any, Any>): Map<Any, Any> =
        if (isOas3) paramSpec.value<Map<Any, Any>>("schema").orEmpty() else paramSpec
}