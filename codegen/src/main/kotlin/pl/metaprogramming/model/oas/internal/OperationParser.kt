/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.log
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.*
import pl.metaprogramming.value

private const val CONTROLLER_TAG = "x-swagger-router-controller"
private val httpMethods = HttpMethod.entries.map { it.name.lowercase() }
private val oas2BodyParams = listOf("body", "formData")

internal abstract class OperationParser(
    protected val oas: OpenapiSpec,
    private val resolver: DataTypeResolver,
    private val parameterParser: ParameterParser
) {

    protected abstract fun parseRequestBody(operation: OperationSpec): HttpRequestBody?
    protected abstract fun parseResponseSchema(spec: Spec, operation: OperationSpec): MutableMap<String, DataSchema>

    fun parse() {
        collectOperations().map {
            Operation(
                resolver.restApi,
                it.controller,
                it.operationId,
                it.fullPath(oas.basePath),
                HttpMethod.valueOf(it.operationType.uppercase()),
                parseRequestBody(it),
                parseParameters(it).toMutableList(),
                parseResponses(it),
                parseSecurity(it),
                it.additives.toMutableMap()
            )
        }.forEach {
            resolver.restApi.operations.add(it)
        }
    }

    protected fun parseSchema(schema: Spec, code: String? = null) =
        resolver.resolve(schema.specPath, schema.spec)
            .asSchema(code ?: if (schema.spec.isRef()) schema.spec.refCode() else null) {
                setAdditives(schema.spec)
            }

    private fun collectOperations(): List<OperationSpec> {
        val result = mutableListOf<OperationSpec>()
        oas.spec.value<Map<String, Map<String, Any>>>("paths")?.forEach { (path, operations) ->
            val controllerTag = operations.value<String>(CONTROLLER_TAG)
            operations.forEach { (operationType, spec) ->
                if (operationType in httpMethods && spec is Map<*, *>) {
                    val tags = spec.value<List<String>>("tags").orEmpty()
                    val controller = listOfNotNull(
                        controllerTag,
                        if (tags.size == 1) tags[0] else null,
                        resolver.restApi.name
                    ).first()
                    @Suppress("UNCHECKED_CAST")
                    result.add(OperationSpec(spec as Map<Any, Any>, path, operationType, controller))
                } else if (!operationType.startsWith("x-")) {
                    log.warn("Ignore operation type: $operationType")
                }
            }
        }
        return result
    }

    private fun parseParameters(operation: OperationSpec): List<Parameter> {
        return operation.parameters.filter { !oas2BodyParams.contains(it.value("in")) }.map {
            if (it.isRef) resolver.restApi.getParameter(it.refCode)
                ?: throw IllegalStateException("[${operation.specPath}] Unknown parameter ${it.refCode}")
            else parameterParser.parse(it)
        }
    }

    private fun parseResponses(operation: OperationSpec) = operation.responses.children().map {
        HttpResponse(
            if (it.name == "default") 0 else it.name.toInt(),
            it.value("description"),
            it.value<Map<String, *>>("headers")?.keys.orEmpty().toMutableSet(),
            parseResponseSchema(it, operation)
        )
    }.toMutableList()

    private fun parseSecurity(operation: OperationSpec) = operation.security.map {
        val schema = it.keys.first()
        SecurityConstraint(schema, it.getValue(schema).toMutableList())
    }.toMutableList()

}