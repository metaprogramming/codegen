/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.HttpRequestBody
import pl.metaprogramming.value

private val formContentTypes = listOf("multipart/form-data", "application/x-www-form-urlencoded")

internal class Oas2OperationParser(oas: OpenapiSpec, resolver: DataTypeResolver, parameterParser: ParameterParser) :
    OperationParser(oas, resolver, parameterParser) {
    override fun parseRequestBody(operation: OperationSpec): HttpRequestBody? {
        return parseBodyParam(operation) ?: parseFormDataParams(operation)
    }

    override fun parseResponseSchema(spec: Spec, operation: OperationSpec): MutableMap<String, DataSchema> {
        val schema = spec.child("schema")?.let { parseSchema(it) }
        return if (schema != null) getOperationProduces(operation).associateWith { schema }.toMutableMap()
        else mutableMapOf()
    }

    private fun parseBodyParam(operation: OperationSpec): HttpRequestBody? {
        return operation.parameters
            .filter { it.value<String>("in") == "body" }
            .map {
                val bodySchema = it.child("schema")?.let { schemaSpec -> parseSchema(schemaSpec) }
                HttpRequestBody(
                    it.valueNotNull("name"),
                    required = it.value("required") ?: false,
                    description = it.value("description"),
                    contents = getOperationConsumes(operation).associateWith { bodySchema!! }.toMutableMap()
                )
            }
            .firstOrNull()
    }

    private fun parseFormDataParams(operation: OperationSpec): HttpRequestBody? {
        val formDataParams = operation.parameters.filter { it.value<String>("in") == "formData" }
        if (formDataParams.isEmpty()) return null
        val consumes = getOperationConsumes(operation).filter { formContentTypes.contains(it) }
        check(consumes.isNotEmpty()) { "Operation with 'formData' parameters should consumes one of content type: $formContentTypes" }
        val properties = formDataParams.associateBy(
            { it.spec.value<String>("name") },
            { it.spec.filter { e -> listOf("type", "description").contains(e.key) } })
        val requiredProperties =
            formDataParams.filter { it.value<Boolean>("required") == true }.map { it.value<String>("name") }
        val bodySchemaSpec = Spec(
            mapOf(
                "type" to "object",
                "required" to requiredProperties,
                "properties" to properties
            ), operation.specPath
        )
        val bodySchema = parseSchema(bodySchemaSpec, "requestBody")
        return HttpRequestBody(
            "requestBody",
            required = true,
            contents = consumes.associateWith { bodySchema }.toMutableMap()
        )
    }

    private fun getOperationConsumes(operation: OperationSpec): List<String> {
        if (operation.spec.containsKey("consumes")) {
            return operation.valueNotNull("consumes")
        }
        if (oas.spec.containsKey("consumes")) {
            @Suppress("UNCHECKED_CAST")
            return oas.spec["consumes"] as List<String>
        }
        if (operation.httpMethod.hasBody()) {
            return listOf("application/json")
        }
        return emptyList()
    }

    private fun getOperationProduces(operation: OperationSpec): List<String> =
        operation.value("produces") ?: oas.spec.value("produces") ?: emptyList()
}