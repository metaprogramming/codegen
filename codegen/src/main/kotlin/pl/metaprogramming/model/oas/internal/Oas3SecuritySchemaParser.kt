/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.model.oas.Oauth2Flow
import pl.metaprogramming.model.oas.SecuritySchema
import pl.metaprogramming.value

internal class Oas3SecuritySchemaParser : SecuritySchemaParser() {
    override fun parse(spec: Map<String, Any>): MutableList<SecuritySchema> {
        val result = mutableListOf<SecuritySchema>()
        spec.value<Map<String, Map<String, Any>>>("components.securitySchemes")?.forEach { (code, securitySpec) ->
            val schema = toSecuritySchema(code, securitySpec)
            securitySpec.value<Map<String, Map<String, Any>>>("flows")?.forEach { (type, flowSpec) ->
                val flow =
                    Oauth2Flow(type, flowSpec.value("authorizationUrl"), flowSpec.value("tokenUrl"))
                flowSpec.value<Map<String, String>>("scopes")?.forEach(flow.scopes::put)
                schema.oauth2Flows.add(flow)
            }
            result.add(schema)
        }
        return result
    }
}