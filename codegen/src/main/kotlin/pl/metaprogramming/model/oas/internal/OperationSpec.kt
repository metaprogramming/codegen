/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas.internal

import pl.metaprogramming.model.oas.HttpMethod

private val operationAdditives = setOf("description", "tags", "summary")

internal class OperationSpec(
    spec: Map<Any, Any>,
    val path: String,
    val operationType: String,
    val controller: String
) : Spec(spec, "paths[${path}].${operationType}") {
    val operationId: String = valueNotNull("operationId")
    val parameters: List<Spec> = children("parameters")
    val responses: Spec = childNotNull("responses")
    val security: List<Map<String, List<String>>> = value<List<Map<String, List<String>>>>("security").orEmpty()
    val httpMethod: HttpMethod = HttpMethod.valueOf(operationType.uppercase())

    val additives: Map<String, Any>
        get() {
            val result = mutableMapOf<String, Any>()
            spec.forEach {
                val k = it.key
                if (k is String && (operationAdditives.contains(k) || k.startsWith("x-"))) result[k] = it.value
            }
            return result
        }

    fun fullPath(basePath: String): String = basePath + (
            if (basePath.endsWith('/') && path.startsWith('/')) path.substring(1) else path)
}