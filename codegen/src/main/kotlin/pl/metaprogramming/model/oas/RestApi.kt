/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import pl.metaprogramming.ValueCounter
import pl.metaprogramming.codegen.Model
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.oas.internal.OpenapiParser
import java.util.function.Predicate

/**
 * REST API model based on the OpenAPI specification.
 */
class RestApi(override val dependsOn: List<RestApi>) : Model {

    companion object {
        @JvmStatic
        fun of(content: String, vararg dependencies: RestApi): RestApi = OpenapiParser.parse(content, dependencies)
    }

    var basePath: String = ""
    var openapiVersion: String = "3.0.3"
    val info: OasInfo = OasInfo()
    val servers: MutableList<OasServer> = mutableListOf()
    val tags: MutableList<OasTag> = mutableListOf()
    val operations: MutableList<Operation> = mutableListOf()
    val parameters: MutableList<Parameter> = mutableListOf()
    val schemas: MutableList<DataSchema> = mutableListOf()
    val securitySchemas: MutableList<SecuritySchema> = mutableListOf()

    override var name: String by info::title

    val groupedOperations: Map<String, List<Operation>> get() = operations.groupBy { it.group }
    override fun toString(): String = name

    val defaultErrorResponse: DataType?
        get() {
            val counter = ValueCounter<DataType>()
            operations.forEach { counter.count(it.defaultResponse?.schema?.dataType) }
            if (counter.dominant == null) {
                operations.forEach { o -> o.errorResponses.forEach { counter.count(it.schema?.dataType) } }
            }
            return counter.dominant
        }

    fun getOperation(code: String): Operation = operations.first { it.code == code }

    fun forEachOperation(consumer: Operation.() -> Unit) = apply {
        operations.forEach(consumer)
    }

    fun removeOperations(filter: Predicate<Operation>) = apply { operations.removeIf(filter) }

    fun addSchema(schema: DataSchema) = apply {
        check(schemas.none { it.code == schema.code }) {
            "Schema with code '${schema.code}' is already defined"
        }
        schemas.add(schema)
    }

    fun forEachSchema(consumer: DataSchema.() -> Unit) = apply {
        schemas.forEach(consumer)
    }

    fun updateSchema(code: String, consumer: DataSchema.() -> Unit) = apply {
        consumer.invoke(getSchema(code))
    }

    /**
     * Gets a [DataSchema] object representing the object's schema in the OpenAPI specification.
     *
     * You can get the schema of an object or the schema for an object's property.
     * To get the schema for an object's property, the [code] parameter should be
     * a combination of the object code and the property code: `<OBJECT_SCHEMA_NAME>.<PROPERTY_NAME>`.
     */
    fun getSchema(code: String): DataSchema = findSchema(code)
        ?: throw NoSuchElementException("Can't find schema with code $code")

    fun addParameter(parameter: Parameter) = apply {
        check(parameters.none { it.code == parameter.code }) {
            "Parameter with code '${parameter.code}' is already defined"
        }
        parameters.add(parameter)
    }

    fun getParameter(codeOrName: String): Parameter? =
        parameters.find { it.code == codeOrName || it.name == codeOrName }
            ?: dependsOn.firstNotNullOfOrNull { it.getParameter(codeOrName) }

    fun findSchema(code: String): DataSchema? = if (code.contains('.')) {
        val split = code.split(".")
        findSchema(split[0])?.objectType?.field(split[1])
    } else {
        schemas.find { it.code == code } ?: dependsOn.firstNotNullOfOrNull { it.findSchema(code) }
    }
}