/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import pl.metaprogramming.codegen.NEW_LINE
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.ObjectType

private const val MULTIPART_FORM_DATA = "multipart/form-data"

class Operation(
    val api: RestApi,
    var group: String,
    var code: String,
    var path: String,
    var method: HttpMethod,
    var requestBody: HttpRequestBody?,
    val parameters: MutableList<Parameter> = mutableListOf(),
    val responses: MutableList<HttpResponse> = mutableListOf(),
    val security: MutableList<SecurityConstraint> = mutableListOf(),
    val additives: MutableMap<String, Any> = mutableMapOf(),
) {
    constructor(api: RestApi, group: String, code: String, path: String, method: HttpMethod) : this(
        api,
        group,
        code,
        path,
        method,
        null
    )

    /**
     * Synthetic value (it may be recalculated after change of other data).
     */
    val securityParameters: Set<Parameter> get() = _securityParametersHolder.value

    /**
     * Synthetic value (it may be recalculated after change of other data).
     */
    val requestSchemas: List<HttpRequestSchema> get() = _requestSchemasHolder.value

    override fun toString() = "$code[$method $path]"

    val multipartFormDataRequestBody: ObjectType? get() = requestBody?.contents?.get(MULTIPART_FORM_DATA)?.objectType

    /**
     * First value of [requestSchemas].
     * Synthetic value (it may be recalculated after change of other data).
     */
    val requestSchema: HttpRequestSchema get() = requestSchemas.first()

    /**
     * The [requestSchema.bodySchema] of the first value of [requestSchemas].
     * Synthetic value (it may be recalculated after change of other data).
     */
    val requestBodySchema: DataSchema? get() = requestSchema.bodySchema
    val hasManyRequestSchemas: Boolean @JvmName("hasManyRequestSchemas") get() = requestSchemas.size > 1
    val description: String
        get() = listOf("summary", "description").mapNotNull(additives::get).joinToString(NEW_LINE)
    val defaultResponse: HttpResponse? get() = responses.find { it.isDefault }
    val successResponse: HttpResponse? get() = responses.find { it.isSuccessResponse }
    val successResponses: List<HttpResponse> get() = responses.filter { it.isSuccessResponse }
    val successResponseSchema: DataSchema? get() = successResponse?.schema
    val successResponseHeaders: Collection<String>? get() = successResponse?.headers
    val errorResponses: Collection<HttpResponse>
        get() = responses.filter { (it.isDefault || it.status >= 400) && it.schema != null }
    val isMultipart: Boolean get() = getParameters(ParamLocation.FORMDATA).isNotEmpty() || multipartFormDataRequestBody != null
    val isConsumeJson: Boolean get() = requestBody?.isJsonAllowed() == true
    val consumes: List<String> get() = requestBody?.contents?.keys?.toList().orEmpty()
    val produces: List<String> get() = responses.fold(setOf<String>()) { acc, e -> acc + e.contents.keys }.toList()

    fun isBodySchema(schema: DataSchema) = requestSchemas.any { it.isSchemaForBody(schema) }
    fun getParameters(location: ParamLocation) = _allParameters.filter { it.location == location }
    fun getSecurity(parameter: Parameter): SecurityConstraint {
        val schema = api.securitySchemas.first { it.paramName == parameter.name }.code
        return security.first { it.schema == schema }
    }

    private val _allParameters: List<Parameter> get() = (securityParameters.toList() + parameters).distinct()

    private val _requestSchemasHolder = object : SyntheticObject<List<HttpRequestSchema>>() {
        override fun makeObject(): List<HttpRequestSchema> {
            val result = mutableListOf<HttpRequestSchema>()
            if (requestBody == null) {
                result.add(
                    HttpRequestSchema(
                        mutableListOf(),
                        makeBaseRequestSchema(),
                        "",
                        null,
                        this@Operation
                    )
                )
            }
            requestBody?.contents?.forEach { (contentType, bodySchema) ->
                val exists = result.find { it.bodySchema?.dataType == bodySchema.dataType }
                if (exists != null) {
                    exists.contentTypes.add(contentType)
                } else {
                    val schema = makeBaseRequestSchema()
                    if (bodySchema.isObject && !bodySchema.objectType.isNamed) {
                        bodySchema.objectType.fields.forEach {
                            schema.fields.add(ParamLocation.FORMDATA.asParam(it))
                        }
                    } else {
                        schema.fields.add(bodySchema.fixBy(requestBody!!))
                    }
                    result.add(
                        HttpRequestSchema(
                            mutableListOf(contentType),
                            schema,
                            if (bodySchema.isObject && bodySchema.objectType.isNamed) bodySchema.objectType.code else contentType,
                            bodySchema,
                            this@Operation
                        )
                    )
                }
            }
            return result
        }

        private fun DataSchema.fixBy(req: HttpRequestBody): DataSchema {
            code = req.code
            required = req.required
            setAdditive("description", req.description)
            return this
        }

        private fun makeBaseRequestSchema() = ObjectType().apply {
            additives.putAll(this@Operation.additives.filter { it.key.startsWith("x-") })
            fields.addAll(_allParameters)
        }

        override fun makeMarkers(result: MutableList<Any?>) {
            result.add(securityParameters)
            result.addAll(parameters)
            result.add(requestBody.hashCode())
            requestBody?.contents?.forEach {
                result.add(it.key)
                result.add(it.value.dataType)
            }
        }
    }

    private val _securityParametersHolder = object : SyntheticObject<Set<Parameter>>() {
        override fun makeObject(): Set<Parameter> {
            val result = mutableSetOf<Parameter>()
            security.forEach { constraint ->
                api.securitySchemas.find { it.code == constraint.schema }?.let { securitySchema ->
                    result.add(parameters.find { it.name == securitySchema.paramName }
                        ?: securitySchema.paramLocation.asParam(securitySchema.paramName, DataType.TEXT))
                }
            }
            if (result.size == 1 && !parameters.contains(result.first())) {
                result.first().required = true
            }
            return result
        }

        override fun makeMarkers(result: MutableList<Any?>) {
            result.addAll(security)
            result.addAll(api.securitySchemas)
            result.addAll(parameters)
        }
    }
}

private abstract class SyntheticObject<T : Any> {

    abstract fun makeObject(): T
    abstract fun makeMarkers(result: MutableList<Any?>)

    val value: T
        get() {
            val newMarkers = mutableListOf<Any?>()
            makeMarkers(newMarkers)
            if (!this::prevValue.isInitialized || !markers.match(newMarkers)) {
                prevValue = makeObject()
                markers = newMarkers
            }
            return prevValue
        }

    private lateinit var prevValue: T
    private lateinit var markers: List<Any?>

    private fun List<Any?>.match(other: List<Any?>): Boolean {
        if (size != other.size) return false
        forEachIndexed { index, item ->
            if (other[index] != item) return false
        }
        return true
    }
}
