/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.model.oas

import org.json.JSONObject
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.Yaml
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.DataTypeCode
import pl.metaprogramming.model.data.NamedDataType
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.oas.internal.REF
import pl.metaprogramming.model.oas.internal.isRef
import java.io.File
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.util.*

// Writing to OpenAPI 3.0.3
// swagger-ui does not support 3.1.0: https://github.com/swagger-api/swagger-ui/issues/5891
// https://swagger.io/specification/v2/ - swagger 2.0
// https://swagger.io/specification/v3/ - openapi 3.0.3
// https://swagger.io/specification/ - openapi 3.1.0

class OpenapiWriter(private val outputCfg: Map<RestApi, String>) {

    companion object {
        @JvmStatic
        fun write(api: RestApi, file: File) {
            write(mapOf(api to file))
        }

        @JvmStatic
        fun write(outputCfg: Map<RestApi, File>) {
            val writer = OpenapiWriter(outputCfg.mapValues { it.value.name })
            outputCfg.forEach(writer::flush)
        }
    }

    var charset: Charset = StandardCharsets.UTF_8
    var lineSeparator: String = System.lineSeparator()
    val yamlDumperOptions: DumperOptions = DumperOptions().apply {
        defaultFlowStyle = DumperOptions.FlowStyle.BLOCK
        defaultScalarStyle = DumperOptions.ScalarStyle.PLAIN
        isPrettyFlow = true
    }

    private fun flush(api: RestApi, file: File) {
        file.parentFile.mkdirs()
        file.writeText(toText(api))
    }

    fun toText(api: RestApi): String {
        val toJson = outputCfg.getValue(api).endsWith("json")
        val openApi = RestApiMapper.toOpenApi(api, outputCfg)
        val outText =
            if (toJson) openApi.prettyJson() else Yaml(yamlDumperOptions).dump(openApi)
        if (lineSeparator == System.lineSeparator()) {
            return outText
        }
        val buf = StringBuilder(outText.length + (outText.length / 10))
        outText.lines().forEach { line -> buf.append(line).append(lineSeparator) }
        return buf.toString()
    }

    private fun Map<Any, Any>.prettyJson(): String {
        return JSONObject(this).toString(2)
    }
}

private class RestApiMapper(
    private val api: RestApi,
    private val outputCfg: Map<RestApi, String>,
    private val flatten: Boolean = outputCfg.size == 1,
) {
    companion object {
        @JvmStatic
        fun toOpenApi(api: RestApi, outputCfg: Map<RestApi, String>): Map<Any, Any> {
            val mapper = RestApiMapper(api, outputCfg)
            mapper.map()
            return mapper.rootNode
        }
    }

    private val rootNode: MutableMap<Any, Any> = mutableMapOf()
    private val allApis: List<RestApi> = listOf(api) + api.dependsOn

    fun map() {
        rootNode["openapi"] = api.openapiVersion
        rootNode["info"] = api.info.toMap()
        if (api.servers.isNotEmpty()) {
            rootNode["servers"] = api.servers.map { it.toMap() }
        }
        if (api.tags.isNotEmpty()) {
            rootNode["tags"] = api.tags.map { it.toMap() }
        }
        api.operations.forEach {
            rootNode.setNode(
                "paths.${it.path.substring(api.basePath.length)}.${it.method.name.lowercase()}",
                it.toMap()
            )
        }
        getSchemas().forEach {
            rootNode.setNode("components.schemas.${it.code}", it.toMap())
        }
        getParameters().forEach {
            rootNode.setNode("components.parameters.${it.code}", it.toMap())
        }
        api.securitySchemas.forEach {
            rootNode.setNode("components.securitySchemes.${it.code}", it.toMap())
        }
    }

    private fun getParameters(restApi: RestApi = api): List<Parameter> {
        val result = restApi.parameters.toMutableList()
        if (flatten) {
            restApi.dependsOn.forEach {
                result.addAll(getParameters(it))
            }
        }
        return result.distinct()
    }

    private fun getSchemas(restApi: RestApi = api): List<DataSchema> {
        val result = restApi.schemas.toList().toMutableList()
        if (flatten) {
            restApi.dependsOn.forEach {
                result.addAll(getSchemas(it))
            }
        }
        return result.distinct()
    }

    private fun parameterRefSpec(param: Parameter): Map<Any, Any> = getParamRef(param.code)
        ?.let { mapOf(REF to it) }
        ?: param.toMap()

    private fun getParamRef(paramCode: String?): String? {
        if (paramCode == null) return null
        allApis.forEach { api ->
            if (api.parameters.any { it.code == paramCode }) {
                return "${getApiFilename(api)}#/components/parameters/$paramCode"
            }
        }
        return null
    }

    private fun getSchemaRef(schemaCode: String?): String? {
        if (schemaCode == null) return null
        allApis.forEach { api ->
            if (api.schemas.any { it.code == schemaCode }) {
                return "${getApiFilename(api)}#/components/schemas/$schemaCode"
            }
        }
        return null
    }

    private fun getApiFilename(restApi: RestApi) = if (flatten || api == restApi) "" else outputCfg.getValue(restApi)

    private fun DataSchema.toMap(): Map<Any, Any> {
        val out = mutableMapOf<Any, Any?>(
            "type" to dataTypeMap.getValue(dataType.typeCode),
            "description" to description,
            "items" to if (isArray) arrayType.itemsSchema.toRefMap() else null,
            "additionalProperties" to if (isMap) mapType.valuesSchema.toRefMap() else null,
        )
        if (isObject) {
            if (objectType.inherits.isNotEmpty()) {
                out["allOf"] = listOf(
                    mutableMapOf(REF to getSchemaRef(objectType.inherits[0].code)),
                    objectType.toMap()
                )
            } else {
                out.putAll(objectType.toMap())
            }
        } else if (isEnum) {
            out["enum"] = enumType.allowedValues
            out["x-ms-enum"] = enumType.descriptions?.let {
                mapOf("values" to it.map { v ->
                    mapOf(
                        "value" to v.key,
                        "description" to v.value,
                    )
                })
            }
        }
        return out.putAdditives(additives).removeEmpty()
    }

    private fun MutableMap<Any, Any?>.putAdditives(additives: Map<out Any, *>): MutableMap<Any, Any?> {
        additives.forEach { (k, v) ->
            if (v != null && !containsKey(k)) {
                this[k] = if (schemaNumberFields.contains(k) && v is String) {
                    if (v.contains('.')) v.toBigDecimal() else v.toLong()
                } else v
            }
        }
        return this
    }

    private fun DataSchema.toRefMap(): Map<Any, Any> =
        getSchemaRef((dataType as? NamedDataType)?.code)?.let { ref ->
            val out = mutableMapOf<Any, Any?>(REF to ref)
            if (additives.isRef()) out.putAdditives(additives)
            else out["description"] = description
            out.removeEmpty()
        } ?: toMap()

    private fun ObjectType.toMap(): MutableMap<Any, Any> = mutableMapOf<Any, Any?>(
        "type" to "object",
        "required" to fields.filter { it.required }.map { it.code },
        "properties" to fields.associate { it.code to it.toRefMap() }.ifEmpty { null }
    ).removeEmpty()

    private fun Parameter.toMap(): Map<Any, Any> = mutableMapOf<Any, Any?>(
        "name" to name,
        "in" to location.name.lowercase(Locale.getDefault()),
        "description" to description,
        "required" to required.orNull(),
        "schema" to this.toRefMap()
    ).removeEmpty()

    private fun Operation.toMap(): Map<Any, Any> = mutableMapOf<Any, Any?>(
        "operationId" to code,
        *additives.toTypedArray(),
        "parameters" to parameters.map(::parameterRefSpec),
        "requestBody" to toRequestBodyMap(),
        "responses" to toResponsesMap(),
        "security" to security.map { security ->
            mapOf(security.schema to security.scopes)
        }
    ).removeEmpty()

    private fun <K, V> Map<K, V>.toTypedArray() = this.map { it.key to it.value }.toTypedArray()

    private fun Operation.toRequestBodyMap(): Map<Any, Any?>? {
        val requestBody = this.requestBody
        if (requestBody == null || requestBody.contents.isEmpty()) {
            return null
        }
        return mutableMapOf<Any, Any?>(
            "description" to requestBody.description,
            "required" to requestBody.required.orNull(),
            "content" to requestBody.contents.mapValues {
                mapOf("schema" to it.value.toRefMap())
            }.ifEmpty { null }
        ).removeEmpty()
    }

    private fun Operation.toResponsesMap(): Map<Any, Any?> = responses.associate { res ->
        val key = if (res.isDefault) "default" else res.status
        key to mutableMapOf<Any, Any?>(
            "description" to res.description,
            "content" to res.contents.mapValues {
                mapOf("schema" to it.value.toRefMap())
            }.ifEmpty { null }
        ).removeEmpty().ifEmpty { null }
    }

    private fun SecuritySchema.toMap(): Map<Any, Any> {
        val isApiKey = type == "apiKey"
        return mutableMapOf<Any, Any?>(
            "type" to type,
            "description" to description,
            "in" to if (isApiKey) paramLocation.name.lowercase() else null,
            "name" to if (isApiKey) paramName else null,
            "flows" to oauth2Flows.associate { flow ->
                flow.type to mutableMapOf<Any, Any?>(
                    "authorizationUrl" to flow.authorizationUrl,
                    "tokenUrl" to flow.tokenUrl,
                    "scopes" to flow.scopes
                ).removeEmpty()
            }.ifEmpty { null }
        ).removeEmpty()
    }

    private fun Boolean.orNull(): Boolean? = if (!this) null else true

    @Suppress("UNCHECKED_CAST")
    private fun <K, V> MutableMap<K, V?>.removeEmpty(): MutableMap<K, V> {
        filterValues { v -> v == null || (v is List<*> && v.isEmpty()) }.keys.forEach(this::remove)
        return this as MutableMap<K, V>
    }

    private fun MutableMap<Any, Any>.setNode(nodeKey: String, nodeValue: Any?) {
        if (nodeValue == null) return
        val path = if (nodeKey.contains('.')) nodeKey.split('.') else listOf(nodeKey)
        var subNode = this
        for (i in 0 until path.size - 1) {
            val key = path[i]
            if (!subNode.containsKey(key)) {
                subNode[key] = mutableMapOf<Any, Any>()
            }
            @Suppress("UNCHECKED_CAST")
            subNode = subNode[key] as MutableMap<Any, Any>
        }
        subNode[path.last()] = nodeValue
    }

    private fun OasInfo.toMap(): Map<String, Any> = mutableMapOf(
        "title" to title,
        "version" to version,
        //"summary" to summary, // since OAS 3.1.0
        "description" to description,
        "termsOfService" to termsOfService,
        "contact" to contact?.toMap(),
        "licence" to licence?.toMap(),
    ).removeEmpty()

    private fun OasContact.toMap(): Map<String, String> = mutableMapOf(
        "name" to name,
        "url" to url,
        "email" to email
    ).removeEmpty()

    private fun OasLicence.toMap(): Map<String, String> = mutableMapOf(
        "name" to name,
        "identifier" to identifier,
        "url" to url
    ).removeEmpty()

    private fun OasTag.toMap(): Map<String, Any> = mutableMapOf(
        "name" to name,
        "description" to description,
        "externalDocs" to externalDocs?.toMap()
    ).removeEmpty()

    private fun OasExternalDocs.toMap(): Map<String, String> = mutableMapOf(
        "description" to description,
        "url" to url
    ).removeEmpty()

    private fun OasServer.toMap(): Map<String, Any> = mutableMapOf(
        "url" to url,
        "description" to description,
        "variables" to variables.mapValues { it.value.toMap() }.ifEmpty { null },
    ).removeEmpty()

    private fun OasServerVariable.toMap(): Map<String, Any> = mutableMapOf(
        "description" to description,
        "enum" to enum,
        "default" to defaultValue,
    ).removeEmpty()
}

private val dataTypeMap = mapOf(
    DataTypeCode.NUMBER to "number",
    DataTypeCode.DECIMAL to "number",
    DataTypeCode.FLOAT to "number",
    DataTypeCode.DOUBLE to "number",
    DataTypeCode.INT16 to "integer",
    DataTypeCode.INT32 to "integer",
    DataTypeCode.INT64 to "integer",
    DataTypeCode.BOOLEAN to "boolean",
    DataTypeCode.STRING to "string",
    DataTypeCode.ENUM to "string",
    DataTypeCode.DATE to "string",
    DataTypeCode.DATE_TIME to "string",
    DataTypeCode.BASE64 to "string",
    DataTypeCode.BINARY to "string",
    DataTypeCode.OBJECT to "object",
    DataTypeCode.MAP to "object",
    DataTypeCode.ARRAY to "array",
)

private val schemaNumberFields = setOf("minimum", "maximum")
