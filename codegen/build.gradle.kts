import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.dokka.DokkaConfiguration.Visibility
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile

// api
val snakeyamlVersion: String by project
val xmlschemaCoreVersion: String by project

// implementation
val jsonVersion: String by project
val freemarkerVersion: String by project
val cxfVersion: String by project
val javaxAnnotationVersion: String by project
val dokkaVersion: String by project

// tests
val junitVersion: String by project
val groovyVersion: String by project
val spockVersion: String by project
val logbackVersion: String by project
val roasterVersion: String by project
val eclipseJdtVersion: String by project

plugins {
    `codegen-lib`
    `java-test-fixtures`

    kotlin("jvm")
    id("org.jetbrains.dokka")
}

tasks.withType<KotlinJvmCompile>().configureEach {
    compilerOptions {
        freeCompilerArgs.add("-Xjvm-default=all")
        jvmTarget.set(JvmTarget.JVM_1_8)
    }
}

dependencies {
    api("org.yaml:snakeyaml:$snakeyamlVersion")
    api("org.freemarker:freemarker:$freemarkerVersion")
    api("org.apache.ws.xmlschema:xmlschema-core:$xmlschemaCoreVersion") // required to easy crate subclass of DataTypeParser<XmlSchemaSimpleType>

    implementation(kotlin("reflect"))
    implementation("org.apache.cxf:cxf-tools-wsdlto-core:$cxfVersion")
    implementation("javax.annotation:javax.annotation-api:$javaxAnnotationVersion") // required by cxf in higher java version
    implementation("org.json:json:$jsonVersion")

    testFixturesApi("org.spockframework:spock-core:$spockVersion")
    testFixturesApi("org.apache.groovy:groovy:$groovyVersion")
    testFixturesApi("ch.qos.logback:logback-classic:$logbackVersion")
    testFixturesApi("org.jboss.forge.roaster:roaster-jdt:$roasterVersion")
    testFixturesApi("org.eclipse.jdt:org.eclipse.jdt.core:$eclipseJdtVersion")
    testFixturesApi("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testFixturesApi("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
}


tasks {

    named<Jar>("javadocJar") {
        archiveClassifier.set("javadoc")
        from(named("dokkaJavadoc"))
        duplicatesStrategy = DuplicatesStrategy.INCLUDE
    }

    val apiForJava by creating(DokkaTask::class) {
        dependencies { plugins("org.jetbrains.dokka:kotlin-as-java-plugin:$dokkaVersion") }
        codegenApiDokka("java")
    }

    val apiForKotlin by creating(DokkaTask::class) { codegenApiDokka("kotlin") }

    jacocoTestReport {
        executionData(fileTree(rootDir.absolutePath).include("build/jacoco/*.exec"))
        mustRunAfter(":codegen-spring-rs2t:jacocoTestReport")
    }
}

fun DokkaTask.codegenApiDokka(lang: String) {
    dependencies {
        plugins(project(":maintenance:api-doc-generator"))
    }

    val outDir = layout.buildDirectory.dir("dokka-${lang}-api")
    outputDirectory.set(outDir)
    outputs.upToDateWhen { false }
    doFirst {
        println("Cleanup: $outDir")
        project.delete(outDir)
    }
    dokkaSourceSets {
        configureEach {
            //reportUndocumented.set(true)
        }
    }
    pluginsMapConfiguration.set(
        mapOf(
            "pl.metaprogramming.dokka.CodegenApiPlugin" to """{"lang": "$lang"}"""
        )
    )
}

tasks.withType<DokkaTask>().configureEach {
    dokkaSourceSets.configureEach {
        documentedVisibilities.set(
            setOf(
                Visibility.PUBLIC,
                Visibility.PROTECTED,
            )
        )
        perPackageOption {
            matchingRegex.set(".*internal.*")
            suppress.set(true)
        }
    }
}