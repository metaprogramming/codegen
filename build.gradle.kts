val groovyVersion: String by project
val spockVersion: String by project
val logbackVersion: String by project
val roasterVersion: String by project
val eclipseJdtVersion: String by project
val testcontainersVersion: String by project
val commonsIoVersion: String by project
val junitVersion: String by project

plugins {
    id("org.sonarqube") version "4.0.0.2929"
    id("com.diffplug.spotless-changelog")
    id("io.github.gradle-nexus.publish-plugin") version "2.0.0"
    //id("com.asarkar.gradle.build-time-tracker") version "4.3.0" - required java11

    kotlin("jvm") apply false
}

allprojects {
    repositories {
        mavenCentral()
    }
}

spotlessChangelog {
    versionSchema(pl.metaprogramming.utils.ver.CodegenVersionFunction::class.java)
}

tasks {
    register<UpdateWebsiteTask>("updateWebsite") {
        // for development, see .gitlab-ci.yml website-build
        dependsOn(
            "codegen:check", "codegen:jacocoTestReport",
//            "codegen:dokkaJavadoc", "codegen:apiForJava", // dokka incorrectly generates properties documentation for java (see notes.txt)
            "codegen:apiForKotlin")
        doLast {
            delete(
                "maintenance/website/static/kotlin-api",
                "maintenance/website/static/code-coverage"
            )
            copy {
                from("codegen/build/dokka-kotlin-api")
                into("maintenance/website/static/kotlin-api")
            }
            copy {
                from("codegen/build/reports/jacoco/test/html")
                into("maintenance/website/static/code-coverage")
            }
        }
    }
    register<SetupNextVersionTask>("setupNextVersion") {
        finalizedBy("updateDependencies", "updateWebsite")
    }
    register<SetupReleaseVersionTask>("setupReleaseVersion") {
        finalizedBy("updateDependencies", "updateWebsite")
    }
    register<UpdateDependenciesTask>("updateDependencies")

    register<SetupGradleTestProjectsTask>("setupGradleInTestProjects")
    register<SetupApiInQuickstartProjectsTask>("setupApiInQuickstartProjects")
}

sonarqube {
    properties {
        property("sonar.sources", "src/main/kotlin")
        property("sonar.language", "kotlin")
    }
}

if (project.hasProperty("sonatypeUsername")) {
    nexusPublishing {
        repositories {
            sonatype()
        }
    }
}
