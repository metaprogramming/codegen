val groovyVersion: String by project

plugins {
    `codegen-lib`
    `java-test-fixtures`
}

dependencies {
    api(project(":codegen"))
    compileOnly("org.apache.groovy:groovy:$groovyVersion")

    testFixturesApi(testFixtures(project(":codegen")))
}

tasks {
    compileGroovy {
        groovyOptions.configurationScript = file("../gradle/config/groovyc.groovy")
    }
}

