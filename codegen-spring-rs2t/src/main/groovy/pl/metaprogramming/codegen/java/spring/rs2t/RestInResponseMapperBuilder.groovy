/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.oas.HttpResponse
import pl.metaprogramming.model.oas.Operation

class RestInResponseMapperBuilder extends BaseMethodCmBuilder<Operation> {

    RestInResponseMapperBuilder(Operation operation) {
        super(operation)
    }

    @Override
    MethodCm makeDeclaration() {
        newMethodCm('map') {
            it.registerAsMapper()
            it.resultType = Spring.responseEntity()
            it.params.add(getClass(SpringRs2tTypeOfCode.RESPONSE_DTO).asField('response').addAnnotation(Java.nonnul()))
        }
    }

    @Override
    String makeImplBody() {
        codeBuf.addLines(
                'ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());',
                'response.getHeaders().forEach(responseBuilder::header);'
        )
        if (model.responses.size() == 1) {
            codeBuf.addLines("return ${prepareResponseMap(model.responses[0])};")
        } else {
            def responses = model.responses.toList().sort { -it.status }
            responses.take(responses.size() - 1).each {
                codeBuf
                        .ifBlock("response.is${it.status}()",
                                "return ${prepareResponseMap(it)};")
                        .endBlock()
            }
            codeBuf.addLines("return ${prepareResponseMap(responses.last())};")
        }
        codeBuf.take()
    }

    private String prepareResponseMap(HttpResponse response) {
        if (response.schema == null) {
            'responseBuilder.build()'
        } else {
            String responseGetter = "response.get${response.status ?: 'Other'}()"
            "responseBuilder.body(${makeTransformation(response.schema, responseGetter)})"
        }
    }

    private String makeTransformation(DataSchema schema, String fieldName) {
        if (schema.dataType.isBinary()) {
            return fieldName
        }
        mapping(schema.dataType)
                .to(SpringRs2tTypeOfCode.REST_DTO)
                .from(SpringRs2tTypeOfCode.DTO, fieldName)
                .make()
    }
}
