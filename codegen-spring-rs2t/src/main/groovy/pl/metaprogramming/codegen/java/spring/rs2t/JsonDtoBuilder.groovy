/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.AnnotationCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.builders.BaseDtoBuilder
import pl.metaprogramming.model.data.DataSchema
import pl.metaprogramming.model.data.ObjectType

class JsonDtoBuilder extends BaseDtoBuilder<ObjectType> {

    static final String CLASS_ANNOTATION = 'com.fasterxml.jackson.annotation.JsonAutoDetect'
    static final String FIELD_ANNOTATION = 'com.fasterxml.jackson.annotation.JsonProperty'
    static final AnnotationCm ANNOTATION = AnnotationCm.of(CLASS_ANNOTATION, [fieldVisibility: ValueCm.value('JsonAutoDetect.Visibility.NONE')])
    static final AnnotationCm JSON_RAW_VALUE_ANNOTATION = AnnotationCm.of('com.fasterxml.jackson.annotation.JsonRawValue')

    @Override
    void makeImplementation() {
        super.makeImplementation()
        addAnnotation(ANNOTATION)
    }

    void addField(DataSchema schema) {
        fields.add(getFieldName(schema), getClass(SpringRs2tTypeOfCode.REST_DTO, schema.dataType)) {
            it.model = schema
            it.annotations = createFieldAnnotations(schema)
            if (schema.defaultValue) {
                it.value = ValueCm.escaped(schema.defaultValue)
            }
        }
    }

    List<AnnotationCm> createFieldAnnotations(DataSchema schema) {
        def result = [AnnotationCm.of(FIELD_ANNOTATION, [value: ValueCm.escaped(schema.code)])]
        if (schema.array ? schema.arrayType.itemsSchema.dataType.isNumberOrBoolean() : schema.dataType.isNumberOrBoolean()) {
            result.add(JSON_RAW_VALUE_ANNOTATION)
        }
        if (schema.isMap() && schema.mapType.valuesSchema.dataType.isNumberOrBoolean()) {
            def serializer = getClass(SpringRs2tCommonTypeOfCode.MAP_RAW_VALUE_SERIALIZER)
            result.add(AnnotationCm.of(
                    'com.fasterxml.jackson.databind.annotation.JsonSerialize',
                    ['using': serializer.classRef()])
            )
        }
        result
    }
}
