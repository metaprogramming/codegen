/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.*
import pl.metaprogramming.codegen.java.base.PackageNameBuilder
import pl.metaprogramming.codegen.java.builders.EnumBuilder
import pl.metaprogramming.codegen.java.builders.InterfaceBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Lombok
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.rs2t.*
import pl.metaprogramming.codegen.java.validation.*
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.EnumType
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.oas.HttpRequestSchema
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.model.oas.RestApi

import java.util.function.Function

class SpringRestService2tGenerator extends JavaGeneratorBuilder<RestApi, TOC, SpringRestService2tGenerator> {

    static class TOC {
        static final TypeOfCode<Operation> REST_CONTROLLER = SpringRs2tTypeOfCode.REST_CONTROLLER
        static final TypeOfCode<List<Operation>> REST_CONTROLLER_MO = SpringRs2tTypeOfCode.REST_CONTROLLER_MO
        static final TypeOfCode<List<Operation>> FACADE = SpringRs2tTypeOfCode.FACADE
        static final TypeOfCode<List<Operation>> FACADE_IMPL = SpringRs2tTypeOfCode.FACADE_IMPL
        static final TypeOfCode<ObjectType> REST_DTO_VALIDATOR = ValidationTypeOfCode.DTO_VALIDATOR
        static final TypeOfCode<HttpRequestSchema> REST_REQUEST_VALIDATOR = ValidationTypeOfCode.REQUEST_VALIDATOR
        static final TypeOfCode<String> SECURITY_CHECKER = ValidationTypeOfCode.SECURITY_CHECKER
        static final TypeOfCode<ObjectType> DTO = SpringRs2tTypeOfCode.DTO
        static final TypeOfCode<ObjectType> REST_DTO = SpringRs2tTypeOfCode.REST_DTO
        static final TypeOfCode<EnumType> ENUM = SpringRs2tTypeOfCode.ENUM
        static final TypeOfCode<HttpRequestSchema> REST_REQUEST_DTO = SpringRs2tTypeOfCode.REST_REQUEST_DTO
        static final TypeOfCode<HttpRequestSchema> REQUEST_DTO = SpringRs2tTypeOfCode.REQUEST_DTO
        static final TypeOfCode<Operation> RESPONSE_DTO = SpringRs2tTypeOfCode.RESPONSE_DTO
        static final TypeOfCode<ObjectType> REST_MAPPER = SpringRs2tTypeOfCode.REST_MAPPER
        static final TypeOfCode<HttpRequestSchema> REQUEST_MAPPER = SpringRs2tTypeOfCode.REQUEST_MAPPER
        static final TypeOfCode<Operation> RESPONSE_MAPPER = SpringRs2tTypeOfCode.RESPONSE_MAPPER
    }

    class Generator extends JavaGenerator<RestApi> {
        List typeOfCodesForOperation

        Generator() {
            super(SpringRestService2tGenerator.this)
            typeOfCodesForOperation = SpringRestService2tGenerator.this.typeOfCodesForOperation
        }

        @Override
        void generate() {
            addSecurityCheckers()
            addObjectParamsCodes(model.parameters)
            addSchemaCodes(model)
            addOperationCodes(model)
            makeCodeModels()
        }

        void addSecurityCheckers() {
            if (!validationParams.securityValidation) return
            def securityParams = []
            model.securitySchemas.each {
                if (!securityParams.contains(it.paramName)) {
                    securityParams.add(it.paramName)
                    register(TOC.SECURITY_CHECKER, it.paramName)
                }
            }
        }

        void addObjectParamsCodes(List<Parameter> parameters) {
            parameters.findAll { it.isObject() }.each { paramDef ->
                registerExternalClass(TOC.REST_DTO, paramDef.dataType, Java.string())
                register(TOC.DTO, paramDef)
                register(TOC.REST_MAPPER, paramDef)
            }
        }

        void addSchemaCodes(RestApi model) {
            model.schemas.each { schema ->
                if (schema.isEnum()) {
                    register(TOC.ENUM, schema)
                }
                if (schema.isObject()) {
                    register(TOC.DTO, schema)
                    register(TOC.REST_DTO, schema)
                    register(TOC.REST_MAPPER, schema)
                    register(TOC.REST_DTO_VALIDATOR, schema)
                }
            }
        }

        void addOperationCodes(RestApi model) {
            model.groupedOperations.each { group, operations ->
                operations.each { operation ->
                    register(TOC.REST_REQUEST_DTO, operation.requestSchema)
                    register(TOC.REQUEST_DTO, operation.requestSchema)
                    register(TOC.REQUEST_MAPPER, operation.requestSchema)
                    register(TOC.REST_REQUEST_VALIDATOR, operation.requestSchema)
                    operationClassTypes().each { classType ->
                        register(classType, operation)
                    }
                }
                resourceClassTypes().each { toc -> register(toc, operations) }
            }
        }

        @TypeChecked(TypeCheckingMode.SKIP)
        private ValidationParams getValidationParams() {
            params.get(ValidationParams)
        }

        @TypeChecked(TypeCheckingMode.SKIP)
        private boolean isMultiOperationControllerStrategy() {
            !params.get(SpringRestParams).controllerPerOperation
        }

        private List<TypeOfCode<Operation>> operationClassTypes() {
            typeOfCodesForOperation + (isMultiOperationControllerStrategy() ? [] : [TOC.REST_CONTROLLER])
        }

        private List<TypeOfCode<List<Operation>>> resourceClassTypes() {
            def result = [TOC.FACADE, TOC.FACADE_IMPL]
            if (isMultiOperationControllerStrategy()) {
                result.add(TOC.REST_CONTROLLER_MO)
            }
            result
        }
    }

    SpringRestService2tGenerator() {
        super(new TOC())
    }

    @Override
    pl.metaprogramming.codegen.Generator<RestApi> make() {
        params.get(ValidationParams).formatValidators
                .setIfNotPresent('date', 'field:VALIDATION_COMMON_CHECKERS.ISO_DATE')
                .setIfNotPresent('date-time', 'field:VALIDATION_COMMON_CHECKERS.ISO_DATE_TIME')

        return new Generator()
    }

    private static String PKG_DTOS_TAIL = "ports.in.rest.dtos"
    private static String PKG_ADAPTER_TAIL = "adapters.in.rest"
    private static String PKG_REST_DTOS_TAIL = "${PKG_ADAPTER_TAIL}.dtos"
    private static String PKG_REST_MAPPERS_TAIL = "${PKG_ADAPTER_TAIL}.mappers"
    private static String PKG_VALIDATORS_TAIL = "${PKG_ADAPTER_TAIL}.validators"

    List<Object> typeOfCodesForOperation = [TOC.RESPONSE_DTO, TOC.RESPONSE_MAPPER] as List<Object>

    void init() {
        def plainDataTypeMapper = new DataTypeMapper({ it ->
            it.isBinary() ? Spring.resource() : !it.isComplex() ? Java.string() : null
        } as Function<DataType, ClassCd>)

        [TOC.REST_DTO, TOC.REST_REQUEST_DTO, TOC.REST_CONTROLLER_MO].forEach {
            dataTypeMapper.set(it, plainDataTypeMapper)
        }

        typeOfCode(TOC.REST_REQUEST_VALIDATOR) {
            it.className.suffix = 'Validator'
            it.className.resolver { it.operation.code }
            it.packageName.infix { req -> makePackageName("validators", req.operation) }
            it.builders.add(new RestRequestValidatorBuilder(TOC.REST_REQUEST_DTO, TOC.ENUM, TOC.REST_DTO_VALIDATOR, true))
            it.addManagedComponentBuilder()
        }
        typeOfCode(TOC.REST_DTO_VALIDATOR) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Validator'
            }
            it.packageName.tail = PKG_VALIDATORS_TAIL
            it.builders.add(new RestDtoValidatorBuilder(TOC.REST_DTO, TOC.REST_DTO_VALIDATOR, TOC.ENUM, true))
            it.addManagedComponentBuilder()
        }

        typeOfCode(TOC.ENUM) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Enum'
            }
            it.packageName.tail = PKG_DTOS_TAIL
            it.builders.add(new EnumBuilder())
        }
        typeOfCode(TOC.RESPONSE_DTO) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Response'
            }
            it.packageName.tail = PKG_DTOS_TAIL
            it.builders.add(new RestResponseBuilder())
        }
        typeOfCode(TOC.RESPONSE_MAPPER)
                .className {
                    it.resolver { it.code }
                    it.suffix = 'ResponseMapper'
                }
                .packageName {
                    it.infix { operation -> makePackageName("mappers", operation) }
                }
                .addManagedComponentBuilder()
                .onDeclaration { it.methods.add(new RestInResponseMapperBuilder(it.model as Operation)) }

        typeOfCode(TOC.DTO) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Dto'
            }
            it.packageName.tail = PKG_DTOS_TAIL
            it.builders.add(new DtoBuilder<ObjectType>())
            it.builders.add(Lombok.dataBuilder())
        }
        typeOfCode(TOC.REST_DTO) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Rdto'
            }
            it.packageName.tail = PKG_REST_DTOS_TAIL
            it.builders.add(new JsonDtoBuilder())
            it.builders.add(Lombok.dataBuilder())
        }
        typeOfCode(TOC.REST_MAPPER) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Mapper'
            }
            it.packageName.tail = PKG_REST_MAPPERS_TAIL
            it.builders.add(new RestDtoMapperBuilder())
            it.addManagedComponentBuilder()
        }

        typeOfCode(TOC.REQUEST_DTO) {
            it.className {
                it.resolver { it.operation.code }
                it.suffix = 'Request'
            }
            it.packageName.tail = PKG_DTOS_TAIL
            it.builders.add(new DtoBuilder<HttpRequestSchema>({ it.schema }))
            it.builders.add(Lombok.dataBuilder())
        }
        typeOfCode(TOC.REST_REQUEST_DTO) {
            it.className {
                it.resolver { it.operation.code }
                it.suffix = 'Rrequest'
            }
            it.packageName.tail = PKG_REST_DTOS_TAIL
            it.builders.add(new PayloadFieldAppenderBuilder())
            it.builders.add(new RawRequestBuilder())
            it.builders.add(Lombok.dataBuilder())
        }
        typeOfCode(TOC.REQUEST_MAPPER) {
            it.className {
                it.resolver { it.operation.code }
                it.suffix = 'RequestMapper'
            }
            it.packageName.tail = PKG_REST_MAPPERS_TAIL
            it.builders.add(new RestInRequestMapperBuilder())
            it.addManagedComponentBuilder()
        }

        typeOfCode(TOC.REST_CONTROLLER) {
            it.forceGeneration = true
            it.className {
                it.resolver { it.code }
                it.suffix = 'Controller'
            }
            it.packageName.infix { operation -> makePackageName(null, operation) }
            it.onImplementation { it.methods.add(new RestControllerMethodBuilder(it.model as Operation)) }
            it.onDecoration { it.addAnnotation(Spring.restController()) }
            it.addDependencyInjectionBuilder()
        }
        typeOfCode(TOC.REST_CONTROLLER_MO) {
            it.forceGeneration = true
            it.className {
                it.resolver { it[0].group }
                it.suffix = 'Controller'
            }
            it.packageName.tail = PKG_ADAPTER_TAIL
            it.builders.add(new RestControllerMoBuilder())
            it.addDependencyInjectionBuilder()
        }
        typeOfCode(TOC.FACADE) {
            it.forceGeneration = true
            it.className {
                it.resolver { it[0].group }
                it.suffix = 'Facade'
            }
            it.packageName.tail = "ports.in.rest"
            it.builders.add(new InterfaceBuilder<List<Operation>>(TOC.FACADE_IMPL))
        }
        typeOfCode(TOC.FACADE_IMPL) {
            it.forceGeneration = true
            it.className {
                it.resolver { it[0].group }
                it.suffix = 'FacadeImpl'
            }
            it.packageName.tail = "application"
            it.builders.add(new FacadeBuilder())
            it.addManagedComponentBuilder()
        }

        typeOfCode(TOC.SECURITY_CHECKER) {
            it.className {
                it.resolver { it }
                it.prefix = 'Security'
                it.suffix = 'Checker'
            }
            it.packageName {
                it.tail = "${PKG_VALIDATORS_TAIL}.custom"
            }
            it.forceGeneration(true)
            it.builders.add(new SecurityCheckerBuilder())
            it.addManagedComponentBuilder()
        }

        codeIndex.putMapper("${Spring.multipartFile().canonicalName}.getResource", Spring.resource())
    }

    private String makePackageName(String type, Operation operation) {
        def tail = params.get(SpringRestParams).controllerPerOperation
                ? params.get(JavaParams).nameMapper.toPackagePart(operation.group)
                : type
        PackageNameBuilder.of(PKG_ADAPTER_TAIL, tail)
    }

}
