/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.model.oas.HttpRequestSchema

import static pl.metaprogramming.codegen.java.libs.Java.string

class PayloadFieldAppenderBuilder extends ClassCmBuilderTemplate<HttpRequestSchema> {

    @Override
    @TypeChecked(TypeCheckingMode.SKIP)
    void makeImplementation() {
        if (params.get(SpringRestParams).payloadField && model.schema) {
            fields.add(params.get(SpringRestParams).payloadField, string())
        }
    }

}
