/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.SpringCommonTypeOfCode
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.ParamLocation
import pl.metaprogramming.model.oas.Parameter

class RestOutRequestMapperBuilder extends BaseMethodCmBuilder<Operation> {

    private static final String REQUEST_PARAM = 'request'
    private static final String RAW_REQUEST_VAR = 'rawRequest'
    private static final String URI_VAR = 'uri'

    @Lazy
    protected RestClientBuildHelper helper = new RestClientBuildHelper(context, model)

    RestOutRequestMapperBuilder(Operation operation) {
        super(operation)
    }

    @Override
    MethodCm makeDeclaration() {
        newMethodCm('map') {
            it.registerAsMapper()
            it.resultType = helper.requestEntity
            it.params.add(getClass(SpringRs2tTypeOfCode.REQUEST_DTO, model.requestSchema).asField(REQUEST_PARAM).addAnnotation(Java.nonnul()))
        }
    }

    @Override
    String makeImplBody() {
        makeRawRequestVar()
        makeUriVar()
        def headersVar = makeHeadersVar()
        def bodyVar = makeBodyVar()
        codeBuf.addLines("return RequestEntity.${model.method.name().toLowerCase()}($URI_VAR)")
        codeBuf.indentInc(2)
        if (headersVar) {
            codeBuf.addLines(".headers($headersVar)")
        }
        if (hasRequestBody()) {
            if (!(model.consumeJson || model.multipart)) {
                codeBuf.addLines(".header(\"content-type\", \"${model.consumes.first()}\")")
            }
            codeBuf.addLines(".body($bodyVar);")
        } else {
            codeBuf.addLines('.build();')
        }
        codeBuf.indentInc(-2)
        codeBuf.take()
    }

    private void makeRawRequestVar() {
        addVarDeclaration(mapping(model.requestSchema)
                .to(SpringRs2tTypeOfCode.REST_REQUEST_DTO)
                .from(methodCm.params)
                .makeField(RAW_REQUEST_VAR))
    }

    private void makeUriVar() {
        dependencies
                .add('java.net.URI')
                .add('org.springframework.web.util.UriComponentsBuilder')
        def pathParams = makePathParamsVar()
        codeBuf
                .addLines("URI $URI_VAR = UriComponentsBuilder")
                .indentInc(2)
                .addLines(".fromUriString(${callComponent(SpringCommonTypeOfCode.ENDPOINT_PROVIDER, 'getEndpoint("' + model.path + '")')})")
                .addLines(model.getParameters(ParamLocation.QUERY).collect {
                    ".queryParam${paramNameValue(it)}".toString()
                } as List<String>)
        if (pathParams) {
            codeBuf.addLines(".build($pathParams);")
        } else {
            codeBuf.addLines('.build()', '.toUri();')
        }
        codeBuf.indentInc(-2)
    }

    private String makePathParamsVar() {
        def pathParams = model.getParameters(ParamLocation.PATH)
        if (pathParams.empty) {
            return ''
        }
        dependencies.add('java.util.Map').add('java.util.HashMap')
        codeBuf.addLines("Map<String, String> pathParams = new HashMap<>();")
        codeBuf.addLines(pathParams.collect {
            "pathParams.put${paramNameValue(it)};".toString()
        } as List<String>)
        "pathParams"
    }

    private String paramNameValue(Parameter parameter) {
        "(\"$parameter.name\", ${getter(RAW_REQUEST_VAR, parameter.name)})"
    }

    private String makeBodyVar() {
        model.multipart ? makeMultipartBodyVar()
                : model.requestBodySchema ? getter(RAW_REQUEST_VAR, model.requestBodySchema.code) : null
    }

    private String makeMultipartBodyVar() {
        dependencies.add(Spring.multiValueMap()).add(Spring.linkedMultiValueMap())
        String var = 'body'
        codeBuf.addLines("MultiValueMap<String, Object> $var = new LinkedMultiValueMap<>();")
        codeBuf.addLines(model.getParameters(ParamLocation.FORMDATA).collect {
            "${var}.add${paramNameValue(it)};".toString()
        } as List<String>)
        codeBuf.addLines(model.multipartFormDataRequestBody?.fields?.collect {
            "${var}.add(\"$it.code\", ${getter(RAW_REQUEST_VAR, it.code)});".toString()
        } as List<String>)
        var
    }

    private String makeHeadersVar() {
        def headers = model.getParameters(ParamLocation.HEADER)
        if (headers.empty) {
            return null
        }
        String var = 'headers'
        addVarDeclaration(var, Spring.httpHeaders(), 'new')
        codeBuf.addLines(headers.collect {
            "${ofNullable(paramGetter(RAW_REQUEST_VAR, it))}.ifPresent(v -> ${var}.add(\"$it.name\", v));".toString()
        } as List<String>)
        var
    }

    private String paramGetter(String varName, Parameter param) {
        getter(varName, context.getFieldName(param))
    }

    private boolean hasRequestBody() {
        model.requestBodySchema || model.multipart
    }
}
