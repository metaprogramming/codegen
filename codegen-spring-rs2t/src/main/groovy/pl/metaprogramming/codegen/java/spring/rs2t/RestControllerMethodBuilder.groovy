/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.rs.RestParamsBuilder
import pl.metaprogramming.model.oas.Operation

import static pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tTypeOfCode.*
import static pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_RESULT

class RestControllerMethodBuilder extends BaseMethodCmBuilder<Operation> {

    RestControllerMethodBuilder(Operation operation) {
        super(operation)
    }

    @Override
    MethodCm makeDeclaration() {
        new RestParamsBuilder(context, REST_DTO).makeControllerMethod(model)
    }

    @Override
    String makeImplBody() {
        def request = mapping(model.requestSchema).to(REST_REQUEST_DTO).from(methodCm.params).makeField('request')
        def validationResult = mapping().to(VALIDATION_RESULT).from(request).makeField('validationResult')
        def serviceCall = mapping().to(RESPONSE_DTO).from(mapping(model.requestSchema).to(REQUEST_DTO).from(request)).makeField(null)
        addVarDeclaration(request)
        addVarDeclaration(validationResult)
        codeBuf.ifElseValue("return ${validationResult.name}.isValid()",
                mapping().to(Spring.responseEntity()).from(serviceCall).make(),
                "${mapping().to(Spring.responseEntity()).from(validationResult)};")
                .take()
    }
}
