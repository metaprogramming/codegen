/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring


import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.DataTypeMapper
import pl.metaprogramming.codegen.java.JavaGenerator
import pl.metaprogramming.codegen.java.JavaGeneratorBuilder
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.builders.EnumBuilder
import pl.metaprogramming.codegen.java.builders.InterfaceBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Lombok
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.rs2t.*
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.data.EnumType
import pl.metaprogramming.model.data.ObjectType
import pl.metaprogramming.model.oas.HttpRequestSchema
import pl.metaprogramming.model.oas.Operation
import pl.metaprogramming.model.oas.Parameter
import pl.metaprogramming.model.oas.RestApi

import java.util.function.Function

class SpringRestClient2tGenerator extends JavaGeneratorBuilder<RestApi, TOC, SpringRestClient2tGenerator> {

    static class TOC {
        static final TypeOfCode<List<Operation>> REST_CLIENT = SpringRs2tTypeOfCode.REST_CLIENT
        static final TypeOfCode<List<Operation>> REST_CLIENT_IMPL = SpringRs2tTypeOfCode.REST_CLIENT_IMPL
        static final TypeOfCode<ObjectType> DTO = SpringRs2tTypeOfCode.DTO
        static final TypeOfCode<ObjectType> REST_DTO = SpringRs2tTypeOfCode.REST_DTO
        static final TypeOfCode<EnumType> ENUM = SpringRs2tTypeOfCode.ENUM
        static final TypeOfCode<HttpRequestSchema> REST_REQUEST_DTO = SpringRs2tTypeOfCode.REST_REQUEST_DTO
        static final TypeOfCode<HttpRequestSchema> REQUEST_DTO = SpringRs2tTypeOfCode.REQUEST_DTO
        static final TypeOfCode<Operation> RESPONSE_DTO = SpringRs2tTypeOfCode.RESPONSE_DTO
        static final TypeOfCode<ObjectType> REST_MAPPER = SpringRs2tTypeOfCode.REST_MAPPER
        static final TypeOfCode<HttpRequestSchema> REQUEST_MAPPER = SpringRs2tTypeOfCode.REQUEST_MAPPER
        static final TypeOfCode<Operation> RESPONSE_MAPPER = SpringRs2tTypeOfCode.RESPONSE_MAPPER
    }

    SpringRestClient2tGenerator() {
        super(new TOC())
    }

    @Override
    pl.metaprogramming.codegen.Generator<RestApi> make() {
        dataTypeMapper.set(DataType.BINARY, Spring.resource())
        return new Generator()
    }

    class Generator extends JavaGenerator<RestApi> {

        Generator() {
            super(SpringRestClient2tGenerator.this)
        }

        @Override
        void generate() {
            addObjectParamsCodes(model.parameters)
            addSchemaCodes(model)
            addOperationCodes(model)
            makeCodeModels()
        }

        void addObjectParamsCodes(List<Parameter> parameters) {
            parameters.findAll { it.isObject() }.each { paramDef ->
                registerExternalClass(TOC.REST_DTO, paramDef.dataType, Java.string())
                register(TOC.DTO, paramDef)
                register(TOC.REST_MAPPER, paramDef)
            }
        }

        void addSchemaCodes(RestApi model) {
            model.schemas.each { schema ->
                if (schema.isEnum()) {
                    register(TOC.ENUM, schema)
                }
                if (schema.isObject()) {
                    register(TOC.DTO, schema)
                    register(TOC.REST_DTO, schema)
                    register(TOC.REST_MAPPER, schema)
                }
            }
        }

        void addOperationCodes(RestApi model) {
            model.operations.forEach { operation ->
                register(TOC.REST_REQUEST_DTO, operation.requestSchema)
                register(TOC.REQUEST_MAPPER, operation.requestSchema)
                register(TOC.REQUEST_DTO, operation.requestSchema)
                register(TOC.RESPONSE_DTO, operation)
                register(TOC.RESPONSE_MAPPER, operation)
            }
            model.groupedOperations.values().forEach { operations ->
                register(TOC.REST_CLIENT, operations)
                register(TOC.REST_CLIENT_IMPL, operations)
            }
        }
    }

    String adapterPackageName = "rest"

    @Override
    void init() {
        typeOfCode(TOC.ENUM) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Enum'
            }
            it.packageName.infix { _ -> dtoPackageTail }
            it.builders.add(new EnumBuilder())
        }
        typeOfCode(TOC.RESPONSE_DTO) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Response'
            }
            it.packageName.infix { _ -> dtoPackageTail }
            it.builders.add(new RestResponseBuilder())
        }
        typeOfCode(TOC.RESPONSE_MAPPER) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'ResponseMapper'
            }
            it.packageName.infix { _ -> mappersPackageTail }
            it.addManagedComponentBuilder()
            it.onDeclaration { ClassCmBuilderTemplate<Operation> builder ->
                builder.methods.add(new RestOutResponseSuccessMapperBuilder(builder.model))
                builder.methods.add(new RestOutResponseFailMapperBuilder(builder.model))
            }
        }
        typeOfCode(TOC.DTO) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Dto'
            }
            it.packageName.infix { _ -> dtoPackageTail }
            it.builders.add(new DtoBuilder<ObjectType>())
            it.builders.add(Lombok.dataBuilder())
        }
        typeOfCode(TOC.REST_DTO) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Rdto'
            }
            it.packageName.infix { _ -> rawDtoPackageTail }
            it.builders.add(new JsonDtoBuilder())
            it.builders.add(Lombok.dataBuilder())
        }
        typeOfCode(TOC.REST_MAPPER) {
            it.className {
                it.resolver { it.code }
                it.suffix = 'Mapper'
            }
            it.packageName.infix { _ -> mappersPackageTail }
            it.addManagedComponentBuilder()
            it.builders.add(new RestDtoMapperBuilder())
        }

        typeOfCode(TOC.REQUEST_DTO) {
            it.className {
                it.resolver { it.operation.code }
                it.suffix = 'Request'
            }
            it.packageName.infix { _ -> dtoPackageTail }
            it.builders.add(new DtoBuilder<HttpRequestSchema>({ it.schema }))
            it.builders.add(Lombok.dataBuilder())
        }
        typeOfCode(TOC.REST_REQUEST_DTO) {
            it.className {
                it.resolver { it.operation.code }
                it.suffix = 'Rrequest'
            }
            it.packageName.infix { _ -> rawDtoPackageTail }
            it.builders.add(new PayloadFieldAppenderBuilder())
            it.builders.add(new RawRequestBuilder())
            it.builders.add(Lombok.dataBuilder())
        }
        typeOfCode(TOC.REQUEST_MAPPER) {
            it.className {
                it.resolver { it.operation.code }
                it.suffix = 'RequestMapper'
            }
            it.packageName.infix { _ -> mappersPackageTail }
            it.onDeclaration { ClassCmBuilderTemplate<HttpRequestSchema> builder ->
                builder.methods.add(new RestOutRequestMapperBuilder(builder.model.operation))
                builder.methods.add(new DtoMapperBuilder(builder.model.schema)
                        .setIsPrivate(true)
                        .setFrom(builder.classLocator(SpringRs2tTypeOfCode.REQUEST_DTO).getDeclared())
                        .setTo(builder.classLocator(SpringRs2tTypeOfCode.REST_REQUEST_DTO).getDeclared()))
            }
            it.addManagedComponentBuilder()
        }

        typeOfCode(TOC.REST_CLIENT) {
            it.forceGeneration = true
            it.className {
                it.resolver { it[0].group }
                it.suffix = 'Client'
            }
            it.packageName.infix { _ -> "ports.out.${adapterPackageName}" }
            it.builders.add(new InterfaceBuilder<List<Operation>>(TOC.REST_CLIENT_IMPL))
        }
        typeOfCode(TOC.REST_CLIENT_IMPL) {
            it.forceGeneration = true
            it.className {
                it.resolver { it[0].group }
                it.suffix = 'ClientImpl'
            }
            it.packageName.infix { _ -> "adapters.out.${adapterPackageName}" }
            it.builders.add(new RestClientImplBuilder())
            it.addManagedComponentBuilder()
        }
        def plainDataTypeMapper = new DataTypeMapper({ it ->
            it.isBinary() ? Spring.resource() : !it.isComplex() ? Java.string() : null
        } as Function<DataType, ClassCd>)
        [TOC.REST_DTO, TOC.REST_REQUEST_DTO].forEach {
            dataTypeMapper.set(it, plainDataTypeMapper)
        }
    }

    private String getDtoPackageTail() {
        "ports.out.${adapterPackageName}.dtos"
    }

    private String getRawDtoPackageTail() {
        "adapters.out.${adapterPackageName}.dtos"
    }

    private String getMappersPackageTail() {
        "adapters.out.${adapterPackageName}.mappers"
    }

}
