/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.spring.rs.RestParamsBuilder
import pl.metaprogramming.model.oas.HttpRequestSchema

import static pl.metaprogramming.codegen.java.spring.rs2t.SpringRs2tTypeOfCode.*

class RestInRequestMapperBuilder extends ClassCmBuilderTemplate<HttpRequestSchema> {

    @Override
    void makeDeclaration() {
        methods.add(new DtoMapperBuilder(model.schema)
                .setFromFields(new RestParamsBuilder(context, REST_DTO).make(model.operation))
                .setTo(classLocator(REST_REQUEST_DTO).getDeclared()))

        methods.add(new DtoMapperBuilder(model.schema)
                .setHandleNullParam(true)
                .setFrom(classLocator(REST_REQUEST_DTO).getDeclared())
                .setTo(classLocator(REQUEST_DTO).getDeclared()))
    }
}
