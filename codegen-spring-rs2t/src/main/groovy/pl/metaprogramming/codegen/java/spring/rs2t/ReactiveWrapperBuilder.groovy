/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.formatter.CodeBuffer
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.model.oas.Operation

class ReactiveWrapperBuilder extends ClassCmBuilderTemplate<Operation> {

    static final ClassCd MONO_CLASS = ClassCd.of('reactor.core.publisher.Mono')
    static final ClassCd SCHEDULER_CLASS = ClassCd.of('reactor.core.scheduler.Scheduler')

    @Override
    void makeImplementation() {
        def schedulerField = fields.add('scheduler', SCHEDULER_CLASS) {
            it.final = true
            it.value = ValueCm.staticRef("reactor.core.scheduler.Schedulers.newElastic(\"${model.code}\")")
        }
        methods.each {
            it.resultType = MONO_CLASS.withGeneric(it.resultType)
            it.implBody = new CodeBuffer()
                    .newLine('return Mono.fromSupplier(() -> {')
                    .indent(1)
                    .addLines(it.implBody)
                    .newLine("}).subscribeOn(${schedulerField.name});")
                    .take()
        }
    }
}
