/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.base.BuildContext
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.model.oas.HttpResponse
import pl.metaprogramming.model.oas.Operation

import static pl.metaprogramming.codegen.java.libs.Java.*

class RestClientBuildHelper {
    BuildContext<?> builder
    Operation operation

    RestClientBuildHelper(BuildContext<?> builder, Operation operation) {
        this.builder = builder
        this.operation = operation
    }

    ClassCd getRequestEntity() {
        Spring.requestEntity(getHttpEntityBodyClass())
    }

    ClassCd getHttpEntityBodyClass() {
        if (operation.multipart) {
            Spring.multiValueMap(string(), objectType())
        } else if (operation.requestBodySchema) {
            builder.getClass(SpringRs2tTypeOfCode.REST_DTO, operation.requestBodySchema.dataType)
        } else {
            voidType()
        }
    }

    HttpResponse getSuccessResponse() {
        if (operation.responses.size() == 1 && operation.responses.first().isDefault()) {
            operation.responses.first()
        } else {
            operation.successResponse
        }
    }

    boolean hasResponseBody() {
        successResponse?.schema?.dataType != null
    }

    DataType getResponseObject() {
        successResponse?.schema?.dataType
    }

    ClassCd getResponseClass() {
        hasResponseBody() ? builder.getClass(SpringRs2tTypeOfCode.REST_DTO, responseObject) : string()
    }

    ClassCd getResponseEntity() {
        Spring.responseEntity(hasResponseBody() ? responseClass : genericParamUnknown())
    }
}
