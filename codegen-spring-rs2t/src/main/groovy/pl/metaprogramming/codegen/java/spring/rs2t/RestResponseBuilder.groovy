/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode
import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.ValueCm
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.model.oas.HttpResponse
import pl.metaprogramming.model.oas.Operation

class RestResponseBuilder extends ClassCmBuilderTemplate<Operation> {
    static final ClassCd COLLECTIONS = ClassCd.of('java.util.Collections')

    @Override
    void makeImplementation() {
        setSuperClass(getClass(SpringRs2tCommonTypeOfCode.REST_RESPONSE_ABSTRACT).withGeneric(classCm))
        if (isStaticFactoryMethod()) {
            addAnnotation(Java.parametersAreNonnullByDefault())
            addConstructor()
        }
        addGetDeclaredStatuses()
        methods.add('self') {
            it.resultType = classCm
            it.implBody = 'return this;'
        }

        model.responses.each {
            def responseType = it.schema != null
                    ? SpringRs2tCommonTypeOfCode.REST_RESPONSE_STATUS_INTERFACE
                    : SpringRs2tCommonTypeOfCode.REST_RESPONSE_STATUS_NO_CONTENT_INTERFACE
            def genericParams = [classCm] as List<ClassCd>
            if (it.schema) {
                genericParams.add(getResponseClass(it))
            }
            implementationOf(getClass(responseType, it.status).withGeneric(genericParams))
            addFactoryMethod(it)
        }
    }

    private void addConstructor() {
        methods.add(classCm.className) {
            it.private = true
            it.resultType = classCm
            it.params.add('status', Java.boxedInteger())
            it.params.add('body', Java.objectType())
            it.implBody = 'super(status, body);'
        }
    }

    private void addGetDeclaredStatuses() {
        def field = fields.add('DECLARED_STATUSES', Java.boxedInteger().asCollection()) {
            it.static = true
            it.final = true
            it.setValue(getDeclaredResponsesValue())
        }
        methods.add('getDeclaredStatuses') {
            it.resultType = field.type
            it.implBody = 'return DECLARED_STATUSES;'
        }
    }

    private ValueCm getDeclaredResponsesValue() {
        def statuses = model.responses.findAll { !it.default }.collect { it.status }
        if (statuses.empty) {
            return ValueCm.value('Collections.emptyList()').dependsOn(COLLECTIONS)
        }
        if (statuses.size() == 1) {
            return ValueCm.value("Collections.singletonList(${statuses[0]})").dependsOn(COLLECTIONS)
        }
        return ValueCm.value("Arrays.asList(${statuses.join(', ')})").dependsOn(ClassCd.of('java.util.Arrays'))
    }

    private void addFactoryMethod(HttpResponse response) {
        if (!isStaticFactoryMethod()) {
            return
        }
        methods.add('set' + (response.isDefault() ? 'Other' : response.status)) {
            it.static = true
            it.resultType = classCm
            it.params.addAll(getFactoryMethodParams(response))
            it.implBody = getFactoryMethodImpl(response)
        }
    }

    private List<FieldCm> getFactoryMethodParams(HttpResponse response) {
        List<FieldCm> params = []
        if (response.isDefault()) {
            params.add(Java.boxedInteger().asField('status'))
        }
        if (response.schema != null) {
            params.add(getResponseClass(response).asField('body'))
        }
        params
    }

    private String getFactoryMethodImpl(HttpResponse response) {
        String status = response.isDefault() ? 'status' : '' + response.status
        String body = response.schema != null ? 'body' : 'null'
        if (response.isDefault()) {
            codeBuf.ifBlock("DECLARED_STATUSES.contains(status)",
                    'throw new IllegalArgumentException(String.format("Status %s is declared. Use dedicated factory method for it.", status));'
            ).endBlock()

        }
        codeBuf.addLines("return new ${classCm.className}($status, $body);")
                .take()
    }

    protected ClassCd getResponseClass(HttpResponse model) {
        if (model.schema) {
            def typeOfCode = model.schema.enumOrItemEnum ? SpringRs2tTypeOfCode.ENUM : SpringRs2tTypeOfCode.DTO
            getClass(typeOfCode, model.schema.dataType)
        } else {
            null
        }
    }

    @TypeChecked(TypeCheckingMode.SKIP)
    private boolean isStaticFactoryMethod() {
        params.get(SpringRestParams).staticFactoryMethodForRestResponse
    }
}
