/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.FieldCm
import pl.metaprogramming.codegen.java.MethodCm
import pl.metaprogramming.codegen.java.base.BaseMethodCmBuilder
import pl.metaprogramming.codegen.java.libs.Java
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.model.oas.Operation

class RestOutInvokeMethodBuilder extends BaseMethodCmBuilder<Operation> {

    private static final String REQUEST_PARAM = 'request'
    private static final FieldCm REST_TEMPLATE_FIELD = Spring.restTemplate().asField('restTemplate')

    @Lazy
    private RestClientBuildHelper helper = new RestClientBuildHelper(context, model)

    RestOutInvokeMethodBuilder(Operation operation) {
        super(operation)
    }

    @Override
    MethodCm makeDeclaration() {
        newMethodCm(nameMapper.toMethodName(model.code)) {
            it.setResultType(getClass(SpringRs2tTypeOfCode.RESPONSE_DTO))
            it.params.add(getClass(SpringRs2tTypeOfCode.REQUEST_DTO, model.requestSchema)
                    .asField(REQUEST_PARAM)
                    .addAnnotation(Java.nonnul()))
            it.setDescription(model.description)
        }
    }

    @Override
    String makeImplBody() {
        setup()
        codeBuf
                .reset()
                .tryBlock("return ${getSuccessResponseMapper()};")
                .catchBlock("HttpStatusCodeException e",
                        "return $failResponseMapper;"
                )
                .endBlock()
                .take()
    }

    private void setup() {
        classCm.fields.add(REST_TEMPLATE_FIELD)
        dependencies.add('org.springframework.web.client.HttpStatusCodeException')
    }

    private FieldCm getRestCall() {
        def responseClass = helper.responseClass
        dependencies.add(responseClass)
        def exchangeArgs = [
                mapping().to(helper.requestEntity).from(methodCm.params).make(),
                getResponseClassType(responseClass)]
        helper.responseEntity.asExpression("restTemplate.exchange(${exchangeArgs.join(', ')})")
    }

    private String getResponseClassType(ClassCd responseClass) {
        if (responseClass.genericParams) {
            def responseClassType = classNameFormatter.format(responseClass)
            dependencies.add('org.springframework.core.ParameterizedTypeReference')
            "new ParameterizedTypeReference<$responseClassType>() {}"
        } else {
            classNameFormatter.classRef(responseClass)
        }
    }

    private String getSuccessResponseMapper() {
        mapping().to(SpringRs2tTypeOfCode.RESPONSE_DTO).from(restCall).make()
    }

    private String getFailResponseMapper() {
        mapping().to(SpringRs2tTypeOfCode.RESPONSE_DTO).from(Spring.httpStatusCodeException(), 'e').make()
    }
}
