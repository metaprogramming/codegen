/*
 * Copyright (c) 2022 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring.rs2t

import pl.metaprogramming.codegen.java.ClassCd
import pl.metaprogramming.codegen.java.ClassCm
import pl.metaprogramming.codegen.java.base.ClassCmBuilderTemplate
import pl.metaprogramming.model.data.ObjectType

class RestDtoMapperBuilder extends ClassCmBuilderTemplate<ObjectType> {

    @Override
    void makeDeclaration() {
        if (restDtoClass instanceof ClassCm) {
            methods.add(new DtoMapperBuilder(model)
                    .setFrom(restDtoClass)
                    .setTo(dtoClass)
                    .setCallMapperWithResultParamAsImpl(true))
            methods.add(new DtoMapperBuilder(model)
                    .setFrom(dtoClass)
                    .setTo(restDtoClass)
                    .setCallMapperWithResultParamAsImpl(true))
            methods.add(new DtoMapperBuilder(model)
                    .setFrom(restDtoClass)
                    .setTo(dtoClass)
                    .setResultAsParam(true))
            methods.add(new DtoMapperBuilder(model)
                    .setFrom(dtoClass)
                    .setTo(restDtoClass)
                    .setResultAsParam(true))
        } else {
            // TODO JWT support
            methods.add(new DtoMapperBuilder(model)
                    .setFrom(restDtoClass)
                    .setTo(dtoClass)
                    .setThrowNotImplementedExceptionAsImpl(true))
            methods.add(new DtoMapperBuilder(model)
                    .setFrom(dtoClass)
                    .setTo(restDtoClass)
                    .setThrowNotImplementedExceptionAsImpl(true))
        }
    }

    ClassCd getDtoClass() {
        getClass(SpringRs2tTypeOfCode.DTO)
    }

    ClassCd getRestDtoClass() {
        getClass(SpringRs2tTypeOfCode.REST_DTO)
    }
}
