/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.fixtures

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestClient2tGenerator
import pl.metaprogramming.codegen.java.spring.SpringRestParams
import pl.metaprogramming.codegen.java.spring.SpringRestService2tGenerator
import pl.metaprogramming.codegen.java.validation.ValidationParams

import static pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.REST_EXCEPTION_HANDLER

class Spring2tTestProjects {

    static def rsServerSpring2tGenerator = new RsServerGenerator('rs-server-spring-2t', ExampleApiModels.OpenapiVer.V3) {
        @Override
        void setParams(CodegenParams params) {
            setDefaultJavaParams(params)
                    .set(JavaParams) {
                        it.nameMapper = LEGACY_JAVA_NAME_MAPPER
                    }
                    .set(ValidationParams) {
                        it.throwExceptionIfValidationFailed = false
                    }
        }
    }

    static def rsServerSpringLegacyGenerator = new RsServerGenerator('rs-server-spring-legacy', ExampleApiModels.OpenapiVer.V2) {
        @Override
        void setParams(CodegenParams params) {
            setDefaultJavaParams(params)
                    .set(new SpringRestParams(
                            controllerPerOperation: false,
                            staticFactoryMethodForRestResponse: false))
                    .set(JavaParams) {
                        it.dependencyInjectionBuilder = Spring.diWithAutowiredBuilder()
                        it.nameMapper = LEGACY_JAVA_NAME_MAPPER
                    }
                    .set(ValidationParams) {
                        it.throwExceptionIfValidationFailed = false
                        it.securityValidation = false
                    }
        }
    }

    static def rsClientSpring2tGenerator = new TestProjectGenerator('rs-client-spring-2t') {
        @Override
        void setParams(CodegenParams params) {
            setDefaultJavaParams(params)
                    .set(JavaParams) {
                        it.nameMapper = LEGACY_JAVA_NAME_MAPPER
                    }
        }

        @Override
        Codegen makeCodegen() {
            def apiDepsModel = ExampleApiModels.loadExampleApiDepsModel()
            def apiModel = ExampleApiModels.loadExampleApiModel(apiDepsModel)
            super.makeCodegen()
                    .generate(new SpringCommonsGenerator()) {
                        it.rootPackage = 'example.commons'
                    }
                    .generate(new SpringRestClient2tGenerator()) {
                        it.model = apiDepsModel
                        it.rootPackage = 'example.commons'
                    }
                    .generate(new SpringRestClient2tGenerator()) {
                        it.model = apiModel
                        it.rootPackage = 'example'
                    }
        }
    }

    static abstract class RsServerGenerator extends TestProjectGenerator {
        ExampleApiModels.OpenapiVer oasVer

        RsServerGenerator(String project, ExampleApiModels.OpenapiVer oasVer) {
            super(project)
            this.oasVer = oasVer
        }

        @Override
        Codegen makeCodegen() {
            def apiDepsModel = ExampleApiModels.loadExampleApiDepsModel(oasVer)
            def apiModel = ExampleApiModels.loadExampleApiModel(apiDepsModel, oasVer)
            return super.makeCodegen()
                    .generate(new SpringCommonsGenerator()) {
                        it.rootPackage = 'example.commons'
                        it.typeOfCode(REST_EXCEPTION_HANDLER) {
                            it.packageName.root = 'example.adapters.in.rest'
                        }
                    }
                    .generate(new SpringRestService2tGenerator()) {
                        it.model = apiDepsModel
                        it.rootPackage = 'example.commons'
                    }
                    .generate(new SpringRestService2tGenerator()) {
                        it.model = apiModel
                        it.rootPackage = 'example'
                    }
        }
    }
}
