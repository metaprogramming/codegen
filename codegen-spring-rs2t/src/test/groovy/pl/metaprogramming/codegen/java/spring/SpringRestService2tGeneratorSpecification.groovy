/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.utils.CheckUtils
import pl.metaprogramming.utils.ClassShadow

import static pl.metaprogramming.codegen.java.libs.Java.string
import static pl.metaprogramming.codegen.java.validation.ValidationCommonTypeOfCode.VALIDATION_COMMON_CHECKERS
import static pl.metaprogramming.utils.JavaCodeGenerationTestUtils.methodShadow

abstract class SpringRestService2tGeneratorSpecification extends SpringRestSpecification {

    abstract CodegenParams setCodegenParams(CodegenParams params)

    @Override
    Codegen makeCodegen() {
        fixSecuritySpecification()
        def codegen = new Codegen()
        setCodegenParams(codegen.params)
        codegen.codeIndex.putMapper('example.auth.AuthContextMapper.toAuthContext', AUTH_CTX_CLASS, string())
        codegen
                .generate(new SpringCommonsGenerator()) {
                    it.rootPackage("commons")
                    it.projectSubDir('src/main/java-gen', VALIDATION_COMMON_CHECKERS)
                    it.projectDir("utils-project")
                }
                .generate(new SpringRestService2tGenerator()) {
                    it.model = commonsApi
                    it.rootPackage = 'example.commons'
                }
                .generate(new SpringRestService2tGenerator()) {
                    it.model = exampleApi
                    it.rootPackage = 'example'
                    configure(it)
                }
        codegen
    }

    void configure(SpringRestService2tGenerator it) {
        it.typeOfCode(SpringRestService2tGenerator.TOC.REQUEST_DTO) {
            it.onDeclaration {
                it.fields.findAll { it.name == 'authorizationParam' }.each {
                    it.name = 'authContext'
                    it.type = AUTH_CTX_CLASS
                }
            }
        }
    }

    @Override
    Map<String, List<ClassShadow>> makeClassesToCheck() {
        [(MODULE_COMMONS_API): classesToCheckForCommonsApi(),
         (MODULE_EXAMPLE_API): classesToCheckForExampleApi()]
    }

    @Override
    Map<String, List<String>> makeExpectedClasses() {
        [(MODULE_COMMONS)    : expectedClassesForCommons(),
         (MODULE_COMMONS_API): expectedClassesForCommonsApi(),
         (MODULE_EXAMPLE_API): expectedClassesForExampleApi()]
    }

    List<ClassShadow> classesToCheckForCommonsApi() {
        [
                new ClassShadow(
                        name: 'example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper',
                        classHeader: 'public class ErrorDescriptionMapper',
                        noMoreMethods: true,
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@Component',
                                '@RequiredArgsConstructor',
                        ],
                        fields: ['private final ErrorDetailMapper errorDetailMapper'],
                        methods: [
                                methodShadow('map2ErrorDescriptionRdto', 'public ErrorDescriptionRdto map2ErrorDescriptionRdto(ErrorDescriptionDto value)',
                                        ['return value == null ? null : map(new ErrorDescriptionRdto(), value);']),
                                methodShadow('map', 'public ErrorDescriptionRdto map(ErrorDescriptionRdto result, ErrorDescriptionDto value)',
                                        ['return result',
                                         '        .setCode(SerializationUtils.toString(value.getCode()))',
                                         '        .setMessage(value.getMessage())',
                                         '        .setErrors(SerializationUtils.transformList(value.getErrors(), errorDetailMapper::map2ErrorDetailRdto))',
                                         '        ;',
                                        ]),
                        ],
                        imports: [
                                'commons.SerializationUtils',
                                'example.commons.adapters.in.rest.dtos.ErrorDescriptionRdto',
                                'example.commons.ports.in.rest.dtos.ErrorDescriptionDto',
                                'lombok.RequiredArgsConstructor',
                                'org.springframework.stereotype.Component',
                        ],
                ),
        ]
    }

    List<ClassShadow> classesToCheckForExampleApi() {
        SpringRestClassShadows.getClassShadows('in') + [
                new ClassShadow(
                        name: 'example.adapters.in.rest.validators.ExtendedObjectValidator',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@RequiredArgsConstructor',
                                '@Component',
                        ],
                        classHeader: 'public class ExtendedObjectValidator implements Checker<ExtendedObjectRdto>',
                        fields: [
                                'private final ExtendedObjectChecker extendedObjectChecker',
                                'private final ExtendedObjectEnumChecker extendedObjectEnumChecker',
                                'private final SimpleObjectValidator simpleObjectValidator',
                                'public static final Field<ExtendedObjectRdto,ExtendedObjectRdto> FIELD_SELF_PROPERTY = new Field<>("self_property", ExtendedObjectRdto::getSelfProperty)',
                                'public static final Field<ExtendedObjectRdto,String> FIELD_EO_ENUM_REUSABLE = new Field<>("eo_enum_reusable", ExtendedObjectRdto::getEoEnumReusable)',
                        ],
                        methods: [
                                methodShadow('check', 'public void check(ValidationContext<ExtendedObjectRdto> ctx)',
                                        ['ctx.check(extendedObjectChecker);',
                                         'simpleObjectValidator.checkWithParent(ctx);',
                                         'ctx.check(FIELD_EO_ENUM_REUSABLE, allow(ReusableEnumEnum.values()), extendedObjectEnumChecker);',
                                         'ctx.check(FIELD_SELF_PROPERTY, this);',
                                        ])],
                        imports: [
                                'commons.validator.Checker',
                                'commons.validator.Field',
                                'commons.validator.ValidationContext',
                                'example.adapters.in.rest.dtos.ExtendedObjectRdto',
                                'example.commons.adapters.in.rest.validators.SimpleObjectValidator',
                                'example.commons.ports.in.rest.dtos.ReusableEnumEnum',
                                'lombok.RequiredArgsConstructor',
                                'org.springframework.stereotype.Component',
                                'static commons.validator.CommonCheckers.*',
                        ],
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.validators.EchoBodyValidator',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@RequiredArgsConstructor',
                                '@Component',
                        ],
                        classHeader: 'public class EchoBodyValidator implements Checker<EchoBodyRdto>',
                        fields: [
                                '@Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT") private final Checker<SimpleObjectRdto> simpleObjectCustomConstraint',
                                'private final AmountChecker amountChecker',
                                'private final DictionaryChecker dictionaryChecker',
                                'private final ExtendedObjectValidator extendedObjectValidator',
                                'private final SimpleObjectValidator simpleObjectValidator',
                                'private final UserDataValidationBean userDataValidationBean',
                                'private static final Checker<String> FIELD_PROP_AMOUNT_PATTERN = matches(Pattern.compile("^(0|([1-9][0-9]{0,}))\\\\.\\\\d{2}$"))',
                                'private static final Checker<String> FIELD_PROP_STRING_PATTERN_PATTERN = matches(Pattern.compile("^[\\\\-\\\\\\".\',:;/\\\\\\\\!@#$%^&*()+_?|><=]{2,19}$"), "custom_error_code")',
                                'public static final Field<EchoBodyRdto,ExtendedObjectRdto> FIELD_PROP_OBJECT_EXTENDED = new Field<>("prop_object_extended", EchoBodyRdto::getPropObjectExtended)',
                                'public static final Field<EchoBodyRdto,Map<String,Object>> FIELD_PROP_OBJECT_ANY = new Field<>("prop_object_any", EchoBodyRdto::getPropObjectAny)',
                                'public static final Field<EchoBodyRdto,SimpleObjectRdto> FIELD_PROP_OBJECT = new Field<>("prop_object", EchoBodyRdto::getPropObject)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_AMOUNT = new Field<>("prop_amount", EchoBodyRdto::getPropAmount)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_AMOUNT_NUMBER = new Field<>("prop_amount_number", EchoBodyRdto::getPropAmountNumber)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_BASE64 = new Field<>("prop_base64", EchoBodyRdto::getPropBase64)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_BOOLEAN = new Field<>("prop_boolean", EchoBodyRdto::getPropBoolean)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_DATE = new Field<>("prop_date", EchoBodyRdto::getPropDate)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_DATE_SECOND = new Field<>("prop_date_second", EchoBodyRdto::getPropDateSecond)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_DATE_TIME = new Field<>("prop_date_time", EchoBodyRdto::getPropDateTime)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_DEFAULT = new Field<>("prop_default", EchoBodyRdto::getPropDefault)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_DOUBLE = new Field<>("prop_double", EchoBodyRdto::getPropDouble)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_ENUM = new Field<>("prop_enum", EchoBodyRdto::getPropEnum)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_ENUM_REUSABLE = new Field<>("prop_enum_reusable", EchoBodyRdto::getPropEnumReusable)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_FLOAT = new Field<>("prop_float", EchoBodyRdto::getPropFloat)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_INT = new Field<>("prop_int", EchoBodyRdto::getPropInt)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_INT_REQUIRED = new Field<>("prop_int_required", EchoBodyRdto::getPropIntRequired)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_INT_SECOND = new Field<>("prop_int_second", EchoBodyRdto::getPropIntSecond)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_STRING = new Field<>("prop_string", EchoBodyRdto::getPropString)',
                                'public static final Field<EchoBodyRdto,String> FIELD_PROP_STRING_PATTERN = new Field<>("prop_string_pattern", EchoBodyRdto::getPropStringPattern)',
                        ],
                        methods: [
                                methodShadow('check', 'public void check(ValidationContext<EchoBodyRdto> ctx)', [
                                        'ctx.check(FIELD_PROP_INT, INT32);',
                                        'ctx.check(FIELD_PROP_INT_SECOND, INT32);',
                                        'ctx.check(FIELD_PROP_INT_REQUIRED, required(), INT64, ge("-1", Long::valueOf));',
                                        'ctx.check(FIELD_PROP_FLOAT, FLOAT, le("101", Float::valueOf));',
                                        'ctx.check(FIELD_PROP_DOUBLE, DOUBLE, gt("0", Double::valueOf), lt("10000", Double::valueOf));',
                                        'ctx.check(FIELD_PROP_AMOUNT, FIELD_PROP_AMOUNT_PATTERN, userDataValidationBean::checkAmountByUser, Checkers.AMOUNT_SCALE_CHECKER);',
                                        'ctx.check(FIELD_PROP_AMOUNT_NUMBER, amountChecker, ge("0.02", BigDecimal::new), le("9999999999.99", BigDecimal::new));',
                                        'ctx.check(FIELD_PROP_STRING, minLength(5), maxLength(10), dictionaryChecker.check(COLORS));',
                                        'ctx.check(FIELD_PROP_STRING_PATTERN, FIELD_PROP_STRING_PATTERN_PATTERN);',
                                        'ctx.check(FIELD_PROP_DEFAULT, minLength(5));',
                                        'ctx.check(FIELD_PROP_DATE, ISO_DATE);',
                                        'ctx.check(FIELD_PROP_DATE_SECOND, ISO_DATE);',
                                        'ctx.check(FIELD_PROP_DATE_TIME, ISO_DATE_TIME);',
                                        'ctx.check(FIELD_PROP_BASE64, BASE64);',
                                        'ctx.check(FIELD_PROP_BOOLEAN, BOOLEAN);',
                                        'ctx.check(FIELD_PROP_OBJECT, simpleObjectValidator, simpleObjectCustomConstraint);',
                                        'ctx.check(FIELD_PROP_OBJECT_EXTENDED, extendedObjectValidator);',
                                        'ctx.check(FIELD_PROP_ENUM_REUSABLE, allow(ReusableEnumEnum.values()));',
                                        'ctx.check(FIELD_PROP_ENUM, allow(EnumTypeEnum.values()));',
                                        'ctx.check(lt(FIELD_PROP_INT, FIELD_PROP_INT_SECOND, SerializationUtils::toInteger).withError(COMPARE_PROP_INT_FAILED));',
                                        'ctx.check(ge(FIELD_PROP_DATE, FIELD_PROP_DATE_SECOND, SerializationUtils::toLocalDate));',
                                ])],
                        imports: [
                                'commons.SerializationUtils',
                                'commons.validator.Checker',
                                'commons.validator.DictionaryChecker',
                                'commons.validator.Field',
                                'commons.validator.ValidationContext',
                                'example.adapters.in.rest.dtos.EchoBodyRdto',
                                'example.adapters.in.rest.dtos.ExtendedObjectRdto',
                                'example.commons.adapters.in.rest.dtos.SimpleObjectRdto',
                                'example.commons.adapters.in.rest.validators.SimpleObjectValidator',
                                'example.commons.ports.in.rest.dtos.ReusableEnumEnum',
                                'example.commons.validator.AmountChecker',
                                'example.commons.validator.Checkers',
                                'example.ports.in.rest.dtos.EnumTypeEnum',
                                'java.math.BigDecimal',
                                'java.util.Map',
                                'java.util.regex.Pattern',
                                'lombok.RequiredArgsConstructor',
                                'org.springframework.beans.factory.annotation.Qualifier',
                                'org.springframework.stereotype.Component',
                                'static commons.validator.CommonCheckers.*',
                                'static commons.validator.DictionaryCode.*',
                                'static commons.validator.ErrorCode.*',
                        ],
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.echo.EchoPostValidator',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@RequiredArgsConstructor',
                                '@Component',
                        ],
                        classHeader: 'public class EchoPostValidator extends Validator<EchoPostRrequest>',
                        fields: [
                                'private final AuthorizationChecker authorizationChecker',
                                'private final EchoBodyValidator echoBodyValidator',
                                'public static final Field<EchoPostRrequest,EchoBodyRdto> FIELD_REQUEST_BODY = new Field<>("requestBody", EchoPostRrequest::getRequestBody)',
                                'public static final Field<EchoPostRrequest,String> FIELD_AUTHORIZATION_PARAM = new Field<>("Authorization (HEADER parameter)", EchoPostRrequest::getAuthorizationParam)',
                                'public static final Field<EchoPostRrequest,String> FIELD_CORRELATION_ID_PARAM = new Field<>("X-Correlation-ID (HEADER parameter)", EchoPostRrequest::getCorrelationIdParam)',
                                'public static final Field<EchoPostRrequest,String> FIELD_INLINE_HEADER_PARAM = new Field<>("Inline-Header-Param (HEADER parameter)", EchoPostRrequest::getInlineHeaderParam)',
                                'public static final Field<EchoPostRrequest,String> FIELD_TIMESTAMP_PARAM = new Field<>("timestamp (HEADER parameter)", EchoPostRrequest::getTimestampParam)',
                        ],
                        methods: [
                                methodShadow('check', 'public void check(ValidationContext<EchoPostRrequest> ctx)',
                                        ['ctx.setBean(OperationId.ECHO_POST);',
                                         'ctx.check(FIELD_AUTHORIZATION_PARAM, authorizationChecker, required());',
                                         'ctx.check(FIELD_CORRELATION_ID_PARAM, required());',
                                         'ctx.check(FIELD_TIMESTAMP_PARAM, ISO_DATE_TIME);',
                                         'ctx.checkRoot(FIELD_REQUEST_BODY, required(), echoBodyValidator);',
                                        ])],
                        imports: [
                                'commons.validator.Field',
                                'commons.validator.OperationId',
                                'commons.validator.ValidationContext',
                                'commons.validator.Validator',
                                'example.adapters.in.rest.dtos.EchoBodyRdto',
                                'example.adapters.in.rest.dtos.EchoPostRrequest',
                                'example.adapters.in.rest.validators.AuthorizationChecker',
                                'example.adapters.in.rest.validators.EchoBodyValidator',
                                'lombok.RequiredArgsConstructor',
                                'org.springframework.stereotype.Component',
                                'static commons.validator.CommonCheckers.*',
                        ],
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.echo.EchoPostController',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@RequiredArgsConstructor',
                                '@RestController',
                        ],
                        classHeader: 'public class EchoPostController',
                        fields: [
                                'private final EchoFacade echoFacade',
                                'private final EchoPostRequestMapper echoPostRequestMapper',
                                'private final EchoPostResponseMapper echoPostResponseMapper',
                                'private final EchoPostValidator echoPostValidator',
                                'private final ValidationResultMapper validationResultMapper',
                        ],
                        methods: [
                                methodShadow(
                                        'echoPost',
                                        'public ResponseEntity echoPost(@RequestHeader(value = "Authorization", required = false) String authorizationParam, @RequestHeader(value = "X-Correlation-ID", required = false) String correlationIdParam, @RequestHeader(value = "timestamp", required = false) String timestampParam, @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam, @RequestBody EchoBodyRdto requestBody)',
                                        ['EchoPostRrequest request = echoPostRequestMapper.map2EchoPostRrequest(authorizationParam, correlationIdParam, timestampParam, inlineHeaderParam, requestBody);',
                                         'ValidationResult validationResult = echoPostValidator.validate(request);',
                                         'return validationResult.isValid()',
                                         '        ? echoPostResponseMapper.map(echoFacade.echoPost(echoPostRequestMapper.map2EchoPostRequest(request)))',
                                         '        : validationResultMapper.map(validationResult);',
                                        ],
                                        ['@PostMapping(value="/api/v1/echo",produces={"application/json"},consumes={"application/json"})']
                                ),
                        ],
                        imports: [
                                'commons.validator.ValidationResult',
                                'commons.validator.ValidationResultMapper',
                                'example.adapters.in.rest.dtos.EchoBodyRdto',
                                'example.adapters.in.rest.dtos.EchoPostRrequest',
                                'example.adapters.in.rest.mappers.EchoPostRequestMapper',
                                'example.ports.in.rest.EchoFacade',
                                'lombok.RequiredArgsConstructor',
                                'org.springframework.http.ResponseEntity',
                                'org.springframework.web.bind.annotation.PostMapping',
                                'org.springframework.web.bind.annotation.RequestBody',
                                'org.springframework.web.bind.annotation.RequestHeader',
                                'org.springframework.web.bind.annotation.RestController',
                        ],
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.echo.EchoPostResponseMapper',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@RequiredArgsConstructor',
                                '@Component',
                        ],
                        classHeader: 'public class EchoPostResponseMapper',
                        fields: [
                                'private final EchoBodyMapper echoBodyMapper',
                                'private final ErrorDescriptionMapper errorDescriptionMapper',
                        ],
                        methods: [
                                methodShadow('map', 'public ResponseEntity map(@Nonnull EchoPostResponse response)',
                                        ['ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());',
                                         'response.getHeaders().forEach(responseBuilder::header);',
                                         'if (response.is200()) {',
                                         '    return responseBuilder.body(echoBodyMapper.map2EchoBodyRdto(response.get200()));',
                                         '}',
                                         'return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.getOther()));',
                                        ]),
                        ],
                        imports: [
                                'example.adapters.in.rest.mappers.EchoBodyMapper',
                                'example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper',
                                'example.ports.in.rest.dtos.EchoPostResponse',
                                'javax.annotation.Nonnull',
                                'lombok.RequiredArgsConstructor',
                                'org.springframework.http.ResponseEntity',
                                'org.springframework.stereotype.Component',
                        ],
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.validators.EchoArraysBodyValidator',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@RequiredArgsConstructor',
                                '@Component',
                        ],
                        classHeader: 'public class EchoArraysBodyValidator implements Checker<EchoArraysBodyRdto>',
                        fields: [
                                '@Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT") private final Checker<SimpleObjectRdto> simpleObjectCustomConstraint',
                                'private final DictionaryChecker dictionaryChecker',
                                'private final SimpleObjectValidator simpleObjectValidator',
                                'private static final Checker<String> FIELD_PROP_AMOUNT_LIST_PATTERN = matches(Pattern.compile("^(0|([1-9][0-9]{0,}))\\\\.\\\\d{2}$"), "invalid_amount")',
                                'public static final Field<EchoArraysBodyRdto,List<List<SimpleObjectRdto>>> FIELD_PROP_OBJECT_LIST_OF_LIST = new Field<>("prop_object_list_of_list", EchoArraysBodyRdto::getPropObjectListOfList)',
                                'public static final Field<EchoArraysBodyRdto,List<List<String>>> FIELD_PROP_DATE_TIME_LIST_OF_LIST = new Field<>("prop_date_time_list_of_list", EchoArraysBodyRdto::getPropDateTimeListOfList)',
                                'public static final Field<EchoArraysBodyRdto,List<SimpleObjectRdto>> FIELD_PROP_OBJECT_LIST = new Field<>("prop_object_list", EchoArraysBodyRdto::getPropObjectList)',
                                'public static final Field<EchoArraysBodyRdto,List<String>> FIELD_PROP_AMOUNT_LIST = new Field<>("prop_amount_list", EchoArraysBodyRdto::getPropAmountList)',
                                'public static final Field<EchoArraysBodyRdto,List<String>> FIELD_PROP_BOOLEAN_LIST = new Field<>("prop_boolean_list", EchoArraysBodyRdto::getPropBooleanList)',
                                'public static final Field<EchoArraysBodyRdto,List<String>> FIELD_PROP_DATE_LIST = new Field<>("prop_date_list", EchoArraysBodyRdto::getPropDateList)',
                                'public static final Field<EchoArraysBodyRdto,List<String>> FIELD_PROP_DOUBLE_LIST = new Field<>("prop_double_list", EchoArraysBodyRdto::getPropDoubleList)',
                                'public static final Field<EchoArraysBodyRdto,List<String>> FIELD_PROP_ENUM_LIST = new Field<>("prop_enum_list", EchoArraysBodyRdto::getPropEnumList)',
                                'public static final Field<EchoArraysBodyRdto,List<String>> FIELD_PROP_ENUM_REUSABLE_LIST = new Field<>("prop_enum_reusable_list", EchoArraysBodyRdto::getPropEnumReusableList)',
                                'public static final Field<EchoArraysBodyRdto,List<String>> FIELD_PROP_FLOAT_LIST = new Field<>("prop_float_list", EchoArraysBodyRdto::getPropFloatList)',
                                'public static final Field<EchoArraysBodyRdto,List<String>> FIELD_PROP_INT_LIST = new Field<>("prop_int_list", EchoArraysBodyRdto::getPropIntList)',
                                'public static final Field<EchoArraysBodyRdto,List<String>> FIELD_PROP_STRING_LIST = new Field<>("prop_string_list", EchoArraysBodyRdto::getPropStringList)',
                                'public static final Field<EchoArraysBodyRdto,Map<String,List<SimpleObjectRdto>>> FIELD_PROP_MAP_OF_LIST_OF_OBJECT = new Field<>("prop_map_of_list_of_object", EchoArraysBodyRdto::getPropMapOfListOfObject)',
                                'public static final Field<EchoArraysBodyRdto,Map<String,SimpleObjectRdto>> FIELD_PROP_MAP_OF_OBJECT = new Field<>("prop_map_of_object", EchoArraysBodyRdto::getPropMapOfObject)',
                                'public static final Field<EchoArraysBodyRdto,Map<String,String>> FIELD_PROP_MAP_OF_INT = new Field<>("prop_map_of_int", EchoArraysBodyRdto::getPropMapOfInt)',
                        ],
                        methods: [
                                methodShadow('check', 'public void check(ValidationContext<EchoArraysBodyRdto> ctx)',
                                        ['ctx.check(FIELD_PROP_INT_LIST, minItems(1), items(INT32, gt("-2", Integer::valueOf)));',
                                         'ctx.check(FIELD_PROP_FLOAT_LIST, items(FLOAT, le("101", Float::valueOf)));',
                                         'ctx.check(FIELD_PROP_DOUBLE_LIST, required(), unique(), minItems(2), maxItems(4), items(DOUBLE, ge("0.01", Double::valueOf), le("1000000000000000", Double::valueOf)));',
                                         'ctx.check(FIELD_PROP_AMOUNT_LIST, items(FIELD_PROP_AMOUNT_LIST_PATTERN));',
                                         'ctx.check(FIELD_PROP_STRING_LIST, items(dictionaryChecker.check(ANIMALS).withError(INVALID_ANIMAL), minLength(5), maxLength(10)));',
                                         'ctx.check(FIELD_PROP_DATE_LIST, items(ISO_DATE));',
                                         'ctx.check(FIELD_PROP_DATE_TIME_LIST_OF_LIST, minItems(1), items(minItems(2), items(ISO_DATE_TIME)));',
                                         'ctx.check(FIELD_PROP_BOOLEAN_LIST, items(BOOLEAN));',
                                         'ctx.check(FIELD_PROP_OBJECT_LIST, items(simpleObjectValidator, simpleObjectCustomConstraint.withError(CUSTOM_FAILED_CODE)));',
                                         'ctx.check(FIELD_PROP_OBJECT_LIST_OF_LIST, items(items(simpleObjectValidator, simpleObjectCustomConstraint)));',
                                         'ctx.check(FIELD_PROP_ENUM_REUSABLE_LIST, items(allow(ReusableEnumEnum.values())));',
                                         'ctx.check(FIELD_PROP_ENUM_LIST, items(allow(EnumTypeEnum.values())));',
                                         'ctx.check(FIELD_PROP_MAP_OF_INT, mapValues(INT32, le("100", Integer::valueOf)));',
                                         'ctx.check(FIELD_PROP_MAP_OF_OBJECT, mapValues(simpleObjectValidator));',
                                         'ctx.check(FIELD_PROP_MAP_OF_LIST_OF_OBJECT, mapValues(items(simpleObjectValidator)));',
                                        ])],
                        imports: [
                                'commons.validator.Checker',
                                'commons.validator.DictionaryChecker',
                                'commons.validator.Field',
                                'commons.validator.ValidationContext',
                                'example.adapters.in.rest.dtos.EchoArraysBodyRdto',
                                'example.commons.adapters.in.rest.dtos.SimpleObjectRdto',
                                'example.commons.adapters.in.rest.validators.SimpleObjectValidator',
                                'example.commons.ports.in.rest.dtos.ReusableEnumEnum',
                                'example.ports.in.rest.dtos.EnumTypeEnum',
                                'java.util.List',
                                'java.util.Map',
                                'java.util.regex.Pattern',
                                'lombok.RequiredArgsConstructor',
                                'org.springframework.beans.factory.annotation.Qualifier',
                                'org.springframework.stereotype.Component',
                                'static commons.validator.CommonCheckers.*',
                                'static commons.validator.DictionaryCode.*',
                                'static commons.validator.ErrorCode.*',
                        ],
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.mappers.EchoArraysPostRequestMapper',
                        classHeader: 'public class EchoArraysPostRequestMapper',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@RequiredArgsConstructor',
                                '@Component',
                        ],
                        fields: [
                                'private final AuthContextMapper authContextMapper',
                                'private final EchoArraysBodyMapper echoArraysBodyMapper',
                        ],
                        methods: [
                                methodShadow('map2EchoArraysPostRrequest', 'public EchoArraysPostRrequest map2EchoArraysPostRrequest(String authorizationParam, String inlineHeaderParam, List<EchoArraysBodyRdto> requestBody)',
                                        ['return new EchoArraysPostRrequest()',
                                         '        .setAuthorizationParam(authorizationParam)',
                                         '        .setInlineHeaderParam(inlineHeaderParam)',
                                         '        .setRequestBody(SerializationUtils.transformList(requestBody, v -> v))',
                                         '        ;',
                                        ]),
                                methodShadow('map2EchoArraysPostRequest', 'public EchoArraysPostRequest map2EchoArraysPostRequest(EchoArraysPostRrequest value)',
                                        ['return value == null ? null : new EchoArraysPostRequest()',
                                         '        .setAuthContext(authContextMapper.toAuthContext(value.getAuthorizationParam()))',
                                         '        .setInlineHeaderParam(value.getInlineHeaderParam())',
                                         '        .setRequestBody(SerializationUtils.transformList(value.getRequestBody(), echoArraysBodyMapper::map2EchoArraysBodyDto))',
                                         '        ;',
                                        ]),
                        ],
                        imports: [
                                'commons.SerializationUtils',
                                'example.adapters.in.rest.dtos.EchoArraysBodyRdto',
                                'example.adapters.in.rest.dtos.EchoArraysPostRrequest',
                                'example.auth.AuthContextMapper',
                                'example.ports.in.rest.dtos.EchoArraysPostRequest',
                                'java.util.List',
                                'lombok.RequiredArgsConstructor',
                                'org.springframework.stereotype.Component',
                        ]
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.echo.EchoArraysPostResponseMapper',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@RequiredArgsConstructor',
                                '@Component',
                        ],
                        classHeader: 'public class EchoArraysPostResponseMapper',
                        fields: [
                                'private final EchoArraysBodyMapper echoArraysBodyMapper',
                                'private final ErrorDescriptionMapper errorDescriptionMapper',
                        ],
                        methods: [
                                methodShadow('map', 'public ResponseEntity map(@Nonnull EchoArraysPostResponse response)', [
                                        'ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());',
                                        'response.getHeaders().forEach(responseBuilder::header);',
                                        'if (response.is400()) {',
                                        '    return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.get400()));',
                                        '}',
                                        'if (response.is204()) {',
                                        '    return responseBuilder.build();',
                                        '}',
                                        'return responseBuilder.body(SerializationUtils.transformList(response.get200(), echoArraysBodyMapper::map2EchoArraysBodyRdto));',
                                ]),
                        ],
                        imports: [
                                'commons.SerializationUtils',
                                'example.adapters.in.rest.mappers.EchoArraysBodyMapper',
                                'example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper',
                                'example.ports.in.rest.dtos.EchoArraysPostResponse',
                                'javax.annotation.Nonnull',
                                'lombok.RequiredArgsConstructor',
                                'org.springframework.http.ResponseEntity',
                                'org.springframework.stereotype.Component',
                        ],
                ),
                new ClassShadow(
                        name: 'example.ports.in.rest.dtos.UploadEchoFileRequest',
                        classHeader: 'public class UploadEchoFileRequest',
                        annotations: CheckUtils.DTO_ANNOTATIONS,
                        imports: CheckUtils.DTO_IMPORTS + CheckUtils.NONNULL_IMPORTS,
                        fields: [
                                '@Nonnull private Long id',
                                '@Nonnull private byte[] requestBody',
                        ]
                ),
                new ClassShadow(
                        name: 'example.ports.in.rest.dtos.UploadEchoFileWithFormRequest',
                        classHeader: 'public class UploadEchoFileWithFormRequest',
                        annotations: CheckUtils.DTO_ANNOTATIONS,
                        imports: CheckUtils.DTO_IMPORTS + CheckUtils.NONNULL_IMPORTS,
                        fields: [
                                '@Nonnull private Long id',
                                '@Nonnull private byte[] file',
                        ]
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.dtos.UploadEchoFileRrequest',
                        classHeader: 'public class UploadEchoFileRrequest',
                        annotations: CheckUtils.DTO_ANNOTATIONS,
                        fields: [
                                'private Resource requestBody',
                                'private String id',
                        ],
                        imports: [
                                'lombok.Data',
                                'lombok.NoArgsConstructor',
                                'lombok.experimental.Accessors',
                                'org.springframework.core.io.Resource',
                        ],
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.dtos.UploadEchoFileWithFormRrequest',
                        classHeader: 'public class UploadEchoFileWithFormRrequest',
                        annotations: CheckUtils.DTO_ANNOTATIONS,
                        fields: [
                                'private Resource file',
                                'private String id',
                        ],
                        imports: [
                                'lombok.Data',
                                'lombok.NoArgsConstructor',
                                'lombok.experimental.Accessors',
                                'org.springframework.core.io.Resource',
                        ],
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.mappers.UploadEchoFileRequestMapper',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@Component',
                        ],
                        methods: [
                                methodShadow('map2UploadEchoFileRrequest', 'public UploadEchoFileRrequest map2UploadEchoFileRrequest(String id, Resource requestBody)',
                                        ['return new UploadEchoFileRrequest()',
                                         '        .setId(id)',
                                         '        .setRequestBody(requestBody)',
                                         '        ;',
                                        ]),
                                methodShadow('map2UploadEchoFileRequest', 'public UploadEchoFileRequest map2UploadEchoFileRequest(UploadEchoFileRrequest value)',
                                        ['return value == null ? null : new UploadEchoFileRequest()',
                                         '        .setId(SerializationUtils.toLong(value.getId()))',
                                         '        .setRequestBody(SerializationUtils.toBytes(value.getRequestBody()))',
                                         '        ;',
                                        ]),
                        ],
                        imports: ['commons.SerializationUtils',
                                  'example.adapters.in.rest.dtos.UploadEchoFileRrequest',
                                  'example.ports.in.rest.dtos.UploadEchoFileRequest',
                                  'org.springframework.stereotype.Component',
                                  'org.springframework.core.io.Resource',
                        ]
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.mappers.UploadEchoFileWithFormRequestMapper',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@Component',
                        ],
                        methods: [
                                methodShadow('map2UploadEchoFileWithFormRrequest', 'public UploadEchoFileWithFormRrequest map2UploadEchoFileWithFormRrequest(String id, MultipartFile file)',
                                        ['return new UploadEchoFileWithFormRrequest()',
                                         '        .setId(id)',
                                         '        .setFile(file.getResource())',
                                         '        ;',
                                        ]),
                                methodShadow('map2UploadEchoFileWithFormRequest', 'public UploadEchoFileWithFormRequest map2UploadEchoFileWithFormRequest(UploadEchoFileWithFormRrequest value)',
                                        ['return value == null ? null : new UploadEchoFileWithFormRequest()',
                                         '        .setId(SerializationUtils.toLong(value.getId()))',
                                         '        .setFile(SerializationUtils.toBytes(value.getFile()))',
                                         '        ;',
                                        ]),
                        ],
                        imports: ['commons.SerializationUtils',
                                  'example.adapters.in.rest.dtos.UploadEchoFileWithFormRrequest',
                                  'example.ports.in.rest.dtos.UploadEchoFileWithFormRequest',
                                  'org.springframework.stereotype.Component',
                                  'org.springframework.web.multipart.MultipartFile',
                        ]
                ),
                new ClassShadow(
                        name: 'example.ports.in.rest.dtos.DownloadEchoFileResponse',
                        classHeader: 'public class DownloadEchoFileResponse extends RestResponseBase<DownloadEchoFileResponse> implements RestResponse200<DownloadEchoFileResponse,byte[]>, RestResponse404NoContent<DownloadEchoFileResponse>, RestResponseOther<DownloadEchoFileResponse,ErrorDescriptionDto>',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@ParametersAreNonnullByDefault',
                        ],
                        fields: [
                                'private static final Collection<Integer> DECLARED_STATUSES = Arrays.asList(200, 404)'
                        ],
                        methods: [
                                methodShadow('set200', 'public static DownloadEchoFileResponse set200(byte[] body)',
                                        ['return new DownloadEchoFileResponse(200, body);']),
                        ],
                        imports: [
                                'commons.RestResponse200',
                                'commons.RestResponse404NoContent',
                                'commons.RestResponseBase',
                                'commons.RestResponseOther',
                                'example.commons.ports.in.rest.dtos.ErrorDescriptionDto',
                                'java.util.Arrays',
                                'java.util.Collection',
                                'javax.annotation.ParametersAreNonnullByDefault',
                        ],
                ),
                new ClassShadow(
                        name: 'example.adapters.in.rest.echo.EchoDateArrayGetResponseMapper',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@RequiredArgsConstructor',
                                '@Component',
                        ],
                        fields: ['private final ErrorDescriptionMapper errorDescriptionMapper'],
                        methods: [
                                methodShadow('map', 'public ResponseEntity map(@Nonnull EchoDateArrayGetResponse response)',
                                        ['ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(response.getStatus());',
                                         'response.getHeaders().forEach(responseBuilder::header);',
                                         'if (response.is200()) {',
                                         '    return responseBuilder.body(SerializationUtils.transformList(response.get200(), SerializationUtils::toString));',
                                         '}',
                                         'return responseBuilder.body(errorDescriptionMapper.map2ErrorDescriptionRdto(response.getOther()));',
                                        ]),
                        ],
                        imports: [
                                'commons.SerializationUtils',
                                'example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper',
                                'example.ports.in.rest.dtos.EchoDateArrayGetResponse',
                                'javax.annotation.Nonnull',
                                'lombok.RequiredArgsConstructor',
                                'org.springframework.http.ResponseEntity',
                                'org.springframework.stereotype.Component',
                        ]
                ),
                new ClassShadow(
                        name: 'example.application.EchoFacadeImpl',
                        classHeader: 'public class EchoFacadeImpl implements EchoFacade',
                        annotations: [
                                '@Generated("pl.metaprogramming.codegen")',
                                '@Component',
                        ],
                        methods: [
                                methodShadow('echoPost', 'public EchoPostResponse echoPost(@Nonnull EchoPostRequest request)', ['return null;'], ['@Override']),
                                methodShadow('echoArraysPost', 'public EchoArraysPostResponse echoArraysPost(@Nonnull EchoArraysPostRequest request)', ['return null;'], ['@Override']),
                                methodShadow('echoDateArrayGet', 'public EchoDateArrayGetResponse echoDateArrayGet(@Nonnull EchoDateArrayGetRequest request)', ['return null;'], ['@Override']),
                                methodShadow('uploadEchoFile', 'public UploadEchoFileResponse uploadEchoFile(@Nonnull UploadEchoFileRequest request)', ['return null;'], ['@Override']),
                                methodShadow('uploadEchoFileWithForm', 'public UploadEchoFileWithFormResponse uploadEchoFileWithForm(@Nonnull UploadEchoFileWithFormRequest request)', ['return null;'], ['@Override']),
                                methodShadow('downloadEchoFile', 'public DownloadEchoFileResponse downloadEchoFile(@Nonnull DownloadEchoFileRequest request)', ['return null;'], ['@Override']),
                                methodShadow('deleteFile', 'public DeleteFileResponse deleteFile(@Nonnull DeleteFileRequest request)', ['return null;'], ['@Override']),
                                methodShadow('echoError', 'public EchoErrorResponse echoError(@Nonnull EchoErrorRequest request)', ['return null;'], ['@Override']),
                        ],
                        imports: [
                                'example.ports.in.rest.EchoFacade',
                                'example.ports.in.rest.dtos.DeleteFileRequest',
                                'example.ports.in.rest.dtos.DeleteFileResponse',
                                'example.ports.in.rest.dtos.DownloadEchoFileRequest',
                                'example.ports.in.rest.dtos.DownloadEchoFileResponse',
                                'example.ports.in.rest.dtos.EchoArraysPostRequest',
                                'example.ports.in.rest.dtos.EchoArraysPostResponse',
                                'example.ports.in.rest.dtos.EchoDateArrayGetRequest',
                                'example.ports.in.rest.dtos.EchoDateArrayGetResponse',
                                'example.ports.in.rest.dtos.EchoDefaultsPostRequest',
                                'example.ports.in.rest.dtos.EchoDefaultsPostResponse',
                                'example.ports.in.rest.dtos.EchoEmptyRequest',
                                'example.ports.in.rest.dtos.EchoEmptyResponse',
                                'example.ports.in.rest.dtos.EchoErrorRequest',
                                'example.ports.in.rest.dtos.EchoErrorResponse',
                                'example.ports.in.rest.dtos.EchoGetRequest',
                                'example.ports.in.rest.dtos.EchoGetResponse',
                                'example.ports.in.rest.dtos.EchoPostRequest',
                                'example.ports.in.rest.dtos.EchoPostResponse',
                                'example.ports.in.rest.dtos.UploadEchoFileRequest',
                                'example.ports.in.rest.dtos.UploadEchoFileResponse',
                                'example.ports.in.rest.dtos.UploadEchoFileWithFormRequest',
                                'example.ports.in.rest.dtos.UploadEchoFileWithFormResponse',
                                'javax.annotation.Nonnull',
                                'org.springframework.stereotype.Component',
                        ],
                ),
                new ClassShadow(
                        name: 'example.ports.in.rest.EchoFacade',
                        classHeader: 'public interface EchoFacade',
                        annotations: CheckUtils.GEN_ANNOTATIONS,
                        methods: [
                                methodShadow('echoPost', 'EchoPostResponse echoPost(@Nonnull EchoPostRequest request)'),
                                methodShadow('echoArraysPost', 'EchoArraysPostResponse echoArraysPost(@Nonnull EchoArraysPostRequest request)'),
                                methodShadow('echoDateArrayGet', 'EchoDateArrayGetResponse echoDateArrayGet(@Nonnull EchoDateArrayGetRequest request)'),
                                methodShadow('uploadEchoFile', 'UploadEchoFileResponse uploadEchoFile(@Nonnull UploadEchoFileRequest request)'),
                                methodShadow('uploadEchoFileWithForm', 'UploadEchoFileWithFormResponse uploadEchoFileWithForm(@Nonnull UploadEchoFileWithFormRequest request)'),
                                methodShadow('downloadEchoFile', 'DownloadEchoFileResponse downloadEchoFile(@Nonnull DownloadEchoFileRequest request)'),
                                methodShadow('deleteFile', 'DeleteFileResponse deleteFile(@Nonnull DeleteFileRequest request)'),
                                methodShadow('echoError', 'EchoErrorResponse echoError(@Nonnull EchoErrorRequest request)'),
                        ],
                        imports: [
                                'example.ports.in.rest.dtos.DeleteFileRequest',
                                'example.ports.in.rest.dtos.DeleteFileResponse',
                                'example.ports.in.rest.dtos.DownloadEchoFileRequest',
                                'example.ports.in.rest.dtos.DownloadEchoFileResponse',
                                'example.ports.in.rest.dtos.EchoArraysPostRequest',
                                'example.ports.in.rest.dtos.EchoArraysPostResponse',
                                'example.ports.in.rest.dtos.EchoDateArrayGetRequest',
                                'example.ports.in.rest.dtos.EchoDateArrayGetResponse',
                                'example.ports.in.rest.dtos.EchoDefaultsPostRequest',
                                'example.ports.in.rest.dtos.EchoDefaultsPostResponse',
                                'example.ports.in.rest.dtos.EchoEmptyRequest',
                                'example.ports.in.rest.dtos.EchoEmptyResponse',
                                'example.ports.in.rest.dtos.EchoErrorRequest',
                                'example.ports.in.rest.dtos.EchoErrorResponse',
                                'example.ports.in.rest.dtos.EchoGetRequest',
                                'example.ports.in.rest.dtos.EchoGetResponse',
                                'example.ports.in.rest.dtos.EchoPostRequest',
                                'example.ports.in.rest.dtos.EchoPostResponse',
                                'example.ports.in.rest.dtos.UploadEchoFileRequest',
                                'example.ports.in.rest.dtos.UploadEchoFileResponse',
                                'example.ports.in.rest.dtos.UploadEchoFileWithFormRequest',
                                'example.ports.in.rest.dtos.UploadEchoFileWithFormResponse',
                                'javax.annotation.Nonnull',
                        ],
                ),
        ]
    }

    @Override
    List<String> expectedClassesForCommons() {
        ['commons.EnumValue',
         'commons.MapRawValueSerializer',
         'commons.RestResponse',
         'commons.RestResponse200',
         'commons.RestResponse200NoContent',
         'commons.RestResponse204NoContent',
         'commons.RestResponse400',
         'commons.RestResponse404NoContent',
         'commons.RestResponseBase',
         'commons.RestResponseOther',
         'commons.SerializationUtils',
         'commons.validator.BoolExp',
         'commons.validator.Checker',
         'commons.validator.Condition',
         'commons.validator.ConditionResolver',
         'commons.validator.ConditionalChecker',
         'commons.validator.DictionaryChecker',
         'commons.validator.DictionaryCode',
         'commons.validator.ErrorCode',
         'commons.validator.Field',
         'commons.validator.OperationId',
         'commons.validator.ValidationContext',
         'commons.validator.ValidationError',
         'commons.validator.ValidationResult',
         'commons.validator.ValidationResultMapper',
         'commons.validator.Validator',
         'utils-project/src/main/java-gen/commons/validator/CommonCheckers.java',
        ]
    }

    @Override
    List<String> expectedClassesForCommonsApi() {
        ['example.commons.adapters.in.rest.dtos.ErrorDescriptionRdto',
         'example.commons.adapters.in.rest.dtos.ErrorDetailRdto',
         'example.commons.adapters.in.rest.dtos.ErrorItemRdto',
         'example.commons.adapters.in.rest.dtos.SimpleObjectRdto',
         'example.commons.adapters.in.rest.mappers.ErrorDescriptionMapper',
         'example.commons.adapters.in.rest.mappers.ErrorDetailMapper',
         'example.commons.adapters.in.rest.mappers.ErrorItemMapper',
         'example.commons.adapters.in.rest.mappers.SimpleObjectMapper',
         'example.commons.adapters.in.rest.validators.SimpleObjectValidator',
         'example.commons.ports.in.rest.dtos.ErrorDescriptionDto',
         'example.commons.ports.in.rest.dtos.ErrorDetailDto',
         'example.commons.ports.in.rest.dtos.ErrorItemDto',
         'example.commons.ports.in.rest.dtos.ReusableEnumEnum',
         'example.commons.ports.in.rest.dtos.SimpleObjectDto',
        ]
    }

    @Override
    List<String> expectedClassesForExampleApi() {
        ['example.adapters.in.rest.dtos.DeleteFileRrequest',
         'example.adapters.in.rest.dtos.DownloadEchoFileRrequest',
         'example.adapters.in.rest.dtos.EchoArraysBodyRdto',
         'example.adapters.in.rest.dtos.EchoArraysPostRrequest',
         'example.adapters.in.rest.dtos.EchoBodyRdto',
         'example.adapters.in.rest.dtos.EchoDateArrayGetRrequest',
         'example.adapters.in.rest.dtos.EchoDefaultsBodyRdto',
         'example.adapters.in.rest.dtos.EchoDefaultsPostRrequest',
         'example.adapters.in.rest.dtos.EchoEmptyRrequest',
         'example.adapters.in.rest.dtos.EchoErrorRrequest',
         'example.adapters.in.rest.dtos.EchoGetBodyRdto',
         'example.adapters.in.rest.dtos.EchoGetRrequest',
         'example.adapters.in.rest.dtos.EchoPostRrequest',
         'example.adapters.in.rest.dtos.ExtendedObjectRdto',
         'example.adapters.in.rest.dtos.UploadEchoFileRrequest',
         'example.adapters.in.rest.dtos.UploadEchoFileWithFormRrequest',
         'example.adapters.in.rest.echo.DeleteFileController',
         'example.adapters.in.rest.echo.DeleteFileResponseMapper',
         'example.adapters.in.rest.echo.DeleteFileValidator',
         'example.adapters.in.rest.echo.DownloadEchoFileController',
         'example.adapters.in.rest.echo.DownloadEchoFileResponseMapper',
         'example.adapters.in.rest.echo.DownloadEchoFileValidator',
         'example.adapters.in.rest.echo.EchoArraysPostController',
         'example.adapters.in.rest.echo.EchoArraysPostResponseMapper',
         'example.adapters.in.rest.echo.EchoArraysPostValidator',
         'example.adapters.in.rest.echo.EchoDateArrayGetController',
         'example.adapters.in.rest.echo.EchoDateArrayGetResponseMapper',
         'example.adapters.in.rest.echo.EchoDateArrayGetValidator',
         'example.adapters.in.rest.echo.EchoDefaultsPostController',
         'example.adapters.in.rest.echo.EchoDefaultsPostResponseMapper',
         'example.adapters.in.rest.echo.EchoDefaultsPostValidator',
         'example.adapters.in.rest.echo.EchoEmptyController',
         'example.adapters.in.rest.echo.EchoEmptyResponseMapper',
         'example.adapters.in.rest.echo.EchoEmptyValidator',
         'example.adapters.in.rest.echo.EchoErrorController',
         'example.adapters.in.rest.echo.EchoErrorResponseMapper',
         'example.adapters.in.rest.echo.EchoErrorValidator',
         'example.adapters.in.rest.echo.EchoGetController',
         'example.adapters.in.rest.echo.EchoGetResponseMapper',
         'example.adapters.in.rest.echo.EchoGetValidator',
         'example.adapters.in.rest.echo.EchoPostController',
         'example.adapters.in.rest.echo.EchoPostResponseMapper',
         'example.adapters.in.rest.echo.EchoPostValidator',
         'example.adapters.in.rest.echo.UploadEchoFileController',
         'example.adapters.in.rest.echo.UploadEchoFileResponseMapper',
         'example.adapters.in.rest.echo.UploadEchoFileValidator',
         'example.adapters.in.rest.echo.UploadEchoFileWithFormController',
         'example.adapters.in.rest.echo.UploadEchoFileWithFormResponseMapper',
         'example.adapters.in.rest.echo.UploadEchoFileWithFormValidator',
         'example.adapters.in.rest.mappers.DeleteFileRequestMapper',
         'example.adapters.in.rest.mappers.DownloadEchoFileRequestMapper',
         'example.adapters.in.rest.mappers.EchoArraysBodyMapper',
         'example.adapters.in.rest.mappers.EchoArraysPostRequestMapper',
         'example.adapters.in.rest.mappers.EchoBodyMapper',
         'example.adapters.in.rest.mappers.EchoDateArrayGetRequestMapper',
         'example.adapters.in.rest.mappers.EchoDefaultsBodyMapper',
         'example.adapters.in.rest.mappers.EchoDefaultsPostRequestMapper',
         'example.adapters.in.rest.mappers.EchoEmptyRequestMapper',
         'example.adapters.in.rest.mappers.EchoErrorRequestMapper',
         'example.adapters.in.rest.mappers.EchoGetBodyMapper',
         'example.adapters.in.rest.mappers.EchoGetRequestMapper',
         'example.adapters.in.rest.mappers.EchoPostRequestMapper',
         'example.adapters.in.rest.mappers.ExtendedObjectMapper',
         'example.adapters.in.rest.mappers.UploadEchoFileRequestMapper',
         'example.adapters.in.rest.mappers.UploadEchoFileWithFormRequestMapper',
         'example.adapters.in.rest.validators.EchoArraysBodyValidator',
         'example.adapters.in.rest.validators.EchoBodyValidator',
         'example.adapters.in.rest.validators.EchoDefaultsBodyValidator',
         'example.adapters.in.rest.validators.ExtendedObjectValidator',
         'example.application.EchoFacadeImpl',
         'example.ports.in.rest.EchoFacade',
         'example.ports.in.rest.dtos.DefaultEnumEnum',
         'example.ports.in.rest.dtos.DeleteFileRequest',
         'example.ports.in.rest.dtos.DeleteFileResponse',
         'example.ports.in.rest.dtos.DownloadEchoFileRequest',
         'example.ports.in.rest.dtos.DownloadEchoFileResponse',
         'example.ports.in.rest.dtos.EchoArraysBodyDto',
         'example.ports.in.rest.dtos.EchoArraysPostRequest',
         'example.ports.in.rest.dtos.EchoArraysPostResponse',
         'example.ports.in.rest.dtos.EchoBodyDto',
         'example.ports.in.rest.dtos.EchoDateArrayGetRequest',
         'example.ports.in.rest.dtos.EchoDateArrayGetResponse',
         'example.ports.in.rest.dtos.EchoDefaultsBodyDto',
         'example.ports.in.rest.dtos.EchoDefaultsPostRequest',
         'example.ports.in.rest.dtos.EchoDefaultsPostResponse',
         'example.ports.in.rest.dtos.EchoEmptyRequest',
         'example.ports.in.rest.dtos.EchoEmptyResponse',
         'example.ports.in.rest.dtos.EchoErrorRequest',
         'example.ports.in.rest.dtos.EchoErrorResponse',
         'example.ports.in.rest.dtos.EchoGetBodyDto',
         'example.ports.in.rest.dtos.EchoGetRequest',
         'example.ports.in.rest.dtos.EchoGetResponse',
         'example.ports.in.rest.dtos.EchoPostRequest',
         'example.ports.in.rest.dtos.EchoPostResponse',
         'example.ports.in.rest.dtos.EnumTypeEnum',
         'example.ports.in.rest.dtos.ExtendedObjectDto',
         'example.ports.in.rest.dtos.UploadEchoFileRequest',
         'example.ports.in.rest.dtos.UploadEchoFileResponse',
         'example.ports.in.rest.dtos.UploadEchoFileWithFormRequest',
         'example.ports.in.rest.dtos.UploadEchoFileWithFormResponse',
        ]
    }
}
