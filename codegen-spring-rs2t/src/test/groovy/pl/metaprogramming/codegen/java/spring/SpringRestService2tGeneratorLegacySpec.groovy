/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.fixtures.CodegenParamsProvider
import pl.metaprogramming.model.data.DataType
import pl.metaprogramming.utils.CheckUtils
import pl.metaprogramming.utils.ClassShadow

import static pl.metaprogramming.utils.JavaCodeGenerationTestUtils.methodShadow

class SpringRestService2tGeneratorLegacySpec extends SpringRestService2tGeneratorSpecification {

    @Override
    CodegenParams setCodegenParams(CodegenParams params) {
        params
                .set(new SpringRestParams(
                        controllerPerOperation: false,
                        staticFactoryMethodForRestResponse: false,
                ))
                .set(new JavaParams()
                        .dependencyInjectionBuilder(Spring.diWithAutowiredBuilder())
                        .generatedAnnotationClass('javax.annotation.Generated')
                        .nameMapper(JAVA_NAME_MAPPER))
                .set(CodegenParamsProvider.makeValidationParams()
                        .formatValidator('amount', 'di-bean:example.commons.validator.AmountChecker')
                        .securityValidation(false)
                        .throwExceptionIfValidationFailed(false))

    }

    @Override
    void configure(SpringRestService2tGenerator it) {
        super.configure(it)
        it.dataTypeMapper.set(DataType.BINARY, Spring.resource().canonicalName)
    }

    @Override
    protected List<ClassShadow> classesToCheckForCommons() {
        fixClassShadows(super.classesToCheckForCommons()).each {
            if (it.name.endsWith('RestResponseBase')) {
                it.fields = [
                        '@Getter private Integer status',
                        '@Getter private Object body',
                        '@Getter private final Map<String,String> headers = new TreeMap<>()',]
                it.imports = [
                        'java.util.Map',
                        'java.util.TreeMap',
                        'javax.annotation.Nonnull',
                        'lombok.Getter',]
            }
        }
    }

    @Override
    protected String generatedAnnotationClass() {
        "javax.annotation.Generated"
    }

    @Override
    List<ClassShadow> classesToCheckForCommonsApi() {
        fixClassShadows(super.classesToCheckForCommonsApi())
    }

    @Override
    List<ClassShadow> classesToCheckForExampleApi() {
        def result = fixClassShadows(super.classesToCheckForExampleApi())
        result.removeAll { it.name.endsWith('Controller') }
        result.addAll(additionalClassesToCheck())
        result
    }

    private List<ClassShadow> fixClassShadows(List<ClassShadow> result) {
        result.each {
            it.annotations.remove('@RequiredArgsConstructor')
            def diFields = it.fields.findAll { it.startsWith('private final') || it.startsWith('@Qualifier') }
            if (diFields) {
                it.fields.removeAll(diFields)
                it.fields.addAll(diFields.collect { it.replace('private final', '@Autowired private') })
                it.imports.remove('lombok.RequiredArgsConstructor')
                it.imports.add('org.springframework.beans.factory.annotation.Autowired')
            }
            if (it.name.endsWith('Validator')
                    && it.name != 'example.commons.adapters.in.rest.validators.SimpleObjectValidator') {
                it.name = replacePackage(it.name, 'example.adapters.in.rest.validators')
                it.imports.removeAll { it.startsWith('example.adapters.in.rest.validators') }
            }
            if (it.name.endsWith('Mapper')
                    && !it.name.endsWith('ErrorDescriptionMapper')
                    && !it.name.endsWith('ValidationResultMapper')) {
                it.name = replacePackage(it.name, 'example.adapters.in.rest.mappers')
                it.imports.removeAll { it.startsWith('example.adapters.in.rest.mappers') }
            }
            if (it.name.endsWith('Response') && it.name != 'commons.RestResponse') {
                it.annotations.remove('@ParametersAreNonnullByDefault')
                it.imports.remove('javax.annotation.ParametersAreNonnullByDefault')
                it.methods.removeAll { it.signature.contains(' static ') }
            }
            fixBinaryDataType(it)
        }
        result
    }

    protected void fixBinaryDataType(ClassShadow shadow) {
        if (shadow.name.endsWith('UploadEchoFileRequest')) {
            shadow.fields.remove('@Nonnull private byte[] requestBody')
            shadow.fields.add('@Nonnull private Resource requestBody')
            shadow.imports.add('org.springframework.core.io.Resource')
        }
        if (shadow.name.endsWith('UploadEchoFileWithFormRequest')) {
            shadow.fields.remove('@Nonnull private byte[] file')
            shadow.fields.add('@Nonnull private Resource file')
            shadow.imports.add('org.springframework.core.io.Resource')
        }
        if (shadow.name.endsWith('DownloadEchoFileResponse')) {
            shadow.classHeader = 'public class DownloadEchoFileResponse extends RestResponseBase<DownloadEchoFileResponse> implements RestResponse200<DownloadEchoFileResponse,Resource>, RestResponse404NoContent<DownloadEchoFileResponse>, RestResponseOther<DownloadEchoFileResponse,ErrorDescriptionDto>'
            shadow.imports.add('org.springframework.core.io.Resource')
        }
        if (shadow.name.endsWith('UploadEchoFileRequestMapper')) {
            shadow.methods.each {
                it.implBody = it.implBody.replace(
                        '.setRequestBody(SerializationUtils.toBytes(value.getRequestBody()))',
                        '.setRequestBody(value.getRequestBody())')
            }
        }
        if (shadow.name.endsWith('UploadEchoFileWithFormRequestMapper')) {
            shadow.methods.each {
                it.implBody = it.implBody.replace(
                        '.setFile(SerializationUtils.toBytes(value.getFile()))',
                        '.setFile(value.getFile())')
            }
        }
        if (shadow.name.endsWith('SerializationUtils')) {
            shadow.imports.removeAll(['lombok.SneakyThrows', 'org.apache.commons.io.IOUtils', 'org.springframework.core.io.Resource'])
            shadow.methods.removeIf({ it.signature == 'public static byte[] toBytes(Resource value)' })
        }
    }

    protected String replacePackage(String canonicalName, String newPackage) {
        newPackage + canonicalName.substring(canonicalName.lastIndexOf('.'))
    }

    protected List<ClassShadow> additionalClassesToCheck() {
        [new ClassShadow(
                name: 'example.adapters.in.rest.EchoController',
                annotations: CheckUtils.REST_CONTROLLER_ANNOTATIONS,
                classHeader: 'public class EchoController',
                fields: [
                        '@Autowired private DeleteFileRequestMapper deleteFileRequestMapper',
                        '@Autowired private DeleteFileResponseMapper deleteFileResponseMapper',
                        '@Autowired private DeleteFileValidator deleteFileValidator',
                        '@Autowired private DownloadEchoFileRequestMapper downloadEchoFileRequestMapper',
                        '@Autowired private DownloadEchoFileResponseMapper downloadEchoFileResponseMapper',
                        '@Autowired private DownloadEchoFileValidator downloadEchoFileValidator',
                        '@Autowired private EchoArraysPostRequestMapper echoArraysPostRequestMapper',
                        '@Autowired private EchoArraysPostResponseMapper echoArraysPostResponseMapper',
                        '@Autowired private EchoArraysPostValidator echoArraysPostValidator',
                        '@Autowired private EchoDateArrayGetRequestMapper echoDateArrayGetRequestMapper',
                        '@Autowired private EchoDateArrayGetResponseMapper echoDateArrayGetResponseMapper',
                        '@Autowired private EchoDateArrayGetValidator echoDateArrayGetValidator',
                        '@Autowired private EchoDefaultsPostRequestMapper echoDefaultsPostRequestMapper',
                        '@Autowired private EchoDefaultsPostResponseMapper echoDefaultsPostResponseMapper',
                        '@Autowired private EchoDefaultsPostValidator echoDefaultsPostValidator',
                        '@Autowired private EchoEmptyRequestMapper echoEmptyRequestMapper',
                        '@Autowired private EchoEmptyResponseMapper echoEmptyResponseMapper',
                        '@Autowired private EchoEmptyValidator echoEmptyValidator',
                        '@Autowired private EchoErrorRequestMapper echoErrorRequestMapper',
                        '@Autowired private EchoErrorResponseMapper echoErrorResponseMapper',
                        '@Autowired private EchoErrorValidator echoErrorValidator',
                        '@Autowired private EchoFacade echoFacade',
                        '@Autowired private EchoGetRequestMapper echoGetRequestMapper',
                        '@Autowired private EchoGetResponseMapper echoGetResponseMapper',
                        '@Autowired private EchoGetValidator echoGetValidator',
                        '@Autowired private EchoPostRequestMapper echoPostRequestMapper',
                        '@Autowired private EchoPostResponseMapper echoPostResponseMapper',
                        '@Autowired private EchoPostValidator echoPostValidator',
                        '@Autowired private UploadEchoFileRequestMapper uploadEchoFileRequestMapper',
                        '@Autowired private UploadEchoFileResponseMapper uploadEchoFileResponseMapper',
                        '@Autowired private UploadEchoFileValidator uploadEchoFileValidator',
                        '@Autowired private UploadEchoFileWithFormRequestMapper uploadEchoFileWithFormRequestMapper',
                        '@Autowired private UploadEchoFileWithFormResponseMapper uploadEchoFileWithFormResponseMapper',
                        '@Autowired private UploadEchoFileWithFormValidator uploadEchoFileWithFormValidator',
                        '@Autowired private ValidationResultMapper validationResultMapper',
                ],
                methods: [
                        methodShadow(
                                'echoPost',
                                'public ResponseEntity echoPost(@RequestHeader(value = "Authorization", required = false) String authorizationParam, @RequestHeader(value = "X-Correlation-ID", required = false) String correlationIdParam, @RequestHeader(value = "timestamp", required = false) String timestampParam, @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam, @RequestBody EchoBodyRdto requestBody)',
                                ['EchoPostRrequest request = echoPostRequestMapper.map2EchoPostRrequest(authorizationParam, correlationIdParam, timestampParam, inlineHeaderParam, requestBody);',
                                 'ValidationResult validationResult = echoPostValidator.validate(request);',
                                 'return validationResult.isValid()',
                                 '        ? echoPostResponseMapper.map(echoFacade.echoPost(echoPostRequestMapper.map2EchoPostRequest(request)))',
                                 '        : validationResultMapper.map(validationResult);',
                                ],
                                ['@PostMapping(value="/api/v1/echo",produces={"application/json"},consumes={"application/json"})']),
                        methodShadow(
                                'echoArraysPost',
                                'public ResponseEntity echoArraysPost(@RequestHeader(value = "Authorization", required = false) String authorizationParam, @RequestHeader(value = "Inline-Header-Param", required = false) String inlineHeaderParam, @RequestBody List<EchoArraysBodyRdto> requestBody)',
                                ['EchoArraysPostRrequest request = echoArraysPostRequestMapper.map2EchoArraysPostRrequest(authorizationParam, inlineHeaderParam, requestBody);',
                                 'ValidationResult validationResult = echoArraysPostValidator.validate(request);',
                                 'return validationResult.isValid()',
                                 '        ? echoArraysPostResponseMapper.map(echoFacade.echoArraysPost(echoArraysPostRequestMapper.map2EchoArraysPostRequest(request)))',
                                 '        : validationResultMapper.map(validationResult);',
                                ],
                                ['@PostMapping(value="/api/v1/echo-arrays",produces={"application/json"},consumes={"application/json"})']),
                        methodShadow(
                                'echoEmpty',
                                'public ResponseEntity echoEmpty()',
                                ['EchoEmptyRrequest request = new EchoEmptyRrequest();',
                                 'ValidationResult validationResult = echoEmptyValidator.validate(request);',
                                 'return validationResult.isValid()',
                                 '        ? echoEmptyResponseMapper.map(echoFacade.echoEmpty(echoEmptyRequestMapper.map2EchoEmptyRequest(request)))',
                                 '        : validationResultMapper.map(validationResult);',
                                ],
                                ['@GetMapping("/api/v1/echo-empty")']),
                ],
                imports: [
                        'commons.validator.ValidationResult',
                        'commons.validator.ValidationResultMapper',
                        'example.adapters.in.rest.dtos.DeleteFileRrequest',
                        'example.adapters.in.rest.dtos.DownloadEchoFileRrequest',
                        'example.adapters.in.rest.dtos.EchoArraysBodyRdto',
                        'example.adapters.in.rest.dtos.EchoArraysPostRrequest',
                        'example.adapters.in.rest.dtos.EchoBodyRdto',
                        'example.adapters.in.rest.dtos.EchoDateArrayGetRrequest',
                        'example.adapters.in.rest.dtos.EchoDefaultsBodyRdto',
                        'example.adapters.in.rest.dtos.EchoDefaultsPostRrequest',
                        'example.adapters.in.rest.dtos.EchoEmptyRrequest',
                        'example.adapters.in.rest.dtos.EchoErrorRrequest',
                        'example.adapters.in.rest.dtos.EchoGetRrequest',
                        'example.adapters.in.rest.dtos.EchoPostRrequest',
                        'example.adapters.in.rest.dtos.UploadEchoFileRrequest',
                        'example.adapters.in.rest.dtos.UploadEchoFileWithFormRrequest',
                        'example.adapters.in.rest.mappers.DeleteFileRequestMapper',
                        'example.adapters.in.rest.mappers.DeleteFileResponseMapper',
                        'example.adapters.in.rest.mappers.DownloadEchoFileRequestMapper',
                        'example.adapters.in.rest.mappers.DownloadEchoFileResponseMapper',
                        'example.adapters.in.rest.mappers.EchoArraysPostRequestMapper',
                        'example.adapters.in.rest.mappers.EchoArraysPostResponseMapper',
                        'example.adapters.in.rest.mappers.EchoDateArrayGetRequestMapper',
                        'example.adapters.in.rest.mappers.EchoDateArrayGetResponseMapper',
                        'example.adapters.in.rest.mappers.EchoDefaultsPostRequestMapper',
                        'example.adapters.in.rest.mappers.EchoDefaultsPostResponseMapper',
                        'example.adapters.in.rest.mappers.EchoEmptyRequestMapper',
                        'example.adapters.in.rest.mappers.EchoEmptyResponseMapper',
                        'example.adapters.in.rest.mappers.EchoErrorRequestMapper',
                        'example.adapters.in.rest.mappers.EchoErrorResponseMapper',
                        'example.adapters.in.rest.mappers.EchoGetRequestMapper',
                        'example.adapters.in.rest.mappers.EchoGetResponseMapper',
                        'example.adapters.in.rest.mappers.EchoPostRequestMapper',
                        'example.adapters.in.rest.mappers.EchoPostResponseMapper',
                        'example.adapters.in.rest.mappers.UploadEchoFileRequestMapper',
                        'example.adapters.in.rest.mappers.UploadEchoFileResponseMapper',
                        'example.adapters.in.rest.mappers.UploadEchoFileWithFormRequestMapper',
                        'example.adapters.in.rest.mappers.UploadEchoFileWithFormResponseMapper',
                        'example.adapters.in.rest.validators.DeleteFileValidator',
                        'example.adapters.in.rest.validators.DownloadEchoFileValidator',
                        'example.adapters.in.rest.validators.EchoArraysPostValidator',
                        'example.adapters.in.rest.validators.EchoDateArrayGetValidator',
                        'example.adapters.in.rest.validators.EchoDefaultsPostValidator',
                        'example.adapters.in.rest.validators.EchoEmptyValidator',
                        'example.adapters.in.rest.validators.EchoErrorValidator',
                        'example.adapters.in.rest.validators.EchoGetValidator',
                        'example.adapters.in.rest.validators.EchoPostValidator',
                        'example.adapters.in.rest.validators.UploadEchoFileValidator',
                        'example.adapters.in.rest.validators.UploadEchoFileWithFormValidator',
                        'example.ports.in.rest.EchoFacade',
                        'java.util.List',
                        'org.springframework.beans.factory.annotation.Autowired',
                        'org.springframework.core.io.Resource',
                        'org.springframework.http.ResponseEntity',
                        'org.springframework.web.bind.annotation.DeleteMapping',
                        'org.springframework.web.bind.annotation.GetMapping',
                        'org.springframework.web.bind.annotation.PathVariable',
                        'org.springframework.web.bind.annotation.PostMapping',
                        'org.springframework.web.bind.annotation.RequestBody',
                        'org.springframework.web.bind.annotation.RequestHeader',
                        'org.springframework.web.bind.annotation.RequestParam',
                        'org.springframework.web.bind.annotation.RequestPart',
                        'org.springframework.web.bind.annotation.RestController',
                        'org.springframework.web.multipart.MultipartFile',
                ]
        ),
        ]
    }

    @Override
    List<String> expectedClassesForExampleApi() {
        def result = super.expectedClassesForExampleApi()
        def classesToFix = result.findAll { it.startsWith('example.adapters.in.rest.books') || it.startsWith('example.adapters.in.rest.echo') }
        result.removeAll(classesToFix)
        result.addAll(classesToFix
                .findAll { it.endsWith('Validator') }
                .collect { replacePackage(it, 'example.adapters.in.rest.validators') })
        result.addAll(classesToFix
                .findAll { it.endsWith('Mapper') }
                .collect { replacePackage(it, 'example.adapters.in.rest.mappers') })
        result.addAll([
                'example.adapters.in.rest.EchoController'
        ])
        result
    }
}
