/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.CodegenParams
import pl.metaprogramming.codegen.TypeOfCode
import spock.lang.Specification

import java.lang.reflect.Modifier

class SpringCommonsGeneratorConfiguratorSpec extends Specification {

    def "configured type of codes should match declared TOC.* fields"() {
        when:
        def generator = new SpringCommonsGenerator()
        generator.setParams(new CodegenParams())
        generator.init()
        def declaredTypeOfCodes = getTypeOfCodeFields(SpringCommonsGenerator.TOC.class)
        def configuredTypeOfCodes = generator.codeBuilders.keySet()

        then:
        (configuredTypeOfCodes - declaredTypeOfCodes).isEmpty()
        (declaredTypeOfCodes - configuredTypeOfCodes).isEmpty()
    }

    Set getTypeOfCodeFields(Class clazz) {
        clazz.declaredFields
                .findAll { Modifier.isStatic(it.modifiers) && Modifier.isPublic(it.modifiers) && it.get(null) instanceof TypeOfCode }
                .collect { it.get(null) }
                .toSet()
    }
}
