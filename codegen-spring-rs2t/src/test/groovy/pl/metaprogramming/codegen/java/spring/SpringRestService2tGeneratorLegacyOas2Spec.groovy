/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.utils.ClassShadow
import pl.metaprogramming.fixtures.ExampleApiModels

class SpringRestService2tGeneratorLegacyOas2Spec extends SpringRestService2tGeneratorLegacySpec {
    @Override
    ExampleApiModels.OpenapiVer getOpenapiVer() {
        ExampleApiModels.OpenapiVer.V2
    }

    @Override
    List<ClassShadow> classesToCheckForExampleApi() {
        def result = super.classesToCheckForExampleApi()
        result.each {
            if (it.name.endsWith('ExtendedObjectValidator')) {
                it.fields.remove('@Autowired private ExtendedObjectEnumChecker extendedObjectEnumChecker')
                it.methods[0].with {
                    moveLine('ctx.check(extendedObjectChecker);', 4)
                    replaceLine(
                            'ctx.check(FIELD_EO_ENUM_REUSABLE, allow(ReusableEnumEnum.values()), extendedObjectEnumChecker);',
                            'ctx.check(FIELD_EO_ENUM_REUSABLE, allow(ReusableEnumEnum.values()), ctx.getBean(ExtendedObjectEnumChecker.class));')
                }
            }
            if (it.name.endsWith('EchoBodyValidator')) {
                it.imports.removeAll(['static commons.validator.ErrorCode.*',
                                      'org.springframework.beans.factory.annotation.Qualifier'])
                it.imports.add('example.commons.adapters.in.rest.validators.SimpleObjectCustomValidator')
                it.fields.removeAll(['@Autowired private UserDataValidationBean userDataValidationBean',
                                     '@Qualifier("SIMPLE_OBJECT_CUSTOM_CONSTRAINT") @Autowired private Checker<SimpleObjectRdto> simpleObjectCustomConstraint'])
                it.fields.addAll(['@Autowired private ValidationBeanFactory validationBeanFactory',
                                  '@Autowired private SimpleObjectCustomValidator simpleObjectCustomValidator'])
                it.methods[0].with {
                    addLine('ctx.setBean(ExtendedObjectEnumChecker.class, validationBeanFactory::createExtendedObjectEnumChecker);')
                    replaceLine('ctx.check(FIELD_PROP_AMOUNT, FIELD_PROP_AMOUNT_PATTERN, userDataValidationBean::checkAmountByUser, Checkers.AMOUNT_SCALE_CHECKER);',
                            'ctx.check(FIELD_PROP_AMOUNT, FIELD_PROP_AMOUNT_PATTERN, ctx.getBean(UserDataValidationBean.class)::checkAmountByUser, Checkers.AMOUNT_SCALE_CHECKER);')
                    replaceLine('ctx.check(lt(FIELD_PROP_INT, FIELD_PROP_INT_SECOND, SerializationUtils::toInteger).withError(COMPARE_PROP_INT_FAILED));',
                            'ctx.check(lt(FIELD_PROP_INT, FIELD_PROP_INT_SECOND, SerializationUtils::toInteger));')
                    replaceLine('ctx.check(FIELD_PROP_OBJECT, simpleObjectValidator, simpleObjectCustomConstraint);',
                            'ctx.check(FIELD_PROP_OBJECT, simpleObjectValidator, simpleObjectCustomValidator);')
                }
            }
            if (it.name.endsWith('EchoPostValidator')) {
                it.fields.add('@Autowired private ValidationBeanFactory validationBeanFactory')
                it.methods[0].addLine('ctx.setBean(UserDataValidationBean.class, validationBeanFactory::createUserDataValidationBean);', 1)
            }
            if (it.name.endsWith('EchoArraysBodyValidator')) {
                it.imports.add('example.commons.adapters.in.rest.validators.SimpleObjectCustomValidator')
                it.fields.add('@Autowired private SimpleObjectCustomValidator simpleObjectCustomValidator')
                it.methods[0].with {
                    replaceLine('ctx.check(FIELD_PROP_OBJECT_LIST_OF_LIST, items(items(simpleObjectValidator, simpleObjectCustomConstraint)));',
                            'ctx.check(FIELD_PROP_OBJECT_LIST_OF_LIST, items(items(simpleObjectValidator, simpleObjectCustomValidator::check)));')
                }
            }
        }
        result
    }
}
