/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.java.spring

import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.GeneratorSpecification
import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.codegen.TypeOfCodeWithNoModel
import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.java.libs.Spring
import pl.metaprogramming.codegen.java.validation.ValidationParams
import pl.metaprogramming.utils.CheckUtils
import pl.metaprogramming.utils.ClassShadow
import pl.metaprogramming.utils.RestMetaModels

import static pl.metaprogramming.utils.JavaCodeGenerationTestUtils.methodShadow

class SpringRestService2tGeneratorRestApiSpec extends GeneratorSpecification {

    static String MODULE_COMMONS_API = 'commons-api'
    static String MODULE_ECHO_API = 'echo-api'

    static TypeOfCode CUSTOM_CLASS_TYPE = new TypeOfCodeWithNoModel('OPERATION_CUSTOM_CLASS')

    @Override
    Codegen makeCodegen() {
        def codegen = new Codegen()
        codegen.params.set(new SpringRestParams(
                        controllerPerOperation: false,
                        staticFactoryMethodForRestResponse: false,
                        payloadField: 'payload'
                ))
        codegen.params.set(new JavaParams(dependencyInjectionBuilder: Spring.diWithAutowiredBuilder()))
        codegen.params.set(new ValidationParams(throwExceptionIfValidationFailed: false))

        codegen.generate(new SpringCommonsGenerator()) {
                    it.rootPackage('commons')
                }
                .generate(new SpringRestService2tGenerator()) {
                    it.model = RestMetaModels.commonsRestServices
                    it.rootPackage('echo')
                }
                .generate(new SpringRestService2tGenerator()) {
                    it.model = RestMetaModels.echoRestServices
                    it.rootPackage('echo')
                    it.typeOfCodesForOperation.add(CUSTOM_CLASS_TYPE)
                    it.typeOfCode(CUSTOM_CLASS_TYPE) {
                        it.forceGeneration = true
                        it.className {
                            it.resolver { it.code }
                            it.suffix = 'OperationAction'
                        }
                        it.onDecoration {
                            it.addAnnotation(Spring.prototypeScope())
                        }
                    }
                    it.typeOfCode(CUSTOM_CLASS_TYPE) { it.packageName.tail = "op" }
                }
    }

    @Override
    Map<String, List<ClassShadow>> makeClassesToCheck() {
        [(MODULE_ECHO_API)   : classesToCheckForEchoApi(),
         (MODULE_COMMONS_API): classesToCheckForCommonsApi()]
    }

    @Override
    Map<String, List<String>> makeExpectedClasses() {
        [(MODULE_COMMONS)    : expectedClassesForCommons(),
         (MODULE_COMMONS_API): expectedClassesForCommonsApi(),
         (MODULE_ECHO_API)   : expectedClassesForEchoApi()
        ]
    }

    @Override
    protected List<ClassShadow> classesToCheckForCommons() {
        def result = super.classesToCheckForCommons()
        result.removeAll {
            [
                    'commons.SerializationUtils',
                    'commons.RestResponseBase']
                    .contains(it.name)
        }
        result
    }

    def classesToCheckForCommonsApi() {
        [new ClassShadow(
                name: 'echo.ports.in.rest.dtos.BadRequestDto',
                classHeader: 'public class BadRequestDto',
                annotations: CheckUtils.DTO_ANNOTATIONS,
                imports: CheckUtils.DTO_IMPORTS + CheckUtils.NULLABLE_IMPORTS + ['java.util.List'],
                fields: ['@Nullable private List<FieldValidationDto> errors']
        ),
         new ClassShadow(
                 name: 'echo.adapters.in.rest.dtos.BadRequestRdto',
                 classHeader: 'public class BadRequestRdto',
                 annotations: CheckUtils.RAW_DTO_ANNOTATIONS,
                 fields: ['@JsonProperty("errors") private List<FieldValidationRdto> errors'],
                 imports: [
                         'com.fasterxml.jackson.annotation.JsonAutoDetect',
                         'com.fasterxml.jackson.annotation.JsonProperty',
                         'java.util.List',
                         'lombok.Data',
                         'lombok.NoArgsConstructor',
                         'lombok.experimental.Accessors',
                 ],
         ),
         new ClassShadow(
                 name: 'echo.adapters.in.rest.mappers.BadRequestMapper',
                 classHeader: 'public class BadRequestMapper',
                 annotations: CheckUtils.COMPONENT_ANNOTATIONS_LEGACY,
                 fields: [
                         '@Autowired private FieldValidationMapper fieldValidationMapper'],
                 methods: [
                         methodShadow('map2BadRequestRdto', 'public BadRequestRdto map2BadRequestRdto(BadRequestDto value)',
                                 ['return value == null ? null : map(new BadRequestRdto(), value);']),
                         methodShadow('map', 'public BadRequestRdto map(BadRequestRdto result, BadRequestDto value)',
                                 ['return result',
                                  '        .setErrors(SerializationUtils.transformList(value.getErrors(), fieldValidationMapper::map2FieldValidationRdto))',
                                  '        ;',
                                 ]),
                 ],
                 imports: [
                         'commons.SerializationUtils',
                         'echo.adapters.in.rest.dtos.BadRequestRdto',
                         'echo.ports.in.rest.dtos.BadRequestDto',
                         'org.springframework.beans.factory.annotation.Autowired',
                         'org.springframework.stereotype.Component',
                 ],
         ),
        ]
    }

    List<ClassShadow> classesToCheckForEchoApi() {
        [new ClassShadow(
                name: 'echo.ports.in.rest.dtos.DataFormDto',
                classHeader: 'public class DataFormDto',
                annotations: CheckUtils.DTO_ANNOTATIONS,
                imports: CheckUtils.DTO_IMPORTS + CheckUtils.NONNULL_NULLABLE_IMPORTS + ['java.math.BigDecimal', 'java.time.LocalDate', 'java.util.List'],
                fields: [
                        '@Nonnull private Integer dInt',
                        '@Nonnull private Long dLong',
                        '@Nonnull private String dRString',
                        '@Nullable private BigDecimal dDecimal',
                        '@Nullable private List<Long> dListLong',
                        '@Nullable private LocalDate dDate',
                        '@Nullable private String dOString',
                ]
        ),
         new ClassShadow(
                 name: 'echo.adapters.in.rest.dtos.DataFormRdto',
                 classHeader: 'public class DataFormRdto',
                 annotations: CheckUtils.RAW_DTO_ANNOTATIONS,
                 fields: [
                         '@JsonProperty("d_date") private String dDate',
                         '@JsonProperty("d_decimal") @JsonRawValue private String dDecimal',
                         '@JsonProperty("d_int") @JsonRawValue private String dInt',
                         '@JsonProperty("d_list_long") @JsonRawValue private List<String> dListLong',
                         '@JsonProperty("d_long") @JsonRawValue private String dLong',
                         '@JsonProperty("d_o_string") private String dOString',
                         '@JsonProperty("d_r_string") private String dRString',
                 ],
                 imports: [
                         'com.fasterxml.jackson.annotation.JsonAutoDetect',
                         'com.fasterxml.jackson.annotation.JsonProperty',
                         'com.fasterxml.jackson.annotation.JsonRawValue',
                         'java.util.List',
                         'lombok.Data',
                         'lombok.NoArgsConstructor',
                         'lombok.experimental.Accessors',
                 ],
         ),
         new ClassShadow(
                 name: 'echo.ports.in.rest.dtos.EchoPostResponse',
                 classHeader: 'public class EchoPostResponse extends RestResponseBase<EchoPostResponse> implements RestResponse200<EchoPostResponse,DataFormDto>, RestResponse400<EchoPostResponse,BadRequestDto>, RestResponse404<EchoPostResponse,ErrorDto>, RestResponse405<EchoPostResponse,ErrorDto>',
                 annotations: CheckUtils.GEN_ANNOTATIONS,
                 fields: [
                         'private static final Collection<Integer> DECLARED_STATUSES = Arrays.asList(200, 400, 404, 405)',
                 ],
                 methods: [
                         methodShadow('getDeclaredStatuses', 'public Collection<Integer> getDeclaredStatuses()',
                                 ['return DECLARED_STATUSES;']),
                         methodShadow('self', 'public EchoPostResponse self()',
                                 ['return this;']),
                 ],
                 imports: [
                         'commons.RestResponse200',
                         'commons.RestResponse400',
                         'commons.RestResponse404',
                         'commons.RestResponse405',
                         'commons.RestResponseBase',
                         'java.util.Arrays',
                         'java.util.Collection',
                 ],
         ),
         new ClassShadow(
                 name: 'echo.adapters.in.rest.EchoController',
                 classHeader: 'public class EchoController',
                 annotations: CheckUtils.REST_CONTROLLER_ANNOTATIONS,
                 fields: [
                         '@Autowired private EchoComplexRequestMapper echoComplexRequestMapper',
                         '@Autowired private EchoComplexResponseMapper echoComplexResponseMapper',
                         '@Autowired private EchoComplexValidator echoComplexValidator',
                         '@Autowired private EchoFacade echoFacade',
                         '@Autowired private EchoGetRequestMapper echoGetRequestMapper',
                         '@Autowired private EchoGetResponseMapper echoGetResponseMapper',
                         '@Autowired private EchoGetValidator echoGetValidator',
                         '@Autowired private EchoPostRequestMapper echoPostRequestMapper',
                         '@Autowired private EchoPostResponseMapper echoPostResponseMapper',
                         '@Autowired private EchoPostValidator echoPostValidator',
                         '@Autowired private ValidationResultMapper validationResultMapper',
                 ],
                 methods: [
                         methodShadow(
                                 'echoGet',
                                 'public ResponseEntity echoGet(@RequestParam(value = "d_r_string", required = false) String dRString, @RequestParam(value = "d_o_string", required = false) String dOString, @RequestParam(value = "d_int", required = false) String dInt, @RequestParam(value = "d_long", required = false) String dLong, @RequestParam(value = "d_list_long", required = false) List<String> dListLong, @RequestParam(value = "d_decimal", required = false) String dDecimal, @RequestParam(value = "d_date", required = false) String dDate)',
                                 ['EchoGetRrequest request = echoGetRequestMapper.map2EchoGetRrequest(dRString, dOString, dInt, dLong, dListLong, dDecimal, dDate);',
                                  'ValidationResult validationResult = echoGetValidator.validate(request);',
                                  'return validationResult.isValid()',
                                  '        ? echoGetResponseMapper.map(echoFacade.echoGet(echoGetRequestMapper.map2EchoGetRequest(request)))',
                                  '        : validationResultMapper.map(validationResult);',
                                 ],
                                 ['@GetMapping(value="/api/v1/echo",produces={"application/json"})']),
                         methodShadow('echoPost', 'public ResponseEntity echoPost(@RequestBody String payload, @RequestBody DataFormRdto requestBody)',
                                 ['EchoPostRrequest request = echoPostRequestMapper.map2EchoPostRrequest(payload, requestBody);',
                                  'ValidationResult validationResult = echoPostValidator.validate(request);',
                                  'return validationResult.isValid()',
                                  '        ? echoPostResponseMapper.map(echoFacade.echoPost(echoPostRequestMapper.map2EchoPostRequest(request)))',
                                  '        : validationResultMapper.map(validationResult);',
                                 ],
                                 ['@PostMapping(value="/api/v1/echo",produces={"application/json"},consumes={"application/json"})']),
                 ],
                 imports: [
                         'commons.validator.ValidationResult',
                         'commons.validator.ValidationResultMapper',
                         'echo.adapters.in.rest.dtos.DataFormRdto',
                         'echo.adapters.in.rest.dtos.EchoComplexRrequest',
                         'echo.adapters.in.rest.dtos.EchoGetRrequest',
                         'echo.adapters.in.rest.dtos.EchoPostRrequest',
                         'echo.adapters.in.rest.dtos.TreeRootRdto',
                         'echo.adapters.in.rest.mappers.EchoComplexRequestMapper',
                         'echo.adapters.in.rest.mappers.EchoComplexResponseMapper',
                         'echo.adapters.in.rest.mappers.EchoGetRequestMapper',
                         'echo.adapters.in.rest.mappers.EchoGetResponseMapper',
                         'echo.adapters.in.rest.mappers.EchoPostRequestMapper',
                         'echo.adapters.in.rest.mappers.EchoPostResponseMapper',
                         'echo.adapters.in.rest.validators.EchoComplexValidator',
                         'echo.adapters.in.rest.validators.EchoGetValidator',
                         'echo.adapters.in.rest.validators.EchoPostValidator',
                         'echo.ports.in.rest.EchoFacade',
                         'java.util.List',
                         'org.springframework.beans.factory.annotation.Autowired',
                         'org.springframework.http.ResponseEntity',
                         'org.springframework.web.bind.annotation.GetMapping',
                         'org.springframework.web.bind.annotation.PostMapping',
                         'org.springframework.web.bind.annotation.RequestBody',
                         'org.springframework.web.bind.annotation.RequestParam',
                         'org.springframework.web.bind.annotation.RestController',
                 ],
         ),
         new ClassShadow(
                 name: 'echo.ports.in.rest.EchoFacade',
                 classHeader: 'public interface EchoFacade',
                 annotations: CheckUtils.GEN_ANNOTATIONS,
                 methods: [
                         methodShadow('echoPost', 'EchoPostResponse echoPost(@Nonnull EchoPostRequest request)', null),
                         methodShadow('echoGet', 'EchoGetResponse echoGet(@Nonnull EchoGetRequest request)', null)
                 ],
                 imports: [
                         'echo.ports.in.rest.dtos.EchoComplexRequest',
                         'echo.ports.in.rest.dtos.EchoComplexResponse',
                         'echo.ports.in.rest.dtos.EchoGetRequest',
                         'echo.ports.in.rest.dtos.EchoGetResponse',
                         'echo.ports.in.rest.dtos.EchoPostRequest',
                         'echo.ports.in.rest.dtos.EchoPostResponse',
                         'javax.annotation.Nonnull',
                 ],
         ),
         new ClassShadow(
                 name: 'echo.application.EchoFacadeImpl',
                 classHeader: 'public class EchoFacadeImpl implements EchoFacade',
                 annotations: CheckUtils.COMPONENT_ANNOTATIONS_LEGACY,
                 methods: [
                         methodShadow('echoPost', 'public EchoPostResponse echoPost(@Nonnull EchoPostRequest request)', ['return null;'], ['@Override']),
                         methodShadow('echoGet', 'public EchoGetResponse echoGet(@Nonnull EchoGetRequest request)', ['return null;'], ['@Override'])
                 ],
                 imports: [
                         'echo.ports.in.rest.EchoFacade',
                         'echo.ports.in.rest.dtos.EchoComplexRequest',
                         'echo.ports.in.rest.dtos.EchoComplexResponse',
                         'echo.ports.in.rest.dtos.EchoGetRequest',
                         'echo.ports.in.rest.dtos.EchoGetResponse',
                         'echo.ports.in.rest.dtos.EchoPostRequest',
                         'echo.ports.in.rest.dtos.EchoPostResponse',
                         'javax.annotation.Nonnull',
                         'org.springframework.stereotype.Component',
                 ],
         ),

         new ClassShadow(
                 name: 'echo.adapters.in.rest.mappers.TreeRootMapper',
                 classHeader: 'public class TreeRootMapper',
                 annotations: CheckUtils.COMPONENT_ANNOTATIONS_LEGACY,
                 fields: ['@Autowired private TreeLeafMapper treeLeafMapper'],
                 methods: [
                         methodShadow('map2TreeRootDto', 'public TreeRootDto map2TreeRootDto(TreeRootRdto value)',
                                 ['return value == null ? null : map(new TreeRootDto(), value);']),
                         methodShadow('map', 'public TreeRootDto map(TreeRootDto result, TreeRootRdto value)',
                                 ['return result',
                                  '        .setComplexField(treeLeafMapper.map2TreeLeafDto(value.getComplexField()))',
                                  '        ;',
                                 ]),
                         methodShadow('map2TreeRootRdto', 'public TreeRootRdto map2TreeRootRdto(TreeRootDto value)',
                                 ['return value == null ? null : map(new TreeRootRdto(), value);']),
                         methodShadow('map', 'public TreeRootRdto map(TreeRootRdto result, TreeRootDto value)',
                                 ['return result',
                                  '        .setComplexField(treeLeafMapper.map2TreeLeafRdto(value.getComplexField()))',
                                  '        ;',
                                 ]),
                 ],
                 imports: [
                         'echo.adapters.in.rest.dtos.TreeRootRdto',
                         'echo.ports.in.rest.dtos.TreeRootDto',
                         'org.springframework.beans.factory.annotation.Autowired',
                         'org.springframework.stereotype.Component',
                 ],
         ),
        ]
    }

    List<String> expectedClassesForCommons() {
        ['commons.EnumValue',
         'commons.RestResponse',
         'commons.RestResponse200',
         'commons.RestResponse400',
         'commons.RestResponse404',
         'commons.RestResponse405',
         'commons.RestResponseBase',
         'commons.validator.BoolExp',
         'commons.validator.Checker',
         'commons.validator.CommonCheckers',
         'commons.validator.ConditionalChecker',
         'commons.validator.Field',
         'commons.validator.OperationId',
         'commons.validator.ValidationContext',
         'commons.validator.ValidationError',
         'commons.validator.ValidationResult',
         'commons.validator.ValidationResultMapper',
         'commons.validator.Validator',
         'commons.SerializationUtils',
        ]
    }

    List<String> expectedClassesForCommonsApi() {
        ['echo.adapters.in.rest.dtos.BadRequestRdto',
         'echo.adapters.in.rest.dtos.ErrorRdto',
         'echo.adapters.in.rest.dtos.FieldValidationRdto',
         'echo.adapters.in.rest.mappers.BadRequestMapper',
         'echo.adapters.in.rest.mappers.ErrorMapper',
         'echo.adapters.in.rest.mappers.FieldValidationMapper',
         'echo.ports.in.rest.dtos.BadRequestDto',
         'echo.ports.in.rest.dtos.ErrorDto',
         'echo.ports.in.rest.dtos.FieldValidationDto',]
    }

    List<String> expectedClassesForEchoApi() {
        ['echo.adapters.in.rest.EchoController',
         'echo.adapters.in.rest.dtos.DataFormRdto',
         'echo.adapters.in.rest.dtos.EchoComplexRrequest',
         'echo.adapters.in.rest.dtos.EchoGetRrequest',
         'echo.adapters.in.rest.dtos.EchoPostRrequest',
         'echo.adapters.in.rest.dtos.TreeLeafRdto',
         'echo.adapters.in.rest.dtos.TreeRootRdto',
         'echo.adapters.in.rest.mappers.DataFormMapper',
         'echo.adapters.in.rest.mappers.EchoComplexRequestMapper',
         'echo.adapters.in.rest.mappers.EchoComplexResponseMapper',
         'echo.adapters.in.rest.mappers.EchoGetRequestMapper',
         'echo.adapters.in.rest.mappers.EchoGetResponseMapper',
         'echo.adapters.in.rest.mappers.EchoPostRequestMapper',
         'echo.adapters.in.rest.mappers.EchoPostResponseMapper',
         'echo.adapters.in.rest.mappers.TreeLeafMapper',
         'echo.adapters.in.rest.mappers.TreeRootMapper',
         'echo.adapters.in.rest.validators.DataFormValidator',
         'echo.adapters.in.rest.validators.EchoComplexValidator',
         'echo.adapters.in.rest.validators.EchoGetValidator',
         'echo.adapters.in.rest.validators.EchoPostValidator',
         'echo.adapters.in.rest.validators.TreeLeafValidator',
         'echo.adapters.in.rest.validators.TreeRootValidator',
         'echo.application.EchoFacadeImpl',
         'echo.op.EchoComplexOperationAction',
         'echo.op.EchoGetOperationAction',
         'echo.op.EchoPostOperationAction',
         'echo.ports.in.rest.EchoFacade',
         'echo.ports.in.rest.dtos.DataFormDto',
         'echo.ports.in.rest.dtos.EchoComplexRequest',
         'echo.ports.in.rest.dtos.EchoComplexResponse',
         'echo.ports.in.rest.dtos.EchoGetRequest',
         'echo.ports.in.rest.dtos.EchoGetResponse',
         'echo.ports.in.rest.dtos.EchoPostRequest',
         'echo.ports.in.rest.dtos.EchoPostResponse',
         'echo.ports.in.rest.dtos.TreeLeafDto',
         'echo.ports.in.rest.dtos.TreeRootDto',
        ]
    }
}
