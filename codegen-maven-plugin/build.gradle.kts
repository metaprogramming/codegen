import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile

val mavenPluginApiVersion: String by project
val mavenPluginAnnotationsVersion: String by project
val mavenProjectVersion: String by project
val commonsIoVersion: String by project

plugins {
    `codegen-lib`
    kotlin("jvm")
    id("de.benediktritter.maven-plugin-development") version "0.4.3"
}

dependencies {
    implementation(project(":codegen"))
    implementation("org.apache.maven:maven-plugin-api:$mavenPluginApiVersion")
    implementation("org.apache.maven:maven-project:$mavenProjectVersion")
    implementation("org.apache.maven.plugin-tools:maven-plugin-annotations:$mavenPluginAnnotationsVersion")
    implementation(kotlin("reflect"))
    testImplementation(testFixtures(project(":codegen")))
    testImplementation(project(":codegen-spring-rs2t"))
    testImplementation("commons-io:commons-io:$commonsIoVersion")
}

tasks.withType<KotlinJvmCompile>().configureEach {
    compilerOptions {
        jvmTarget.set(JvmTarget.JVM_1_8)
    }
}
