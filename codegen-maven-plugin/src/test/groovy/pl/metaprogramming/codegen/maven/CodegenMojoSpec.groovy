/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.maven

import org.apache.commons.io.IOUtils
import org.codehaus.plexus.configuration.PlexusConfiguration
import org.codehaus.plexus.configuration.xml.XmlPlexusConfiguration
import org.codehaus.plexus.util.xml.Xpp3DomBuilder
import pl.metaprogramming.codegen.java.JavaParams
import pl.metaprogramming.codegen.java.LicenceDecorator
import pl.metaprogramming.codegen.java.spring.*
import pl.metaprogramming.model.data.DataType
import spock.lang.Specification

import java.nio.charset.StandardCharsets
import java.util.function.Consumer

class CodegenMojoSpec extends Specification {

    def "should prepare codegen configuration (all possibilities)"() {
        when:
        def codegen = makeCodegenMojo {
            it.lineSeparator = "\n"
        }.makeCodegen()
        then:
        codegen.cfg.modules.size() == 7

        when:
        def javaParams = codegen.params.get(JavaParams)

        then:
        javaParams.alwaysGenerateEnumFromValueMethod
        javaParams.javaVersion == 11
        javaParams.generatedAnnotationClass == "javax.annotation.Generated"
        javaParams.generatedAnnotationValue == "my_generation_tag"
        javaParams.codeDecorators.find { it instanceof LicenceDecorator }.licenceHeader.contains("My licence")

        when:
        def dataTypeMapper = codegen.dataTypeMapper
        then:
        dataTypeMapper.get(DataType.TEXT).canonicalName == "java.lang.String"
        dataTypeMapper.get(DataType.BASE64).canonicalName == "fake.Base64"
        dataTypeMapper.get(DataType.DATE_TIME).canonicalName == "fake.Date"

        when:
        def commonsGenerator = codegen.cfg.modules[0] as SpringCommonsGenerator.Generator
        def tocValidationException = commonsGenerator.codeBuilders.get(SpringCommonsGenerator.TOC.VALIDATION_EXCEPTION)
        then:
        tocValidationException.className.fixed == "InvalidInputException"
        tocValidationException.packageName.root == "new.root.pkg"
        tocValidationException.packageName.tail == "other"
        commonsGenerator.dataTypeMapper.get(DataType.DATE_TIME).canonicalName == "fake.Date"

        when:
        def soapClientGenerator = codegen.cfg.modules[1] as SpringSoapClientGenerator.Generator
        def namespace2Package = soapClientGenerator.params.get(SpringSoapParams).namespace2Package
        then:
        soapClientGenerator.codeBuilders.get(SpringSoapClientGenerator.TOC.CLIENT_IMPL).packageName.root == 'example.ws'
        soapClientGenerator.codeBuilders.get(SpringSoapClientGenerator.TOC.DTO).className.suffix == 'SoapDto'
        namespace2Package['http://spring.io/guides/gs-producing-web-service'] == 'example.ws.schema'
        namespace2Package['http://spring.io/guides/gs-producing-web-model'] == 'example.ws.schema.model'

        when:
        def rsGenerator = codegen.cfg.modules[2] as SpringRestServiceGenerator.Generator
        then:
        rsGenerator.codeBuilders.get(SpringRestServiceGenerator.TOC.DTO).className.suffix == 'RsDto'
        rsGenerator.codeBuilders.get(SpringRestServiceGenerator.TOC.DTO).packageName.tail == 'rsdto'

        when:
        def rsClientGenerator = codegen.cfg.modules[3] as SpringRestClientGenerator.Generator
        then:
        rsClientGenerator.codeBuilders.get(SpringRestClientGenerator.TOC.DTO).className.suffix == 'RsClientDto'
        !rsClientGenerator.params.get(JavaParams).alwaysGenerateEnumFromValueMethod
        rsClientGenerator.params.get(JavaParams).generatedAnnotationValue == "my_client_generation_tag"
        // TODO value should be propagated from codegen params
        //rsClientGenerator.params.get(JavaParams).javaVersion == 11
        rsClientGenerator.dataTypeMapper.get(DataType.DATE_TIME).canonicalName == "java.util.Date"

        when:
        def rs2tGenerator = codegen.cfg.modules[4] as SpringRestService2tGenerator.Generator
        then:
        rs2tGenerator.codeBuilders.get(SpringRestService2tGenerator.TOC.DTO).className.suffix == 'Rs2tDto'

        when:
        def rs2tClientGenerator = codegen.cfg.modules[5] as SpringRestClient2tGenerator.Generator
        then:
        rs2tClientGenerator.codeBuilders.get(SpringRestClient2tGenerator.TOC.DTO).className.suffix == 'RsClient2tDto'

        when:
        def swaggerUiGenerator = codegen.cfg.modules[6] as SpringSwaggerUiGenerator.Generator
        def apis = swaggerUiGenerator.model.apis.entrySet().first()
        then:
        apis.key.name == "Swagger Petstore"
        apis.value == "petstore.yaml"
    }

    private static makeCodegenMojo(Consumer<CodegenMojo> setter) {
        def cfg = toPlexusConfiguration(CFG)
        def mojo = new CodegenMojo()
        mojo.dataTypeMapper = cfg.getChild("dataTypeMapper")
        mojo.params = cfg.getChild("params")
        mojo.generators = cfg.getChild("generators")
        setter.accept(mojo)
        mojo
    }

    private static PlexusConfiguration toPlexusConfiguration(String xmlText) {
        new XmlPlexusConfiguration(Xpp3DomBuilder.build(IOUtils.toInputStream(xmlText, StandardCharsets.UTF_8), "UTF-8"))
    }

    static def CFG = """
<configuration>
  <dataTypeMapper>
    <set type="BASE64">fake.Base64</set>
    <set type="DATE_TIME">fake.Date</set>
  </dataTypeMapper>
  <params>
    <params class="pl.metaprogramming.codegen.java.JavaParams">
      <isAlwaysGenerateEnumFromValueMethod>true</isAlwaysGenerateEnumFromValueMethod>
      <javaVersion>11</javaVersion>
      <generatedAnnotationClass>javax.annotation.Generated</generatedAnnotationClass>
      <generatedAnnotationValue>my_generation_tag</generatedAnnotationValue>
      <licenceHeader>My licence</licenceHeader>
    </params>
  </params>
  <generators>
    <generator class="pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator">
      <rootPackage>example.commons</rootPackage>
      <typeOfCode of="VALIDATION_EXCEPTION">
        <className.fixed>InvalidInputException</className.fixed>
        <packageName>
            <root>new.root.pkg</root>
            <tail>other</tail>
        </packageName>
      </typeOfCode>
    </generator>
    <generator class="pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator">
      <model>../codegen/src/test/resources/wsdl/countries.wsdl</model>
      <rootPackage>example.ws</rootPackage>
      <typeOfCode of="DTO">
        <className.suffix>SoapDto</className.suffix>
      </typeOfCode>
      <namespacePackage ns="http://spring.io/guides/gs-producing-web-service">example.ws.schema</namespacePackage>
      <setNamespacePackage ns="http://spring.io/guides/gs-producing-web-model">example.ws.schema.model</setNamespacePackage>
    </generator>
    <generator class="pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator">
      <model>../codegen/src/test/resources/swagger/petstore.yaml</model>
      <interceptor class="pl.metaprogramming.codegen.maven.RsDtoInterceptor" />
      <rootPackage>example.rs</rootPackage>
      <interceptor class="pl.metaprogramming.codegen.maven.RsDtoPkgInterceptor" />
    </generator>
    <generator class="pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator">
      <model>../codegen/src/test/resources/swagger/petstore.yaml</model>
      <rootPackage>example.rs</rootPackage>
      <params>
          <set class="pl.metaprogramming.codegen.java.JavaParams">
            <isAlwaysGenerateEnumFromValueMethod>false</isAlwaysGenerateEnumFromValueMethod>
            <generatedAnnotationValue>my_client_generation_tag</generatedAnnotationValue>
          </set>
      </params>
      <dataTypeMapper>
         <set type="DATE_TIME">java.util.Date</set>
      </dataTypeMapper>
      <typeOfCode of="DTO">
        <className.suffix>RsClientDto</className.suffix>
      </typeOfCode>
    </generator>
    <generator class="pl.metaprogramming.codegen.java.spring.SpringRestService2tGenerator">
      <model>../codegen/src/test/resources/swagger/petstore.yaml</model>
      <rootPackage>example.rs</rootPackage>
      <typeOfCode of="DTO">
        <className.suffix>Rs2tDto</className.suffix>
      </typeOfCode>
    </generator>
    <generator class="pl.metaprogramming.codegen.java.spring.SpringRestClient2tGenerator">
      <model>../codegen/src/test/resources/swagger/petstore.yaml</model>
      <rootPackage>example.rs</rootPackage>
      <typeOfCode of="DTO">
        <className.suffix>RsClient2tDto</className.suffix>
      </typeOfCode>
    </generator>
    <generator class="pl.metaprogramming.codegen.java.spring.SpringSwaggerUiGenerator">
      <api of="../codegen/src/test/resources/swagger/petstore.yaml">petstore.yaml</api>
    </generator>
  </generators>
</configuration>"""

}
