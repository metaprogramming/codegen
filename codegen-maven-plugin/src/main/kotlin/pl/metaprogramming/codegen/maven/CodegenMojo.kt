/*
 * Copyright (c) 2024 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.metaprogramming.codegen.maven

import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import org.codehaus.plexus.configuration.PlexusConfiguration
import pl.metaprogramming.codegen.Codegen
import pl.metaprogramming.codegen.GeneratorBuilder
import pl.metaprogramming.codegen.TypeOfCode
import pl.metaprogramming.model.oas.RestApi
import pl.metaprogramming.model.wsdl.WsdlApi
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.function.Consumer
import kotlin.reflect.*
import kotlin.reflect.full.memberFunctions
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
class CodegenMojo : AbstractMojo() {

    @Parameter(defaultValue = ".", required = true)
    var baseDir: File? = null

    @Parameter(defaultValue = "generatedFiles.yaml")
    var indexFile: File? = null

    @Parameter(defaultValue = "true", required = true)
    var forceMode: Boolean = true

    @Parameter(defaultValue = "false", required = true)
    var addLastGenerationTag: Boolean = false

    @Parameter(defaultValue = "true", required = true)
    var addLastUpdateTag: Boolean = true

    @Parameter(defaultValue = "UTF-8", required = true)
    var charset: String = "UTF-8"

    @Parameter(defaultValue = "false", required = true)
    var overrideWhenDiffsInWhitespacesOnly: Boolean = false

    @Parameter
    var lineSeparator: String = System.lineSeparator()

    @Parameter(name = "dataTypeMapper")
    var dataTypeMapper: PlexusConfiguration? = null

    @Parameter(name = "params")
    var params: PlexusConfiguration? = null

    @Parameter(name = "generators")
    var generators: PlexusConfiguration? = null


    private val codegen = Codegen()
    private lateinit var currentGenerator: Any

    override fun execute() {
        log.info("[$currentTime] Going to run code generation")
        makeCodegen().run()
        log.info("[$currentTime] Code generation SUCCESS")
    }

    private val currentTime: String get() = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)

    fun makeCodegen(): Codegen {
        codegen
            .baseDir(baseDir ?: File("."))
            .indexFile(indexFile)
            .forceMode(forceMode)
            .addLastGenerationTag(addLastGenerationTag)
            .addLastUpdateTag(addLastUpdateTag)
            .charset(charset)
            .overrideWhenDiffsInWhitespacesOnly(overrideWhenDiffsInWhitespacesOnly)
            .lineSeparator(lineSeparator)
        dataTypeMapper?.let {
            codegen.dataTypeMapper.setProperties(it)
        }
        params?.toObjectList("params")?.forEach {
            codegen.params.set(it)
        }
        generators?.toObjectList("generator")?.forEach {
            if (it is GeneratorBuilder<*, *, *>) {
                it.codeIndex = codegen.codeIndex
                codegen.generate(it.make())
                log.info(it.headline())
            }
        }
        return codegen
    }

    private fun GeneratorBuilder<*, *, *>.headline(): String {
        val modelName = try {
            model.name
        } catch (e: Exception) {
            null
        }
        return javaClass.simpleName + (modelName?.let { ": $it" } ?: "")
    }

    private fun PlexusConfiguration.toObjectList(elementName: String) =
        children.mapNotNull {
            val clazz = it.getAttribute("class")?.let { className -> Class.forName(className) }
            if (it.name == elementName && clazz != null) {
                it.toObject(clazz)
            } else {
                null
            }
        }

    private fun PlexusConfiguration.toObject(clazz: Class<*>): Any {
        val result = clazz.getDeclaredConstructor().newInstance()
        if (result is GeneratorBuilder<*, *, *>) {
            currentGenerator = result
            result.dataTypeMapper = codegen.dataTypeMapper.copy()
            result.params = codegen.params.copy()
            result.codeIndex = codegen.codeIndex
            result.init()
        }
        return result.setProperties(this)
    }

    private fun Any.setProperties(cfg: PlexusConfiguration) = apply {
        cfg.children.forEach { setProperty(it) }
    }

    private fun Any.setProperty(cfg: PlexusConfiguration, name: String = cfg.name) {
        check(setWithClassType(cfg, name) || setByProperty(cfg, name) || setByFunction(cfg, name)) {
            "Can't handle $cfg"
        }
    }

    private fun Any.setByProperty(cfg: PlexusConfiguration, name: String): Boolean {
        if (cfg.attributeNames.isNotEmpty()) {
            return false
        }
        if (name.contains(".")) {
            val path = name.split(".")
            getNestedProperty(path.dropLast(1)).setProperty(cfg, path.last())
            return true
        }

        val prop = getProperty(name)
        val oldVal = try {
            prop?.getter?.call(this)
        } catch (e: Exception) {
            null
        }
        if (prop is KMutableProperty<*>) {
            val value = cfg.value?.to(prop.returnType) ?: if (oldVal == null) cfg.toObject(prop.javaClass) else null
            if (value != null) {
                prop.setter.call(this, value)
                return true
            }
        }
        if (oldVal != null) {
            oldVal.setProperties(cfg)
            return true
        }
        return false
    }

    private fun Any.getProperty(name: String): KProperty1<*, *>? {
        val prop = this::class.memberProperties.find { it.name == name }
        prop?.isAccessible = true
        return prop
    }

    private fun Any.getNestedProperty(path: List<String>): Any {
        var o = this
        for (p in path) {
            val prop = requireNotNull(o.getProperty(p))
            o = prop.getter.call(o) ?: {
                val newVal = prop.javaClass.getDeclaredConstructor().newInstance()
                (prop as KMutableProperty<*>).setter.call(o, newVal)
                newVal
            }
        }
        return o
    }

    private fun Any.setWithClassType(cfg: PlexusConfiguration, name: String) =
        if (cfg.attributeNames.size == 1 && cfg.attributeNames[0] == "class") {
            val funNames = listOf(name, "set${name.upperFirstChar()}")
            val valObj = cfg.toObject(Class.forName(cfg.getAttribute("class")))
            val success = this::class.memberFunctions.any { f ->
                if (funNames.contains(f.name) && f.parameters.size == 2) {
                    f.call(this, valObj)
                    true
                } else {
                    false
                }
            }
            if (!success && name == "interceptor" && valObj is Consumer<*>) {
                @Suppress("UNCHECKED_CAST")
                (valObj as Consumer<Any>).accept(this)
                true
            } else success
        } else {
            false
        }

    private fun Any.setByFunction(cfg: PlexusConfiguration, name: String): Boolean {
        val funNames = listOf(name, "set${name.upperFirstChar()}")
        val params = (cfg.attributeNames.map(cfg::getAttribute) + cfg.value).filterNotNull()
        val clazz = if (this is Lazy<*>) this.value!!::class else this::class
        return clazz.memberFunctions.any { f ->
            if (funNames.contains(f.name) && f.parameters.size - 1 == params.size) {
                try {
                    val fixedParams = params.mapIndexed { idx, v -> v.to(f.parameters[idx + 1].type) }.toMutableList()
                    fixedParams.add(0, this)
                    val callResult = f.call(*fixedParams.toTypedArray())
                    if (callResult != null) {
                        cfg.children.forEach { callResult.setProperty(it) }
                    }
                    true
                } catch (e: Exception) {
                    false
                }
            } else {
                false
            }
        }
    }

    private fun String.to(type: KType): Any? {
        return when (type.classifier) {
            String::class -> this
            Boolean::class -> toBoolean()
            Int::class -> toInt()
            WsdlApi::class -> WsdlApi.of(this)
            RestApi::class -> RestApi.of(File(this).readText())
            TypeOfCode::class -> toTypeOfCode()
            else -> toConstVal(type.classifier)
        }
    }

    private fun String.toTypeOfCode(): Any {
        val tocContainer = requireNotNull(currentGenerator.getProperty("toc")).getter.call(currentGenerator)
        return tocContainer!!::class.staticField(this)!!
    }

    private fun String.toConstVal(classifier: KClassifier?): Any? {
        return if (classifier is KClass<*>) classifier.staticField(this) else null
    }

    private fun KClass<*>.staticField(fieldName: String): Any? {
        try {
            val field = java.getDeclaredField(fieldName)
            field.isAccessible = true
            return field[null]
        } catch (e: Exception) {
            // nothing to do
            return null
        }
    }

    private val KMutableProperty<*>.javaClass get() = (returnType.classifier as KClass<*>).java

    private fun String.upperFirstChar() =
        replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
}