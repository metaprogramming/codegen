module.exports = {
  title: 'Codegen',
  tagline: 'For those who don\'t like to write boilerplate code ',
  url: 'https://metaprogramming.gitlab.io/',
  baseUrl: '/codegen/',
  favicon: 'img/favicon.ico',
  organizationName: 'metaprogramming.pl', // Usually your GitHub org/user name.
  projectName: 'codegen', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: '',
      logo: {
        alt: 'codegen Logo',
        src: 'img/codegen.png',
        srcDark: 'img/codegen_dark.png'
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {
          href: 'https://gitlab.com/metaprogramming/codegen',
          label: 'GitLab',
          position: 'right',
        },
        {
          href: 'https://mvnrepository.com/artifact/pl.metaprogramming/codegen',
          label: 'MavenCentral',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Introduction',
              to: 'docs/',
            },
            {
              label: 'Getting Started',
              to: 'docs/getting-started/how-to-use',
            },
            {
              label: 'Core design',
              to: 'docs/guides/core-design',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/metaprogramming/codegen',
            },
            {
              label: 'Maven Central',
              href: 'https://mvnrepository.com/artifact/pl.metaprogramming/codegen',
            },
            {
              label: 'Code coverage',
              to: 'code-coverage/',
              target: '_blank',
            },
          ],
        },
        {
          title: 'API',
          items: [
            {
              label: 'for kotlin',
              to: 'kotlin-api/',
              target: '_blank',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Codegen, Inc. Built with Docusaurus.`,
    },
    prism: {
        additionalLanguages: ['java', 'properties'],
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://gitlab.com/metaprogramming/codegen/-/edit/develop/maintenance/website/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  markdown: {
    mermaid: true,
  },
  themes: ['@docusaurus/theme-mermaid'],
};
