---
id: quick-start-spring-maven-complex
title: Use Codegen in a maven project - complex
sidebar_label: Maven plugin - complex
---

This guide is an extension of the guide [Use Codegen in a maven project](quick-start-spring-maven.md).
Its purpose is to demonstrate how to perform advanced code generation configuration using java code in a maven project.

Specifically, the code generation of the `RestExceptionHandler` class will be configured as it was done in the guide [Use Codegen in a gradle project](quick-start-spring-gradle.md).

To configure code generation using Java code in a Maven project, this configuration code must be separated into a library,
which must then be used in the `codegen-maven-plugin` plugin configuration.

In this case the configuration code was separated into the `generator` module.
However, it could just as well be in a standalone maven project
(the only important thing is that it should be available in the maven repository).

## Project structure

Our project files will look like this (only selected files and directories are presented):

```sh
parent-project
├── app <almost like the project in the guide "Use Codegen in a maven project">
│   ├── ...
│   └── pom.xml
├── generator 
│   ├── src
│   │   └── main
│   │       └── java
│   │           └── example
│   │               └── RestExceptionGeneratorInterceptor.java
│   └── pom.xml
└── pom.xml
.
```

## Parent project
In the root directory, place the `pom.xml` file:

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>pl.metaprogramming.codegen.examples</groupId>
    <artifactId>spring-maven-complex</artifactId>
    <packaging>pom</packaging>
    <version>0.0.1-SNAPSHOT</version>

    <properties>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <modules>
        <module>app</module>
        <module>generator</module>
    </modules>
</project>
```


## Generator module
In directory `generator` place the `pom.xml` file:

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>pl.metaprogramming.codegen.examples</groupId>
        <artifactId>spring-maven-complex</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>
    <artifactId>generator</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>pl.metaprogramming</groupId>
            <artifactId>codegen</artifactId>
            <version>2.0.1-SNAPSHOT</version>
        </dependency>
    </dependencies>
</project>
```


In directory `generator/src/main/java/example` place the `RestExceptionGeneratorInterceptor.java` file:

```java title="RestExceptionGeneratorInterceptor.java"
package example;

import pl.metaprogramming.codegen.java.MethodCm;
import pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator;

import java.util.function.Consumer;

public class RestExceptionGeneratorInterceptor implements Consumer<SpringCommonsGenerator> {
    @Override
    public void accept(SpringCommonsGenerator generator) {
        generator.typeOfCode(SpringCommonsGenerator.TOC.REST_EXCEPTION_HANDLER, it -> {
            it.onDecoration(cls -> {
                        MethodCm methodCm = cls.getMethods().get("toResponseEntity(result, request)");
                        methodCm.getImplDependencies().add("example.ports.in.rest.dtos.ErrorDescriptionDto");
                        methodCm.setImplBody(cls.getCodeBuf()
                                .addLines("ValidationError firstError = result.getErrors().get(0);",
                                        "return ResponseEntity",
                                        "        .status(result.getStatus())",
                                        "        .header(\"x-correlation-id\", request.getHeader(\"x-correlation-id\"))",
                                        "        .body(new ErrorDescriptionDto()",
                                        "                .setCode(firstError.getCode())",
                                        "                .setField(firstError.getField())",
                                        "                .setMessage(String.format(\"%s: %s\", firstError.getField(), firstError.getCode())));"
                                )
                                .take()
                        );
                    }
            );
        });
    }
}
```


## App module
In the "app" directory, prepare the files as done in the [Use Codegen in a maven project](quick-start-spring-maven.md) guide.

Do not modify the generated class `RestExceptionHandler`.
Modify the file `pom.xml` instead.
There are basically two changes in the code generation configuration - `codegen-maven-plugin` plugin:
- a dependency to the `generator` module has been added
  ```xml
  <dependency>
    <groupId>pl.metaprogramming.codegen.examples</groupId>
    <artifactId>generator</artifactId>
    <version>${project.version}</version>
  </dependency>
  ```
- a code
  ```xml
  <interceptor class="example.RestExceptionGeneratorInterceptor" />
  ```
  was added into the element
  ```xml
  <generator class="pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator">
  ```

Finally `pom.xml` looks like:

```xml title="pom.xml"
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>pl.metaprogramming.codegen.examples</groupId>
        <artifactId>spring-maven-complex</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>
    <artifactId>app</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web-services</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.ws</groupId>
            <artifactId>spring-ws-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>jsr305</artifactId>
            <version>3.0.2</version>
        </dependency>

        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>2.3.1</version>
        </dependency>
        <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-impl</artifactId>
            <version>2.3.9</version>
        </dependency>
        <dependency>
            <groupId>com.sun.xml.messaging.saaj</groupId>
            <artifactId>saaj-impl</artifactId>
            <version>1.5.3</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.zalando</groupId>
            <artifactId>logbook-spring-boot-starter</artifactId>
            <version>2.14.0</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.7.2</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.squareup.okhttp3</groupId>
            <artifactId>mockwebserver</artifactId>
            <version>4.12.0</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>2.7.18</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.7.18</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.5.1</version>
            </plugin>
            <plugin>
                <groupId>pl.metaprogramming</groupId>
                <artifactId>codegen-maven-plugin</artifactId>
                <version>2.0.1-SNAPSHOT</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>pl.metaprogramming.codegen.examples</groupId>
                        <artifactId>generator</artifactId>
                        <version>${project.version}</version>
                    </dependency>
                </dependencies>
                <configuration>
                    <dataTypeMapper>
                        <set type="FLOAT">java.math.BigDecimal</set>
                    </dataTypeMapper>
                    <params>
                        <params class="pl.metaprogramming.codegen.java.JavaParams">
                            <generatedAnnotationValue>my_generation_tag</generatedAnnotationValue>
                        </params>
                    </params>
                    <generators>
                        <generator class="pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator">
                            <rootPackage>example.commons</rootPackage>
                            <interceptor class="example.RestExceptionGeneratorInterceptor" />
                            <typeOfCode of="REST_EXCEPTION_HANDLER">
                                <packageName>
                                    <root>example</root>
                                </packageName>
                            </typeOfCode>
                        </generator>
                        <generator class="pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator">
                            <model>${project.basedir}/example-api.yaml</model>
                            <rootPackage>example</rootPackage>
                        </generator>
                        <generator class="pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator">
                            <model>${project.basedir}/example-api.yaml</model>
                            <rootPackage>example</rootPackage>
                            <dataTypeMapper>
                                <set type="FLOAT">java.lang.Double</set>
                            </dataTypeMapper>
                            <params>
                                <set class="pl.metaprogramming.codegen.java.JavaParams">
                                    <generatedAnnotationValue>my_client_generation_tag</generatedAnnotationValue>
                                </set>
                                <set class="pl.metaprogramming.codegen.java.test.TestDataParams">
                                    <isEnabled>false</isEnabled>
                                </set>
                            </params>
                        </generator>
                        <generator class="pl.metaprogramming.codegen.java.spring.SpringSwaggerUiGenerator">
                            <api of="${project.basedir}/example-api.yaml">example-api.yaml</api>
                        </generator>
                        <generator class="pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator">
                            <model>${project.basedir}/countries.wsdl</model>
                            <rootPackage>example</rootPackage>
                            <namespacePackage ns="http://spring.io/guides/gs-producing-web-service">example.ports.out.soap.dtos</namespacePackage>
                            <urlProperty>COUNTRIES_SERVICE_URL</urlProperty>
                            <typeOfCode of="DTO">
                                <className.suffix>SoapDto</className.suffix>
                            </typeOfCode>
                        </generator>
                    </generators>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```


## Project build and test

To generate codes, build the project and run tests, execute the command:
```shell
mvn install
```

Alternatively, you can execute a series of commands:
- build and install the 'generator' module to the local maven repository
  ```shell
  mvn -f generator install
  ```
- generate codes
  ```shell
  mvn -f app codegen:generate
  ```
- build and run tests of the 'app' module
  ```shell
  mvn -f app package
  ```

## More about "interceptor" element
The `interceptor` element can be placed in `generator` elements.

The `interceptor` element should have the `class` attribute set, the value of which must indicate a class implementing
the `java.function.Consumer<T>` interface, where `T` should correspond to the generator class
(a subclass of `GeneratorBuilder`, in this case `SpringCommonsGenerator`).

The `interceptor` element can be placed multiple times in a single `generator` element.
The order of execution of interceptors will be the order in which they are placed in the xml.
