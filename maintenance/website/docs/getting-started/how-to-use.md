---
id: how-to-use
title: How to use Codegen
sidebar_label: How to use it
---

Codegen is available from the [Central Maven Repository](https://mvnrepository.com/artifact/pl.metaprogramming/codegen).

## Quick Start Projects

To get a better understanding of how to use Codegen, you can go through the following quickstart project guides:

- [Use Codegen in a gradle project](quick-start-spring-gradle.md)
- [Use Codegen in a maven project](quick-start-spring-maven.md)
- [Use Codegen in a maven project - complex](quick-start-spring-maven-complex.md)

See also the [core design](guides/core-design.md) article that provides an explanation of the basic elements of the Codegen project.

## Using a development version

To use a current development version, follow these steps:

1. Checkout git repository:
```
git clone https://gitlab.com/metaprogramming/codegen.git
```

2. Build and publish to maven local repository
```
cd codegen
gradlew publishToMavenLocal
```

To check the version number you just added to your local maven repository, take a look at the gradle.properties file (codegenVersion property).

3. Now in your project you can use unreleased version.

If you use gradle, you should also setup maven local repository to use it:
```
repositories {
    mavenLocal()
    mavenCentral()
}
```
