---
id: quick-start-spring-gradle
title: Use Codegen in a gradle project
sidebar_label: Gradle plugin
---

This guide will walk you through the process of generating codes using the Codegen Gradle plugin (`pl.metaprogramming.codegen`)
and give you a general overview of how to use Codegen.

:::info
The plugin requires at least **Gradle** version **8.0**.
:::

This example will generate codes for
a [REST server](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-rest-service-generator/index.html),
a [REST client](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-rest-client-generator/index.html),
and a [SOAP client](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-soap-client-generator/index.html).

The code generation setup in this example is not as simple as possible.
They are intentionally complicated to show how code generation can be configured.

This example uses the API described in the OpenAPI/swagger and WSDL standards.

Both REST client and REST server codes are generated for the same API described in OpenAPI/swagger.

API documentation using swagger-ui was also generated using the [SpringSwaggerUiGenerator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-swagger-ui-generator/index.html) generator.

## Project structure

Our project files will look like this (only selected files and directories are presented):

```sh
project-root
├── src
│   └── main
│       ├── java
│       │   └── example
│       │       ├── adapters <GENERATED>
│       │       │   ├── in
│       │       │   └── out
│       │       │       ├── rest
│       │       │       └── soap
│       │       ├── commons <GENERATED>
│       │       ├── ports <GENERATED>
│       │       │   ├── in
│       │       │   └── out
│       │       │       ├── rest
│       │       │       └── soap
│       │       ├── process <GENERATED classes stubs to implementation>
│       │       ├── Application.java
│       │       ├── RestExceptionHandler.java <GENERATED>
│       │       └── RestTemplateConfigurator.java
│       ├── resources
│       │    └── application.properties
│       └── static <GENERATED swagger-ui page>
│            ├── example-api.yaml
│            └── index.html
├── build.gradle.kts <build project script>
├── countries.wsdl <api description for SOAP client>
├── example-api.yaml <api description for REST client/server>
├── generatedFiles.yaml <generated files index>
└── settings.gradle.kts
.
```

## Project build script and generator configuration


```kts title="build.gradle.kts"
plugins {
    java
    id("pl.metaprogramming.codegen") version "2.0.1-SNAPSHOT"
    id("org.springframework.boot") version "2.7.18"
    id("io.spring.dependency-management") version "1.1.6"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.ws:spring-ws-core")
    implementation("com.google.code.findbugs:jsr305:3.0.2")

    implementation("javax.xml.bind:jaxb-api:2.3.1")
    implementation("com.sun.xml.bind:jaxb-impl:2.3.9")
    implementation("com.sun.xml.messaging.saaj:saaj-impl:1.5.3")

    compileOnly("org.projectlombok:lombok:1.18.34")
    annotationProcessor("org.projectlombok:lombok:1.18.34")

    implementation("org.zalando:logbook-spring-boot-starter:2.14.0")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.2")
    testImplementation("com.squareup.okhttp3:mockwebserver:4.12.0")
}

tasks.test {
    useJUnitPlatform()
}

codegen {
    dataTypeMapper[pl.metaprogramming.model.data.DataType.FLOAT] = "java.lang.Double"
    params.set(pl.metaprogramming.codegen.java.JavaParams::class) {
        generatedAnnotationValue = "my_generation_tag"
    }
    val api = restApi(file("example-api.yaml").readText(Charsets.UTF_8))
    generate(pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator()) {
        rootPackage = "example.commons"
        typeOfCode(toc.REST_EXCEPTION_HANDLER) {
            packageName.root = "example"
            onDecoration {
                methods["toResponseEntity(result, request)"].apply {
                    implDependencies.add(getClass(pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator.TOC.DTO, api.schemas.first { it.code == "ErrorDescription" }.dataType))
                    // or simpler
                    implDependencies.add("example.ports.in.rest.dtos.ErrorDescriptionDto")
                    implBody = """
                        |ValidationError firstError = result.getErrors().get(0);
                        |return ResponseEntity
                        |        .status(result.getStatus())
                        |        .header("x-correlation-id", request.getHeader("x-correlation-id"))
                        |        .body(new ErrorDescriptionDto()
                        |                .setCode(firstError.getCode())
                        |                .setField(firstError.getField())
                        |                .setMessage(String.format("%s: %s", firstError.getField(), firstError.getCode())));
                    """.trimMargin()
                }
            }
        }
    }
    generate(pl.metaprogramming.codegen.java.spring.SpringRestServiceGenerator()) {
        model = api
        rootPackage = "example"
        dataTypeMapper[pl.metaprogramming.model.data.DataType.FLOAT] = "java.math.BigDecimal"
    }
    generate(pl.metaprogramming.codegen.java.spring.SpringRestClientGenerator()) {
        model = api
        rootPackage = "example"
        params.allowDuplicateClasses = true
        params.set(pl.metaprogramming.codegen.java.JavaParams::class) {
            generatedAnnotationValue = "my_client_generation_tag"
        }
        params[pl.metaprogramming.codegen.java.test.TestDataParams::class].isEnabled = false
    }
    generate(pl.metaprogramming.codegen.java.spring.SpringSwaggerUiGenerator()) {
        api(api, "example-api.yaml")
    }
    generate(pl.metaprogramming.codegen.java.spring.SpringSoapClientGenerator()) {
        model = wsdlApi("countries.wsdl")
        rootPackage = "example"
        setNamespacePackage("http://spring.io/guides/gs-producing-web-service", "example.ports.out.soap.dtos")
        setUrlProperty("COUNTRIES_SERVICE_URL")
        typeOfCode(toc.DTO).className.suffix = "SoapDto"
    }
}
```


Code generation configuration should be done in the `codegen` section.
In this section you are in the context of an object of type [Codegen](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen/index.html). So you should familiarize yourself with the API of this class.

The generation of specific codes is indicated in the `generate` section.
In this section, you must provide a specific code generator and the model/API for which the codes will be generated.
In this section you are in the context of an object of subtype [GeneratorBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-generator-builder/index.html) (for java code generator it will be subtype of the [JavaGeneratorBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-java-generator-builder/index.html)).
So you should familiarize yourself with the API of these classes, especially the [JavaGeneratorBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-java-generator-builder/index.html),
which provides a lot of generation configuration mechanisms.

The next level of code generation configuration is the type of code (e.g. DTO, controller).
Types of code are specific to specific generators. The list of types of code specific to a given generator is accessed by the `toc` property (is of generic type and is inherited from [GeneratorBuilder.toc](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-generator-builder/toc.html)) or, by a static internal object called `TOC` (if the generator follows the convention).
To configure the type of code you should use `typeOfCode` section (calls the [JavaGeneratorBuilder.typeOfCode](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-java-generator-builder/type-of-code.html) method).
In this section you are in the context of the [ClassBuilderConfigurator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/index.html) object.

A more detailed description of the code generation configuration options can be found in the [Core design guide](guides/core-design.md).


### OpenAPI - REST

Place the following API definition in the file:

```yaml title="example-api.yaml"
openapi: 3.0.1
info:
  title: Example API
  version: "1.0"
servers:
  - url: http://example.io/api/v1
paths:
  /echo:
    post:
      tags:
        - echo
      operationId: echo
      parameters:
        - $ref: '#/components/parameters/correlationIdParam'
      requestBody:
        description: body param
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/EchoBody'
        required: true
      responses:
        200:
          description: request body
          headers:
            X-Correlation-ID:
              schema:
                type: string
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EchoBody'
        default:
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDescription'
components:
  schemas:
    EchoBody:
      title: Echo object
      type: object
      properties:
        prop_int:
          type: integer
          format: int32
        prop_float:
          maximum: 101
          type: number
          format: float
        prop_double:
          maximum: 9999.9999
          minimum: 0.01
          type: number
          format: double
        prop_amount:
          pattern: ^(0|([1-9][0-9]{0,}))\.\d{2}$
          type: string
        prop_date_time:
          type: string
          format: date-time
      description: Test object
    ErrorDescription:
      required:
        - code
        - message
      type: object
      properties:
        field:
          type: string
        code:
          type: string
        message:
          type: string
  parameters:
    correlationIdParam:
      name: X-Correlation-ID
      in: header
      description: 'Correlates HTTP requests between a client and server. '
      required: true
      schema:
        type: string
```


At this point, it is worth mentioning that codegen also supports OpenAPI extensions for specifying more complex constraints on data (see [Validation support guide](guides/validations.md) to get more info).

### WSDL - SOAP

Place the following API definition in the file:

```xml title="countries.wsdl"
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<wsdl:definitions xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                  xmlns:sch="http://spring.io/guides/gs-producing-web-service"
                  xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
                  xmlns:tns="http://spring.io/guides/gs-producing-web-service"
                  targetNamespace="http://spring.io/guides/gs-producing-web-service">
    <wsdl:types>
        <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified"
                   targetNamespace="http://spring.io/guides/gs-producing-web-service">

            <xs:element name="getCountryRequest">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="name" type="xs:string"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>

            <xs:element name="getCountryResponse">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="country" type="tns:country"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>

            <xs:complexType name="country">
                <xs:sequence>
                    <xs:element name="name" type="xs:string"/>
                    <xs:element name="population" type="xs:int"/>
                    <xs:element name="capital" type="xs:string"/>
                    <xs:element name="currency" type="tns:currency"/>
                </xs:sequence>
            </xs:complexType>

            <xs:simpleType name="currency">
                <xs:restriction base="xs:string">
                    <xs:enumeration value="GBP"/>
                    <xs:enumeration value="EUR"/>
                    <xs:enumeration value="PLN"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:schema>
    </wsdl:types>
    <wsdl:message name="getCountryResponse">
        <wsdl:part element="tns:getCountryResponse" name="getCountryResponse">
        </wsdl:part>
    </wsdl:message>
    <wsdl:message name="getCountryRequest">
        <wsdl:part element="tns:getCountryRequest" name="getCountryRequest">
        </wsdl:part>
    </wsdl:message>
    <wsdl:portType name="CountriesPort">
        <wsdl:operation name="getCountry">
            <wsdl:input message="tns:getCountryRequest" name="getCountryRequest">
            </wsdl:input>
            <wsdl:output message="tns:getCountryResponse" name="getCountryResponse">
            </wsdl:output>
        </wsdl:operation>
    </wsdl:portType>
    <wsdl:binding name="CountriesPortSoap11" type="tns:CountriesPort">
        <soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
        <wsdl:operation name="getCountry">
            <soap:operation soapAction=""/>
            <wsdl:input name="getCountryRequest">
                <soap:body use="literal"/>
            </wsdl:input>
            <wsdl:output name="getCountryResponse">
                <soap:body use="literal"/>
            </wsdl:output>
        </wsdl:operation>
    </wsdl:binding>
    <wsdl:service name="CountriesPortService">
        <wsdl:port binding="tns:CountriesPortSoap11" name="CountriesPortSoap11">
            <soap:address location="http://localhost:8080/ws"/>
        </wsdl:port>
    </wsdl:service>
</wsdl:definitions>
```



### Triggering code generation

To generate codes, run the `generate` task:

```sh
gradlew generate
```

## Non generated codes

For the application to work, you also need to add some non-generated codes.

### Spring Boot application

To declare Spring Boot application create a class `Application` with the following content:

```java title="Application.java"
package example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```


### Logging REST traffic

The logbook library was used to log HTTP content for the REST protocol.
Therefore, in the `build.gradle.kts` file there is a dependency to the `org.zalando:logbook-spring-boot-starter` library.

To log REST traffic, the following configuration must be also placed in the `application.properties` file:

```properties title="application.properties"
logging.level.org.zalando.logbook=TRACE
```


For the REST client, you must also configure the RestTemplate appropriately. To do this, create a `RestTemplateConfigurator` class with the following code:

```java title="RestTemplateConfigurator.java"
package example;

import lombok.RequiredArgsConstructor;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.zalando.logbook.httpclient.LogbookHttpRequestInterceptor;
import org.zalando.logbook.httpclient.LogbookHttpResponseInterceptor;

@Configuration
@RequiredArgsConstructor
public class RestTemplateConfigurator {
    private final LogbookHttpRequestInterceptor logbookHttpRequestInterceptor;
    private final LogbookHttpResponseInterceptor logbookHttpResponseInterceptor;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .requestFactory(this::clientHttpRequestFactory)
                .build();
    }

    private ClientHttpRequestFactory clientHttpRequestFactory() {
        CloseableHttpClient client = HttpClientBuilder.create()
                .addInterceptorFirst(logbookHttpRequestInterceptor)
                .addInterceptorLast(logbookHttpResponseInterceptor)
                .build();
        return new HttpComponentsClientHttpRequestFactory(client);
    }
}
```


### Logging SOAP traffic

To enable SOAP traffic logging, place the following configuration in the `application.properties` file:

```properties title="application.properties"
logging.level.org.springframework.ws.client.MessageTracing=TRACE
```


### Service implementation

In the generator, a separate class with implementation is generated for each endpoint
(this can be changed by parameterization - by setting the [SpringRestParams.delegateToFacade](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-rest-params/delegate-to-facade.html) property).
The name of such a class is derived from the `operationId` field from the specification (swagger/OpenAPI) and depending on the HTTP method of the operation, the class will have a `Query` or `Command` postfix.

In this case, the service implementation should be done in the `EchoCommond` class.

First, remove the `@Generated` annotation. This is needed to prevent the next generation from removing the modifications to this file.

Next, consider removing the `@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)` annotation, which causes a new instance of this class to be created to handle each request.

The most important thing is to add the request handling code to the `execute` method.
If the API specifies that the operation returns HTTP headers, the execute method should return a `ResponseEntity<T>` object, otherwise it should return a `T`.


```java title="EchoCommand.java"
package example.process;

import example.ports.in.rest.IEchoCommand;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EchoRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
public class EchoCommand implements IEchoCommand {

    @Override
    public ResponseEntity<EchoBodyDto> execute(@Nonnull EchoRequest request) {
        return ResponseEntity.ok()
                .header("x-correlation-id", request.getXCorrelationId())
                .body(request.getBody());
    }
}
```


### RestExceptionHandler

Since the response message structure for bad requests can be freely defined in the API, usually it is necessary to define a mapping of validation errors to this message structure.
To do this you should fix implementation of `toResponseEntity` method in the generated class `RestExceptionHandler`.

There are two ways to modify the generated files:
- simply modify the code and remove the `@Generated` annotation
- modify the code by configuring the generator accordingly

In this case (as well as in the guide [Use Codegen in a maven project - complex](quick-start-spring-maven-complex.md))
the code was modified by configuring the generator (see `typeOfCode(toc.REST_EXCEPTION_HANDLER)` section in `build.gradle.kts`).

### Tests

At the end we will add tests.

The `RestClientServerTest` test verifies the client and server codes of the REST service - it calls the client codes, which in turn calls the server.

```java title="RestClientServerTest.java"
package example;

import example.commons.RestClientException;
import example.ports.out.rest.EchoClient;
import example.ports.in.rest.dtos.EchoBodyDto;
import example.ports.in.rest.dtos.EchoRequest;
import example.ports.in.rest.dtos.ErrorDescriptionDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class RestClientServerTest {

    @Autowired
    private EchoClient echoClient;

    @Test
    void testEchoCall() {
        String xCorrelationId = "xCorrelationId";
        String propAmount = "10.01";
        ZonedDateTime propDateTime = ZonedDateTime.now();
        Double propDouble = 20.02;
        BigDecimal propFloat = BigDecimal.valueOf(40.04);
        Integer propInt = 3;
        ResponseEntity<EchoBodyDto> responseEntity = echoClient.echo(new EchoRequest()
                .setXCorrelationId(xCorrelationId)
                .setBody(new EchoBodyDto()
                        .setPropAmount(propAmount)
                        .setPropDateTime(propDateTime)
                        .setPropDouble(propDouble)
                        .setPropFloat(propFloat)
                        .setPropInt(propInt)
                )
        );

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(xCorrelationId, responseEntity.getHeaders().get("x-correlation-id").get(0));
        assertEquals(propAmount, responseEntity.getBody().getPropAmount());
        assertEquals(propDateTime.toInstant(), responseEntity.getBody().getPropDateTime().toInstant());
        assertEquals(propDouble, responseEntity.getBody().getPropDouble());
        assertEquals(propFloat, responseEntity.getBody().getPropFloat());
        assertEquals(propInt, responseEntity.getBody().getPropInt());
    }

    @Test
    void testEchoCallWithInvalidData() {
        String xCorrelationId = "xCorrelationId";
        RestClientException response = assertThrows(RestClientException.class, () -> echoClient.echo(new EchoRequest()
                .setXCorrelationId(xCorrelationId)
                .setBody(new EchoBodyDto().setPropDouble(0.0))
        ));

        assertEquals(xCorrelationId, response.getHeaders().get("x-correlation-id").get(0));

        ErrorDescriptionDto description = response.getResponse(ErrorDescriptionDto.class);
        assertEquals("prop_double", description.getField());
        assertEquals("is_too_small", description.getCode());
    }
}
```


The `SoapClientTest` test verifies the SOAP service client codes.

```java title="SoapClientTest.java"
package example;

import example.ports.out.soap.CountriesPortServiceClient;
import example.ports.out.soap.dtos.GetCountryRequestSoapDto;
import example.ports.out.soap.dtos.GetCountryResponseSoapDto;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class SoapClientTest {

    @Autowired CountriesPortServiceClient client;

    @Test
    void testSoapCall() throws IOException {
        try (MockWebServer server = new MockWebServer()) {
            server.start(22222);
            server.setDispatcher(new MockDispatcher());
            GetCountryResponseSoapDto response = client.getCountry(new GetCountryRequestSoapDto().setName("Poland"));
            assertNotNull(response);
            assertEquals("PLN", response.getCountry().getCurrency().getValue());
        }
    }

    static class MockDispatcher extends Dispatcher {
        @Override
        public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
            MockResponse response = new MockResponse();
            response.setBody("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header /><SOAP-ENV:Body><ns2:getCountryResponse xmlns:ns2=\"http://spring.io/guides/gs-producing-web-service\"><ns2:country><ns2:name>POLAND</ns2:name><ns2:population>37636508</ns2:population><ns2:capital>Warsaw</ns2:capital><ns2:currency>PLN</ns2:currency></ns2:country></ns2:getCountryResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>");
            return response;
        }
    }
}
```



To perform tests run following task:

```sh
gradlew check
```
