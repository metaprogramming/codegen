---
id: contribution
title: Contribution
sidebar_label: Contribution
---

Codegen is currently under alpha development.

## Get involved

There are many ways to contribute to Codegen, and many of them do not involve writing any code. Here's a few ideas to get started:
* Start using Codegen! Go through the Getting Started guides.
  Does everything work as expected? If not, let us know by opening an issue.
* Help us making the docs better.
  File an issue if you find anything that is confusing or can be improved.
* Look through the issues. If you find an issue you would like to fix, open a merge request.
* You have idea for a new feature? Let us know by opening an issue.

Contributions are very welcome.
If you think you need help planning your contribution,
please ping us on Discord and let us know you are looking for a bit of help.

## License

By contributing to Codegen, you agree that your contributions will be licensed under its Apache License, Version 2.0.

You should make appropriate copyright header for source code.
