---
id: about
title: About codegen
sidebar_label: About
---

## Goal

The goal of the project is to create a tool that will support software development in accordance with model-driven development (MDD) or API-first approach.

It is a framework for creating your own code generators. The code that will meet the coding standards in your team.

Some code generators are provided with the framework.
First of all, they are an illustration of how to make a code generator, and secondly (if in general the given generator meets the expectations) they can be customized and used.


## Advantages and disadvantages of MDD

I believe that this approach to software development has a significant value when based on the description of the model / API code can be generated, while this description should still be created as project documentation.

Then you can expect that in this way the software will be built faster and the technological update will be easier (it will be automated).

In this approach, three key parts are involved in software development:
- model / API (can be made by a less technical and more analytical person)
- code generator (can be used in many projects)
- code (generated and handwritten)

Such decomposition when creating software makes the process certainly more complicated than simply writing code, but if you have a portfolio of similar products, it will definitely be more economical.
