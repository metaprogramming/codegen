---
id: core-design
title: Core design
sidebar_label: Core design
---
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

![Docusaurus](/img/core-design.png)

## Entrypoint

The entry point to start generating code is the [Codegen](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen/index.html) object.
This is also (although not explicitly) the case for the [gradle](getting-started/quick-start-spring-gradle.md) or [maven plugin](getting-started/quick-start-spring-maven.md).

Using this object, you will compose generators and set common parameters used by them,
such as the character set (default UTF-8) and the line separator (default system line separator).

Using the API it might look something like this:
<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
codegen {
    baseDir = projectRoot
    lineSeparator = "\r\n"
    charset = Charsets.UTF_8
    indexFile = File(baseDir, "generated_files.yaml")
    forceMode = false
    addLastGenerationTag = false
    addLastUpdateTag = false

    dataTypeMapper[DataType.NUMBER] = "java.lang.Double"

    params.set(JavaParams::class) {
        isAlwaysGenerateEnumFromValueMethod = true
        generatedAnnotationValue = "my_generation_tag"
    }

    generate(SpringCommonsGenerator()) {
        rootPackage = "com.example.commons"
        typeOfCode().forEach { classCfg ->
            if (classCfg.typeOfCode != toc.REST_EXCEPTION_HANDLER) {
                classCfg.projectSubDir = "src/main/java-gen"
            }
        }
    }

    val apiCommons = restApi(openapiCommonsBody)
    val api = restApi(openapiAppBody, apiCommons)
    val externalApi = restApi(openapiExternalAppBody, apiCommons)

    generate(SpringRestServiceGenerator()) {
        model = apiCommons
        rootPackage = "com.example.commons"
    }

    generate(SpringRestServiceGenerator()) {
        model = api
        rootPackage = "com.example"
        adapterPackageName = "rest"
    }

    generate(SpringRestClientGenerator()) {
        model = externalApi
        rootPackage = "com.example"
        adapterPackageName = "extsysname"
        params.set(JavaParams::class) {
            generatedAnnotationValue = "my_client_generation_tag"
        }
        dataTypeMapper[DataType.NUMBER] = "java.lang.Float"
        typeOfCode(toc.CLIENT) {
            forceGeneration = false
            onDecoration {
                methods.forEach {
                    it.implBody = "// comment\n${it.implBody}"
                }
            }
        }
    }

    generate(SpringSwaggerUiGenerator()) {
        api(api, "api.yaml")
        api(apiCommons, "commons.yaml")
    }
}
```
</TabItem>
<TabItem value="java">
```java
RestApi apiCommons = RestApi.of(openapiCommonsBody);
RestApi api = RestApi.of(openapiAppBody, apiCommons);
RestApi externalApi = RestApi.of(openapiExternalAppBody, apiCommons);

new Codegen()
        .baseDir(projectRoot)
        .lineSeparator("\r\n")
        .charset("UTF-8")
        .indexFile(indexFile)
        .forceMode(false)
        .addLastGenerationTag(false)
        .addLastUpdateTag(false)

        .dataTypeMapper(dtp -> dtp.set(DataType.NUMBER, "java.lang.Double"))

        .params(p -> p.set(JavaParams.class, jp -> {
            jp.setAlwaysGenerateEnumFromValueMethod(true);
            jp.setGeneratedAnnotationValue("my_generation_tag");
        }))

        .generate(new SpringCommonsGenerator(), m -> {
            m.setRootPackage("com.example.commons");
            m.typeOfCode(SpringCommonsGenerator.TOC.REST_EXCEPTION_HANDLER, cb -> {
                cb.projectSubDir("src/main/java-gen");
            });
        })
        .generate(new SpringRestServiceGenerator(), m -> {
            m.model = apiCommons;
            m.rootPackage("com.example.commons");
        })
        .generate(new SpringRestServiceGenerator(), m -> {
            m.model = api;
            m.rootPackage("com.example");
            m.setAdapterPackageName("rest");
        })
        .generate(new SpringRestClientGenerator(), m -> {
            m.model = externalApi;
            m.rootPackage("com.example");
            m.setAdapterPackageName("extsysname");
            m.params.set(JavaParams.class, jp -> jp.setGeneratedAnnotationValue("my_client_generation_tag"));
            m.dataTypeMapper.set(DataType.NUMBER, "java.lang.Float");
            m.typeOfCode(SpringRestClientGenerator.TOC.CLIENT, cb -> {
                cb.onDecoration(template -> {
                    template.getMethods().forEach(methodCm -> {
                        methodCm.setImplBody("// comment\n" + methodCm.getImplBody());
                    });
                });
            });
        })
        .generate(new SpringSwaggerUiGenerator(), m -> {
            m.api(api, "api.yaml");
            m.api(apiCommons, "commons.yaml");
        })
        .run();
```
</TabItem>
</Tabs>

Note that in the case of Kotlin, the [codegen](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/codegen.html)
function is used to create the [Codegen](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen/index.html) object and run the generation.

### Generator
As you can guess from the code above. The hardest part will be creating a [Generator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-generator/index.html) / [GeneratorBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-generator-builder/index.html) object.

The responsibility of the [Generator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-generator/index.html) object is to traverse the model (API) and add tasks to generate a certain *type of code* for its particular elements.

For generators that are planned to have a lot of configuration options, it is recommended to use the
[GeneratorBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-generator-builder/index.html) / [JavaGeneratorBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-java-generator-builder/index.html) class.
This will allow for simple use of the configuration mechanisms already existing in Codegen and will make it easier to use
for people who already know other generators (in Codegen) and trouble-free use of the new generator in the maven or gradle plugin.

### Ready-made generators
There are available following ready-made generators:
- [SpringRestServiceGenerator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-rest-service-generator/index.html) for generating Spring REST services,
- [SpringRestClientGenerator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-rest-client-generator/index.html) for generating Spring REST client,
- [SpringSoapClientGenerator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-soap-client-generator/index.html) for generating Spring WS client,
- [SpringCommonsGenerator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-commons-generator/index.html) for generating utils classes used by above generators.

Even if you plan to create your own generator, a good start would be to review the codes for any of the generators above.

In [how to use it](getting-started/how-to-use.md) article you will learn how to use the above generators in sample projects.

## Type of code and code generation

### Type of code
The type of code is a kind of abstraction for the class. For example, for the REST service, the REST controller is the type of code.
The types of codes will correspond to the particular elements of the solution architecture.

To get a list of code types supported by ready-made generators, just look for static public fields with name `TOC` in the classes of these generators
(e.g. [SpringRestServiceGenerator.TOC](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-rest-service-generator/-t-o-c/index.html)).

### Code generation

The code that will be generated is defined separately for each type of code.
To do this (or update it), use the [JavaGeneratorBuilder.typeOfCode](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-java-generator-builder/type-of-code.html) overloaded method.

The [ClassBuilderConfigurator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/index.html) class is used to specify the code generation.
You can use one of two approaches:
1. Template-based using the FreeMarker template engine (set [freemarkerTemplate](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/freemarker-template.html) property)
   or another template engine (set [template](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/template.html) property).
2. By creating a code model using the following methods:
   - [onDeclaration](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/on-declaration.html)
   - [onImplementation](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/on-implementation.html)
   - [onDecoration](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/on-decoration.html)
   - [builders](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/builders.html) property

While the first approach is simpler, the second is much more flexible and allows you to:
- reusing code generation logic in generating different types of code,
- dividing the code generation logic into independent parts,
- making nontrivial modifications to an existing generator without modifying the existing code generation logic and easily turning them on and off as needed.

Using the [ClassBuilderConfigurator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/index.html) you can also set:
- class name,
- location for the generated classes (package and project).

### Code Model

The code model is represented by the [ClassCm](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-class-cm/index.html) object.
And the [ClassCmBuilderTemplate](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-cm-builder-template/index.html) object is used to construct it.

When using the [onDeclaration](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/on-declaration.html),
[onImplementation](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/on-implementation.html),
[onDecoration](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/on-decoration.html) methods,
the ClassCmBuilderTemplate object will simply be provided (as method parameter) for use.

When using the [builders](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/builders.html) property to implement the [IClassCmBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-i-class-cm-builder/index.html) interface,
you can inherit from the [ClassCmBuilderTemplate](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-cm-builder-template/index.html) class.

### Methods

The method code model is represented by the [MethodCm](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-method-cm/index.html) class.
It describes the input parameters, return type, visibility modifiers, and annotations.

To add a method to a class use the [ClassCm.methods](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-class-cm/methods.html) / [ClassCmBuilderTemplate.methods](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-cm-builder-template/methods.html) property.
If the code generating the method body is a bit more complicated, encapsulate it in a class that implements [IMethodCmBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-i-method-cm-builder/index.html) interface.
You can also inherit from [BaseMethodCmBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-base-method-cm-builder/index.html) class.
To create implementation body you can use a codeBuf property (available in both [BaseMethodCmBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-base-method-cm-builder/index.html) and [ClassCmBuilderTemplate](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-cm-builder-template/index.html)).

## Code index

The code index is represented by an object of type [CodeIndex](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-code-index/index.html).

It is mainly used to register generated code and search it by given parameters.
This allows loose coupling of generators codes, even if the codes generated by them may be related to each other.

Code index is an index of:
- classes, where the key is the *type of code* and model element,
- methods (`mappers`), where the key is the return value type and the list of parameter types.

### Mappers

Not all generated methods are registered in the code index as mappers.
Registering a method as a mapper is done using the [BuildContext.registerMapper](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-build-context/register-mapper.html) method,
although in practice it may be more convenient to use the [MethodCm.registerAsMapper](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-method-cm/register-as-mapper.html) method,
but this should be done when adding the method to the [ClassCmMethods](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-cm-methods/index.html) object (not after adding it).

Registering a method as a mapper in the code index will allow other parts of the generator to search for it based on the
return value type and list of types of input parameters. This means that the generator codes do not need to know either
the name of the class or the method that will be used in the generated codes.

### Class registration in the code index

All generated classes are registered in the code index.
Generators use the [JavaGenerator.register](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-java-generator/register.html) method for this purpose.

### Non-generated codes in the code index

In some cases, the code index can also be used to hook external codes to be used in generated codes.
For this reason, the [CodeIndex](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-code-index/index.html) is also available in the [Codegen](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen/code-index.html) object.

### Code index search

To search for a class corresponding to the type of code for a given model fragment, you can use
[ClassCmBuilderTemplate.getClass](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-cm-builder-template/get-class.html)
/ [ClassCmBuilderTemplate.classLocator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-cm-builder-template/class-locator.html)
/ [BaseMethodCmBuilder.getClass](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-base-method-cm-builder/get-class.html)
/ [BaseMethodCmBuilder.classLocator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-base-method-cm-builder/class-locator.html).

To search for a mapper, you can use [BaseMethodCmBuilder.mapping](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-base-method-cm-builder/mapping.html) or directly [MappingExpressionBuilder](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-mapping-expression-builder/index.html).

The following code illustrates the use of the code register in the above-mentioned scopes:

<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
generate(SpringRestServiceGenerator()) {
    model = getRestApi()
    typeOfCode(toc.OPERATION_EXECUTOR_IMPL) {
        onDeclaration {
            methods.add("execute") {
                registerAsMapper()
                resultType = getClass(toc.DTO, model.successResponseSchema)
                params.add("request", getClass(toc.REQUEST_DTO)) {
                    addAnnotation(Java.nonnul())
                }
                implBody = "// TODO"
            }
        }
    }
    typeOfCode(toc.REST_CONTROLLER) {
        onImplementation {
            methods.add(nameMapper.toMethodName(model.code)) {
                resultType = getClass(toc.DTO, model.successResponseSchema)
                params.add("request", getClass(toc.REQUEST_DTO))
                implBody = "return ${
                    MappingExpressionBuilder(context, model, implDependencies)
                        .to(getClass(toc.DTO, model.successResponseSchema))
                        .from(toc.REQUEST_DTO, "request")
                };"
            }
        }
    }
}
```
</TabItem>
<TabItem value="java">
```java
.generate(new SpringRestServiceGenerator(), gb -> {
    gb.model = getRestApi();
    gb.typeOfCode(SpringRestServiceGenerator.TOC.OPERATION_EXECUTOR_IMPL, cbc -> cbc
            .onDecoration(cb -> cb.getMethods().add("execute", mb -> {
                mb.registerAsMapper();
                mb.setResultType(cb.getClass(SpringRestServiceGenerator.TOC.DTO, cb.getModel().getSuccessResponseSchema()));
                mb.getParams().add("request", cb.getClass(SpringRestServiceGenerator.TOC.REQUEST_DTO), p ->
                        p.addAnnotation(Java.nonnul()));
                mb.setImplBody("// TODO");
            })));
    gb.typeOfCode(SpringRestServiceGenerator.TOC.REST_CONTROLLER, cbc -> cbc
            .onImplementation(cb -> cb.getMethods().add(cb.getNameMapper().toMethodName(cb.getModel().getCode()), mb -> {
                mb.setResultType(cb.getClass(SpringRestServiceGenerator.TOC.DTO, cb.getModel().getSuccessResponseSchema()));
                mb.getParams().add("request", cb.getClass(SpringRestServiceGenerator.TOC.REQUEST_DTO));
                mb.setImplBody("return " + new MappingExpressionBuilder(cb.getContext(), cb.getModel(), mb.getImplDependencies())
                        .to(cb.getClass(SpringRestServiceGenerator.TOC.DTO, cb.getModel().getSuccessResponseSchema()))
                        .from(SpringRestServiceGenerator.TOC.REQUEST_DTO, "request")
                        + ";"
                );
            })));
})
```
</TabItem>
</Tabs>

## Generation phases

There are following phases of creating a code model:
- registration: when [Generator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-generator/index.html) register a class for a given fragment of the model that the given type of code will be built,
- declaration: in this phase the class elements that are used in building other classes must be declared (e.g. registering mappers),
- implementation: building the code model of the class,
- decoration: decorate the class model (e.g. adding an annotation; in the basic generator codes it is best to avoid changes in this phase so that the generator user can safely use it to make their modifications).

## Not generating unnecessary codes

There may be situations when you do not want to generate codes for all API elements.
For example, you may be interested in using only one of many operations from the REST API.
In this case, at the [RestApi](pathname:///kotlin-api/codegen/pl.metaprogramming.model.oas/-rest-api/index.html) model level, you should simply remove unwanted operations
and use the [SpringRestClientGenerator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-rest-client-generator/index.html) generator.

As a result, not only the codes of the calls to the removed operations will not be generated, but also the DTO classes accordingly.

Whether a given type of code is always designated for generation, or is designated for generation only when used
by other code designated for generation, is controlled by the [ClassBuilderConfigurator.forceGeneration](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.base/-class-builder-configurator/force-generation.html) property.

Subsequently, this property is copied to [ClassCd.forceGeneration](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-class-cd/force-generation.html) during code generation.

If the [ClassCd.forceGeneration](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-class-cd/force-generation.html) property is `false`, the [ClassCd.used](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-class-cd/used.html) property is used
to determine whether the class should be generated.

The [ClassCd.used](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-class-cd/used.html) property can be set to `true` in the implementation phase
when the class is referenced from another class that has the
[forceGeneration](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-class-cd/force-generation.html) or [used](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-class-cd/used.html) property set to `true`.

The implementation and decoration phase is performed only for classes
that have the [forceGeneration](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-class-cd/force-generation.html) or [used](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-class-cd/used.html) property set to `true`.

## Codegen params

[CodegenParams](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen-params/index.html) allows you to configure selected aspects that affect the generation of various types of codes.
It is a composition of different sets of parameters for different parts of the generator.

Creating a [CodegenParams](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen-params/index.html) object might look like this:
<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
CodegenParams()
    .set(
        JavaParams()
            .licenceHeader("My licence header")
            .generatedAnnotationClass("javax.annotation.Generated")
            .generatedAnnotationValue("my_generator")
            .isAlwaysGenerateEnumFromValueMethod(true)
    )
    .set(ValidationParams::class) {
        throwExceptionIfValidationFailed = false
        isRequiredErrorCode = "validation.error.required"
        useNamedBeanAsRuleValidator = false
        ruleValidator("USER_ACCOUNT_BALANCE", "com.example.UserAccountBalanceValidator")
        formatValidator("amount", "com.example.AmountValidator")
    }
```
</TabItem>
<TabItem value="java">
```java
new CodegenParams()
        .set(new JavaParams()
                .licenceHeader("My licence header")
                .generatedAnnotationClass("javax.annotation.Generated")
                .generatedAnnotationValue("my_generator")
                .isAlwaysGenerateEnumFromValueMethod(true)
        )
        .set(ValidationParams.class, p -> {
            p.setThrowExceptionIfValidationFailed(false);
            p.setRequiredErrorCode("validation.error.required");
            p.useNamedBeanAsRuleValidator(false);
            p.ruleValidator("USER_ACCOUNT_BALANCE", "com.example.UserAccountBalanceValidator");
            p.formatValidator("amount", "com.example.AmountValidator");
        })
;
```
</TabItem>
</Tabs>

[CodegenParams](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen-params/index.html) is basically a collection of any parameter objects, where the key for the object is their class.

There are two ways to set these parameter objects (via the overloaded [set](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen-params/set.html) method).
- simple passing of a parameter object; the previous object of this class will be overwritten,
- passing the class and update function of the parameter object;
  if the object of this class did not exist, it will be created and set according to the update function;
  if it existed, it will be corrected by the update function.

[CodegenParams](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen-params/index.html) can be set at the global code generation level using [Codegen.params](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen/params.html).
The parameters set here will be copied to the included generators (when calling [Codegen.generate](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen/generate.html)).
If there is a need to set specific parameters at the generator level, it can be done using [GeneratorBuilder.params](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-generator-builder/params.html).

If it is required that the parameters object is copied deep, it should have a method called `copy` which should perform deep copying.

In the case of ready-made generators for this purpose, you will use the following classes:
- [JavaParams](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-java-params/index.html) to parameterize:
  - the annotation used to mark the generated codes (@Generated),
  - aspects related to bean management and dependency injection,
  - the name mapping from the model to the java code,
  - the text processing of generated java codes (license header, code formatting),
  - the generation of the 'fromValue' method for enums (regardless of its use in the generated codes)
- [ValidationParams](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.validation/-validation-params/index.html) to parameterize:
  - the error codes for standard validations,
  - the stopping of (or continuing) validation when a validation error is encountered ([ValidationParams.stopAfterFirstError](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.validation/-validation-params/stop-after-first-error.html)),
  - validators for data [formats](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.validation/-validation-params/format-validator.html) and [rules](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.validation/-validation-params/rule-validator.html) (see [Validation support](validations) guide),
- [SpringRestParams](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-rest-params/index.html) to parameterize the generation of REST server/client codes
- [SpringSoapParams](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-soap-params/index.html) to parameterize the generation of WS/SOAP client codes

## Data type configuration

Data type configuration in the generator, i.e. setting the representation for basic data types (numbers, dates, etc.)
in the generated code should be done using [DataTypeMapper](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java/-data-type-mapper/index.html).

Configuration can be done globally via [Codegen.dataTypeMapper](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen/data-type-mapper.html).
This configuration will be copied to specific generators (when [Codegen.generate](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-codegen/generate.html) is called).

However, if any change to this configuration is required at the level of a specific generator,
[GeneratorBuilder.dataTypeMapper](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/-generator-builder/data-type-mapper.html) should be used.
