---
id: oas-support
title: OpenAPI Specification support
sidebar_label: OpenAPI support
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Import OpenAPI specification

To load OpenAPI specification (OAS2 - swagger/OAS3 - OpenAPI 3) use the static method [RestApi.of](pathname:///kotlin-api/codegen/pl.metaprogramming.model.oas/-rest-api/-companion/of.html).
This method takes the contents of the OAS specification as an argument, and optionally, dependencies (varargs).

<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
val restApi = RestApi.of(apiText)
```
</TabItem>
<TabItem value="java">
```java
RestApi restApi = RestApi.of(apiText);
```
</TabItem>
</Tabs>

## Editing of the OpenAPI model

Before you use the [RestApi](pathname:///kotlin-api/codegen/pl.metaprogramming.model.oas/-rest-api/index.html) model to generate the codes, you can modify it.
This, of course, only makes sense for minor technical modifications.

### Skip code generation for selected operations
<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
restApi.operations.removeIf { it.code == "operationCode" }
```
</TabItem>
<TabItem value="java">
```java
restApi.getOperations().removeIf(it -> it.getCode().equals("operationCode"));
```
</TabItem>
</Tabs>

### Grouping of operations
Ready-made generators require REST operations to be grouped.
This can be done in the OpenAPI specification using "tags" or "x-swagger-router-controller".
If it is not done in the specification, it can be done after import as follows:

<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
restApi.operations.forEach { it.group = "group" }
```
</TabItem>
<TabItem value="java">
```java
restApi.getOperations().forEach(it -> it.setGroup("group"));
```
</TabItem>
</Tabs>

### Addition of a technical parameter for all operations

<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
val newParam = Parameter("x-my-header", ParamLocation.COOKIE, DataType.TEXT)
restApi.addParameter(newParam)
restApi.operations.forEach { it.parameters.add(newParam) }
```
</TabItem>
<TabItem value="java">
```java
Parameter newParam = new Parameter("x-my-header", ParamLocation.COOKIE, DataType.TEXT);
restApi.getParameters().add(newParam);
restApi.getOperations().forEach(o -> o.getParameters().add(newParam));
```
</TabItem>
</Tabs>

## Export as OpenAPI specification

To save [RestApi](pathname:///kotlin-api/codegen/pl.metaprogramming.model.oas/-rest-api/index.html) model as OAS3 specification use the static method [OpenapiWriter.write](pathname:///kotlin-api/codegen/pl.metaprogramming.model.oas/-openapi-writer/-companion/write.html).
There are two variants of this method, one allows you to save the split API into multiple files, the other allows you to save a single flattened file.

<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
OpenapiWriter.write(restApi, File("out/api.yaml"))
```
</TabItem>
<TabItem value="java">
```java
OpenapiWriter.write(restApi, new File("out/api.yaml"));
```
</TabItem>
</Tabs>
