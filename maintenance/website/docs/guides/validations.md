---
id: validations
title: Validation support
sidebar_label: Validation support
---
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

API request validation is one of the core parts of an API implementation. Therefore, Codegen provides support in this area as follows:
- generated codes take into account the constraints described in the OpenAPI specification,
- adds an extension to the OpenAPI specification that allows to include non-standard constraints in the API description (for which appropriate codes are also generated).


## Support for OAS constraints
The following constraints that are part of the OpenAPI specification are supported:
- maximum
- minimum
- maxLength
- minLength
- pattern
- maxItems
- minItems
- uniqueItems
- required
- enum

This means that if any of the above constraints are used in the API, an appropriate data validation code will be generated.
These codes, when a constraint is not met, will generate an error that clearly indicates which field does not meet which constraint.

### Default error codes
If the default error codes for unmet constraints are inappropriate for you, you can change them using [ValidationParams](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.validation/-validation-params/index.html)
by setting the appropriate property with the `ErrorCode` suffix in the name.

<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
CodegenParams().set(ValidationParams::class) {
    isRequiredErrorCode = "validation.error.required"
}
```
</TabItem>
<TabItem value="java">
```java
new CodegenParams().set(ValidationParams.class, vp ->
        vp.setRequiredErrorCode("validation.error.required")
);
```
</TabItem>
</Tabs>

### Format
According to the [OpenAPI specification](https://swagger.io/specification/#data-types), primitives can have a format modifier.
The format modifier is open, and you can set any value.
```yaml
SomeObject:
  properties:
    prop_amount:
      type: number
      format: amount
```

In the above example, the format `amount` was set for the numeric type.
To set the validator for this format, use the [ValidationParams.formatValidator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.validation/-validation-params/format-validator.html) method.
<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
CodegenParams().set(ValidationParams::class) {
    formatValidator("amount", "com.example.AmountValidator")
}
```
</TabItem>
<TabItem value="java">
```java
new CodegenParams().set(ValidationParams.class, vp ->
        vp.formatValidator("amount", "com.example.AmountValidator"));
```
</TabItem>
</Tabs>

The openness of the format modifier and the fact that it essentially describes data constraints makes it a very good point
for injecting custom validations.

## x-constraints
In order to be able to describe any data constraints, an [extension to the OpenAPI specification](https://swagger.io/specification/#specification-extensions) has been added.

Custom constraints are described using the `x-constraints` element that contains a list of objects,
with each object describing a separate constraint.
The property `x-constrains` can be included in an object property specification or directly in an object specification.

The API specification might look like this:
```yaml
RecurringTransfer:
  properties:
    date_from:
      type: string
      format: date
    date_to:
      type: string
      format: date
    amount:
      type: number
      format: amount
      x-constraints:
      - rule: USER_ACCOUNT_BALANCE
        description: Some description about this constraint
        priority: 102
      - rule: USER_DAILY_LIMIT
        error-code: date_to-must-be-greater-than-date_from
        priority: 101
    period:
      type: string
      x-constraints:
      - dictionary: PERIODS_FOR_RECURRING_TRANSFERS
  x-constraints:
  - condition: date_from < date_to
    error-code: date_to-must-be-greater-than-date_from

```

The following types of constraints are supported:
- rule
- dictionary
- condition
- conditional requirement

The `rule` constraint is most universal, but others (`dictionary` and `condition`) improve information transfer
(about input data requirements) and, more importantly, allow the more appropriate codes to be generated.


### rule
The `rule` constraint is a universal construct that allows you to declare any validation rule.
```yaml
amount:
  x-constraints:
  - rule: USER_ACCOUNT_BALANCE
```

Validation rule implementation is injected by rule name. In the above example, the rule `USER_ACCOUNT_BALANCE` should be handled.
There are two ways to do this.

Using the [ValidationParams.ruleValidator](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.validation/-validation-params/rule-validator.html) method:
<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
CodegenParams().set(ValidationParams::class) {
    ruleValidator("USER_ACCOUNT_BALANCE", "com.example.UserAccountBalanceValidator")
}
```
</TabItem>
<TabItem value="java">
```java
new CodegenParams().set(ValidationParams.class, vp ->
        vp.ruleValidator("USER_ACCOUNT_BALANCE", "com.example.UserAccountBalanceValidator")
);
```
</TabItem>
</Tabs>

Or add a bean in the application with the annotation `@Qualifier("USER_ACCOUNT_BALANCE")`:
```java
@Component
@Qualifier("USER_ACCOUNT_BALANCE")
public class UserAccountBalanceValidator implements Checker<BigDecimal> {
    public void check(ValidationContext<BigDecimal> ctx) {
        ...
    }
}
```

Note. If you have configured dependency injection via constructor (the default), you should also add the following [lombok configuration](https://stackoverflow.com/questions/38549657/is-it-possible-to-add-qualifiers-in-requiredargsconstructoronconstructor/50287955#50287955):
```properties title="lombok.config"
lombok.copyableAnnotations += org.springframework.beans.factory.annotation.Qualifier
```

To learn how to create a validator implementation, [go here](#creating-validator-implementation).


### dictionary
The `dictionary` constraint is similar to the enum, except that the allowed values are dynamic,
are not encoded in the API specification, but are stored in some data source that can be changed while the application is running.
```yaml
period:
  x-constraints:
  - dictionary: PERIODS_FOR_RECURRING_TRANSFERS
```

All `dictionary` constraints, even if they refer to different dictionaries,
are validated by one class ([VALIDATION_DICTIONARY_CHECKER](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-commons-generator/-t-o-c/-v-a-l-i-d-a-t-i-o-n_-d-i-c-t-i-o-n-a-r-y_-c-h-e-c-k-e-r.html)).
The class is generated but with empty implementation. Depending on how the different dictionaries are stored,
it may not require additional work to handle the restriction for a new dictionary.

To configure a package for dictionary validator class and enumeration for dictionary codes, do as follows:
<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
generate(SpringCommonsGenerator()) {
    typeOfCode(
        toc.VALIDATION_DICTIONARY_CHECKER,
        toc.VALIDATION_DICTIONARY_CODE_ENUM
    ).forEach { it.packageName.root = "example.adapters.in.rest.validators.custom" }
}
```
</TabItem>
<TabItem value="java">
```java
.generate(new SpringCommonsGenerator(), gb ->
        gb.typeOfCode(
                SpringCommonsGenerator.TOC.VALIDATION_DICTIONARY_CHECKER,
                SpringCommonsGenerator.TOC.VALIDATION_DICTIONARY_CODE_ENUM
        ).forEach(it -> it.getPackageName().setRoot("example.adapters.in.rest.validators.custom")))
```
</TabItem>
</Tabs>

### condition
This constraint can only be used at the object schema specification level and allows you to specify
the expected relationship between two properties of a given object.
```yaml
Filter:
  properties:
    valueFrom:
      type: number
    valueTo:
      type: number
  x-constraints:
  - condition: from < to
```

The `condition` constraint should be set with a comparison expression according to the syntax
```
<OBJECT_PROPERTY_1> <COMPARE_OPERATOR> <OBJECT_PROPERTY_2>
```
Where `<COMPARE_OPERATOR>` can take the following values: `=`, `>`, `>=`, `<`, `<=`.

For the `condition` constraint, all code needed for message validation is generated.
And as long as the field data types (referred by this constraint) implement the `Comparable` interface, they don't need any coding.

## x-constraints options

### if
Custom constraints don't always have to be active. By setting the `if` property you can set when the constraint will take effect.
```yaml
x-constraints:
  - required: true
    if: CONDITION1 AND (CONDITION2 OR operationId[some_operation_id|another_operation_id])
    description: The field is only required in certain circumstances 
```

Expression syntax supports `AND` and `OR` operators. Expressions can also be grouped using parentheses.

The basic elements of an expression (e.g. 'CONDITION1') are any strings of characters
(they cannot contain whitespace and cannot be the values 'AND' and 'OR').
Their logical value (true/false) will be calculated by the ConditionResolver class object
([VALIDATION_CONDITION_RESOLVER](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-commons-generator/-t-o-c/-v-a-l-i-d-a-t-i-o-n_-c-o-n-d-i-t-i-o-n_-r-e-s-o-l-v-e-r.html) type of code).
Only signature of this class is generated - the actual implementation must be done by the programmer.

In addition, the special element `operationId[]` is supported.
It means checking whether the validation of a given element is performed in the context of specific operations.
The implementation of this check is fully generated.


### error-code
For a custom constraint, it is also possible to specify an error code when the constraint is not met.
This should be done using the `error-code` property.
```yaml
amount:
  x-constraints:
  - rule: USER_ACCOUNT_BALANCE
    error-code: daily_transfer_limit_exceeded
```

This is not necessary, but may be helpful when this information is used by the consumer of the service.

The error code set in the API specification overwrites the error code that was used in the validator implementation.

The `type of code` for error codes enumeration is [VALIDATION_ERROR_CODE_ENUM](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-commons-generator/-t-o-c/-v-a-l-i-d-a-t-i-o-n_-e-r-r-o-r_-c-o-d-e_-e-n-u-m.html).

### invalid-pattern-code
The pattern constraint coming from the OpenAPI specification can be very useful and comprehensive.
However, it is not possible to produce a human-readable error message from it.
To be able to configure a dedicated error message for pattern mismatch, you should do something like this:
```yaml
prop_amount:
  type: string
  pattern: ^(0|([1-9][0-9]{0,}))\.\d{2}$
  x-constraints:
  - invalid-pattern-code: invalid_amount
```

### priority
The constraint checks are performed in a specific order. In most cases, the default order will be OK.
In other case you can change the order in which it is checked using the `priority` property (only for custom constraints).
In order to properly set the priority of the constraint check,
you should know that the constraints check is performed in the following order:
- custom constraints with priority `<=` 0,
- required,
- uniqueItems,
- minItems, maxItems,
- enum,
- format,
- pattern,
- minLength, maxLength,
- minimum, maximum
- custom constraints with priority > 0

If the priority of a constraint is not specified explicitly, it is set automatically based on the order
in which it occurs in the API specification (within the context).
The first constraint is given priority 100, the second constraint 101, and so on.

You should know that the validation priority only applies to the given context.
For example, setting a priority of '-1' for a field constraint in an object
only means that the given constraint will be checked before other constraints for that field,
but does not cause the field to be validated before other fields in that object.


### description
Additional textual description can be added to the specification of a custom constraint using the `description` property.
It is not taken into account at all for code generation, but it can be helpful in explaining what the constraint is.


## Injecting constraints into API model
If for some reason you are unable to add custom constraints to the API specification,
you can add them by modifying the API model in the generator configuration.
<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
restApi.getSchema("Address").addConstraint(CheckerRef.of("com.validators.AdvancedAddressValidator"))
restApi.getSchema("Address.state").addConstraint(Constraints.dictionary("SATES"))
```
</TabItem>
<TabItem value="java">
```java
restApi.getSchema("Address").addConstraint(CheckerRef.of("com.validators.AdvancedAddressValidator"));
restApi.getSchema("Address.state").addConstraint(Constraints.dictionary("SATES"));
```
</TabItem>
</Tabs>

First get the schema model using [RestApi.getSchema](pathname:///kotlin-api/codegen/pl.metaprogramming.model.oas/-rest-api/get-schema.html) method.
Then add a constraint using [DataSchema.addConstraint](pathname:///kotlin-api/codegen/pl.metaprogramming.model.data/-data-schema/add-constraint.html) method.
This method takes a [DataConstraint](pathname:///kotlin-api/codegen/pl.metaprogramming.model.data.constraint/-data-constraint/index.html) object as an argument.
You can create it using static methods:
[Constraints.rule](pathname:///kotlin-api/codegen/pl.metaprogramming.model.data.constraint/-constraints/rule.html),
[Constraints.dictionary](pathname:///kotlin-api/codegen/pl.metaprogramming.model.data.constraint/-constraints/dictionary.html),
[Constraints.condition](pathname:///kotlin-api/codegen/pl.metaprogramming.model.data.constraint/-constraints/condition.html).

You can also use the [CheckerRef](pathname:///kotlin-api/codegen/pl.metaprogramming.model.data.constraint/-checker-ref/index.html) object with which you can indicate an implementation
that will verify a given constraint. You can use following static methods:
[of](pathname:///kotlin-api/codegen/pl.metaprogramming.model.data.constraint/-checker-ref/-companion/of.html),
[ofMethodRef](pathname:///kotlin-api/codegen/pl.metaprogramming.model.data.constraint/-checker-ref/-companion/of-method-ref.html),
[ofStaticField](pathname:///kotlin-api/codegen/pl.metaprogramming.model.data.constraint/-checker-ref/-companion/of-static-field.html)
to create this object.


## Validator implementation

To implement the validator, you need to implement the `Checker<T>` interface
([SpringCommonsGenerator.TOC.VALIDATION_CHECKER](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-commons-generator/-t-o-c/-v-a-l-i-d-a-t-i-o-n_-c-h-e-c-k-e-r.html) type of code).
This is a functional interface, and there is only one `void check(ValidationContext<T> context)` method to implement.

A simple validator might look like this:

```java 
@Component
public class AmountChecker implements Checker<BigDecimal> {

    @Override
    public void check(ValidationContext<BigDecimal> ctx) {
        if (ctx.getValue().scale() != 2) {
            ctx.addError("is_not_amount", ctx.getPath() + " is not amount");
        }
    }
}
```


You may also notice that:
- a validation error should be reported using the `ValidationContext.addError(...)` method,
- the value to be checked should be retrieved using the `ValidationContext.getValue()` method,
- there is no protection against null value, this is because by default the validator is not run for null values.

If a given validator should be executed even for null values, the `Checker.checkNull` method should be overridden (it should return true).


```java 
    @Override
    public boolean checkNull() {
        return true;
    }
```


The `ValidationContext.addError(...)` method is overloaded and also accepts a `ValidationError` object.
Using it you can control the http status code returned or whether validation should be stopped.


## Validation error handling

After validation, if there are any errors, `ValidationException` is thrown
([SpringCommonsGenerator.TOC.VALIDATION_EXCEPTION](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-commons-generator/-t-o-c/-v-a-l-i-d-a-t-i-o-n_-e-x-c-e-p-t-i-o-n.html) type of code).

In order to return the appropriate message to the service consumer, it should be handled by a `RestExceptionHandler` class
([SpringCommonsGenerator.TOC.REST_EXCEPTION_HANDLER](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-commons-generator/-t-o-c/-r-e-s-t_-e-x-c-e-p-t-i-o-n_-h-a-n-d-l-e-r.html) type of code).
It does not have a full implementation and will certainly require manual modification (or generation modification).

If necessary, a validation error can also be reported outside the validation step by throwing a `ValidationException`
([SpringCommonsGenerator.TOC.VALIDATION_EXCEPTION](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.spring/-spring-commons-generator/-t-o-c/-v-a-l-i-d-a-t-i-o-n_-e-x-c-e-p-t-i-o-n.html) type of code).

It is also possible to set whether the validation should stop when the first error is encountered or continue.
This can be done by setting a [ValidationParams.stopAfterFirstError](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen.java.validation/-validation-params/stop-after-first-error.html) parameter.

<Tabs groupId="kotlin-java" defaultValue="kotlin" values={[{label: 'kotlin', value: 'kotlin'}, {label: 'java', value: 'java'}]}>
<TabItem value="kotlin">
```kt
CodegenParams().set(ValidationParams::class) {
    stopAfterFirstError = true
}
```
</TabItem>
<TabItem value="java">
```java
new CodegenParams().set(ValidationParams.class, vp ->
        vp.setStopAfterFirstError(true)
);
```
</TabItem>
</Tabs>
