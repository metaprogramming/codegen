---
id: quick-start-spring-soap-client
title: Generate SOAP client to use with Spring
sidebar_label: Spring SOAP client
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

This guide walks you through the process of creating with codegen an application which consuming a SOAP-based web service with Spring.

The application consumes services created in this [guide](https://spring.io/guides/gs/producing-web-service/).

[Here](https://spring.io/guides/gs/consuming-web-service/) is a guide for creating this client in more traditional way.

## Project structure

Our project will consist of two subprojects:
- `app`: contains Spring Boot application
- `generator`: contains API description and generator configuration. It is used to generate codes for the `app` subproject

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ws
│   │       │       │   ├── schema <GENERATED>
│   │       │       │   ├── CountriesClient.java <GENERATED>
│   │       │       │   └── CountriesClientConfiguration.java <GENERATED>
│   │       │       └── Application.java
│   │       └── resources
│   │           └── logback-spring.xml
│   └── pom.xml
├── generator
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── GeneratorRunner.java <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── countries.wsdl <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── pom.xml
└── pom.xml
.
```

</TabItem>
<TabItem value="groovy">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ws
│   │       │       │   ├── schema <GENERATED>
│   │       │       │   ├── CountriesClient.java <GENERATED>
│   │       │       │   └── CountriesClientConfiguration.java <GENERATED>
│   │       │       └── Application.java
│   │       └── resources
│   │           └── logback-spring.xml
│   └── build.gradle
├── generator
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── GeneratorRunner.java <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── countries.wsdl <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── build.gradle
├── build.gradle
└── settings.gradle
.
```

</TabItem>
</Tabs>

## Root project

First create a project directory and put `pom.xml` (if you prefer maven) or `build.gradle` and `settings.gradle` (if you prefer gradle):

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-soap-client-maven/pom.xml</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-soap-client-gradle/build.gradle</includeFile>
  <includeFile>${codegen-quickstart-dir}/spring-soap-client-gradle/settings.gradle</includeFile>
</TabItem>
</Tabs>


## Generator subproject

Then create the `generator` subproject with the following `pom.xml` / `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-soap-client-maven/generator/pom.xml</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-soap-client-gradle/generator/build.gradle</includeFile>
</TabItem>
</Tabs>


### API description

Let's assume that we have the following API to integrate with:
<includeFile>${codegen-quickstart-dir}/spring-soap-client-maven/generator/src/main/resources/countries.wsdl</includeFile>
Place the API definition in the `src/main/resources/countries.wsdl` file.

### Generator configuration

To configure code generation you need something like this:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Java', value: 'java'},
    {label: 'Groovy', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-soap-client-maven/generator/src/main/java/GeneratorRunner.java</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-soap-client-gradle/generator/src/main/groovy/GeneratorRunner.groovy</includeFile>
</TabItem>
</Tabs>

Place above code into `src/main/java/GeneratorRunner.java` (if you prefer java) or into `src/main/groovy/GeneratorRunner.groovy` (if you prefer groovy).

The code performs:
- imports WSDL API from `countries.wsdl` file 
- configures `commons`
  - specifies needed classes,
  - sets the project directory to `app`,
  - sets root package to `example.commons`
- configures `countries-client` module:
  - sets the project directory to `app`,
  - sets root package to `example.ws` for generated codes,
  - sets mapping xml namespace to java package (consequence of using JAXB)
  - removes the 'PortService' suffix from the service name (to generate the 'CountriesClient' class instead of 'CountriesPortServiceClient')
- generates codes for above modules

### Triggering code generation

To generate codes run following task:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f generator/pom.xml compile exec:java
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :generator:run
```

</TabItem>
</Tabs>


## App subproject

Most of the codes for this subproject was generated in the previous step, however there are a few things to do.

Create subproject definition with following `pom.xml` or `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-soap-client-maven/app/pom.xml</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-soap-client-gradle/app/build.gradle</includeFile>
</TabItem>
</Tabs>


### Spring Boot application

To declare Spring Boot application create a class `example.Application` with the following content:
<includeFile>${codegen-quickstart-dir}/spring-soap-client-maven/app/src/main/java/example/Application.java</includeFile>

### Logging SOAP traffic

To enable logging for REST traffic, create a file `src/main/resources/logback-spring.xml` with the following content:
<includeFile>${codegen-quickstart-dir}/spring-soap-client-maven/app/src/main/resources/logback-spring.xml</includeFile>

## Run and test

To call the SOAP service, just execute the command:


<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f app/pom.xml spring-boot:run
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :app:bootRun
```

</TabItem>
</Tabs>

:::note
You must be running the server described in the [tutorial](https://spring.io/guides/gs/producing-web-service/).
:::


## Full project sources

You can download the full project source codes from the GitLab repository:
- [spring boot REST server - gradle](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-soap-client-gradle)
- [spring boot REST server - maven](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-soap-client-maven)
