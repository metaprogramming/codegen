---
id: quick-start-spring-rest-client
title: Generate REST client to use with Spring
sidebar_label: Spring REST client
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

This guide walks you through the process of creating with codegen a REST client with Spring.

## Project structure

Our project will consist of two subprojects:
- `app`: contains Spring Boot application
- `generator`: contains API description and generator configuration. It is used to generate codes for the `app` subproject

It will look like this:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── adapters <GENERATED>
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ports <GENERATED>
│   │       │       ├── Application.java
│   │       │       ├── EndpointProvider.java
│   │       │       └── RestTemplateConfigurator.java
│   │       └── resources
│   │           └── application.properties
│   └── pom.xml
├── generator
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── GeneratorRunner.java <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── example-api.yaml <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── pom.xml
└── pom.xml
.
```

</TabItem>
<TabItem value="groovy">

```sh
project-root
├── app
│   ├── src
│   │   └── main
│   │       ├── java
│   │       │   └── example
│   │       │       ├── adapters <GENERATED>
│   │       │       ├── commons <GENERATED>
│   │       │       ├── ports <GENERATED>
│   │       │       ├── Application.java
│   │       │       ├── EndpointProvider.java
│   │       │       └── RestTemplateConfigurator.java
│   │       └── resources
│   │           └── application.properties
│   └── build.gradle
├── generator
│   ├── src
│   │   └── main
│   │       ├── groovy
│   │       │   └── GeneratorRunner.groovy <GENERATOR CONFIGURATION>
│   │       └── resources
│   │           ├── example-api.yaml <API DESCRIPTION>
│   │           └── GeneratedFiles.yaml <GENERATED FILE INDEX>
│   └── build.gradle
├── build.gradle
└── settings.gradle
.
```

</TabItem>
</Tabs>


## Root project

First create a project directory and put `pom.xml` (if you prefer maven) or `build.gradle` and `settings.gradle` (if you prefer gradle):

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-rs-client-maven/pom.xml</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-rs-client-gradle/build.gradle</includeFile>
  <includeFile>${codegen-quickstart-dir}/spring-rs-client-gradle/settings.gradle</includeFile>
</TabItem>
</Tabs>


## Generator subproject

Then create the `generator` subproject with the following `pom.xml` / `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-rs-client-maven/generator/pom.xml</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-rs-client-gradle/generator/build.gradle</includeFile>
</TabItem>
</Tabs>

### API description

Let's assume that we have the following API to implement:

<includeFile>${codegen-quickstart-dir}/spring-rs-client-maven/generator/src/main/resources/example-api.yaml</includeFile>

:::note
OAS2 (swagger 2) and OAS3 (OpenAPI 3) are supported.
:::

Place the API definition in the `src/main/resources/example-api.yaml` file.

Luckily the API is the same as in the [Generate REST services to use with Spring](quick-start-spring-rest-server.md) tutorial, so you can use the server to test this client.

### Generator configuration

To configure code generation you need something like this:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Java', value: 'java'},
    {label: 'Groovy', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-rs-client-maven/generator/src/main/java/GeneratorRunner.java</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-rs-client-gradle/generator/src/main/groovy/GeneratorRunner.groovy</includeFile>
</TabItem>
</Tabs>

Place above code into `src/main/java/GeneratorRunner.java` (if you prefer java) or into `src/main/groovy/GeneratorRunner.groovy` (if you prefer groovy).

The code performs:
- imports REST API from `example-api.yaml` OpenAPI file (see [guide](guides/oas-support.md) to get more about OAS import)
- configures `commons` module (API independent utility codes): sets the project directory to `app` and package to `example.commons` for generated codes (except EndpointProvider class, which is part of application configuration, so is placed in main package)
- configures `example-api` module: sets the project directory to `app` and root package to `example` for generated codes
- generates codes for modules
  - `commons`
  - `example-api` (REST API adapter for example-api.yaml - client)

:::note
Codegen uses `modules` to distinguish different group of codes.

Codes of one module can be placed into different packages and even different projects - it makes sense, since is generated adapter, port, and implementation stub. By default, the codes are grouped in packages: adapters, ports and application.

Codes from many modules can also be placed into one package/project - maybe it makes sense sometimes, you can do it anyway.
:::


### Triggering code generation

To generate codes run following task:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f generator/pom.xml compile exec:java
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :generator:run
```

</TabItem>
</Tabs>


## App subproject

Most of the codes for this subproject was generated in the previous step, however there are a few things to do.

Create subproject definition with following `pom.xml` or `build.gradle`:

<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">
  <includeFile>${codegen-quickstart-dir}/spring-rs-client-maven/app/pom.xml</includeFile>
</TabItem>
<TabItem value="groovy">
  <includeFile>${codegen-quickstart-dir}/spring-rs-client-gradle/app/build.gradle</includeFile>
</TabItem>
</Tabs>


### Spring Boot application and client calls

To declare Spring Boot application create a class `example.Application` with the following content:
<includeFile>${codegen-quickstart-dir}/spring-rs-client-maven/app/src/main/java/example/Application.java</includeFile>

### Logging REST traffic

To enable logging for REST traffic (and to disable starting tomcat), create a file `src/main/resources/application.properties` with the following content:
<includeFile>${codegen-quickstart-dir}/spring-rs-client-maven/app/src/main/resources/application.properties</includeFile>

You also need to configure the RestTemplate accordingly. For this purpose create `RestTemplateConfigurator` class:
<includeFile>${codegen-quickstart-dir}/spring-rs-client-maven/app/src/main/java/example/RestTemplateConfigurator.java</includeFile>

### Configure endpoint

To set endpoint for calling REST service set BASE_URL with application.properties. The default is `http://localhost:8080` so you probably won't need it for testing.

However, if your application will use multiple services located on different hosts, you will need to configure this by modifying the generated `example.EndpointProvider` class.


:::note
After changing generated class you should remove `@Generated` annotation.
:::

:::danger
In principle, you should avoid modifying generated classes, otherwise the codes will be more difficult to develop and maintain, but sometimes is necessary.
:::


## Run and test

To try created client, execute the command:


<Tabs
  groupId="java-groovy"
  defaultValue="java"
  values={[
    {label: 'Maven', value: 'java'},
    {label: 'Gradle', value: 'groovy'},
  ]
}>
<TabItem value="java">

```sh
mvn -f app/pom.xml spring-boot:run
```

</TabItem>
<TabItem value="groovy">

```sh
./gradlew :app:bootRun
```

</TabItem>
</Tabs>

:::note
Remember to run server created in the [Generate REST services to use with Spring](quick-start-spring-rest-server.md) guide.
:::


## Full project sources

You can download the full project source codes from the GitLab repository:
- [spring boot REST server - gradle](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-rs-client-gradle)
- [spring boot REST server - maven](https://gitlab.com/metaprogramming/codegen/-/tree/master/quickstart-projects/spring-rs-client-maven)
