---
id: why
title: Why working on models is better than just coding?
sidebar_label: Why...
---

Because:
* model building is cheaper than the whole implementation
* models are just simpler
* models are better suited to transfer knowledge
* <ins>a lot of code can be generated based on the model</ins>

## API first approach

The above statements are especially true for API models.

In addition, both the server development team and the client development team will use the API.
And each one of them can use it to generate the code.

Moreover, API can develop either analyst, server programmer, client programmer or all them together.

API models often meet [good models criteria](#good-model-description-criteria).

## Reusable code and technical update

It seems that this approach appears to provide better opportunities for creating reusable code.
1. The generator and generator configuration is indeed a reusable code.
1. Models may also be easier to reuse because their description is less detailed.

Because lots of code is generated it's cheaper to update these codes by update the generator.

## When to avoid using models

The model is a new abstraction layer which, if it is inappropriate,
 can make work on the system more difficult.
 
It is not good when the model description standards are an additional threshold for entering the project.

## Good model description criteria

1. Model description language should be a popular standard.
1. Model description language should be adequate for the problem.
