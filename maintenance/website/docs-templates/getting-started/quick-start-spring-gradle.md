---
id: quick-start-spring-gradle
title: Use Codegen in a gradle project
sidebar_label: Gradle plugin
---

This guide will walk you through the process of generating codes using the Codegen Gradle plugin (`pl.metaprogramming.codegen`)
and give you a general overview of how to use Codegen.

:::info
The plugin requires at least **Gradle** version **8.0**.
:::

This example will generate codes for
a <javadoc>REST server|SpringRestServiceGenerator</javadoc>,
a <javadoc>REST client|SpringRestClientGenerator</javadoc>,
and a <javadoc>SOAP client|SpringSoapClientGenerator</javadoc>.

The code generation setup in this example is not as simple as possible.
They are intentionally complicated to show how code generation can be configured.

This example uses the API described in the OpenAPI/swagger and WSDL standards.

Both REST client and REST server codes are generated for the same API described in OpenAPI/swagger.

API documentation using swagger-ui was also generated using the <javadoc>SpringSwaggerUiGenerator</javadoc> generator.

## Project structure

Our project files will look like this (only selected files and directories are presented):

```sh
project-root
├── src
│   └── main
│       ├── java
│       │   └── example
│       │       ├── adapters <GENERATED>
│       │       │   ├── in
│       │       │   └── out
│       │       │       ├── rest
│       │       │       └── soap
│       │       ├── commons <GENERATED>
│       │       ├── ports <GENERATED>
│       │       │   ├── in
│       │       │   └── out
│       │       │       ├── rest
│       │       │       └── soap
│       │       ├── process <GENERATED classes stubs to implementation>
│       │       ├── Application.java
│       │       ├── RestExceptionHandler.java <GENERATED>
│       │       └── RestTemplateConfigurator.java
│       ├── resources
│       │    └── application.properties
│       └── static <GENERATED swagger-ui page>
│            ├── example-api.yaml
│            └── index.html
├── build.gradle.kts <build project script>
├── countries.wsdl <api description for SOAP client>
├── example-api.yaml <api description for REST client/server>
├── generatedFiles.yaml <generated files index>
└── settings.gradle.kts
.
```

## Project build script and generator configuration

<includeFile>${codegen-quickstart-dir}/spring-gradle/build.gradle.kts</includeFile>

Code generation configuration should be done in the `codegen` section.
In this section you are in the context of an object of type <javadoc>Codegen</javadoc>. So you should familiarize yourself with the API of this class.

The generation of specific codes is indicated in the `generate` section.
In this section, you must provide a specific code generator and the model/API for which the codes will be generated.
In this section you are in the context of an object of subtype <javadoc>GeneratorBuilder</javadoc> (for java code generator it will be subtype of the <javadoc>JavaGeneratorBuilder</javadoc>).
So you should familiarize yourself with the API of these classes, especially the <javadoc>JavaGeneratorBuilder</javadoc>,
which provides a lot of generation configuration mechanisms.

The next level of code generation configuration is the type of code (e.g. DTO, controller).
Types of code are specific to specific generators. The list of types of code specific to a given generator is accessed by the `toc` property (is of generic type and is inherited from <javadoc>GeneratorBuilder.toc</javadoc>) or, by a static internal object called `TOC` (if the generator follows the convention).
To configure the type of code you should use `typeOfCode` section (calls the <javadoc>JavaGeneratorBuilder.typeOfCode</javadoc> method).
In this section you are in the context of the <javadoc>ClassBuilderConfigurator</javadoc> object.

A more detailed description of the code generation configuration options can be found in the [Core design guide](guides/core-design.md).


### OpenAPI - REST

Place the following API definition in the file:
<includeFile>${codegen-quickstart-dir}/spring-gradle/example-api.yaml</includeFile>

At this point, it is worth mentioning that codegen also supports OpenAPI extensions for specifying more complex constraints on data (see [Validation support guide](guides/validations.md) to get more info).

### WSDL - SOAP

Place the following API definition in the file:
<includeFile>${codegen-quickstart-dir}/spring-gradle/countries.wsdl</includeFile>


### Triggering code generation

To generate codes, run the `generate` task:

```sh
gradlew generate
```

## Non generated codes

For the application to work, you also need to add some non-generated codes.

### Spring Boot application

To declare Spring Boot application create a class `Application` with the following content:
<includeFile>${codegen-quickstart-dir}/spring-gradle/src/main/java/example/Application.java</includeFile>

### Logging REST traffic

The logbook library was used to log HTTP content for the REST protocol.
Therefore, in the `build.gradle.kts` file there is a dependency to the `org.zalando:logbook-spring-boot-starter` library.

To log REST traffic, the following configuration must be also placed in the `application.properties` file:
<includeFile path="${codegen-quickstart-dir}/spring-gradle/src/main/resources/application.properties">
    <line>logging.level.org.zalando.logbook=TRACE</line>
</includeFile>

For the REST client, you must also configure the RestTemplate appropriately. To do this, create a `RestTemplateConfigurator` class with the following code:
<includeFile>${codegen-quickstart-dir}/spring-gradle/src/main/java/example/RestTemplateConfigurator.java</includeFile>

### Logging SOAP traffic

To enable SOAP traffic logging, place the following configuration in the `application.properties` file:
<includeFile path="${codegen-quickstart-dir}/spring-gradle/src/main/resources/application.properties">
    <line>logging.level.org.springframework.ws.client.MessageTracing=TRACE</line>
</includeFile>

### Service implementation

In the generator, a separate class with implementation is generated for each endpoint
(this can be changed by parameterization - by setting the <javadoc>SpringRestParams.delegateToFacade</javadoc> property).
The name of such a class is derived from the `operationId` field from the specification (swagger/OpenAPI) and depending on the HTTP method of the operation, the class will have a `Query` or `Command` postfix.

In this case, the service implementation should be done in the `EchoCommond` class.

First, remove the `@Generated` annotation. This is needed to prevent the next generation from removing the modifications to this file.

Next, consider removing the `@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)` annotation, which causes a new instance of this class to be created to handle each request.

The most important thing is to add the request handling code to the `execute` method.
If the API specifies that the operation returns HTTP headers, the execute method should return a `ResponseEntity<T>` object, otherwise it should return a `T`.

<includeFile>${codegen-quickstart-dir}/spring-gradle/src/main/java/example/process/EchoCommand.java</includeFile>

### RestExceptionHandler

Since the response message structure for bad requests can be freely defined in the API, usually it is necessary to define a mapping of validation errors to this message structure.
To do this you should fix implementation of `toResponseEntity` method in the generated class `RestExceptionHandler`.

There are two ways to modify the generated files:
- simply modify the code and remove the `@Generated` annotation
- modify the code by configuring the generator accordingly

In this case (as well as in the guide [Use Codegen in a maven project - complex](quick-start-spring-maven-complex.md))
the code was modified by configuring the generator (see `typeOfCode(toc.REST_EXCEPTION_HANDLER)` section in `build.gradle.kts`).

### Tests

At the end we will add tests.

The `RestClientServerTest` test verifies the client and server codes of the REST service - it calls the client codes, which in turn calls the server.
<includeFile>${codegen-quickstart-dir}/spring-gradle/src/test/java/example/RestClientServerTest.java</includeFile>

The `SoapClientTest` test verifies the SOAP service client codes.
<includeFile>${codegen-quickstart-dir}/spring-gradle/src/test/java/example/SoapClientTest.java</includeFile>


To perform tests run following task:

```sh
gradlew check
```
