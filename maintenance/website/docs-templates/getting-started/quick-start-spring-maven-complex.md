---
id: quick-start-spring-maven-complex
title: Use Codegen in a maven project - complex
sidebar_label: Maven plugin - complex
---

This guide is an extension of the guide [Use Codegen in a maven project](quick-start-spring-maven.md).
Its purpose is to demonstrate how to perform advanced code generation configuration using java code in a maven project.

Specifically, the code generation of the `RestExceptionHandler` class will be configured as it was done in the guide [Use Codegen in a gradle project](quick-start-spring-gradle.md).

To configure code generation using Java code in a Maven project, this configuration code must be separated into a library,
which must then be used in the `codegen-maven-plugin` plugin configuration.

In this case the configuration code was separated into the `generator` module.
However, it could just as well be in a standalone maven project
(the only important thing is that it should be available in the maven repository).

## Project structure

Our project files will look like this (only selected files and directories are presented):

```sh
parent-project
├── app <almost like the project in the guide "Use Codegen in a maven project">
│   ├── ...
│   └── pom.xml
├── generator 
│   ├── src
│   │   └── main
│   │       └── java
│   │           └── example
│   │               └── RestExceptionGeneratorInterceptor.java
│   └── pom.xml
└── pom.xml
.
```

## Parent project
In the root directory, place the `pom.xml` file:
<includeFile>${codegen-quickstart-dir}/spring-maven-complex/pom.xml</includeFile>

## Generator module
In directory `generator` place the `pom.xml` file:
<includeFile>${codegen-quickstart-dir}/spring-maven-complex/generator/pom.xml</includeFile>

In directory `generator/src/main/java/example` place the `RestExceptionGeneratorInterceptor.java` file:
<includeFile>${codegen-quickstart-dir}/spring-maven-complex/generator/src/main/java/example/RestExceptionGeneratorInterceptor.java</includeFile>

## App module
In the "app" directory, prepare the files as done in the [Use Codegen in a maven project](quick-start-spring-maven.md) guide.

Do not modify the generated class `RestExceptionHandler`.
Modify the file `pom.xml` instead.
There are basically two changes in the code generation configuration - `codegen-maven-plugin` plugin:
- a dependency to the `generator` module has been added
  ```xml
  <dependency>
    <groupId>pl.metaprogramming.codegen.examples</groupId>
    <artifactId>generator</artifactId>
    <version>${project.version}</version>
  </dependency>
  ```
- a code
  ```xml
  <interceptor class="example.RestExceptionGeneratorInterceptor" />
  ```
  was added into the element
  ```xml
  <generator class="pl.metaprogramming.codegen.java.spring.SpringCommonsGenerator">
  ```

Finally `pom.xml` looks like:
<includeFile>${codegen-quickstart-dir}/spring-maven-complex/app/pom.xml</includeFile>

## Project build and test

To generate codes, build the project and run tests, execute the command:
```shell
mvn install
```

Alternatively, you can execute a series of commands:
- build and install the 'generator' module to the local maven repository
  ```shell
  mvn -f generator install
  ```
- generate codes
  ```shell
  mvn -f app codegen:generate
  ```
- build and run tests of the 'app' module
  ```shell
  mvn -f app package
  ```

## More about "interceptor" element
The `interceptor` element can be placed in `generator` elements.

The `interceptor` element should have the `class` attribute set, the value of which must indicate a class implementing
the `java.function.Consumer<T>` interface, where `T` should correspond to the generator class
(a subclass of `GeneratorBuilder`, in this case `SpringCommonsGenerator`).

The `interceptor` element can be placed multiple times in a single `generator` element.
The order of execution of interceptors will be the order in which they are placed in the xml.
