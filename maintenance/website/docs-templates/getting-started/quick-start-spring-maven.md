---
id: quick-start-spring-maven
title: Use Codegen in a maven project
sidebar_label: Maven plugin
---

This guide will walk you through the process of generating codes using the Codegen Maven plugin (`codegen-maven-plugin`).

This example will generate codes for
a <javadoc>REST server|SpringRestServiceGenerator</javadoc>,
a <javadoc>REST client|SpringRestClientGenerator</javadoc>,
and a <javadoc>SOAP client|SpringSoapClientGenerator</javadoc>.

The code generation setup in this example is not as simple as possible.
They are intentionally complicated to show how code generation can be configured.

This example uses the API described in the OpenAPI/swagger and WSDL standards.

Both REST client and REST server codes are generated for the same API described in OpenAPI/swagger.

API documentation using swagger-ui was also generated using the <javadoc>SpringSwaggerUiGenerator</javadoc> generator.

In principle the Codegen Maven plugin configuration reflects the codegen API.
Using this plugin it should be possible to parameterize the generator in any way that will respond to setting a simple or complex value via the API.

You can also pass code that will configure code generation (like in the guide [Use Codegen in a gradle project](getting-started/quick-start-spring-gradle.md)).
However, then the project structure becomes a bit more complicated, because the code cannot be passed directly in "pom.xml".
The description of how to do this is in the article [Use Codegen in a maven project - complex](getting-started/quick-start-spring-maven-complex.md).

To deepen your knowledge about Codegen follow the [core design](guides/core-design.md) article.

## Project structure

Our project files will look like this (only selected files and directories are presented):

```sh
project-root
├── src
│   └── main
│       ├── java
│       │   └── example
│       │       ├── adapters <GENERATED>
│       │       │   ├── in
│       │       │   └── out
│       │       │       ├── rest
│       │       │       └── soap
│       │       ├── commons <GENERATED>
│       │       ├── ports <GENERATED>
│       │       │   ├── in
│       │       │   └── out
│       │       │       ├── rest
│       │       │       └── soap
│       │       ├── process <GENERATED classes stubs to implementation>
│       │       ├── Application.java
│       │       ├── RestExceptionHandler.java <GENERATED to customize>
│       │       └── RestTemplateConfigurator.java
│       ├── resources
│       │    └── application.properties
│       └── static <GENERATED swagger-ui page>
│            ├── example-api.yaml
│            └── index.html
├── countries.wsdl <api description for SOAP client>
├── example-api.yaml <api description for REST client/server>
├── generatedFiles.yaml <generated files index>
└── pom.xml
.
```

## Project build script and generator configuration
<includeFile>${codegen-quickstart-dir}/spring-maven/pom.xml</includeFile>

### OpenAPI - REST

Place the following API definition in the file:
<includeFile>${codegen-quickstart-dir}/spring-maven/example-api.yaml</includeFile>

At this point, it is worth mentioning that codegen also supports OpenAPI extensions for specifying more complex constraints on data (see [Validation support guide](guides/validations.md) to get more info).

### WSDL - SOAP

Place the following API definition in the file:
<includeFile>${codegen-quickstart-dir}/spring-maven/countries.wsdl</includeFile>


### Triggering code generation

To generate codes run following task:

```sh
mvn codegen:generate
```

## Non generated codes

For the application to work, you also need to add some non-generated codes.

### Spring Boot application

To declare Spring Boot application create a class `Application` with the following content:
<includeFile>${codegen-quickstart-dir}/spring-maven/src/main/java/example/Application.java</includeFile>

### Logging REST traffic

The logbook library was used to log HTTP content for the REST protocol.
Therefore, in the `pom.xml` file there is a dependency to the `org.zalando:logbook-spring-boot-starter` library.

To log REST traffic, the following configuration must be also placed in the `application.properties` file:
<includeFile path="${codegen-quickstart-dir}/spring-maven/src/main/resources/application.properties">
    <line>logging.level.org.zalando.logbook=TRACE</line>
</includeFile>

For the REST client, you must also configure the RestTemplate appropriately. To do this, create a `RestTemplateConfigurator` class with the following code:
<includeFile>${codegen-quickstart-dir}/spring-maven/src/main/java/example/RestTemplateConfigurator.java</includeFile>

### Logging SOAP traffic

To enable SOAP traffic logging, place the following configuration in the `application.properties` file:
<includeFile path="${codegen-quickstart-dir}/spring-maven/src/main/resources/application.properties">
    <line>logging.level.org.springframework.ws.client.MessageTracing=TRACE</line>
</includeFile>

### Service implementation

In the generator, a separate class with implementation is generated for each endpoint
(this can be changed by parameterization - by setting the <javadoc>SpringRestParams.delegateToFacade</javadoc> property).
The name of such a class is derived from the `operationId` field from the specification (swagger/OpenAPI) and depending on the HTTP method of the operation, the class will have a `Query` or `Command` postfix.

In this case, the service implementation should be done in the `EchoCommond` class.

First, remove the `@Generated` annotation. This is needed to prevent the next generation from removing the modifications to this file.

Next, consider removing the `@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)` annotation, which causes a new instance of this class to be created to handle each request.

The most important thing is to add the request handling code to the `execute` method.
If the API specifies that the operation returns HTTP headers, the execute method should return a `ResponseEntity<T>` object, otherwise it should return a `T`.

<includeFile>${codegen-quickstart-dir}/spring-maven/src/main/java/example/process/EchoCommand.java</includeFile>

### RestExceptionHandler

Since the response message structure for bad requests can be freely defined in the API, it is necessary to define a mapping of validation errors to this message structure.
To do this you should fix implementation of `toResponseEntity` method in the generated class `RestExceptionHandler`.

<includeFile path="${codegen-quickstart-dir}/spring-maven/src/main/java/example/RestExceptionHandler.java">
    <block>toResponseEntity(ValidationResult result, WebRequest request)</block>
</includeFile>

Don't forget to remove the `@Generated` annotation.

Modification of the generated code can be done by configuring the generator (but not in this simple scenario).
This was done in guides [Use Codegen in a gradle project](quick-start-spring-gradle.md)
and [Use Codegen in a maven project - complex](quick-start-spring-maven-complex.md).

### Tests

At the end we will add tests.

The `RestClientServerTest` test verifies the client and server codes of the REST service - it calls the client codes, which in turn calls the server.
<includeFile>${codegen-quickstart-dir}/spring-maven/src/test/java/example/RestClientServerTest.java</includeFile>

The `SoapClientTest` test verifies the SOAP service client codes.
<includeFile>${codegen-quickstart-dir}/spring-maven/src/test/java/example/SoapClientTest.java</includeFile>


## Project build and test

To generate codes, build the project and run tests, execute the command:
```shell
mvn install
```

To generate codes only, execute the command:
```shell
mvn codegen:generate
```
