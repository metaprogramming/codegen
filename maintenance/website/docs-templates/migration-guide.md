---
id: migration-guide
title: Migration guide
sidebar_label: Migration guide
---

Sorry for the changes that force changes to your codes as well. However, I think most of these changes will make your codes easier to read as well.

Below you will find tips on how to migrate to the next versions with breaking changes.

## Note
Starting from version 1.0, a new category of breaking change was introduced: "slightly breaking". It means that a certain change may cause difficulties in migration to a newer version, but these difficulties will be relatively easy to overcome. These are, for example, changes to the nomenclature (codegen codes or generated codes) or changes that can be mitigated by additional codegen parameterization.

The "slightly breaking" changes influence on the MINOR version (not the MAJOR).

Changes to the generated code are considered breaking when it changes the way that code is used - as long as it is a code that (according to certain assumptions) at all can be directly invoked by non-generated code.

## Breaking changes in 2.0.0
This version brought a lot of changes.
Fortunately, the core design hasn't changed much.
Unfortunately, there are a lot of name changes and API improvements.
So we suggest you read the documentation again and then try the migration.

## Slightly breaking changes in 1.7.0

1. Rename the `name` field of ValidationContext class to `path`.
2. Rename generated enums, and corresponding type of code static fields
   - `DictionaryCodes` -> `DictionaryCode`; TOC_VALIDATION_DICTIONARY_CODES_ENUM -> TOC_VALIDATION_DICTIONARY_CODE_ENUM
   - `ErrorCodes` -> `ErrorCode`; TOC_VALIDATION_ERROR_CODES_ENUM -> TOC_VALIDATION_ERROR_CODE_ENUM
   - TOC_VALIDATION_SECURITY_PRIVILEGES_ENUM -> TOC_VALIDATION_SECURITY_PRIVILEGE_ENUM
3. Remove the `SimpleChecker` class.
   It's more readable to use a lambda expression to implement the 'Checker' interface.
4. Refactor of static methods in `CommonCheckers` class:
   - replacing the `range` method with the `ge` and `le` methods for validating values according to the declared 'minimum' and 'maximum' values
   - replacing the `length` method with the `minLength` and `maxLength` methods for validating the length of text fields
   - replacing the `size` method with `minItems` and `maxItems` methods to validate the array size
   - replacing the `list` method with the `items` method for validating array elements

## Slightly breaking changes in 1.5.0

1. Renamed the OperationType enum to HttpMethod.
2. Renamed the `type` filed of Operation class to `method`.
3. Static fields `FIELD_*` moved to validator classes.

## Slightly breaking changes in 1.3.0

1. Generating a name representing an HTTP parameter is now based on parameter name (not on the parameter component element in OAS).
   If this change is too troublesome, you can revert to the previous generator behavior by parameterizing as follows:
   ```
   new JavaModuleParams().setNameMapper(new DefaultJavaNameMapper() {
            @Override String toFieldName(DataSchema schema) {
                toFieldName(schema.code)
            }
        })
   ```
   
2. Mapping the HTTP content as 'body' field in the REQUEST object.
   If this change is too troublesome, you can revert to the previous generator behavior by modify imported RestApi model:
   ```
   api.forEachOperation { it.requestBody?.code = 'requestBody' }
   ```
   
3. The API of the DataSchema nad Parameter classes (due to migration to kotlin).
   
   Changes to the DataSchema class:
   - change accessors names to: isRequired, isNillable, isRootElement
   - remove addAdditiveItems, setAdditiveIfNotSet methods (use the getAdditive(key, supplier) method instead)
   - setters do not return 'this' object anymore, you can use DataType.asSchema method for creating DataSchema object

   The static factory methods 'Parameter.of' have been removed. Regular constructors are available, but the best way to instantiate a Parameter class is to use the static method 'ParamLocation.asParam'.

## Slightly breaking changes in 1.2.0
New classes are generated to improve test writing. If you do not need them completely, you can turn off their generation with 'TestDataParams.enabled = false'.

## Slightly breaking changes in 1.1.0
Introduces *RestClientProvider classes, which took over the responsibility for determining operation endpoints (previously make by the EndpointProvider class).
Move the logic from EndpointProvider to RestClientProvider or call EndpointProvider from it.

## Breaking changes in 1.0.0

1. Replaced Module Builders and Module Builder Configurators with Module Generators.

   All Module Generators are placed in 'pl.metaprogramming.codegen.java.spring' package.

   The following Module Builder Configurators are replaced with Module Generators:
   - SpringCommonsGenerator -> CommonsModuleConfigurator
   - SpringRestInAdapterConfigurator -> SpringRestService2tGenerator
   - SpringRestOutAdapterConfigurator -> SpringRestClient2tGenerator
   - SpringWsOutAdapterConfigurator -> SpringSoapClientGenerator
   - SpringRestClientsConfigurator -> SpringRestClientGenerator
   - SpringRestServicesConfigurator -> SpringRestServiceGenerator

   In addition, the enumerations for type of codes (ClassType, TypeOfCode) have been replaced by fields in Module Generetar classes with the naming convention: 'TOC_*'. In general, the names of code types are compatible with a few exceptions (ClassType.REST_MAPPER -> SpringCommonsGenerator.TOC_SERIALIZATION_UTILS).

   To get familiar with the new conventions, it is recommended to read articles: [the getting started guide](getting-started/how-to-use) and [the codegen core design](guides/core-design).

2. Package refactor

   Packages for many classes have changed.
   - All classes in the 'pl.metaprogramming.codemodel.*' packages have been moved to 'pl.metaprogramming.codegen.java.*'.
   - Package 'pl.metaprogramming.metamodel' has been changed to 'pl.metaprogramming.model'.

3. Default parameters value

   The default value of the parameter `ValidationParams.throwExceptionIfValidationFailed` has been changed to `true`. If you want to be backwards compatible (using ValidationResultMapper to handle validation errors), you should set the value `false` manually.

4. Renamed CodeGenerator to MainGenerator

5. The NEW_LINE field has been moved from JavaCodeFormatter to Codegen

## Breaking changes in 0.10.0

1. Creating objects of type ClassCd, MethodCm, FieldCm, AnnotationCm

   Use the static 'of' methods to instantiate these classes.

   You can also use the 'ClassCd.asField' method to instantiate the FieldCm class.

   In many cases, there is no need to explicitly instantiate these classes. The following methods do it themselves:
   - ClassCmBuildStrategy.addMethod(methodName, methodBuilder)
   - ClassCmBuildStrategy.addMapper(methodName, methodBuilder)
   - ClassCmBuildStrategy.addField(fieldName, fieldType, fieldBuilder)
   - ClassCm.addMethod(methodName, methodBuilder)
   - ClassCm.addField(fieldName, fieldType, fieldBuilder)
   - MethodCm.addParam

2. Removed methods form ClassCmBuildStrategy:

   - addMethod(MethodCm... methods)
   - addMapper(MethodCm... methods)
   - addFields(List fields) and addFields(FieldCm... fields)

   Instead, you should use the previously mentioned methods.

3. Renamed ClassCm.extend to ClassCm.superClass

4. JavaDefs has been removed. The constants that were defined there are mostly available by static methods in ClassCd and AnnotationCm classes.

## Breaking changes in 0.9.0

1. ClassCmBuildStrategy

   The methods supporting the creation of method implementation have been moved to the BaseMethodCmBuilder class.

   Overloads methods makeTransformation and transform was replaced with MappingExpressionBuilder which is returned by new
   mapping method (available in the subclasses of ClassCmBuildStrategy and BaseMethodCmBuilder).
   To create a data transform expression, use the following code `mapping().to(x).from(y).makeExp()`.

   Some methods (getClass / getClassCm / findClass / getGenericClass) have been removed, use the classLocator method to handle more complicated cases.

   The 'addImports' method was replaced with overloads 'addImport' method.

   The 'addMethods' method was replaced with overloads 'addMethod' method.

2. Removed classes:
   - StaticMappersBuildStrategy (use ClassBuilderConfigurator.onDeclaration)
   - StatefulClassCmBuildStrategy

     ClassCmBuildStrategy should not be stateful.
     The only case when it is really needed is when the mapper's implementation should be performed later than its declaration.
     However, creating a stateful ClassCmBuildStrategy object is still possible by overriding the getInstance method.
    
   - InheritFromTemplateMethodCmBuilder (use ClassCmBuildStrategy.addInheritedMapper)
  
3. CommonCheckers
   - 'REQUIRED' field removed, use 'required' method
   - 'NOT_ALLOWED' field removed, use 'notAllowed' method

## Breaking changes in 0.8.0

1. ClassBuilderConfigurator
  
   The following fields was removed: className, classNameSuffix, classNamePrefix.

   To configure class name generation, use the following methods: setFixedName, setNameSuffix, setNamePrefixAndSuffix, setClassNameBuilder.

2. For the SpringRestServices generator, * Facade classes are no longer generated, so that they are still generated, set the value of the SpringRestParams.delegateToFacade parameter to true.


## Breaking changes in 0.7.0

1. The static FormatValidators.instance field has been removed.

   To set the validation for a given data format, use CodegenParams in conjunction with ValidationParams. See '[validation parametrization](guides/validations#validation-parameterization)' for more details.

2. The schema method in the RestApi class has been renamed to updateSchema.

## Breaking changes in 0.6.0

1. The structure of packages inside 'pl.metaprogramming.model' has changed.

   References to classes from package 'pl.metaprogramming.model.**' will result in compilation error. You have to look for a new package for these classes.
   E.g. the package for the OpenapiParser class has been changed from pl.metaprogramming.model.parser.openapi to pl.metaprogramming.model.oas.

2. Changing fields in RestApi model classes.

   The 'xDescriptions', 'attributes', 'description' fields in the 'RestApi' classes ('Operation', 'DataSchema', 'Parameter', 'ObjectType') are replaced with the new 'additives' field.
   The value of the 'description' field is available in the additions field under the "description" key.

## Breaking changes in 0.5.0

1. Generated DTO classes no longer implements 'java.io.Serializable' interface. If you expect certain classes to implement this interface, you can get it by [configuring the generator](guides/recipes#code-generator-modification-recipes).

2. ClassBuilderConfig has been renamed to ClassBuilderConfigurator.

3. ClassAnnotationBuildStrategy has been renamed to AnnotatedBuildStrategy and its package has been changed.

4. The generated BaseDataMapper class has been replaced by the SerializationUtils class.

5. The resourceResolvers, fixedResource, additionalParameters fields have been removed from [OpenapiParserConfig](https://javadoc.io/doc/pl.metaprogramming/codegen/0.4.2/pl/metaprogramming/metamodel/parser/openapi/OpenapiParserConfig.html). To achieve the same effect, you need to [modify the RestApi model after importing it](guides/recipes#restapi-model-modification-recipes).

6. A more aggressive policy of not generating unused codes has been used.

   This may, for example, result in the 'fromValue' method not being generated for the generated enumerations. Generation of selected types of codes can be forced by the 'generateAlways' method on the module configurator object.
   ```
   moduleConfigurator.generateAlways(ClassType.ENUM)
   ```

7. MapperBuilder has been renamed to MethodCmBuilder and its package has been changed

## Breaking changes in 0.4.0

1. [Codegen](https://javadoc.io/doc/pl.metaprogramming/codegen/0.4.0/pl/metaprogramming/codegen/Codegen.html) API

   The following methods have been removed: commonsModule, javaModule, springRestInAdapter, springRestOutAdapter, springWsOutAdapter, mainSetter, newSpringRestInAdapterConfigurator.
   
   To add a module to be generated, use the 'addModule' or 'addCommonsModule' method. Both have only one parameter of the ModuleBuilder type, the value for this parameter can be obtained using the makeBuilder methods from the module configurators (SpringRestInAdapterConfigurator, SpringRestOutAdapterConfigurator, SpringWsOutAdapterConfigurator, CommonsModuleConfigurator).

2. Code generator configurators

   Changes in module configurators API:

   - 'init' has been removed (is no longer needed),
   - 'initRsClient', 'initWsClient', 'addJaxbLocalDateAdapters', 'addEndpointProvider'methods have been removed  (CommonsModuleConfigurator),
   - 'generateAlways' method has been added - you can use it if you want to force the generation of certain classes from the commons module that are no longer generated because they are not used by other generated codes,
   - 'generateOnlyIfUsed' method has been added

   The 'makeDecoration' method has been added to the 'ClassCmBuildStrategy' class. You should implement this method (instead of 'makeImplementation') if the intention is to modify (previously added) methods or fields (eg add annotations or modifiers).

3. Parameterization of code generators
   
   The following ways to parameterize code generators have been removed:
   - from Codegen class: mainSetter, newSpringRestInAdapterConfigurator
   - from JavaModuleConfigurator class: diAutowiredStrategy
   - from SpringRestInAdapterConfigurator: multiOperationControllerStrategy, generateValidatorsForAllObjects (is no longer needed)
   - ControllerConfig.payloadField
   
   Class CodegenParams should be used instead.
   
   First, create a CodegenParams object. E.g. (groovy):
   ```groovy
   new CodegenParams()
      .with(new SpringRestParams(
            controllerPerOperation: false,
            staticFactoryMethodForRestResponse: false,
      ))
      .with(new JavaModuleParams(diStrategy: new DiAutowiredBuildStrategy()))
   ```

   Then use it as a parameter for the module builder configurator constructor (SpringRestInAdapterConfigurator, SpringRestOutAdapterConfigurator, CommonsModuleConfigurator...).

4. API parser

  In general, instead of creating new parser instances, you should use static 'parse' methods.
  
  The SwaggersParser is replaced by [OpenapiParser](https://javadoc.io/doc/pl.metaprogramming/codegen/0.4.0/pl/metaprogramming/metamodel/parser/openapi/OpenapiParser.html), which can parse OAS2 (Swagger 2) as well as OAS3 (OpenAPI 3).
  The parser has been fixed to be more OAS2 compliant when handling the 'in: body' parameter. Now the fields 'required' and 'name' are respected (previously they were always treated like 'required: true', and 'name: requestBody'). If you don't like this behavior then you can tweak the created API model to make it look like it was created by the old SwaggerParser. This can be done with the code (groovy):
  ```groovy
  api.forEachOperation({
      if (it.requestBody) {
         it.requestBody.isRequired = true
         it.requestBody.code = 'requestBody'
      }
  })
  ```
  
  [WsdlParser](https://javadoc.io/doc/pl.metaprogramming/codegen/0.4.0/pl/metaprogramming/metamodel/parser/wsdl/WsdlParser.html) should be configured via [WsdlParserConfig](https://javadoc.io/doc/pl.metaprogramming/codegen/0.4.0/pl/metaprogramming/metamodel/parser/wsdl/WsdlParserConfig.html).


5. Generated REST response classes
   
   Now generated REST response classes does not extends ValueHolder class.

   - In codes where you access response status or body you should remove '.getValue()' part.
   - In the codes where you create a response, you should do so using the static factory methods present in the response class.

   If you want to avoid customizing your codes to the new REST response class interface, you can use the following settings:

   - Set SpringRestParams.staticFactoryMethodForRestResponse = false. Use the following value as a parameter for the 'CommonsModuleConfigurator' constructor:
     ```
     new CodegenParams().with(new SpringRestParams(staticFactoryMethodForRestResponse: false))
     ```

   - Add 'getValue' and 'isPresent' methods for 'REST_RESPONSE_INTERFACE' class. Call the 'update' method as below on the 'CommonsModuleConfigurator' object:
     ```
     update(REST_RESPONSE_INTERFACE, {
                    it.strategies.add(new ClassCmBuildStrategy() {
                        @Override
                        void makeImplementation() {
                            addInterfaces(JavaDefs.I_SERIALIZABLE)
                            addMethods(
                                    new MethodCm(
                                            name: 'getValue',
                                            resultType: classModel,
                                            modifiers: JavaDefs.MODIFIER_DEFAULT,
                                            implBody: 'return this;'),
                                    new MethodCm(
                                            name: 'isPresent',
                                            resultType: JavaDefs.T_BOOLEAN_PRIMITIVE,
                                            modifiers: JavaDefs.MODIFIER_DEFAULT,
                                            implBody: 'return getStatus() != null;'
                                    )
                            )
                        }
                    })
                })
     ```

6. Others
  
  The ModuleWithoutModelBuilder class was removed, use JavaModuleBuilder instead.

## Breaking changes in 0.3.0

1. 'metaModel' was renamed to 'model'

   In order to be more compliant with MDE standards, the naming convention has been changed from /metaModel' to 'model' in some cases.

2. CodeGenerationConfigurator class
   - the class was renamed to 'Codegen'
   - from the 'javaModule' method was removed 'moduleName' parameter (the first one)
   - the 'generate' method has been added - you no longer need to use 'CodeGenerator.run'
   - the 'setForceMode' method has been added - you no longer need to use 'getCfg().setForceMode'. The default value of this parameter has also been changed to 'true'.
   - the 'setAddLastGenerationTag' method has been added - you no longer need to use 'getCfg().setAddLastGenerationTag'. The default value of this parameter has also been changed to 'false'.
   - the 'setCharset' method has been added - you no longer need to use 'getCfg().setCharset'
   - the 'setLineSeparator' method has been added - you no longer need to use 'JavaCodeFormatter.NEW_LINE'

3. ClassCmBuildStrategy class (ClassCmBuildHelper, MethodCmBuilder)

   - The ClassCmBuildHelper interface has been removed, the methods it declares are now part of the ClassCmBuildStrategy class. But following methods ware renamed:
     - getMetaModel -> getModel
     - getBuiltClass -> getClassModel
   - The MethodCmBuilder class and its subclasses have been removed. The helper methods from these classes have been moved to the ClassCmBuildStrategy class. In the ClassCmBuildStrategy class there is a new field codeBuf of type CodeBuffer. It can be used for method's body building. Use the 'take' method to get the code text as it also clears the buffer so that it can be used to build the code of another method.
   - The methods in the ClassCmBuildStrategy class "makeDeclaration" and "makeImplementation" no longer have the "builder" parameter. It is not needed any more, as the methods provided on this parameter are available in the ClassCmBuildStrategy class.
4. ClassCmBuildStrategyWrapper class was removed - use ClassCmBuildStrategy.setMetaModelMapper method instead.

5. REST_REQUEST_MAPPER class type was renamed to REQUEST_MAPPER

6. JavaModuleBuilder class

   The constructor for the JavaModuleBuilder class now takes parameters:
   - String moduleName (new one)
   - JavaModuleConfig config (pre-existing)

   This change also affects the constructs of the inheriting classes:
   - 'SpringRestInAdapterBuilder'
   - 'SpringWsOutAdapterBuilder'
   - 'ModuleWithoutModelBuilder'
