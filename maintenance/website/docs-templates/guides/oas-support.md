---
id: oas-support
title: OpenAPI Specification support
sidebar_label: OpenAPI support
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Import OpenAPI specification

To load OpenAPI specification (OAS2 - swagger/OAS3 - OpenAPI 3) use the static method <javadoc>RestApi.of|RestApi.Companion.of</javadoc>.
This method takes the contents of the OAS specification as an argument, and optionally, dependencies (varargs).

<codeSnippet>REST_API_CREATE</codeSnippet>

## Editing of the OpenAPI model

Before you use the <javadoc>RestApi</javadoc> model to generate the codes, you can modify it.
This, of course, only makes sense for minor technical modifications.

### Skip code generation for selected operations
<codeSnippet>REST_API_OPERATIONS_REMOVE</codeSnippet>

### Grouping of operations
Ready-made generators require REST operations to be grouped.
This can be done in the OpenAPI specification using "tags" or "x-swagger-router-controller".
If it is not done in the specification, it can be done after import as follows:

<codeSnippet>REST_API_OPERATIONS_GROUPING</codeSnippet>

### Addition of a technical parameter for all operations

<codeSnippet>REST_API_ADD_PARAMETER</codeSnippet>

## Export as OpenAPI specification

To save <javadoc>RestApi</javadoc> model as OAS3 specification use the static method <javadoc>OpenapiWriter.write|OpenapiWriter.Companion.write</javadoc>.
There are two variants of this method, one allows you to save the split API into multiple files, the other allows you to save a single flattened file.

<codeSnippet>REST_API_SAVE</codeSnippet>