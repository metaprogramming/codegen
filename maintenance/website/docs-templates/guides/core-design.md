---
id: core-design
title: Core design
sidebar_label: Core design
---
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

![Docusaurus](/img/core-design.png)

## Entrypoint

The entry point to start generating code is the <javadoc>Codegen</javadoc> object.
This is also (although not explicitly) the case for the [gradle](getting-started/quick-start-spring-gradle.md) or [maven plugin](getting-started/quick-start-spring-maven.md).

Using this object, you will compose generators and set common parameters used by them,
such as the character set (default UTF-8) and the line separator (default system line separator).

Using the API it might look something like this:
<codeSnippet>CODEGEN_SAMPLE</codeSnippet>

Note that in the case of Kotlin, the [codegen](pathname:///kotlin-api/codegen/pl.metaprogramming.codegen/codegen.html)
function is used to create the <javadoc>Codegen</javadoc> object and run the generation.

### Generator
As you can guess from the code above. The hardest part will be creating a <javadoc>Generator</javadoc> / <javadoc>GeneratorBuilder</javadoc> object.

The responsibility of the <javadoc>Generator</javadoc> object is to traverse the model (API) and add tasks to generate a certain *type of code* for its particular elements.

For generators that are planned to have a lot of configuration options, it is recommended to use the
<javadoc>GeneratorBuilder</javadoc> / <javadoc>JavaGeneratorBuilder</javadoc> class.
This will allow for simple use of the configuration mechanisms already existing in Codegen and will make it easier to use
for people who already know other generators (in Codegen) and trouble-free use of the new generator in the maven or gradle plugin.

### Ready-made generators
There are available following ready-made generators:
- <javadoc>SpringRestServiceGenerator</javadoc> for generating Spring REST services,
- <javadoc>SpringRestClientGenerator</javadoc> for generating Spring REST client,
- <javadoc>SpringSoapClientGenerator</javadoc> for generating Spring WS client,
- <javadoc>SpringCommonsGenerator</javadoc> for generating utils classes used by above generators.

Even if you plan to create your own generator, a good start would be to review the codes for any of the generators above.

In [how to use it](getting-started/how-to-use.md) article you will learn how to use the above generators in sample projects.

## Type of code and code generation

### Type of code
The type of code is a kind of abstraction for the class. For example, for the REST service, the REST controller is the type of code.
The types of codes will correspond to the particular elements of the solution architecture.

To get a list of code types supported by ready-made generators, just look for static public fields with name `TOC` in the classes of these generators
(e.g. <javadoc>SpringRestServiceGenerator.TOC</javadoc>).

### Code generation

The code that will be generated is defined separately for each type of code.
To do this (or update it), use the <javadoc>JavaGeneratorBuilder.typeOfCode</javadoc> overloaded method.

The <javadoc>ClassBuilderConfigurator</javadoc> class is used to specify the code generation.
You can use one of two approaches:
1. Template-based using the FreeMarker template engine (set <javadoc>freemarkerTemplate|ClassBuilderConfigurator.freemarkerTemplate</javadoc> property)
   or another template engine (set <javadoc>template|ClassBuilderConfigurator.template</javadoc> property).
2. By creating a code model using the following methods:
   - <javadoc>onDeclaration|ClassBuilderConfigurator.onDeclaration</javadoc>
   - <javadoc>onImplementation|ClassBuilderConfigurator.onImplementation</javadoc>
   - <javadoc>onDecoration|ClassBuilderConfigurator.onDecoration</javadoc>
   - <javadoc>builders|ClassBuilderConfigurator.builders</javadoc> property

While the first approach is simpler, the second is much more flexible and allows you to:
- reusing code generation logic in generating different types of code,
- dividing the code generation logic into independent parts,
- making nontrivial modifications to an existing generator without modifying the existing code generation logic and easily turning them on and off as needed.

Using the <javadoc>ClassBuilderConfigurator</javadoc> you can also set:
- class name,
- location for the generated classes (package and project).

### Code Model

The code model is represented by the <javadoc>ClassCm</javadoc> object.
And the <javadoc>ClassCmBuilderTemplate</javadoc> object is used to construct it.

When using the <javadoc>onDeclaration|ClassBuilderConfigurator.onDeclaration</javadoc>,
<javadoc>onImplementation|ClassBuilderConfigurator.onImplementation</javadoc>,
<javadoc>onDecoration|ClassBuilderConfigurator.onDecoration</javadoc> methods,
the ClassCmBuilderTemplate object will simply be provided (as method parameter) for use.

When using the <javadoc>builders|ClassBuilderConfigurator.builders</javadoc> property to implement the <javadoc>IClassCmBuilder</javadoc> interface,
you can inherit from the <javadoc>ClassCmBuilderTemplate</javadoc> class.

### Methods

The method code model is represented by the <javadoc>MethodCm</javadoc> class.
It describes the input parameters, return type, visibility modifiers, and annotations.

To add a method to a class use the <javadoc>ClassCm.methods</javadoc> / <javadoc>ClassCmBuilderTemplate.methods</javadoc> property.
If the code generating the method body is a bit more complicated, encapsulate it in a class that implements <javadoc>IMethodCmBuilder</javadoc> interface.
You can also inherit from <javadoc>BaseMethodCmBuilder</javadoc> class.
To create implementation body you can use a codeBuf property (available in both <javadoc>BaseMethodCmBuilder</javadoc> and <javadoc>ClassCmBuilderTemplate</javadoc>).

## Code index

The code index is represented by an object of type <javadoc>CodeIndex</javadoc>.

It is mainly used to register generated code and search it by given parameters.
This allows loose coupling of generators codes, even if the codes generated by them may be related to each other.

Code index is an index of:
- classes, where the key is the *type of code* and model element,
- methods (`mappers`), where the key is the return value type and the list of parameter types.

### Mappers

Not all generated methods are registered in the code index as mappers.
Registering a method as a mapper is done using the <javadoc>BuildContext.registerMapper</javadoc> method,
although in practice it may be more convenient to use the <javadoc>MethodCm.registerAsMapper</javadoc> method,
but this should be done when adding the method to the <javadoc>ClassCmMethods</javadoc> object (not after adding it).

Registering a method as a mapper in the code index will allow other parts of the generator to search for it based on the
return value type and list of types of input parameters. This means that the generator codes do not need to know either
the name of the class or the method that will be used in the generated codes.

### Class registration in the code index

All generated classes are registered in the code index.
Generators use the <javadoc>JavaGenerator.register</javadoc> method for this purpose.

### Non-generated codes in the code index

In some cases, the code index can also be used to hook external codes to be used in generated codes.
For this reason, the <javadoc>CodeIndex</javadoc> is also available in the <javadoc>Codegen|Codegen.codeIndex</javadoc> object.

### Code index search

To search for a class corresponding to the type of code for a given model fragment, you can use
<javadoc>ClassCmBuilderTemplate.getClass</javadoc>
/ <javadoc>ClassCmBuilderTemplate.classLocator</javadoc>
/ <javadoc>BaseMethodCmBuilder.getClass</javadoc>
/ <javadoc>BaseMethodCmBuilder.classLocator</javadoc>.

To search for a mapper, you can use <javadoc>BaseMethodCmBuilder.mapping</javadoc> or directly <javadoc>MappingExpressionBuilder</javadoc>.

The following code illustrates the use of the code register in the above-mentioned scopes:

<codeSnippet>CODE_INDEX_MAPPER</codeSnippet>

## Generation phases

There are following phases of creating a code model:
- registration: when <javadoc>Generator</javadoc> register a class for a given fragment of the model that the given type of code will be built,
- declaration: in this phase the class elements that are used in building other classes must be declared (e.g. registering mappers),
- implementation: building the code model of the class,
- decoration: decorate the class model (e.g. adding an annotation; in the basic generator codes it is best to avoid changes in this phase so that the generator user can safely use it to make their modifications).

## Not generating unnecessary codes

There may be situations when you do not want to generate codes for all API elements.
For example, you may be interested in using only one of many operations from the REST API.
In this case, at the <javadoc>RestApi</javadoc> model level, you should simply remove unwanted operations
and use the <javadoc>SpringRestClientGenerator</javadoc> generator.

As a result, not only the codes of the calls to the removed operations will not be generated, but also the DTO classes accordingly.

Whether a given type of code is always designated for generation, or is designated for generation only when used
by other code designated for generation, is controlled by the <javadoc>ClassBuilderConfigurator.forceGeneration</javadoc> property.

Subsequently, this property is copied to <javadoc>ClassCd.forceGeneration</javadoc> during code generation.

If the <javadoc>ClassCd.forceGeneration</javadoc> property is `false`, the <javadoc>ClassCd.used</javadoc> property is used
to determine whether the class should be generated.

The <javadoc>ClassCd.used</javadoc> property can be set to `true` in the implementation phase
when the class is referenced from another class that has the
<javadoc>forceGeneration|ClassCd.forceGeneration</javadoc> or <javadoc>used|ClassCd.used</javadoc> property set to `true`.

The implementation and decoration phase is performed only for classes
that have the <javadoc>forceGeneration|ClassCd.forceGeneration</javadoc> or <javadoc>used|ClassCd.used</javadoc> property set to `true`.

## Codegen params

<javadoc>CodegenParams</javadoc> allows you to configure selected aspects that affect the generation of various types of codes.
It is a composition of different sets of parameters for different parts of the generator.

Creating a <javadoc>CodegenParams</javadoc> object might look like this:
<codeSnippet>CODEGEN_PARAMS</codeSnippet>

<javadoc>CodegenParams</javadoc> is basically a collection of any parameter objects, where the key for the object is their class.

There are two ways to set these parameter objects (via the overloaded <javadoc>set|CodegenParams.set</javadoc> method).
- simple passing of a parameter object; the previous object of this class will be overwritten,
- passing the class and update function of the parameter object;
  if the object of this class did not exist, it will be created and set according to the update function;
  if it existed, it will be corrected by the update function.

<javadoc>CodegenParams</javadoc> can be set at the global code generation level using <javadoc>Codegen.params</javadoc>.
The parameters set here will be copied to the included generators (when calling <javadoc>Codegen.generate</javadoc>).
If there is a need to set specific parameters at the generator level, it can be done using <javadoc>GeneratorBuilder.params</javadoc>.

If it is required that the parameters object is copied deep, it should have a method called `copy` which should perform deep copying.

In the case of ready-made generators for this purpose, you will use the following classes:
- <javadoc>JavaParams</javadoc> to parameterize:
  - the annotation used to mark the generated codes (@Generated),
  - aspects related to bean management and dependency injection,
  - the name mapping from the model to the java code,
  - the text processing of generated java codes (license header, code formatting),
  - the generation of the 'fromValue' method for enums (regardless of its use in the generated codes)
- <javadoc>ValidationParams</javadoc> to parameterize:
  - the error codes for standard validations,
  - the stopping of (or continuing) validation when a validation error is encountered (<javadoc>ValidationParams.stopAfterFirstError</javadoc>),
  - validators for data <javadoc>formats|ValidationParams.formatValidator</javadoc> and <javadoc>rules|ValidationParams.ruleValidator</javadoc> (see [Validation support](validations) guide),
- <javadoc>SpringRestParams</javadoc> to parameterize the generation of REST server/client codes
- <javadoc>SpringSoapParams</javadoc> to parameterize the generation of WS/SOAP client codes

## Data type configuration

Data type configuration in the generator, i.e. setting the representation for basic data types (numbers, dates, etc.)
in the generated code should be done using <javadoc>DataTypeMapper</javadoc>.

Configuration can be done globally via <javadoc>Codegen.dataTypeMapper</javadoc>.
This configuration will be copied to specific generators (when <javadoc>Codegen.generate</javadoc> is called).

However, if any change to this configuration is required at the level of a specific generator,
<javadoc>GeneratorBuilder.dataTypeMapper</javadoc> should be used.