module.exports = {
  someSidebar: [
    {
      'Codegen': [
        'introduction',
        'why',
        'contribution'
      ]
    },
    {
      'Getting Started': [
        'getting-started/how-to-use',
        'getting-started/quick-start-spring-gradle',
        'getting-started/quick-start-spring-maven',
        'getting-started/quick-start-spring-maven-complex'
      ]
    },
    {
      'Guides': [
        'guides/core-design',
        'guides/oas-support',
        'guides/validations',
        'guides/recipes',
        'guides/file-index'
      ]
    },
    {
      'Changes': [
        'changelog',
        'migration-guide'
      ]
    }
  ]
};
