https://jojozhuang.github.io/tutorial/mermaid-cheat-sheet/

core-design.png

```mermaid
graph TD
        MP[ModelParser] --- MF["ModelFile (OpenAPI)"]
        MP --- M["Model (RestApi)"]
        MBC[ModuleBuilderConfigurator] --- M
        MBC --- MB[ModuleBuilder]
        CG[Codegen] --- MB
        CG --- CM[CodeModel]
        CG --- CF[CodeFiles]
```
