
* konfigurator generacji
* metamodel (np.: REST/SOAP webservices)
  - parser
* codemodel (np.: adapter i port we/wy REST/SOAP webservices)
  - konfigurator transformacji metamodel -> codemodel
  - transfrormacja metamodel -> codemodel
  - transformacja codemodel -> pliki źródłowe

# Struktura kodów

Wyróżniamy trzy główne części:

* kod sterujący: kontroler/API
* metamodel, to "abstrakcyjnego" opisu komponentu (interfejs)
* model kodów, do szczegółowego opisu kodu, który będzie generowany


## Kontroler/API - kod sterujący

`pl.metaprogramming.codegen`


Na wejściu przyjmuje **metamodel** oprogramowania oraz parametry generowania kodów (**budowniczy modelu kodów**). Generowany jest z tego **model kodów**, a następnie pliki kodów.

Rejestruje wygenerowane pliki kodów.

Nowe wygenerowanie kodów powoduje nadpisanie starych plików z kodami, a jeżeli nowe kody nie zawierają już pewnych plików, to są one usuwane.



## Metamodel

`pl.metaprogramming.model.model`


Abstrakcyjny model tworzonego oprogramowania


### Metamodel danych

`pl.metaprogramming.model.model.data`


Dane opisywane są przy użyciu pojęć:

* **typ danych (`DataType`)**, opisuje dane "w sobie" (np.: liczba całkowita, tekst, data, struktura), typ danych przekłada się na typ w języku programowania
* **schemat danych (`DataSchema`)**, opisuje dane w kontekście ich użycia, określa nazwy (pól), wymagalność/opcjonalność, foramat (serializacja/deserializacja)

Szczególnym przypadkiem jest definicja struktury (`ObjectType`), która sama w sobie jest definicją typu danych, lecz do tego celu zostało użyte pojęcie schematu danych.

### Metamodel danych

`pl.metaprogramming.model.model.rest`


Model interfejsu usług REST.


### Budzowniczy metamodelu

`pl.metaprogramming.model.model.builder`


Kody używane do opisu metamodeli.


### Impor metamodelu

`pl.metaprogramming.model.model.parser`


Kody do importowania metamodeli z otwartych standardów:

* swagger 2.0 (`pl.metaprogramming.model.model.parser.swagger`)


## Model kodów

`pl.metaprogramming.codemodel.model`


Model kodów stanowi dokładny opis kodu, który będzie wygenerowany.

Klasy opisujące kod są opatrzone przyrostkiem `Cm` (np.: `ClassCm`).

Ponadto istnieje klasa `ClassCd`, która opisuje klasy (deklaracja klasy) używane w modelu kodu, ale nie zawiera modelu kodu tej klasy.


## Budowniczy modelu kodów

`pl.metaprogramming.codemodel.builder`


Kody, które na podstawie metamodelu budują modelu kodów.
