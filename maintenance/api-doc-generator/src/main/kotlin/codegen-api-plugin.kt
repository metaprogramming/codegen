/*
 * Copyright (c) 2023 Dawid Walczak.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.metaprogramming.dokka

import org.jetbrains.dokka.base.DokkaBase
import org.jetbrains.dokka.base.signatures.KotlinSignatureUtils.driOrNull
import org.jetbrains.dokka.base.transformers.documentables.DocumentableReplacerTransformer
import org.jetbrains.dokka.base.transformers.documentables.SuppressedByConditionDocumentableFilterTransformer
//import org.jetbrains.dokka.kotlinAsJava.KotlinAsJavaPlugin
import org.jetbrains.dokka.model.*
import org.jetbrains.dokka.model.doc.*
import org.jetbrains.dokka.pages.*
import org.jetbrains.dokka.plugability.*
import org.jetbrains.dokka.transformers.documentation.PreMergeDocumentableTransformer
import org.jetbrains.dokka.transformers.pages.PageTransformer

data class CodegenApiPluginConfiguration(val lang: String = "java") : ConfigurableBlock

val emptyPreMergeDocumentableTransformer = object : PreMergeDocumentableTransformer {
    override fun invoke(modules: List<DModule>) = modules
}

class CodegenApiPlugin : DokkaPlugin() {
    private val dokkaBase by lazy { plugin<DokkaBase>() }

    // removes from documentation java/kotlin specific functions
    val filterLangSpecificFunctions by extending {
        dokkaBase.preMergeDocumentableTransformer providing { ctx ->
            if (ctx.isKotlinApi()) CodegenKotlinApiFilter(ctx) else CodegenJavaApiFilter(ctx)
        }
    }

    // copy documentation from properties to getters/setters (only java)
    val fixAccessorsDocumentation by extending {
        dokkaBase.preMergeDocumentableTransformer providing { ctx ->
            if (ctx.isKotlinApi()) emptyPreMergeDocumentableTransformer else SetupJavaSettersByProperties(ctx)
        }
    }

    // remove properties section (only java)
    val fixPageModelBeforeRendering by extending {
        dokkaBase.htmlPreprocessors providing { ctx ->
            if (ctx.isKotlinApi()) PageTransformer { it } else RemoveClassPropertiesTransformer()
        }
    }

    override fun pluginApiPreviewAcknowledgement(): PluginApiPreviewAcknowledgement {
        TODO("Not yet implemented")
    }

    private fun DokkaContext.isKotlinApi() =
        "kotlin" == configuration<CodegenApiPlugin, CodegenApiPluginConfiguration>(this)?.lang
}

class SetupJavaSettersByProperties(context: DokkaContext) : DocumentableReplacerTransformer(context) {

    private var currentClass: DClasslike? = null
    override fun processClassLike(classlike: DClasslike): AnyWithChanges<DClasslike> {
        currentClass = classlike
        if (classlike.name == "Codegen") {
            println("processClassLike: ${classlike.name}")
        }
        return super.processClassLike(classlike)
    }

    override fun processFunction(dFunction: DFunction): AnyWithChanges<DFunction> {
        currentClass?.findProperty(dFunction.name)?.let { dProperty ->
            if (dFunction.documentation.isEmpty() && dProperty.documentation.isNotEmpty()) {
                val sourceSet = dProperty.documentation.keys.first()
                val propDoc = dProperty.documentation.values.first()
                val docRoot = propDoc.children.first().root
                val propTitle = (docRoot.children.first().children.first() as Text).body
                val setterTitle = "Setter for the ${dFunction.name} - $propTitle"
                val docs = mutableListOf<DocTag>(P(children = listOf<DocTag>(Text(setterTitle))))
                if (docRoot.children.size > 1) docs.addAll(docRoot.children.drop(1))
                val description = Description(CustomDocTag(children = docs, name = "MARKDOWN_FILE"))
                return AnyWithChanges(
                    dFunction.copy(
                        documentation = mapOf(sourceSet to DocumentationNode(listOf(description)))
                    ), true
                )
            }
        }
        return super.processFunction(dFunction)
    }
}

abstract class DocumentableFilterTransformer(context: DokkaContext) :
    SuppressedByConditionDocumentableFilterTransformer(context) {
    private var currentClass: DClasslike? = null

    override fun shouldBeSuppressed(d: Documentable): Boolean {
        if (d is DClasslike) {
            currentClass = d
        }
        return d is DFunction && shouldBeSuppressed(d, currentClass!!)
                || d is DProperty && shouldBeSuppressed(d, currentClass!!)
    }

    open fun shouldBeSuppressed(f: DFunction, owner: DClasslike): Boolean = false
    open fun shouldBeSuppressed(p: DProperty, owner: DClasslike): Boolean = false
}

class CodegenKotlinApiFilter(context: DokkaContext) : DocumentableFilterTransformer(context) {

    override fun shouldBeSuppressed(f: DFunction, owner: DClasslike): Boolean {
        if (f.name == "register") {
            println(f)
        }
        val type = if (f.parameters.size == 1) f.parameters[0].type.toCanonicalName() else null
        return type != null && owner.hasProperty(f.name, type) // is java fluent API setter
                || owner.hasKotlinAlternative(f) // has java.lang.Class param but there is function with the same name but with KClass param
    }

    private fun DClasslike.hasKotlinAlternative(f: DFunction): Boolean {
        val javaClassParams = f.findParamsIndexes("java.lang.Class")
        return javaClassParams.isNotEmpty() && functions.any {
            it != f && it.name == name && it.parameters.size == f.parameters.size
                    && it.findParamsIndexes("kotlin.reflect.KClass") == javaClassParams
        }
    }
}

class CodegenJavaApiFilter(context: DokkaContext) : DocumentableFilterTransformer(context) {
    override fun shouldBeSuppressed(f: DFunction, owner: DClasslike) = f.receiver != null // funkcja z odbiornikiem
            || f.isLastParamExtensionFunction() // funkcja gdzie ostatni parametr jest typu funkcyjnego z odbiorcą (T.() -> Unit)
            || f.parameters.any { "${it.type.driOrNull}".startsWith("kotlin.") } // funkcja gdzie jakikolwiek parametr jest z biblioteki standardowej kotlin

    override fun shouldBeSuppressed(p: DProperty, owner: DClasslike) =
        owner.hasJavaFluentSetter(p) // jest "fluent API" setter, nie chcemy ani property, ani gettera i settera

    private fun DFunction.isLastParamExtensionFunction(): Boolean {
        if (parameters.isNotEmpty()) {
            val lastParam = parameters.last().type
            return lastParam is FunctionalTypeConstructor && lastParam.isExtensionFunction
        }
        return false
    }
}

class RemoveClassPropertiesTransformer : PageTransformer {
    override fun invoke(input: RootPageNode) = input.modified(children = filter(input.children))

    private fun filter(children: List<PageNode>): List<PageNode> {
        return children.map {
            when (it) {
                is ModulePageNode -> it.modified(children = filter(it.children))
                is PackagePageNode -> it.modified(children = filter(it.children))
                is ClasslikePageNode -> it.modified(content = it.content.removeProperties())
                else -> it
            }
        }
    }

    private fun ContentNode.removeProperties(): ContentNode {
        if (this !is ContentGroup) return this
        return copy(children = children.filter { !it.isPropertiesContainer() }.map { it.removeProperties() })
    }

    private fun ContentNode.isPropertiesContainer() =
        this is ContentGroup && children.size == 2 && children[0] is ContentHeader && children[1] is ContentTable
                && children[0].children.size == 1 && children[0].children[0] is ContentText
                && (children[0].children[0] as ContentText).text == "Properties"

}

private fun DFunction.findParamsIndexes(className: String): List<Int> {
    val result = mutableListOf<Int>()
    parameters.forEachIndexed { index, dParameter ->
        val typeClass = dParameter.type.toCanonicalName()
        if (typeClass == className) result.add(index)
    }
    return result
}

private fun DClasslike.hasProperty(name: String, canonicalName: String) = findProperty(name)?.type?.toCanonicalName() == canonicalName

private fun DClasslike.findProperty(name: String): DProperty? {
    return properties.find {
        it.name == name && it.visibility.values.contains(KotlinVisibility.Public)
    }
}

private fun DClasslike.hasJavaFluentSetter(p: DProperty) = null != functions.find {
    it.parameters.size == 1 && it.name == p.name// && it.parameters[0].type == p.type
}

private fun Bound.toCanonicalName() = driOrNull?.let { "${it.packageName}.${it.classNames}" }